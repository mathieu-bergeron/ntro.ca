---
title: "Config serveur OVH"
weight: 30
bookCollapseSection: true
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt>}}
**NOTE**

* les commandes se font sur le serveur, sauf indication contraire
* pour simplifier on nomme le serveur `aiguilleur` plutôt que son nom complet
{{</excerpt>}}

## Config de départ serveur OVH

1. Update et redémarrer après update

```bash
$ sudo dnf install -y epel-release
$ sudo dnf update
$ sudo reboot
```

1. Installer vim

```bash
$ sudo dnf install -y vim
```


1. Config SSH

```
$ sudo vim /etc/ssh/sshd_config

    PermitRootLogin no

    PasswordAuthentication no

$ sudo systemctl restart sshd
```

### Ajout des usagers `mbergeron` et `vgiroux`

1. Création des usagers

```bash
$ sudo useradd mbergeron
$ sudo useradd vgiroux
```

1. Installer les clés SSH de `mbergeron`

    ```bash
    [moi@mon-ordi]$ ssh almalinux@aiguilleur "mkdir cles_ssh_mbergeron"
    [moi@mon-ordi]$ scp id_rsa almalinux@aiguilleur:/cles_ssh_mbergeron/
    [moi@mon-ordi]$ scp id_rsa.pub almalinux@aiguilleur:/cles_ssh_mbergeron/

    [moi@mon-ordi]$ ssh almalinux@aiguilleur 
    [almalinux@aiguilleur]$ sudo cp -r cles_ssh_mbergeron /home/mbergeron/.ssh
    [almalinux@aiguilleur]$ sudo chown -R mbergeron.mbergeron /home/mbergeron/.ssh
    [almalinux@aiguilleur]$ sudo -iu mbergeron

    [mbergeron@aiguilleur]$ chmod 700 .ssh
    [mbergeron@aiguilleur]$ chmod 600 .ssh/id_rsa
    [mbergeron@aiguilleur]$ chmod 600 .ssh/id_rsa.pub
    [mbergeron@aiguilleur]$ cat .ssh/id_rsa.pub >> .ssh/authorized_keys
    [mbergeron@aiguilleur]$ chmod 600 .ssh/authorized_keys
    ```

1. Installer les clés SSH de `vgiroux`

    ```bash
    ...
    ```

1. Autoriser la connexion en tant que `almalinux` pour `mbergeron` et `vgiroux`

    ```bash
    $ sudo cat /home/mbergeron/.ssh/id_rsa.pub >> .ssh/authorized_keys
    $ sudo cat /home/vgiroux/.ssh/id_rsa.pub >> .ssh/authorized_keys
    ```

### Recommandations de OVH

Voir https://help.ovhcloud.com/csm/fr-vps-security-tips?id=kb_article_view&sysparm_article=KB0047708

1. Ne pas changer le port SSH, le pare-feu de l'école bloquera un autre port

1. Config du pare-feu du serveur

    ```bash
    $ sudo dnf install -y firewalld
    $ sudo systemctl enable firewalld
    $ sudo systemctl start firewalld

    $ sudo firewall-cmd --zone=public --permanent --add-service=http
    $ sudo firewall-cmd --zone=public --permanent --add-service=https
    $ sudo firewall-cmd --zone=public --permanent --add-service=ssh         
    $ sudo firewall-cmd --reload
    ```

    * NOTE: l'ajout de `ssh` n'est pas nécessaire, cette règle est là par défaut

1. Ajout de `fail2ban` selon les recommandations d'OVH

    ```bash
    $ sudo dnf install -y fail2ban fail2ban-firewalld
    $ sudo systemctl enable fail2ban
    $ sudo systemctl start fail2ban
    ```

    * **TODO** modifier `/etc/fail2ban/jail.conf` selon nos besoins 
        * p.ex. filtre les requêtes HTTP répétées rapidement?

1. Config du pare-feu OVH selon les recommandations d'OVH

    * https://help.ovhcloud.com/csm/en-ca-dedicated-servers-firewall-network?id=kb_article_view&sysparm_article=KB0043446

## Installation de docker et docker-compose

1. Installer `docker`

    ```bash
    $ sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
    $ sudo dnf update

    $ sudo dnf install -y docker-ce docker-ce-cli containerd.io

    $ sudo systemctl enable docker
    $ sudo systemctl start docker
    ```

1. Installer `docker-compose`

    ```bash
    $ sudo curl -SL https://github.com/docker/compose/releases/download/v2.18.1/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
    $ sudo chmod a+x /usr/local/bin/docker-compose
    $ sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
    ```

## Cloner les dépôts + créer les dockers

1. Installer et configurer Git

    ```bash
    $ sudo dnf install -y git
    $ git config --global user.name "Admin aiguilleur.ca"
    $ git config --global user.mail "aiguilleur.ca@gmail.com"
    ```

1. Cloner le dépôt racine

    ```bash
    $ git clone git@gitlab.com:mathieu-bergeron/racine.aiguilleur.ca aiguilleur.ca
    ```

1. Lancer le script pour cloner les dépôts

    ```bash
    $ cd aiguilleur.ca
    $ sh scripts/init.sh
    ```

## Créer les Dockers la première fois (c'est long!)

1. Choisir la config (ovh version dev pour commencer):

    ```
    $ cd dockers
    $ sh select_config_file.sh config-ovh-dev.env
    ```

1. Vérifier la config 

    ```
    $ sh check_config_file.sh
    CONFIG FILE: config-ovh-dev.env
    ```

1. Créer les dockers pour la première fois (attendre ~5 minutes)

    ```bash
    $ sh compose.sh build
    ```


## Modifier les permissions pour `/var/lib/docker/volumes`

1. Rendre le répertoire accessible (mais pas lisible, pas besoin)

    ```bash
    $ sudo chmod o+x /var/lib/docker
    $ sudo chmod -R o+x /var/lib/docker/volumes
    ```

## Créer le répertoire `ntro.ca` pour des fichiers statiques

1. À refaire pour chaque prof qui demande un site statique


    ```bash
    sudo mkdir /var/lib/docker/volumes/www/_data/ntro.ca

    sudo chown -R root.mbergeron /var/lib/docker/volumes/www/_data/ntro.ca
    sudo chmod -R g+rwx /var/lib/docker/volumes/www/_data/ntro.ca


    sudo -iu mbergeron
    [mbergeron@aiguilleur] mkdir www
    [mbergeron@aiguilleur] cd www
    [mbergeron@aiguilleur] ln -s /var/lib/docker/volumes/www/_data/ntro.ca ntro.ca
    ```


## TODO

1. Config pour update et reboot automatique à chaque dimanche 3am

1. Config pour redémarrer automatiquement les Dockers après chaque reboot (planifié ou pas)

## Cloner le dépôt racine


## Cloner les trois dépôts principaux avec `init.sh`



1. Voici le contenu des dépôts

    ```bash
    $ cd aiguilleur.ca
    $ tree
    ```
