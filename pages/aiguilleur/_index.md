---
title: "Procédures aiguilleur.ca (Aquiletour)"
weight: 30
bookCollapseSection: true
bookHidden: true
---

{{% pageTitle %}}

## Cloner les dépôts + créer les dockers

1. Installer et configurer Git

    ```bash
    $ sudo dnf install -y git
    $ git config --global user.name "Mon Nom"
    $ git config --global user.mail "MonCourriel@Bidon.com"
    ```

1. Cloner le dépôt racine

    ```bash
    $ git clone git@gitlab.com:mathieu-bergeron/racine.aiguilleur.ca aiguilleur.ca
    ```

1. Lancer le script pour cloner les dépôts

    ```bash
    $ cd aiguilleur.ca
    $ sh scripts/init.sh
    ```

## Créer les Dockers la première fois (c'est long!)

1. Choisir la config (ovh version dev pour commencer):

    ```
    $ cd dockers
    $ sh select_config_file.sh config-ovh-dev.env
    ```

1. Vérifier la config 

    ```
    $ sh check_config_file.sh
    CONFIG FILE: config-ovh-dev.env
    ```

1. Créer les dockers pour la première fois (attendre ~5 minutes)

    ```bash
    $ sh compose.sh build
    ```

1. Utiliser `/etc/hosts` pour tester avec les certificats

    ```bash
    $ vim /c/Windows/System32/drivers/etc/hosts
    # ajouter

    127.0.0.1 aiguilleur.ca

    # pas oublier de retirer plus tard!
    ```

## Config serveur OVH

* [Voir ici](./serveur_ovh/)
