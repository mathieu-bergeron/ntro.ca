---
title: "Motivation"
weight: 10
# bookFlatSection: false
# bookToc: true
# bookHidden: false
bookCollapseSection: true
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

(<a href="../source">accès au code source</a>)

## Présentation visuelle de la solution

Communiquer l'objectif à réaliser autrement que par du texte.

{{% animation src="/ntro/demo/motivation/solution.mp4" %}}

## Animation du code de l'étudiant

Introduction en douceur au déboggeur.

{{% animation src="/ntro/demo/motivation/code_etudiant.mp4" %}}

## Validation automatique du résultat

L'outil affiche des ✓ quand l'exercice est réussi:

<center>
<img class="small-figure" src="exercice02_validation.png" />
</center>

## Expliquer en manipulant des cartes


<img class="small-figure" src="travail02.jpg"/>

crédit photo: Marlond Augustin


<img class="small-figure" src="/approche/trier/trier_par_sorte.jpg" />
<img class="small-figure" src="/approche/trier/trier_par_numero01.jpg" />
<img class="small-figure" src="/approche/trier/trier_par_numero03.jpg" />
<img class="small-figure" src="/approche/trier/trier_par_numero02.jpg" />

crédit photo: Mikael Tardif

