---
title: "Créer le projet Eclipse"
weight: 10
# bookFlatSection: false
# bookToc: true
bookHidden: true
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

* Extraire `demo.zip`

* Naviguer jusqu'au répertoire contenant les fichiers suivants:

<img src="dossier_racine.png" />

* Ouvrir un GitBash dans ce répertoire

    * *Clic-droit* dans espace vide => *Git Bash here*

<img src="git_bash_here.png" />


* Dans GitBash, exécuter la commande suivante

    ```bash
    $ sh gradlew eclipse
    ```

    * ce script télécharge Gradle et toutes les dépendances du projet
    * et ensuite compile le tout
    * (ce qui va prendre un certain temps)

* Le projet peut maintenant être importé en Eclipse


