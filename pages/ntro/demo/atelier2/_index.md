---
title: "Atelier 2: un exemple"
weight: 20
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

(<a href="../source">accès au code source</a>)

## Préambule

* Au besoin, <a href="./installer_outils">installer Eclipse/Git/JDK</a>
* Télécharger et extraire {{% download "demo.zip" "demo.zip" %}}, puis
   <a href="./creer_projet">créer le projet avec</a>

    ```
    $ cd demo
    $ sh gradlew eclipse
    ```

* <a href="./importer_projet">Importer le projet</a> `demo` en Eclipse 

## Créer la classe `MonAtelier2`

* Ajouter la classe suivante au projet `demo`
    * Nom de la classe: `MonAtelier2`
    * **cocher** `public static ...`

* Débuter l'implantation comme suit:

```java
{{% embed "MonAtelier2.java" %}}
```

* Utiliser {{% key "Ctrl+1" %}} pour importer `Atelier2`

* Utiliser {{% key "Ctrl+1" %}} pour ajouter les méthodes obligatoires

## Exécuter l'outil de validation

* Clic-droit sur `MonAtelier2.java` => *Run As* => *Java Application*

* Voici comment l'outil fonctionne

    <img src="outil.png"/>

    1. Les cartes crées par le programme
    2. Choix de l'exercice: *Mon code* ou *Solution*
    3. Avancer/reculer dans les étapes d'exécution
    4. Ligne de code courante

## Pour créer une carte

* Une variable de type `String` va créer une carte, p.ex.

<center>
<table>
<tr>
<th>chaîne</th>
<td><code>"2_CO"</code></td>
<td><code>"1_CA"</code></td>
<td><code>"3_TR"</code></td>
<td><code>"4_PI"</code></td>
<td><code>null</code></td>
</tr>

<tr>
<th>carte</th>
<td><img style="max-width:75px;" src="2_CO.png"/></td>
<td><img style="max-width:75px;" src="1_CA.png"/></td>
<td><img style="max-width:75px;" src="3_TR.png"/></td>
<td><img style="max-width:75px;" src="4_PI.png"/></td>
<td><img style="max-width:75px;" src="null.png"/></td>
</tr>
</table>
</center>

## Travail à réaliser

### La méthode `exercice01` qui

* crée une carte
* crée un tableau de cartes

#### Résultat désiré

<img src="exercice01.png"/>

#### Validation

L'outil affiche un ✓ quand l'exercice est réussi:

<center>
<img class="small-figure" src="exercice01_validation.png" />
</center>



### La méthode `exercice02` qui

* reçoit une `taille` en entrée
* crée un tableau de cartes

#### Résultats désirés

<table>
<tr>
<th>
<code>taille</code>
</th>
<th>
résultat désiré
</th>
</tr>


<tr>
<td>
3
</td>
<td>
<img src="exercice02_3.png"/>
</td>
</tr>

<tr>
<td>
5
</td>
<td>
<img src="exercice02_5.png"/>
</td>
</tr>

<tr>
<td>
9
</td>
<td>
<img src="exercice02_9.png"/>
</td>
</tr>



</table>

#### Validation

L'outil affiche des ✓ quand l'exercice est réussi:

<center>
<img class="small-figure" src="exercice02_validation.png" />
</center>

