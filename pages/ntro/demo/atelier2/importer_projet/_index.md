---
title: "Importer le projet en Eclipse"
weight: 10
# bookFlatSection: false
# bookToc: true
bookHidden: true
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

* En Eclipse, choisir *File* => *Import*

* Ensuite, choisir *General* => *Existing Projects into Workspace*

* Cliquer sur *Browse* et naviguer jusqu'au répertoire contenant `.gradle`, `buildSrc`, etc.

    * Sélectionner `demo`

        <img class="small-figure" src="selectionner_demo.png"/>

* Cliquer sur *Finish*

