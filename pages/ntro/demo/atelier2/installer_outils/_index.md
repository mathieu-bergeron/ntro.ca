---
title: "Installer Eclilpse/Git/JDK"
weight: 10
# bookFlatSection: false
# bookToc: true
bookHidden: true
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

## Eclipse

* La version la plus récente: https://www.eclipse.org/downloads/

* Choisir l'option Java IDE


## Git

* https://git-scm.com/downloads

* on a surtout besoin de GitBash

## JDK

* Testé avec le JDK17:  https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html
* (et aussi avec JDK11 et OpenJDK)
