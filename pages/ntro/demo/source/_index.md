---
title: "Accès au code source"
weight: 40
# bookFlatSection: false
# bookToc: true
# bookHidden: false
bookCollapseSection: true
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

## Dépôt principal

https://gitlab.com/mathieu-bergeron/ca.ntro.cards

Pour créer les projets eclipse

```bash
$ cd ca.ntro.cards
$ sh gradlew eclipse
```

Le projet qui correspond à la démo est

* `intro_procedure`
* voir en particulier le paquet `ca.ntro.cards.intro.solution_via_jdi`

## Dépôt `solutions`

https://gitlab.com/mathieu-bergeron/ca.ntro.cards.solutions

C'est un dépôt privé, demander l'accès.

Ajouter les dépôts privés à `ca.ntro.cards`

```bash
$ cd ca.ntro.cards
$ sh scripts/init_private_repos.sh
```

Le projet qui correspond à la démo est

* `solutions/intro_solution`




