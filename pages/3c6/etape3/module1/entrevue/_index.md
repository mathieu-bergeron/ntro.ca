---
title: "Entrevue 3.1: paramètres de type"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier ou dans un éditeur de texte

<br>

1. Considérer les classes suivantes

    ```java
    public class Animal {
    }

    public class Loup extends Animal {
    }

    public class Bovin extends Animal {
    }

    public class Groupe {

        private List<Object> membres;

        public List<Object> getMembres(){
            return membres;
        }
    }

    public class Meute extends Groupe {

        // getMembres doit retourner une List<Loup>

    }

    public class Troupeau extends Groupe {

        // getMembres doit retourner une List<Bovin>

    }
    ```

1. Modifier ces classes pour que:

    * `Groupe` mémorise une liste d'animaux
    * `Meute` mémorise une liste de loups
    * `Troupeau` mémorise une liste de bovins

1. Maintenant, considérer les classes suivantes

    ```java
    public class Vehicule {
    }

    public class Velo extends Vehicule {
    }

    public class Camion extends Vehicule {
    }

    public class Groupe {

        private List<Object> membres;

        public List<Object> getMembres(){
            return membres;
        }

    }

    public class Peloton extends Groupe {

        // getMembres doit retourner une List<Velo>

    }

    public class Flotte extends Groupe {

        // getMembres doit retourner une List<Camion>

    }
    ```

1. Modifier ces classes pour que:

    * `Groupe` mémorise une liste de véhicules
    * `Peloton` mémorise une liste de vélos
    * `Flotte` mémorise une liste de camions
    

