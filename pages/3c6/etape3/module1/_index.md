---
title: "Module 3.1: structure générique"
weight: 30
---

{{% pageTitle %}}

1. Théorie: 
    * {{% link "/3c6/etape3/module1/theorie/structures_generiques01" "implanter des structures génériques (1)" %}}
    * {{% link "/3c6/etape3/module1/theorie/structures_generiques02" "implanter des structures génériques (2)" %}}

1. {{% link "/3c6/etape3/module1/entrevue" "Entrevue 3.1" %}}

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=330294" target="_blank">Mini-test théorique</a>

1. Atelier
    * exercice A: {{% link "/3c6/etape3/module1/atelier/comparable" "liste de Comparable" %}}
    * exercice B: {{% link "/3c6/etape3/module1/atelier/generique" "liste générique (avec paramètre de type)" %}}

1. Remises 
    * {{% link "/3c6/etape3/module1/atelier/remise_gitlab" "sur GitLab" %}}
    * {{% link "/3c6/etape3/module1/atelier/remise_moodle" "sur Moodle" %}}

<br>
<br>


{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}
