public class MonAtelier3_1_B extends Atelier3_1_B {
    
    public static void main(String[] args) {
        
        new MonAtelier3_1_B().valider();
    }

    @Override
    public Object creerListeEntiers(Integer[] entiersInitiaux) {
        return new MaListe<Integer>(entiersInitiaux);
    }

    @Override
    public Object creerListeChaines(String[] chainesInitiales) {
        return new MaListe<String>(chainesInitiales);
    }

    @Override
    public Object creerListeVehicules(Vehicule[] vehiculesInitiaux) {
        return new MaListe<Vehicule>(vehiculesInitiaux);
    }

    @Override
    public Planteur fournirPlanteur() {
        return new MonPlanteur();
    }

}
