public class MonAtelier3_1_A extends Tutoriel3_1 {
    
    public static void main(String[] args) {
        
        new MonAtelier3_1_A().valider();
    }

    @Override
    public Object creerListeEntiers(Integer[] entiersInitiaux) {
        return new MaListe(entiersInitiaux);
    }

    @Override
    public Object creerListeChaines(String[] chainesInitiales) {
        return new MaListe(chainesInitiales);
    }

    @Override
    public Object creerListeVehicules(Vehicule[] vehiculesInitiaux) {
        return new MaListe(vehiculesInitiaux);
    }

    @Override
    public Planteur fournirPlanteur() {
        return new MonPlanteur();
    }
}
