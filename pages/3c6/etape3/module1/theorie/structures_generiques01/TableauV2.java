public interface Liste {

    Comparable obtenirValeur(int index);

    void modifierValeur(int index, Comparable nouvelleValeur);

    Comparable valeurMinimale();

}
