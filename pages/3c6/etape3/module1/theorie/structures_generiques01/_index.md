---
title: ""
weight: 1
bookHidden: true
---


# Théorie 3.1: structure générique (1)

<center>
<video width="50%" src="01.mp4" type="video/mp4" controls playsinline>
</center>

* Comment définir une liste générique contenant des méthodes comme
    * `valeurMinimale` (le plus petit élément de la liste)
    * `trierListe`

* Comment s'assurer que le même code fonctionne pour:
    * liste d'entiers, liste de chaînes, liste de `Vehicule`, etc.

* On regarde deux techniques ici:
    * mémoriser les éléments de la liste dans un tableau d'objets `Object[] valeurs;`
    * mémoriser les éléments de la liste dans un tableau d'objets comparables `Comparable[] valeurs;`

## Première version: `Object[] valeurs`

<center>
<video width="50%" src="02.mp4" type="video/mp4" controls playsinline>
</center>

* En java, une classe hérite toujours de `Object`

* Un tableau `Object[] valeurs` peut alors stoquer n'importe quel sorte d'objet

* Voici l'interface de la première version de notre `Liste` générique:

```java
{{% embed "TableauV1.java" %}}
```

* La `Liste` peut faire trois choses:
    * `obtenirValeur`: récupérer la valeur stoquée à un certain `index`
    * `modifierValeur`: stoquer une valeur à un certain `index`
    * `valeurMinimale`: trouver et retourner la plus petite valeur de la liste


## Implanter `Object[] valeurs`

<center>
<video width="50%" src="03.mp4" type="video/mp4" controls playsinline>
</center>

* L'implantation commence comme suit:

```java
{{% embed src="MonTableauV1.java" first-line="1" last-line="8" %}}
```

* Le constructeur permet de spécifier les valeurs initiales de la liste (le tableau d'objets `Object[] valeurs`)
    * ces valeurs sont mémorisées dans un attribut privé

* La liste est générique. On peut créer plusieurs types de liste:


```java
{{% embed src="UtiliserV1.java" first-line="5" last-line="13" %}}
```

* L'utilisation de la liste est simple:

    ```java
    {{% embed src="UtiliserV1.java" first-line="15" last-line="22" indent-level="1" %}}
    ```

    * NOTE: contrairement à un tableau, on en peut pas utiliser 
        * `liste[index]` pour accéder à un élément de la liste ou 
        * `liste[index] = 3` pour modifier un élément.
        * il faut toujours appeler des méthodes, comme 
            * `liste.obtenirValeur(index)` ou 
            * `liste.modifierValeur(index, 3)`

<br>

* L'implantation de `modifierValeur` et `obtenirValeur` est simple:

    ```java
    {{% embed src="MonTableauV1.java" first-line="11" last-line="19" indent-level="1" %}}
    ```

    * NOTE: ça va se compliquer quand on va vouloir modifier la taille de la liste en ajoutant des éléments (c'est le sujet de l'étape 4)

<br>



* L'implantation de `valeurMinimale` est simple aussi... à première vue:

```java
{{% embed src="MonTableauV1.java" first-line="21" last-line="38" %}}
```

* Le problème est dans l'implantation de `siValeurPlusPetite`

## Problème avec `Object[] valeurs`

<center>
<video width="50%" src="04.mp4" type="video/mp4" controls playsinline>
</center>

* Comparer les valeurs est un problème:

```java
{{% embed src="MonTableauV1.java" first-line="40" last-line="62" %}}
```

* Il faudrait ajouter du code à la méthode ci-haut pour chaque type d'objet

* Notre classe `Liste` n'est plus vraiment générique

* La solution est de déléguer aux objets la tâche de se comparer

* On a besoin pour ça d'un contrat avec ces objets


## Deuxième version: `Comparable[] valeurs`

<center>
<video width="50%" src="05.mp4" type="video/mp4" controls playsinline>
</center>

* L'interface `Comparable` est un contrat qui signifie qu'un objet sait se comparer (à un autre)

* Pour remplir ce contrat, il faut implanter la méthode `int compareTo(Object autre)`
    * retourner `-1` si l'objet courant est plus petit que l'`autre` objet
    * retourner `0` si l'objet courant est égal à l'`autre` objet
    * retourner `+1` si l'objet courant plus grand que l'`autre` objet

* Avec `Comparable`, l'interface de notre liste générique devient:

```java
{{% embed src="TableauV2.java"  %}}
```

* En VSCode, vous allez avoir des avertissements de *type incomplet*
* On peut les ignorer pour l'instant

    <center>
    <video width="50%" src="avertissements.mp4" type="video/mp4" controls playsinline>
    </center>

* L'implantation commence comme suit:

```java
{{% embed src="MonTableauV2.java" first-line="1" last-line="8" %}}
```

* Cette fois-ci, on mémorise un tableau de `Comparable`
    * on ne connaît rien des objets, sauf qu'ils implante `compareTo(Object autre)`

* Les méthodes `obtenirValeur`, `modifierValeur` et `valeurMinimale` sont pareilles

* La méthode `siValeurPlusPetite` n'est plus du tout problématique

```java
{{% embed src="MonTableauV2.java" first-line="40" last-line="42" %}}
```

* Par contre, il faut maintenant que `Vehicule` implante `Comparable`

```java
{{% embed src="Vehicule.java" %}}
```

* NOTE: les classes `String` et `Integer` (et d'autres) implantent déjà `Comparable`



## Recapitulation

<center>
<video width="50%" src="06.mp4" type="video/mp4" controls playsinline>
</center>

* Pour créer une liste générique, il faut mémoriser des objets qu'on ne connaît pas

* Pour comparer ces objets, il faut exiger qu'ils implantent l'interface `Comparable`

* Pour en faire plus, il faut exiger une interface avec plus de méthodes

* Par exemple, si on veut que `Liste` affiche les valeurs, on peut demander:
    * que chaque objet implante `ElementListe` qui contient `String formater()`
    * alors on a:

```java
{{% embed src="MonTableau_formater.java" first-line="1" last-line="26" %}}
```

## Problèmes avec `Comparable[] valeurs`

<center>
<video width="50%" src="07.mp4" type="video/mp4" controls playsinline>
</center>

* Notre `Liste` ne permet pas vraiment de spécifier le type des éléments

* En particulier, on peut faire:

```java
{{% embed src="Utiliser_erreurs.java" first-line="5" last-line="15" %}}
```

* Est-ce qu'on peut ajouter un entier ou une `Auto` à une liste de chaînes?

* Comment bloquer cette option pour éviter les erreurs d'exécution?

* Réponse ici: {{% link "/3c6/etape3/module1/theorie/structures_generiques02/" "structures génériques (2)" %}}
