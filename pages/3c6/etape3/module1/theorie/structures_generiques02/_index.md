---
title: ""
weight: 1
bookHidden: true
---


# Théorie 3.2: structure générique (2)

<center>
<video width="50%" src="01.mp4" type="video/mp4" controls playsinline>
</center>

* Comment restreindre une liste générique à un type précis?
    * liste de chaînes, liste d'entiers, liste de `Vehicule`

* Comment prévenir une erreur d'exécution de ce genre:

```java
{{% embed src="Erreur.java" %}}
```

* La technique à utiliser s'appelle les *paramètres de type*

## Utiliser les paramètres de type

<center>
<video width="50%" src="02.mp4" type="video/mp4" controls playsinline>
</center>

* Un paramètre de type permet de spécifier quel genre de `Liste` on veut:

```java
{{% embed src="Utiliser.java" first-line="1" last-line="5" %}}
```

* `<Integer>` est le paramètre de type restreignant la liste aux entiers

* `<String>` est le paramètre de type restreignant la liste aux chaînes

* `<Vehicule>` est le paramètre de type restreignant la liste aux `Vehicule`

* Le paramètre vide `<>` est rempli (deviné) par le compilateur, lorsque possible
    * p.ex. la première ligne équivaut à:

```java
{{% embed src="Utiliser.java" first-line="7" last-line="8" %}}
```

## Erreurs de compilation

<center>
<video width="50%" src="03.mp4" type="video/mp4" controls playsinline>
</center>

* Grace aux paramètres de type, le compilateur peut faire plus de vérifications

* Certaines erreur d'exécution deviennent des erreurs de compilation:

```java
{{% embed src="Utiliser.java" first-line="10" last-line="13" %}}
```

## L'interface `Liste` avec paramètres de type

<center>
<video width="50%" src="04.mp4" type="video/mp4" controls playsinline>
</center>

```java
{{% embed src="Tableau.java" %}}
```

* Le paramètre `<C extends Comparable>` se lit:
    * le type `C` est une sous-classe de `Comparable`

* Le type `C` est ensuite utilisé où `Comparable` serait utilisé


## Implanter `Liste` avec paramètres de type

<center>
<video width="50%" src="05.mp4" type="video/mp4" controls playsinline>
</center>


* L'implantation commence comme suit:

```java
{{% embed src="MonTableau.java" first-line="1" last-line="8" %}}
```

* Il y a deux paramètres de type:
    * `<C extends Comparable>` indique: le type `C` est une sous-classe de `Comparable`
    * `<C>` spécifie que l'interface `Liste` est utilisée avec le type `C`

* L'implantation des méthodes utilise le type `C`:

```java
{{% embed src="MonTableau.java" first-line="10" last-line="35" %}}
```


## Il reste une erreur d'exécution!

<center>
<video width="50%" src="06.mp4" type="video/mp4" controls playsinline>
</center>

* Le code ci-bas va compiler, mais provoquer une erreur d'exécution:

```java
{{% embed src="Erreur2.java" %}}
```

* Pour régler l'erreur il faut spécifier cette notion:
    * un `Vehicule` peut seulement être comparé à un autre `Vehicule`

* Pour ce faire, il faut paramétrer `Comparable` quand on définit `Vehicule`

```java
{{% embed src="Vehicule.java" %}}
```

* De la même façon, il faut paramétrer `Comparable` quand on définit `Liste`

```java
{{% embed src="Tableau2.java" %}}
```

* Le paramètre de type `<C extends Comparable<C>>` se lit:
    * `C` est un sous-type de `Comparable` qui peut seulement se comparer à `C`

* Les avertissements de *type incomplet* devraient ainsi disparaîtrent
