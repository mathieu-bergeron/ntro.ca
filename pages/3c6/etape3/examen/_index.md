---
title: "Examen 3"
weight: 40
draft: false
---

# 3C6: Examen 3

* Durée: 2h

* Individuel

* Documentation permise: ntro.ca, Moodle, recherches sur le Web, **votre code**, des cartes à jouer

* L'examen est 100% à choix de réponse sur Moodle

## Sujets

* comprendre les paramètres de type (opérateur `< >`)

* reconnaître les graphes d'efficacité

* analyser l'efficacité avec la notation `O( )`

## 5pts) Volet théorique

* 5 questions de compréhension à choix de réponse

## 5pts) Volet pratique

* 4 questions du type «&nbsp;compléter le code suivant&nbsp;» 
