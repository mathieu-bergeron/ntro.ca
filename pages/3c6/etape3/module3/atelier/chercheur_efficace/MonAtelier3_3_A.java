public class MonAtelier3_3_A extends Atelier3_3_A {
    
    public static void main(String[] args) {
        
        new MonAtelier3_3_A().valider();
    }

    @Override
    public <C extends Comparable<C>> Chercheur<C> fournirChercheurNaif() {
        return new ChercheurNaif<>();
    }

    @Override
    public <C extends Comparable<C>> Chercheur<C> fournirChercheurEfficace() {
        return new ChercheurEfficace<>();
    }
}
