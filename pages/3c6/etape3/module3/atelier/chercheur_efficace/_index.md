---
title: "Atelier 3.3.A: recherche dans un tableau **trié**"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Travail à réaliser

* En VSCode, sélectionner le **paquet** `atelier3_3_A`

* Créer la classe `MonAtelier3_3_A` qui hérite de la classe `Atelier3_3_A`

* Créer la classe `ChercheurNaif` qui implante l'interface `Chercheur`

* Implanter la méthode pour trouver une valeur dans le tableau 
    * (ou copier le code de `ChercheurA`, `atelier3_2_A`)

```java
{{% embed src="./ChercheurNaif.java"  %}}
```

* Créer la classe `ChercheurEfficace` qui implante l'interface `Chercheur`

* Implanter une recherche binaire dans une liste triée

```java
{{% embed src="./ChercheurEfficace.java"  %}}
```

* Ajouter une méthode `main` à la classe `MonAtelier3_3_A`:

```java
{{% embed src="./MonAtelier3_3_A.java" first-line="3" last-line="6" %}}
```

* Implanter les méthodes pour remplir le contrat du `Atelier3_3_A`, p.ex:

```java
{{% embed src="./MonAtelier3_3_A.java" first-line="8" last-line="26" %}}
```

## Validation en deux étapes

1. Exécuter mon projet et valider mes classes et mes méthodes

    <img class="figure" src="validation.png" />

2. Fermer la fenêtre afin d'exécuter les tests de performance

    <img class="figure" src="performance.png" />

