public class TrieurJdk<C extends Comparable<C>> implements Trieur<C> {

    @Override
    public Liste<C> trier(Liste<C> entree) {
        Arrays.sort(entree.valeurs());
        return entree;
    }
}
