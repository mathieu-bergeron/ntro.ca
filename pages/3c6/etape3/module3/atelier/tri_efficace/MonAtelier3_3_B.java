public class MonAtelier3_3_B extends Atelier3_3_B {
    
    public static void main(String[] args) {
        
        (new MonAtelier3_3_B()).valider();
    }

    @Override
    public <C extends Comparable<C>> Trieur<C> fournirTrieurNaif() {
        return new TrieurNaif<C>();
    }

    @Override
    public <C extends Comparable<C>> Trieur<C> fournirTrieurJdk() {
        return new TrieurJdk<C>();
    }

    @Override
    public <C extends Comparable<C>> Trieur<C> fournirTrieurEfficace() {
        return new TrieurEfficace<C>();
    }
}
