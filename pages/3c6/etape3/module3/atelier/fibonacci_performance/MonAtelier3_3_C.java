public class MonAtelier3_3_C extends Atelier3_3_C {
    
    public static void main(String[] args) {
        
        (new MonAtelier3_3_C()).valider();
    }

    @Override
    public CalculateurFibonacci fournirFibonacciGraphe() {
        return new MonFibonacciGraphe();
    }

    @Override
    public CalculateurFibonacci fournirFibonacciTableau() {
        return new MonFibonacciTableau();
    }

    @Override
    public CalculateurFibonacci fournirFibonacciVariables() {
        return new MonFibonacciVariables();
    }

}
