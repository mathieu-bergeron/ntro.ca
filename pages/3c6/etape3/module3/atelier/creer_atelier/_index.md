---
title: "Atelier3.3: créer les projets Eclipse"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

1. Télécharger {{% download "atelier3_3.zip" "atelier3_3.zip" %}}

1. Copier le fichier `atelier3_3.zip` **à la racine** de mon dépôt Git

1. Extraire les fichiers **directement** à la racine du dépôt Git

    * Clic-droit sur le fichier => *Extraire tout*

    * **Effacer `atelier3_3` du chemin proposé**

    * Cliquer sur *Extraire*


1. Ouvrir *Git Bash* **à la racine** de mon dépôt Git

    * *Windows 10* : Clic-droit => *Git Bash Here*
    * *Windows 11* : Clic-droit => *Show more options* => *Git Bash Here*

1. En *Git Bash*, exécuter le script `creer_projets_eclipse.sh`

    ```bash
    $ sh scripts/creer_projets_eclipse.sh
    ```

    * au besoin fermer Eclipse avant d'exécuter le script

    * appuyer sur {{% key "Entrée" %}} dans fenêtre *Git Bash* pour vraiment lancer le script

1. Attendre que le script termine

1. Ouvrir Eclipse et importer les projets `atelier3_3_*`

    * *File* => *Import* => *Existing Projects into Workspace*

    * Cliquer sur *Browse* et naviguer jusqu'à la racine de mon dépôt Git

    * Cliquer sur *Sélectionner un dossier*

    * Vérifier que les projets `atelier3_3_*` apparaissent dans la case *Projects*

    * Cliquer sur *Finish*
