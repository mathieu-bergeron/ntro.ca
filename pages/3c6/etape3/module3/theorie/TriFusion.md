---
title: ""
weight: 1
bookHidden: true
---


`Liste trier(Liste entree)`:

* **CRÉER** une liste `resultat` vide
* **SI** `entree` contient un seul élément ou moins:
    * **copier** `entree` dans `resultat`
* **SINON**:
    * **diviser** la liste `entree` en deux sous-listes égales
    * **appeler** `trier()` pour trier chaque sous-liste
    * **appeler** `fusionner()` pour fusionner les deux sous-listes
    * **copier** la liste fusionné dans `resultat`
* **retourner** la liste `resultat`
