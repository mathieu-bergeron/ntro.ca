---
title: ""
weight: 1
bookHidden: true
---


`Liste<C> trier(Liste<C> entree)`:

* **CRÉER** une liste `resultat` vide
* **TANT QUE** la liste `entree` n'est pas vide
    * **appeler** `valeurMinimale` et **mémoriser** la valeur de retour
    * **retirer** cette valeur de la liste `entree`
    * **ajouter** cette valeur à la fin de la liste `resultat`
* **retourner** la liste `resultat`
