---
title: ""
weight: 1
bookHidden: true
---


`Valeur obtenirValeurMinimale(liste)`:

* **considérer** que le premier élément de la `liste` est la valeur minimale
* **POUR TOUS** les éléments `el` de la `liste`:
    * **SI** `el` est plus petit que la valeur minimale courante:
        * **mémoriser** `el` comme la nouvelle valeur minimale courante
* **retourner** la valeur minimale

