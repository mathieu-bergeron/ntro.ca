---
title: "Théorie 3.3, ajout: pourquoi Fibonacci naïf est exponentiel"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Fibonacci efficace

```java
public MonFibonacci calculerFib(int n){

    MonFibonacci resultat = new MonFibonacci();

    if(n == 0){

        resultat.setReponse(0L);

    }else if(n == 1){

        MonFibonacci moinsUn = calculerFib(0);

        resultat.setReponse(1L);
        resultat.setMoinsUn(moinsUn);

    }else{

        MonFibonacci moinsUn = calculerFib(n-1);

        MonFibonacci moinsDeux = moinsUn.getMoinsUn(); // la clé est ici

        resultat.setReponse(moinsUn.getReponse() + moinsDeux.getReponse());

        resultat.setMoinsUn(moinsUn);
        resultat.setMoinsDeux(moinsDeux);

    }

    return resultat;
}
```

Ça nous donnes des graphes comme:

* `calculerFib(0)`

    <img src="Fib_0.png" />

* `calculerFib(1)`

    <img src="Fib_1.png" />

* `calculerFib(2)`


    <img src="Fib_2.png" />

* `calculerFib(3)`

    <img src="Fib_3.png" />



## Fibonacci naïf

```java
public long fib(int n){
    long reponse;

    if(n == 0){

        reponse = 0L;

    }else if(n == 1){

        reponse = 1L;

    }else{

        reponse = fib(n-1) + fib(n-2);

    }

    return reponse;
}
```

Ou encore:

```java
public MonFibonacci calculerFib(int n){

    MonFibonacci resultat = new MonFibonacci();

    if(n == 0){

        resultat.setReponse(0L);

    }else if(n == 1){

        MonFibonacci moinsUn = calculerFib(0);

        resultat.setReponse(1L);
        resultat.setMoinsUn(moinsUn);

    }else{

        MonFibonacci moinsUn = calculerFib(n-1);

        MonFibonacci moinsDeux = calculerFib(n-2); // 2ième appel récursif!

        resultat.setReponse(moinsUn.getReponse() + moinsDeux.getReponse());

        resultat.setMoinsUn(moinsUn);
        resultat.setMoinsDeux(moinsDeux);

    }

    return resultat;
}
```

Dans les deux cas, le problème est qu'on fait deux appels récursifs à chaque fois. 

* `n=2`: `2` appels récursifs
* `n=3`: `4` appels récursifs
* `n=4`: `8` appels récursifs
* `n=5`: `16` appels récursifs
* `n=6`: `32` appels récursifs
* etc.

    

Ce qui nous donnerait plutôt des graphes comme:

* `calculerFib(0)`

    <img width="400px" src="Fib_exp0.png" />

* `calculerFib(1)`

    <img width="400px" src="Fib_exp1.png" />

* `calculerFib(2)`


    <img width="400px" src="Fib_exp2.png" />

* `calculerFib(3)`

    <img width="400px" src="Fib_exp3.png" />

* `calculerFib(4)`

    <img width="400px" src="Fib_exp4.png" />

* `calculerFib(5)`

    <img width="400px" src="Fib_exp5.png" />

* `calculerFib(6)`

    <img width="400px" src="Fib_exp6.png" />

* ...

* `calculerFib(10)`

    <img width="400px" src="Fib_exp10.png" />


