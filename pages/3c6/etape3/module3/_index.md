---
title: "Module 3.3: efficacité (2)"
weight: 30
draft: false
---

{{% pageTitle %}}

1. Théorie
    * {{% link "/3c6/etape3/module3/theorie/" "efficacité (2)" %}}
    * {{% link "/3c6/etape3/module3/theorie/fibonacci" "pourquoi Fibonacci naïf est exponentiel?" %}}

1. {{% link "/3c6/etape3/module3/entrevue/" "Entrevue 3.3" %}}

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=331198" target="_blank">Mini-test théorique</a>

1. Atelier
    * exercice A: {{% link "/3c6/etape3/module3/atelier/chercheur_efficace" "Recherche efficace dans une liste **triée**" %}}
    * exercice B: {{% link "/3c6/etape3/module3/atelier/tri_efficace" "Tri efficace d'une liste" %}}
    * exercice C (BONUS): {{% link "/3c6/etape3/module3/atelier/fibonacci_performance" "Trois performances pour Fibonacci" %}}

1. Remises 
    * {{% link "/3c6/etape3/module3/atelier/remise_gitlab" "sur GitLab" %}}
    * {{% link "/3c6/etape3/module3/atelier/remise_moodle" "sur Moodle" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}
