---
title: ""
weight: 1
bookHidden: true
---


`Liste fusionner(Liste listeGauche, Liste listeDroite)`:

* **CRÉER** une liste `resultat` vide
* **INITIALISER** les indices `i` et `j` à 0 
* **TANT QUE** i est < à la taille de `listeGauche` ET j est < à la taille de `listeDroite`:
    * **SI** l'élément à l'index `i` de `listeGauche` est <= à l'élément à l'index `j` de `listeDroite`: 
        * **ajouter** l'élément à l'index `i` de `listeGauche` dans `resultat`
	* **incrementer** `i`
    * **SINON:**
        * **ajouter** l'élément à l'index `j` de `listeDroite` dans `resultat`	
        * **incrementer** `j`	
* **TANT QUE** `i` est inférieur à la taille de `listeGauche`: 
    * **ajouter** l'élément à l'index `i` de `listeGauche` dans `resultat`
    * **incrementer** `i`
* **TANT QUE** `j` est inférieur à la taille de `listeDroite`:  
    * **ajouter** l'élément à l'index `j` de `listeDroite` dans `resultat`	
    * **incrementer** `j`	
* **retourner** la liste `resultat`
