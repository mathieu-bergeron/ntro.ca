---
title: ""
weight: 1
bookHidden: true
---


`int afficherElements(liste)`:

* **POUR TOUS** les éléments `el` de la `liste`:
    * **afficher** cet élément
    * (appel à `afficherElement`)

<br>

`int afficherElement(element)`:

* **POUR CHAQUE** attribut `attr` de l'`element`:
    * **afficher** la valeur de cet `attr`
