---
title: "Entrevue 3.3: comprendre l'efficacité"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier 


## Analyse 1

* Avec la notation `O()`, indiquer l'efficacité du pseudo-code ci-bas:
    * `n` fait référence à la taille de la liste

<span class="pseudo">
{{% embed "AfficherVoisinage.md" %}}
</span>

* Expliquer votre raisonnement


* NOTE: les choix plausibles sont
    * `O(n)`: linéaire
    * <code>O(n<sup>2</sup>)</code>: quadratique
    * <code>O(2<sup>n</sup>)</code>: exponentiel

## Analyse 2

* Avec la notation `O()`, indiquer l'efficacité du pseudo-code ci-bas:
    * `n` fait référence à la taille de la liste
    * un `element` a au plus `3` attributs

<span class="pseudo">
{{% embed "AfficherElements.md" %}}
</span>

* Expliquer votre raisonnement


* NOTE: les choix plausibles sont
    * `O(n)`: linéaire
    * <code>O(n<sup>2</sup>)</code>: quadratique
    * <code>O(2<sup>n</sup>)</code>: exponentiel

## Analyse 3

* Avec la notation `O()`, indiquer l'efficacité du pseudo-code de l'algorithme qui fusionne 2 listes ci-bas:
    * `n` fait référence à la taille de la liste de gauche + la liste de droite 

<span class="pseudo">
{{% embed "Fusion.md" %}}
</span>

* Expliquer votre raisonnement

* NOTE: les choix plausibles sont
    * `O(n)`: linéaire
    * <code>O(n<sup>2</sup>)</code>: quadratique
    * <code>O(2<sup>n</sup>)</code>: exponentiel