---
title: ""
weight: 1
bookHidden: true
---


`int afficherVoisinage(liste)`:

* **POUR TOUS** les éléments `el` de la `liste`:
    * **afficher** cet élément, ainsi que ses voisins
    * (appel à `afficherElement`)

<br>

`int afficherElement(liste, element)`:

* afficher l'`element`
* **POUR TOUS** les éléments `el` de la `liste`:
    * **SI** `el` est un voisin de `element`
        * afficher aussi `el`
