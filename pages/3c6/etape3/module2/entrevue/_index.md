---
title: "Entrevue 3.2: comprendre l'efficacité"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier (ou avec votre outil préféré pour dessiner des graphes)

1. **Sans regarder la théorie**:

    * dessiner un graphe pour illustrer la différence entre
        * `O(n)`: linéaire
        * <code>O(n<sup>2</sup>)</code>: quadratique
        * <code>O(2<sup>n</sup>)</code>: exponentiel

    * en `X` (horizontal): taille des données en entrée, p.ex. de `1` à `10`

    * en `Y` (vertical): nombre d'étapes dans le calcul, p.ex. de `1` à `1024`

    * (note: bien illustrer la *tendance* est plus important que l'exactitude)


1. Considérer la version naïve de `Fibonacci`

    ```java
    public long fib(int n){
        if(n == 0){

            return 0;

        }else if(n == 1){

            return 1;

        }else{

            return fib(n-1) + fib(n-2);
        }
    }
    ```

    * Selon vous, quelle est l'efficacité de cette procédure?


    * Autrement dit, quel est la suite de ce graphique?

        <img src="fibonacci_naif.png"/>


    * Réponse via `l'atelier3_2_C`!










    

