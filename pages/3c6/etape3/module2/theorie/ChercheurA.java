public int trouverIndicePourValeur(Liste liste, Comparable valeur) {
    int indice = -1;

    for(int i = 0; i < liste.longueur(); i++) {
        if(liste.obtenirValeur(i).compareTo(valeur) == 0) {
            indice = i;
        }
    }

    return indice;
}
