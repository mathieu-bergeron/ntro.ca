@Override
public int trouverIndicePourValeur(Liste<C> liste, C valeur) {
    int indice = liste.longueur();

    while (liste.obtenirValeur(--indice).compareTo(valeur) != 0 && indice != 0); 

    return indice;
}
