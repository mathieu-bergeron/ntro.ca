@Override
public int trouverIndicePourValeur(Liste<C> liste, C valeur) {
    int indice = liste.longueur() - 1;

    while (indice >= 0) {
        if(liste.obtenirValeur(indice).compareTo(valeur) == 0) {

            break;

        } else {

            indice--;

        }
    }

    return indice;
}
