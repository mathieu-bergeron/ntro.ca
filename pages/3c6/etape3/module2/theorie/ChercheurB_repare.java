public int trouverIndicePourValeur(Liste li, C v) {
    int i,l;

    for(i=0,l=l(li);--l>0;i=eq(v,v(li,i))?i:++i);

    return i;
}
