public int trouverIndicePourValeur(Liste li, Comparable v) {
    int i,l;

    for(i=0,l=l(li);l>0;i=eq(v,v(li,i%l(li)))?(--l>0?i++:i):++i);

    return i;
}
