---
title: "Module 3.2: efficacité (1)"
weight: 30
draft: false
---

{{% pageTitle %}}

1. {{% link "/3c6/etape3/module2/theorie/" "Théorie" %}}

1. {{% link "/3c6/etape3/module2/entrevue/" "Entrevue 3.2" %}}


1. Mini-test théorique

   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=331103" target="_blank">Mathieu Bergeron</a>
   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=410384" target="_blank">Marc-Olivier Tremblay</a>

1. Atelier
    * exercice A: {{% link "/3c6/etape3/module2/atelier/chercheur" "Recherche dans un tableau" %}}
    * exercice B: {{% link "/3c6/etape3/module2/atelier/tri_naif" "Tri naïf d'un tableau" %}}
    * exercice C: {{% link "/3c6/etape3/module2/atelier/fibonacci_naif" "Fibonacci naïf Vs efficace" %}}

1. Remises 
    * {{% link "/3c6/etape3/module2/atelier/remise_gitlab" "sur GitLab" %}}
    * {{% link "/3c6/etape3/module2/atelier/remise_moodle" "sur Moodle" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}
