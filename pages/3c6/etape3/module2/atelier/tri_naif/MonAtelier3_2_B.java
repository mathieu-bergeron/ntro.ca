public class MonAtelier3_2_B extends Atelier3_2_B {
    
    public static void main(String[] args) {
        
        (new MonAtelier3_2_B()).valider();
    }

    @Override
    public <C extends Comparable<C>> Trieur<C> fournirTrieurNaif() {
        return new TrieurNaif<C>();
    }

    @Override
    public <C extends Comparable<C>> Trieur<C> fournirTrieurJdk() {
        return new TrieurJdk<C>();
    }
}
