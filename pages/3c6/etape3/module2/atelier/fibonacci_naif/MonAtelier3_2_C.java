public class MonAtelier3_2_C extends Atelier3_2_C {
    
    public static void main(String[] args) {
        
        (new MonAtelier3_2_C()).valider();
    }

    @Override
    public CalculateurFibonacci fournirFibonacciNaif() {
        return new MonFibonacciNaif();
    }

    @Override
    public CalculateurFibonacci fournirFibonacciEfficace() {
        return new MonFibonacciGraphe();
    }
}
