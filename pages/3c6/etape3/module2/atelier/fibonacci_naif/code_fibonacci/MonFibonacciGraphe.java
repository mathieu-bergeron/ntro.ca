package atelier3_2_C;

public class MonFibonacciGraphe extends Calculateur implements CalculateurFibonacci {

    @Override
    public void construireGraphe() {
        if(n >= 0) {
            tete = new MonFibonacci();
            tete.setN(0);
            tete.calculerReponseEtNombreOr();
        }

        if(n >= 1) {

            MonFibonacci nouvelleTete = new MonFibonacci();
            nouvelleTete.setN(1);
            nouvelleTete.setMoinsUn(tete);
            tete = nouvelleTete;

            tete.calculerReponseEtNombreOr();
        }

        for(int i = 2; i <= n; i++) {
            MonFibonacci nouvelleTete = new MonFibonacci();
            nouvelleTete.setN(i);
            nouvelleTete.setMoinsUn(tete);
            nouvelleTete.setMoinsDeux(tete.getMoinsUn());
            tete = nouvelleTete;
            
            tete.calculerReponseEtNombreOr();
        }
    }
}
