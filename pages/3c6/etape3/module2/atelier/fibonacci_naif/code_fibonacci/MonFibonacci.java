package atelier3_2_C;

public class MonFibonacci extends Fibonacci {

    @Override
    public void calculerReponseEtNombreOr() {
        if(n == 0) {

            setReponse(0l);
            setNombreOr(0);

        }else if(n == 1) {
            
            setReponse(1l);
            setNombreOr(0);
            
        }else {
            
            setReponse(getMoinsUn().getReponse() + getMoinsDeux().getReponse());
            setNombreOr(Double.valueOf(getReponse()) / Double.valueOf(getMoinsUn().getReponse()));

        }
    }

    @Override
    public void construireGrapheRecursivement() {
    }
}
