public class MonAtelier3_2_A extends Atelier3_2_A {
    
    public static void main(String[] args) {
        
        new MonAtelier3_2_A().valider();
    }

    @Override
    public <C extends Comparable<C>> Chercheur<C> fournirChercheurA() {
        return new ChercheurA<C>();
    }

    @Override
    public <C extends Comparable<C>> Chercheur<C> fournirChercheurB() {
        return new ChercheurB<C>();
    }
}
