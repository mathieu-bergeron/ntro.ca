public class ChercheurB<C extends Comparable<C>> implements Chercheur<C> {

    @Override
    public int trouverIndicePourValeur(Liste<C> li, C v) {
        int i,l;

        for(i=0,l=l(li);l>0;i=eq(v,v(li,i%l(li)))?(--l>0?i++:i):++i);

        return i;
    }
    
    private boolean eq(C v1, C v2) {
        return v1.compareTo(v2) == 0;
    }

    private C v(Liste<C> li, int i) {
        return li.obtenirValeur(i);
    }

    private int l(Liste<C> li) {
        return li.longueur();
    }
}
