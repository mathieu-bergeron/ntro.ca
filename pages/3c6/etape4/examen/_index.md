---
title: "Examen 4"
weight: 40
draft: false
---

# 3C6: Examen 4

* **SVP apporter des écouteurs**, il y a une vidéo pour présenter l'examen

* Durée: 2h

* Individuel

* Documentation permise: ntro.ca, Moodle, recherches sur le Web, **votre code**, des cartes à jouer

* L'examen est 100% à choix de réponse sur Moodle

## Sujets

* comprendre les différentes implantations d'une liste

* efficacité d'une liste selon l'opération à effectuer

## 5pts) Volet théorique

* 3 questions de compréhension 
* 4 questions courtes sur les types de liste

## 5pts) Volet pratique

* 4 questions du type «&nbsp;compléter le code suivant&nbsp;» 
