---
title: "Théorie 5: les listes du SDK"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Implantations de `List<E>`

<center>
<video width="50%" src="03.mp4" type="video/mp4" controls playsinline>
</center>


* `List<E>` est une interface

* Le SDK Java fournit différentes implantations, p.ex:
    * `ArrayList<E>`
    * `LinkedList<E>`

* Pour l'instant, on va utiliser `ArrayList<E>`

* Quelle est la différence?
    * à utiliser: aucune (c'est la même interface)
    * en terme d'efficacité? à coder?
        * on va en parler à l'étape 4

### Comparaison des implantations

* Chaque implantation a ses avantages et ses inconvénients

* Pour ajouter des éléments **à la fin**:
    * `LinkedList` un peu plus efficace

    <center>
        <img src="list_ajouts.png" width="90%"/>
    </center>

* Pour insérer des éléments **au début**: 
    * `ArrayList` plus effiace

    <center>
        <img src="list_ajouts_debut.png" width="90%"/>
    </center>

* Pour insérer des éléments un peu partout:
    * `ArrayList` beaucoup plus efficace

    <center>
        <img src="list_modifs.png" width="90%"/>
    </center>

* Pour retirer des éléments **au début**: 
    * `LinkedList` beaucoup plus efficace

    <center>
        <img src="list_retraits.png" width="90%"/>
    </center>

* Pour modifier des éléments un peu partout:
    * `ArrayList` beaucoup plus efficace

    <center>
        <img src="list_insertions.png" width="90%"/>
    </center>

## `List<E>`: quand utiliser quelle implantation?

* Par défaut, on recommande `ArrayList`

* `LinkedList` est plus spécialisée

* Par exemple, on devrait utiliser `LinkedList` pour une file d'attente:
    * on ajoute toujours à la fin
    * on retire toujours au début

* NOTE: le Java SDK fournit aussi la structure `Deque<E>` spécifiquement pour les files:
    * <a href="https://docs.oracle.com/javase/7/docs/api/java/util/Deque.html" target="_blank">https://docs.oracle.com/javase/7/docs/api/java/util/Deque.html</a>

* Une `Deque<E>` est aussi efficace pour une *pile*:
    * on insère toujours au début
    * on retire toujours au début

## `Map<K,V>`: mappage générique

<center>
<video width="50%" src="04.mp4" type="video/mp4" controls playsinline>
</center>

* Les clés d'un mappage peuvent être de n'importe quel type
    * contrairement à notre `Dictionnaire` où les clés étaients toujours des chaînes

* Il y a deux paramètres de types:
    * `K`: le type des clés
    * `V`: le type des valeurs

* IMPORTANT: une clé est associée à **une seule valeur**

* Exemple d'utilisation:

```java
{{% embed "./Map_exemples.java" %}}
```
    * NOTE: on utilise la notation `<P1, P2>` quand il y a plusieurs paramètres de type


* Extrait de l'interface:

```java
{{% embed "./Map_interface.java" %}}
```

* Documentation complète: 
    * <a href="https://docs.oracle.com/javase/8/docs/api/java/util/Map.html" target="_blank">https://docs.oracle.com/javase/8/docs/api/java/util/Map.html</a>

## Implantations de `Map<K,V>`

<center>
<video width="50%" src="05.mp4" type="video/mp4" controls playsinline>
</center>

* Le SDK Java fournit différentes implantations, p.ex:
    * `HashMap<K,V>`
    * `TreeMap<K,V>`
    * `LinkedHashMap<K,V>`

* Quelle est la différence?
    * à utiliser: presqu'aucune (*)
    * à coder: on en parle à partir de l'$[link ../../../05/03/](étape 5.3)
    * en terme d'efficacité? on va le tester!

* (*) `LinkedHashMap` est un cas spécial:
    * en plus d'implanter l'interface `Map<K,V>`, on ajoute la promesse suivante:
        * il est possible d'accéder aux clés dans l'ordre où elles ont été insérées


### Comparaison des implantations

* L'implantation `HashMap` est typiquement supérieure

* Pour ajouter plusieurs valeurs
    * `HashMap` est beaucoup plus efficace

    <center>
        <img src="map_ajouts.png" width="90%"/>
    </center>

* Pour modifier plusieurs valeurs:
    * `HashMap` est plus efficace

    <center>
        <img src="map_modifs.png" width="90%"/>
    </center>

* Pour retirer plusieurs valeurs:
    * `HashMap` est plus efficace

    <center>
        <img src="map_retraits.png" width="90%"/>
    </center>

* Pour accéder aux clés de la plus petite à la plus grande:
    * `TreeMap` est très légèrement plus efficace

    <center>
        <img src="map_cles.png" width="90%"/>
    </center>

## `Map<K,V>`: quand utiliser quelle implantation?

* Utiliser `HashMap` par défaut

* Mais `TreeMap` peut être utile dans certains cas:
    * p.ex. ça consomme moins d'espace mémoire (voir $[link ../../04/theorie](théorie 4.4))
