---
title: "Atelier 4.3.A: implanter une liste chaînée simple"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer la classe `ElementChaineSimple`

* Ajouter la classe suivante au **paquet** `atelier4_3_A`
    * Nom de la classe: `ElementChaineSimple`

* Débuter l'implantation comme suit:

```java
{{% embed "ElementChaineSimple.java" %}}
```

* **TRUC**: compléter l'implantation de l'élément une fois l'outil de validation configuré

## Créer la classe `ListeChaineeSimple`

* Ajouter la classe suivante au **paquet** `atelier4_3_A`
    * Nom de la classe: `ListeChaineeSimple`

* Débuter l'implantation comme suit:

```java
{{% embed "ListeChaineeSimple.java" %}}
```

* Utiliser {{% key "Ctrl+1" %}} pour générer les méthodes d'une liste

* **TRUC**: compléter l'implantation de la liste une fois l'outil de validation configuré

## Recopier certaines classe de l'`atelier4_2`

* À partir de l'`atelier4_2`, recopier 
	* `ListeTableau`
	* `TesteurDeListeAbstrait`
	* `TesteurDeListeTableau`

## Créer la classe `TesteurDeListeChaineeSimple`

* Ajouter la classe suivante au **paquet** `atelier4_3_A`
    * Nom de la classe: `TesteurDeListeChaineeSimple`

* Implanter la classe comme suit:

```java
{{% embed "TesteurDeListeChaineeSimple.java" %}}
```

## Créer la classe `MonAtelier4_3_A`

* Ajouter la classe suivante au **paquet** `atelier4_3_A`
    * Nom de la classe: `MonAtelier4_3_A`

* Implanter la classe comme suit:

```java
{{% embed "MonAtelier4_2.java" %}}
```

## Compléter les implantations

* Utiliser l'outil de validation pour tester votre code

<img src="validation.png"/>

* vous pouvez aussi afficher le fichier `html` suivant:

    * `ListeChaineeSimple.html`
    * (à la racine du projet)

## Tester l'efficacité du code

* Les méthodes de votre `TesteurDeListeAbstrait` seront appelées
    * typiquement, utiliser une boucle pour effectuer une opération un certain nombre de fois

* Vous devriez avoir des résultats similaires aux résultats ci-bas

    * `melangerLaListe`

    <img src="melangerLaListe.png"/>

    * `melangerLaListeEfficace`

    <img src="melangerLaListeEfficace.png"/>

    * `fairePlusieursModificationsAleatoires`

    <img src="fairePlusieursModificationsAleatoires.png"/>

    * `fairePlusieursAjouts`

    <img src="fairePlusieursAjouts.png"/>

    * `fairePlusieursRetraitsALaFin`

    <img src="fairePlusieursRetraitsALaFin.png"/>

    * `fairePlusieursRetraitsAuDebut`

    <img src="fairePlusieursRetraitsAuDebut.png"/>


    * `fairePlusieursInsertionsAuDebut`

    <img src="fairePlusieursInsertionsAuDebut.png"/>

    * `fairePlusieursInsertionsAleatoires`

    <img src="fairePlusieursInsertionsAleatoires.png"/>

