package atelier4_2;

import tutoriels.liste.Liste;

public class ListeTableau<E extends Object> extends ListeJava<E> {
	
	private final int TAILLE_INITIALE = 4;

	private E[] grosTableau = nouveauTableau(TAILLE_INITIALE);
	private int indiceDernierElement = -1;

	public E[] getGrosTableau() {
		return grosTableau;
	}

	public void setGrosTableau(E[] grosTableau) {
		this.grosTableau = grosTableau;
	}

	public int getIndiceDernierElement() {
		return indiceDernierElement;
	}

	public void setIndiceDernierElement(int indiceDernierElement) {
		this.indiceDernierElement = indiceDernierElement;
	}

	public ListeTableau(Class<E> typeElement){
		super(typeElement);
	}

	private void copier(E[] src, int debutSrc, int finSrc, E[] dst, int debutDst) {
		int decalage = debutDst - debutSrc;

		for(int i = debutSrc; i <= finSrc; i++){
			dst[i+decalage] = src[i];
		}
	}
	
	private void decalerVersLaGauche(int debut, int fin) {
		for(int i = debut; i <= fin; i++) {
			grosTableau[i-1] = grosTableau[i];
		}
	}

	private void decalerVersLaDroite(int debut, int fin) {
		for(int i = fin; i >= debut; i--) {
			grosTableau[i+1] = grosTableau[i];
		}
	}

	private void agrandir() {
		E[] nouveauGrosTableau = nouveauTableau(grosTableau.length*2);
		
		copier(grosTableau, 0, indiceDernierElement, nouveauGrosTableau, 0);
		
		grosTableau = nouveauGrosTableau;
	}

	@Override
	public void add(E e) {
		insert(size(),e);
	}

	@Override
	public void addAll(E[] valeurs_a_ajouter) {
		for(E e : valeurs_a_ajouter) {
			add(e);
		}
	}

	@Override
	public void insert(int position, E e) {
		if(indiceDernierElement == grosTableau.length - 1) {
			agrandir();
		}

		decalerVersLaDroite(position, indiceDernierElement);
		indiceDernierElement++;

		grosTableau[position] = e;
	}

	@Override
	public void set(int i, E e) {
		grosTableau[i] = e;
	}

	@Override
	public E get(int i) {
		return grosTableau[i];
	}

	@Override
	public void clear() {
		indiceDernierElement = -1;
	}

	@Override
	public int size() {
		return indiceDernierElement + 1;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public boolean contains(Object o) {
		return indexOf(o) != -1;
	}

	@Override
	public int indexOf(Object o) {
		int indice = -1;
		
		for(int i = 0; i <= indiceDernierElement; i++) {
			if(grosTableau[i].equals(o)) {
				indice = i;
				break;
			}
		}

		return indice;
	}

	@Override
	public void removeValue(Object o) {
		int index = indexOf(o);
		if(index >= 0) {
			remove(index);
		}
	}

	@Override
	public void remove(int position) {
		decalerVersLaGauche(position+1, indiceDernierElement);
		indiceDernierElement--;
	}

}
