package atelier4_2;

import tutoriels.liste.Liste;

public class ListeDebut<E extends Object> extends ListeJava<E> {
	
	private final int TAILLE_INITIALE = 100;

	private E[] grosTableau = nouveauTableau(TAILLE_INITIALE);
	private int indicePremierElement = TAILLE_INITIALE;
	
	public ListeDebut(Class<E> typeElement){
		super(typeElement);
	}

	private void copier(E[] src, int debutSrc, int finSrc, E[] dst, int debutDst) {
		int decalage = debutDst - debutSrc;

		for(int i = debutSrc; i <= finSrc; i++){
			dst[i+decalage] = src[i];
		}
	}
	
	private void agrandir() {
		int ancienneTaille = grosTableau.length;
		int nouvelleTaille = ancienneTaille * 2;

		E[] nouveauGrosTableau = nouveauTableau(nouvelleTaille);
		
		int nouvelIndicePremierElement = nouvelleTaille - ancienneTaille;
		
		copier(grosTableau, indicePremierElement, ancienneTaille-1, nouveauGrosTableau, nouvelIndicePremierElement);
		
		indicePremierElement = nouvelIndicePremierElement;
		grosTableau = nouveauGrosTableau;
	}

	@Override
	public void add(E e) {
		insert(size(), e);
	}

	@Override
	public void addAll(E[] valeurs_a_ajouter) {
		for(E e : valeurs_a_ajouter) {
			add(e);
		}
	}

	@Override
	public void insert(int position, E e) {
		if(indicePremierElement == 0) {
			agrandir();
		}
		
		
		// décaler vers la gauche
		for(int i = indicePremierElement; i < indicePremierElement+position; i++) {
			grosTableau[i-1] = grosTableau[i];
		}

		indicePremierElement--;

		grosTableau[indicePremierElement+position] = e;
	}

	@Override
	public void set(int position, E e) {
		grosTableau[indicePremierElement + position] = e;
	}

	@Override
	public E get(int position) {
		return grosTableau[indicePremierElement + position];
	}

	@Override
	public void clear() {
		indicePremierElement = grosTableau.length;
	}

	@Override
	public int size() {
		return grosTableau.length - indicePremierElement;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public boolean contains(Object o) {
		return indexOf(o) != -1;
	}

	@Override
	public int indexOf(Object o) {
		int position = -1;

		for(int i = indicePremierElement; i < grosTableau.length; i++) {
			if(grosTableau[i].equals(o)) {
				position = i - indicePremierElement;
				break;
			}
		}

		return position;
	}

	@Override
	public void removeValue(Object o) {
		int index = indexOf(o);
		if(index >= 0) {
			remove(index);
		}
	}

	@Override
	public void remove(int position) {
		
		// décaler vers la droite
		for(int i = indicePremierElement+position; i > indicePremierElement; i--) {
			grosTableau[i] = grosTableau[i-1];
		}

		indicePremierElement++;
	}
}
