public class ListeChaineeSimple<E extends Object> extends ListeJava<E> {
	
	private int taille = 0;
	private ElementChaineSimple<E> tete = null;

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	public ElementChaineSimple<E> getTete() {
		return tete;
	}

	public void setTete(ElementChaineSimple<E> tete) {
		this.tete = tete;
	}

	public ListeChaineeSimple(Class<E> typeElement) {
		super(typeElement);
	}

	// TODO
}
