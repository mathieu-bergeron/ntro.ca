public class TesteurDeListeChaineeSimple extends TesteurDeListeAbstrait {

	@Override
	public ListeJava<Character> nouvelleListe() {
		return new ListeChaineeSimple<>(Character.class);
	}
}
