public class ElementChaineSimple<E> {
	
	private E valeur;

	private ElementChaineSimple<E> suivant = null;

	public E getValeur() {
		return valeur;
	}

	public void setValeur(E valeur) {
		this.valeur = valeur;
	}

	public ElementChaineSimple<E> getSuivant() {
		return suivant;
	}

	public void setSuivant(ElementChaineSimple<E> suivant) {
		this.suivant = suivant;
	}

	public ElementChaineSimple(E e) {
		this.valeur = e;
	}

	// TODO: au besoin, ajouter une méthode insererApres
}
