public class MonAtelier4_3_A extends Atelier4_3_A {
    
    public static void main(String[] args) {
        (new MonAtelier4_3_A()).valider();
    }

    @Override
    public ListeJava<Character> fournirListeTableau() {
        return new ListeTableau<Character>(Character.class);
    }

    @Override
    public ListeJava<Character> fournirListeChaineeSimple() {
        return new ListeChaineeSimple<Character>(Character.class);
    }

    @Override
    public TesteurDeListe fournirTesteurDeListeTableau() {
        return new TesteurDeListeTableau();
    }

    @Override
    public TesteurDeListe fournirTesteurDeListeSimple() {
        return new TesteurDeListeChaineeSimple();
    }
}
