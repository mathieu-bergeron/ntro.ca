---
---
<div style='max-width:80%; margin:auto;'><h1>test00</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test00_avant.png'/></td><td>liste.add(k);</td><td>[k]<br><img src='./_storage/graphs/ListeChaineeSimple_test00_ok.png'/></td></tr></table><h1>test01</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k]<br><img src='./_storage/graphs/ListeChaineeSimple_test01_avant.png'/></td><td>liste.add(f);</td><td>[k, f]<br><img src='./_storage/graphs/ListeChaineeSimple_test01_ok.png'/></td></tr></table><h1>test02</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f]<br><img src='./_storage/graphs/ListeChaineeSimple_test02_avant.png'/></td><td>liste.add(x);</td><td>[k, f, x]<br><img src='./_storage/graphs/ListeChaineeSimple_test02_ok.png'/></td></tr></table><h1>test03</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x]<br><img src='./_storage/graphs/ListeChaineeSimple_test03_avant.png'/></td><td>liste.add(h);</td><td>[k, f, x, h]<br><img src='./_storage/graphs/ListeChaineeSimple_test03_ok.png'/></td></tr></table><h1>test04</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h]<br><img src='./_storage/graphs/ListeChaineeSimple_test04_avant.png'/></td><td>liste.add(m);</td><td>[k, f, x, h, m]<br><img src='./_storage/graphs/ListeChaineeSimple_test04_ok.png'/></td></tr></table><h1>test05</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m]<br><img src='./_storage/graphs/ListeChaineeSimple_test05_avant.png'/></td><td>liste.add(s);</td><td>[k, f, x, h, m, s]<br><img src='./_storage/graphs/ListeChaineeSimple_test05_ok.png'/></td></tr></table><h1>test06</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s]<br><img src='./_storage/graphs/ListeChaineeSimple_test06_avant.png'/></td><td>liste.add(p);</td><td>[k, f, x, h, m, s, p]<br><img src='./_storage/graphs/ListeChaineeSimple_test06_ok.png'/></td></tr></table><h1>test07</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s, p]<br><img src='./_storage/graphs/ListeChaineeSimple_test07_avant.png'/></td><td>liste.add(a);</td><td>[k, f, x, h, m, s, p, a]<br><img src='./_storage/graphs/ListeChaineeSimple_test07_ok.png'/></td></tr></table><h1>test08</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s, p, a]<br><img src='./_storage/graphs/ListeChaineeSimple_test08_avant.png'/></td><td>liste.add(i);</td><td>[k, f, x, h, m, s, p, a, i]<br><img src='./_storage/graphs/ListeChaineeSimple_test08_ok.png'/></td></tr></table><h1>test09</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s, p, a, i]<br><img src='./_storage/graphs/ListeChaineeSimple_test09_avant.png'/></td><td>liste.add(z);</td><td>[k, f, x, h, m, s, p, a, i, z]<br><img src='./_storage/graphs/ListeChaineeSimple_test09_ok.png'/></td></tr></table><h1>test10</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s, p, a, i, z]<br><img src='./_storage/graphs/ListeChaineeSimple_test10_avant.png'/></td><td>liste.add(o);</td><td>[k, f, x, h, m, s, p, a, i, z, o]<br><img src='./_storage/graphs/ListeChaineeSimple_test10_ok.png'/></td></tr></table><h1>test11</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s, p, a, i, z, o]<br><img src='./_storage/graphs/ListeChaineeSimple_test11_avant.png'/></td><td>liste.add(q);</td><td>[k, f, x, h, m, s, p, a, i, z, o, q]<br><img src='./_storage/graphs/ListeChaineeSimple_test11_ok.png'/></td></tr></table><h1>test12</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s, p, a, i, z, o, q]<br><img src='./_storage/graphs/ListeChaineeSimple_test12_avant.png'/></td><td>liste.add(e);</td><td>[k, f, x, h, m, s, p, a, i, z, o, q, e]<br><img src='./_storage/graphs/ListeChaineeSimple_test12_ok.png'/></td></tr></table><h1>test13</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s, p, a, i, z, o, q, e]<br><img src='./_storage/graphs/ListeChaineeSimple_test13_avant.png'/></td><td>liste.add(a);</td><td>[k, f, x, h, m, s, p, a, i, z, o, q, e, a]<br><img src='./_storage/graphs/ListeChaineeSimple_test13_ok.png'/></td></tr></table><h1>test14</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s, p, a, i, z, o, q, e, a]<br><img src='./_storage/graphs/ListeChaineeSimple_test14_avant.png'/></td><td>liste.add(j);</td><td>[k, f, x, h, m, s, p, a, i, z, o, q, e, a, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test14_ok.png'/></td></tr></table><h1>test15</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test15_avant.png'/></td><td>liste.addAll([]);</td><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test15_ok.png'/></td></tr></table><h1>test16</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test16_avant.png'/></td><td>liste.addAll([a, b, c, d, e, f, g]);</td><td>[a, b, c, d, e, f, g]<br><img src='./_storage/graphs/ListeChaineeSimple_test16_ok.png'/></td></tr></table><h1>test17</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test17_avant.png'/></td><td>liste.insert(0,j);</td><td>[j]<br><img src='./_storage/graphs/ListeChaineeSimple_test17_ok.png'/></td></tr></table><h1>test18</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s, p, a, i, z, o, q, e, a, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test18_avant.png'/></td><td>liste.insert(14,g);</td><td>[k, f, x, h, m, s, p, a, i, z, o, q, e, a, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test18_ok.png'/></td></tr></table><h1>test19</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[k, f, x, h, m, s, p, a, i, z, o, q, e, a, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test19_avant.png'/></td><td>liste.insert(0,y);</td><td>[y, k, f, x, h, m, s, p, a, i, z, o, q, e, a, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test19_ok.png'/></td></tr></table><h1>test20</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[y, k, f, x, h, m, s, p, a, i, z, o, q, e, a, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test20_avant.png'/></td><td>liste.insert(2,a);</td><td>[y, k, a, f, x, h, m, s, p, a, i, z, o, q, e, a, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test20_ok.png'/></td></tr></table><h1>test21</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, a, i, z, o, q, e, a, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test21_avant.png'/></td><td>liste.set(12,s);</td><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, a, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test21_ok.png'/></td></tr></table><h1>test22</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[j]<br><img src='./_storage/graphs/ListeChaineeSimple_test22_avant.png'/></td><td>liste.set(0,n);</td><td>[n]<br><img src='./_storage/graphs/ListeChaineeSimple_test22_ok.png'/></td></tr></table><h1>test23</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, a, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test23_avant.png'/></td><td>liste.set(15,n);</td><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, n, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test23_ok.png'/></td></tr></table><h1>test24</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[a, b, c, d, e, f, g]<br><img src='./_storage/graphs/ListeChaineeSimple_test24_avant.png'/></td><td>liste.get(5);</td><td>f</td></table><h1>test25</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[n]<br><img src='./_storage/graphs/ListeChaineeSimple_test25_avant.png'/></td><td>liste.get(0);</td><td>n</td></table><h1>test26</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, n, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test26_avant.png'/></td><td>liste.get(16);</td><td>g</td></table><h1>test27</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test27_avant.png'/></td><td>liste.size();</td><td>0</td></table><h1>test28</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, n, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test28_avant.png'/></td><td>liste.size();</td><td>18</td></table><h1>test29</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, n, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test29_avant.png'/></td><td>liste.size();</td><td>18</td></table><h1>test30</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test30_avant.png'/></td><td>liste.isEmpty();</td><td>true</td></table><h1>test31</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[n]<br><img src='./_storage/graphs/ListeChaineeSimple_test31_avant.png'/></td><td>liste.isEmpty();</td><td>false</td></table><h1>test32</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[a, b, c, d, e, f, g]<br><img src='./_storage/graphs/ListeChaineeSimple_test32_avant.png'/></td><td>liste.isEmpty();</td><td>false</td></table><h1>test33</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test33_avant.png'/></td><td>liste.contains(b);</td><td>false</td></table><h1>test34</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, n, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test34_avant.png'/></td><td>liste.contains(v);</td><td>false</td></table><h1>test35</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[n]<br><img src='./_storage/graphs/ListeChaineeSimple_test35_avant.png'/></td><td>liste.contains(n);</td><td>true</td></table><h1>test36</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test36_avant.png'/></td><td>liste.indexOf(h);</td><td>-1</td></table><h1>test37</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, n, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test37_avant.png'/></td><td>liste.indexOf(h);</td><td>5</td></table><h1>test38</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[n]<br><img src='./_storage/graphs/ListeChaineeSimple_test38_avant.png'/></td><td>liste.indexOf(n);</td><td>0</td></table><h1>test39</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test39_avant.png'/></td><td>liste.removeValue(a);</td><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test39_ok.png'/></td></tr></table><h1>test40</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, f, g]<br><img src='./_storage/graphs/ListeChaineeSimple_test40_avant.png'/></td><td>liste.removeValue(g);</td><td>[a, b, c, d, e, f]<br><img src='./_storage/graphs/ListeChaineeSimple_test40_ok.png'/></td></tr></table><h1>test41</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, f]<br><img src='./_storage/graphs/ListeChaineeSimple_test41_avant.png'/></td><td>liste.removeValue(a);</td><td>[b, c, d, e, f]<br><img src='./_storage/graphs/ListeChaineeSimple_test41_ok.png'/></td></tr></table><h1>test42</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, n, g, j]<br><img src='./_storage/graphs/ListeChaineeSimple_test42_avant.png'/></td><td>liste.removeValue(j);</td><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, n, g]<br><img src='./_storage/graphs/ListeChaineeSimple_test42_ok.png'/></td></tr></table><h1>test43</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, a, i, z, s, q, e, n, g]<br><img src='./_storage/graphs/ListeChaineeSimple_test43_avant.png'/></td><td>liste.removeValue(i);</td><td>[y, k, a, f, x, h, m, s, p, a, z, s, q, e, n, g]<br><img src='./_storage/graphs/ListeChaineeSimple_test43_ok.png'/></td></tr></table><h1>test44</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[n]<br><img src='./_storage/graphs/ListeChaineeSimple_test44_avant.png'/></td><td>liste.remove(0);</td><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test44_ok.png'/></td></tr></table><h1>test45</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[b, c, d, e, f]<br><img src='./_storage/graphs/ListeChaineeSimple_test45_avant.png'/></td><td>liste.remove(4);</td><td>[b, c, d, e]<br><img src='./_storage/graphs/ListeChaineeSimple_test45_ok.png'/></td></tr></table><h1>test46</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, a, z, s, q, e, n, g]<br><img src='./_storage/graphs/ListeChaineeSimple_test46_avant.png'/></td><td>liste.remove(9);</td><td>[y, k, a, f, x, h, m, s, p, z, s, q, e, n, g]<br><img src='./_storage/graphs/ListeChaineeSimple_test46_ok.png'/></td></tr></table><h1>test47</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[b, c, d, e]<br><img src='./_storage/graphs/ListeChaineeSimple_test47_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test47_ok.png'/></td></tr></table><h1>test48</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[y, k, a, f, x, h, m, s, p, z, s, q, e, n, g]<br><img src='./_storage/graphs/ListeChaineeSimple_test48_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test48_ok.png'/></td></tr></table><h1>test49</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test49_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='./_storage/graphs/ListeChaineeSimple_test49_ok.png'/></td></tr></table></div>
