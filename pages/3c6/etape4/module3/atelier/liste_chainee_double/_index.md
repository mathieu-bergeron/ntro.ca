---
title: "Atelier 4.3.B: implanter une liste chaînée double"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Objectif


* {{% link "/3c6/etape4/module3/atelier/liste_chainee_double/coder_atelier" "Implanter et valider une liste chaînée double" %}}

## Exemples de tests réussis

* <a href="tests/ListeChaineeDouble"><code>ListeChaineeDouble</code></a>


