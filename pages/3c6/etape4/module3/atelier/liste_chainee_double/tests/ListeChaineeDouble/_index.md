---
---
<div style='max-width:80%; margin:auto;'><h1>test00</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test00_avant.png'/></td><td>liste.add(i);</td><td>[i]<br><img src='./_storage/graphs/ListeChaineeDouble_test00_ok.png'/></td></tr></table><h1>test01</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i]<br><img src='./_storage/graphs/ListeChaineeDouble_test01_avant.png'/></td><td>liste.add(m);</td><td>[i, m]<br><img src='./_storage/graphs/ListeChaineeDouble_test01_ok.png'/></td></tr></table><h1>test02</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m]<br><img src='./_storage/graphs/ListeChaineeDouble_test02_avant.png'/></td><td>liste.add(c);</td><td>[i, m, c]<br><img src='./_storage/graphs/ListeChaineeDouble_test02_ok.png'/></td></tr></table><h1>test03</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c]<br><img src='./_storage/graphs/ListeChaineeDouble_test03_avant.png'/></td><td>liste.add(g);</td><td>[i, m, c, g]<br><img src='./_storage/graphs/ListeChaineeDouble_test03_ok.png'/></td></tr></table><h1>test04</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g]<br><img src='./_storage/graphs/ListeChaineeDouble_test04_avant.png'/></td><td>liste.add(d);</td><td>[i, m, c, g, d]<br><img src='./_storage/graphs/ListeChaineeDouble_test04_ok.png'/></td></tr></table><h1>test05</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d]<br><img src='./_storage/graphs/ListeChaineeDouble_test05_avant.png'/></td><td>liste.add(v);</td><td>[i, m, c, g, d, v]<br><img src='./_storage/graphs/ListeChaineeDouble_test05_ok.png'/></td></tr></table><h1>test06</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v]<br><img src='./_storage/graphs/ListeChaineeDouble_test06_avant.png'/></td><td>liste.add(f);</td><td>[i, m, c, g, d, v, f]<br><img src='./_storage/graphs/ListeChaineeDouble_test06_ok.png'/></td></tr></table><h1>test07</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f]<br><img src='./_storage/graphs/ListeChaineeDouble_test07_avant.png'/></td><td>liste.add(o);</td><td>[i, m, c, g, d, v, f, o]<br><img src='./_storage/graphs/ListeChaineeDouble_test07_ok.png'/></td></tr></table><h1>test08</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, o]<br><img src='./_storage/graphs/ListeChaineeDouble_test08_avant.png'/></td><td>liste.add(u);</td><td>[i, m, c, g, d, v, f, o, u]<br><img src='./_storage/graphs/ListeChaineeDouble_test08_ok.png'/></td></tr></table><h1>test09</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, o, u]<br><img src='./_storage/graphs/ListeChaineeDouble_test09_avant.png'/></td><td>liste.add(e);</td><td>[i, m, c, g, d, v, f, o, u, e]<br><img src='./_storage/graphs/ListeChaineeDouble_test09_ok.png'/></td></tr></table><h1>test10</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, o, u, e]<br><img src='./_storage/graphs/ListeChaineeDouble_test10_avant.png'/></td><td>liste.add(p);</td><td>[i, m, c, g, d, v, f, o, u, e, p]<br><img src='./_storage/graphs/ListeChaineeDouble_test10_ok.png'/></td></tr></table><h1>test11</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, o, u, e, p]<br><img src='./_storage/graphs/ListeChaineeDouble_test11_avant.png'/></td><td>liste.add(f);</td><td>[i, m, c, g, d, v, f, o, u, e, p, f]<br><img src='./_storage/graphs/ListeChaineeDouble_test11_ok.png'/></td></tr></table><h1>test12</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, o, u, e, p, f]<br><img src='./_storage/graphs/ListeChaineeDouble_test12_avant.png'/></td><td>liste.add(p);</td><td>[i, m, c, g, d, v, f, o, u, e, p, f, p]<br><img src='./_storage/graphs/ListeChaineeDouble_test12_ok.png'/></td></tr></table><h1>test13</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, o, u, e, p, f, p]<br><img src='./_storage/graphs/ListeChaineeDouble_test13_avant.png'/></td><td>liste.add(p);</td><td>[i, m, c, g, d, v, f, o, u, e, p, f, p, p]<br><img src='./_storage/graphs/ListeChaineeDouble_test13_ok.png'/></td></tr></table><h1>test14</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, o, u, e, p, f, p, p]<br><img src='./_storage/graphs/ListeChaineeDouble_test14_avant.png'/></td><td>liste.add(n);</td><td>[i, m, c, g, d, v, f, o, u, e, p, f, p, p, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test14_ok.png'/></td></tr></table><h1>test15</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test15_avant.png'/></td><td>liste.addAll([]);</td><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test15_ok.png'/></td></tr></table><h1>test16</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test16_avant.png'/></td><td>liste.addAll([a, b, c, d, e, f, g]);</td><td>[a, b, c, d, e, f, g]<br><img src='./_storage/graphs/ListeChaineeDouble_test16_ok.png'/></td></tr></table><h1>test17</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test17_avant.png'/></td><td>liste.insert(0,l);</td><td>[l]<br><img src='./_storage/graphs/ListeChaineeDouble_test17_ok.png'/></td></tr></table><h1>test18</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, o, u, e, p, f, p, p, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test18_avant.png'/></td><td>liste.insert(14,r);</td><td>[i, m, c, g, d, v, f, o, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test18_ok.png'/></td></tr></table><h1>test19</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[l]<br><img src='./_storage/graphs/ListeChaineeDouble_test19_avant.png'/></td><td>liste.insert(0,b);</td><td>[b, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test19_ok.png'/></td></tr></table><h1>test20</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[b, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test20_avant.png'/></td><td>liste.insert(1,m);</td><td>[b, m, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test20_ok.png'/></td></tr></table><h1>test21</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, f, g]<br><img src='./_storage/graphs/ListeChaineeDouble_test21_avant.png'/></td><td>liste.set(6,x);</td><td>[a, b, c, d, e, f, x]<br><img src='./_storage/graphs/ListeChaineeDouble_test21_ok.png'/></td></tr></table><h1>test22</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[b, m, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test22_avant.png'/></td><td>liste.set(1,g);</td><td>[b, g, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test22_ok.png'/></td></tr></table><h1>test23</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, o, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test23_avant.png'/></td><td>liste.set(7,n);</td><td>[i, m, c, g, d, v, f, n, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test23_ok.png'/></td></tr></table><h1>test24</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[i, m, c, g, d, v, f, n, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test24_avant.png'/></td><td>liste.get(4);</td><td>d</td></table><h1>test25</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[i, m, c, g, d, v, f, n, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test25_avant.png'/></td><td>liste.get(12);</td><td>p</td></table><h1>test26</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[a, b, c, d, e, f, x]<br><img src='./_storage/graphs/ListeChaineeDouble_test26_avant.png'/></td><td>liste.get(0);</td><td>a</td></table><h1>test27</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test27_avant.png'/></td><td>liste.size();</td><td>0</td></table><h1>test28</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[i, m, c, g, d, v, f, n, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test28_avant.png'/></td><td>liste.size();</td><td>16</td></table><h1>test29</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[b, g, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test29_avant.png'/></td><td>liste.size();</td><td>3</td></table><h1>test30</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test30_avant.png'/></td><td>liste.isEmpty();</td><td>true</td></table><h1>test31</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[i, m, c, g, d, v, f, n, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test31_avant.png'/></td><td>liste.isEmpty();</td><td>false</td></table><h1>test32</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[b, g, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test32_avant.png'/></td><td>liste.isEmpty();</td><td>false</td></table><h1>test33</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test33_avant.png'/></td><td>liste.contains(u);</td><td>false</td></table><h1>test34</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[a, b, c, d, e, f, x]<br><img src='./_storage/graphs/ListeChaineeDouble_test34_avant.png'/></td><td>liste.contains(k);</td><td>false</td></table><h1>test35</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[a, b, c, d, e, f, x]<br><img src='./_storage/graphs/ListeChaineeDouble_test35_avant.png'/></td><td>liste.contains(c);</td><td>true</td></table><h1>test36</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test36_avant.png'/></td><td>liste.indexOf(f);</td><td>-1</td></table><h1>test37</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[i, m, c, g, d, v, f, n, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test37_avant.png'/></td><td>liste.indexOf(q);</td><td>-1</td></table><h1>test38</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[b, g, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test38_avant.png'/></td><td>liste.indexOf(l);</td><td>2</td></table><h1>test39</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test39_avant.png'/></td><td>liste.removeValue(v);</td><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test39_ok.png'/></td></tr></table><h1>test40</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, n, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test40_avant.png'/></td><td>liste.removeValue(h);</td><td>[i, m, c, g, d, v, f, n, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test40_ok.png'/></td></tr></table><h1>test41</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, f, x]<br><img src='./_storage/graphs/ListeChaineeDouble_test41_avant.png'/></td><td>liste.removeValue(a);</td><td>[b, c, d, e, f, x]<br><img src='./_storage/graphs/ListeChaineeDouble_test41_ok.png'/></td></tr></table><h1>test42</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, n, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test42_avant.png'/></td><td>liste.removeValue(n);</td><td>[i, m, c, g, d, v, f, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test42_ok.png'/></td></tr></table><h1>test43</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, u, e, p, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test43_avant.png'/></td><td>liste.removeValue(p);</td><td>[i, m, c, g, d, v, f, u, e, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test43_ok.png'/></td></tr></table><h1>test44</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[b, g, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test44_avant.png'/></td><td>liste.remove(0);</td><td>[g, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test44_ok.png'/></td></tr></table><h1>test45</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[i, m, c, g, d, v, f, u, e, f, p, p, r, n]<br><img src='./_storage/graphs/ListeChaineeDouble_test45_avant.png'/></td><td>liste.remove(13);</td><td>[i, m, c, g, d, v, f, u, e, f, p, p, r]<br><img src='./_storage/graphs/ListeChaineeDouble_test45_ok.png'/></td></tr></table><h1>test46</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[g, l]<br><img src='./_storage/graphs/ListeChaineeDouble_test46_avant.png'/></td><td>liste.remove(0);</td><td>[l]<br><img src='./_storage/graphs/ListeChaineeDouble_test46_ok.png'/></td></tr></table><h1>test47</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[b, c, d, e, f, x]<br><img src='./_storage/graphs/ListeChaineeDouble_test47_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test47_ok.png'/></td></tr></table><h1>test48</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[l]<br><img src='./_storage/graphs/ListeChaineeDouble_test48_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test48_ok.png'/></td></tr></table><h1>test49</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test49_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='./_storage/graphs/ListeChaineeDouble_test49_ok.png'/></td></tr></table></div>
