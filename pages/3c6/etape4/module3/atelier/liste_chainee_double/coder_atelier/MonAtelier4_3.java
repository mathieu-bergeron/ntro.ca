package atelier4_3;

import tutoriels.liste.Liste;
import tutoriels.liste.TesteurDeListe;

public class MonAtelier4_3_B extends Atelier4_3_B {
    
    public static void main(String[] args) {
        
        (new MonAtelier4_3_B()).valider();
    }


    @Override
    public ListeJava<Character> fournirListeTableau() {
        return new ListeTableau<Character>(Character.class);
    }


    @Override
    public ListeJava<Character> fournirListeChaineeSimple() {
        return new ListeChaineeSimple<Character>(Character.class);
    }

    @Override
    public ListeJava<Character> fournirListeChaineeDouble() {
        return new ListeChaineeDouble<Character>(Character.class);
    }

    @Override
    public TesteurDeListe fournirTesteurDeListeTableau() {
        return new TesteurDeListeTableau();
    }

    @Override
    public TesteurDeListe fournirTesteurDeListeSimple() {
        return new TesteurDeListeChaineeSimple();
    }

    @Override
    public TesteurDeListe fournirTesteurDeListeDouble() {
        return new TesteurDeListeChaineeDouble();
    }

}
