public class ElementChaineDouble<E> {
	
	private E valeur;

	private ElementChaineDouble<E> suivant = this;
	private ElementChaineDouble<E> precedent = this;

	public E getValeur() {
		return valeur;
	}

	public void setValeur(E valeur) {
		this.valeur = valeur;
	}

	public ElementChaineDouble<E> getSuivant() {
		return suivant;
	}

	public void setSuivant(ElementChaineDouble<E> suivant) {
		this.suivant = suivant;
	}

	public ElementChaineDouble<E> getPrecedent() {
		return precedent;
	}

	public void setPrecedent(ElementChaineDouble<E> precedent) {
		this.precedent = precedent;
	}

	public ElementChaineDouble(E e) {
		this.valeur = e;
	}


	public ElementChaineDouble(ElementChaineDouble<E> precedent, E e, ElementChaineDouble<E> suivant) {
		this.precedent = precedent;
		this.valeur = e;
		this.suivant = suivant;
	}

	// Au besion, ajouter des méthodes du genre
	// insererAvant
	// insererApres
}
