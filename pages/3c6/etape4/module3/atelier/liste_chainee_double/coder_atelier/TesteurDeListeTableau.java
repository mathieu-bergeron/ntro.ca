package atelier4_3;


import tutoriels.liste.Liste;
import tutoriels.liste.ListeReference;

public class TesteurDeListeTableau extends TesteurDeListeAbstrait {

	@Override
	public ListeJava<Character> nouvelleListe() {
		return new ListeTableau<>(Character.class);
		//return new ListeDebut<>(Character.class);
	}

}
