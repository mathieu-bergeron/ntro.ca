public class TesteurDeListeChaineeDouble extends TesteurDeListeAbstrait {

	@Override
	public ListeJava<Character> nouvelleListe() {
		return new ListeChaineeDouble<>(Character.class);
	}
}
