---
title: "Module 4.3: liste chaînée"
weight: 30
draft: false
---

{{% pageTitle %}}


1. Théorie
    * {{% link "/3c6/etape4/module3/theorie/liste_chainee_simple" "Liste chaînée simple" %}}
    * {{% link "/3c6/etape4/module3/theorie/liste_chainee_double" "Liste chaînée double" %}}

1. {{% link "/3c6/etape4/module3/entrevue/" "Entrevue 4.3" %}}

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=332680" target="_blank">Mini-test théorique</a>

1. Atelier 4.3
    1. {{% link "/3c6/etape4/module3/atelier/liste_chainee_simple" "Exercice 4.3.A" %}}: liste chaînée simple

    1. (optionnel) {{% link "/3c6/etape4/module3/atelier/liste_chainee_double" "Exercice 4.3.B" %}}: liste chaînée double

    1. {{% link "atelier/remise_gitlab" "Remise GitLab" %}}

    1. {{% link "atelier/remise_moodle" "Remise Moodle" %}}


<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}
