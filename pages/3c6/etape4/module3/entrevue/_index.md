---
title: "Entrevue 4.2: liste chaînée simple"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier 

## Ajouter un élément à une liste chaînée simple

* Imaginer la liste chaînée simple suivante:

<img src="avant01.png" />

* Dessiner la même liste, mais **après** l'appel à `add('x')`

* Expliquer votre raisonnement

## Insérer un élément au début d'une liste chaînée simple

* Imaginer la liste chaînée simple suivante:

<img src="avant02.png" />

* Dessiner la même liste, mais **après** l'appel à `insert(0, 's')`

* Expliquer votre raisonnement


