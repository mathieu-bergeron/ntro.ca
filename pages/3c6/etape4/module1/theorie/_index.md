---
title: ""
weight: 1
bookHidden: true
---


# Théorie 4.1: liste naïve

<center>
<video width="50%" src="01.mp4" type="video/mp4" controls playsinline>
</center>

* Une liste est un tableau qui peut grandir et rapetisser

```java
{{% embed "ExempleListe.java" %}}
```

* Comment implanter une liste de façon efficace?

* Ça dépend. Efficace pour quelle opération?:
    * la modification d'éléments existants? (module 5.1)
    * l'ajout et le retrait de nouveaux éléments? (module 5.2)

* Rappel, voici les méthodes d'une liste

```java
{{% embed "Liste.java" %}}
```




## `O(1)`: temps constant

<center>
<video width="50%" src="02.mp4" type="video/mp4" controls playsinline>
</center>

* Quand on a parlé d'efficacité, on a définit:
    * Efficace:
        * `O(log(n))`: temps logarithmique
        * `O(n)`: temps linéaire
    * Pas efficace:
        * <code>O(n<sup>2</sup>)</code>: temps quadratique
        * <code>O(2<sup>n</sup>)</code>: temps exponentiel

* Il faut ajouter:
    * Efficace:
        * `O(1)`: temps constant
            * le nombre d'instructions ne dépend pas de la taille des données

* Accéder à une valeur dans un tableau se fait en temps contant:

```java
{{% embed src="ExempleTableau.java" first-line="1" last-line="6" %}}
```

* Même chose pour modifier une valeur dans un tableau:

```java
{{% embed src="ExempleTableau.java" first-line="9" last-line="15" %}}
```

## Liste par tableau naïve

<center>
<video width="50%" src="03.mp4" type="video/mp4" controls playsinline>
</center>

* Le plus simple est de mémoriser les éléments de la liste dans un tableau

```java
{{% embed src="ListeNaive.java" first-line="1" last-line="3" %}}
```

* La modification d'un élément existant se fera en temps constant

```java
{{% embed src="ListeNaive.java" first-line="5" last-line="8" %}}
```

* Pour ajouter un nouvel élément, c'est plus compliqué:

```java
{{% embed src="ListeNaive.java" first-line="10" last-line="21" %}}
```

* Pour ajouter un élément, il faut:
    * créer un nouveau tableau plus grand d'un emplacement
    * copier les éléments existants dans le nouveau tableau
    * mémoriser le nouvel élement dans le nouvel emplacement

* L'opération se fait en temps linéaire `O(n)`

* Le problème est que `add` est typiquement utilisé dans une boucle

* P.ex. pour mélanger une liste:

```java
{{% embed src="MelangeurNaif.java" %}}
```

* On a une boucle qui visite les éléments de `entree` 
    * si chaque `add` requiert une autre boucle sur les éléments
    * on va avoir une boucle dans une boucle et donc <code>O(n<sup>2</sup>)</code>

    <center>
    <img src="melangerNaif.png" width="90%"/>
    </center>
