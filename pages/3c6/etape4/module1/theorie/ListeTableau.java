public class ListeTableau<E extends Object> extends ListeJava<E> {
    
    private final int TAILLE_INITIALE = 1; // Pour tester agrandir

    private E[] grosTableau;
    private int indiceDernierElement = -1;
    
    @Override
    public void add(E e) {
        if(indiceDernierElement == grosTableau.length - 1) {
            agrandir();
        }
        
        indiceDernierElement++;

        grosTableau[indiceDernierElement] = e;
    }
