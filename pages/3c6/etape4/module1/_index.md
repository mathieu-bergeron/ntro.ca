---
title: "Module 4.1: liste naïve"
weight: 30
draft: false
---

{{% pageTitle %}}

1. {{% link "/3c6/etape4/module1/theorie/" "Théorie" %}}

1. {{% link "/3c6/etape4/module1/entrevue/" "Entrevue 4.1" %}}

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=332345" target="_blank">Mini-test théorique</a>

1. {{% link "/3c6/etape4/module1/atelier" "Atelier 4.1" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}

