public class MonAtelier4_1 extends Atelier4_1 {
    
    public static void main(String[] args) {
        
        (new MonAtelier4_1()).valider();
    }

    @Override
    public ListeJava<Character> fournirListeNaive() {
        return new ListeNaive<Character>(Character.class);
    }

    @Override
    public TesteurDeListe fournirTesteurDeListeNaive() {
        return new TesteurDeListeNaive();
    }
}
