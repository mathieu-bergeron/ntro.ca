public class TesteurDeListeAbstrait implements TesteurDeListe {
    
    private static Random alea = new Random();

    @Override
    public ListeJava<Character> melangerLaListe(ListeJava<Character> liste) {
        ListeJava<Character> resultat = nouvelleListe();

        // TODO

        return resultat;
    }

    @Override
    public ListeJava<Character> melangerLaListeEfficace(ListeJava<Character> liste) {
        ListeJava<Character> resultat = nouvelleListe();

        // TODO

        return resultat;
    }

    /* TODO: des méthodes pour tester l'efficacité de l'implantation
     *
     * Par exemple:
     *
     * @Override
     * public void fairePlusieursAjouts(ListeJava<Character> liste, int nombreOperations) {
     * 
     *       faire 'nombreOperations' appels à liste.add('x')
     *
     * }
     *
     */

}
