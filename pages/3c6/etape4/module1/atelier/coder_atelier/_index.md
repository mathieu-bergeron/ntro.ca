---
title: "Atelier 4.1: implanter une liste naïve et une liste par tableau"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer la classe `ListeNaive`

* Ajouter la classe suivante au **paquet** `atelier4_1`
    * Nom de la classe: `ListeNaive`

* Débuter l'implantation comme suit:

```java
{{% embed "ListeNaive.java" %}}
```

* Utiliser {{% key "Ctrl+1" %}} pour générer les méthodes d'une liste

* **TRUC**: compléter l'implantation de la liste naïve une fois l'outil de validation configuré

## Créer la classe `TesteurDeListeAbstrait`

* Ajouter la classe suivante au **paquet** `atelier4_1`
    * Nom de la classe: `TesteurDeListeAbstrait`

* Débuter l'implantation comme suit:

```java
{{% embed "TesteurDeListeAbstrait.java" %}}
```

* Utiliser {{% key "Ctrl+1" %}} pour générer les méthodes du testeur

* **TRUC**: mettre la classe `abstract` une fois les méthodes générées (cette classe n'est pas instanciée)

* **TRUC**: compléter l'implantation de la classe fois l'outil de validation configuré

## Créer la classe `TesteurDeListeNaive`

* Ajouter la classe suivante au **paquet** `atelier4_1`
    * Nom de la classe: `TesteurDeListeNaive`

* Implanter la classe comme suit:

```java
{{% embed "TesteurDeListeNaive.java" %}}
```

## Créer la classe `MonAtelier4_1`

* Ajouter la classe suivante au **paquet** `atelier4_1`
    * Nom de la classe: `MonAtelier4_1`

* Implanter la classe comme suit:

```java
{{% embed "MonAtelier4_1.java" %}}
```

## Compléter les implantations

* Utiliser l'outil de validation pour tester votre code

<img src="validation.png"/>

* vous pouvez aussi afficher le fichier `html`:

    * `ListeNaive.html`
    * (à la racine du projet)

## Tester l'efficacité du code

* Les méthodes de votre `TesteurDeListeAbstrait` seront appelées
    * typiquement, utiliser une boucle pour effectuer une opération un certain nombre de fois

* Vous devriez avoir des résultats similaires aux résultats ci-bas

    * `melangerLaListe`

    <img src="melangerLaListe.png"/>

    * `melangerLaListeEfficace`

    <img src="melangerLaListeEfficace.png"/>

    * `fairePlusieursModificationsAleatoires`

    <img src="fairePlusieursModificationsAleatoires.png"/>

    * `fairePlusieursAjouts`

    <img src="fairePlusieursAjouts.png"/>

    * `fairePlusieursRetraitsALaFin`

    <img src="fairePlusieursRetraitsALaFin.png"/>

    * `fairePlusieursRetraitsAuDebut`

    <img src="fairePlusieursRetraitsAuDebut.png"/>


    * `fairePlusieursInsertionsAuDebut`

    <img src="fairePlusieursInsertionsAuDebut.png"/>

    * `fairePlusieursInsertionsAleatoires`

    <img src="fairePlusieursInsertionsAleatoires.png"/>


