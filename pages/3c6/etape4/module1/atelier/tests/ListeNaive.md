---
---
<div style="max-width:80%;margin:auto;"><h1>test00</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeNaive_test00_avant.png'/></td><td>liste.add(v);</td><td>[v]<br><img src='../graphs/ListeNaive_test00_ok.png'/></td></tr></table><h1>test01</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v]<br><img src='../graphs/ListeNaive_test01_avant.png'/></td><td>liste.add(f);</td><td>[v, f]<br><img src='../graphs/ListeNaive_test01_ok.png'/></td></tr></table><h1>test02</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f]<br><img src='../graphs/ListeNaive_test02_avant.png'/></td><td>liste.add(d);</td><td>[v, f, d]<br><img src='../graphs/ListeNaive_test02_ok.png'/></td></tr></table><h1>test03</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d]<br><img src='../graphs/ListeNaive_test03_avant.png'/></td><td>liste.add(l);</td><td>[v, f, d, l]<br><img src='../graphs/ListeNaive_test03_ok.png'/></td></tr></table><h1>test04</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l]<br><img src='../graphs/ListeNaive_test04_avant.png'/></td><td>liste.add(m);</td><td>[v, f, d, l, m]<br><img src='../graphs/ListeNaive_test04_ok.png'/></td></tr></table><h1>test05</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m]<br><img src='../graphs/ListeNaive_test05_avant.png'/></td><td>liste.add(m);</td><td>[v, f, d, l, m, m]<br><img src='../graphs/ListeNaive_test05_ok.png'/></td></tr></table><h1>test06</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m]<br><img src='../graphs/ListeNaive_test06_avant.png'/></td><td>liste.add(o);</td><td>[v, f, d, l, m, m, o]<br><img src='../graphs/ListeNaive_test06_ok.png'/></td></tr></table><h1>test07</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m, o]<br><img src='../graphs/ListeNaive_test07_avant.png'/></td><td>liste.add(l);</td><td>[v, f, d, l, m, m, o, l]<br><img src='../graphs/ListeNaive_test07_ok.png'/></td></tr></table><h1>test08</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m, o, l]<br><img src='../graphs/ListeNaive_test08_avant.png'/></td><td>liste.add(b);</td><td>[v, f, d, l, m, m, o, l, b]<br><img src='../graphs/ListeNaive_test08_ok.png'/></td></tr></table><h1>test09</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m, o, l, b]<br><img src='../graphs/ListeNaive_test09_avant.png'/></td><td>liste.add(s);</td><td>[v, f, d, l, m, m, o, l, b, s]<br><img src='../graphs/ListeNaive_test09_ok.png'/></td></tr></table><h1>test10</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m, o, l, b, s]<br><img src='../graphs/ListeNaive_test10_avant.png'/></td><td>liste.add(r);</td><td>[v, f, d, l, m, m, o, l, b, s, r]<br><img src='../graphs/ListeNaive_test10_ok.png'/></td></tr></table><h1>test11</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m, o, l, b, s, r]<br><img src='../graphs/ListeNaive_test11_avant.png'/></td><td>liste.add(w);</td><td>[v, f, d, l, m, m, o, l, b, s, r, w]<br><img src='../graphs/ListeNaive_test11_ok.png'/></td></tr></table><h1>test12</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m, o, l, b, s, r, w]<br><img src='../graphs/ListeNaive_test12_avant.png'/></td><td>liste.add(m);</td><td>[v, f, d, l, m, m, o, l, b, s, r, w, m]<br><img src='../graphs/ListeNaive_test12_ok.png'/></td></tr></table><h1>test13</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m, o, l, b, s, r, w, m]<br><img src='../graphs/ListeNaive_test13_avant.png'/></td><td>liste.add(u);</td><td>[v, f, d, l, m, m, o, l, b, s, r, w, m, u]<br><img src='../graphs/ListeNaive_test13_ok.png'/></td></tr></table><h1>test14</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m, o, l, b, s, r, w, m, u]<br><img src='../graphs/ListeNaive_test14_avant.png'/></td><td>liste.add(m);</td><td>[v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test14_ok.png'/></td></tr></table><h1>test15</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeNaive_test15_avant.png'/></td><td>liste.addAll([]);</td><td>[]<br><img src='../graphs/ListeNaive_test15_ok.png'/></td></tr></table><h1>test16</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeNaive_test16_avant.png'/></td><td>liste.addAll([a, b, c, d, e, f, g]);</td><td>[a, b, c, d, e, f, g]<br><img src='../graphs/ListeNaive_test16_ok.png'/></td></tr></table><h1>test17</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeNaive_test17_avant.png'/></td><td>liste.insert(0,u);</td><td>[u]<br><img src='../graphs/ListeNaive_test17_ok.png'/></td></tr></table><h1>test18</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[u]<br><img src='../graphs/ListeNaive_test18_avant.png'/></td><td>liste.insert(0,z);</td><td>[z, u]<br><img src='../graphs/ListeNaive_test18_ok.png'/></td></tr></table><h1>test19</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test19_avant.png'/></td><td>liste.insert(0,c);</td><td>[c, v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test19_ok.png'/></td></tr></table><h1>test20</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, f, g]<br><img src='../graphs/ListeNaive_test20_avant.png'/></td><td>liste.insert(5,m);</td><td>[a, b, c, d, e, m, f, g]<br><img src='../graphs/ListeNaive_test20_ok.png'/></td></tr></table><h1>test21</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[z, u]<br><img src='../graphs/ListeNaive_test21_avant.png'/></td><td>liste.set(1,o);</td><td>[z, o]<br><img src='../graphs/ListeNaive_test21_ok.png'/></td></tr></table><h1>test22</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[c, v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test22_avant.png'/></td><td>liste.set(0,o);</td><td>[o, v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test22_ok.png'/></td></tr></table><h1>test23</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[z, o]<br><img src='../graphs/ListeNaive_test23_avant.png'/></td><td>liste.set(0,x);</td><td>[x, o]<br><img src='../graphs/ListeNaive_test23_ok.png'/></td></tr></table><h1>test24</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[x, o]<br><img src='../graphs/ListeNaive_test24_avant.png'/></td><td>liste.get(0);</td><td>x</td></table><h1>test25</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[o, v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test25_avant.png'/></td><td>liste.get(4);</td><td>l</td></table><h1>test26</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[o, v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test26_avant.png'/></td><td>liste.get(2);</td><td>f</td></table><h1>test27</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='../graphs/ListeNaive_test27_avant.png'/></td><td>liste.size();</td><td>0</td></table><h1>test28</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[o, v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test28_avant.png'/></td><td>liste.size();</td><td>16</td></table><h1>test29</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[x, o]<br><img src='../graphs/ListeNaive_test29_avant.png'/></td><td>liste.size();</td><td>2</td></table><h1>test30</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='../graphs/ListeNaive_test30_avant.png'/></td><td>liste.isEmpty();</td><td>true</td></table><h1>test31</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[x, o]<br><img src='../graphs/ListeNaive_test31_avant.png'/></td><td>liste.isEmpty();</td><td>false</td></table><h1>test32</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[a, b, c, d, e, m, f, g]<br><img src='../graphs/ListeNaive_test32_avant.png'/></td><td>liste.isEmpty();</td><td>false</td></table><h1>test33</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='../graphs/ListeNaive_test33_avant.png'/></td><td>liste.contains(l);</td><td>false</td></table><h1>test34</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[x, o]<br><img src='../graphs/ListeNaive_test34_avant.png'/></td><td>liste.contains(p);</td><td>false</td></table><h1>test35</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[o, v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test35_avant.png'/></td><td>liste.contains(d);</td><td>true</td></table><h1>test36</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='../graphs/ListeNaive_test36_avant.png'/></td><td>liste.indexOf(s);</td><td>-1</td></table><h1>test37</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[a, b, c, d, e, m, f, g]<br><img src='../graphs/ListeNaive_test37_avant.png'/></td><td>liste.indexOf(s);</td><td>-1</td></table><h1>test38</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[o, v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test38_avant.png'/></td><td>liste.indexOf(m);</td><td>5</td></table><h1>test39</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeNaive_test39_avant.png'/></td><td>liste.removeValue(w);</td><td>[]<br><img src='../graphs/ListeNaive_test39_ok.png'/></td></tr></table><h1>test40</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[x, o]<br><img src='../graphs/ListeNaive_test40_avant.png'/></td><td>liste.removeValue(k);</td><td>[x, o]<br><img src='../graphs/ListeNaive_test40_ok.png'/></td></tr></table><h1>test41</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[o, v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test41_avant.png'/></td><td>liste.removeValue(o);</td><td>[v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test41_ok.png'/></td></tr></table><h1>test42</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[x, o]<br><img src='../graphs/ListeNaive_test42_avant.png'/></td><td>liste.removeValue(o);</td><td>[x]<br><img src='../graphs/ListeNaive_test42_ok.png'/></td></tr></table><h1>test43</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, m, f, g]<br><img src='../graphs/ListeNaive_test43_avant.png'/></td><td>liste.removeValue(f);</td><td>[a, b, c, d, e, m, g]<br><img src='../graphs/ListeNaive_test43_ok.png'/></td></tr></table><h1>test44</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[v, f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test44_avant.png'/></td><td>liste.remove(0);</td><td>[f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test44_ok.png'/></td></tr></table><h1>test45</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, m, g]<br><img src='../graphs/ListeNaive_test45_avant.png'/></td><td>liste.remove(6);</td><td>[a, b, c, d, e, m]<br><img src='../graphs/ListeNaive_test45_ok.png'/></td></tr></table><h1>test46</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, m]<br><img src='../graphs/ListeNaive_test46_avant.png'/></td><td>liste.remove(5);</td><td>[a, b, c, d, e]<br><img src='../graphs/ListeNaive_test46_ok.png'/></td></tr></table><h1>test47</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e]<br><img src='../graphs/ListeNaive_test47_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='../graphs/ListeNaive_test47_ok.png'/></td></tr></table><h1>test48</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[x]<br><img src='../graphs/ListeNaive_test48_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='../graphs/ListeNaive_test48_ok.png'/></td></tr></table><h1>test49</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[f, d, l, m, m, o, l, b, s, r, w, m, u, m]<br><img src='../graphs/ListeNaive_test49_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='../graphs/ListeNaive_test49_ok.png'/></td></tr></table></div>
