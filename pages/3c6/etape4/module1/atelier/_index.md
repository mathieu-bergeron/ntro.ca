---
title: "Atelier 4.1: implanter une liste naïve"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Objectif


* {{% link "/3c6/etape4/module1/atelier/coder_atelier" "Implanter et valider une liste naïve" %}}

## Exemples de tests réussis

* <a href="tests/ListeNaive"><code>ListeNaive</code></a>



## Remises

* {{% link "/3c6/etape4/module1/atelier/remise_gitlab" "Remise via GitLab" %}}

* {{% link "/3c6/etape4/module1/atelier/remise_moodle" "Remise vis Moodle" %}}

