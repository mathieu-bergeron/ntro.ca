---
title: "Entrevue 4.1: liste naïve"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier 

## Ajouter un élément à une liste naïve (1)

* Imaginer la liste naïve suivante:

<img src="ListeNaive_test03_avant.png" />

* Dessiner la même liste, mais **après** l'ajout de la valeur `l`

* Expliquer votre raisonnement


## Ajouter un élément à une liste naïve (2)

* Imaginer la liste naïve suivante:

* Dessiner la même liste, mais **après** l'ajout de la valeur `k`

* Expliquer votre raisonnement

<img src="ListeNaive_test04_avant.png" />





