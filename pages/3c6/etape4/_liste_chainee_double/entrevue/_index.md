---
title: "Entrevue 4.3: liste chaînée double"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier 

## Ajouter un élément à une liste chaînée double

* Imaginer la liste chaîne double suivante:

    <img src="avant01.png" />

    * **ATTENTION** 
        * le graphe est généré, les éléments ne sont pas nécessairement affichés dans l'ordre
        * il faut suivre les références `precedent` et `suivant` pour comprendre l'ordre

* Dessiner la même liste, mais **après** l'appel à `add('x')`

* Expliquer votre raisonnement

## Insérer un élément au début d'une liste chaînée double

* Imaginer la liste chaînée double suivante:

    <img src="avant02.png" />

* Dessiner la même liste, mais **après** l'appel à `insert(0, 's')`

* Expliquer votre raisonnement


