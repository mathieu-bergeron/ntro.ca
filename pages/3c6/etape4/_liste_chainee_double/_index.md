---
title: "Ancien module 4.3: liste chaînée double"
weight: 30
draft: true
bookHidden: true
---

{{% pageTitle %}}

1. {{% link "/3c6/etape4/module3/theorie/" "Théorie" %}}

1. {{% link "/3c6/etape4/module3/entrevue/" "Entrevue 4.3" %}}

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=332680" target="_blank">Mini-test théorique</a>

1. {{% link "/3c6/etape4/module3/atelier" "Atelier 4.3" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}
