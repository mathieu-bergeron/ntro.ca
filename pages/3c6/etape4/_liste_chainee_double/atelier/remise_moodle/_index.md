---
title: "Atelier4.3: remise sur Moodle"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
NOTE:

* je peux refaire la remise une deuxième fois si j'ai modifié mon code après la première remise
{{</excerpt>}}

1. Naviguer jusqu'au répertoire contenant mon dépôt Git

1. Créer une archive `.zip` avec **uniquement** les fichiers du répertoire `atelier4_1`

    * Clic-droit sur le dossier => *Envoyer vers* => *dossier compressé*

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=332681">Remettre cette archive sur Moodle</a>
