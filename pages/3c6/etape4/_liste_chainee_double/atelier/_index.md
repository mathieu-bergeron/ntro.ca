---
title: "Atelier 4.3: implanter une liste chaînée double"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Objectif


* {{% link "/3c6/etape4/module3/atelier/coder_atelier" "Implanter et valider une liste chaînée double" %}}

## Exemples de tests réussis

* <a href="tests/ListeChaineeDouble.html" target="_blank"><code>ListeChaineeDouble</code></a>


## Remises

* {{% link "/3c6/etape4/module3/atelier/remise_gitlab" "Remise via GitLab" %}}

* {{% link "/3c6/etape4/module3/atelier/remise_moodle" "Remise vis Moodle" %}}

