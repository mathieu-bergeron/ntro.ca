package atelier4_3;

import tutoriels.liste.Liste;
import tutoriels.liste.TesteurDeListe;

public class MonAtelier4_3 extends Atelier4_3 {
	
	public static void main(String[] args) {
		
		(new MonAtelier4_3()).valider();
	}


	@Override
	public Liste<Character> fournirListeTableau() {
		return new ListeTableau<Character>(Character.class);
	}


	@Override
	public Liste<Character> fournirListeChaineeSimple() {
		return new ListeChaineeSimple<Character>(Character.class);
	}

	@Override
	public Liste<Character> fournirListeChaineeDouble() {
		return new ListeChaineeDouble<Character>(Character.class);
	}

	@Override
	public TesteurDeListe fournirTesteurDeListeTableau() {
		return new TesteurDeListeTableau();
	}

	@Override
	public TesteurDeListe fournirTesteurDeListeSimple() {
		return new TesteurDeListeChaineeSimple();
	}

	@Override
	public TesteurDeListe fournirTesteurDeListeDouble() {
		return new TesteurDeListeChaineeDouble();
	}

}
