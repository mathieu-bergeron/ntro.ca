package atelier4_3;


import tutoriels.liste.Liste;

public class TesteurDeListeChaineeSimple extends TesteurDeListeAbstrait {

	@Override
	public Liste<Character> nouvelleListe() {
		return new ListeChaineeSimple<>(Character.class);
	}




}
