package atelier4_3;

import java.util.Random;

import tutoriels.liste.Liste;
import tutoriels.liste.TesteurDeListe;

public abstract class TesteurDeListeAbstrait implements TesteurDeListe {
    
    private static Random alea = new Random();

    @Override
    public void fairePlusieursAjouts(Liste<Character> liste, int nombreOperations) {
        for(int i = 0; i < nombreOperations; i++) {
            liste.add('a');
        }
    }

    @Override
    public void fairePlusieursInsertionsAuDebut(Liste<Character> liste, int nombreOperations) {
        for(int i = 0; i < nombreOperations; i++) {
            liste.insert(0, 'a');
        }
    }

    @Override
    public void fairePlusieursInsertionsAleatoires(Liste<Character> liste, int nombreOperations) {
        liste.insert(0, 'a');

        for(int i = 1; i < nombreOperations; i++) {
            liste.insert(alea.nextInt(liste.size()), 'a');
        }
    }

    
    @Override
    public void fairePlusieursModificationsAleatoires(Liste<Character> liste, int nombreOperations) {
        for(int i = 0; i < nombreOperations; i++) {
            liste.set(alea.nextInt(liste.size()), 'a');
        }
    }

    @Override
    public void fairePlusieursRetraitsAleatoires(Liste<Character> liste, int nombreOperations) {
        for(int i = 0; i < nombreOperations; i++) {
            liste.remove(alea.nextInt(liste.size()));
        }
    }

    @Override
    public void fairePlusieursRetraitsAuDebut(Liste<Character> liste, int nombreOperations) {
        for(int i = 0; i < nombreOperations; i++) {
            liste.remove(0);
        }
    }

    @Override
    public void fairePlusieursRetraitsALaFin(Liste<Character> liste, int nombreOperations) {
        for(int i = 0; i < nombreOperations; i++) {
            liste.remove(liste.size()-1);
        }
    }

    @Override
    public Liste<Character> melangerLaListe(Liste<Character> liste) {
        Liste<Character> resultat = nouvelleListe();
        
        while(!liste.isEmpty()) {
            
            int positionAuHasard = alea.nextInt(liste.size());
            
            Character elementAuHasard = liste.get(positionAuHasard);
            
            resultat.add(elementAuHasard);
            
            liste.remove(positionAuHasard);
        }

        return resultat;
    }

    @Override
    public Liste<Character> melangerLaListeEfficace(Liste<Character> liste) {
        Liste<Character> resultat = nouvelleListe();

        int indiceMin = 0;
        
        while(resultat.size() < liste.size()) {
            
            int positionAuHasard = indiceMin + alea.nextInt(liste.size() - indiceMin);

            Character elementAuHasard = liste.get(positionAuHasard);
            resultat.add(elementAuHasard);

            Character remplacement = liste.get(indiceMin);
            
            liste.set(positionAuHasard, remplacement);
        }

        return resultat;
    }



}
