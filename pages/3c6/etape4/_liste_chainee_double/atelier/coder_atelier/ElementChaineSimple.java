package atelier4_3;

public class ElementChaineSimple<E> {
	
	private E valeur;

	private ElementChaineSimple<E> suivant = null;

	public E getValeur() {
		return valeur;
	}

	public void setValeur(E valeur) {
		this.valeur = valeur;
	}

	public ElementChaineSimple<E> getSuivant() {
		return suivant;
	}

	public ElementChaineSimple(E e) {
		this.valeur = e;
	}

	public ElementChaineSimple<E> suivant() {
		return suivant;
	}

	public E valeur() {
		return valeur;
	}
	
	public void set(E e) {
		this.valeur = e;
	}
	
	public void setSuivant(ElementChaineSimple<E> suivant) {
		this.suivant = suivant;
	}
	
	public void insererApres(E e) {
		
		ElementChaineSimple<E> nouveau = new ElementChaineSimple<E>(e);
		
		if(suivant != null) {
			nouveau.setSuivant(suivant);
		}

		suivant = nouveau;
	}
}
