public class TesteurDeListeChaineeDouble extends TesteurDeListeAbstrait {

	@Override
	public Liste<Character> nouvelleListe() {
		return new ListeChaineeDouble<>(Character.class);
	}
}
