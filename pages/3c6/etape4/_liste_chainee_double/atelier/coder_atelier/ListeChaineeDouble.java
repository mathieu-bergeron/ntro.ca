public class ListeChaineeDouble<E extends Object> extends Liste<E> implements Serializable {

	private int taille = 0;
	private ElementChaineDouble<E> tete = null;

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	public ElementChaineDouble<E> getTete() {
		return tete;
	}

	public void setTete(ElementChaineDouble<E> tete) {
		this.tete = tete;
	}

	public ListeChaineeDouble(Class<E> typeElement) {
		super(typeElement);
	}

	// TODO
}
