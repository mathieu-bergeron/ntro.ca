---
title: "Atelier 4.3: implanter une liste chaînée double"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer la classe `ElementChaineDouble`

* Ajouter la classe suivante au **paquet** `atelier4_3`
    * Nom de la classe: `ElementChaineDouble`

* Débuter l'implantation comme suit:

```java
{{% embed "ElementChaineDouble.java" %}}
```

* **TRUC**: compléter l'implantation de l'élément une fois l'outil de validation configuré

## Créer la classe `ListeChaineeDouble`

* Ajouter la classe suivante au **paquet** `atelier4_3`
    * Nom de la classe: `ListeChaineeDouble`

* Débuter l'implantation comme suit:

```java
{{% embed "ListeChaineeDouble.java" %}}
```

* Utiliser {{% key "Ctrl+1" %}} pour générer les méthodes d'une liste

* **TRUC**: compléter l'implantation de la liste une fois l'outil de validation configuré

## Recopier certaines classe de l'`atelier4_2`

* À partir de l'`atelier4_2`, recopier 
	* `ListeTableau`
	* `ListeChaineeSimple`
	* `ElementChaineSimple`
	* `TesteurDeListeAbstrait`
	* `TesteurDeListeTableau`
	* `TesteurDeListeChaineeSimple`

## Créer la classe `TesteurDeListeChaineeDouble`

* Ajouter la classe suivante au **paquet** `atelier4_3`
    * Nom de la classe: `TesteurDeListeChaineeDouble`

* Implanter la classe comme suit:

```java
{{% embed "TesteurDeListeChaineeDouble.java" %}}
```

## Créer la classe `MonAtelier4_3`

* Ajouter la classe suivante au **paquet** `atelier4_3`
    * Nom de la classe: `MonAtelier4_3`

* Implanter la classe comme suit:

```java
{{% embed "MonAtelier4_3.java" %}}
```

## Compléter les implantations

* Utiliser l'outil de validation pour tester votre code

<img src="validation.png"/>

* vous pouvez aussi afficher le fichier `html` suivant:

    * `ListeChaineeSimple.html`
    * (à la racine du projet)

## Tester l'efficacité du code

* Les méthodes de votre `TesteurDeListeAbstrait` seront appelées
    * typiquement, utiliser une boucle pour effectuer une opération un certain nombre de fois

* Vous devriez avoir des résultats similaires aux résultats ci-bas

    * `melangerLaListe`

    <img src="melangerLaListe.png"/>

    * `melangerLaListeEfficace`

    <img src="melangerLaListeEfficace.png"/>

    * `fairePlusieursModificationsAleatoires`

    <img src="fairePlusieursModificationsAleatoires.png"/>

    * `fairePlusieursAjouts`

    <img src="fairePlusieursAjouts.png"/>

    * `fairePlusieursRetraitsALaFin`

    <img src="fairePlusieursRetraitsALaFin.png"/>

    * `fairePlusieursRetraitsAuDebut`

    <img src="fairePlusieursRetraitsAuDebut.png"/>


    * `fairePlusieursInsertionsAuDebut`

    <img src="fairePlusieursInsertionsAuDebut.png"/>

    * `fairePlusieursInsertionsAleatoires`

    <img src="fairePlusieursInsertionsAleatoires.png"/>

