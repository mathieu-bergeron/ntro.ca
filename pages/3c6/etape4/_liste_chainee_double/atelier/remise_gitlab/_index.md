---
title: "Atelier4.3: remise sur GitLab"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
NOTE:

* je peux refaire la remise une deuxième fois si j'ai modifié mon code après la première remise
{{</excerpt>}}

1. Naviguer jusqu'à **la racine** de mon dépôt Git

1. Ouvrir GitBash dans ce répertoire

    * *Windows 10* : Clic-droit => *Git Bash Here*
    * *Windows 11* : Clic-droit => *Show more options* => *Git Bash Here*


1. En GitBash, s'assurer d'ajouter tous les nouveaux fichiers

    ```bash
    $ git add .
    ```

1. Faire un commit avec le commentaire `atelier4_3`

    ```bash
    $ git commit -m"atelier4_3"
    ```

    * au besoin, utiliser l'option `--allow-empty`

        ```bash
        $ git commit -m"atelier4_3" --allow-empty 
        ```

1. Pousser le commit sur GitLab

    ```bash
    $ git push
    ```

1. Se connecter à <a href="https://gitlab.com" target="_blank">GitLab</a> et vérifier que le commit est sur le serveur



