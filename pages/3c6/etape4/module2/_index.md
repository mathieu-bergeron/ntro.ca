---
title: "Module 4.2: liste par tableau"
weight: 30
draft: false
---

{{% pageTitle %}}

1. {{% link "/3c6/etape4/module2/theorie/" "Théorie" %}}

1. {{% link "/3c6/etape4/module2/entrevue/" "Entrevue 4.2" %}}

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=332670" target="_blank">Mini-test théorique</a>

1. {{% link "/3c6/etape4/module2/atelier" "Atelier 4.2" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}

