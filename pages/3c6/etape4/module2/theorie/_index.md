---
title: ""
weight: 1
bookHidden: true
---


# Théorie 4.2: liste avec tableau

## Liste par tableau plus efficace

<center>
<video width="50%" src="04.mp4" type="video/mp4" controls playsinline>
</center>

* Si on veut être plus efficace, il faut éviter de toujours recopier les éléments

* Une façon de faire est d'utiliser un tableau plus grand 


```java
{{% embed src="ListeTableau.java" first-line="1" last-line="6" %}}
```

* Quand on ajoute un élément, on peut simplement mémoriser que la liste grandit

```java
{{% embed src="ListeTableau.java" first-line="8" last-line="9" %}}
```

```java
{{% embed src="ListeTableau.java" first-line="14" last-line="16" %}}
```

* Évidemment, il faudra parfois faire grandir le tableau aussi:

```java
{{% embed src="ListeTableau.java" first-line="8" last-line="16" %}}
```

* Néanmoins, `add` est beaucoup plus efficace:

    <center>
    <img src="add.png" width="90%"/>
    </center>

* Ainsi que retirer à la fin:

    <center>
    <img src="remove_fin.png" width="90%"/>
    </center>

* C'est un exemple de compromis temps/espace mémoire 
    * en utilisant plus d'espace mémoire, on améliore le temps d'exécution

* Par contre, retirer au début est presqu'identique:

    <center>
    <img src="remove_debut.png" width="90%"/>
    </center>

## Exemples 

<center>
<video width="50%" src="exemple.mp4" type="video/mp4" controls playsinline>
</center>


### Ajouts à partir d'une liste vide

<table>
<tr>
<td>
<code>liste</code>
</td>
<td>
<code>[null,null,null,...,null]</code>
<br>
<code>indiceDernierElement == -1</code>
</td>
</tr>

<tr>
<td>
<code>liste.add('a')</code>
</td>
<td>
<code>[a,null,null,...,null]</code>
<br>
<code>indiceDernierElement == 0</code>
</td>
</tr>

<tr>
<td>
<code>liste.add('b')</code>
</td>
<td>
<code>[a,b,null,...,null]</code>
<br>
<code>indiceDernierElement == 1</code>
</td>
</tr>

</table>

### Un retrait au milieu

<table>

<tr>
<td>
<code>liste</code>
</td>
<td>
<code>[a,b,c,d,e,null,...,null]</code>
<br>
<code>indiceDernierElement == 4</code>
</td>
</tr>

<tr>
<td>
<code>liste.remove('c')</code>
</td>
<td>
<code>[a,b,<strong>d</strong>,d,e,null,...,null]</code>
<br>
<code>[a,b,d,<strong>e</strong>,e,null,...,null]</code>
<br>
<code>indiceDernierElement == 3</code>



</td>
</tr>


</table>


## Implanter un mélangeur efficace

<center>
<video width="50%" src="05.mp4" type="video/mp4" controls playsinline>
</center>

* Re-considérer notre mélangeur:

```java
{{% embed src="MelangeurNaif2.java" %}}
```

* L'efficacité du mélangeur est améliorée:

    <center>
    <img src="melangerNaif2.png" width="90%"/>
    </center>

* Mais on peut faire mieux!

* Comment modifier notre mélangeur pour ne pas utiliser `remove`?

* On pourrait alors avoir:

    <center>
    <img src="melangerEfficace.png" width="90%"/>
    </center>

* Pour écrire du code efficace, il faut:
    * savoir quelles opérations sont coûteuses sur notre structure de données
