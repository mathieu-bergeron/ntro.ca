---
title: "Entrevue 4.1: liste par tableau"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier 

## Ajouter un élément à une liste par tableau (1)

* Imaginer la liste par tableau suivante:

<img src="ListeTableau_test03_avant.png" />

* Dessiner la même liste, mais **après** l'ajout de la valeur `s`

* Expliquer votre raisonnement



## Ajouter un élément à une liste par tableau (2)

* Imaginer la liste par tableau suivante:

* Dessiner la même liste, mais **après** l'ajout de la valeur `k`

* Expliquer votre raisonnement

<img src="ListeTableau_test04_avant.png" />





