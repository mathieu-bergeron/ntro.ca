---
---
<div style="max-width:80%; margin:auto;"><h1>test00</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeTableau_test00_avant.png'/></td><td>liste.add(w);</td><td>[w]<br><img src='../graphs/ListeTableau_test00_ok.png'/></td></tr></table><h1>test01</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w]<br><img src='../graphs/ListeTableau_test01_avant.png'/></td><td>liste.add(y);</td><td>[w, y]<br><img src='../graphs/ListeTableau_test01_ok.png'/></td></tr></table><h1>test02</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y]<br><img src='../graphs/ListeTableau_test02_avant.png'/></td><td>liste.add(g);</td><td>[w, y, g]<br><img src='../graphs/ListeTableau_test02_ok.png'/></td></tr></table><h1>test03</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g]<br><img src='../graphs/ListeTableau_test03_avant.png'/></td><td>liste.add(r);</td><td>[w, y, g, r]<br><img src='../graphs/ListeTableau_test03_ok.png'/></td></tr></table><h1>test04</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r]<br><img src='../graphs/ListeTableau_test04_avant.png'/></td><td>liste.add(s);</td><td>[w, y, g, r, s]<br><img src='../graphs/ListeTableau_test04_ok.png'/></td></tr></table><h1>test05</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s]<br><img src='../graphs/ListeTableau_test05_avant.png'/></td><td>liste.add(i);</td><td>[w, y, g, r, s, i]<br><img src='../graphs/ListeTableau_test05_ok.png'/></td></tr></table><h1>test06</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s, i]<br><img src='../graphs/ListeTableau_test06_avant.png'/></td><td>liste.add(o);</td><td>[w, y, g, r, s, i, o]<br><img src='../graphs/ListeTableau_test06_ok.png'/></td></tr></table><h1>test07</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s, i, o]<br><img src='../graphs/ListeTableau_test07_avant.png'/></td><td>liste.add(d);</td><td>[w, y, g, r, s, i, o, d]<br><img src='../graphs/ListeTableau_test07_ok.png'/></td></tr></table><h1>test08</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s, i, o, d]<br><img src='../graphs/ListeTableau_test08_avant.png'/></td><td>liste.add(s);</td><td>[w, y, g, r, s, i, o, d, s]<br><img src='../graphs/ListeTableau_test08_ok.png'/></td></tr></table><h1>test09</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s, i, o, d, s]<br><img src='../graphs/ListeTableau_test09_avant.png'/></td><td>liste.add(f);</td><td>[w, y, g, r, s, i, o, d, s, f]<br><img src='../graphs/ListeTableau_test09_ok.png'/></td></tr></table><h1>test10</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s, i, o, d, s, f]<br><img src='../graphs/ListeTableau_test10_avant.png'/></td><td>liste.add(h);</td><td>[w, y, g, r, s, i, o, d, s, f, h]<br><img src='../graphs/ListeTableau_test10_ok.png'/></td></tr></table><h1>test11</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s, i, o, d, s, f, h]<br><img src='../graphs/ListeTableau_test11_avant.png'/></td><td>liste.add(w);</td><td>[w, y, g, r, s, i, o, d, s, f, h, w]<br><img src='../graphs/ListeTableau_test11_ok.png'/></td></tr></table><h1>test12</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s, i, o, d, s, f, h, w]<br><img src='../graphs/ListeTableau_test12_avant.png'/></td><td>liste.add(z);</td><td>[w, y, g, r, s, i, o, d, s, f, h, w, z]<br><img src='../graphs/ListeTableau_test12_ok.png'/></td></tr></table><h1>test13</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s, i, o, d, s, f, h, w, z]<br><img src='../graphs/ListeTableau_test13_avant.png'/></td><td>liste.add(y);</td><td>[w, y, g, r, s, i, o, d, s, f, h, w, z, y]<br><img src='../graphs/ListeTableau_test13_ok.png'/></td></tr></table><h1>test14</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s, i, o, d, s, f, h, w, z, y]<br><img src='../graphs/ListeTableau_test14_avant.png'/></td><td>liste.add(v);</td><td>[w, y, g, r, s, i, o, d, s, f, h, w, z, y, v]<br><img src='../graphs/ListeTableau_test14_ok.png'/></td></tr></table><h1>test15</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeTableau_test15_avant.png'/></td><td>liste.addAll([]);</td><td>[]<br><img src='../graphs/ListeTableau_test15_ok.png'/></td></tr></table><h1>test16</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeTableau_test16_avant.png'/></td><td>liste.addAll([a, b, c, d, e, f, g]);</td><td>[a, b, c, d, e, f, g]<br><img src='../graphs/ListeTableau_test16_ok.png'/></td></tr></table><h1>test17</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeTableau_test17_avant.png'/></td><td>liste.insert(0,d);</td><td>[d]<br><img src='../graphs/ListeTableau_test17_ok.png'/></td></tr></table><h1>test18</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, f, g]<br><img src='../graphs/ListeTableau_test18_avant.png'/></td><td>liste.insert(6,x);</td><td>[a, b, c, d, e, f, x, g]<br><img src='../graphs/ListeTableau_test18_ok.png'/></td></tr></table><h1>test19</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, f, x, g]<br><img src='../graphs/ListeTableau_test19_avant.png'/></td><td>liste.insert(0,y);</td><td>[y, a, b, c, d, e, f, x, g]<br><img src='../graphs/ListeTableau_test19_ok.png'/></td></tr></table><h1>test20</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[d]<br><img src='../graphs/ListeTableau_test20_avant.png'/></td><td>liste.insert(1,g);</td><td>[d, g]<br><img src='../graphs/ListeTableau_test20_ok.png'/></td></tr></table><h1>test21</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[d, g]<br><img src='../graphs/ListeTableau_test21_avant.png'/></td><td>liste.set(1,r);</td><td>[d, r]<br><img src='../graphs/ListeTableau_test21_ok.png'/></td></tr></table><h1>test22</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[w, y, g, r, s, i, o, d, s, f, h, w, z, y, v]<br><img src='../graphs/ListeTableau_test22_avant.png'/></td><td>liste.set(0,r);</td><td>[r, y, g, r, s, i, o, d, s, f, h, w, z, y, v]<br><img src='../graphs/ListeTableau_test22_ok.png'/></td></tr></table><h1>test23</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[d, r]<br><img src='../graphs/ListeTableau_test23_avant.png'/></td><td>liste.set(0,p);</td><td>[p, r]<br><img src='../graphs/ListeTableau_test23_ok.png'/></td></tr></table><h1>test24</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[p, r]<br><img src='../graphs/ListeTableau_test24_avant.png'/></td><td>liste.get(1);</td><td>r</td></table><h1>test25</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[p, r]<br><img src='../graphs/ListeTableau_test25_avant.png'/></td><td>liste.get(1);</td><td>r</td></table><h1>test26</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[y, a, b, c, d, e, f, x, g]<br><img src='../graphs/ListeTableau_test26_avant.png'/></td><td>liste.get(1);</td><td>a</td></table><h1>test27</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='../graphs/ListeTableau_test27_avant.png'/></td><td>liste.size();</td><td>0</td></table><h1>test28</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[r, y, g, r, s, i, o, d, s, f, h, w, z, y, v]<br><img src='../graphs/ListeTableau_test28_avant.png'/></td><td>liste.size();</td><td>15</td></table><h1>test29</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[y, a, b, c, d, e, f, x, g]<br><img src='../graphs/ListeTableau_test29_avant.png'/></td><td>liste.size();</td><td>9</td></table><h1>test30</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='../graphs/ListeTableau_test30_avant.png'/></td><td>liste.isEmpty();</td><td>true</td></table><h1>test31</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[p, r]<br><img src='../graphs/ListeTableau_test31_avant.png'/></td><td>liste.isEmpty();</td><td>false</td></table><h1>test32</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[p, r]<br><img src='../graphs/ListeTableau_test32_avant.png'/></td><td>liste.isEmpty();</td><td>false</td></table><h1>test33</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='../graphs/ListeTableau_test33_avant.png'/></td><td>liste.contains(v);</td><td>false</td></table><h1>test34</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[r, y, g, r, s, i, o, d, s, f, h, w, z, y, v]<br><img src='../graphs/ListeTableau_test34_avant.png'/></td><td>liste.contains(u);</td><td>false</td></table><h1>test35</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[p, r]<br><img src='../graphs/ListeTableau_test35_avant.png'/></td><td>liste.contains(r);</td><td>true</td></table><h1>test36</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[]<br><img src='../graphs/ListeTableau_test36_avant.png'/></td><td>liste.indexOf(a);</td><td>-1</td></table><h1>test37</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[p, r]<br><img src='../graphs/ListeTableau_test37_avant.png'/></td><td>liste.indexOf(w);</td><td>-1</td></table><h1>test38</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>[y, a, b, c, d, e, f, x, g]<br><img src='../graphs/ListeTableau_test38_avant.png'/></td><td>liste.indexOf(a);</td><td>1</td></table><h1>test39</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeTableau_test39_avant.png'/></td><td>liste.removeValue(s);</td><td>[]<br><img src='../graphs/ListeTableau_test39_ok.png'/></td></tr></table><h1>test40</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[r, y, g, r, s, i, o, d, s, f, h, w, z, y, v]<br><img src='../graphs/ListeTableau_test40_avant.png'/></td><td>liste.removeValue(z);</td><td>[r, y, g, r, s, i, o, d, s, f, h, w, y, v]<br><img src='../graphs/ListeTableau_test40_ok.png'/></td></tr></table><h1>test41</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[p, r]<br><img src='../graphs/ListeTableau_test41_avant.png'/></td><td>liste.removeValue(p);</td><td>[r]<br><img src='../graphs/ListeTableau_test41_ok.png'/></td></tr></table><h1>test42</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[r]<br><img src='../graphs/ListeTableau_test42_avant.png'/></td><td>liste.removeValue(r);</td><td>[]<br><img src='../graphs/ListeTableau_test42_ok.png'/></td></tr></table><h1>test43</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[y, a, b, c, d, e, f, x, g]<br><img src='../graphs/ListeTableau_test43_avant.png'/></td><td>liste.remove(0);</td><td>[a, b, c, d, e, f, x, g]<br><img src='../graphs/ListeTableau_test43_ok.png'/></td></tr></table><h1>test44</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, f, x, g]<br><img src='../graphs/ListeTableau_test44_avant.png'/></td><td>liste.remove(7);</td><td>[a, b, c, d, e, f, x]<br><img src='../graphs/ListeTableau_test44_ok.png'/></td></tr></table><h1>test45</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, f, x]<br><img src='../graphs/ListeTableau_test45_avant.png'/></td><td>liste.remove(5);</td><td>[a, b, c, d, e, x]<br><img src='../graphs/ListeTableau_test45_ok.png'/></td></tr></table><h1>test46</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[r, y, g, r, s, i, o, d, s, f, h, w, y, v]<br><img src='../graphs/ListeTableau_test46_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='../graphs/ListeTableau_test46_ok.png'/></td></tr></table><h1>test47</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[a, b, c, d, e, x]<br><img src='../graphs/ListeTableau_test47_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='../graphs/ListeTableau_test47_ok.png'/></td></tr></table><h1>test48</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>[]<br><img src='../graphs/ListeTableau_test48_avant.png'/></td><td>liste.clear();</td><td>[]<br><img src='../graphs/ListeTableau_test48_ok.png'/></td></tr></table></div>
