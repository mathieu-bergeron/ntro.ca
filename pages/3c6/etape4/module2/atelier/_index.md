---
title: "Atelier 4.2: implanter une liste par tableau"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Objectif


* {{% link "/3c6/etape4/module2/atelier/coder_atelier" "Implanter et valider une liste par tableau" %}}

## Exemples de tests réussis

* <a href="tests/ListeNaive"><code>ListeNaive</code></a>
* <a href="tests/ListeTableau"><code>ListeTableau</code></a>

## Remises

* {{% link "/3c6/etape4/module2/atelier/remise_gitlab" "Remise via GitLab" %}}

* {{% link "/3c6/etape4/module2/atelier/remise_moodle" "Remise vis Moodle" %}}

