public class TesteurDeListeTableau extends TesteurDeListeAbstrait {

    @Override
    public ListeJava<Character> nouvelleListe() {
        return new ListeTableau<>(Character.class);
    }

}
