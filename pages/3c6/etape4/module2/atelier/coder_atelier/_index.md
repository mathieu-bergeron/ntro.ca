---
title: "Atelier 4.2: implanter une liste par tableau"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Recopier certaines classe de l'`atelier4_1`

* À partir de l'`atelier4_1`, recopier 
	* `ListeNaive`
	* `TesteurDeListeAbstrait`
	* `TesteurDeListeNaive`

## Créer la classe `ListeTableau`

* Ajouter la classe suivante au **paquet** `atelier4_2`
    * Nom de la classe: `ListeTableau`

* Débuter l'implantation comme suit:

```java
{{% embed "ListeTableau.java" %}}
```

* Utiliser {{% key "Ctrl+1" %}} pour générer les méthodes d'une liste

* **TRUC**: compléter l'implantation de la classe une fois l'outil de validation configuré


## Créer la classe `TesteurDeListeTableau`

* Ajouter la classe suivante au **paquet** `atelier4_2`
    * Nom de la classe: `TesteurDeListeTableau`

* Implanter la classe comme suit:

```java
{{% embed "TesteurDeListeTableau.java" %}}
```

## Créer la classe `MonAtelier4_2`

* Ajouter la classe suivante au **paquet** `atelier4_2`
    * Nom de la classe: `MonAtelier4_2`

* Implanter la classe comme suit:

```java
{{% embed "MonAtelier4_1.java" %}}
```

## Compléter les implantations

* Utiliser l'outil de validation pour tester votre code

<img src="validation.png"/>

* vous pouvez aussi afficher un des fichiers `html`:

    * `ListeTableau.html`
    * `ListeNaive.html`
    * (à la racine du projet)

## Tester l'efficacité du code

* Les méthodes de votre `TesteurDeListeAbstrait` seront appelées
    * typiquement, utiliser une boucle pour effectuer une opération un certain nombre de fois

* Vous devriez avoir des résultats similaires aux résultats ci-bas

    * `melangerLaListe`

    <img src="melangerLaListe.png"/>

    * `melangerLaListeEfficace`

    <img src="melangerLaListeEfficace.png"/>

    * `fairePlusieursModificationsAleatoires`

    <img src="fairePlusieursModificationsAleatoires.png"/>

    * `fairePlusieursAjouts`

    <img src="fairePlusieursAjouts.png"/>

    * `fairePlusieursRetraitsALaFin`

    <img src="fairePlusieursRetraitsALaFin.png"/>

    * `fairePlusieursRetraitsAuDebut`

    <img src="fairePlusieursRetraitsAuDebut.png"/>


    * `fairePlusieursInsertionsAuDebut`

    <img src="fairePlusieursInsertionsAuDebut.png"/>

    * `fairePlusieursInsertionsAleatoires`

    <img src="fairePlusieursInsertionsAleatoires.png"/>


