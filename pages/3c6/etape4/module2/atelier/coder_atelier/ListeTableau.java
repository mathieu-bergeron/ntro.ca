public class ListeTableau<E extends Object> extends ListeJava<E> {
    
    private final int TAILLE_INITIALE = 2;

    private E[] grosTableau = nouveauTableau(TAILLE_INITIALE);
    private int indiceDernier = -1;

    public E[] getGrosTableau() {
        return grosTableau;
    }

    public void setGrosTableau(E[] grosTableau) {
        this.grosTableau = grosTableau;
    }

    public int getIndiceDernier() {
        return indiceDernier;
    }

    public void setIndiceDernier(int indiceDernier) {
        this.indiceDernier = indiceDernier;
    }

    public ListeTableau(Class<E> typeElement){
        super(typeElement);
    }

    private void agrandir() {
        E[] nouveauGrosTableau = nouveauTableau(grosTableau.length*2);

        // TODO
    }

    // TODO

}
