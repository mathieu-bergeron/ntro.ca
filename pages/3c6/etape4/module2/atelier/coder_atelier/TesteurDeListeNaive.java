public class TesteurDeListeNaive extends TesteurDeListeAbstrait {

    @Override
    public ListeJava<Character> nouvelleListe() {
        return new ListeNaive<>(Character.class);
    }
}
