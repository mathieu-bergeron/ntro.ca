public class MonAtelier4_2 extends Atelier4_2 {
    
    public static void main(String[] args) {
        
        (new MonAtelier4_2()).valider();
    }

    @Override
    public ListeJava<Character> fournirListeTableau() {
        return new ListeTableau<Character>(Character.class);
    }

    @Override
    public ListeJava<Character> fournirListeNaive() {
        return new ListeNaive<Character>(Character.class);
    }

    @Override
    public TesteurDeListe fournirTesteurDeListeTableau() {
        return new TesteurDeListeTableau();
    }

    @Override
    public TesteurDeListe fournirTesteurDeListeNaive() {
        return new TesteurDeListeNaive();
    }
}
