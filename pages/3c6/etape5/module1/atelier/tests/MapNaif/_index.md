---
---
<div style='max-width:80%; margin:auto;'><h1>test00</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='./_storage/graphs/MapNaif_test00_avant.png'/></td><td>map.put("vzly",&nbsp;95);</td><td>{"vzly":95}<br><img src='./_storage/graphs/MapNaif_test00_ok.png'/></td></tr></table><h1>test01</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":95}<br><img src='./_storage/graphs/MapNaif_test01_avant.png'/></td><td>map.put("vzly",&nbsp;52);</td><td>{"vzly":52}<br><img src='./_storage/graphs/MapNaif_test01_ok.png'/></td></tr></table><h1>test02</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":52}<br><img src='./_storage/graphs/MapNaif_test02_avant.png'/></td><td>map.put("vzly",&nbsp;90);</td><td>{"vzly":90}<br><img src='./_storage/graphs/MapNaif_test02_ok.png'/></td></tr></table><h1>test03</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":90}<br><img src='./_storage/graphs/MapNaif_test03_avant.png'/></td><td>map.put("r",&nbsp;50);</td><td>{"vzly":90, "r":50}<br><img src='./_storage/graphs/MapNaif_test03_ok.png'/></td></tr></table><h1>test04</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":90, "r":50}<br><img src='./_storage/graphs/MapNaif_test04_avant.png'/></td><td>map.put("r",&nbsp;98);</td><td>{"vzly":90, "r":98}<br><img src='./_storage/graphs/MapNaif_test04_ok.png'/></td></tr></table><h1>test05</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":90, "r":98}<br><img src='./_storage/graphs/MapNaif_test05_avant.png'/></td><td>map.put("dlnk",&nbsp;29);</td><td>{"vzly":90, "r":98, "dlnk":29}<br><img src='./_storage/graphs/MapNaif_test05_ok.png'/></td></tr></table><h1>test06</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":90, "r":98, "dlnk":29}<br><img src='./_storage/graphs/MapNaif_test06_avant.png'/></td><td>map.put("vzly",&nbsp;20);</td><td>{"vzly":20, "r":98, "dlnk":29}<br><img src='./_storage/graphs/MapNaif_test06_ok.png'/></td></tr></table><h1>test07</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29}<br><img src='./_storage/graphs/MapNaif_test07_avant.png'/></td><td>map.put("ttg",&nbsp;23);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":23}<br><img src='./_storage/graphs/MapNaif_test07_ok.png'/></td></tr></table><h1>test08</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":23}<br><img src='./_storage/graphs/MapNaif_test08_avant.png'/></td><td>map.put("ttg",&nbsp;19);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":19}<br><img src='./_storage/graphs/MapNaif_test08_ok.png'/></td></tr></table><h1>test09</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":19}<br><img src='./_storage/graphs/MapNaif_test09_avant.png'/></td><td>map.put("ghl",&nbsp;38);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":19, "ghl":38}<br><img src='./_storage/graphs/MapNaif_test09_ok.png'/></td></tr></table><h1>test10</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":19, "ghl":38}<br><img src='./_storage/graphs/MapNaif_test10_avant.png'/></td><td>map.put("km",&nbsp;85);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":19, "ghl":38, "km":85}<br><img src='./_storage/graphs/MapNaif_test10_ok.png'/></td></tr></table><h1>test11</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":19, "ghl":38, "km":85}<br><img src='./_storage/graphs/MapNaif_test11_avant.png'/></td><td>map.put("ttg",&nbsp;70);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85}<br><img src='./_storage/graphs/MapNaif_test11_ok.png'/></td></tr></table><h1>test12</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85}<br><img src='./_storage/graphs/MapNaif_test12_avant.png'/></td><td>map.put("o",&nbsp;61);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85, "o":61}<br><img src='./_storage/graphs/MapNaif_test12_ok.png'/></td></tr></table><h1>test13</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85, "o":61}<br><img src='./_storage/graphs/MapNaif_test13_avant.png'/></td><td>map.put("l",&nbsp;49);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85, "o":61, "l":49}<br><img src='./_storage/graphs/MapNaif_test13_ok.png'/></td></tr></table><h1>test14</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85, "o":61, "l":49}<br><img src='./_storage/graphs/MapNaif_test14_avant.png'/></td><td>map.put("yx",&nbsp;18);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85, "o":61, "l":49, "yx":18}<br><img src='./_storage/graphs/MapNaif_test14_ok.png'/></td></tr></table><h1>test15</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85, "o":61, "l":49, "yx":18}<br><img src='./_storage/graphs/MapNaif_test15_avant.png'/></td><td>map.put("b",&nbsp;40);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85, "o":61, "l":49, "yx":18, "b":40}<br><img src='./_storage/graphs/MapNaif_test15_ok.png'/></td></tr></table><h1>test16</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85, "o":61, "l":49, "yx":18, "b":40}<br><img src='./_storage/graphs/MapNaif_test16_avant.png'/></td><td>map.put("jxxp",&nbsp;19);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85, "o":61, "l":49, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test16_ok.png'/></td></tr></table><h1>test17</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":85, "o":61, "l":49, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test17_avant.png'/></td><td>map.put("km",&nbsp;64);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":64, "o":61, "l":49, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test17_ok.png'/></td></tr></table><h1>test18</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":64, "o":61, "l":49, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test18_avant.png'/></td><td>map.put("l",&nbsp;5);</td><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test18_ok.png'/></td></tr></table><h1>test19</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":29, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test19_avant.png'/></td><td>map.put("dlnk",&nbsp;6);</td><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test19_ok.png'/></td></tr></table><h1>test20</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./_storage/graphs/MapNaif_test20_avant.png'/></td><td>map.get("ev");</td><td>null</td></table><h1>test21</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test21_avant.png'/></td><td>map.get("df");</td><td>null</td></table><h1>test22</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test22_avant.png'/></td><td>map.get("cuf");</td><td>null</td></table><h1>test23</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test23_avant.png'/></td><td>map.get("vmsm");</td><td>null</td></table><h1>test24</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test24_avant.png'/></td><td>map.get("yuic");</td><td>null</td></table><h1>test25</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test25_avant.png'/></td><td>map.get("ttg");</td><td>70</td></table><h1>test26</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test26_avant.png'/></td><td>map.get("l");</td><td>5</td></table><h1>test27</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test27_avant.png'/></td><td>map.get("vzly");</td><td>20</td></table><h1>test28</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test28_avant.png'/></td><td>map.get("o");</td><td>61</td></table><h1>test29</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test29_avant.png'/></td><td>map.get("l");</td><td>5</td></table><h1>test30</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test30_avant.png'/></td><td>map.get("t");</td><td>null</td></table><h1>test31</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./_storage/graphs/MapNaif_test31_avant.png'/></td><td>map.size();</td><td>0</td></table><h1>test32</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test32_avant.png'/></td><td>map.size();</td><td>11</td></table><h1>test33</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./_storage/graphs/MapNaif_test33_avant.png'/></td><td>map.isEmpty();</td><td>true</td></table><h1>test34</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test34_avant.png'/></td><td>map.isEmpty();</td><td>false</td></table><h1>test35</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./_storage/graphs/MapNaif_test35_avant.png'/></td><td>map.containsKey("bpfx");</td><td>false</td></table><h1>test36</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test36_avant.png'/></td><td>map.containsKey("dlnk");</td><td>true</td></table><h1>test37</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test37_avant.png'/></td><td>map.containsKey("zap");</td><td>false</td></table><h1>test38</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test38_avant.png'/></td><td>map.containsKey("kbd");</td><td>false</td></table><h1>test39</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test39_avant.png'/></td><td>map.containsKey("zu");</td><td>false</td></table><h1>test40</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test40_avant.png'/></td><td>map.containsKey("fkjg");</td><td>false</td></table><h1>test41</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test41_avant.png'/></td><td>map.containsKey("boli");</td><td>false</td></table><h1>test42</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test42_avant.png'/></td><td>map.containsKey("e");</td><td>false</td></table><h1>test43</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test43_avant.png'/></td><td>map.containsKey("r");</td><td>true</td></table><h1>test44</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test44_avant.png'/></td><td>map.containsKey("jxxp");</td><td>true</td></table><h1>test45</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test45_avant.png'/></td><td>map.containsKey("vt");</td><td>false</td></table><h1>test46</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./_storage/graphs/MapNaif_test46_avant.png'/></td><td>map.containsValue("5");</td><td>false</td></table><h1>test47</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test47_avant.png'/></td><td>map.containsValue("48");</td><td>false</td></table><h1>test48</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test48_avant.png'/></td><td>map.containsValue("83");</td><td>false</td></table><h1>test49</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test49_avant.png'/></td><td>map.containsValue("18");</td><td>true</td></table><h1>test50</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test50_avant.png'/></td><td>map.containsValue("61");</td><td>true</td></table><h1>test51</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test51_avant.png'/></td><td>map.containsValue("53");</td><td>false</td></table><h1>test52</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test52_avant.png'/></td><td>map.containsValue("12");</td><td>false</td></table><h1>test53</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test53_avant.png'/></td><td>map.containsValue("28");</td><td>false</td></table><h1>test54</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test54_avant.png'/></td><td>map.containsValue("65");</td><td>false</td></table><h1>test55</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test55_avant.png'/></td><td>map.containsValue("43");</td><td>false</td></table><h1>test56</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test56_avant.png'/></td><td>map.containsValue("22");</td><td>false</td></table><h1>test57</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./_storage/graphs/MapNaif_test57_avant.png'/></td><td>map.keys();</td><td>[]</td></table><h1>test58</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test58_avant.png'/></td><td>map.keys();</td><td>[vzly,r,dlnk,ttg,ghl,km,o,l,yx,b,jxxp]</td></table><h1>test59</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='./_storage/graphs/MapNaif_test59_avant.png'/></td><td>map.remove("v");</td><td>{}<br><img src='./_storage/graphs/MapNaif_test59_ok.png'/></td></tr></table><h1>test60</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test60_avant.png'/></td><td>map.remove("ro");</td><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test60_ok.png'/></td></tr></table><h1>test61</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "l":5, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test61_avant.png'/></td><td>map.remove("l");</td><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test61_ok.png'/></td></tr></table><h1>test62</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test62_avant.png'/></td><td>map.remove("lw");</td><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test62_ok.png'/></td></tr></table><h1>test63</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test63_avant.png'/></td><td>map.remove("j");</td><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test63_ok.png'/></td></tr></table><h1>test64</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test64_avant.png'/></td><td>map.remove("v");</td><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test64_ok.png'/></td></tr></table><h1>test65</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test65_avant.png'/></td><td>map.remove("l");</td><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test65_ok.png'/></td></tr></table><h1>test66</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='./_storage/graphs/MapNaif_test66_avant.png'/></td><td>map.clear();</td><td>{}<br><img src='./_storage/graphs/MapNaif_test66_ok.png'/></td></tr></table><h1>test67</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"vzly":20, "r":98, "dlnk":6, "ttg":70, "ghl":38, "km":64, "o":61, "yx":18, "b":40, "jxxp":19}<br><img src='./_storage/graphs/MapNaif_test67_avant.png'/></td><td>map.clear();</td><td>{}<br><img src='./_storage/graphs/MapNaif_test67_ok.png'/></td></tr></table></div>
