---
title: "Atelier 5.1: implanter un mappage naïf"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Objectif


* {{% link "/3c6/etape5/module1/atelier/coder_atelier" "Implanter et valider un mappage naïf" %}}

## Exemples de tests réussis

* <a href="tests/MapNaif"><code>MapNaif</code></a>


## Remises

* {{% link "/3c6/etape5/module1/atelier/remise_gitlab" "Remise via GitLab" %}}

* {{% link "/3c6/etape5/module1/atelier/remise_moodle" "Remise vis Moodle" %}}

