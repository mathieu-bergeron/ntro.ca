public class TesteurDeMapNaif extends TesteurDeMapAbstrait {

    @Override
    public MapJava<Cle<String>, Integer> nouveauMap() {
        return new MapNaif<Cle<String>, Integer>();
    }

    @Override
    public Cle<String> nouvelleCle(String valeur) {
        return new Cle<String>(valeur);
    }

}
