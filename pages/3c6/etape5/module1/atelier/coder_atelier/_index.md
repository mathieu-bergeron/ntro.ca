---
title: "Atelier 5.1: implanter un mappage naïf"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer la classe `MapNaif`

* Ajouter la classe suivante au **paquet** `atelier5_1`
    * Nom de la classe: `MapNaif`

* Débuter l'implantation comme suit:

```java
{{% embed "MapNaif.java" %}}
```

## Créer la classe `TesteurDeMapAbstrait`

* Ajouter la classe suivante au **paquet** `atelier5_1`
    * Nom de la classe: `TesteurDeMapAbstrait`

* Débuter l'implantation comme suit:

```java
{{% embed "TesteurDeMapAbstrait.java" %}}
```

## Créer la classe `TesteurDeMapNaif`

* Ajouter la classe suivante au **paquet** `atelier5_1`
    * Nom de la classe: `TesteurDeMapNaif`

* Implanter la classe comme suit:

```java
{{% embed "TesteurDeMapNaif.java" %}}
```

## Créer la classe `MonAtelier5_1`

* Ajouter la classe suivante au **paquet** `atelier5_1`
    * Nom de la classe: `MonAtelier5_1`

* Implanter la classe comme suit:

```java
{{% embed "MonAtelier5_1.java" %}}
```

## Compléter les implantations

* Utiliser l'outil de validation pour tester votre code

<img src="validation.png"/>

* vous pouvez aussi afficher le fichier `html`:

    * `MapNaif.html`
    * (à la racine du projet)

## Tester l'efficacité du code

* Les méthodes de votre `TesteurDeMapAbstrait` seront appelées
    * typiquement, utiliser une boucle pour effectuer une opération un certain nombre de fois
    * **XXX**: SVP faire `nombreOperations*50` fois (l'outil est mal calibré)

* Vous devriez avoir des résultats similaires aux résultats ci-bas

    * `fairePlusieursAjoutsAleatoires`:

    <img src="fairePlusieursAjoutsAleatoires.png"/>

    * `fairePlusieursModificationsAleatoires`:

    <img src="fairePlusieursModificationsAleatoires.png"/>

    * `fairePlusieursRetraitsAleatoires`:

    <img src="fairePlusieursRetraitsAleatoires.png"/>

    * `accederAuxClesDansOrdre`:

        <img src="accederAuxClesDansOrdre.png"/>

        * (*) ce graphe semble varier beaucoup d'un ordi à l'autre
