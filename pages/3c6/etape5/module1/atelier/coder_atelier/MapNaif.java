public class MapNaif <C extends Cle<?>, V extends Object> extends MapJava<C,V> {
    
    private List<C> cles = new ArrayList<>();
    private List<V> valeurs = new ArrayList<>();

    public List<C> getCles() {
        return cles;
    }

    public void setCles(List<C> cles) {
        this.cles = cles;
    }

    public List<V> getValeurs() {
        return valeurs;
    }

    public void setValeurs(List<V> valeurs) {
        this.valeurs = valeurs;
    }

    @Override
    public void put(C c, V v) {
        // TODO
    }

    @Override
    public V get(C c) {
        // TODO
    }

    @Override
    public void clear() {
        // TODO
    }

    @Override
    public int size() {
        // TODO
    }

    @Override
    public boolean isEmpty() {
        // TODO
    }

    @Override
    public boolean containsKey(C c) {
        // TODO
    }

    @Override
    public boolean containsValue(V v) {
        // TODO
    }

    @Override
    public void remove(C c) {
        // TODO
    }

    @Override
    public List<C> keys() {
        // TODO
    }
}
