public abstract class TesteurDeMapAbstrait extends TesteurDeMap {
    
    private static final Random alea = new Random();

    @Override
    public void fairePlusieursAjoutsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations) {
        /*
         * TODO: 
         *
         * faire 'nombreOperations' fois:
         *     
         *     - choisir une cle au hasard dans le tableau cles[]
         *     - appeler la méthode put avec cette cle (n'importe quelle valeur)
         */
    }

    @Override
    public void fairePlusieursModificationsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations) {
        /*
         * TODO: comme fairePlusieursAjoutsAleatoires, la différence est que le map reçu en entrée n'est pas vide
         */
    }

    @Override
    public void fairePlusieursRetraitsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations) {
        /*
         * TODO: 
         *
         * faire 'nombreOperations' fois:
         *     
         *     - choisir une cle au hasard dans le tableau cles[]
         *     - appeler la méthode remove avec cette cle
         */
    }

    @Override
    public void accederAuxClesDansOrdre(MapJava<Cle<String>, Integer> map) {
        /*
         * TODO:
         *
         * Créer deux listes
         *    - liste1: récupérer la liste de clés en appelant la méthode keys()
         *    - liste2: une liste de chaînes vide
         *
         * Pour chaque objet Cle contenu dans dans liste1
         *   - ajouter la valeur de la clé dans liste2
         *
         * trier liste2 avec Collections.sort
         *
         * afficher chaque valeur de liste2 en appelant la méthode afficherCle
         *
         */

        // XXX: afficher avec
        afficherCle("CLÉ " + cle);
    }
}
