public class Cle<T extends Object> {

	private T valeur;
	
	public Cle(T valeur) {
		this.valeur = valeur;
	}
	
	public T valeur() {
		return valeur;
	}
}
