---
title: ""
weight: 1
bookHidden: true
---


# Théorie 5.1: Map naïf

<center>
<video src="01.mp4" width="50%" type="video/mp4" controls playsinline>
</center>

* Un map permet d'associer une clé à une valeur
    * chaque clé est unique

* Par exemple, le map ci-bas associe la clé `"xdf"` à la valeur `12`:
    * `{"sdx":5,"xdf":12,"lkm":54}`


* Un map est aussi appelé: dictionnaire, liste associative, objet JSON

* On peut voir un map comme un tableau où les indices ne sont **pas** des entiers:

```java
{{% embed "MapCommeTableau.java" %}}
```


## Interface d'un map

<center>
<video src="02.mp4" width="50%" type="video/mp4" controls playsinline>
</center>

```java
{{% embed "MapJava.java" %}}
```

## Implantation naïve d'un map

<center>
<video src="03.mp4" width="50%" type="video/mp4" controls playsinline>
</center>

* On utilise deux listes:
    * la liste des clés
    * la liste des valeurs

* Une paire clé/valeur est mémorisée au même indice

* Par exemple:

```java
{{% embed "MapJavaNaifExemple.java" %}}
```

* Si la clé `"xdf"` est à l'indice `1` dans `cles`
* Alors la valeur associée à `"xdf"` est aussi à l'indice 1, mais dans `valeurs`

* En général:

```java
{{% embed "MapJavaNaif.java" %}}
```


<center>
<video src="04.mp4" width="50%" type="video/mp4" controls playsinline>
</center>

* Chaque clé doit héritée de la classe `Cle`:

```java
{{% embed "Cle.java" %}}
```

* on veut ainsi rendre explicite la notion de clé

## Problème d'efficacité

<center>
<video src="05.mp4" width="50%" type="video/mp4" controls playsinline>
</center>

* Modifier une valeur dans notre map naïf n'est pas efficace:

<center>
    <img src="naif_modifs.png"/>
</center>

* Le problème est que `indexOf` fait une boucle sur la liste des clés

* Mais on aimerait que ça soit efficace:
    * on veut utiliser le map comme un tableau

