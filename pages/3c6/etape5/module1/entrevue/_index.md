---
title: "Entrevue 5.1: mappage naïf"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier 

## Interpréter un mappage naïf

* Imaginer le map naïf suivant

<img src="exemple01.png" />

* Écrire ce mappage en JSON

## Créer un mappage naïf

* Considérer le mappage JSON suivant

    * `{"3f4":true, "4ad":12, "xxw":"asdf", "44rr":[1,3,4]}`

* Créer un mappage naïf pour mémoriser le mappage JSON ci-haut






