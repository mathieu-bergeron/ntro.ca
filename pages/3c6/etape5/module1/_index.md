---
title: "Module 5.1: mappage naïf"
weight: 30
draft: false
---

{{% pageTitle %}}

1. {{% link "/3c6/etape5/module1/theorie/" "Théorie" %}}

1. {{% link "/3c6/etape5/module1/entrevue/" "Entrevue 5.1" %}}

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=332687" target="_blank">Mini-test théorique</a>

1. {{% link "/3c6/etape5/module1/atelier" "Atelier 5.1" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}
