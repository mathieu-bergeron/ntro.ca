---
title: "Examen 5"
weight: 40
draft: false
---

# 3C6: Examen 5

* **SVP** apporter des écouteurs, il y a une vidéo pour présenter l'examen

* Durée: 2h

* Individuel

* Documentation permise: ntro.ca, Moodle, recherches sur le Web, **votre code**, des cartes à jouer

* L'examen est 100% à choix de réponse sur Moodle

## Sujets

* comprendre les différentes implantations d'un mappage

* efficacité d'un mappage selon le contexte

## 5pts) Volet théorique

* 2 questions de compréhension 
* 4 questions courtes sur les types de mappage

## 5pts) Volet pratique

* 4 questions du type «&nbsp;compléter le code suivant&nbsp;» 
