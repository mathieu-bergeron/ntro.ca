---
title: "Entrevue 5.2: mappage par hachage"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier 

## Évaluer la qualité d'une fonction de hachage (1)

* Imaginer la fonction de hachage suivante:

```java
public int indice(Vehicule vehicule){
    return (int) vehicule.getMarque().premiereLettre();
}
```

* Est-ce une bonne fonction de hachage? Pourquoi?

## Évaluer la qualité d'une fonction de hachage (2)

* Imaginer la fonction de hachage suivante:

```java
public int indice(Vehicule vehicule){
    int indice = 0;
    String numeroSerie = vehicule.getNumeroSerie();

    for(int i = 0; i < numeroSerie.length(); i++){
        indice += (int) numeroSerie.charAt(i);
    }

    return indice;
}
```

* Est-ce une bonne fonction de hachage? Pourquoi?


## Modifier un map par hachage

* Considérer le map par hachage suivant

<img src="exemple01.png" />

* Écrire ce mappage en JSON

* Quelle est la fonction de hachage? Est-ce une bonne fonction de hachage?







