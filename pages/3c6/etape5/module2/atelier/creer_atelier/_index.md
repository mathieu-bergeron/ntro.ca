---
title: "Atelier5.2: créer le projet Eclipse"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

1. Télécharger {{% download "atelier5_2.zip" "atelier5_2.zip" %}}

1. Copier le fichier `atelier5_2.zip` **à la racine** de mon dépôt Git

1. Extraire les fichiers **directement** à la racine du dépôt Git

    * Clic-droit sur le fichier => *Extraire tout*

    * **Effacer `atelier5_2` du chemin proposé**

    * Cliquer sur *Extraire*

    * Choisir *Remplacer les fichiers dans la destination*

    * Vérifier que les fichiers sont **à la racine** du dépôt Git 

        * dans le même répertoire que `.git`


1. Ouvrir *Git Bash* **à la racine** de mon dépôt Git

    * *Windows 10* : Clic-droit => *Git Bash Here*
    * *Windows 11* : Clic-droit => *Show more options* => *Git Bash Here*

1. En Git Bash, exécuter le script `creer_projets_eclipse.sh`

    ```bash
    $ sh scripts/creer_projets_eclipse.sh
    ```

    * au besoin fermer Eclipse avant d'exécuter le script

    * appuyer sur {{% key "Entrée" %}} dans fenêtre *Git Bash* pour vraiment lancer le script


1. Attendre que le script termine

1. Ouvrir Eclipse et importer le projet `atelier5_2`

    * *File* => *Import* => *Existing Projects into Workspace*

    * Cliquer sur *Browse* et naviguer jusqu'à la racine de mon dépôt Git

    * Cliquer sur *Sélectionner un dossier*

    * Vérifier que le projet `atelier5_2` apparaît dans la case *Projects*

    * Cliquer sur *Finish*

