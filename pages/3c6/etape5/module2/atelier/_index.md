---
title: "Atelier 5.2: implanter un map par hachage"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Objectif


* {{% link "/3c6/etape5/module2/atelier/coder_atelier" "Implanter et valider un map par hachage (et trois fonctions de hachage)" %}}

## Exemples de tests réussis

* <a href="tests/MapHasha"><code>MapHasha</code></a>
* <a href="tests/MapHashb"><code>MapHashb</code></a>
* <a href="tests/MapHashc"><code>MapHashc</code></a>


## Remises

* {{% link "/3c6/etape5/module2/atelier/remise_gitlab" "Remise via GitLab" %}}

* {{% link "/3c6/etape5/module2/atelier/remise_moodle" "Remise vis Moodle" %}}

