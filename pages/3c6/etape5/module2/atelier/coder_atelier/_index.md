---
title: "Atelier 5.2: implanter un mappage par hachage"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Recopier des classes à partir de l'`atelier5_1`

* Recopier les classes suivantes:

    * `MapNaif`
    * `TesteurDeMapAbstrait`
    * `TesteurDeMapNaif`

## Créer la classe `ChaineHasha` (une mauvaise fonction de hachage)

* Ajouter la classe suivante au **paquet** `atelier5_2`
    * Nom de la classe: `ChaineHasha`

* Débuter l'implantation comme suit:

```java
{{% embed "ChaineHasha.java" %}}
```

## Créer la classe `ChaineHashb` (une fonction de hachage moyenne)

* Ajouter la classe suivante au **paquet** `atelier5_2`
    * Nom de la classe: `ChaineHashb`

* Débuter l'implantation comme suit:

```java
{{% embed "ChaineHashb.java" %}}
```

## Créer la classe `ChaineHashc` (une bonne fonction de hachage)

* Ajouter la classe suivante au **paquet** `atelier5_2`
    * Nom de la classe: `ChaineHashc`

* Débuter l'implantation comme suit:

```java
{{% embed "ChaineHashc.java" %}}
```

## Créer la classe `MapHash`

* Ajouter la classe suivante au **paquet** `atelier5_2`
    * Nom de la classe: `MapHash`

* Débuter l'implantation comme suit:

```java
{{% embed "MapHash.java" %}}
```

## Créer la classe `TesteurDeMapHasha`

* Ajouter la classe suivante au **paquet** `atelier5_2`
    * Nom de la classe: `TesteurDeMapHasha`

* Implanter comme suit:

```java
{{% embed "TesteurDeMapHasha.java" %}}
```

## Créer la classe `TesteurDeMapHashb`

* Ajouter la classe suivante au **paquet** `atelier5_2`
    * Nom de la classe: `TesteurDeMapHashb`

* Implanter comme suit:

```java
{{% embed "TesteurDeMapHashb.java" %}}
```

## Créer la classe `TesteurDeMapHashc`

* Ajouter la classe suivante au **paquet** `atelier5_2`
    * Nom de la classe: `TesteurDeMapHashc`

* Implanter comme suit:

```java
{{% embed "TesteurDeMapHashc.java" %}}
```

## Créer la classe `MonAtelier5_2`

* Ajouter la classe suivante au **paquet** `atelier5_2`
    * Nom de la classe: `MonAtelier5_2`

* Implanter comme suit:

```java
{{% embed "MonAtelier5_2.java" %}}
```

## Compléter les implantations

* Utiliser l'outil de validation pour tester votre code

<img src="validation.png"/>

* vous pouvez aussi afficher les fichiers `html`:

    * `MapHasha.html`
    * `MapHashb.html`
    * `MapHashc.html`
    * (à la racine du projet)

## Tester l'efficacité du code

* Les méthodes de votre `TesteurDeMapAbstrait` seront appelées
    * typiquement, utiliser une boucle pour effectuer une opération un certain nombre de fois

* Vous devriez avoir des résultats similaires aux résultats ci-bas

    * `fairePlusieursAjoutsAleatoires`:

    <img src="fairePlusieursAjoutsAleatoires.png"/>

    * `fairePlusieursModificationsAleatoires`:

    <img src="fairePlusieursModificationsAleatoires.png"/>

    * `fairePlusieursRetraitsAleatoires`:

    <img src="fairePlusieursRetraitsAleatoires.png"/>

    * `accederAuxClesDansOrdre`:

        <img src="accederAuxClesDansOrdre.png"/>

        * (*) ce graphe semble varier beaucoup d'un ordi à l'autre
