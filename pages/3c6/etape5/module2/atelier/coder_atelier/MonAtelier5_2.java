public class MonAtelier5_2 extends Atelier5_2 {
    
    public static void main(String[] args) {
        
        (new MonAtelier5_2()).valider();
    }

    @Override
    public TesteurDeMap fournirTesteurDeMapNaif() {
        return new TesteurDeMapNaif();
    }

    @Override
    public TesteurDeMap fournirTesteurDeMapHasha() {
        return new TesteurDeMapHasha();
    }

    @Override
    public TesteurDeMap fournirTesteurDeMapHashb() {
        return new TesteurDeMapHashb();
    }

    @Override
    public TesteurDeMap fournirTesteurDeMapHashc() {
        return new TesteurDeMapHashc();
    }
}
