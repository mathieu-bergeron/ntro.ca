public class TesteurDeMapHashb extends TesteurDeMapAbstrait {

    @Override
    public MapJava<Cle<String>, Integer> nouveauMap() {
        return new MapHash();
    }

    @Override
    public CleHachable<String> nouvelleCle(String valeur) {
        return new ChaineHashb(valeur);
    }
}
