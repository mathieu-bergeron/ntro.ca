public class MapHash <C extends CleHachable<?>, V extends Object> extends MapJava<C,V> {
    
    private static final int TAILLE_TABLE_HACHAGE = 20;
    
    private MapNaif<C,V>[] table = new MapNaif[TAILLE_TABLE_HACHAGE];
    private int taille = 0;

    public MapNaif<C, V>[] getTable() {
        return table;
    }

    public void setTable(MapNaif<C, V>[] table) {
        this.table = table;
    }

    public int getTaille() {
        return taille;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    private int indiceTable(C c) {
        int indiceBrut = c.indice();
        return indiceBrut % TAILLE_TABLE_HACHAGE;
    }

    @Override
    public void put(C c, V v) {
        // TODO
    }

    @Override
    public V get(C c) {
        // TODO
    }

    @Override
    public void clear() {
        // TODO
    }

    @Override
    public int size() {
        // TODO
    }

    @Override
    public boolean isEmpty() {
        // TODO
    }

    @Override
    public boolean containsKey(C c) {
        // TODO
    }

    @Override
    public boolean containsValue(V v) {
        // TODO
    }

    @Override
    public void remove(C c) {
        // TODO
    }

    @Override
    public List<C> keys() {
        // TODO
    }
}
