---
---
<div style='max-width:80%; margin:auto;'><h1>test00</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashb_test00_avant.png'/></td><td>map.put("gge",&nbsp;56);</td><td>{"gge":56}<br><img src='../_storage/graphs/MapHashb_test00_ok.png'/></td></tr></table><h1>test01</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"gge":56}<br><img src='../_storage/graphs/MapHashb_test01_avant.png'/></td><td>map.put("lba",&nbsp;95);</td><td>{"gge":56, "lba":95}<br><img src='../_storage/graphs/MapHashb_test01_ok.png'/></td></tr></table><h1>test02</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"gge":56, "lba":95}<br><img src='../_storage/graphs/MapHashb_test02_avant.png'/></td><td>map.put("gge",&nbsp;33);</td><td>{"gge":33, "lba":95}<br><img src='../_storage/graphs/MapHashb_test02_ok.png'/></td></tr></table><h1>test03</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"gge":33, "lba":95}<br><img src='../_storage/graphs/MapHashb_test03_avant.png'/></td><td>map.put("b",&nbsp;27);</td><td>{"b":27, "gge":33, "lba":95}<br><img src='../_storage/graphs/MapHashb_test03_ok.png'/></td></tr></table><h1>test04</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "gge":33, "lba":95}<br><img src='../_storage/graphs/MapHashb_test04_avant.png'/></td><td>map.put("r",&nbsp;57);</td><td>{"b":27, "r":57, "gge":33, "lba":95}<br><img src='../_storage/graphs/MapHashb_test04_ok.png'/></td></tr></table><h1>test05</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":57, "gge":33, "lba":95}<br><img src='../_storage/graphs/MapHashb_test05_avant.png'/></td><td>map.put("nsy",&nbsp;99);</td><td>{"b":27, "r":57, "gge":33, "lba":95, "nsy":99}<br><img src='../_storage/graphs/MapHashb_test05_ok.png'/></td></tr></table><h1>test06</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":57, "gge":33, "lba":95, "nsy":99}<br><img src='../_storage/graphs/MapHashb_test06_avant.png'/></td><td>map.put("tps",&nbsp;12);</td><td>{"b":27, "r":57, "gge":33, "lba":95, "nsy":99, "tps":12}<br><img src='../_storage/graphs/MapHashb_test06_ok.png'/></td></tr></table><h1>test07</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":57, "gge":33, "lba":95, "nsy":99, "tps":12}<br><img src='../_storage/graphs/MapHashb_test07_avant.png'/></td><td>map.put("veq",&nbsp;0);</td><td>{"b":27, "r":57, "gge":33, "lba":95, "nsy":99, "tps":12, "veq":0}<br><img src='../_storage/graphs/MapHashb_test07_ok.png'/></td></tr></table><h1>test08</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":57, "gge":33, "lba":95, "nsy":99, "tps":12, "veq":0}<br><img src='../_storage/graphs/MapHashb_test08_avant.png'/></td><td>map.put("gge",&nbsp;13);</td><td>{"b":27, "r":57, "gge":13, "lba":95, "nsy":99, "tps":12, "veq":0}<br><img src='../_storage/graphs/MapHashb_test08_ok.png'/></td></tr></table><h1>test09</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":57, "gge":13, "lba":95, "nsy":99, "tps":12, "veq":0}<br><img src='../_storage/graphs/MapHashb_test09_avant.png'/></td><td>map.put("tps",&nbsp;23);</td><td>{"b":27, "r":57, "gge":13, "lba":95, "nsy":99, "tps":23, "veq":0}<br><img src='../_storage/graphs/MapHashb_test09_ok.png'/></td></tr></table><h1>test10</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":57, "gge":13, "lba":95, "nsy":99, "tps":23, "veq":0}<br><img src='../_storage/graphs/MapHashb_test10_avant.png'/></td><td>map.put("r",&nbsp;24);</td><td>{"b":27, "r":24, "gge":13, "lba":95, "nsy":99, "tps":23, "veq":0}<br><img src='../_storage/graphs/MapHashb_test10_ok.png'/></td></tr></table><h1>test11</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":24, "gge":13, "lba":95, "nsy":99, "tps":23, "veq":0}<br><img src='../_storage/graphs/MapHashb_test11_avant.png'/></td><td>map.put("r",&nbsp;49);</td><td>{"b":27, "r":49, "gge":13, "lba":95, "nsy":99, "tps":23, "veq":0}<br><img src='../_storage/graphs/MapHashb_test11_ok.png'/></td></tr></table><h1>test12</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":49, "gge":13, "lba":95, "nsy":99, "tps":23, "veq":0}<br><img src='../_storage/graphs/MapHashb_test12_avant.png'/></td><td>map.put("tps",&nbsp;89);</td><td>{"b":27, "r":49, "gge":13, "lba":95, "nsy":99, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test12_ok.png'/></td></tr></table><h1>test13</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":49, "gge":13, "lba":95, "nsy":99, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test13_avant.png'/></td><td>map.put("r",&nbsp;84);</td><td>{"b":27, "r":84, "gge":13, "lba":95, "nsy":99, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test13_ok.png'/></td></tr></table><h1>test14</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":84, "gge":13, "lba":95, "nsy":99, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test14_avant.png'/></td><td>map.put("lba",&nbsp;38);</td><td>{"b":27, "r":84, "gge":13, "lba":38, "nsy":99, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test14_ok.png'/></td></tr></table><h1>test15</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":84, "gge":13, "lba":38, "nsy":99, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test15_avant.png'/></td><td>map.put("y",&nbsp;12);</td><td>{"b":27, "r":84, "y":12, "gge":13, "lba":38, "nsy":99, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test15_ok.png'/></td></tr></table><h1>test16</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":27, "r":84, "y":12, "gge":13, "lba":38, "nsy":99, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test16_avant.png'/></td><td>map.put("b",&nbsp;44);</td><td>{"b":44, "r":84, "y":12, "gge":13, "lba":38, "nsy":99, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test16_ok.png'/></td></tr></table><h1>test17</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":13, "lba":38, "nsy":99, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test17_avant.png'/></td><td>map.put("nsy",&nbsp;67);</td><td>{"b":44, "r":84, "y":12, "gge":13, "lba":38, "nsy":67, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test17_ok.png'/></td></tr></table><h1>test18</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":13, "lba":38, "nsy":67, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test18_avant.png'/></td><td>map.put("nsy",&nbsp;10);</td><td>{"b":44, "r":84, "y":12, "gge":13, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test18_ok.png'/></td></tr></table><h1>test19</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":13, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test19_avant.png'/></td><td>map.put("gge",&nbsp;75);</td><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test19_ok.png'/></td></tr></table><h1>test20</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashb_test20_avant.png'/></td><td>map.get("idbn");</td><td>null</td></table><h1>test21</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test21_avant.png'/></td><td>map.get("ofzj");</td><td>null</td></table><h1>test22</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test22_avant.png'/></td><td>map.get("y");</td><td>12</td></table><h1>test23</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test23_avant.png'/></td><td>map.get("y");</td><td>12</td></table><h1>test24</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test24_avant.png'/></td><td>map.get("i");</td><td>null</td></table><h1>test25</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test25_avant.png'/></td><td>map.get("zzo");</td><td>null</td></table><h1>test26</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test26_avant.png'/></td><td>map.get("vbe");</td><td>null</td></table><h1>test27</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test27_avant.png'/></td><td>map.get("ejrg");</td><td>null</td></table><h1>test28</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test28_avant.png'/></td><td>map.get("n");</td><td>null</td></table><h1>test29</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test29_avant.png'/></td><td>map.get("lba");</td><td>38</td></table><h1>test30</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test30_avant.png'/></td><td>map.get("indi");</td><td>null</td></table><h1>test31</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashb_test31_avant.png'/></td><td>map.size();</td><td>0</td></table><h1>test32</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test32_avant.png'/></td><td>map.size();</td><td>8</td></table><h1>test33</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashb_test33_avant.png'/></td><td>map.isEmpty();</td><td>true</td></table><h1>test34</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test34_avant.png'/></td><td>map.isEmpty();</td><td>false</td></table><h1>test35</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashb_test35_avant.png'/></td><td>map.containsKey("wegf");</td><td>false</td></table><h1>test36</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test36_avant.png'/></td><td>map.containsKey("et");</td><td>false</td></table><h1>test37</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test37_avant.png'/></td><td>map.containsKey("b");</td><td>true</td></table><h1>test38</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test38_avant.png'/></td><td>map.containsKey("d");</td><td>false</td></table><h1>test39</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test39_avant.png'/></td><td>map.containsKey("veq");</td><td>true</td></table><h1>test40</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test40_avant.png'/></td><td>map.containsKey("fdfp");</td><td>false</td></table><h1>test41</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test41_avant.png'/></td><td>map.containsKey("nsy");</td><td>true</td></table><h1>test42</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test42_avant.png'/></td><td>map.containsKey("r");</td><td>true</td></table><h1>test43</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test43_avant.png'/></td><td>map.containsKey("r");</td><td>true</td></table><h1>test44</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test44_avant.png'/></td><td>map.containsKey("gge");</td><td>true</td></table><h1>test45</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test45_avant.png'/></td><td>map.containsKey("dmot");</td><td>false</td></table><h1>test46</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashb_test46_avant.png'/></td><td>map.containsValue("18");</td><td>false</td></table><h1>test47</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test47_avant.png'/></td><td>map.containsValue("34");</td><td>false</td></table><h1>test48</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test48_avant.png'/></td><td>map.containsValue("0");</td><td>true</td></table><h1>test49</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test49_avant.png'/></td><td>map.containsValue("35");</td><td>false</td></table><h1>test50</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test50_avant.png'/></td><td>map.containsValue("25");</td><td>false</td></table><h1>test51</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test51_avant.png'/></td><td>map.containsValue("61");</td><td>false</td></table><h1>test52</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test52_avant.png'/></td><td>map.containsValue("31");</td><td>false</td></table><h1>test53</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test53_avant.png'/></td><td>map.containsValue("50");</td><td>false</td></table><h1>test54</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test54_avant.png'/></td><td>map.containsValue("98");</td><td>false</td></table><h1>test55</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test55_avant.png'/></td><td>map.containsValue("84");</td><td>true</td></table><h1>test56</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test56_avant.png'/></td><td>map.containsValue("91");</td><td>false</td></table><h1>test57</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashb_test57_avant.png'/></td><td>map.keys();</td><td>[]</td></table><h1>test58</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test58_avant.png'/></td><td>map.keys();</td><td>[b,r,y,gge,lba,nsy,tps,veq]</td></table><h1>test59</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashb_test59_avant.png'/></td><td>map.remove("dtf");</td><td>{}<br><img src='../_storage/graphs/MapHashb_test59_ok.png'/></td></tr></table><h1>test60</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test60_avant.png'/></td><td>map.remove("tqs");</td><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test60_ok.png'/></td></tr></table><h1>test61</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test61_avant.png'/></td><td>map.remove("bl");</td><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test61_ok.png'/></td></tr></table><h1>test62</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":44, "r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test62_avant.png'/></td><td>map.remove("b");</td><td>{"r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test62_ok.png'/></td></tr></table><h1>test63</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test63_avant.png'/></td><td>map.remove("o");</td><td>{"r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test63_ok.png'/></td></tr></table><h1>test64</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"r":84, "y":12, "gge":75, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test64_avant.png'/></td><td>map.remove("gge");</td><td>{"r":84, "y":12, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test64_ok.png'/></td></tr></table><h1>test65</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"r":84, "y":12, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test65_avant.png'/></td><td>map.remove("mh");</td><td>{"r":84, "y":12, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test65_ok.png'/></td></tr></table><h1>test66</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashb_test66_avant.png'/></td><td>map.clear();</td><td>{}<br><img src='../_storage/graphs/MapHashb_test66_ok.png'/></td></tr></table><h1>test67</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"r":84, "y":12, "lba":38, "nsy":10, "tps":89, "veq":0}<br><img src='../_storage/graphs/MapHashb_test67_avant.png'/></td><td>map.clear();</td><td>{}<br><img src='../_storage/graphs/MapHashb_test67_ok.png'/></td></tr></table></div>
