---
---
<div style='max-width:80%; margin:auto;'><h1>test00</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHasha_test00_avant.png'/></td><td>map.put("o",&nbsp;2);</td><td>{"o":2}<br><img src='../_storage/graphs/MapHasha_test00_ok.png'/></td></tr></table><h1>test01</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":2}<br><img src='../_storage/graphs/MapHasha_test01_avant.png'/></td><td>map.put("lo",&nbsp;31);</td><td>{"o":2, "lo":31}<br><img src='../_storage/graphs/MapHasha_test01_ok.png'/></td></tr></table><h1>test02</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":2, "lo":31}<br><img src='../_storage/graphs/MapHasha_test02_avant.png'/></td><td>map.put("bsh",&nbsp;26);</td><td>{"o":2, "lo":31, "bsh":26}<br><img src='../_storage/graphs/MapHasha_test02_ok.png'/></td></tr></table><h1>test03</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":2, "lo":31, "bsh":26}<br><img src='../_storage/graphs/MapHasha_test03_avant.png'/></td><td>map.put("fd",&nbsp;42);</td><td>{"o":2, "lo":31, "bsh":26, "fd":42}<br><img src='../_storage/graphs/MapHasha_test03_ok.png'/></td></tr></table><h1>test04</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":2, "lo":31, "bsh":26, "fd":42}<br><img src='../_storage/graphs/MapHasha_test04_avant.png'/></td><td>map.put("mqb",&nbsp;83);</td><td>{"o":2, "lo":31, "bsh":26, "fd":42, "mqb":83}<br><img src='../_storage/graphs/MapHasha_test04_ok.png'/></td></tr></table><h1>test05</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":2, "lo":31, "bsh":26, "fd":42, "mqb":83}<br><img src='../_storage/graphs/MapHasha_test05_avant.png'/></td><td>map.put("fd",&nbsp;59);</td><td>{"o":2, "lo":31, "bsh":26, "fd":59, "mqb":83}<br><img src='../_storage/graphs/MapHasha_test05_ok.png'/></td></tr></table><h1>test06</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":2, "lo":31, "bsh":26, "fd":59, "mqb":83}<br><img src='../_storage/graphs/MapHasha_test06_avant.png'/></td><td>map.put("ss",&nbsp;82);</td><td>{"o":2, "lo":31, "bsh":26, "fd":59, "mqb":83, "ss":82}<br><img src='../_storage/graphs/MapHasha_test06_ok.png'/></td></tr></table><h1>test07</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":2, "lo":31, "bsh":26, "fd":59, "mqb":83, "ss":82}<br><img src='../_storage/graphs/MapHasha_test07_avant.png'/></td><td>map.put("fd",&nbsp;55);</td><td>{"o":2, "lo":31, "bsh":26, "fd":55, "mqb":83, "ss":82}<br><img src='../_storage/graphs/MapHasha_test07_ok.png'/></td></tr></table><h1>test08</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":2, "lo":31, "bsh":26, "fd":55, "mqb":83, "ss":82}<br><img src='../_storage/graphs/MapHasha_test08_avant.png'/></td><td>map.put("o",&nbsp;33);</td><td>{"o":33, "lo":31, "bsh":26, "fd":55, "mqb":83, "ss":82}<br><img src='../_storage/graphs/MapHasha_test08_ok.png'/></td></tr></table><h1>test09</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":26, "fd":55, "mqb":83, "ss":82}<br><img src='../_storage/graphs/MapHasha_test09_avant.png'/></td><td>map.put("dhf",&nbsp;76);</td><td>{"o":33, "lo":31, "bsh":26, "fd":55, "mqb":83, "ss":82, "dhf":76}<br><img src='../_storage/graphs/MapHasha_test09_ok.png'/></td></tr></table><h1>test10</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":26, "fd":55, "mqb":83, "ss":82, "dhf":76}<br><img src='../_storage/graphs/MapHasha_test10_avant.png'/></td><td>map.put("fd",&nbsp;45);</td><td>{"o":33, "lo":31, "bsh":26, "fd":45, "mqb":83, "ss":82, "dhf":76}<br><img src='../_storage/graphs/MapHasha_test10_ok.png'/></td></tr></table><h1>test11</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":26, "fd":45, "mqb":83, "ss":82, "dhf":76}<br><img src='../_storage/graphs/MapHasha_test11_avant.png'/></td><td>map.put("tuyp",&nbsp;73);</td><td>{"o":33, "lo":31, "bsh":26, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73}<br><img src='../_storage/graphs/MapHasha_test11_ok.png'/></td></tr></table><h1>test12</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":26, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73}<br><img src='../_storage/graphs/MapHasha_test12_avant.png'/></td><td>map.put("bsh",&nbsp;2);</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73}<br><img src='../_storage/graphs/MapHasha_test12_ok.png'/></td></tr></table><h1>test13</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73}<br><img src='../_storage/graphs/MapHasha_test13_avant.png'/></td><td>map.put("dhl",&nbsp;47);</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47}<br><img src='../_storage/graphs/MapHasha_test13_ok.png'/></td></tr></table><h1>test14</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47}<br><img src='../_storage/graphs/MapHasha_test14_avant.png'/></td><td>map.put("rh",&nbsp;26);</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26}<br><img src='../_storage/graphs/MapHasha_test14_ok.png'/></td></tr></table><h1>test15</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26}<br><img src='../_storage/graphs/MapHasha_test15_avant.png'/></td><td>map.put("nczp",&nbsp;29);</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29}<br><img src='../_storage/graphs/MapHasha_test15_ok.png'/></td></tr></table><h1>test16</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29}<br><img src='../_storage/graphs/MapHasha_test16_avant.png'/></td><td>map.put("ahs",&nbsp;57);</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57}<br><img src='../_storage/graphs/MapHasha_test16_ok.png'/></td></tr></table><h1>test17</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57}<br><img src='../_storage/graphs/MapHasha_test17_avant.png'/></td><td>map.put("doxr",&nbsp;29);</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29}<br><img src='../_storage/graphs/MapHasha_test17_ok.png'/></td></tr></table><h1>test18</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29}<br><img src='../_storage/graphs/MapHasha_test18_avant.png'/></td><td>map.put("qtx",&nbsp;18);</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18}<br><img src='../_storage/graphs/MapHasha_test18_ok.png'/></td></tr></table><h1>test19</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18}<br><img src='../_storage/graphs/MapHasha_test19_avant.png'/></td><td>map.put("q",&nbsp;64);</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test19_ok.png'/></td></tr></table><h1>test20</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHasha_test20_avant.png'/></td><td>map.get("sr");</td><td>null</td></table><h1>test21</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test21_avant.png'/></td><td>map.get("y");</td><td>null</td></table><h1>test22</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test22_avant.png'/></td><td>map.get("rh");</td><td>26</td></table><h1>test23</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test23_avant.png'/></td><td>map.get("q");</td><td>64</td></table><h1>test24</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test24_avant.png'/></td><td>map.get("o");</td><td>33</td></table><h1>test25</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test25_avant.png'/></td><td>map.get("s");</td><td>null</td></table><h1>test26</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test26_avant.png'/></td><td>map.get("o");</td><td>33</td></table><h1>test27</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test27_avant.png'/></td><td>map.get("dhl");</td><td>47</td></table><h1>test28</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test28_avant.png'/></td><td>map.get("dhl");</td><td>47</td></table><h1>test29</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test29_avant.png'/></td><td>map.get("jjs");</td><td>null</td></table><h1>test30</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test30_avant.png'/></td><td>map.get("dhl");</td><td>47</td></table><h1>test31</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHasha_test31_avant.png'/></td><td>map.size();</td><td>0</td></table><h1>test32</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test32_avant.png'/></td><td>map.size();</td><td>15</td></table><h1>test33</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHasha_test33_avant.png'/></td><td>map.isEmpty();</td><td>true</td></table><h1>test34</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test34_avant.png'/></td><td>map.isEmpty();</td><td>false</td></table><h1>test35</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHasha_test35_avant.png'/></td><td>map.containsKey("j");</td><td>false</td></table><h1>test36</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test36_avant.png'/></td><td>map.containsKey("q");</td><td>true</td></table><h1>test37</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test37_avant.png'/></td><td>map.containsKey("kkfo");</td><td>false</td></table><h1>test38</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test38_avant.png'/></td><td>map.containsKey("dhl");</td><td>true</td></table><h1>test39</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test39_avant.png'/></td><td>map.containsKey("rz");</td><td>false</td></table><h1>test40</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test40_avant.png'/></td><td>map.containsKey("tuyp");</td><td>true</td></table><h1>test41</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test41_avant.png'/></td><td>map.containsKey("o");</td><td>true</td></table><h1>test42</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test42_avant.png'/></td><td>map.containsKey("rh");</td><td>true</td></table><h1>test43</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test43_avant.png'/></td><td>map.containsKey("q");</td><td>true</td></table><h1>test44</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test44_avant.png'/></td><td>map.containsKey("mqb");</td><td>true</td></table><h1>test45</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test45_avant.png'/></td><td>map.containsKey("rh");</td><td>true</td></table><h1>test46</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHasha_test46_avant.png'/></td><td>map.containsValue("78");</td><td>false</td></table><h1>test47</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test47_avant.png'/></td><td>map.containsValue("68");</td><td>false</td></table><h1>test48</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test48_avant.png'/></td><td>map.containsValue("49");</td><td>false</td></table><h1>test49</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test49_avant.png'/></td><td>map.containsValue("0");</td><td>false</td></table><h1>test50</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test50_avant.png'/></td><td>map.containsValue("80");</td><td>false</td></table><h1>test51</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test51_avant.png'/></td><td>map.containsValue("47");</td><td>true</td></table><h1>test52</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test52_avant.png'/></td><td>map.containsValue("53");</td><td>false</td></table><h1>test53</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test53_avant.png'/></td><td>map.containsValue("78");</td><td>false</td></table><h1>test54</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test54_avant.png'/></td><td>map.containsValue("6");</td><td>false</td></table><h1>test55</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test55_avant.png'/></td><td>map.containsValue("78");</td><td>false</td></table><h1>test56</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test56_avant.png'/></td><td>map.containsValue("15");</td><td>false</td></table><h1>test57</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHasha_test57_avant.png'/></td><td>map.keys();</td><td>[]</td></table><h1>test58</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test58_avant.png'/></td><td>map.keys();</td><td>[o,lo,bsh,fd,mqb,ss,dhf,tuyp,dhl,rh,nczp,ahs,doxr,qtx,q]</td></table><h1>test59</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHasha_test59_avant.png'/></td><td>map.remove("tx");</td><td>{}<br><img src='../_storage/graphs/MapHasha_test59_ok.png'/></td></tr></table><h1>test60</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "dhl":47, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test60_avant.png'/></td><td>map.remove("dhl");</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test60_ok.png'/></td></tr></table><h1>test61</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test61_avant.png'/></td><td>map.remove("oa");</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test61_ok.png'/></td></tr></table><h1>test62</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test62_avant.png'/></td><td>map.remove("b");</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test62_ok.png'/></td></tr></table><h1>test63</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test63_avant.png'/></td><td>map.remove("yujw");</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test63_ok.png'/></td></tr></table><h1>test64</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test64_avant.png'/></td><td>map.remove("e");</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test64_ok.png'/></td></tr></table><h1>test65</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "doxr":29, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test65_avant.png'/></td><td>map.remove("doxr");</td><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test65_ok.png'/></td></tr></table><h1>test66</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHasha_test66_avant.png'/></td><td>map.clear();</td><td>{}<br><img src='../_storage/graphs/MapHasha_test66_ok.png'/></td></tr></table><h1>test67</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"o":33, "lo":31, "bsh":2, "fd":45, "mqb":83, "ss":82, "dhf":76, "tuyp":73, "rh":26, "nczp":29, "ahs":57, "qtx":18, "q":64}<br><img src='../_storage/graphs/MapHasha_test67_avant.png'/></td><td>map.clear();</td><td>{}<br><img src='../_storage/graphs/MapHasha_test67_ok.png'/></td></tr></table></div>
