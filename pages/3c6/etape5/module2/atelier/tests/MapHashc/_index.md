---
---
<div style='max-width:80%; margin:auto;'><h1>test00</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashc_test00_avant.png'/></td><td>map.put("wqb",&nbsp;76);</td><td>{"wqb":76}<br><img src='../_storage/graphs/MapHashc_test00_ok.png'/></td></tr></table><h1>test01</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"wqb":76}<br><img src='../_storage/graphs/MapHashc_test01_avant.png'/></td><td>map.put("wqb",&nbsp;88);</td><td>{"wqb":88}<br><img src='../_storage/graphs/MapHashc_test01_ok.png'/></td></tr></table><h1>test02</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"wqb":88}<br><img src='../_storage/graphs/MapHashc_test02_avant.png'/></td><td>map.put("wqb",&nbsp;84);</td><td>{"wqb":84}<br><img src='../_storage/graphs/MapHashc_test02_ok.png'/></td></tr></table><h1>test03</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"wqb":84}<br><img src='../_storage/graphs/MapHashc_test03_avant.png'/></td><td>map.put("wqb",&nbsp;65);</td><td>{"wqb":65}<br><img src='../_storage/graphs/MapHashc_test03_ok.png'/></td></tr></table><h1>test04</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"wqb":65}<br><img src='../_storage/graphs/MapHashc_test04_avant.png'/></td><td>map.put("tq",&nbsp;27);</td><td>{"tq":27, "wqb":65}<br><img src='../_storage/graphs/MapHashc_test04_ok.png'/></td></tr></table><h1>test05</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":27, "wqb":65}<br><img src='../_storage/graphs/MapHashc_test05_avant.png'/></td><td>map.put("c",&nbsp;23);</td><td>{"tq":27, "wqb":65, "c":23}<br><img src='../_storage/graphs/MapHashc_test05_ok.png'/></td></tr></table><h1>test06</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":27, "wqb":65, "c":23}<br><img src='../_storage/graphs/MapHashc_test06_avant.png'/></td><td>map.put("wqb",&nbsp;25);</td><td>{"tq":27, "wqb":25, "c":23}<br><img src='../_storage/graphs/MapHashc_test06_ok.png'/></td></tr></table><h1>test07</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":27, "wqb":25, "c":23}<br><img src='../_storage/graphs/MapHashc_test07_avant.png'/></td><td>map.put("r",&nbsp;21);</td><td>{"tq":27, "wqb":25, "r":21, "c":23}<br><img src='../_storage/graphs/MapHashc_test07_ok.png'/></td></tr></table><h1>test08</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":27, "wqb":25, "r":21, "c":23}<br><img src='../_storage/graphs/MapHashc_test08_avant.png'/></td><td>map.put("wqb",&nbsp;86);</td><td>{"tq":27, "wqb":86, "r":21, "c":23}<br><img src='../_storage/graphs/MapHashc_test08_ok.png'/></td></tr></table><h1>test09</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":27, "wqb":86, "r":21, "c":23}<br><img src='../_storage/graphs/MapHashc_test09_avant.png'/></td><td>map.put("r",&nbsp;55);</td><td>{"tq":27, "wqb":86, "r":55, "c":23}<br><img src='../_storage/graphs/MapHashc_test09_ok.png'/></td></tr></table><h1>test10</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":27, "wqb":86, "r":55, "c":23}<br><img src='../_storage/graphs/MapHashc_test10_avant.png'/></td><td>map.put("wqb",&nbsp;70);</td><td>{"tq":27, "wqb":70, "r":55, "c":23}<br><img src='../_storage/graphs/MapHashc_test10_ok.png'/></td></tr></table><h1>test11</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":27, "wqb":70, "r":55, "c":23}<br><img src='../_storage/graphs/MapHashc_test11_avant.png'/></td><td>map.put("tq",&nbsp;42);</td><td>{"tq":42, "wqb":70, "r":55, "c":23}<br><img src='../_storage/graphs/MapHashc_test11_ok.png'/></td></tr></table><h1>test12</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":42, "wqb":70, "r":55, "c":23}<br><img src='../_storage/graphs/MapHashc_test12_avant.png'/></td><td>map.put("r",&nbsp;44);</td><td>{"tq":42, "wqb":70, "r":44, "c":23}<br><img src='../_storage/graphs/MapHashc_test12_ok.png'/></td></tr></table><h1>test13</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":42, "wqb":70, "r":44, "c":23}<br><img src='../_storage/graphs/MapHashc_test13_avant.png'/></td><td>map.put("wqb",&nbsp;57);</td><td>{"tq":42, "wqb":57, "r":44, "c":23}<br><img src='../_storage/graphs/MapHashc_test13_ok.png'/></td></tr></table><h1>test14</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":42, "wqb":57, "r":44, "c":23}<br><img src='../_storage/graphs/MapHashc_test14_avant.png'/></td><td>map.put("c",&nbsp;47);</td><td>{"tq":42, "wqb":57, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test14_ok.png'/></td></tr></table><h1>test15</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":42, "wqb":57, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test15_avant.png'/></td><td>map.put("nhv",&nbsp;47);</td><td>{"tq":42, "wqb":57, "nhv":47, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test15_ok.png'/></td></tr></table><h1>test16</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":42, "wqb":57, "nhv":47, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test16_avant.png'/></td><td>map.put("nhv",&nbsp;34);</td><td>{"tq":42, "wqb":57, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test16_ok.png'/></td></tr></table><h1>test17</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":42, "wqb":57, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test17_avant.png'/></td><td>map.put("wqb",&nbsp;31);</td><td>{"tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test17_ok.png'/></td></tr></table><h1>test18</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test18_avant.png'/></td><td>map.put("r",&nbsp;44);</td><td>{"tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test18_ok.png'/></td></tr></table><h1>test19</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test19_avant.png'/></td><td>map.put("uk",&nbsp;97);</td><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test19_ok.png'/></td></tr></table><h1>test20</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashc_test20_avant.png'/></td><td>map.get("swtx");</td><td>null</td></table><h1>test21</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test21_avant.png'/></td><td>map.get("tq");</td><td>42</td></table><h1>test22</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test22_avant.png'/></td><td>map.get("wqb");</td><td>31</td></table><h1>test23</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test23_avant.png'/></td><td>map.get("h");</td><td>null</td></table><h1>test24</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test24_avant.png'/></td><td>map.get("uk");</td><td>97</td></table><h1>test25</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test25_avant.png'/></td><td>map.get("c");</td><td>47</td></table><h1>test26</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test26_avant.png'/></td><td>map.get("wqb");</td><td>31</td></table><h1>test27</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test27_avant.png'/></td><td>map.get("vyb");</td><td>null</td></table><h1>test28</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test28_avant.png'/></td><td>map.get("fmdy");</td><td>null</td></table><h1>test29</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test29_avant.png'/></td><td>map.get("o");</td><td>null</td></table><h1>test30</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test30_avant.png'/></td><td>map.get("uzf");</td><td>null</td></table><h1>test31</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashc_test31_avant.png'/></td><td>map.size();</td><td>0</td></table><h1>test32</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test32_avant.png'/></td><td>map.size();</td><td>6</td></table><h1>test33</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashc_test33_avant.png'/></td><td>map.isEmpty();</td><td>true</td></table><h1>test34</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test34_avant.png'/></td><td>map.isEmpty();</td><td>false</td></table><h1>test35</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashc_test35_avant.png'/></td><td>map.containsKey("vvcz");</td><td>false</td></table><h1>test36</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test36_avant.png'/></td><td>map.containsKey("lqkn");</td><td>false</td></table><h1>test37</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test37_avant.png'/></td><td>map.containsKey("h");</td><td>false</td></table><h1>test38</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test38_avant.png'/></td><td>map.containsKey("vn");</td><td>false</td></table><h1>test39</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test39_avant.png'/></td><td>map.containsKey("wqb");</td><td>true</td></table><h1>test40</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test40_avant.png'/></td><td>map.containsKey("zry");</td><td>false</td></table><h1>test41</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test41_avant.png'/></td><td>map.containsKey("yek");</td><td>false</td></table><h1>test42</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test42_avant.png'/></td><td>map.containsKey("lz");</td><td>false</td></table><h1>test43</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test43_avant.png'/></td><td>map.containsKey("r");</td><td>true</td></table><h1>test44</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test44_avant.png'/></td><td>map.containsKey("wqb");</td><td>true</td></table><h1>test45</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test45_avant.png'/></td><td>map.containsKey("gxwd");</td><td>false</td></table><h1>test46</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashc_test46_avant.png'/></td><td>map.containsValue("64");</td><td>false</td></table><h1>test47</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test47_avant.png'/></td><td>map.containsValue("55");</td><td>false</td></table><h1>test48</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test48_avant.png'/></td><td>map.containsValue("79");</td><td>false</td></table><h1>test49</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test49_avant.png'/></td><td>map.containsValue("69");</td><td>false</td></table><h1>test50</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test50_avant.png'/></td><td>map.containsValue("49");</td><td>false</td></table><h1>test51</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test51_avant.png'/></td><td>map.containsValue("90");</td><td>false</td></table><h1>test52</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test52_avant.png'/></td><td>map.containsValue("23");</td><td>false</td></table><h1>test53</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test53_avant.png'/></td><td>map.containsValue("90");</td><td>false</td></table><h1>test54</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test54_avant.png'/></td><td>map.containsValue("49");</td><td>false</td></table><h1>test55</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test55_avant.png'/></td><td>map.containsValue("43");</td><td>false</td></table><h1>test56</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test56_avant.png'/></td><td>map.containsValue("31");</td><td>true</td></table><h1>test57</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashc_test57_avant.png'/></td><td>map.keys();</td><td>[]</td></table><h1>test58</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test58_avant.png'/></td><td>map.keys();</td><td>[uk,tq,wqb,nhv,r,c]</td></table><h1>test59</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashc_test59_avant.png'/></td><td>map.remove("afl");</td><td>{}<br><img src='../_storage/graphs/MapHashc_test59_ok.png'/></td></tr></table><h1>test60</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test60_avant.png'/></td><td>map.remove("i");</td><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test60_ok.png'/></td></tr></table><h1>test61</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"uk":97, "tq":42, "wqb":31, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test61_avant.png'/></td><td>map.remove("wqb");</td><td>{"uk":97, "tq":42, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test61_ok.png'/></td></tr></table><h1>test62</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"uk":97, "tq":42, "nhv":34, "r":44, "c":47}<br><img src='../_storage/graphs/MapHashc_test62_avant.png'/></td><td>map.remove("r");</td><td>{"uk":97, "tq":42, "nhv":34, "c":47}<br><img src='../_storage/graphs/MapHashc_test62_ok.png'/></td></tr></table><h1>test63</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"uk":97, "tq":42, "nhv":34, "c":47}<br><img src='../_storage/graphs/MapHashc_test63_avant.png'/></td><td>map.remove("fq");</td><td>{"uk":97, "tq":42, "nhv":34, "c":47}<br><img src='../_storage/graphs/MapHashc_test63_ok.png'/></td></tr></table><h1>test64</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"uk":97, "tq":42, "nhv":34, "c":47}<br><img src='../_storage/graphs/MapHashc_test64_avant.png'/></td><td>map.remove("c");</td><td>{"uk":97, "tq":42, "nhv":34}<br><img src='../_storage/graphs/MapHashc_test64_ok.png'/></td></tr></table><h1>test65</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"uk":97, "tq":42, "nhv":34}<br><img src='../_storage/graphs/MapHashc_test65_avant.png'/></td><td>map.remove("xkj");</td><td>{"uk":97, "tq":42, "nhv":34}<br><img src='../_storage/graphs/MapHashc_test65_ok.png'/></td></tr></table><h1>test66</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='../_storage/graphs/MapHashc_test66_avant.png'/></td><td>map.clear();</td><td>{}<br><img src='../_storage/graphs/MapHashc_test66_ok.png'/></td></tr></table><h1>test67</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"uk":97, "tq":42, "nhv":34}<br><img src='../_storage/graphs/MapHashc_test67_avant.png'/></td><td>map.clear();</td><td>{}<br><img src='../_storage/graphs/MapHashc_test67_ok.png'/></td></tr></table></div>
