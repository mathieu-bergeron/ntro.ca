---
title: "Module 5.2: mappage par hachage"
weight: 30
draft: false
---

{{% pageTitle %}}

1. {{% link "/3c6/etape5/module2/theorie/" "Théorie" %}}

    * {{% link "hachage_gr1" "Fonction de hachage, gr1" %}}
    * {{% link "hachage_gr3" "Fonction de hachage, gr3" %}}
<!--


    * {{% link "hachage_gr1" "Fonction de hachage, gr1" %}}
    * {{% link "hachage_gr2" "Fonction de hachage, gr2" %}}
    * {{% link "hachage_gr3" "Fonction de hachage, gr3" %}}

-->

1. {{% link "/3c6/etape5/module2/entrevue/" "Entrevue 5.2" %}}

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=332691" target="_blank">Mini-test théorique</a>

1. {{% link "/3c6/etape5/module2/atelier" "Atelier 5.2" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}


