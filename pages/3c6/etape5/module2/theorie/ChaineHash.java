public class ChaineHash extends CleHachable<String> {

	public ChaineHash(String valeurJava) {
		super(valeurJava);
	}

	@Override
	public int indice() {
		int indice = 0;
		for(int i = 0; i < getValeur().length(); i++) {
			indice += getValeur().charAt(i);
		}
		return indice;
	}

}
