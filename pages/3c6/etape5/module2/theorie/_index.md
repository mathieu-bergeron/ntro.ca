---
title: ""
weight: 1
bookHidden: true
---


# Théorie 5.2: Map avec hachage

## Fonction et table de hachage

<center>
<video src="06.mp4" width="50%" type="video/mp4" controls playsinline>
</center>

* On veut éviter de chercher l'indice de la clé avec un `indexOf`

* On utiliser une fonction de hachage, c-à-d une méthode qui va
    * calculer un indice pour chaque clé
    * sans chercher d'information sur les autres clés

* Une clé hachable doit implanter la méthode `indice` qui retourne l'indice de la clé

```java
{{% embed "CleHachable.java" %}}
```

* Voici par exemple une clé hachable quand la clé est une chaîne:

```java
{{% embed "ChaineHashb.java" %}}
```

* l'indice est simplement la longueur de la clé

* Une fois qu'on a un indice, on peut mémoriser la valeur dans un tableau

```java
{{% embed "TableHachage.java" %}}
```

* IMPORTANT: la fonction de hachage ne doit **pas** être aléatoire
    * sinon on ne peut plus retrouver une valeur mémorisée par indice!

## Quoi faire en cas de collision?

<center>
<video src="07.mp4" width="50%" type="video/mp4" controls playsinline>
</center>

* Quoi faire s'il y a une collision, c-à-d si deux clés ont le même indice?

```java
{{% embed "TableHachageCollision.java" %}}
```

* En fait, on ne peut pas associer un indice directement à une valeur

* Il faut plutôt associer l'indice à un map naïf:

```java
{{% embed "TableHachage2.java" %}}
```

* S'il n'y a pas souvent de collision:
    * on accède aux valeurs **surtout** par indice 
    * le map naïf associé à l'indice va contenir très peu de valeurs
    * c'est alors efficace

* Au contraire, s'il y beaucoup de collisions:
    * on accède presque toujours aux mêmes indices
    * le map naïf associé à un indice va contenir beaucoup de valeurs
    * on accède donc aux valeurs **surtout** en cherchant la clé
    * c'est alors inefficace

* Donc, une *bonne fonction de hachage* doit:
    * retourner le plus souvent possible un indice différent
    * retourner toujours le même indice pour la même clé (ne pas être aléatoire)

## Implantation d'un map avec hachage

<center>
<video src="08.mp4" width="50%" type="video/mp4" controls playsinline>
</center>

* Voici le début de l'implantation

```java
{{% embed "MapHash.java" %}}
```

* IMPORTANT: 
    * la méthode `CleHachable.indice` peut retourner un indice `>= table.length`
    * il faut adapter l'indice à la table de notre tableau
    * habituellement, on fait ça avec l'opérateur modulo `%`


## Efficacité

<center>
<video src="09.mp4" width="50%" type="video/mp4" controls playsinline>
</center>

* L'efficacité dépend du nombre de collisions, ce qui dépend en fait de:
    * la taille de la table de hachage 
    * la qualité de la fonction de hachage

* On a encore le compromis temps/espace mémoire:
    * pour obtenir plus d'efficacité en temps, on va utiliser plus d'espace mémoire
    
* Exemple:

<center>
    <img src="tous_modifs.png" width="100%"/>
</center>

* La fonction `Hashc` est très efficace, alors que
    * `Hashb` est un peu efficace 
    * `Hasha` équivaut à l'implantation naïve
