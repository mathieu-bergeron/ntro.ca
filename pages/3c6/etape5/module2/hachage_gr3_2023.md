---
title: "Révision, fonction de hachage"
weight: 30
draft: false
bookHidden: true
---

{{% pageTitle %}}

## Fonction de hachage

1. Fonction de hachage invalide
    * je donne la même clé deux fois, et ça retourne des indices différents

1. Fonction de hachage valide, mais inefficace
    * pour n'importe quelle clé, ça retourne toujours le même indice
    * beacoup de collisions

1. Fonction de hachage valide et efficace
    * éparpillé dans le tableau
    * deux clés différentes ont une forte chance d'avoir des indices différents
    * éviter les collisions


## Qualités et défauts de la table de hachage (par rapport au MapNaif)

1. Qualités
    * efficace => si tout va bien, c'est O(1) pour put et get (temps constant)
 
1. Défauts
    * prends plus d'espace mémoire qu'on a de valeurs
    * pas un bon candidat pour une BD
