---
title: "Module 5.3: mappage par arbre"
weight: 30
draft: false
---

{{% pageTitle %}}

1. {{% link "/3c6/etape5/module3/theorie/" "Théorie" %}}

1. {{% link "/3c6/etape5/module3/entrevue/" "Entrevue 5.3" %}}

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=332695" target="_blank">Mini-test théorique</a>

1. Atelier
    * exercice A: {{% link "/3c6/etape5/module3/atelier/arbre" "implanter un arbre" %}}
    * exercice B: {{% link "/3c6/etape5/module3/atelier/mappage_arbre" "implanter un mappage par arbre" %}}

1. Remises 
    * {{% link "/3c6/etape5/module3/atelier/remise_gitlab" "sur GitLab" %}}
    * {{% link "/3c6/etape5/module3/atelier/remise_moodle" "sur Moodle" %}}


<br>
<br>



{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}

