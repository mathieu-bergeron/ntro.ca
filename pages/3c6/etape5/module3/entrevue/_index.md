---
title: "Entrevue 5.3: mappage par arbre"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier 

## Arbre équilibré ou pas

* Considérer les deux arbres ci-bas

<img src="arbre01.png" />

<img src="arbre02.png" />


* Est-ce que l'arbre est équilibré? Sinon, dessiner une version équilibré de l'arbre

## Interpréter un map par arbre

* Considérer le map par arbre suivant

<img src="map01.png" />

* Écrire ce mappage en JSON
