public class TesteurDeMapArbre extends TesteurDeMapAbstrait {
    
    @Override
    public MapJava<Cle<String>, Integer> nouveauMap() {
        return new MapArbre();
    }

    @Override
    public CleComparable<String> nouvelleCle(String valeur) {
        return new ChaineComparable(valeur);
    }

    @Override
    public void accederAuxClesDansOrdre(MapJava<Cle<String>, Integer> map) {
        /*
         * TODO: cette fois-ci pas besoin de trier les clés!
         * 
         */

        //afficherCle("CLÉ ARBRE: " + cle.getValeur());
    }
}
