/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package atelier5_3_B;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import tutoriels.mappage.Cle;
import tutoriels.mappage.MapJava;
import tutoriels.mappage.TesteurDeMap;

public abstract class TesteurDeMapAbstrait extends TesteurDeMap {
	
	private static final Random alea = new Random();

	@Override
	public void fairePlusieursAjoutsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations) {
		for(int i = 0; i < nombreOperations; i++) {
			map.put(cles[alea.nextInt(cles.length)], 0);
		}
	}

	@Override
	public void fairePlusieursModificationsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations) {
		for(int i = 0; i < nombreOperations; i++) {
			map.put(cles[alea.nextInt(cles.length)], 0);
		}
	}

	@Override
	public void fairePlusieursRetraitsAleatoires(MapJava<Cle<String>, Integer> map, Cle<String>[] cles, int nombreOperations) {
		for(int i = 0; i < nombreOperations; i++) {
			map.remove(cles[alea.nextInt(cles.length)]);
		}
	}

	@Override
	public void accederAuxClesDansOrdre(MapJava<Cle<String>, Integer> map) {
		List<Cle<String>> cles = map.keys();
		
		List<String> valeursCles = new ArrayList<>();
		for(Cle<String> cle : cles) {
			valeursCles.add(cle.getValeur());
		}
		
		Collections.sort(valeursCles);

		for(String valeurCle : valeursCles) {
			afficherCle("CLÉ: " + valeurCle);
			//System.out.println("CLÉ: " + valeurCle);
		}
	}


}
