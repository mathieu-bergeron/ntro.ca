public class MonAtelier5_3_B extends Atelier5_3_B {
    
    public static void main(String[] args) {
        
        (new MonAtelier5_3_B()).valider();
    }

    @Override
    public TesteurDeMap fournirTesteurDeMapHash() {
        return new TesteurDeMapHashc();
    }

    @Override
    public TesteurDeMap fournirTesteurDeMapArbre() {
        return new TesteurDeMapArbre();
    }
}
