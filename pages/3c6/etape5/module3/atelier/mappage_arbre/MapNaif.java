/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package atelier5_3_B;

import java.util.ArrayList;
import java.util.List;

import tutoriels.mappage.Cle;
import tutoriels.mappage.MapJava;

public class MapNaif <C extends Cle<?>, V extends Object> extends MapJava<C,V> {
	
	private List<C> cles = new ArrayList<>();
	private List<V> valeurs = new ArrayList<>();

	public List<C> getCles() {
		return cles;
	}

	public void setCles(List<C> cles) {
		this.cles = cles;
	}

	public List<V> getValeurs() {
		return valeurs;
	}

	public void setValeurs(List<V> valeurs) {
		this.valeurs = valeurs;
	}

	@Override
	public void put(C c, V v) {
		int indiceCle = cles.indexOf(c);
		
		if(indiceCle != -1) {
			valeurs.set(indiceCle, v);
		}else {
			cles.add(c);
			valeurs.add(v);
		}
	}

	@Override
	public V get(C c) {
		V valeur = null;
		
		int indiceCle = cles.indexOf(c);
		
		if(indiceCle != -1) {
			valeur = valeurs.get(indiceCle);
		}

		return valeur;
	}

	@Override
	public void clear() {
		cles.clear();
		valeurs.clear();
	}

	@Override
	public int size() {
		return cles.size();
	}

	@Override
	public boolean isEmpty() {
		return cles.isEmpty();
	}

	@Override
	public boolean containsKey(C c) {
		return cles.contains(c);
	}

	@Override
	public boolean containsValue(V v) {
		return valeurs.contains(v);
	}

	@Override
	public void remove(C c) {
		int indiceCle = cles.indexOf(c);
		
		if(indiceCle != -1) {
			cles.remove(indiceCle);
			valeurs.remove(indiceCle);
		}
	}

	@Override
	public List<C> keys() {
		return cles;
	}
}
