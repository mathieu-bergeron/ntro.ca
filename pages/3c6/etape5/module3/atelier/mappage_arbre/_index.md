---
title: "Atelier 5.3, exercice B: implanter un mappage par arbre"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Exemple de tests réussis

* <a href="./MapArbre">MapArbre</a>

## Recopier des classes à partir de l'`atelier5_2`

* Recopier les classes suivantes:

    * `MapNaif`
    * `MapHash`
    * `ChaineHashc`
    * `TesteurDeMapAbstrait`
    * `TesteurDeMapHashc`

## Recopier des classes à partir de l'`atelier5_3_A`

* Recopier les classes suivantes:

    * `MonNoeud`
    * `MonArbre`

## Créer la classe `ChaineComparable`

* Ajouter la classe suivante au **paquet** `atelier5_3_B`
    * Nom de la classe: `ChaineComparable`

* Débuter l'implantation comme suit:

```java
{{% embed "ChaineComparable.java" %}}
```

## Créer la classe `MaPairePourMap`

* Ajouter la classe suivante au **paquet** `atelier5_3_B`
    * Nom de la classe: `MaPairePourMap`

* Débuter l'implantation comme suit:

```java
{{% embed "MaPairePourMap.java" %}}
```

## Créer la classe `MapArbre`

* Ajouter la classe suivante au **paquet** `atelier5_3_B`
    * Nom de la classe: `MapArbre`

* Débuter l'implantation comme suit:

```java
{{% embed "MapArbre.java" %}}
```

## Créer la classe `TesteurDeMapArbre`

* Ajouter la classe suivante au **paquet** `atelier5_3_B`
    * Nom de la classe: `TesteurDeMapArbre`

* Implanter comme suit:

```java
{{% embed "TesteurDeMapArbre.java" %}}
```

## Créer la classe `MonAtelier5_3_B`

* Ajouter la classe suivante au **paquet** `atelier5_3_B`
    * Nom de la classe: `MonAtelier5_3_B`

* Implanter comme suit:

```java
{{% embed "MonAtelier5_3_B.java" %}}
```


## Compléter les implantations

* Utiliser l'outil de validation pour tester votre code

<img src="validation.png"/>

* vous pouvez aussi afficher les fichiers `html`:

    * `MapArbre.html`
    * (à la racine du projet)

## Tester l'efficacité du code

* Les méthodes de votre `TesteurDeMapAbstrait` seront appelées
    * typiquement, utiliser une boucle pour effectuer une opération un certain nombre de fois

* Vous devriez avoir des résultats similaires aux résultats ci-bas

    * `fairePlusieursAjoutsAleatoires`:

    <img src="fairePlusieursAjoutsAleatoires.png"/>

    * `fairePlusieursModificationsAleatoires`:

    <img src="fairePlusieursModificationsAleatoires.png"/>

    * `fairePlusieursRetraitsAleatoires`:

    <img src="fairePlusieursRetraitsAleatoires.png"/>

    * `accederAuxClesDansOrdre` (*):

        <img src="accederAuxClesDansOrdre.png"/>

        * (*) ce graphe semble varier beaucoup d'un ordi à l'autre
