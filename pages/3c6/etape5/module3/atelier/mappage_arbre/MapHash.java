/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package atelier5_3_B;

import java.util.ArrayList;
import java.util.List;

import tutoriels.mappage.CleHachable;
import tutoriels.mappage.MapJava;

public class MapHash <C extends CleHachable<?>, V extends Object> extends MapJava<C,V> {
	
	private static final int TAILLE_TABLE_HACHAGE = 20;
	
	@SuppressWarnings("unchecked")
	private MapNaif<C,V>[] table = new MapNaif[TAILLE_TABLE_HACHAGE];
	private int taille = 0;

	public MapNaif<C, V>[] getTable() {
		return table;
	}

	public void setTable(MapNaif<C, V>[] table) {
		this.table = table;
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	private int indiceTable(C c) {
		int indiceBrut = c.indice();
		return indiceBrut % TAILLE_TABLE_HACHAGE;
	}

	@Override
	public void put(C c, V v) {
		int indiceTable = indiceTable(c);
		MapNaif<C,V> mapPourIndice = table[indiceTable];
		
		if(mapPourIndice == null) {
			mapPourIndice = new MapNaif<C,V>();
			table[indiceTable] = mapPourIndice;
		}
		
		int tailleMapPourIndiceAvant = mapPourIndice.size();

		mapPourIndice.put(c, v);
		
		int tailleMapPourIndiceApres = mapPourIndice.size();
		
		taille += (tailleMapPourIndiceApres - tailleMapPourIndiceAvant);
		
	}

	@Override
	public V get(C c) {
		V valeur = null;

		int indiceTable = indiceTable(c);
		MapNaif<C,V> mapPourIndice = table[indiceTable];
		
		if(mapPourIndice != null) {
			valeur = mapPourIndice.get(c);
		}
		
		return valeur;
	}

	@Override
	public void clear() {
		table = new MapNaif[TAILLE_TABLE_HACHAGE];
		taille = 0;
	}

	@Override
	public int size() {
		return taille;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public boolean containsKey(C c) {
		boolean siContient = false;

		int indiceTable = indiceTable(c);
		MapNaif<C,V> mapPourIndice = table[indiceTable];
		
		if(mapPourIndice != null) {
			siContient = mapPourIndice.containsKey(c);
		}

		return siContient;
	}

	@Override
	public boolean containsValue(V v) {
		boolean siContient = false;
		
		for(int i = 0; i < table.length; i++) {
			if(table[i] != null && table[i].containsValue(v)) {
				siContient = true;
				break;
			}
		}

		return siContient;
	}

	@Override
	public void remove(C c) {
		int indiceTable = indiceTable(c);
		MapNaif<C,V> mapPourIndice = table[indiceTable];
		
		if(mapPourIndice == null) {
			mapPourIndice = new MapNaif<C,V>();
			table[indiceTable] = mapPourIndice;
		}
		
		int tailleMapPourIndiceAvant = mapPourIndice.size();

		mapPourIndice.remove(c);
		
		int tailleMapPourIndiceApres = mapPourIndice.size();
		
		taille += (tailleMapPourIndiceApres - tailleMapPourIndiceAvant);
	}

	@Override
	public List<C> keys() {
		List<C> cles = new ArrayList<>();

		for(int i = 0; i < table.length; i++) {
			if(table[i] != null) {
				cles.addAll(table[i].keys());
			}
		}
		
		return cles;
	}

}
