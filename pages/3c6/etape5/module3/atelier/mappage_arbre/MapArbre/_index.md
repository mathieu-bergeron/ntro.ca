---
---
<div style='max-width:80%; margin:auto;'><h1>test00</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='./graphs/MapArbre_test00_avant.png'/></td><td>map.put("b",&nbsp;13);</td><td>{"b":13}<br><img src='./graphs/MapArbre_test00_ok.png'/></td></tr></table><h1>test01</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":13}<br><img src='./graphs/MapArbre_test01_avant.png'/></td><td>map.put("pjm",&nbsp;90);</td><td>{"b":13, "pjm":90}<br><img src='./graphs/MapArbre_test01_ok.png'/></td></tr></table><h1>test02</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":13, "pjm":90}<br><img src='./graphs/MapArbre_test02_avant.png'/></td><td>map.put("bh",&nbsp;56);</td><td>{"b":13, "bh":56, "pjm":90}<br><img src='./graphs/MapArbre_test02_ok.png'/></td></tr></table><h1>test03</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":13, "bh":56, "pjm":90}<br><img src='./graphs/MapArbre_test03_avant.png'/></td><td>map.put("b",&nbsp;88);</td><td>{"b":88, "bh":56, "pjm":90}<br><img src='./graphs/MapArbre_test03_ok.png'/></td></tr></table><h1>test04</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":88, "bh":56, "pjm":90}<br><img src='./graphs/MapArbre_test04_avant.png'/></td><td>map.put("pjm",&nbsp;84);</td><td>{"b":88, "bh":56, "pjm":84}<br><img src='./graphs/MapArbre_test04_ok.png'/></td></tr></table><h1>test05</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":88, "bh":56, "pjm":84}<br><img src='./graphs/MapArbre_test05_avant.png'/></td><td>map.put("v",&nbsp;77);</td><td>{"b":88, "bh":56, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test05_ok.png'/></td></tr></table><h1>test06</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":88, "bh":56, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test06_avant.png'/></td><td>map.put("lw",&nbsp;94);</td><td>{"b":88, "bh":56, "lw":94, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test06_ok.png'/></td></tr></table><h1>test07</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":88, "bh":56, "lw":94, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test07_avant.png'/></td><td>map.put("bh",&nbsp;30);</td><td>{"b":88, "bh":30, "lw":94, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test07_ok.png'/></td></tr></table><h1>test08</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":88, "bh":30, "lw":94, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test08_avant.png'/></td><td>map.put("bh",&nbsp;82);</td><td>{"b":88, "bh":82, "lw":94, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test08_ok.png'/></td></tr></table><h1>test09</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":88, "bh":82, "lw":94, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test09_avant.png'/></td><td>map.put("bh",&nbsp;33);</td><td>{"b":88, "bh":33, "lw":94, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test09_ok.png'/></td></tr></table><h1>test10</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":88, "bh":33, "lw":94, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test10_avant.png'/></td><td>map.put("hm",&nbsp;3);</td><td>{"b":88, "bh":33, "hm":3, "lw":94, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test10_ok.png'/></td></tr></table><h1>test11</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":88, "bh":33, "hm":3, "lw":94, "pjm":84, "v":77}<br><img src='./graphs/MapArbre_test11_avant.png'/></td><td>map.put("v",&nbsp;98);</td><td>{"b":88, "bh":33, "hm":3, "lw":94, "pjm":84, "v":98}<br><img src='./graphs/MapArbre_test11_ok.png'/></td></tr></table><h1>test12</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":88, "bh":33, "hm":3, "lw":94, "pjm":84, "v":98}<br><img src='./graphs/MapArbre_test12_avant.png'/></td><td>map.put("b",&nbsp;13);</td><td>{"b":13, "bh":33, "hm":3, "lw":94, "pjm":84, "v":98}<br><img src='./graphs/MapArbre_test12_ok.png'/></td></tr></table><h1>test13</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":13, "bh":33, "hm":3, "lw":94, "pjm":84, "v":98}<br><img src='./graphs/MapArbre_test13_avant.png'/></td><td>map.put("pj",&nbsp;82);</td><td>{"b":13, "bh":33, "hm":3, "lw":94, "pj":82, "pjm":84, "v":98}<br><img src='./graphs/MapArbre_test13_ok.png'/></td></tr></table><h1>test14</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":13, "bh":33, "hm":3, "lw":94, "pj":82, "pjm":84, "v":98}<br><img src='./graphs/MapArbre_test14_avant.png'/></td><td>map.put("gkbw",&nbsp;51);</td><td>{"b":13, "bh":33, "gkbw":51, "hm":3, "lw":94, "pj":82, "pjm":84, "v":98}<br><img src='./graphs/MapArbre_test14_ok.png'/></td></tr></table><h1>test15</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":13, "bh":33, "gkbw":51, "hm":3, "lw":94, "pj":82, "pjm":84, "v":98}<br><img src='./graphs/MapArbre_test15_avant.png'/></td><td>map.put("v",&nbsp;32);</td><td>{"b":13, "bh":33, "gkbw":51, "hm":3, "lw":94, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test15_ok.png'/></td></tr></table><h1>test16</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":13, "bh":33, "gkbw":51, "hm":3, "lw":94, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test16_avant.png'/></td><td>map.put("kex",&nbsp;68);</td><td>{"b":13, "bh":33, "gkbw":51, "hm":3, "kex":68, "lw":94, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test16_ok.png'/></td></tr></table><h1>test17</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":13, "bh":33, "gkbw":51, "hm":3, "kex":68, "lw":94, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test17_avant.png'/></td><td>map.put("lw",&nbsp;23);</td><td>{"b":13, "bh":33, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test17_ok.png'/></td></tr></table><h1>test18</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":13, "bh":33, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test18_avant.png'/></td><td>map.put("bh",&nbsp;9);</td><td>{"b":13, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test18_ok.png'/></td></tr></table><h1>test19</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":13, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test19_avant.png'/></td><td>map.put("b",&nbsp;41);</td><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test19_ok.png'/></td></tr></table><h1>test20</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./graphs/MapArbre_test20_avant.png'/></td><td>map.get("ofyf");</td><td>null</td></table><h1>test21</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test21_avant.png'/></td><td>map.get("bd");</td><td>null</td></table><h1>test22</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test22_avant.png'/></td><td>map.get("hm");</td><td>3</td></table><h1>test23</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test23_avant.png'/></td><td>map.get("b");</td><td>41</td></table><h1>test24</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test24_avant.png'/></td><td>map.get("bh");</td><td>9</td></table><h1>test25</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test25_avant.png'/></td><td>map.get("v");</td><td>32</td></table><h1>test26</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test26_avant.png'/></td><td>map.get("wqv");</td><td>null</td></table><h1>test27</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test27_avant.png'/></td><td>map.get("gkbw");</td><td>51</td></table><h1>test28</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test28_avant.png'/></td><td>map.get("kv");</td><td>null</td></table><h1>test29</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test29_avant.png'/></td><td>map.get("butv");</td><td>null</td></table><h1>test30</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test30_avant.png'/></td><td>map.get("uny");</td><td>null</td></table><h1>test31</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./graphs/MapArbre_test31_avant.png'/></td><td>map.size();</td><td>0</td></table><h1>test32</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test32_avant.png'/></td><td>map.size();</td><td>9</td></table><h1>test33</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./graphs/MapArbre_test33_avant.png'/></td><td>map.isEmpty();</td><td>true</td></table><h1>test34</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test34_avant.png'/></td><td>map.isEmpty();</td><td>false</td></table><h1>test35</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./graphs/MapArbre_test35_avant.png'/></td><td>map.containsKey("o");</td><td>false</td></table><h1>test36</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test36_avant.png'/></td><td>map.containsKey("kex");</td><td>true</td></table><h1>test37</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test37_avant.png'/></td><td>map.containsKey("njq");</td><td>false</td></table><h1>test38</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test38_avant.png'/></td><td>map.containsKey("crvd");</td><td>false</td></table><h1>test39</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test39_avant.png'/></td><td>map.containsKey("pj");</td><td>true</td></table><h1>test40</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test40_avant.png'/></td><td>map.containsKey("wk");</td><td>false</td></table><h1>test41</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test41_avant.png'/></td><td>map.containsKey("ldd");</td><td>false</td></table><h1>test42</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test42_avant.png'/></td><td>map.containsKey("oro");</td><td>false</td></table><h1>test43</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test43_avant.png'/></td><td>map.containsKey("b");</td><td>true</td></table><h1>test44</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test44_avant.png'/></td><td>map.containsKey("lw");</td><td>true</td></table><h1>test45</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test45_avant.png'/></td><td>map.containsKey("k");</td><td>false</td></table><h1>test46</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./graphs/MapArbre_test46_avant.png'/></td><td>map.containsValue("3");</td><td>false</td></table><h1>test47</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test47_avant.png'/></td><td>map.containsValue("22");</td><td>false</td></table><h1>test48</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test48_avant.png'/></td><td>map.containsValue("39");</td><td>false</td></table><h1>test49</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test49_avant.png'/></td><td>map.containsValue("20");</td><td>false</td></table><h1>test50</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test50_avant.png'/></td><td>map.containsValue("22");</td><td>false</td></table><h1>test51</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test51_avant.png'/></td><td>map.containsValue("64");</td><td>false</td></table><h1>test52</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test52_avant.png'/></td><td>map.containsValue("58");</td><td>false</td></table><h1>test53</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test53_avant.png'/></td><td>map.containsValue("0");</td><td>false</td></table><h1>test54</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test54_avant.png'/></td><td>map.containsValue("61");</td><td>false</td></table><h1>test55</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test55_avant.png'/></td><td>map.containsValue("39");</td><td>false</td></table><h1>test56</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test56_avant.png'/></td><td>map.containsValue("87");</td><td>false</td></table><h1>test57</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{}<br><img src='./graphs/MapArbre_test57_avant.png'/></td><td>map.keys();</td><td>[]</td></table><h1>test58</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test58_avant.png'/></td><td>map.keys();</td><td>[b,bh,gkbw,hm,kex,lw,pj,pjm,v]</td></table><h1>test59</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='./graphs/MapArbre_test59_avant.png'/></td><td>map.remove("dero");</td><td>{}<br><img src='./graphs/MapArbre_test59_ok.png'/></td></tr></table><h1>test60</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":41, "bh":9, "gkbw":51, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test60_avant.png'/></td><td>map.remove("gkbw");</td><td>{"b":41, "bh":9, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test60_ok.png'/></td></tr></table><h1>test61</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":41, "bh":9, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test61_avant.png'/></td><td>map.remove("q");</td><td>{"b":41, "bh":9, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test61_ok.png'/></td></tr></table><h1>test62</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":41, "bh":9, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test62_avant.png'/></td><td>map.remove("s");</td><td>{"b":41, "bh":9, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test62_ok.png'/></td></tr></table><h1>test63</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":41, "bh":9, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test63_avant.png'/></td><td>map.remove("zl");</td><td>{"b":41, "bh":9, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test63_ok.png'/></td></tr></table><h1>test64</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":41, "bh":9, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test64_avant.png'/></td><td>map.remove("bh");</td><td>{"b":41, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test64_ok.png'/></td></tr></table><h1>test65</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":41, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test65_avant.png'/></td><td>map.remove("ritk");</td><td>{"b":41, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test65_ok.png'/></td></tr></table><h1>test66</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{}<br><img src='./graphs/MapArbre_test66_avant.png'/></td><td>map.clear();</td><td>{}<br><img src='./graphs/MapArbre_test66_ok.png'/></td></tr></table><h1>test67</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td>{"b":41, "hm":3, "kex":68, "lw":23, "pj":82, "pjm":84, "v":32}<br><img src='./graphs/MapArbre_test67_avant.png'/></td><td>map.clear();</td><td>{}<br><img src='./graphs/MapArbre_test67_ok.png'/></td></tr></table></div>
