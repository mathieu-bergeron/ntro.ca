public class MapArbre <C extends CleComparable<C>, V extends Object> extends MapJava<C,V> {
    
    private MonArbre<PairePourMap<C,V>> arbre = new MonArbre<>();

    public MonArbre<PairePourMap<C, V>> getArbre() {
        return arbre;
    }

    public void setArbre(MonArbre<PairePourMap<C, V>> arbre) {
        this.arbre = arbre;
    }

    @Override
    public void put(C c, V v) {
        // TODO
    }

    @Override
    public V get(C c) {
        // TODO
    }

    @Override
    public void clear() {
        // TODO
    }

    @Override
    public int size() {
        // TODO
    }

    @Override
    public boolean isEmpty() {
        // TODO
    }

    @Override
    public boolean containsKey(C c) {
        // TODO
    }

    @Override
    public boolean containsValue(V v) {
        // TODO
    }

    @Override
    public void remove(C c) {
        // TODO
        //
        // trouver le bon noeud
        // appeler la méthode seRetirer() sur ce noeud
    }

    @Override
    public List<C> keys() {
        // TODO
    }
}
