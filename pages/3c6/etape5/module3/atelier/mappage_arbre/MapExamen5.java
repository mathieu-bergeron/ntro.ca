/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package atelier5_3_B;

import java.util.List;

import tutoriels.mappage.Cle;
import tutoriels.mappage.MapJava;

public class MapExamen5 <C extends Cle<?>, V extends Object> extends MapJava<C,V> {
	
	// La clé est une List<Character> où les caractères sont a,b,c ou d
	// mapSuivants sont indexé par caractère
	// si on est «rendu» (la cle est vide) alors on est à la valeurIci

	private MapExamen5[] mapSuivants = new MapExamen5[4];
	private V valeurIci = null;
	
	
	@Override
	public void put(C c, V v) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public V get(C c) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean containsKey(C c) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean containsValue(V v) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void remove(C c) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public List<C> keys() {
		// TODO Auto-generated method stub
		return null;
	}
}
