/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package atelier5_3_A;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ca.ntro.core.system.debug.T;
import tutoriels.core.TestCaseGenerator;
import tutoriels.core.models.test_cases.MethodOrdering;

public class GenerateurAtelier5_3_A extends TestCaseGenerator {

	static {
		// XXX: this provokes initialization
		new MonAtelier5_3_A();
	}
	
	public static void main(String[] args) {
		T.call(GenerateurAtelier5_3_A.class);
		
		MethodOrdering.desiredMethodOrder.add("add");
		MethodOrdering.desiredMethodOrder.add("addAll");
		MethodOrdering.desiredMethodOrder.add("insert");
		MethodOrdering.desiredMethodOrder.add("set");
		MethodOrdering.desiredMethodOrder.add("get");
		MethodOrdering.desiredMethodOrder.add("size");
		MethodOrdering.desiredMethodOrder.add("isEmpty");
		MethodOrdering.desiredMethodOrder.add("contains");
		MethodOrdering.desiredMethodOrder.add("indexOf");
		MethodOrdering.desiredMethodOrder.add("removeValue");
		MethodOrdering.desiredMethodOrder.add("remove");
		MethodOrdering.desiredMethodOrder.add("clear");

		TestCaseGenerator.generateTestCases(GenerateurAtelier5_3_A.class, MonAtelier5_3_A.class);
	}

	private Random random = new Random();

	@Override
	public List<List<Object>> generateMultipleInputArguments(String providerMethodName, Object providedObject,
			Method methodToTest) {
		
		List<List<Object>> listOfInputArguments = new ArrayList<>();
		
		if(methodToTest.getName().equals("add")) {
			List<Object> args = new ArrayList<>();
			
			args.add(1);
			
			listOfInputArguments.add(args);
		}
		else if(methodToTest.getName().equals("insert")) {
			List<Object> args = new ArrayList<>();

			if(providerMethodName.equals("fournirListeNaive01")) {
				
				args.add(0);
				args.add(0);
				listOfInputArguments.add(args);
				
			}else if(providerMethodName.equals("fournirListeNaive02")) {

				args.add(1);
				args.add(0);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive03")) {

				args.add(1);
				args.add(0);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive04")) {

				args.add(2);
				args.add(0);
				listOfInputArguments.add(args);
				
				
			}
			
			
		}
		else if(methodToTest.getName().equals("addAll")) {
			List<Object> args = new ArrayList<>();
			
			args.add(new Integer[] {10,11,12});
			
			listOfInputArguments.add(args);
			
		}
		else if(methodToTest.getName().equals("set")) {
			List<Object> args = new ArrayList<>();

			if(providerMethodName.equals("fournirListeNaive01")) {
				
				args.add(0);
				args.add(0);
				listOfInputArguments.add(args);
				
			}else if(providerMethodName.equals("fournirListeNaive02")) {

				args.add(1);
				args.add(0);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive03")) {

				args.add(1);
				args.add(0);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive04")) {

				args.add(2);
				args.add(0);
				listOfInputArguments.add(args);
				
			}
			
			
		}
		else if(methodToTest.getName().equals("get")) {
			List<Object> args = new ArrayList<>();

			if(providerMethodName.equals("fournirListeNaive01")) {
				
				args.add(0);
				listOfInputArguments.add(args);
				
			}else if(providerMethodName.equals("fournirListeNaive02")) {

				args.add(1);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive03")) {

				args.add(1);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive04")) {

				args.add(2);
				listOfInputArguments.add(args);
				
			}
			
			
		}
		else if(methodToTest.getName().equals("clear")) {
			List<Object> args = new ArrayList<>();
			
			listOfInputArguments.add(args);
			
		}
		else if(methodToTest.getName().equals("size")) {
			List<Object> args = new ArrayList<>();
			
			listOfInputArguments.add(args);
			
		}
		else if(methodToTest.getName().equals("isEmpty")) {
			List<Object> args = new ArrayList<>();
			
			listOfInputArguments.add(args);
			
		}
		else if(methodToTest.getName().equals("contains")) {
			List<Object> args = new ArrayList<>();

			if(providerMethodName.equals("fournirListeNaive01")) {
				
				args.add(0);
				listOfInputArguments.add(args);
				
			}else if(providerMethodName.equals("fournirListeNaive02")) {

				args.add(1);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive03")) {

				args.add(1);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive04")) {

				args.add(2);
				listOfInputArguments.add(args);
				
			}
			
		}
		else if(methodToTest.getName().equals("indexOf")) {
			List<Object> args = new ArrayList<>();

			if(providerMethodName.equals("fournirListeNaive01")) {
				
				args.add(0);
				listOfInputArguments.add(args);
				
			}else if(providerMethodName.equals("fournirListeNaive02")) {

				args.add(1);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive03")) {

				args.add(1);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive04")) {

				args.add(2);
				listOfInputArguments.add(args);
				
			}
			
			
		}
		else if(methodToTest.getName().equals("removeValue")) {
			List<Object> args = new ArrayList<>();

			if(providerMethodName.equals("fournirListeNaive01")) {
				
				args.add(1);
				listOfInputArguments.add(args);
				
			}else if(providerMethodName.equals("fournirListeNaive02")) {

				args.add(1);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive03")) {

				args.add(1);
				listOfInputArguments.add(args);
				
			}else if(providerMethodName.equals("fournirListeNaive04")) {

				args.add(1);
				listOfInputArguments.add(args);
				
			}
		}
		else if(methodToTest.getName().equals("remove")) {
			List<Object> args = new ArrayList<>();

			if(providerMethodName.equals("fournirListeNaive01")) {
				
				args.add(0);
				listOfInputArguments.add(args);
				
			}else if(providerMethodName.equals("fournirListeNaive02")) {

				args.add(1);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive03")) {

				args.add(1);
				listOfInputArguments.add(args);
				
				
			}else if(providerMethodName.equals("fournirListeNaive04")) {

				args.add(2);
				listOfInputArguments.add(args);
				
			}
			
			
		}

		return listOfInputArguments;
	}
}
