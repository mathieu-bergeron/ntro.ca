public class MonAtelier5_3_A extends Atelier5_3_A {
    
    public static void main(String[] args) {
        new MonAtelier5_3_A().valider();
    }

    @Override
    public Arbre<Character> fournirArbre() {
        return new MonArbre<Character>();
    }
}
