---
---
<div style='max-width:80%; margin:auto;'><h1>test00</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test00_avant.png'/></td><td>arbre.ajouter(v);</td><td><br><img src='./graphs/MonArbre_test00_ok.png'/></td></tr></table><h1>test01</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test01_avant.png'/></td><td>arbre.ajouter(k);</td><td><br><img src='./graphs/MonArbre_test01_ok.png'/></td></tr></table><h1>test02</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test02_avant.png'/></td><td>arbre.ajouter(k);</td><td><br><img src='./graphs/MonArbre_test02_ok.png'/></td></tr></table><h1>test03</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test03_avant.png'/></td><td>arbre.ajouter(m);</td><td><br><img src='./graphs/MonArbre_test03_ok.png'/></td></tr></table><h1>test04</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test04_avant.png'/></td><td>arbre.ajouter(x);</td><td><br><img src='./graphs/MonArbre_test04_ok.png'/></td></tr></table><h1>test05</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test05_avant.png'/></td><td>arbre.ajouter(w);</td><td><br><img src='./graphs/MonArbre_test05_ok.png'/></td></tr></table><h1>test06</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test06_avant.png'/></td><td>arbre.ajouter(b);</td><td><br><img src='./graphs/MonArbre_test06_ok.png'/></td></tr></table><h1>test07</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test07_avant.png'/></td><td>arbre.ajouter(g);</td><td><br><img src='./graphs/MonArbre_test07_ok.png'/></td></tr></table><h1>test08</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test08_avant.png'/></td><td>arbre.ajouter(b);</td><td><br><img src='./graphs/MonArbre_test08_ok.png'/></td></tr></table><h1>test09</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test09_avant.png'/></td><td>arbre.ajouter(w);</td><td><br><img src='./graphs/MonArbre_test09_ok.png'/></td></tr></table><h1>test10</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test10_avant.png'/></td><td>arbre.ajouter(t);</td><td><br><img src='./graphs/MonArbre_test10_ok.png'/></td></tr></table><h1>test11</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test11_avant.png'/></td><td>arbre.ajouter(g);</td><td><br><img src='./graphs/MonArbre_test11_ok.png'/></td></tr></table><h1>test12</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test12_avant.png'/></td><td>arbre.ajouter(b);</td><td><br><img src='./graphs/MonArbre_test12_ok.png'/></td></tr></table><h1>test13</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test13_avant.png'/></td><td>arbre.ajouter(x);</td><td><br><img src='./graphs/MonArbre_test13_ok.png'/></td></tr></table><h1>test14</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test14_avant.png'/></td><td>arbre.ajouter(x);</td><td><br><img src='./graphs/MonArbre_test14_ok.png'/></td></tr></table><h1>test15</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test15_avant.png'/></td><td>arbre.ajouter(v);</td><td><br><img src='./graphs/MonArbre_test15_ok.png'/></td></tr></table><h1>test16</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test16_avant.png'/></td><td>arbre.ajouter(v);</td><td><br><img src='./graphs/MonArbre_test16_ok.png'/></td></tr></table><h1>test17</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test17_avant.png'/></td><td>arbre.ajouter(n);</td><td><br><img src='./graphs/MonArbre_test17_ok.png'/></td></tr></table><h1>test18</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test18_avant.png'/></td><td>arbre.ajouter(h);</td><td><br><img src='./graphs/MonArbre_test18_ok.png'/></td></tr></table><h1>test19</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test19_avant.png'/></td><td>arbre.ajouter(q);</td><td><br><img src='./graphs/MonArbre_test19_ok.png'/></td></tr></table><h1>test20</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td><br><img src='./graphs/MonArbre_test20_avant.png'/></td><td>arbre.racine();</td><td>null</td></table><h1>test21</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td><br><img src='./graphs/MonArbre_test21_avant.png'/></td><td>arbre.racine();</td><td>v</td></table><h1>test22</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td><br><img src='./graphs/MonArbre_test22_avant.png'/></td><td>arbre.racine();</td><td>t</td></table><h1>test23</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td><br><img src='./graphs/MonArbre_test23_avant.png'/></td><td>arbre.tousLesNoeuds();</td><td>[]</td></table><h1>test24</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td><br><img src='./graphs/MonArbre_test24_avant.png'/></td><td>arbre.tousLesNoeuds();</td><td>[b, g, k, m, v, w, x]</td></table><h1>test25</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td><br><img src='./graphs/MonArbre_test25_avant.png'/></td><td>arbre.tousLesNoeuds();</td><td>[b, g, h, n, q, t, v, x]</td></table><h1>test26</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test26_avant.png'/></td><td>arbre.trouverNoeud(s);</td><td>null</td></table><h1>test27</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test27_avant.png'/></td><td>arbre.trouverNoeud(g);</td><td>g</td></table><h1>test28</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test28_avant.png'/></td><td>arbre.trouverNoeud(m);</td><td>m</td></table><h1>test29</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test29_avant.png'/></td><td>arbre.trouverNoeud(g);</td><td>g</td></table><h1>test30</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test30_avant.png'/></td><td>arbre.trouverNoeud(h);</td><td>null</td></table><h1>test31</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test31_avant.png'/></td><td>arbre.trouverNoeud(t);</td><td>null</td></table><h1>test32</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test32_avant.png'/></td><td>arbre.trouverNoeud(v);</td><td>v</td></table><h1>test33</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test33_avant.png'/></td><td>arbre.trouverNoeud(j);</td><td>null</td></table><h1>test34</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test34_avant.png'/></td><td>arbre.trouverNoeud(k);</td><td>k</td></table><h1>test35</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test35_avant.png'/></td><td>arbre.trouverNoeud(k);</td><td>k</td></table><h1>test36</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test36_avant.png'/></td><td>arbre.trouverNoeud(b);</td><td>b</td></table><h1>test37</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test37_avant.png'/></td><td>arbre.trouverNoeud(j);</td><td>null</td></table><h1>test38</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test38_avant.png'/></td><td>arbre.trouverNoeud(x);</td><td>x</td></table><h1>test39</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test39_avant.png'/></td><td>arbre.trouverNoeud(q);</td><td>q</td></table><h1>test40</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test40_avant.png'/></td><td>arbre.trouverNoeud(n);</td><td>n</td></table><h1>test41</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test41_avant.png'/></td><td>arbre.trouverNoeud(d);</td><td>null</td></table><h1>test42</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test42_avant.png'/></td><td>arbre.trouverNoeud(r);</td><td>null</td></table><h1>test43</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test43_avant.png'/></td><td>arbre.trouverNoeud(s);</td><td>null</td></table><h1>test44</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test44_avant.png'/></td><td>arbre.trouverNoeud(g);</td><td>g</td></table><h1>test45</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test45_avant.png'/></td><td>arbre.trouverNoeud(v);</td><td>v</td></table><h1>test46</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test46_avant.png'/></td><td>arbre.trouverNoeud(b);</td><td>b</td></table><h1>test47</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td><br><img src='./graphs/MonArbre_test47_avant.png'/></td><td>arbre.nombreDeNoeuds();</td><td>0</td></table><h1>test48</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td><br><img src='./graphs/MonArbre_test48_avant.png'/></td><td>arbre.nombreDeNoeuds();</td><td>7</td></table><h1>test49</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur de retour</th></tr><tr><td><br><img src='./graphs/MonArbre_test49_avant.png'/></td><td>arbre.nombreDeNoeuds();</td><td>8</td></table><h1>test50</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test50_avant.png'/></td><td>arbre.retirer(p);</td><td><br><img src='./graphs/MonArbre_test50_ok.png'/></td></tr></table><h1>test51</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test51_avant.png'/></td><td>arbre.retirer(v);</td><td><br><img src='./graphs/MonArbre_test51_ok.png'/></td></tr></table><h1>test52</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test52_avant.png'/></td><td>arbre.retirer(w);</td><td><br><img src='./graphs/MonArbre_test52_ok.png'/></td></tr></table><h1>test53</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test53_avant.png'/></td><td>arbre.retirer(g);</td><td><br><img src='./graphs/MonArbre_test53_ok.png'/></td></tr></table><h1>test54</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test54_avant.png'/></td><td>arbre.retirer(m);</td><td><br><img src='./graphs/MonArbre_test54_ok.png'/></td></tr></table><h1>test55</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test55_avant.png'/></td><td>arbre.retirer(l);</td><td><br><img src='./graphs/MonArbre_test55_ok.png'/></td></tr></table><h1>test56</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test56_avant.png'/></td><td>arbre.retirer(k);</td><td><br><img src='./graphs/MonArbre_test56_ok.png'/></td></tr></table><h1>test57</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test57_avant.png'/></td><td>arbre.retirer(x);</td><td><br><img src='./graphs/MonArbre_test57_ok.png'/></td></tr></table><h1>test58</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test58_avant.png'/></td><td>arbre.retirer(b);</td><td><br><img src='./graphs/MonArbre_test58_ok.png'/></td></tr></table><h1>test59</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test59_avant.png'/></td><td>arbre.retirer(x);</td><td><br><img src='./graphs/MonArbre_test59_ok.png'/></td></tr></table><h1>test60</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test60_avant.png'/></td><td>arbre.retirer(o);</td><td><br><img src='./graphs/MonArbre_test60_ok.png'/></td></tr></table><h1>test61</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test61_avant.png'/></td><td>arbre.retirer(r);</td><td><br><img src='./graphs/MonArbre_test61_ok.png'/></td></tr></table><h1>test62</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test62_avant.png'/></td><td>arbre.retirer(j);</td><td><br><img src='./graphs/MonArbre_test62_ok.png'/></td></tr></table><h1>test63</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test63_avant.png'/></td><td>arbre.retirer(g);</td><td><br><img src='./graphs/MonArbre_test63_ok.png'/></td></tr></table><h1>test64</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test64_avant.png'/></td><td>arbre.retirer(g);</td><td><br><img src='./graphs/MonArbre_test64_ok.png'/></td></tr></table><h1>test65</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test65_avant.png'/></td><td>arbre.retirer(a);</td><td><br><img src='./graphs/MonArbre_test65_ok.png'/></td></tr></table><h1>test66</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test66_avant.png'/></td><td>arbre.retirer(x);</td><td><br><img src='./graphs/MonArbre_test66_ok.png'/></td></tr></table><h1>test67</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test67_avant.png'/></td><td>arbre.retirer(t);</td><td><br><img src='./graphs/MonArbre_test67_ok.png'/></td></tr></table><h1>test68</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test68_avant.png'/></td><td>arbre.retirer(i);</td><td><br><img src='./graphs/MonArbre_test68_ok.png'/></td></tr></table><h1>test69</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test69_avant.png'/></td><td>arbre.retirer(h);</td><td><br><img src='./graphs/MonArbre_test69_ok.png'/></td></tr></table><h1>test70</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test70_avant.png'/></td><td>arbre.retirer(x);</td><td><br><img src='./graphs/MonArbre_test70_ok.png'/></td></tr></table><h1>test71</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test71_avant.png'/></td><td>arbre.retirer(d);</td><td><br><img src='./graphs/MonArbre_test71_ok.png'/></td></tr></table><h1>test72</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test72_avant.png'/></td><td>arbre.retirer(n);</td><td><br><img src='./graphs/MonArbre_test72_ok.png'/></td></tr></table><h1>test73</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test73_avant.png'/></td><td>arbre.retirer(m);</td><td><br><img src='./graphs/MonArbre_test73_ok.png'/></td></tr></table><h1>test74</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test74_avant.png'/></td><td>arbre.retirer(v);</td><td><br><img src='./graphs/MonArbre_test74_ok.png'/></td></tr></table><h1>test75</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test75_avant.png'/></td><td>arbre.retirer(b);</td><td><br><img src='./graphs/MonArbre_test75_ok.png'/></td></tr></table><h1>test76</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test76_avant.png'/></td><td>arbre.retirer(c);</td><td><br><img src='./graphs/MonArbre_test76_ok.png'/></td></tr></table><h1>test77</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test77_avant.png'/></td><td>arbre.retirer(q);</td><td><br><img src='./graphs/MonArbre_test77_ok.png'/></td></tr></table><h1>test78</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test78_avant.png'/></td><td>arbre.retirer(n);</td><td><br><img src='./graphs/MonArbre_test78_ok.png'/></td></tr></table><h1>test79</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test79_avant.png'/></td><td>arbre.retirer(l);</td><td><br><img src='./graphs/MonArbre_test79_ok.png'/></td></tr></table><h1>test80</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test80_avant.png'/></td><td>arbre.retirer(v);</td><td><br><img src='./graphs/MonArbre_test80_ok.png'/></td></tr></table><h1>test81</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test81_avant.png'/></td><td>arbre.retirer(g);</td><td><br><img src='./graphs/MonArbre_test81_ok.png'/></td></tr></table><h1>test82</h1><style>table{border:1px solid black;}
tr{border:1px solid black;}
td{border:1px solid black;}
th{border:1px solid black;}</style><table><tr><th>Valeur avant opération</th><th>Opération</th><th>Valeur après opération</th></tr><tr><td><br><img src='./graphs/MonArbre_test82_avant.png'/></td><td>arbre.retirer(g);</td><td><br><img src='./graphs/MonArbre_test82_ok.png'/></td></tr></table></div>
