public class MonNoeud<C extends Comparable<C>> extends Noeud<C> {


    public MonNoeud(C valeur) {
        super(valeur);
    }

    public MonNoeud(C valeur, Noeud<C> parent) {
        super(valeur, parent);
    }


    @Override
    public void inserer(C valeur) {
        /*
         * TODO
         *
         * insérer à gauche, à droite ou ici-même
         * selon la comparaison de la valeur courante (si elle existe)
         * et de la valeur à insérer
         *
         * NOTES: 
         * - si valeur < courant: insérer à gauche
         * - si valeur > courant: insérer à droite
         * - sinon sauvegarder la valeur dans le noeud courant
         */
    }

    private void insererVersLaGauche(C valeur) {
        /*
         * TODO
         *
         * IMPORTANT: utiliser nouveauNoeud(valeur, this) pour créer un nouveau noeud
         *
         * IMPORTANT: appeler this.equilibrer() **une seule fois**, 
         *            tout de suite après avoir insérer un nouveau noeud
         *
         */
    }

    private void insererVersLaDroite(C valeur) {
        /*
         * TODO
         *
         * IMPORTANT: utiliser nouveauNoeud(valeur, this) pour créer un nouveau noeud
         *
         * IMPORTANT: appeler this.equilibrer() **une seule fois**, 
         *            tout de suite après avoir insérer un nouveau noeud
         *
         */
    }

    @Override
    protected Noeud<C> nouveauNoeud(C valeur, Noeud<C> parent) {
        return new MonNoeud<C>(valeur, parent);
    }

    @Override
    protected Noeud<C> nouveauNoeud(C valeur) {
        return new MonNoeud<C>(valeur);
    }
}
