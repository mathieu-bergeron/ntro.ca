public class MonArbre<C extends Comparable<C>> extends Arbre<C> {
    /*
     * Dans la classe parent
     *
     * protected Noeud<C> racine;
     *
     * public Noeud<C> getRacine() {
     *    return racine;
     * }
     *
     * public void setRacine(Noeud<C> racine) {
     *    this.racine = racine;
     * }
     *
     */

    @Override
    public Noeud<C> racine() {
        return racine;
    }

    @Override
    public void ajouter(C valeur) {
        // TODO
    }

    @Override
    public void retirer(C valeur) {
        // TODO
    }

    @Override
    public Noeud<C> trouverNoeud(C valeur) {
        // TODO
    }
    
    private Noeud<C> trouverNoeud(Noeud<C> curseur, C valeur){
        // TODO
    }

    @Override
    public List<Noeud<C>> tousLesNoeuds() {
        // TODO
    }
    
    @Override
    public int nombreDeNoeuds() {
        // TODO
    }
}
