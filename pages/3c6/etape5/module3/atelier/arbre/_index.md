---
title: "Atelier 5.3, exercice A: implanter un arbre"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Exemple de tests réussis

* <a href="./MonArbre">MonArbre</a>

## Créer la classe `MonNoeud`

* Ajouter la classe suivante au **paquet** `atelier5_3_A`
    * Nom de la classe: `MonNoeud`

* Débuter l'implantation comme suit:

```java
{{% embed "MonNoeud.java" %}}
```

## Créer la classe `MonArbre`

* Ajouter la classe suivante au **paquet** `atelier5_3_A`
    * Nom de la classe: `MonArbre`

* Débuter l'implantation comme suit:

```java
{{% embed "MonArbre.java" %}}
```

## Créer la classe `MonAtelier5_3_A`

* Ajouter la classe suivante au **paquet** `atelier5_3_A`
    * Nom de la classe: `MonAtelier5_3_A`

* Implanter comme suit:

```java
{{% embed "MonAtelier5_3_A.java" %}}
```
## Compléter les implantations

* Utiliser l'outil de validation pour tester votre code

<img src="validation.png"/>

* vous pouvez aussi afficher le fichier `html`:

    * `Arbre.html`
    * (à la racine du projet)
