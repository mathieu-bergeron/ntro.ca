---
title: "Examen 2"
weight: 40
draft: false
---

# 3C6: Examen 2

* Durée: 2h

* Individuel

* Documentation permise: ntro.ca, Moodle, recherches sur le Web, **votre code**, des cartes à jouer

* L'examen est 100% à choix de réponse sur Moodle

## Sujets

* données en JSON

* données en Java

* récursivité
    * pile d'appels
    * récursivité dans les données
    * récursif Vs dynamique
    * Modélisation de `Factoriel` (entrevue 1.3)
    * Modélisation de `Fibonacci` (atelier 1.3)

## 5pts) Volet théorique

* 5 questions de compréhension à choix de réponse

## 5pts) Volet pratique

* 5 questions du type «&nbsp;compléter le code suivant&nbsp;» 
