---
title: 'Module 2.1: données JSON'
weight: 1
draft: false
---

{{% pageTitle %}}

1. Théorie:

   - {{% link "/3c6/etape2/module1/theorie/format_json" "le format JSON" %}}
   - {{% link "/3c6/etape2/module1/theorie/modele" "notion de modèle" %}}

1. {{% link "/3c6/etape2/module1/entrevue" "Entrevue 2.1" %}}

1. Mini-test théorique

   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=328302" target="_blank">Mathieu Bergeron</a>
   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=410353" target="_blank">Marc-Olivier Tremblay</a>

1. Atelier 2.1

   1. {{% link "/3c6/etape2/module1/atelier/correctif" "Correctif" %}}: ajouter les fichiers `.db` manquants
   1. {{% link "/3c6/etape2/module1/atelier/encoder_cartes" "Exercice 2.1.A" %}}: encoder des cartes en Json
   1. {{% link "/3c6/etape2/module1/atelier/encoder_donnees" "Exercice 2.1.B" %}}: encoder des données et valider mon travail

1. Remises
   - {{% link "/3c6/etape2/module1/atelier/remise_gitlab" "sur GitLab" %}}
   - {{% link "/3c6/etape2/module1/atelier/remise_moodle" "sur Moodle" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}
