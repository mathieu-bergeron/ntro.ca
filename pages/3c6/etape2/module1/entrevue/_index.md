---
title: "Entrevue 2.1: encoder des données en JSON"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier ou dans un éditeur de texte


<br>

1. Choisir une carte au hasard: <span class="span-cartes" nombre-de-cartes="1"></span> <button id="bouton-cartes">Générer</button>

1. En vous inspirant de la {{% link "/3c6/etape2/module1/theorie/modele" "théorie" %}}:

    * encoder cette carte en JSON, **avec les bonnes indentations**

1. Choisir deux autre cartes au hasard: <span class="span-cartes" nombre-de-cartes="2"></span>

    * l'idée est de former un tableau

1. Encoder cette liste de cartes en JSON, **avec les bonnes indentations**

1. **En respectant les indentations**, encoder un modèle `MonTriLibre` contenant

    * l'attribut `cartes` qui mémorise le tableau de cartes

    * l'attribut `siTrie` qui indique si les cartes sont triées


<script src="/3c6/cartes.js"></script>

<style>
#span-cartes{
    padding:0.2rem;
}
</style>
