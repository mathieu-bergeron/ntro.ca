---
title: "Question 1"
weight: 2
bookHidden: false
draft: false
---

Constuire le JSON qui correspond au modèle ci-bas. Utiliser `∅` si le choix n'est pas pertinent.

<img src="https://ntro.ca/3c6/etape2/module1/mini_test_theorique/question01/ExempleJson.png" />

```
{1:MCS:=&#123;~&#125;~]~[~∅}
      {1:MCS:="_C": "ExempleJson",~"_C": "Carte"~"_C": "Model"~"_C": "Value"~∅}
          "racine": {1:MCS:=&#123;~&#125;~]~[~∅}
                {1:MCS:"_C": "Value",~"_C": "Carte"~"_C": "Model"~"_C": "Racine"~=∅}
                "sp02": {1:MCS:=&#123;~&#125;~]~[~∅}
                      "equipement": {1:MCS:&#123;~&#125;~]~=[~∅}
                            "souliers",
                            "balon"
                      {1:MCS:&#123;~&#125;~=]~[~∅},
                      "type": "sportEquipe",
                      "nom": "soccer"
                {1:MCS:&#123;~=&#125;~]~[~∅},
                "sp01": {1:MCS:=&#123;~&#125;~]~[~∅}
                      "equipement": {1:MCS:&#123;~&#125;~]~=[~∅}
                            "casque",
                            "plastron",
                            "fleuret",
                            "gant"
                      {1:MCS:&#123;~&#125;~=]~[~∅},
                      "type": "sportIndividuel",
                      "nom": "escrime"
        {1:MCS:&#123;~=&#125;~]~[~∅}
    {1:MCS:&#123;~=&#125;~]~[~∅}
{1:MCS:&#123;~=&#125;~]~[~∅}
```
---
bookCollapseSection: true
---

