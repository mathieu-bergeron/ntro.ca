---
title: "Atelier2.1, exercice A: encoder des cartes"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer la classe `MonTableau`

1. En VSCode, sélectionner le **paquet** `atelier2_1_A`

1. Ajouter la classe suivante au **paquet** `atelier2_1_A`
    * Nom de la classe: `MonTableau`

1. Ouvrir la classe `MonTableau` et ajuster la signature

    ```java
    public class MonTableau extends Tableau {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `Tableau`

## Créer la classe `Procedure`

1. Ajouter la classe suivante au **paquet** `atelier2_1_A`
    * Nom de la classe: `Procedure`

1. Ouvrir la classe `Procedure` et ajuster la signature

    ```java
    public class Procedure extends ProcedureDecaler<MonTableau> {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `ProcedureDecaler`

    * ajouter la méthode obligatoire `classeMonTableau`

1. Ajouter la méthode `main`

    ```java
    public static void main(String[] args) {
        NtroAppFx.launch(args);
    }
    ```

    * utiliser {{% key "Ctrl+1" %}} pour corriger les erreurs de compilation

1. Ajouter le code suivant à la méthode `classeMonTableau`

    ```java
    protected Class<MonTableau> classeMonTableau() {
        return MonTableau.class;
    }
    ```

## Exécuter l'outil de validation

1. En VSCode, sélectionner le **paquet** `atelier2_1_A`

1. Ouvrir la classe `Procedure` du paquet `atelier2_1_A`

    * Cliquer sur *Run*

## Modifier le fichier `_storage/models/MonTableau/ex01.json`

1. Avec l'outil de validtion ouvert:

    * Ouvrir le fichier `atelier2_1_A/_storage/models/MonTableau/ex01.json` dans un éditeur de texte

1. Modifier le fichier et visualiser les modifications avec l'outil

    {{% animation src="/3c6/etape2/module1/atelier/encoder_cartes/modifier_json.mp4" width="100%" %}}

{{<excerpt class="note">}}

NOTES:

* Utiliser un éditeur de texte simple comme `Notepad++`

* L'outil peut lancer une exception, p.ex. en cas d'erreur dans le JSON

    * l'outil ne devrait pas planter... sinon le relancer

{{</excerpt>}}

## Encoder `MonTableau/ex01.json`

1. Encoder les cartes suivantes

    <img src="MonTableau01.png" />

## Encoder `MonTableau/ex02.json`

1. Encoder les cartes suivantes

    <img src="MonTableau02.png" />

## Encoder `MonTableau/ex03.json`

1. Encoder les cartes suivantes

    <img src="MonTableau03.png" />

## Sauvegarder les fichiers `.json` dans Git

1. Le répertoire `atelier2_1_A/_storage/models` n'est pas ignoré par Git

1. Avec un GitBash à la racine de votre dépôt Git

    ```bash
    $ git add .
    $ git commit -a -m".json de atelier2_1_A"
    $ git push
    ```

1. S'assurer que les fichiers `.json` que vous avez modifié sont sur GitLab






