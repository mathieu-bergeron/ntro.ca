---
title: 'Atelier2.1, exercice B: encoder des données'
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer la classe `MonTableau`

1. En VSCode, sélectionner le **paquet** `atelier2_1_B`

1. Ajouter la classe suivante au **paquet** `atelier2_1_B`

   - Nom de la classe: `MonTableau`

1. Ouvrir la classe `MonTableau` et ajuster la signature

   ```java
   public class MonTableau extends Tableau {
   ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
   - importer la classe `Tableau`

## Créer la classe `Valider`

1. Ajouter la classe suivante au **paquet** `atelier2_1_B`

   - Nom de la classe: `Valider`

1. Ouvrir la classe `Valider` et ajuster la signature

   ```java
   public class Valider extends ValiderShift {
   ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes

   - importer la classe `ValiderShift`

   - ajouter la méthode obligatoire `validateModels`

1. Ajouter le code suivant à la méthode `main`

   ```java
   public static void main(String[] args) {
       NtroAppFx.launch(args);
   }
   ```

   - utiliser {{% key "Ctrl+1" %}} pour corriger les erreurs de compilation

1. Ajouter le code suivant à la méthode `validateModels`

   ```java
   @Override
   protected void validateModels(Validator<ShiftModel> validator) {
       validator.validateModel(MonTableau.class, "ex01");
       validator.validateModel(MonTableau.class, "ex02");
       validator.validateModel(MonTableau.class, "ex03");
   }
   ```

## Copier vos fichiers `MonTableau/exXX.json` dans `_storage/models/MonTableau/`

1. Naviguer vers les répertoires `atelier2_1_B/_storage/models/MonTableau/`

1. Y copier vos fichier `MonTableau/ex01.json`, `MonTableau/ex02.json` et `MonTableau/ex03.json`

   - (les fichiers créés dans l'{{% link "/3c6/etape2/module1/atelier/encoder_cartes" "exercice A" %}})

## Exécuter l'outil de validation

1. En VSCode, sélectionner le **paquet** `atelier2_1_B`


1. Cliquer *run* pour exécuter le main

1. Vérifier que vos modèles sont valides:

   ```bash
   [OK]    votre modèle MonTableau-ex01 est valide
   [OK]    votre modèle MonTableau-ex02 est valide
   [OK]    votre modèle MonTableau-ex03 est valide
   ```

## Créer la classe `MesDonneesJson`

1. En VSCode, sélectionner le **paquet** `atelier2_1_B`

1. Ajouter la classe suivante au **paquet** `atelier2_1_B`

   - Nom de la classe: `MesDonneesJson`

1. Ouvrir la classe `MesDonneesJson` et ajuster la signature

   ```java
   public class MesDonneesJson extends DonneesJson {
   ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
   - importer la classe `DonneesJson`

## Encoder `MesDonneesJson/ex01.json`

1. Créer un nouveau fichier `_storage/models/MesDonneesJson/ex01.json`

1. Encoder le modèle suivant:

<img class="figure" src="./MesDonneesJson01.png" />

{{<excerpt>}}
**IMPORTANT**

- Un objet `Map` correspond à un objet JSON `{}`

- Un objet `List` correspond à une liste JSON `[]`

{{</excerpt>}}

1. Le fichier `.json` commence comme suit:

   ```json
   {
     "_C": "MesDonneesJson",
     "racine": {

     // ...
   ```

## Encoder `MesDonneesJson/ex02.json`

1. Créer un nouveau fichier `_storage/models/MesDonneesJson/ex02.json`

1. Encoder le modèle suivant:

<img class="figure" src="MesDonneesJson02.png" />

## Encoder `MesDonneesJson/03.json`

1. Créer un nouveau fichier `_storage/models/MesDonneesJson/ex03.json`

1. Encoder le modèle suivant:

<img class="figure max-width-75" src="MesDonneesJson03.png"/>

## Exécuter l'outil de validation

1. En VSCode, sélectionner le **paquet** `atelier2_1_B`

1. Ouvrir la classe `Valider`

1. Ajouter le code suivant à la méthode `validateModels`

   ```java
   @Override
   protected void validateModels(Validator<ShiftModel> validator) {
       // ...
       // ajouter
       validator.validateModel(MesDonneesJson.class, "ex01");
       validator.validateModel(MesDonneesJson.class, "ex02");
       validator.validateModel(MesDonneesJson.class, "ex03");
   }
   ```


1. Sélectionner la classe `Valider` et cliquer sur *run* pour exécuter le main

1. Vérifier que vos modèles sont valides:

   ```bash
   [OK]    votre modèle MesDonneesJson-ex01 est valide
   [OK]    votre modèle MesDonneesJson-ex02 est valide
   [OK]    votre modèle MesDonneesJson-ex03 est valide
   [OK]    votre modèle MonTableau-ex01 est valide
   [OK]    votre modèle MonTableau-ex02 est valide
   [OK]    votre modèle MonTableau-ex03 est valide
   ```

## S'assurer de sauvegarder les `.json` dans Git

1. Le répertoire `atelier2_1_B/_storage/models` n'est pas ignoré par Git

1. Avec un GitBash à la racine de votre dépôt Git

   ```bash
   $ git add .
   $ git commit -a -m".json de atelier2_1_B"
   $ git push
   ```

1. S'assurer que les fichiers `.json` que vous avez modifié sont sur GitLab
