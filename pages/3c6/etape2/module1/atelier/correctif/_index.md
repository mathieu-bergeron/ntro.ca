---
title: "Ajout de fichiers `.db` manquants"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
* à faire une seule fois
{{</excerpt>}}

1. Télécharger {{% download "db_atelier2_1_A.zip" "db_atelier2_1_A.zip" %}} 

1. Placer le fichier `db_atelier2_1_A.zip` à la racine de mon dépôt Git

1. Ouvrir GitBash à la racine de mon dépôt Git
    * au besoin, glisser le chemin dans GitBash à partir du navigateur Windows

1. Extraire l'archive sur place 

    ```bash
    $ unzip db_atelier2_1_A.zip
    ```

1. Ajouter les nouveaux fichiers et pousser sur GitLab

    ```bash
    $ git add .
    $ git commit -a -m"db atelier2_1_A"
    $ git push
    ```
