---
title: "Entrevue 2.2: encoder des données en Java"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier ou dans un éditeur de texte


<br>


1. Écrivez le code Java pour représenter les données suivantes:

<img src="MesDonneesJava04.png"/>

* NOTE: on imagine que les classes `MesDonneesJava` et `GardeRobe` existent déjà

