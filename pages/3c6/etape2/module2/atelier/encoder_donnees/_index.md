---
title: "Atelier2.2, exercice B: encoder des données"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="max-width-75">}}

**ATTENTION**, dans cet atelier:

* utiliser `NtroHashMap` plutôt que `HashMap` dans votre code Java
* pour démarrer la validation

    1. dans VSCode, ouvrir un terminal Bash

    2. démarrer l'outil de validation avec la commande suivante

    ```bash
    $ sh gradlew atelier2_2_B
    ```

{{</excerpt>}}

## Créer la classe `MesDonneesJava`

1. En VSCode, sélectionner le **paquet** `atelier2_2_B`

1. Ajouter la classe suivante au **paquet** `atelier2_2_B`
    * Nom de la classe: `MesDonneesJava`

1. Ouvrir la classe `MesDonneesJava` et ajuster la signature

    ```java
    public class MesDonneesJava extends DonneesJavaSolutionSuperClass {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes

    * importer la classe `DonneesJavaSolutionSuperClass`

    * ajouter la méthode obligatoire `initialize`

1. Vérifier que j'ai la méthode `initialize`

    ```java
    @Override
    public void initialize(String id) {

        if(id.equals("ex01")){

            // construire MesDonneesJava/ex01

        }else if(id.equals("ex02")){

            // construire MesDonneesJava/ex02

        }else if(id.equals("ex03")){

            // construire MesDonneesJava/ex03

        }else if(id.equals("ex04")){

            // construire MesDonneesJava/ex04

        }else if(id.equals("ex05")){

            // construire MesDonneesJava/ex05

        }else if(id.equals("ex06")){

            // construire MesDonneesJava/ex06

        }
    }

    ```

## Créer la classe `Valider`

1. Ajouter la classe suivante au **paquet** `atelier2_2_B`
    * Nom de la classe: `Valider`

1. Ouvrir la classe `Valider` et ajuster la signature

    ```java
    public class Valider extends ValiderShift3 {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `ValiderShift3`

    * ajouter la méthode obligatoire `validateModels`

1. Ajouter le code suivant à la méthode `main`

    ```java
    public static void main(String[] args) {
        NtroAppFx.launch(args);
    }
    ```

    * utiliser {{% key "Ctrl+1" %}} pour corriger les erreurs de compilation

1. Ajouter le code suivant à la méthode `validateModels`

    ```java
    @Override
    protected void validateModels(Validator<Shift3Model> validator) {
        // ...
        validator.validateModel(MesDonneesJava.class, "ex01");
        validator.validateModel(MesDonneesJava.class, "ex02");
        validator.validateModel(MesDonneesJava.class, "ex03");
        validator.validateModel(MesDonneesJava.class, "ex04");
        validator.validateModel(MesDonneesJava.class, "ex05");
        validator.validateModel(MesDonneesJava.class, "ex06");
    }
    ```


## Encoder `MesDonneesJava`/`ex01`


1. Encoder le modèle suivant:

    <img class="figure" src="./MesDonneesJava01.png" />

## Encoder `MesDonneesJava`/`ex02`

1. Encoder le modèle suivant:

    <img class="figure" src="./MesDonneesJava02.png" />

    
## Créer `MesDonneesJava`/`ex03` **avec une boucle**

1. En utilisant une boucle, créer le modèle suivant:

    <img class="figure" src="./MesDonneesJava03.png" />

1. TRUC: 
    * *clic-droit* => *ouvrir l'image dans un nouvel onglet* 
    * puis <kbd>Ctrl</kbd>+<kbd>+</kbd> pour zoomer

## Créer `MesDonneesJava`/`ex04` **avec une boucle**

1. En utilisant une boucle, créer le modèle suivant:

    <img class="figure" src="./MesDonneesJava04.png" />


## Créer `MesDonneesJava`/`ex05` **avec une boucle**

1. En utilisant une boucle, créer le modèle suivant:

    <img class="figure" src="./MesDonneesJava05.png" />

## Créer `MesDonneesJava`/`ex06` **avec une boucle**

1. Utiliser la même boucle que `MesDonneesJava`/`ex05` pour créer le modèle suivant:

    <img class="figure" src="./MesDonneesJava06.png" />



## Exécuter l'outil de validation

1. En VSCode, ouvrir un terminal Bash

1. Démarrer la validation avec la commande suivante

    ```bash
    $ sh gradlew atelier2_2_B
    ```


1. Vérifier que vos modèles sont valides:

    ```bash
    [OK]     votre modèle MesDonneesJava-ex01 est valide
    [OK]     votre modèle MesDonneesJava-ex02 est valide
    [OK]     votre modèle MesDonneesJava-ex03 est valide
    [OK]     votre modèle MesDonneesJava-ex04 est valide
    [OK]     votre modèle MesDonneesJava-ex05 est valide
    [OK]     votre modèle MesDonneesJava-ex06 est valide
    ```

    * NOTE: n'affiche pas nécessairement dans l'ordre

