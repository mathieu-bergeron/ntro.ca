import ca.ntro.cards.common.models.enums.Sorte;

public class Carte {

    private int numero;
    private Sorte sorte; // XXX: la valeur par défaut est null

    public Carte() {

        this.numero = 2;
        this.sorte = Sorte.COEUR;

    }

    public Carte(int numero, Sorte sorte){

        this.numero = numero;
        this.sorte = sorte;

    }

}
