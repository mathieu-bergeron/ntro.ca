---
title: "Théorie 2.2: énumération en Java"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Énumération ou `enum`

{{% video src="/3c6/etape2/module2/theorie/enum/01.mp4" %}}

* Un `enum` est simplement une liste de constantes

* P.ex. pour les sortes de cartes

```java
{{% embed "./Sorte01.java" %}}
```

* La valeur de la constante d'un `enum` est «cachée»

    * le compilateur décide comment les représenter

    * p.ex. le compilateur pourrait décider

        * `int COEUR = 0, CARREAU = 1, TREFLE = 2, PIQUE = 3;`

        * (prends moins de place en mémoire que des chaînes!)

* En JSON, on va représenter les constantes d'un `enum` comme des chaînes

## Utiliser les constantes d'un `enum`

{{% video src="/3c6/etape2/module2/theorie/enum/02.mp4" %}}

* On utilise l'`enum` comme une classe (un type de données)

```java
{{% embed "./Carte01.java" %}}
```

* On doit importer `l'enum`, comme une classe

* On peut ensuite utiliser les constantes directement

* Attention, la valeur par défaut d'une variable `Sorte` est `null`

## Méthodes fournies avec un `enum`

{{% video src="/3c6/etape2/module2/theorie/enum/03.mp4" %}}

```java
{{% embed "./Carte02.java" %}}
```
