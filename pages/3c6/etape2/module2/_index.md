---
title: 'Module 2.2: données en Java'
weight: 30
draft: false
---

{{% pageTitle %}}

1. Théorie:

   - {{% link "/3c6/etape2/module2/theorie/enum" "énumération en Java" %}}
   - {{% link "/3c6/etape2/module2/theorie/structures" "structures en Java" %}}
   - {{% link "/3c6/etape2/module2/theorie/graphe" "notion de graphe d'objets" %}}

1. {{% link "/3c6/etape2/module2/entrevue" "Entrevue 2.2" %}}

1. Mini-test théorique

   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=328947" target="_blank">Mathieu Bergeron</a>
   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=410356" target="_blank">Marc-Olivier Tremblay</a>

1. Atelier 2.2

   1. {{% link "/3c6/etape2/module2/atelier/encoder_cartes" "Exercice 2.2.A" %}}: encoder des cartes en Java
   1. {{% link "/3c6/etape2/module2/atelier/encoder_donnees" "Exercice 2.2.B" %}}: encoder des données en Java et valider mon travail

1. Remises
   - {{% link "/3c6/etape2/module2/atelier/remise_gitlab" "sur GitLab" %}}
   - {{% link "/3c6/etape2/module2/atelier/remise_moodle" "sur Moodle" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}
