---
title: "Mise-à-jour des fichiers .jar"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
* à faire une seule fois
{{</excerpt>}}

1. Télécharger {{% download "maj_jars.zip" "maj_jars.zip" %}} 

1. Placer le fichier `maj_jars.zip` à la racine de mon dépôt Git

1. Ouvrir GitBash à la racine de mon dépôt Git
    * au besoin, glisser le chemin dans GitBash à partir du navigateur Windows

1. Extraire avec l'option `-o` (pour écraser les fichiers existants)

    ```bash
    $ unzip -o maj_jars.zip
    ```

1. Ajouter les nouveaux fichiers et pousser sur GitLab

    ```bash
    $ git add .
    $ git commit -a -m"correctif 2_3"
    $ git push
    ```
