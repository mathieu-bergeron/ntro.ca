---
title: "Atelier 2.3: implanter Fibonacci"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Objectif


* {{% link "/3c6/etape2/module3/atelier/coder_atelier" "Implanter et valider Fibonacci" %}}


## Remises

* {{% link "/3c6/etape2/module3/atelier/remise_gitlab" "Remise via GitLab" %}}

* {{% link "/3c6/etape2/module3/atelier/remise_moodle" "Remise vis Moodle" %}}

