---
title: "Atelier2.2, exercice A: encoder des cartes"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer la classe `MonTableau`

1. En VSCode, sélectionner le **paquet** `atelier2_2_A`

1. Ajouter la classe suivante au **paquet** `atelier2_2_A`
    * Nom de la classe: `MonTableau`

1. Ouvrir la classe `MonTableau` et ajuster la signature

    ```java
    public class MonTableau extends Tableau {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `Tableau`


1. Ajouter la méthode `construireModele`

    ```java
    @Override
    public void construireModele(String nomExemple) {
        // nomExemple prendra trois valeurs:
        // "ex01"
        // "ex02"
        // "ex03"
    }
    ```

## Créer la classe `MaCarte`

1. En VSCode, sélectionner le **paquet** `atelier2_2_A`

1. Ajouter la classe suivante au **paquet** `atelier2_2_A`
    * Nom de la classe: `MaCarte`

1. Ouvrir la classe `MaCarte` et ajuster la signature

    ```java
    public class MaCarte extends CarteIncomplete {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `CarteIncomplete`

    * ajouter la méthode obligatoire `dessinerCarte`


1. Vérifier que j'ai la méthode `dessinerCarte

    ```java
    @Override
    protected void dessinerCarte(GraphicsContext gc) {
        // XXX: question bonus: implanter cette méthode
    }
    ```

1. Ajouter les deux constructeurs suivants:

    ```java
    public MaCarte() {
        super();
    }

    public MaCarte(int numero, Sorte sorte) {
        super(numero, sorte);
    }
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour corriger les erreurs de compilation

## Créer la classe `Procedure`

1. Ajouter la classe suivante au **paquet** `atelier2_2_A`
    * Nom de la classe: `Procedure`

1. Ouvrir la classe `Procedure` et ajuster la signature

    ```java
    public class Procedure extends ProcedureDecaler<MonTableau, MaCarte> {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `ProcedureDecaler`

    * ajouter la méthode obligatoire `classeMonTableau`

    * ajouter la méthode obligatoire `classeMaCarte`

1. Ajouter le code suivant à la méthode `main`

    ```java
    public static void main(String[] args) {
        NtroAppFx.launch(args);
    }
    ```

    * utiliser {{% key "Ctrl+1" %}} pour corriger les erreurs de compilation

1. Ajouter le code suivant à la méthode `classeMonTableau`

    ```java
    protected Class<MonTableau> classeMonTableau() {
        return MonTableau.class;
    }
    ```

1. Ajouter le code suivant à la méthode `classeMaCarte`

    ```java
    protected Class<MaCarte> classeMaCarte() {
        return MaCarte.class;
    }
    ```

## Construire le modèle `"ex01"`

1. En complétant la méthode `construireModele`, encoder les cartes suivantes en Java

    <img src="MonTableau01.png" />

    * NOTE: `insererAuDebut : true`

1. Vérifier avec l'outil de validation que le modèle est bon

    <img src="ex01_valide.png"/>

## Construire le modèle `"ex02"`

1. En complétant la méthode `construireModele`, encoder les cartes suivantes en Java

    <img src="MonTableau02.png" />

    * NOTE: `insererAuDebut : true`

1. Vérifier avec l'outil de validation que le modèle est bon

    <img src="ex02_valide.png"/>

## Construire le modèle `"ex03"`

1. En complétant la méthode `construireModele`, encoder les cartes suivantes en Java

    <img src="MonTableau03.png" />

    * NOTE: `insererAuDebut : true`

1. Vérifier avec l'outil de validation que le modèle est bon

    <img src="ex03_valide.png"/>


## Question bonus: dessiner les cartes

1. Compléter la méthode `dessinerCarte`

    ```java
    @Override
    protected void dessinerCarte(GraphicsContext gc) {
        int width = 50; // largeur typique d'une carte
        int height = 75 // hauteur typique d'une carte

        // le coin haut-gauche de la carte typique est (0,0)
        // le coin bas-droit de la carte typique est (50,75)

        // pour dessiner un rectangle plein
        // gc.fillRect(x,y,larger,hauteur);

        // pour dessiner le contour d'un rectangle
        // gc.strokeRect(x,y,larger,hauteur);

        // pour ajouter du texte
        // gc.strokeText(texte, x, y);
    }
    ```

    * NOTE: le `GraphicsContext` est celui de JavaFx: https://docs.oracle.com/javase/8/javafx/api/javafx/scene/canvas/GraphicsContext.html

1. Exécuter l'outil de validation
    * cliquer sur un exemple sous *Mon Code*
    * cliquer sur ⏭ pour avancer à la dernière étape
    * vérifier que les cartes s'affichent

    <img src="dessiner_valide.png"/>


