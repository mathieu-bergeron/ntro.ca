---
title: "Atelier 2.3: Fibonnaci"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer la classe `MonFibonacci`

1. Ajouter la classe suivante au **paquet** `atelier2_3`
    * Nom de la classe: `MonFibonacci`

1. Ouvrir la classe `MonFibonacci` et ajuster la signature

    ```java
    public class MonFibonacci extends Fibonacci {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `Fibonacci`

1. Ajouter la méthode `calculerReponseEtNombreOr`

    ```java
    @Override
    public void calculerReponseEtNombreOr() {

    }
    ```

1. Ajouter la méthode `construireGrapheRecursivement`

    ```java
    @Override
    public void construireGrapheRecursivement() {

    }
    ```

## Créer la classe `MonCalculateur`

1. Ajouter la classe suivante au **paquet** `atelier2_3`
    * Nom de la classe: `MonCalculateur`

1. Ouvrir la classe `MonCalculateur` et ajuster la signature

    ```java
    public class MonCalculateur extends Calculateur {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `Calculateur`

1. Ajouter la méthode `construireGraphe`

    ```java
    @Override
    public void construireGraphe() {
        if(siRecursif) {

            // construire le graphe récursivement

        }else {

            // construire le graphe dynamiquement

        }
    }
    ```

## Créer la classe `Procedure`

1. Ajouter la classe suivante au **paquet** `atelier2_3`
    * Nom de la classe: `Procedure`

1. Ouvrir la classe `Procedure` et ajuster la signature

    ```java
    public class Procedure extends FibonacciProcedureApp<MonCalculateur, MonFibonacci> {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `FibonacciProcedureApp`

    * ajouter la méthode obligatoire `classeCalculateur`

    * ajouter la méthode obligatoire `classeFibonacci`

1. Ajouter le code suivant à la méthode `main`

    ```java
    public static void main(String[] args) {
        NtroAppFx.launch(args);
    }
    ```

    * utiliser {{% key "Ctrl+1" %}} pour corriger les erreurs de compilation

1. Ajouter le code suivant à la méthode `classeCalculateur`

    ```java
    @Override
    protected Class<MonCalculateur> classeCalculateur() {
        return MonCalculateur.class;
    }
    ```

1. Ajouter le code suivant à la méthode `classeFibonacci`

    ```java
    @Override
    protected Class<MonFibonacci> classeFibonacci() {
        return MonFibonacci.class;
    }
    ```

## Exécuter l'outil de validation une première fois

* En VSCode, sélectionner le **paquet** `atelier2_3`

* Cliquer sur la classe `Procedure` et cliquer sur *run* pour exécuter le main


## Compléter la version récursive

{{<excerpt class="note">}}
Au besion, voir {{% link "/3c6/etape2/module3/theorie/fibonacci/#construire-le-graphe-dobjets-récursivement" "le pseudo code présenté en théorie" %}}
{{</excerpt>}}

* Compléter la méthode `calculerReponseEtNombreOr`

* Compléter la méthode `construireGrapheRecursivement`

* Compléter la méthode `construireGraphe`, version récursive

* Exécuter l'outil de validation pour tester mon code

## Compléter la méthode `construireGraphe`, version dynamique

{{<excerpt class="note">}}
Au besion, voir la {{% link "/3c6/etape2/module3/theorie/fibonacci/#construire-le-graphe-dobjets-dynamiquement" "le pseudo code présenté en théorie"  %}}
{{</excerpt>}}

* Voici des graphes pour m'aider à implanter la création de la nouvelle tête

    1. Utiliser une variable `nouvelleTete`

        <img src="dyn01.png"/>

    1. Créer un nouvel objet `MonFibonacci`

        <img src="dyn02.png"/>

    1. Le `moinsUn` de la `nouvelleTete` doit pointer vers la `tete` actuelle 
        * (et le `moinsDeux` de la `nouvelleTete` doit pointer vers le `moinUn` de la `tete` actuelle)

            <img src="dyn03.png"/>

    1. La `tete` devient maintenant la `nouvelleTete`

        <img src="dyn04.png"/>

    1. Calculer la réponse avant de continuer la boucle

        <img src="dyn05.png"/>

* Exécuter l'outil de validation pour tester mon code


## Question bonus: modélisation plus simple

* Utiliser une modélisation plus simple pour calculer la suite de Fibonacci

* Pour valider, construire le même graphe d'objets à partir de la modélisation plus simple


