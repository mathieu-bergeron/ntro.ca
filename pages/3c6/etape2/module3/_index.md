---
title: 'Module 2.3: récursivité'
weight: 30
---

{{% pageTitle %}}

1. Théorie:

   - {{% link "/3c6/etape2/module3/theorie/pile_appel" "récursivité avec la pile d'appel" %}}
   - {{% link "/3c6/etape2/module3/theorie/donnees" "récursivité dans les données" %}}
   - {{% link "/3c6/etape2/module3/theorie/fibonacci" "exemple: séquence de Fibonacci" %}}

1. {{% link "/3c6/etape2/module3/entrevue" "Entrevue 2.3" %}}

   - **ATTENTION**: cette entrevue fait partie de la matière pour le mini-test 2.3 **et** pour l'examen 2

1. Mini-test théorique

   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=329182" target="_blank">Mathieu Bergeron</a>
   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=410359" target="_blank">Marc-Olivier Tremblay</a>

1. {{% link "./correctif" "Correctif 2.3" %}}

1. {{% link "/3c6/etape2/module3/atelier" "Atelier 2.3" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}
