---
title: "Entrevue 2.3: factorielle avec un graphe d'objets"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: à faire sur papier

<br>

1. Soit la fonction factorielle définie par:

    {{< katex display="block" class="text-center"  >}}
    F_0 = 1\\~\\

    F_n = n \times F_{n-1}\\~\\
    {{< /katex >}}

1. Modéliser la fonction factorielle avec une classe

    * un peu comme on a fait en théorie pour Fibonacci

    * doit contenir la valeur en entrée et la réponse

    * doit contenir une référence pour représenter l'aspect récursif

1. Dessiner le graphe d'objets pour {{<katex display="inline">}}F_3{{</katex>}} (factorielle de `3`)

    * devrait contenir 4 objets `Factoriel` et un objet `Calculateur`

