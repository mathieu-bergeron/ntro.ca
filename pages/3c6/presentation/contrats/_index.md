---
title: "Contrats de classe"
weight: 1
---


{{% pageTitle %}}

<center>
<div style="background-color:orange;width:50%;border:2px dashed black;padding:10px">
«On embauche pour des compétences et on congédie pour des attitudes»	
</div>
</center>

* L'attitude professionnelle pourrait valoir jusqu'à 20% de votre finale

## Étudiant.e

1. Tous mes écrans affichent du matériel pédagogique
1. Je parle à voix basse en tout temps
1. Ma musique est inaudible pour mes voisin.es
1. J'entre et je sors de la classe en silence
1. Je quitte la classe pour socialiser, manger, prendre une pause, etc.
1. Si en retard, j'attends qu'on m'ouvre et je rattrape mon retard sans déranger
1. En classe, j'utilise la file d'attente pour mes questions


## Prof

1. J'explique le déroulement de chaque cours
1. Je fais respecter les règles de classe 
1. Je m'assure que l'ambiance est toujours propice au travail
1. Je m'efforce de reformuler mes explications au besoin
1. Je suis disponible pour les questions

## Groupe1

{{% embed "./groupe1.md" %}}

## Groupe2

{{% embed "./groupe2.md" %}}

## Groupe3

{{% embed "./groupe3.md" %}}
