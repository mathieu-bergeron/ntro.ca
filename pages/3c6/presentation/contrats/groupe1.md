---
title: "Contrat de classe, groupe 1"
weight: 1
bookHidden: true
bookCollapseSection: true
---


### L'enseignant s'engage aussi à:


1. S'efforcer d'expliquer clairement, reformuler au besoin
1. Enthousiasme et énergie (le plus possible)
1. Mettre les notes de cours à l'avance
1. Être respectueux




### L'étudiant.e s'engage aussi à:

1. Être respectueux (idéallement de bonne humeur)
    * respecter les questions des autres
1. Faire le travail demander en classe. Être responsable de son apprentissage


