---
title: "Contrat de classe, groupe 3"
weight: 1
bookHidden: true
bookCollapseSection: true
---


### L'enseignant s'engage aussi à:

1. Prendre le temps d'écouter les questions
    * respecter la question
1. S'efforcer à énoncer clairement les notions
1. Être à l'heure




### L'étudiant.e s'engage aussi à:

1. Mettre l'effort. Être responsable de sa réussite
1. Ne pas hésiter à poser des questions
    * respecter la question de l'autre
1. Être à l'heure (remise à temps)

