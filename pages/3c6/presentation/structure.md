---
title: "Structure du cours"
weight: 30
---

{{% pageTitle %}}

{{% video 
    src="/3c6/presentation/structure01.mp4" 
    width="50%"
    formats="mp4" 
%}}

* Le cours est divisé en 5 étapes.

* Chaque étape contient 3 modules et se termine par un examen contenant un volet théorique et un volet pratique


## Travail à réaliser

### Le jour même où le module est présenté

1. Lire ou écouter l'exposé théorique 
1. Effectuer par moi-même une question d'entrevue *formative* 
    * le prof peut aussi me convoquer à faire la question avec lui
    * le but est de vérifier/encourager la compréhension de la théorie **avant** de commencer l'atelier
1. Effectuer le mini-test théorique, qui contient
    * quelques questions théoriques
    * un retour sur la question d'entrevue

### Au plus tard *une semaine* après la présentation de la théorie du module

1. Effectuer un atelier et le remettre sur Moodle

## Déroulement de chaque étape

<table>
<tr>
    <th>Séance
    </th>
    <th>Théorie abordée
    </th>
    <th>Mini-test à remettre
    </th>
    <th>Atelier à remettre
    </th>
</tr>
<tr>
<td>
1
</td>
<td>

* Théorie 1

</td>
<td>

* Mini-test théorique 1

</td>
<td>
</td>
</tr>
<tr>
<td>
2
</td>
<td>

* Théorie 2

</td>
<td>

* Mini-test théorique 2

</td>
<td>

</td>
<tr>
<td>
3
</td>
<td>

* Théorie 3

</td>
<td>

* Mini-test théorique 3

</td>
<td>

* Atelier 1

</td>
</tr>
<tr>
<td>
4
</td>
<td>

* Travail libre et questions

</td>
<td>


</td>
<td>

* Atelier 2

</td>
</tr>

<tr>
<td>
5
</td>
<td>

* Travail libre et questions

</td>
<td>

</td>
<td>

* Atelier 3

</td>
</tr>

<tr>
<td style="background-color:pink">
6
</td>
<td colspan="3" style="text-align:center;background-color:pink;">

Examen 

</td>
</table>



## Pour poser une question

{{% video 
    src="/3c6/presentation/structure02.mp4" 
    width="50%"
    formats="m4v mp4" 
%}}

### Pendant les heures de cours

1. Sur demande, effectuer la question d'entrevue avec le prof
1. Utiliser la <a href="https://aiguilleur.ca/file_d_attente/mathieu.bergeron">file d'attente</a> du prof

<br>

À la première connexion à `aiguilleur.ca`

1.  **Me créer mot de passe pour aiguilleur.ca**
  * Cliquer sur mon nom => *Ajouter un mot de passe*

    <img class="small-figure" src="/3c6/presentation/structure/aiguilleur01.png" />

  * Saisir le mot de passe deux fois => Cliquer sur *Ajouter mot de passe*

    <img class="small-figure" src="/3c6/presentation/structure/aiguilleur02.png" />
      

### En dehors des heures de cours

* Mathieu Bergeorn: par courriel à <a href="mailto:mathieu.bergeron@cmontmorency.qc.ca">mathieu.bergeron@cmontmorency.qc.ca</a>
  * heures de disponibilité
      * lundi 12:35 à 13:30
      * mecredi 14:25 à 15:15
      * vendredi 11:40 à 13:25
      * sur rendez-vous

<br>

* Marc-Olivier Tremblay: via Teams
