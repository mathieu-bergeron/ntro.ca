---
title: "Calendrier"
weight: 20
---

{{% pageTitle %}}

{{% video 
    src="/3c6/presentation/calendrier.mp4" 
    width="50%"
    formats="m4v mp4" 
%}}

<br> 

<table>
<tr>
  <th>Étape
  </th>
  <th>Contenu
  </th>
  <th>Date de l'examen
  </th>
</tr>
<tr>
<td style="text-align:center;">

### Étape 1

<br>(Semaines&nbsp;1-3)

</td>
<td>

* {{% link "/3c6/etape1/module1" "module 1.1: installation et introduction" %}}
* {{% link "/3c6/etape1/module2" "module 1.2: rappel concepts POO" %}}
* {{% link "/3c6/etape1/module3" "module 1.3: rappel tableau d'objets" %}}

</td>
<td style="text-align:center" ><span style="text-align:center;background-color:orange;padding:5px;font-weight:bold;">séance&nbsp;3.2</span>
<div style="margin-top:10px;">
</div>
</td>


</tr>


<tr>
<td style="text-align:center;">

### Étape 2

<br>(Semaines&nbsp;4-6)

</td>
<td>


<br>
<br>

* {{% link "/3c6/etape2/module1" "module 2.1: modélisation JSON" %}}
* {{% link "/3c6/etape2/module2" "module 2.2: modélisation Java (graphe d'objets)"  %}}
* {{% link "/3c6/etape2/module3" "module 2.3: récursivité (dans les données Vs pile d'appel)"  %}}

</td>
<td style="text-align:center"><span style="text-align:center;background-color:orange;padding:5px;font-weight:bold;">séance&nbsp;6.2</span>
<div style="margin-top:10px;">
</div>
</td>
</tr>

<tr>
<td style="text-align:center;">

### Étape 3

<br>(Semaines&nbsp;6-8)
</td>
<td>

* {{% link "/3c6/etape3/module1" "module 3.1: structures génériques (paramètres de type)" %}}
* {{% link "/3c6/etape3/module2" "module 3.2: tri naïf + notion d'efficacité"  %}}
* {{% link "/3c6/etape3/module3" "module 3.3: tri fusion"  %}}

</td>

<td style="text-align:center">
<span style="text-align:center;background-color:orange;padding:5px;font-weight:bold;">séance&nbsp;8.2</span>
<div style="margin-top:10px;">
</div>
</td>

</tr>
<tr>
<td style="text-align:center;">

### Étape 4

<br>(Semaines&nbsp;9-11)

</td>
<td>


* {{% link "/3c6/etape4/module1" "module 4.1: liste naïve"  %}}
* {{% link "/3c6/etape4/module2" "module 4.2: liste avec tableau"  %}}
* {{% link "/3c6/etape4/module3" "module 4.3: liste chaînée (simple et double)"  %}}

</td>
<td style="text-align:center">
<span style="text-align:center;background-color:orange;padding:5px;font-weight:bold;">séance&nbsp;11.2</span>
<div style="margin-top:10px;">
</div>
</td>
</tr>

<tr>
<td style="text-align:center;">

### Étape 5

<br>(Semaines&nbsp;12-15)

</td>
<td>

* {{% link "/3c6/etape5/module1" "module 5.1: mappage naïf"  %}}
* {{% link "/3c6/etape5/module2" "module 5.2: mappage avec table de hachage"  %}}
* {{% link "/3c6/etape5/module3" "module 5.3: mappage avec arbre"  %}}
</td>

<td style="text-align:center"><span style="text-align:center;background-color:orange;padding:5px;font-weight:bold;">séance&nbsp;15.2</span>
<div style="margin-top:10px;">
</div>
</td>
</tr>
</table>

* NOTE: consulter la <a href="https://www.cmontmorency.qc.ca/etudiants/cheminement-scolaire-et-registrariat/informations-nos-etudiants/calendriers/calendriers-scolaires/" target="_blank">répartition des jours d'enseignement</a> pour convertir le numéro de séance comme 6.2 en date comme 4 octobre 2024.

