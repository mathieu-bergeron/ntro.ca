---
title: "Présentation du cours"
weight: 1
bookCollapseSection: true
---

{{% pageTitle %}}

Le cours 3C6 présente l'idée de structure de données: comment organiser et trier les données d'un programme informatique, et l'impact que cette organisation sur l'efficacité du programme.

{{% video "/3c6/presentation/presentation.mp4" %}}

## 1. Liens utiles

{{% embed "/3c6/presentation/liens.md" %}}

## 2. {{% link "/3c6/presentation/calendrier" "Contenu des étapes" %}}

## 3. {{% link "/3c6/presentation/calendrier_seances" "Calendrier des séances" %}}

## 4. {{% link "/3c6/presentation/structure" "Structure du cours" %}}

## 5. {{% link "/3c6/presentation/evaluations" "Évaluations" %}}

## 6. {{% link "/3c6/presentation/materiel" "Matériel à se procurer" %}}


