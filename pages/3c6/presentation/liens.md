---
title: 'Liens utiles'
weight: 10
---

* <a href="/3c6/plan/420-3C6-MO-A24_MB_MOT.pdf" download>Plan de cours</a>
* Pages Moodle du cours
  * <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=5671" target="_blank">Mathieu Bergeron</a>
  * <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9348" target="_blank">Marc-Olivier Tremblay</a>
* <a href="/3c6/plan/regles_departement_informatique.pdf" download>Règles département d'informatique</a>
* <a href="/3c6/NormesProgrammationJava.pdf" download>Normes de programmation</a>
* Code des exemples en classe: https://gitlab.com/mathieu-bergeron/3c6_exemples
* Centre d'aide: https://www.cmontmorency.qc.ca/etudiants/services-aux-etudiants/aide-a-la-reussite/aide-techniques/centre-daide-en-informatique/
