---
bookHidden: true
---

<div class="excerpt max-width-75" style="margin:auto;">

RAPPEL: vous devez programmer **vous même** tout votre code

* ne **jamais** utiliser ChatGPT ou autre IA
* ne **jamais** copier-coller le code d'un autre étudiant
* ne **jamais** écrire directement le code qu'un autre étudiant vous dicte

</div>
