---
title: "Mathieu Bergeron"
weight: 200
---

{{% pageTitle %}}

## Cours

* Est-ce que vous avez des périodes libre ou l'on peut vous poser des questions (pendant le cours ou après cours)

## Parcours

* ca va?
* Quel est ton parcours de carrière, donc ce qui t'as mené à devenir enseignant en informatique ?
* combien d'années avez-vous travailler en programmation
* pourquoi être devenu prof?
* votre parcours académique
* votre age ?
* Depuis combien de temps enseignez-vous?
* quels on été vaut étude
* Quelle est le trajet que tu as pris pour être où tu es maintenant.
* Quel est votre parcours ?
* Qu'elle domaine préfères tu dans la programmation?

## Programmation

* Quel est votre langage de programmation préféré?
* Qu'elle sont vos forces en informatique?
* Comment et quand avez-vous découvert votre passion pour la programmation?

## Loisirs

* Quel est votre loisir préféré
* Un pays que vous voulez visiter?
* Quel est le jeu vidéo qui vous a le plus marqué ?

    * https://us.shop.battle.net/en-us/product/starcraft
    * https://youtu.be/lBIX6hHRtDQ?si=ewoyRlYpZLcKPrAj&t=217


* Avez-vous une passion autre que l'informatique?


