---
title: "Évaluations"
weight: 40
---

{{% pageTitle %}}


{{% video 
    src="/3c6/presentation/evaluations.mp4" 
    width="50%"
    formats="m4v mp4" 
%}}

### Évaluation des étapes

* Chaque étape vaut 20pts

    * Les mini-tests valent 5pts

    * Les ateliers valent 5pts

    * Chaque étape se termine par **un examen de 10pts à faire en classe**
        * l'examen est à faire **en classe**, **documentation permise**
  

### Évaluation des modules

* Pour chaque module, les activités suivantes sont évaluées:

    1. Le mini-test théorique **le jour même** de la présentation du module

    1. L'atelier **une semaine après** la présentation du module

        * La remise de l'atelier est via Git. <br>Une pénalité de 10% est appliquée pour les remises non-fonctionnelles dans Git.


### Résumé des évaluations

<table>
<tr>
    <th>Étape
    </th>
    <th>Évaluations
    </th>
</tr>
<tr>
    <td style="text-align:center;">Étape 1
    </td>
<td>

* 5pts) mini-test théoriques 1.1, 1.2, 1.3

* 5pts) ateliers 1.1, 1.2, 1.3

* 10pts) examen 1

</td>
</tr>
<tr>
    <td style="text-align:center;">Étape 2
    </td>
<td>

* 5pts) mini-test théoriques 2.1, 2.2, 2.3

* 5pts) ateliers 2.1, 2.2, 2.3

* 10pts) examen 2

</td>


</tr>
<tr>
    <td style="text-align:center;">Étape 3
    </td>
<td>

* 5pts) mini-test théoriques 3.1, 3.2, 3.3

* 5pts) ateliers 3.1, 3.2, 3.3

* 10pts) examen 3

</td>
</tr>
<tr>
  <td style="text-align:center;">Étape 4
  </td>
<td>

* 5pts) mini-test théoriques 4.1, 4.2, 4.3

* 5pts) ateliers 4.1, 4.2, 4.3

* 10pts) examen 4

</td>
</tr>
<tr>
    <td style="text-align:center;">Étape 5
    </td>
<td>

* 5pts) mini-test théoriques 5.1, 5.2, 5.3

* 5pts) ateliers 5.1, 5.2, 5.3

* 10pts) examen 5

</td>
</tr>
</table>

