---
title: 'Calendrier groupes 1, 3'
weight: 21
---

{{% pageTitle %}}

<table>
<tr>
  <th>Semaine
  </th>
  <th>Séance
  </th>
  <th>Évaluation
  </th>
  <th>Groupe 1
  </th>
  <th>Groupe 3
  </th>
</tr>
<tr>
<tr>

<td rowspan="2">
1
</td>

<td>
1
</td>

<td>

- Mini-test théorique&nbsp;1.1

</td>

<td>
21 août
</td>

<td>
21 août
</td>

</tr>

<tr>

<td>
2
</td>

<td>

*annulée*


</td>

<td>
26 août
</td>

<td>
26 août
</td>

</tr>

<tr>

<td rowspan="2">
2
</td>

<td>
1
</td>

<td>

- Mini-test théorique&nbsp;1.2
- (fermeture mini-test 1.1)

</td>

<td>
28 août
</td>

<td>
28 août
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- Mini-test théorique 1.3
- (fermeture mini-test 1.2)
- Remise atelier 1.1


</td>

<td>
4 septembre
<br>(horaire du lundi)
</td>

<td>
4 septembre
<br>(horaire du lundi)
</td>

</tr>

<tr>

<td rowspan="2">
3
</td>

<td>
1
</td>

<td>

- Remise atelier 1.2
- (fermeture mini-test 1.3)

</td>

<td>
9 septembre
</td>

<td>
9 septembre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- (avant examen: remise atelier 1.3)
- **Examen 1**

</td>

<td>
11 septembre
</td>

<td>
11 septembre
</td>

</tr>

<tr>

<td rowspan="2">
4
</td>

<td>
1
</td>

<td>

- Mini-test théorique 2.1

</td>

<td>
16 septembre
</td>

<td>
16 septembre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- Mini-test théorique 2.2

</td>

<td>
18 septembre
</td>

<td>
18 septembre
</td>

</tr>

<tr>

<td rowspan="2">
5
</td>

<td>
1
</td>

<td>

- Mini-test théorique 2.3
- Atelier 2.1

</td>

<td>
23 septembre
</td>

<td>
23 septembre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- Atelier 2.2

</td>

<td>
25 septembre
</td>

<td>
25 septembre
</td>

</tr>

<tr>

<td rowspan="2">
6
</td>

<td>
1
</td>

<td>

- Atelier 2.3

</td>

<td>
30 septembre
</td>

<td>
30 septembre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- **Examen 2**

</td>

<td>
2 octobre
</td>

<td>
2 octobre
</td>

</tr>

<tr>

<td rowspan="2">
7
</td>

<td>
1
</td>

<td>

- Mini-test théorique 3.1

</td>

<td>
7 octobre 
</td>

<td>
7 octobre 
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- Mini-test théorique 3.2

</td>

<td>
9 octobre
</td>

<td>
9 octobre
</td>

</tr>

<tr>

<td rowspan="2">
8
</td>

<td>
1
</td>

<td>

- Mini-test théorique 3.3
- Atelier 3.1

</td>

<td>
16 octobre
</td>

<td>
16 octobre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- Atelier 3.2

</td>

<td>
21 octobre
</td>

<td>
21 octobre
</td>

</tr>

<tr>

<td rowspan="2">
9
</td>

<td>
1
</td>

<td>

* Atelier 3.3

</td>

<td>
23 octobre
</td>

<td>
23 octobre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- **Examen 3**

</td>

<td>
28 octobre
</td>

<td>
28 octobre
</td>

</tr>

<tr>

<td rowspan="2">
10
</td>

<td>
1
</td>

<td>

- Mini-test théorique 4.1

</td>

<td>
30 octobre
</td>

<td>
30 octobre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- Mini-test théorique 4.2

</td>

<td>
4 novembre
</td>

<td>
4 novembre
</td>

</tr>

<tr>

<td rowspan="2">
11
</td>

<td>
1
</td>

<td>

- Mini-test théorique 4.3
- Atelier 4.1

</td>

<td>
6 novembre
</td>

<td>
6 novembre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- Atelier 4.2

</td>

<td>
13 novembre
<br>(horaire du lundi)
</td>

<td>
13 novembre
<br>(horaire du lundi)
</td>

</tr>

<tr>

<td rowspan="2">
12
</td>

<td>
1
</td>

<td>

- Atelier 4.3

</td>

<td>
18 novembre
</td>

<td>
18 novembre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- **Examen 4**

</td>

<td>
20 novembre
</td>

<td>
20 novembre
</td>

</tr>

<tr>

<td rowspan="2">
13
</td>

<td>
1
</td>

<td>

- Mini-test théorique 5.1

</td>

<td>
25 novembre
</td>

<td>
25 novembre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- Mini-test théorique 5.2

</td>

<td>
27 novembre
</td>

<td>
27 novembre
</td>

</tr>

<tr>

<td rowspan="2">
14
</td>

<td>
1
</td>

<td>

- Mini-test théorique 5.3
- Atelier 5.1

</td>

<td>
2 décembre
</td>

<td>
2 décembre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- Atelier 5.2

</td>

<td>
4 décembre
</td>

<td>
4 décembre
</td>

</tr>

<tr>

<td rowspan="2">
15
</td>

<td>
1
</td>

<td>

- Atelier 5.3

</td>

<td>
9 décembre
</td>

<td>
9 décembre
</td>

</tr>

<tr>

<td>
2
</td>

<td>

- **Examen 5**

</td>

<td>
11 décembre
</td>

<td>
11 décembre
</td>

</tr>

</table>
