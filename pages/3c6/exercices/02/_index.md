---
title: ""
weight: 1
bookHidden: true
---

<style type="text/css">code{background-color:#fafafa; padding:4px;}</style>

# Atelier 02: les tableaux

## Créer le projet `atelier02`

Dans *Eclipse*:

1. Créer le projet `atelier02`

1. Sélectionner l'encodage *UTF-8* pour tout le projet:
    * Clique-droit sur le projet => *paramètres*
        * Dans *Resources* => *Text file encoding* =>
            * Choisir *Other* et sélectionner *UTF-8*
            * Cliquer sur *Apply*

1. Sauvegarder {{% download "./z4_atelier02.jar" "z4_atelier02.jar"  %}}
**à la racine** de votre projet Eclipse

1. Ajouter `z4_atelier02.jar` comme librairie externe du projet
    * Clique-droit sur le projet =>
    * *Build Path* =>
    * *Configure Build Path* =>
        * Onglet *Librairies* =>
            * *Classpath* =>
                * *Add JAR* =>
                * Sélectionner `z4_atelier02.jar`

## Créer la classe `Atelier02`

1. Importer la classe `Z4`

```java
{{% embed src="./Atelier02.java" first-line="3" last-line="3" %}}
```

1. Créer la méthode `main`

```java
{{% embed src="./Atelier02.java" first-line="7" last-line="7" %}}
```

1. Dans la méthode `main`, lancer la validation via `Z4`

```java
{{% embed src="./Atelier02.java" first-line="7" last-line="11" %}}
```


## Créer la méthode `echanger`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="14" last-line="14" %}}
```

### Spécification

Entrée:

* le `tableau` à modifier
* l'indice `i` du premier élément à échanger
* l'indice `j` du deuxième élément à échanger

Sortie:

* un nouveau tableau où les éléments en `i` et `j` ont changés de place

## Créer la méthode `toString`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="15" last-line="15" %}}
```

### Spécification

Entrée:

* le `tableau` à formater en chaîne

Sortie:

* une chaîne débutant par `{`, terminant par `}` et où les éléments sont séparés par `, `
* p.ex. `{1, 2, 3}`

## Créer la méthode `ajusterDebut`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="16" last-line="16" %}}
```

### Spécification

Entrée:

* un indice `debut`

Sortie:

* si le début est négatif, alors retourer `0`
* sinon retourner le début

## Créer la méthode `ajusterFin`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="17" last-line="17" %}}
```

### Spécification

Entrée:

* un indice `fin`

Sortie:

* si `fin` est au-delà du tableau, alors retourner l'indice du dernier élément du tableau
* sinon retourner `fin`

## Créer la méthode `siIndicesCoherents`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="18" last-line="18" %}}
```

### Spécification

Entrée:

* un indice `debut`
* un indice `fin`

Sortie:

* pour que les indices soient cohérents, **il ne faut pas**:
    * que le `debut` soit après la `fin`
    * que le `debut` soit après le dernier élément du tableau
    * que la `fin` soit avant le premier élément du tableau

## Créer la méthode `extraireQuandIndicesValides`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="19" last-line="19" %}}
```

### Spécification

Entrée:

* le `tableau` d'où extraire les éléments
* un indice `debut`
* un indice `fin`

Sortie:

* un nouveau tableau qui contient les éléments compris entre `debut` et `fin` (inclusivement)

## Créer la méthode `extraire`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="20" last-line="20" %}}
```

### Spécification

Entrée:

* le `tableau` d'où extraire les éléments
* un indice `debut`
* un indice `fin`

Sortie:

* si les indices ne sont pas cohérents, retourner un tableau vide
* sinon:
    * ajuster le `debut` grace à la méthode `ajusterDebut`
    * ajuster la `fin` grace à la méthode `ajusterFin`
    * retourner les éléments à extraire grace à `extraireQuandIndicesValides`

## Créer la méthode `fusionner`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="21" last-line="21" %}}
```

### Spécification

Entrée:

* le tableau de `gauche`
* le tableau de `droite`

Sortie:

* un nouveau tableau qui contient d'abord les éléments du tableau de `gauche`, puis ceux du tableau de `droite`

## Créer la méthode `retirer`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="22" last-line="22" %}}
```

### Spécification

Entrée:

* le `tableau` à modifier
* l'`indice` de l'élément à retirer

Sortie:

* un nouveau tableau contenant les mêmes éléments, sauf celui à retirer

#### NOTE

* Cette méthode s'implante en trois lignes si vous appelez judicieusement les méthodes `extraire` et `fusionner`

## Créer la méthode `ajouter`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="23" last-line="23" %}}
```

### Spécification

Entrée:

* le `tableau` à modifier
* un `element` à ajouter à la fin du tableau

Sortie:

* un nouveau tableau contenant les mêmes éléments, plus `element` ajouté à la fin

#### NOTE

* Cette méthode s'implante en une ligne si vous appelez judicieusement `fusionner`

## Créer la méthode `insererAvant`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="24" last-line="24" %}}
```

### Spécification

Entrée:

* le `tableau` à modifier
* l'`indice` avant lequel il faut insérer un élément
* l'`element` à ajouter

Sortie:

* un nouveau tableau contenant les mêmes éléments, plus `element` ajouté juste avant l'`indice`

#### NOTE

* Cette méthode s'implante en quatre lignes si vous appelez judicieusement les méthodes `extraire`, `ajouter` et `fusionner`

## Créer la méthode `inserer`

### Signature

```java
{{% embed src="./Atelier02.java" first-line="25" last-line="25" %}}
```

### Spécification

Entrée:

* le `tableau` à modifier
* un `element` à insérer au début du tableau

Sortie:

* un nouveau tableau contenant les mêmes éléments, plus `element` inséré au début

#### NOTE

* Cette méthode s'implante en une ligne si vous appelez judicieusement `insererAvant`

# Remise

1. **Faire valider** en clase par l'enseignant.e
1. **Remettre sur Moodle** votre fichier `Atelier02.java`

NOTE:

* Il faut faire valider au courant des deux prochaines séances
