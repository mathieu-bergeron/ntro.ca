---
title: ""
weight: 1
bookHidden: true
---


# Exercices supplémentaires

## Tableaux à une dimension
 
* <a href="tableaux.pdf" target="_blank">Théorie</a>
    * voir à la fin pour les algorithmes
* <a href="/3c6/exercices/02/">Atelier 02</a>
    * NOTES:
        * Il faut installer *Firefox* pour afficher la validation
        * Utiliser `tableau.clone()` pour créer une copie du tableau reçu en entrée

## Tri et recherche

* <a href="recherche_et_tri.pdf" target="_blank">Théorie</a>
    * voir à la fin pour les algorithmes
* <a href="/3c6/exercices/03/">Atelier 03</a>
    * NOTES:
        * Il faut installer *Firefox* pour afficher la validation
        * Utiliser `tableau.clone()` pour créer une copie du tableau reçu en entrée