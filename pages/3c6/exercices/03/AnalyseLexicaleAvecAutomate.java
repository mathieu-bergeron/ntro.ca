import java.lang.reflect.Array;

public class AnalyseLexicaleAvecAutomate {

	public static String chaine = "ab cde fg \"h i\" a g d  \"as dfa sdf sd f \" asd fas df  df";

	public static enum Etats {A,B};

	public static void main(String[] args) {


		System.out.println(chaine);
		
		System.out.println(toString(decouperChaine(chaine)));




	}

	public static String[] decouperChaine(String chaine) {

		String[] decoupage = new String[0];

		char[] caracteres = chaine.toCharArray();

		Etats etatCourant = Etats.A;

		String motEnCours = "";

		for(char caractere : caracteres) {

			switch(etatCourant) {

			case A:
				switch(caractere) {

				case '"':
					etatCourant = Etats.B;
					motEnCours += caractere;
					break;

				case ' ':
					decoupage = ajouter(decoupage, motEnCours);
					motEnCours = "";
					break;

				default:
					motEnCours += caractere;
					break;
				}
				break;

			case B:
				
				switch(caractere) {
				case '"':
					etatCourant = Etats.A;
					motEnCours += caractere;
					break;
					
				default:
					motEnCours += caractere;
					break;
				}

				break;
			}
		}
		
		if(!motEnCours.isEmpty()) {
			decoupage = ajouter(decoupage, motEnCours);
		}

		return decoupage;
	}









	
static String toString(Object tableau) {
		
		StringBuilder retour = new StringBuilder();

		retour.append("{");
		
		if(Array.getLength(tableau) > 0) {
			
			retour.append(Array.get(tableau, 0));

		}

		for(int i = 1; i < Array.getLength(tableau); i++) {
			
			retour.append(",");
			retour.append(Array.get(tableau, i));
		}

		retour.append("}");

		return retour.toString();
	}












	public static String[] fusionner(String[] gauche, String[] droite) {

		String[] fusion = new String[gauche.length + droite.length];

		for(int i = 0 ; i < fusion.length; i++) {

			if(i < gauche.length) {

				fusion[i] = gauche[i];

			}else {

				fusion[i] = droite[i - gauche.length];

			}
		}

		return fusion;
	}

	public static String[] ajouter(String[] tableau, String element) {

		return fusionner(tableau, new String[] {element});

	}


}
