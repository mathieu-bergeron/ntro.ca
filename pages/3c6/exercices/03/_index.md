---
title: ""
weight: 1
bookHidden: true
---

<style type="text/css">code{background-color:#fafafa; padding:4px;}</style>

# Atelier 03: tri et recherche

## Créer le projet `atelier03`

Dans *Eclipse*:

1. Créer le projet `atelier03`

1. Sélectionner l'encodage *UTF-8* pour tout le projet:
    * Clique-droit sur le projet => *paramètres*
        * Dans *Resources* => *Text file encoding* =>
            * Choisir *Other* et sélectionner *UTF-8*
            * Cliquer sur *Apply*

1. Sauvegarder {{% download "./z4_atelier03.jar" "z4_atelier03.jar" %}} **à la racine** de votre projet Eclipse

1. Ajouter `z4_atelier03.jar` comme librairie externe du projet
    * Clique-droit sur le projet =>
    * *Build Path* =>
    * *Configure Build Path* =>
        * Onglet *Librairies* =>
            * *Classpath* =>
                * *Add JAR* =>
                * Sélectionner `z4_atelier03.jar`

1. Copier-coller votre fichier `Atelier02.java` dans le répertoire `src` du projet `atelier03`


## Créer la classe `Atelier03`

1. Importer la classe `Z4`

```java
{{% embed src="./Atelier03.java" first-line="3" last-line="4" %}}
```

1. Créer la méthode `main`

```java
{{% embed src="./Atelier03.java" first-line="9" last-line="9" %}}
```

1. Dans la méthode `main`

```java
{{% embed src="./Atelier03.java" first-line="7" last-line="11" %}}
```


## Créer la méthode `indiceMin`

### Signature

```java
{{% embed src="./Atelier03.java" first-line="14" last-line="14" %}}
```

### Spécification

Entrée:

* un `tableau`

Sortie:

* l'**indice** de l'élément le plus petit du tableau

## Créer la méthode `triParSelection`

### Signature

```java
{{% embed src="./Atelier03.java" first-line="15" last-line="15" %}}
```

### Spécification

Entrée:

* un `tableau`

Sortie:

* un nouveau tableau, trié du plus petit au plus grand

## Créer la méthode `siDejaTrie`

### Signature

```java
{{% embed src="./Atelier03.java" first-line="16" last-line="16" %}}
```

### Spécification

Entrée:

* un `tableau`

Sortie:

* `true` si le `tableau` est trié
* `false` sinon

## Créer la méthode `monterBullesSousPlafond`

### Signature

```java
{{% embed src="./Atelier03.java" first-line="17" last-line="17" %}}
```

### Spécification

Entrée:

* un `tableau` à modifier
* le `plafond` à respecter

Sortie:

* un nouveau tableau où les bulles ont été montées (de gauche à droite), en respectant le plafond



## Créer la méthode `triParBulles`

### Signature

```java
{{% embed src="./Atelier03.java" first-line="18" last-line="18" %}}
```

### Spécification

Entrée:

* le `tableau` à trier

Sortie:

* un nouveau tableau, trié de plus petit au plus grand


## Créer la méthode `rechercheDichotomique`

### Signature

```java
{{% embed src="./Atelier03.java" first-line="21" last-line="21" %}}
```

### Spécification

Entrée:


* le `tableau` où chercher
* l'`element` à chercher

Sortie:

* l'indice de l'`element` dans le tableau

### Note

* Pour effectuer la recherche, appeler `chercherAGauche` et `chercherADroite` sur les bons sous-tableaux

## Créer la méthode `chercherAGauche`

### Signature

```java
{{% embed src="./Atelier03.java" first-line="19" last-line="19" %}}
```

### Spécification

Entrée:

* le `tableau` où chercher
* le `pivot` à utiliser pour diviser le tableau
* l'`element` à chercher

Sortie:

* l'indice de l'`element` dans le sous-tableau *à gauche* du `pivot`

### Note

* Pour effectuer la recherche, appeler `rechercheDichotomique` sur le bon sous-tableau

## Créer la méthode `chercherADroite`

### Signature

```java
{{% embed src="./Atelier03.java" first-line="20" last-line="20" %}}
```

### Spécification

Entrée:

* le `tableau` où chercher
* le `pivot` à utiliser pour diviser le tableau
* l'`element` à chercher

Sortie:

* l'indice de l'`element` dans le sous-tableau *à droite* du `pivot`

### Note

* Pour effectuer la recherche, appeler `rechercheDichotomique` sur le bon sous-tableau

# Remise

1. **Faire valider** en clase par l'enseignant.e
1. **Remettre sur Moodle** votre fichier `Atelier03.java`

NOTE:

* Il faut faire valider au courant des deux prochaines séances
