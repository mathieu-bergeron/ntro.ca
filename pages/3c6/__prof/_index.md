---
title: "Cloner le matériel du cours"
draft: false
bookHidden: true
---

{{% pageTitle %}}

## Cloner les dépôts

1. Cloner le dépôt racine

    ```bash
    $ git clone https://gitlab.com/mathieu-bergeron/cours_3c6.git
    ```

1. Cloner les autres dépôts via le script init_https.sh

    ```bash
    $ cd cours_3c6
    $ sh init_https.sh
    ```

## Utiliser les scripts pour faire `pull` et `commit`/`push`

1. Pour faire un `pull` de tous les dépôts

    ```bash
    $ sh scripts/pull.sh
    ```

1. Pour faire un `commit` sur tous les dépôts

    ```bash
    $ sh scripts/commit.sh "commentaire de commit"
    ```

1. Pour faire un `push` pour tous les dépôts

    ```bash
    $ sh scripts/push.sh
    ```

## Le site Web

1. Installer Hugo (édition extended): https://gohugo.io/installation/windows/

1. Afficher le site Web en local

    * faire

        ```bash
        $ cd web
        $ cd ntro.ca
        $ sh hugo.sh
        ```

    * puis naviguer sur [http://localhost:1313](http://localhost:1313)

1. Pousser sur le serveur

    ```bash
    $ cd web
    $ cd ntro.ca
    
    # commande: publish.sh "inclure" "exclure1" "exclure2"

    # par exemple, publier etape1/module1 sans pousser les vidéos
    $ sh scripts/publish.sh "3c6/etape1/module1" "webm" "mp4"

    # ou créer un script raccourci
    $ sh scripts/maj_etape1_module1.sh
    ```

    * **ATTENTION**: en Windows, le script `publish.sh` pousse uniquement les fichiers, pas les nouveaux répertoires
        * (il faudrait ajouter des commandes `sftp` pour vérifier la présence d'un répertoire et le créer au besoin)

## Le site Web privé

1. Répertoire `web/_ntro.ca`

1. Utilisé pour rédiger certaines questions d'examens (qui sont ensuite recopiées à la main dans Moodle)

    ```bash
    $ cd web
    $ cd _ntro.ca
    $ sh hugo.sh
    ```

## Le code, version prof

1. Créer les projets Eclipse

    ```bash
    $ cd cours_3c6
    $ sh scripts/eclipse.sh
    ```

1. Créer un Workspace `3c6_prof`

1. Importer les projets Eclipse des répertoires suivants

    * `cours_3c6/code/ntro_3c6`
    * `cours_3c6/code/cards`
    * `cours_3c6/code/ateliers_3c6`

1. **NOTE**

    * avant d'exécuter une solution (p.ex. `Procedure` ou `MonAtelier1_2_A`):
        * générer les données (p.ex. avec `Generer` ou `GenerateurAtelier1_2_A`)

1. Les projets suivants sont associés aux ateliers suivants

    ```
    freesort_solution         => atelier1_1
    shift_solution            => atelier1_2
    shift2_solution           => atelier2_1_A
    validator_shift2_solution => atelier2_1_B
    shift3_solution           => atelier2_2_A
    validator_shift3_solution => atelier2_2_B
    fibonacci_solution        => atelier2_3
    ```

1. NOTES

    * le projet `3c6_exemples` contient le code de démos faites en classe l'an passé
    * le projet `examens_3c6` contient du code qui servait lors d'examens pratiques en Eclipse (plus utilisé, mais peut servir d'inspiration)


## Le code, version étudiant

1. Re-compiler les `.jar` et les `.db` destinés aux étudiants

    ```bash
    $ cd code/enonces_3c6

    # mettre à jour les .jar et les .db
    $ sh scripts/maj.sh   
    ```

1. Créer un Workspace `3c6_etudiant`

1. Importer les projets Eclipse du répertoire suivant

    * `cours_3c6/code/enonces_3c6`

1. Re-compiler le .zip avec les fichiers de départ

    * installer `zip` pour GitBash (non-testé):
        * https://ranxing.wordpress.com/2016/12/13/add-zip-into-git-bash-on-windows/

    * ensuite:

        ```bash
        $ cd code/enonces_3c6

        # créer le fichier .zip de départ
        $ sh scripts/fichier_de_depart.sh

        # créer le fichier .zip avec les solutions
        $ sh scripts/fichier_de_depart.sh avec_solutions
        ```
