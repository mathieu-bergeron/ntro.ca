const boutonCartes = document.getElementById('bouton-cartes');
const spansCartes = document.getElementsByClassName('span-cartes');

const choisir = function(valeurs){
    const indice = Math.floor(Math.random() * valeurs.length);
    return valeurs[indice];
}

const genererTexteCartes = function(nombreDeCartes){

    const numeros = [2,3,4,5,6,7,8,9,10];
    const sortes = ["♡","♢","♧","♤"];
    const nombreMaxEssais = 10000;
    const lesCartes = [];

    for(var i = 0; i < nombreMaxEssais; i++){
        const numero = choisir(numeros);
        const sorte = choisir(sortes);
        const carte = numero + sorte;

        if(!lesCartes.includes(carte)){
            lesCartes.push(carte);
        }

        if(lesCartes.length >= nombreDeCartes){
            break;
        }
    }

    var texteLesCartes = "";

    for(var i = 0; i < lesCartes.length; i++){
        texteLesCartes += lesCartes[i] + "  ";
    }

    return texteLesCartes;

}


boutonCartes.onclick = function(){
    for(var i = 0; i < spansCartes.length; i++){
        const spanCartes = spansCartes[i];
        var nombreDeCartes = parseInt(spanCartes.getAttribute('nombre-de-cartes'));
        if(!nombreDeCartes){
            nombreDeCartes = 6;
        }
        spanCartes.textContent = genererTexteCartes(nombreDeCartes);
    }
}

window.onload = function(){

    boutonCartes.click();

}

