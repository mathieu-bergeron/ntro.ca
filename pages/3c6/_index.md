---
title: "3C6: Structures de données"
weight: 10
# bookFlatSection: false
# bookToc: true
# bookHidden: false
bookCollapseSection: true
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

## {{% link "/3c6/presentation" "Présentation détaillée du cours" %}}

### Liens rapides

* {{% link "/3c6/etape1/module1/atelier/g/" "Pour travailler sur un nouvel ordi" %}}
* {{% link "/3c6/etape1/module1/erreurs" "Guides en cas d'erreurs" %}}
* Code des exemples en classe: https://gitlab.com/mathieu-bergeron/3c6_exemples
* {{% link "/3c6/presentation/calendrier" "Calendrier" %}}
* {{% link "/3c6/presentation/structure" "Structure du cours" %}}
* {{% link "/3c6/presentation/evaluations" "Évaluations" %}}


### Autres liens utiles

{{% embed "/3c6/presentation/liens.md" %}}

