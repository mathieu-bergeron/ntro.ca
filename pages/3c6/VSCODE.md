---
title: ""
weight: 1
bookHidden: true
draft: true
---

## Installer

1. https://code.visualstudio.com/Download

## Vider la cache

* `C:\Users\MON_UTILISATEUR\AppData\Roaming\Code\User\workspaceStorage`
    * trouver quel sous-répertoire a un `workspace.json` qui contient le répertoire de votre workspace
    * effacer ce sous-répertoire

## Désinstaller VSCode

* Supprimer 
    * `C:\Users\MON_UTILISATEUR\AppData\Local\Programs\Microsoft VS Code`
    * `C:\Users\MON_UTILISATEUR\.vscode`


## VIEUX


1. Installer l'extension gradle

    * Ctrl+P
    * `ext install vscjava.vscode-gradle`

1. Installer l'extension Java de RedHat

    * Ctrl+P
    * `ext install redhat.java`

1. (autre option): installer l'extension Java de Microsoft

    * Ctrl+P
    * `ext install vscjava.vscode-java-pack`

<!--

1. (optionnel) installer le mode *vim*

    * Ctrl+P
    * `ext install vscodevim.vim`

-->

1. Configurer: java.configuration.runtimes

    ```json
    "java.configuration.runtimes": [
        {
            "name": "JavaSE-11",
            "path": "/chemin/vers/jdk-11",
        },
        {
            "name": "JavaSE-17",
            "path": "/chemin/vers/jdk-17",
        }
     ]
    ```

1. Lancer `code` à la racine du dépôt Git

    ```bash
    $ cd ~/chemin/vers/depot/git
    $ code .
    ```


