---
title: 'Module 1.1: installation + trier des cartes'
weight: 1
---

{{% pageTitle %}}

<!--
<center>
<video width="50%" src="rappel.mp4" type="video/mp4" controls playsinline>
</center>
-->

1. Théorie

   1. {{% link "/3c6/presentation" "Présentation du cours" %}}
   1. {{% link "/3c6/approche/trier" "Trier des cartes" %}}

1. **Entrevue 1.1**: trier six cartes choisies au hasard: <span class="span-cartes" nombre-de-cartes="6"></span>

   - (rappel: ♡ ♢ ♧ ♤)
   - <button id="bouton-cartes">Générer</button>

1. Mini-test théorique

   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=322635" target="_blank">Mathieu Bergeron</a>
   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=410324" target="_blank">Marc-Olivier Tremblay</a>

1. {{% link "./atelier" "Atelier 1.1" %}}: installation + synchronisation Git

1. {{% link "./atelier/g/" "Pour travailler sur un nouvel ordi" %}}

1. {{% link "./erreurs" "Guides en cas d'erreur" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}

<script src="/3c6/cartes.js"></script>

<style>
#span-cartes{
    padding:0.2rem;
}
</style>
