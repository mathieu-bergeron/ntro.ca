---
title: "Erreurs avec VSCode"
weight: 1
bookHidden: true
---


{{% pageTitle %}}


## Effacer la cache de `VSCode`

1. Naviguer jusqu'à `C:\Users\Mon Usager\AppData\Roaming\Code\User\workspaceStorage`

    <img src="naviguer.png"/>

    ```bash
    $ cd '/c/Users/Mon Usager/AppData/Roaming/Code/User/workspaceStorage'
    ```

1. S'assurer d'être dans le bon répertoire

    ```bash
    $ pwd 
        /c/Users/Mathieu Bergeron/AppData/Roaming/Code/User/workspaceStorage
    ```

1. Effacer tous les sous-répertoires

    <img src="supprimer.png"/>

    ```bash
    $ rm -rf *
    ```
