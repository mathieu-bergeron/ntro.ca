---
title: "Erreurs avec le JDK"
weight: 1
bookHidden: true
---


{{% pageTitle %}}

{{<excerpt class="note">}}
Ne pas effectuer cette étape sur un Windows du Collège
{{</excerpt>}}

## Erreur de type `JAVA_HOME` non-trouvé

1. En Windows, faire {{% key "⊞" %}} et chercher pour *variables d'env*


<img src="chercher_variables.png"/>

1. Naviguer vers les variables d'environnement

<img src="proprietes_systeme.png" />

1. Ajouter une nouvelle variable nommée `JAVA_HOME`

<img src="variables_env.png" />


1. Utiliser *Parcourir le Répertoire* pour être sûr d'avoir le bon chemin
<img src="nouvelle_java_home.png" />

1. Finalement ajouter ou modifier la variable `Path`

<img src="modifier_path.png" />

    * ajouter exactement `%JAVA_HOME%\bin ;`



## Ajouter un `JAVA_HOME` et un `PATH` local via GitBash

1. Ouvrir un Explorateur de fichier et naviguer vers le dossier du JDK

1. Ouvrir un GitBash et saisir `cd` puis un {{% key "espace" %}}

    ```bash
    $ cd 
    ```

1. Glisser le dossier de l'explorateur de fichier vers GitBash

    <img src="chemin_vers_gitbash01.png"/>

1. Après avoir glissé, le chemin devrait apparaître dans GitBash

    <img src="chemin_vers_gitbash02.png"/>

1. Dans GitBash, faire {{% key "Entrée" %}}

1. Vérifier le chemin courant

    ```bash
    $ pwd
    /c/Program Files/Java/jdk-17
    ```

1. Ajouter `JAVA_HOME` au `.bash_profile` avec la commande suivante:

    ```bash
    $ echo "JAVA_HOME='$(pwd)'" >> ~/.bash_profile
    ```

1. Ouvrir le fichier `.bash_profile` avec la commande suivante:

    ```bash
    $ notepad ~/.bash_profile
    ```

1. Ajouter la ligne suivante au fichier `.bash_profile`

    ```bash
    PATH=$JAVA_HOME/bin:$PATH
    ```

1. Vérifier que la configuration est correcte

    ```bash
    $ source ~/.bash_profile
    $ which java
    /c/Program Files/Java/jdk-17/bin/java

    $ echo $JAVA_HOME
    /c/Program Files/Java/jdk-17
    ```



    

