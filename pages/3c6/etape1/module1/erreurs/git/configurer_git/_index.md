---
title: "Atelier1.1: configurer Git"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt>}}
* votre nom Git doit contenir votre nom et le nom de l'ordi
* cet objectif est évalué aux étapes F et H
{{</excerpt>}}

1. En GitBash, mémoriser votre nom et le nom de l'ordi

    ```bash
    $ export NOM="Mathieu Bergeron ($(hostname))"
    ```

    * **remplacer** `Mathieu Bergeron` par votre nom

1. Vérifier la valeur de la variable `NOM`

    ```bash
    $ echo $NOM

    Mathieu Bergeron (marcheur)
    ```

    * doit afficher **votre nom** et le nom de l'ordi

1. Configurer Git avec votre nom

    ```bash
    $ git config --global user.name "$NOM"
    ```

1. Vérifier votre nom Git

    ```bash
    $ git config --list | grep user.name

        user.name=Mathieu Bergeron (marcheur)
    ```

    * doit afficher **votre nom** et le nom de l'ordi
    * (rappel: cet objectif est évalué aux étapes F et H)

1. Configurer Git avec votre courriel

    ```bash
    $ git config --global user.email "USAGER@SERVEUR.CA"
    ```

1. *(optionnel)* &nbsp; Spécifier `notepad` comme éditeur pour les commentaires de commit

    ```bash
    $ git config --global core.editor "notepad.exe"
    ```

1. *(optionnel)* &nbsp; Utiliser une fusion "simple" lors d'un pull

    ```bash
    $ git config --global pull.rebase false
    ```

1. Vérifier les configurations

    ```bash
    $ git config --list
    ```

1. NOTE: ces configurations se trouvent dans le fichier `~/.gitconfig`

    ```bash
    $ cat ~/.gitconfig
    ```
