---
title: "En cas d'erreur avec Git"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Erreurs sur `git commit`

<table>

<tr>
<th>
Erreur
</th>
<td>

```
Votre nom et votre adresse courriel ont été configurés automatiquement [...]
Vous pouvez supprimer ce message en les paramétrant explicitement :
```
</td>

</tr>

<tr>
<th>
Autre version de l'erreur
</th>
<td>

```
Identité d'auteur inconnue

*** Veuillez me dire qui vous êtes.
```
</td>

</tr>


<tr>

<th>
Solution
</th>

<td>

* En GitBash, insérer vos infos dans les commandes ci-bas


    ```bash
    $ git config --global user.name "MON NOM"
    $ git config --global user.email "MON@COURRIEL.CA"
    ```
</td>
</tr>


</table>




## Erreurs sur `git pull` (erreur de configuration)

<table>

<tr>
<th>
Erreur
</th>
<td>

```
astuce: Vous avez des branches divergentes et vous devez spécifier comment
astuce: les réconcilier. [...]
```

</td>

</tr>

<tr>

<th>
Solution
</th>

<td>

* Faire la commande suivante pour choisir le mode de fusion par défaut:


    ```bash
    $ git config --global pull.rebase false
    ```
</td>

</tr>
</table>

## Erreurs sur `git pull` (erreurs de fusion)

<table>

<tr>
<th>
Erreur
</th>
<td>

```
CONFLIT (...) : Conflit de fusion dans ...
```

</td>

</tr>

<tr>

<th>
Solution
</th>

<td>

* Faire `git status` pour voir la liste de tous les conflits

* Ouvrir chaque fichier avec conflit, p.ex.

    ```
    <<<<<<< HEAD
    public void nouveauNomDeMethode() {
    =======
    public void uneMethode() {
    >>>>>>> e112f19282c49f232f02e60e665e2ff2eae57656
    ```

    * choisir la bonne version (soit en haut de `=====` ou en bas)

* Une fois les conflits résolus, ré-ajouter les fichiers et faire un commit

    ```bash
    $ git add .
    $ git commit -a -m"conflits résolus"
    $ git push
    ```

</td>

</tr>

<tr>

<th>
Autre solution
</th>

<td>

* Quand on est sûr de la bonne version, on peut

    * choisir la version locale pour tous les fichiers

        ```bash
        $ git checkout . --ours  ## choisir notre version
        ```

    * choisir la version du serveur pour tous les fichiers

        ```bash
        $ git checkout . --theirs ## choisir leur version
        ```
</td>

</tr>
</table>

<br>
<br>


<table>
<tr>
<th>
Erreur
</th>
<td>

```
error: Les fichiers suivants non suivis de la copie de travail seraient 
       effacés par la fusion :
```
</td>

</tr>

<tr>

<th>
Solution
</th>

<td>

* Ajouter les fichiers non-suivis et faire un commit

    ```bash
    $ git add .
    $ git commit -a -m"ajout avant merge"
    $ git push
    ```
* Refaire un `$ git pull` pour provoquer une nouvelle fusion
    * (au besoin, régler les conflits)

</td>
</tr>



</table>

<br>
<br>

## Erreurs sur `git push`

<table>
<tr>
<th>
Erreur
</th>
<td>

```
! [rejected]        main -> main (fetch first)
error: impossible de pousser des références vers ...
```
</td>

</tr>

<tr>

<th>
Solution
</th>

<td>

* Le dépôt local n'a pas la toute dernière version

* Faire un `git pull` d'abord pour avoir la dernière version
    * au besoin, régler les erreurs du `git pull`

* Refaire le `git push`

</td>
</tr>


</table>

## Coincé avec `vim`

<table>

<tr>
<th>
Erreur
</th>
<td>

Je suis coincé dans `vim` et j'arrive pas à faire mon message de commit

</td>

</tr>

<tr>

<th>
Solution
</th>

<td>

* Faire {{% key "Shift+Z" %}} deux fois pour quitter `vim`

* Faire la commande suivante pour utiliser plutôt `nodepad` comme éditeur


    ```bash
    $ git config --global core.editor "notepad.exe"
   ```

* Refaire le commit et écrire votre commentaire en haut du fichier

    <img src="notepad.png"/>


* En Notepad, sauvegarder et quitter 


</td>
</tr>


</table>



## Erreur réseau ou erreur d'accès

<table>
<tr>
<th>
Erreur
</th>
<td>

```
fatal: Impossible de lire le dépôt distant.
```
</td>

</tr>

<tr>

<th>
Solution
</th>

<td>

* Vérifier que vous avez accès à Internet

* Vérifier que vous être membre du projet sur GitLab

* Vérifier l'URL du dépôt dans le fichier `.git/config`

    ```
    [remote "origin"]
	    url = https://gitlab.com/USAGER/PROJET
    ```

</td>
</tr>


</table>

