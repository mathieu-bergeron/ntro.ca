---
title: "Erreurs avec Eclipse"
weight: 2
# bookFlatSection: false
# bookToc: true
bookHidden: true
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

{{<excerpt class="note">}}
Ne pas effectuer cette étape sur un Windows du Collège
{{</excerpt>}}

<!--
<center>
<video width="50%" src="configurer_eclipse.mp4" type="video/mp4" controls playsinline>
</center>
-->


## Projets fermés ou erreurs avec les `import`s

1. En VSCode, supprimer les projets 

1. Recréer les fichiers `.project` et `.classpath` avec les commandes suivantes:

    ```bash
    $ sh gradlew cleanEclipse --continue
    $ sh gradlew eclipse --continue
    ```

1. Ré-importer les projets dans Eclipse

## Erreur de type *JRE* ou *JDK* non-trouvé

1. S'assurer qu'Eclipse utilise le JDK installé
	* *Window* => *Preferences*
	* *Java* => *Installed JREs*

		<center>
			<img width="80%" src="eclipse_jre.png">
		</center>

	* Idéallement, Eclipse utiliser **uniquement** le JDK installé
	    * recommandé: faire *Remove* sur les autres JDK

	* Si le JDK installé n'est pas là, il faut l'ajouter:
		* *Add* =>
		* *Standard VM* => *Next*
		* Sélectionner le répertoire racine du *JDK*, p.ex: 
			* `C:\Program Files\Java\jdk-17.XXX`
		* *Finish*
		* *Apply and Close*

	* S'assurer que le JDK installlé est le défaut (en gras)
    


