---
title: "Correctif 01: mise à jour aux fichiers de départ"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
* **obligatoire** pour groupe 1 et 2
{{</excerpt>}}

1. Télécharger {{% download "3c6_maj01.zip" "3c6_maj01.zip" %}} 

1. Placer le fichier `3c6_maj01.zip` à la racine de mon dépôt Git

1. Ouvrir GitBash à la racine de mon dépôt Git
    * au besoin, glisser le chemin dans GitBash à partir du navigateur Windows

1. Extraire l'archive sur place avec l'option `-o` (remplacer les fichiers existants)

    ```bash
    $ unzip -o 3c6_maj01.zip
    ```

1. **ATTENTION**: il faut extraire l'archive sur place

    * votre dépôt Git ne **doit pas** contenir un répertoire `3c6_maj01` que Windows aurait pu ajouter

1. Ajouter les nouveaux fichiers de dépar dans Git et les pousser sur le serveur

    ```bash
    $ git add .
    $ git commit -a -m"maj01"
    $ git push
    ```
1. Rafraîchir les projets Eclipse:

    * En VSCode, supprimer les projets
    * En GitBash, utiliser Gralde pour recréer les fichiers `.project` et `.classpath` qui définissent les projets Eclipse

        ```bash
        $ sh gradlew cleanEclipse --continue
        $ sh gradlew eclipse --continue
        ```
    * En VSCode, ré-importer les projets dans Eclipse

1. Vérifier sur GitLab que les nouveaux fichiers sont présent

    * en particulier `libs` 

    <img class="small-figure" src="libs01.png"/>

    <img class="small-figure" src="libs02.png"/>

    * et le nouveau fichier `.gitignore`

    <img class="small-figure" src="gitignore.png"/>


1. Supprimer le fichier `.zip`, on en a plus besoin

    ```bash
    $ rm 3c6_maj01.zip
    ```





