---
title: "Installation"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Ouvrir le Workspace VSCode pour la première fois

1. En VSCode
    * File => Open Workspace from File => naviguer jusqu'à `3c6.code-workspace`
    * Valider qu'on fait confiance au projet => Yes, I trush the authors
        <center>
        <img  src="vscode_trust.png"/>
        </center>

1. NOTE: on peut aussi ouvrir à partir de GitBash

    ```bash
    $ code 3c6.code-workspace
    ```

    * (il faut quand même valider)


## Installer les extensions Java

1. VSCode va vous suggérer d'installer les extensions Java

    <center>
    <img  src="vscode_extension_java01.png"/>
    </center>


1. Une fois l'extension Java installée, attendre la création des projets Java

    <center>
    <img  src="vscode_attendre_chargement_java01.png"/>
    </center>

    <center>
    <img  src="vscode_attendre_chargement_java02.png"/>
    </center>

1. Au besoin, indiquer à VSCode que Java est déjà installé

    * Cliquer sur *Mark done*

    <center>
    <img  src="jdk_mark_done.png"/>
    </center>

1. Au besoin, forcer VSCode à installer les extensions Java en créer un fichier `Bidon.java`

1. En VSCode
    * Ouvrir `3C6 (WORKSPACE)`
        * Ouvrir `atelier1_1`
            * Ouvrir `src\main\java\atelier1_1`
            * Clique-droit sur `\src\main\java\atelier1_1`
                * => New File
                * Saisir `Bidon.java`

1. Ouvrir `Bidon.java` et installer les extensions Java



## Créer une première classe Java

1. Au besoin, supprimer le fichier vide `Bidon.java`

1. S'assurer d'afficher les **projets Java** (pas simplement les fichiers)

    <center>
    <img width="50%" src="vscode_projets_java.png"/>
    </center>

1. On peut maintenant créer des classes 
    * Supprimer `MonTriLibre.java`
    * Cliquer sur `+` et créer la classe `MonTriLibre`

    <center>
    <img src="vscode_creer_classe01.png"/>
    </center>

    <center>
    <img  src="vscode_creer_classe02.png"/>
    </center>

    <center>
    <img  src="vscode_creer_classe03.png"/>
    </center>

## Exécuter le projet

1. Ajouter la méthode `main` à la classe `MonTriLibre`

1. Exécuter le projet

    <center>
    <img src="vscode_executer.png"/>
    </center>

1. Au besoin, autoriser l'accès via le pare-feu Windows

    <center>
    <img src="pare_feu_windows01.png"/>
    </center>




