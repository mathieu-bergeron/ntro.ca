---
title: "Atelier1.1: créer le projets Eclipse"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

1. Ouvrir un GitBash à la racine de mon dépôt Git
    * au besoin, glisser le chemin à partir de l'explorateur Windows
        
1. En Git Bash, faire la commande suivante

    ```bash
    $ sh gradlew eclipse --continue
    ```

    * la commande va prendre un certain temps à:
        * télécharger Gradle
        * télécharger les dépendances des projets
        * compiler les ateliers pour la première fois
        * créer les projets Eclipse pour chaque atelier

1. Attendre que la commande termine

1. Vérifier que les projets Eclipse ont été créés

    * Ouvrir un répertoire de projet, p.ex. `atelier2_1_A`

        <img src="verifier.png"/>

    * Un projet Eclipse est sauvegardé dans
        * les fichiers `.project` et `.classpath` 
        * le répertoire `.settings`
