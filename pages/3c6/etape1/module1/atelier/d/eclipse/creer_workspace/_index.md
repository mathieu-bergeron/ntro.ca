---
title: "Atelier1.1: chosir ou créer un Workspace"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

* Démarrer Eclipse

    * (si Eclipse est déjà ouvert, faire File => Workspace => Other)

    * Choisir ou créer un *Workspace* sur le `C:\`

        <img src="workspace.png"/>

{{<excerpt class="max-width-75">}}

<center>

**IMPORTANT**<br>ne **pas** ajouter de fichiers dans le *Workspace*
<br> 
(laisser Eclipse gérer ce répertoire)

</center>

{{</excerpt>}}

* Prendre en note le chemin du Workspace pour pouvoir le réouvrir la prochaine fois


