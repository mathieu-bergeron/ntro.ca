---
title: "Atelier1.1: importer les projets dans Eclipse"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

1. Ouvrir Eclipse et importer tous les projets du dépôt Git

    * *File* => *Import* => *Existing Projects into Workspace*

        <img class="small-figure" src="./existing_projects.png"/>

    * Cliquer sur *Browse* et naviguer jusqu'à la racine de mon dépôt Git

        <img class="small-figure" src="./naviguer.png"/>

        * Cliquer sur *Sélectionner un dossier*

    * Vérifier que tous les projets apparaissent dans la case *Projects*

        <img class="figure" src="./verifier.png"/>

    * Cliquer sur *Finish*

1. Vérifier que tous les projets apparaissent dans Eclipse

    <img class="figure" src="./verifier02.png"/>
