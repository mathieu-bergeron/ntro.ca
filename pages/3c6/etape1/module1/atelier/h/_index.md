---
title: "Atelier1.1.H: tester la synchronisation via Git (suite)"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

1. Ouvrir un GitBash à racine du dépôt Git

1. Aller chercher les derniers commit avec un pull

    ```bash
    $ git pull
    ```

    * NOTE: comme on vient juste de cloner, il ne devrait pas y avoir de nouveaux commits

1. Vérifier que le fichier `premiere_capture.png` est là

    ```bash
    $ ls | grep -i png
        premiere_capture.png
    ```

1. Prendre une nouvelle capture d'écran qui montre le nom de la nouvelle machine
   et la première capture d'écran

    <img src="deuxieme_capture.png"/>

1. Renommer cette capture `deuxieme_capture.png` et l'ajouter à votre dépôt Git

1. Remettre les deux captures d'écran sur Moodle
