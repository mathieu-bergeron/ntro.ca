---
title: 'Atelier 1.1: installation + synchronisation Git'
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
L'ordre des étapes est important: JDK, VSCode, puis Git

- permet à VSCode de détecter que Java est installé et à Git d'utiliser VSCode comme éditeur par défaut
  {{</excerpt>}}

## {{% link "/3c6/etape1/module1/atelier/a" "Étape A" %}}: créer le dépôt Git pour le cours 


## {{% link "/3c6/etape1/module1/atelier/b" "Étape B" %}}: installer et configurer l'environnement de développement

- **NOTE**: ne pas faire cette étape sur un Windows du Collège

## {{% link "/3c6/etape1/module1/atelier/c" "Étape C" %}}: ajouter les fichiers de départ à mon dépôt Git

## {{% link "/3c6/etape1/module1/atelier/d" "Étape D" %}}: ouvrir les projets en VSCode

- alternative: {{% link "/3c6/etape1/module1/atelier/d/eclipse" "ouvrir les projets en Eclipse" %}}

{{<excerpt class="note">}}
le cours fait la transition Eclipse => VSCode cette année
{{</excerpt>}}

## {{% link "/3c6/etape1/module1/atelier/e" "Étape E" %}}: coder le projet `atelier1_1`

## {{% link "/3c6/etape1/module1/atelier/f" "Étape F" %}}: tester la synchronisation via Git

## {{% link "/3c6/etape1/module1/atelier/g" "Étape G" %}}: travailler sur un nouvel ordi

- **IMPORTANT**: le répertoire `C:\Users\Mon Usager` n'est **pas** sauvegardé automatiquement
  - utiliser Git pour synchroniser vos fichiers d'un ordinateur à l'autre
  - ne **pas** utiliser OneDrive pour synchroniser un dépôt Git
  - ne **pas** travailler à partir d'une clé USB

## {{% link "/3c6/etape1/module1/atelier/h" "Étape H" %}}: tester la synchronisation via Git (suite)

## Remises

- {{% link "/3c6/etape1/module1/atelier/remise_gitlab" "sur GitLab" %}}
- {{% link "/3c6/etape1/module1/atelier/remise_moodle" "sur Moodle" %}}
  - **IMPORTANT** remettre l'URL de mon dépôt Git sur Moodle
