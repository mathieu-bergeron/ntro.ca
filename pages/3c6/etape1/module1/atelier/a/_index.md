---
title: "Atelier 1.1.A: créer le dépôt Git pour le cours"
weight: 1
bookHidden: true
---


{{% pageTitle %}}



{{<excerpt class="note">}}
* à faire une seule fois
* utiliser le même dépôt pour **toute la session**
{{</excerpt>}}


<br>
<br>

* Groupe de Marc-Olivier Tremblay: suivre plutôt la <a href="./procedure-github-1.pdf" target="_blank">procédure pour GitHub</a>
* Groupes de Mathieu Bergeron, suivre la procédure ci-bas

<br>
<br>

## Préalable) créer un compte GitLab


1. Au besoin, {{% link "./creer_compte_gitlab" "créer un compte GitLab" %}}

## Étape 1) créer le dépôt comme tel

1. Me connecter à <a href="https://gitlab.com" target="_blank"  >GitLab</a>

1. Cliquer sur *Créer un projet*

    <img class="small-figure" src="./new_project.png"/>

1. Choisir *Créer un projet vide*

    <img class="small-figure" src="./blank_project.png"/>

1. Remplir le formulaire

    <img class="figure" src="./formulaire.png"/>

    * le nom de projet doit être `3c6_PRENOM_NOM`
        * remplacer `PRENOM` par mon prénom
        * remplacer `NOM` par mon nom
    * pour le reste, utiliser les options par défaut:
        * doit être coché: *Private*
        * doit être coché: *Initialize repository with a README*

1. Cliquer sur *Create project*

    <img class="small-figure" src="./create_project.png"/>

1. Sur la page du projet, cliquer sur *Clone*

    <img class="small-figure" src="./clone.png"/>

    <!--
    * copier l'URL SSH
        <img class="small-figure" src="./url_ssh.png"/> 
    -->

    * copier l'URL HTTPS

        <img class="small-figure" src="./url_https.png"/>


    * coller cet URL dans un document pour le mémoriser

## Étape 2) partager le dépôt avec le prof

1. Sur la page du projet, cliquer sur *Gestion* => *Membres*

    <img class="small-figure" src="./members.png"/>

1. Cliquer sur *Inviter des membres*

    <img class="figure" src="./invite_members.png"/>

1. Saisir et sélectionner `mathieu-bergeron`

    <img class="small-figure" src="./mathieu-bergeron.png"/>

1. Sous *Select a role*, sélectionner *Developer* 

    <img class="small-figure" src="./developer.png"/>

1. Cliquer sur *Inviter*

    <img class="small-figure" src="./invite.png"/>
