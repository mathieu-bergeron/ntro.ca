---
title: "Procédure: s'inscrire à GitLab"
weight: 2
# bookFlatSection: false
# bookToc: true
bookHidden: true
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

1. Visiter [https://gitlab.com/users/sign_in](https://gitlab.com/users/sign_in)

    * Cliquer sur *Inscrivez-vous maintenant*

    <img class="figure" src="register.png"/>


1. Remplir le formulaire

    <img class="small-figure" src="formulaire.png"/>

    * Cliquer sur *Je ne suis pas un robot*

    * Cliquer sur *Register*

1. Entrer le code reçu par courriel

    <img class="small-figure" src="code_courriel.png"/>

1. Remplir le formulaire en cochant *Rejoindre un projet*

    <img class="small-figure" src="formulaire2.png"/>

1. Cliquer sur Continuer



