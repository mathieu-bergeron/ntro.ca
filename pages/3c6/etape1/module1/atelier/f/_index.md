---
title: "Atelier1.1.F: tester la synchronisation via Git"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

1. Sur l'ordinateur courant, ouvrir un GitBash

1. Prendre une capture d'écran de GitBash **où le nom du poste est visible**

    <img src="premiere_capture.png"/>

1. Renommer la capture `premiere_capture.png`

1. Déplacer `premiere_capture.png` à la racine du dépôt Git

1. Ajouter la capture à mon dépôt Git et pousser vers le serveur

    ```bash
    $ git add .
    $ git commit -a -m"atelier1_1) première capture d'écran"
    $ git push
    ```

1. Se déconnecter de cet ordinateur (pour laisser la place à quelqu'un d'autre!)

1. Se connecter sur un nouvel ordinateur pour faire la prochaine étape ({{% link "../g" "Étape G" %}})

