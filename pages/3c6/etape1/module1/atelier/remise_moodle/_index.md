---
weight: 1
bookHidden: true
---

{{%
    3c6_remise_moodle
    atelier="1.1"
    url_mb="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=322634"
    url_mot="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=410332"
%}}

1. Remettre l'URL de mon dépôt Git

   - Groupes de <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/questionnaire/view.php?id=376451">Mathieu Bergeron</a>
   - Groupe de <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/questionnaire/view.php?id=410331">Marc-Olivier Tremblay</a>

1. Ajouter les captures d'écran des étapes F et H à la remise sur Moodle
   - Groupes de <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=322634">Mathieu Bergeron</a>
   - Groupe de <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=410332">Marc-Olivier Tremblay</a>
