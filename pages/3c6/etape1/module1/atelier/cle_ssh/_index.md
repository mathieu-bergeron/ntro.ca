---
title: "Atelier1.1: ajouter ma clé SSH sur GitLab"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
répéter cette étape sur chaque nouvelle machine où je veux travailler
{{</excerpt>}}

1. En GitBash, générer la clé SSH

    ```bash
    $ ssh-keygen
    ```

    * Appuyer sur {{% key "Entrée" %}} plusieurs fois pour utiliser les options par défaut

1. Ouvrir la clé publique SSH avec `notepad.exe`

    ```bash
    $ notepad ~/.ssh/id_rsa.pub
    ```

    * copier cette clé

    <img class="small-figure" src="./copier_cle.png"/>

1. Ouvrir mes préférences GitLab

    <img class="figure" src="./preferences_gitlab.png"/>

1. Cliquer sur *SSH Keys*

    <img class="small-figure" src="./ssh_keys.png"/>

1. Coller votre clé dans le champ *Key*

    <img class="figure" src="./add_key.png"/>

    * Cliquer sur *Add key*


