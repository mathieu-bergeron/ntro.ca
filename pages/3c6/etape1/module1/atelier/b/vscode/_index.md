---
title: "Installation"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Installer VSCode

1. https://code.visualstudio.com/Download
    * exécuter le fichier `VSCodeUserSetup-x64-XXX.exe`
    * accepter les termes et cliquer sur *Suivant*
    * utiliser les options par défaut

