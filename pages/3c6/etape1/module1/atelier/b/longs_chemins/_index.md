---
title: "Activer les longs chemins"
weight: 2
# bookFlatSection: false
# bookToc: true
bookHidden: true
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

{{<excerpt class="note">}}
Ne pas effectuer cette étape sur un Windows du Collège
{{</excerpt>}}

<br>
<br>

1. Ouvrir `reged` (éditeur de registre) en tant qu'Administrateur
1. Naviguer jusqu'à `Ordinateur\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\FileSystem`
1. Ouvrir `LongPathsEnabled`
1. Changer la valeur de 0 à 1

    <img src="longs_chemins.png"/>

1. Tester que ça fonctionne
    * ouvrir un CMD
    * faire la commande suivante

        ```bat
        echo blahblah > bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb.txt
        ```

    * la commande devrait fonctionner sans erreurs

    <img src="commande.png"/>
