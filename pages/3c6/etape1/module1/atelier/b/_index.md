---
title: "Atelier 1.1.B: environnement de développement"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
Ne pas effectuer ces étapes sur un Windows du Collège
{{</excerpt>}}

1. Sur mon ordi, installer
    * {{% link "./jdk" "Un JDK" %}} 
        * version minimale: JDK 11
        * version recommandée: JDK 17 LTS
        * testé jusqu'à: JDK 20
    * {{% link "./vscode" "VSCode" %}}
        * alternative: {{% link "./eclipse" "Eclipse" %}}
    * {{% link "./git" "Git" %}}

1. Pour Windows: 
    * {{% link "./longs_chemins" "Activer les longs chemins" %}} 

