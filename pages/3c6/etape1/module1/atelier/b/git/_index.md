---
title: "Procédure: installer Git"
weight: 2
# bookFlatSection: false
# bookToc: true
bookHidden: true
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

{{<excerpt class="note">}}
Ne pas effectuer cette étape sur un Windows du Collège
{{</excerpt>}}

<br>
<br>

1. Visiter <a href="https://git-scm.com/downloads" target="_blank">https://git-scm.com/downloads</a>

1. Télécharger la version pour votre OS, p.ex.
    * Cliquer sur *Download for Windows*
    * Télécharger la version *64-bit Git for Windows Setup*
    * Exécuter le fichier, p.ex. `Git-XXX-64.exe`

* Installer avec les options par défaut, **sauf**:
	* **Recommandé**: choisir `Visual Studio Code` comme éditeur plutôt que `vim`:
        <center>
        <img width="50%" src="git_choisir_vscode.png"/>
        </center>



