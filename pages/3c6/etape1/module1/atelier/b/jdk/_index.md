---
title: "Procédure: installer un JDK"
weight: 2
# bookFlatSection: false
# bookToc: true
bookHidden: true
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

{{<excerpt class="note">}}
Ne pas effectuer cette étape sur un Windows du Collège
{{</excerpt>}}

<br>
<br>

1. *Recommandé*&nbsp;: désinstaller les JDK autre que 17

1. Visiter <a href="https://www.oracle.com/ca-en/java/technologies/downloads/" target="_blank">https://www.oracle.com/ca-en/java/technologies/downloads/</a>

1. Sélectionner le JDK 17
    * (fonctionne aussi avec JDK 20 et le <a href="https://jdk.java.net/20/">OpenJdk</a>)

1. Télécharger la version pour votre OS, p.ex.
    * Sélectionner *Windows*
    * Choisir le *x64 Installer*: `https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.exe`
    * Exécuter le fichier `jdk-17_windows-x64_bin.exe`
    * Installer avec les options par défaut
