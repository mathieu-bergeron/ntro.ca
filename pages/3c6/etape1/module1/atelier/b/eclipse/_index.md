---
title: "Procédure: installer Eclipse"
weight: 2
# bookFlatSection: false
# bookToc: true
bookHidden: true
# bookCollapseSection: false
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

{{<excerpt class="note">}}
Ne pas effectuer cette étape sur un Windows du Collège
{{</excerpt>}}


<br>
<br>

1. *Recommandé*&nbsp;: désinstaller toute ancienne version d'Eclipse

1. Installer Eclipse (version *2023-06* ou plus récent)
	* Visiter <a href="https://www.eclipse.org/downloads/">https://www.eclipse.org/downloads/</a>
	* Télécharger en cliquant sur <i>Download x86_64</i>
	* Exécuter `eclipse-inst-jre-win64.exe`
	* Installer *Eclipse IDE for Java Developers* avec les options par défaut

