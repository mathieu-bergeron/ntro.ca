---
title: "Atelier1.1: cloner mon dépôt Git sur ma machine"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

1. Ouvrir le répertoire où placer mon dépôt Git

    * **reommandé**: `C:\Users\Mon Usager` 


    <img class="figure" src="./choisir_repertoire.png"/>

    * **IMPORTANT**: ne pas placer le dépôt Git dans un répertoire
        * OneDrive
        * Eclipse
        * ni tout autre répertoire gérer par un logiciel

1. Ouvrir un GitBash 

    * saisir `cd␣` (`cd`+ {{% key Espace %}})

1. Glisser le chemin du répertoire choisi dans GitBash

    <img  src="./glisser_chemin.png"/>

    <img src="./resultat_glisser_chemin.png"/>

1. En GitBash, faire {{% key "Entrée" %}}

1. Vérifier le répertoire courant de GitBash

    ```bash
    $ pwd
        /c/Users/Mathieu Bergeron/
    ```

    * où le chemin doit être celui de **votre répertoire**

1. Avec GitBash ouvert et dans le bon répertoire courant, cloner votre dépôt

    ```bash
    $ git clone https://gitlab.com/NOM_USAGER/3c6_PRENOM_NOM
    ```

    * où l'URL est celui de **votre dépôt GitLab**

    * **TRUC** copier-coller l'URL HTTPS à partir de la page GitLab de votre dépôt

1. Faire `ls` pour vérifier la présence du dépôt Git

    ```bash
    $ ls -d 3c6*
        3c6_mathieu_bergeron
    ```

    * où le répertoire doit être **votre dépôt**

    <img src="verifier01.png"/>

1. Dans l'explorateur Windows, ouvrir votre dépôt

    * vérifier la présence du répertoire `.git`

        <img src="verifier02.png"/>

        * (au besoin activer l'affichage des répertoires cachés)

    * `.git` est la base de données locale de Git pour ce dépôt

    * ça nous confirme que le répertoire est bien un dépôt Git



