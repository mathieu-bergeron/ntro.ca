---
title: "Atelier1.1: ajouter les fichiers de départ"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
* à faire une seule fois
* utiliser le même dépôt pour **toute la session**
{{</excerpt>}}

1. Télécharger {{% download "3c6_depart.zip" "3c6_depart.zip" %}} 

1. Placer le fichier `3c6_depart.zip` à la racine de mon dépôt Git

1. Ouvrir GitBash à la racine de mon dépôt Git
    * au besoin, glisser le chemin dans GitBash à partir du navigateur Windows

1. Extraire l'archive sur place

    ```bash
    $ unzip 3c6_depart.zip
    ```

1. Le dépôt Git doit avoir l'air de ceci

    <img src="fichiers.png"/>

    * ne **doit pas** contenir un répertoire `3c6_depart` que Windows aurait pu ajouter

1. Ajouter les fichiers dans Git

    ```bash
    $ git add .
    ```

1. Faire un commit avec les nouveaux fichiers

    ```bash
    $ git commit -a -m"premier commit"
    ```

1. Pousser le commit vers le serveur

    ```bash
    $ git push
    ```

1. Vérifier sur GitLab que les fichiers sont bien sur le serveur

    <img src="gitlab.png"/>

    * NOTE: le fichier `.zip` n'est pas ajouté grace au fichier `.gitignore`

1. Supprimer le fichier `.zip`, on en a plus besoin

    ```bash
    $ rm 3c6_depart.zip
    ```





