---
title: 'Atelier 1.1.C: ajouter les fichiers de départ'
weight: 1
bookHidden: true
---

{{% pageTitle %}}

1. Cloner mon dépôt Git

    * {{% link "./cloner_depot" "Groupes de Mathieu Bergeron" %}}
    * <a href="./cloner_depot/procedure-github-2.pdf" target="_blank">Marc-Olivier Tremblay (groupe 2)</a>

1. _optionnel:_ {{% link "./configurer_cd_automatique" "Configurer GitBash pour ouvrir automatiquement à la racine de mon dépôt Git" %}}

1. {{% link "./ajouter_fichiers" "Télécharger les fichiers de départ et les ajouter à mon dépôt Git" %}}
