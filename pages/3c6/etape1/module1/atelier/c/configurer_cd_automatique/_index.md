---
title: "Atelier1.1: configurer un `cd` automatique"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


1. Navigurer vers mon dépôt Git

    <img src="naviguer.png" />

1. Ouvrir un GitBash

    * saisir `cd␣` (`cd`+ {{% key Espace %}})

1. Glisser l'emplacement dans GitBash

    <img src="glisser01.png" />

    <img src="glisser02.png" />

1. En GitBash, faire {{% key "Entrée" %}}

1. Vérifier le répertoire courant de GitBash

    ```bash
    $ pwd
        /c/Users/Mathieu Bergeron/3c6_mathieu_bergeron
    ```

    * où le chemin doit être celui de mon dépôt Git

1. En GitBash, exécuter la commande suivante

    ```bash
    $ echo "cd '$(pwd)'"
    ```

    * **ATTENTION**  à l'emplacement des `"` et des `'`

    * Le résultat doit être

        ```bash
        cd '/c/Users/Mathieu Bergeron/3c6_mathieu_bergeron'
        ```

1. Copier le résultat de la commande ci-haut dans le fichier `C:\Users\Mon Usager\.bash_profile`

    * En GitBash:

        ```bash
        $ echo "cd '$(pwd)'" >> ~/.bash_profile
        ```

1. Ouvrir un nouveau GitBash et vérifier le répertoire courant

    ```bash
    $ pwd   
        /c/Users/Mathieu Bergeron/3c6_mathieu_bergeron
    ```

