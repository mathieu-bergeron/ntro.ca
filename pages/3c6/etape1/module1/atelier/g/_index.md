---
title: "Atelier 1.1.G: travailler sur un nouvel ordi"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
* cette étape est **obligatoire** pour l'atelier 1.1
* par la suite, refaire cette étape au besoin
{{</excerpt>}}



## Vérifier l'ordi

1. Vérifier la version de Java

    ```bash
    $ java --version

        # doit afficher une version supportée (au moins 11 et jusqu'à 20)
    ```

1. Démarrer GitBash et VSCode pour vérifier que ça fonctionne

1. Au besoin, installer les outils manquant via {{% link "../b" "l'étape B" %}}

## Créer ou ouvrir les projets Java

1. Selon le cas

    * **Si** mon dépôt Git est déjà sur cet ordi

        * {{% link "../../erreurs/git" "Faire un `git pull` et régler les erreurs au besoin" %}}

    * **Sinon:** 

        * {{% link "../c/cloner_depot" "Cloner mon dépôt Git" %}}

1. Ensuite

    * {{% link "../d/" "Ouvrir les projets en VSCode" %}}
