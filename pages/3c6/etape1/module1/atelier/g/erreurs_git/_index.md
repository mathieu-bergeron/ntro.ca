---
title: "Atelier 1.1: réparer une erreur de Git"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Erreurs sur `git pull` (erreurs de fusion)

<table>

<tr>
<th>
Erreur
</th>
<td>

```
CONFLIT (...) : Conflit de fusion dans ...
```

</td>

</tr>

<tr>

<th>
Solution
</th>

<td>

* Faire `git status` pour voir la liste de tous les conflits

* Ouvrir chaque fichier avec conflit, p.ex.

    ```
    <<<<<<< HEAD
    public void nouveauNomDeMethode() {
    =======
    public void uneMethode() {
    >>>>>>> e112f19282c49f232f02e60e665e2ff2eae57656
    ```

    * choisir la bonne version (soit en haut de `=====` ou en bas)

* Une fois les conflits résolus, ré-ajouter les fichiers et faire un commit

    ```bash
    $ git add .
    $ git commit -a -m"conflits résolus"
    $ git push
    ```

</td>

</tr>

<tr>

<th>
Autre solution
</th>

<td>

* Quand on est sûr de la bonne version, on peut

    * choisir la version locale pour tous les fichiers

        ```bash
        $ git checkout . --ours  ## choisir notre version
        ```

    * choisir la version du serveur pour tous les fichiers

        ```bash
        $ git checkout . --theirs ## choisir leur version
        ```
</td>

</tr>
</table>

<br>
<br>


<table>
<tr>
<th>
Erreur
</th>
<td>

```
error: Les fichiers suivants non suivis de la copie de travail seraient 
       effacés par la fusion :
```
</td>

</tr>

<tr>

<th>
Solution
</th>

<td>

* Ajouter les fichiers non-suivis et faire un commit

    ```bash
    $ git add .
    $ git commit -a -m"ajout avant merge"
    $ git push
    ```
* Refaire un `$ git pull` pour provoquer une nouvelle fusion
    * (au besoin, régler les conflits)

</td>
</tr>



</table>

<br>
<br>



## Erreurs sur `git push`

<table>
<tr>
<th>
Erreur
</th>
<td>

```
! [rejected]        main -> main (fetch first)
error: impossible de pousser des références vers ...
```
</td>

</tr>

<tr>

<th>
Solution
</th>

<td>

* Le dépôt local n'a pas la toute dernière version

* Faire un `git pull` d'abord pour avoir la dernière version
    * au besoin, régler les erreurs du `git pull`

* Refaire le `git push`

</td>
</tr>


</table>

## Erreur réseau ou erreur d'accès

<table>
<tr>
<th>
Erreur
</th>
<td>

```
fatal: Impossible de lire le dépôt distant.
```
</td>

</tr>

<tr>

<th>
Solution
</th>

<td>

* Vérifier que vous avez accès à Internet

* Vérifier que vous être membre du projet sur GitLab

* Vérifier l'URL du dépôt dans le fichier `.git/config`

    ```
    [remote "origin"]
	    url = https://gitlab.com/USAGER/PROJET
    ```

</td>
</tr>


</table>

