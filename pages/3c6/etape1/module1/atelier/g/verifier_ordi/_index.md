---
title: "Atelier1.1: vérifier la config d'un ordi"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

* Ouvrir GitBash

* Vérifier le JDK

    ```bash
    $ java --version
        
        # doit afficher une version supporté (au moins 11 et jusqu'à 20)
    
    $ echo $JAVA_HOME

        # doit afficher un chemin valide vers un JDK
    ```

* Vérifier Git
    ```
    $ git config --get user.name

        # doit afficher votre nom et le nom de l'ordi

    $ git config --get user.email

        # doit afficher votre courriel
    ```

* Vérifier qu'Eclipse démarre

