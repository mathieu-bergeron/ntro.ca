---
title: "Atelier1.1: chosir et vérifier mon Workspace"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

* Démarrer Eclipse

    * (si Eclipse est déjà ouvert, faire File => Workspace => Other)

    * Faire *Browse* pour choisir mon *Workspace* sur le `C:\`

        <img src="workspace.png"/>


    * Cliquer sur *Launch*

* Vérifier que la liste de projet dans Eclipse

    <img src="verifier02.png"/>

* Si un projet est fermé <img src="icone_projet_ferme.png"/>, vérifier que son chemin est correct:

    <img src="projet_ferme.png"/>

    * Clique-droit sur le projet => Properties

    * Vérifier l'emplacement du projet

    <img src="chemin_projet.png"/>

* Au besoin, rafraîchir les projets Eclipse

    * en GitBash, recréer les projets Eclipse

        ```bash
        $ sh gradlew cleanEclipse --continue
        $ sh gradlew eclipse --continue
        ```

    * en Eclipse, tous les projets Eclipse
    * ré-importer les projets du dépôt Git comme à {{% link "../../d/importer_projets" "l'Étape D" %}}
    

    


