---
title: "Installation"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

NOTE: l'ordre d'installation est important, p.ex. installer le JDK avant VSCode facilite l'installation des extensions Java de VSCode

## Installer Jdk 17

1. Options par défaut => Close



## Installer VSCode

1. https://code.visualstudio.com/Download
    * *Windows*: exécuter le fichier `VSCodeUserSetup-x64-XXX.exe`
    * accepter les termes et cliquer sur *Suivant*
    * utiliser les options par défaut

## Installer Git

1. Visiter <a href="https://git-scm.com/downloads" target="_blank">https://git-scm.com/downloads</a>

1. Télécharger la version pour votre OS, p.ex.
    * Cliquer sur *Download for Windows*
    * Télécharger la version *64-bit Git for Windows Setup*
    * Exécuter le fichier, p.ex. `Git-2.45.2-64.exe`

* Installer avec les options par défaut, **sauf**:
	* **Recommandé**: choisir `Visual Studio Code` comme éditeur plutôt que `vim`:
        <center>
        <img width="50%" src="git_choisir_vscode.png"/>
        </center>



## Cloner le dépôt Git


## Ajouter les fichiers

1. Dézipper

    ```bash
    $ unzip -o 3c6_depart.zip
    ```


## Ouvrir le Workspace VSCode pour la première fois

1. Via GitBash

    ```bash
    $ code 3c6.code-workspace
    ```

    * Valider qu'on fait confiance au projet => Yes, I trush the authors
        <center>
        <img  src="vscode_trust.png"/>
        </center>

1. Ou en VSCode
    * File => Open Workspace from File => naviguer jusqu'à `3c6.code-workspace`
    * Valider qu'on fait confiance au projet => Yes, I trush the authors

## Créer un premier fichier Java 

1. En VSCode
    * Ouvrir `3C6 (WORKSPACE)`
        * Ouvrir `atelier1_1`
            * Ouvrir `src\main\java\atelier1_1`
            * Clique-droit sur `\src\main\java\atelier1_1`
                * => New File
                * Saisir `MonTriLibre.java`

1. VSCode va vous suggérer d'installer les extensions Java

    <center>
    <img  src="vscode_extension_java01.png"/>
    </center>


1. Une fois l'extension Java installer, attendre la création des projets Java

    <center>
    <img  src="vscode_attendre_chargement_java01.png"/>
    </center>

    <center>
    <img  src="vscode_attendre_chargement_java02.png"/>
    </center>

## Créer une première classe Java

1. Supprimer le fichier vide `MonTriLibre.java`

1. S'assurer d'afficher les projets Java (et non les fichiers)

    <center>
    <img width="50%" src="vscode_projets_java.png"/>
    </center>

1. On peut maintenant créer des classes 
    * Supprimer `MonTriLibre.java`
    * Cliquer sur `+` et créer la classe `MonTriLibre`

    <center>
    <img src="vscode_creer_classe01.png"/>
    </center>

    <center>
    <img  src="vscode_creer_classe02.png"/>
    </center>

    <center>
    <img  src="vscode_creer_classe03.png"/>
    </center>

## Exécuter le projet

1. Au besoin, autoriser l'accès via le pare-feu Windows

    <center>
    <img src="pare_feu_windows01.png"/>
    </center>




