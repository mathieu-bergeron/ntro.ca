---
title: "Atelier1.1.E: coder le projet `atelier1_1`"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Créer la classe `MonTriLibre`

1. En VSCode, sélectionner le **paquet** `atelier1_1`

1. Ajouter la classe suivante au **paquet** `atelier1_1`
    * Nom de la classe: `MonTriLibre`

1. Ouvrir la classe `MonTriLibre` et ajuster la signature

    ```java
    public class MonTriLibre extends TriLibre {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `TriLibre`

        <img class="small-figure" src="./import1.png"/>

1. Le code final devrait être comme suit

    ```java
    package atelier1_1;

    import freesort_procedure.ProcedureTriLibre;

    public class MonTriLibre extends TriLibre {

    }
    ```

## Créer la classe `Procedure`

1. Ajouter la classe suivante au **paquet** `atelier1_1`
    * Nom de la classe: `Procedure`

1. Ouvrir la classe `Procedure` et ajuster la signature

    ```java
    public class Procedure extends ProcedureTriLibre<MonTriLibre> {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `ProcedureTriLibre`

        <img class="small-figure" src="./import2.png"/>

    * ajouter la méthode obligatoire `classeTriLibre`

        <img class="small-figure" src="./methode_obligatoire1.png"/>

1. Ajouter la méthode méthode `main` suivante

    ```java
    public static void main(String[] args) {
        NtroAppFx.launch(args);
    }
    ```

    * utiliser {{% key "Ctrl+1" %}} pour corriger les erreurs de compilation

1. Ajouter le code suivant à la méthode `classeTriLibre`

    ```java
    protected Class<MonTriLibre> classeTriLibre() {
        return MonTriLibre.class;
    }
    ```

1. Exécuter le projet et trier les cartes à l'écran

    {{% animation "/3c6/etape1/module1/atelier/e/tri_libre.mp4" %}}


