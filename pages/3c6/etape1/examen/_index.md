---
title: "Examen 1"
weight: 40
---

# 3C6: Examen 1

* Durée: 2h

* Individuel

* Documentation permise: ntro.ca, Moodle, recherches sur le Web, **votre code**, des cartes à jouer

* L'examen est 100% à choix de réponse sur Moodle

## Sujets

* hiérarchie de classes et concepts POO

* trier des cartes selon l'ordre {{% link "/3c6/approche/trier" "spécifié dans le cours" %}}
    * d'abord selon les sortes ♡ ♢ ♧ ♤
    * ensuite selon le numéro

* procédure sur un tableau d'objets

## 5pts) Volet théorique

* 5 questions de compréhension à choix de réponse

## 5pts) Volet pratique

* 5 questions du type «&nbsp;compléter le code suivant&nbsp;» 
