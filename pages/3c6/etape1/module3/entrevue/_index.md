---
title: "Entrevue 1.3: procédure déplacer au début"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Préparation

* Noter 4 cartes choisies au hasard: <span class="span-cartes" nombre-de-cartes="4"></span> <button id="bouton-cartes">Générer</button>

* En suivant {{% link "../theorie/deplacer_decaler/#visualisation-du-code" "la théorie" %}}:

    * Dessiner `aDeplacer` au dessus de la `3ième` carte 

    * Dessiner un `i` avec la valeur de `-1`

    * Dessiner `memoireA` et `memoireB` comme des cases vides en dessous de `i`


## Faire 3 étapes de la procédure

1. En dessous de votre premier dessin, dessiner la prochaine étape de la {{% link "/3c6/etape1/module3/theorie/deplacer_decaler/#procédure-déplacer-au-début" "procédure expliquée en théorie" %}}, pour l'opération *déplacer au début*

1. Dessiner au moins 3 étapes de plus, en sautant une ligne à chaque étape

## Tableau d'objet à dessiner

1. Dessiner le {{% link "/3c6/etape1/module3/theorie/dessiner_tableau/#exemple-1" "tableau d'objet" %}} qui correspond à l'étape de la procédure où vous êtes rendu, sans oublier les valeurs de `i`, `memoireA`, `memoireB`

<script src="/3c6/cartes.js"></script>

<style>
#span-cartes{
    padding:0.2rem;
}
</style>
