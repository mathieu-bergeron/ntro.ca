---
title: "Mini-test 1.3, question0"
weight: 1
bookHidden: false
---

Selon la visualisation suivante:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question00_596be6103faf5126aaf1/etat.png"/>

La valeur de `aDeplacer` est `{1:MCS:-1~0~1~2~=3~4~5~6~7~8}`

La valeur de `i` est `{1:MCS:-1~0~=1~2~3~4~5~6~7~8}`

La valeur de `memoireA` est `{1:MCS:5♡~=2♧~5♧~3♢~7♤~2♡}`

La valeur de `memoireB` est `{1:MCS:5♡~2♧~=5♧~3♢~7♤~2♡}`

