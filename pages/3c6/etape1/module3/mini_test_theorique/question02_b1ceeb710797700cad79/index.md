---
title: "Mini-test 1.3, question2"
weight: 1
bookHidden: false
---

Considérer la visualisation suivante:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question02_b1ceeb710797700cad79/question02_courant.png"/>

Si on suit la procédure **déplacer à la fin** décrite en théorie, quelle est la prochaine étape?

A:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question02_b1ceeb710797700cad79/question02_a.png"/>

B:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question02_b1ceeb710797700cad79/question02_b.png"/>

C:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question02_b1ceeb710797700cad79/question02_c.png"/>


aucune de ces réponses



<br>
<br>
<br>
<br>
<br>

Réponse: A





