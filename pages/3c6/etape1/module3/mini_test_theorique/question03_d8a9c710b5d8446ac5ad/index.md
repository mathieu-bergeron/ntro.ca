---
title: "Mini-test 1.3, question3"
weight: 1
bookHidden: false
---

La visualisation suivante

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question03_d8a9c710b5d8446ac5ad/visualisation.png"/>

correspond au tableau d'objets suivant

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question03_d8a9c710b5d8446ac5ad/tableau_depart.png"/>

après les opérations ci-bas

```java
cartes[1] = cartes[0]
cartes[0] = null;

cates[3] = cartes[4];

memoireA = memoireB;
memoireB = memoireA;
```


quel est maintenant le tableau d'objets?

A:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question03_d8a9c710b5d8446ac5ad/tableau_a.png"/>

B:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question03_d8a9c710b5d8446ac5ad/tableau_b.png"/>


C:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question03_d8a9c710b5d8446ac5ad/tableau_c.png"/>



Aucune de ces réponses



<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

RÉPONSE: C

