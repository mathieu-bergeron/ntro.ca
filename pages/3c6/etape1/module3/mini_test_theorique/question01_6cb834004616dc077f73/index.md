---
title: "Mini-test 1.3, question1"
weight: 1
bookHidden: false
---

Considérer la visualisation suivante:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question01_6cb834004616dc077f73/question01_courant.png"/>

Si on suit la procédure **déplacer au début** décrite en théorie, quelle est la prochaine étape?

A:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question01_6cb834004616dc077f73/question01_a.png"/>

B:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question01_6cb834004616dc077f73/question01_b.png"/>

C:

<img src="https://ntro.ca/3c6/etape1/module3/mini_test_theorique/question01_6cb834004616dc077f73/question01_c.png"/>


aucune de ces réponses



<br>
<br>
<br>
<br>
<br>

Réponse: c





