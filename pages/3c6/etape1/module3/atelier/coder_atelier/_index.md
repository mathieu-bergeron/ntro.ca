---
title: "Atelier1.3: implanter les procédures *déplacer à la fin/début*"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer la classe `MonTableau`

1. En VSCode, sélectionner le **paquet** `atelier1_3`

1. Ajouter la classe suivante au **paquet** `atelier1_3`
    * Nom de la classe: `MonTableau`

1. Ouvrir la classe `MonTableau` et ajuster la signature

    ```java
    public class MonTableau extends Tableau {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `Tableau`


## Créer la classe `Procedure`

1. Ajouter la classe suivante au **paquet** `atelier1_3`
    * Nom de la classe: `Procedure`

1. Ouvrir la classe `Procedure` et ajuster la signature

    ```java
    public class Procedure extends ProcedureDecaler<MonTableau> {
    ```

1. En utilisant le raccourci {{% key "Ctrl+1" %}}, effectuer les tâches suivantes
    * importer la classe `ProcedureDecaler`

    * ajouter la méthode obligatoire `classeMonTableau`

1. Ajouter le code suivant à la méthode `main`

    ```java
    public static void main(String[] args) {
        NtroAppFx.launch(args);
    }
    ```

    * utiliser {{% key "Ctrl+1" %}} pour corriger les erreurs de compilation

1. Ajouter le code suivant à la méthode `classeMonTableau`

    ```java
    protected Class<MonTableau> classeMonTableau() {
        return MonTableau.class;
    }
    ```

## Coder les procédures dans la classe `MonTableau`

1. Ouvrir `MonTableau`

1. Ajouter la méthode suivante:

    ```java
    @Override
    public void deplacerDecaler() {

        if(insererAuDebut) {

            insererAuDebut();

        }else {

            insererALaFin();
        }
    }
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour ajouter les méthodes privées `insererAuDebut` et `insererALaFin`

1. Coder les méthodes `insererAuDebut` et `insererALaFin` selon les procédures vues en théorie

1. **IMPORTANT** par héritage, `MonTableau` contient déjà les attributs que vous devez utiliser 

    <img class="figure" src="attributs.png" />

    {{<excerpt class="note">}}


* il faut utiliser ces attributs plutôt que des variables
    * p.ex. utiliser l'attribut `i` dans la boucle `for` et non une variable `int i`
* sinon l'outil de validation ne pourra pas visualiser l'exécution de votre code

    {{</excerpt>}}

## Exécuter l'outil de validation

1. L'outil va vous indiquer si votre code est une solution (✔) ou s'il contient une ou des erreurs (✗)

    <img class="small-figure" src="solution_ou_erreur.png"/>

    {{<excerpt class="note">}}

**IMPORTANT** 

* il faut quand même regarder la console pour voir les exceptions de type `IndexOutOfBounds` ou `NullPointerException`
* vous pouvez quand même utiliser le débogueur

    {{</excerpt>}}

## Utiliser l'outil de validation pour visualiser votre code

1. Pour que l'outil puisse afficher chacune des étapes de la procédure, vous devez ajouter à la main des instructions `Execution.ajouterEtape()`, p.ex.

    ```java
    public void insererAuDebut() {

        i = 0;

        Execution.ajouterEtape();

        memoireA = cartes[0];

        Execution.ajouterEtape();

        //...
    }
    ```

    * c'est à vous de décider ce qui constitue une étape. 
    * cela n'influence pas la validation, mais vous permet de visualiser ce que votre code fait et pourrait vous aider à trouver des erreurs

## Utiliser l'outil de validation pour visualiser le tableau d'objet

1. Quand vous quitter (fermer) l'outil de validation, ce dernier va générer une visualisation du deriner tableau d'objets affiché

1. Par exemple

    * Quitter à l'étape 7 / 11 ci-bas:

        <img class="figure" src="etape7.png"/>

    * Va générer une visualisation du tableau d'objet de l'étape 7 / 11:

        <img class="figure" src="graphe_etape7.png"/>

        ```bash
        [...]
        [INFO] Writing JSON files
        [INFO] Generating graphs
        [INFO] Exiting
        ```

    * Cette image ce trouve ici:

        `_storage/graphs/MonTableau-05-solution.png`

        * TRUC: on peut l'ouvrir à partir de GitBash avec

            ```bash
            $ start _storage/graphs/MonTableau.png`     # en windows

            $ xdg-open _storage/graphs/MonTableau.png`  # en linux
            $ open _storage/graphs/MonTableau.png`      # en macos
            ```

## Question bonus 

1. Coder une autre procédure qui implante l'opération *déplacer au début*

1. Avec l'outil de validation, comparer le nombre d'étapes entre la procédure vue en théorie et la votre

1. Quelle est la différence d'efficacité? Est-ce une grosse différence selon vous?








