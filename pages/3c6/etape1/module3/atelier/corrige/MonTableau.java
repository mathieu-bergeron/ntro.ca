public class MonTableau extends Tableau {

    // ...

    private void insererALaFin() {

        memoireA = cartes[aDeplacer];
        cartes[aDeplacer] = null;
        Execution.ajouterEtape();

        for(i = aDeplacer; i < cartes.length -1; i++) {

            Execution.ajouterEtape();
                            
            cartes[i] = cartes[i+1];
            cartes[i+1] = null;
            Execution.ajouterEtape();   
        }
        
        Execution.ajouterEtape();

        cartes[cartes.length-1] = memoireA;
        memoireA = null;
        Execution.ajouterEtape();

    }

    private void insererAuDebut() {
        memoireA = cartes[aDeplacer];
        cartes[aDeplacer] = null;
        Execution.ajouterEtape();
        
        for(i = 0; i <= aDeplacer; i++) {

            Execution.ajouterEtape();
            
            memoireB = cartes[i];
            cartes[i] = null;

            Execution.ajouterEtape();
            
            cartes[i] = memoireA;
            memoireA = null;

            Execution.ajouterEtape();
            
            memoireA = memoireB;
            memoireB = null;

            Execution.ajouterEtape();
        }
    }
}
