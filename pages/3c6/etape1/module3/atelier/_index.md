---
title: "Atelier 1.3: procédure *deplacer au début/fin*"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


## Objectif

* {{% link "/3c6/etape1/module3/atelier/outil_validation" "Se familiariser avec l'outil de validation du projet `codelescartes`" %}}

* {{% link "/3c6/etape1/module3/atelier/coder_atelier" "Implanter et valider la procédure" %}}

## Remises

* {{% link "/3c6/etape1/module3/atelier/remise_gitlab" "Remise via GitLab" %}}

* {{% link "/3c6/etape1/module3/atelier/remise_moodle" "Remise via Moodle" %}}

## Corrigé

* {{% link "./corrige" "Corrigé: code pour les procédures" %}}

