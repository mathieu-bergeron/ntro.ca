---
title: "Module 1.3: tableau d'objets"
weight: 1
---

{{% pageTitle %}}

1. Théorie
    1. Procédure: {{% link "/3c6/etape1/module3/theorie/deplacer_decaler" "déplacer et décaler dans un tableau" %}}
    1. Compréhension: {{% link "/3c6/etape1/module3/theorie/dessiner_tableau" "dessiner un tableau d'objets" %}}
    1. Compréhension: {{% link "/3c6/etape1/module3/theorie/references_multiples" "références multiples" %}}

1. {{% link "/3c6/etape1/module3/entrevue" "Entrevue 1.3" %}}

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=325880" target="_blank">Mini-test théorique</a>


1. {{% link "/3c6/etape1/module3/atelier" "Atelier 1.3" %}}


<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}



<!--

1. $[link ./theorie/](Théorie)
1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=204770" target="_blank">Mini-test sur la théorie</a>
1. $[link ./tutoriel/](Tutoriel)
1. $[link ./atelier/](Atelier)
1. <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=204771" target="_blank">Mini-test sur l'atelier</a>

<br>
<div style="padding:5px;background:pink;border-style:dotted" >
RAPPEL: toutes les évaluations sont <strong>individuelles</strong> 
<ul>
<li>chaque remise est analysée par un outil de détection de plagiat
<li>ne <strong>jamais</strong> copier-coller le code d'un autre étudiant
<li>ne <strong>jamais</strong> écrire directement le code qu'un autre étudiant vous dicte
</ul> 
</div>

-->
