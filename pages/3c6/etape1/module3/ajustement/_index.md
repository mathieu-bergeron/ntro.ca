---
title: "Ajustement: environnement de développement portable"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Étape 1: télécharger l'environnement potable

1. Se connecter à mon compte Microsoft du Collège

1. Télécharger l'archive `git_java_eclipse.zip` à partir du OneDrive du prof

    * https://cmontmorency365-my.sharepoint.com/:u:/g/personal/mbergeron_cmontmorency_qc_ca/EXqF07hKcHtEq4wc5xEZ5l4BQNZsW1vgwkZaIzdWJ1e0Zw?e=DuyZAL

    * Cliquer sur *Télécharger*

        <img class="figure" src="telecharger_zip01.png"/>

    * ou sur *télécharger le fichier zip entier*

        <img class="figure" src="telecharger_zip02.png"/>

1. Déplacer le fichier `git_java_eclipse.zip` dans mon **répertoire usager**

    <img class="figure" src="deplacer_zip.png"/>

1. Extraire les fichiers **directement** dans mon répertoire usager

    * Clic-droit sur le fichier => *Extraire tout*

        <img class="figure" src="extraire_tout.png"/>

    * **Effacer `git_java_eclipse` du chemin proposé**

        <img class="figure" src="extraire_ici01.png"/>

    * Cliquer sur *Extraire*

        <img class="figure" src="extraire_ici02.png"/>

    * Attendre que les fichiers de l'archive soit tous copiés

    * Vérifier que le répertoire `C:\Users\MON_USAGER\git_java_eclipse` contient bien les sous-répertoires suivants

        <img class="figure" src="verifier.png"/>

    * On **ne dois pas** avoir `C:\Users\MON_USAGER\git_java_eclipse\git_java_eclipse`

    * Supprimer `git_java_eclipse.zip` 
        * (ou le déplacer vers une clé USB pour en garder une copie)

## Étape 2: installer et ouvrir Git (version portable)

{{<excerpt class="note">}}
Si Git est déjà installé, ouvrir Git Bash et passer à l'étape&nbsp;3
{{</excerpt>}}

1. Double-ciquer sur `PortableGit-2.37.2.2-64bit.7z.exe` pour installer Git 

    <img class="figure" src="installer_git.png"/>

    * Cliquer sur *OK* pour accepter le chemin proposé

1. Attendre que Git s'installe

1. Supprimer le fichier `PortableGit-2.37.2.2-64bit.7z.exe`

1. Ouvrir le répertoire `PortableGit`

1. Double-cliquer sur `git-bash.exe` pour exécuter Git Bash

1. *TRUC* créer un raccourci sur le bureau et/ou épingler Git Bash

    <img class="figure" src="raccourci_gitbash.png"/>
    <br>
    <img class="figure" src="epingler_gitbash.png"/>

## Étape 3: configurer l'environnement

1. Dans Git Bash, faire les commandes suivantes

    ```bash
    $ cd
    $ cd git_java_eclipse
    $ sh scripts/configurer.sh
    ```

1. Recharger la configuration de Bash

    ```bash
    $ cd 
    $ source .bash_profile
    ```

1. Tester la version de Java

    ```bash
    $ java --version

        java 11.0.16 2022-07-19 LTS
        Java(TM) SE Runtime Environment 18.9 (build 11.0.16+11-LTS-199)
        Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.16+11-LTS-199, mixed mode)
    ```

## Étape 4: démarrer Eclipse

1. Revenir au répertoire `git_java_eclipse`

1. Ouvrir le répertoire `eclipse`

1. Double-cliquer sur `eclipse.exe` pour exécuter Eclipse

1. Cliquer sur *Launch* pour utiliser le Workspace par défaut

    <img class="figure" src="workspace.png"/>

1. *TRUC* créer un raccourci sur le bureau et/ou épingler Eclipse

    <img class="figure" src="raccourci_eclipse.png"/>
    <br>
    <img class="figure" src="epingler_eclipse.png"/>



