---
title: 'Module 1.2: rappels POO'
weight: 1
---

{{% pageTitle %}}

<!--
<center>
<video width="50%" src="rappel.mp4" type="video/mp4" controls playsinline>
</center>
-->

1. **IMPORTANT**: {{% link "./correctif" "correctif au dépôt Git pour les groupes de Mathieu Bergeron" %}}

1. **Rappel**: `C:\Users\MON_USAGER` est un répertoire temporaire qui peut être effacé sans préavis

   {{<excerpt>}}

**AVANT DE TRAVAILLER**:

```bash
$ git pull
```

- OU: {{% link "../module1/atelier/g" "Étapes pour travailler sur un nouvel ordi" %}}

**AVANT DE QUITTER**:

```bash
$ git add .
$ git commit -a -m "en cours) XYZ"
$ git push
```

- ET: vérifier que vos fichiers sont sur GitLab

  {{</excerpt>}}

1. Théorie

   - {{% link "/3c6/etape1/module2/theorie/contrat" "code de librairie Vs code d'application" %}}
   - {{% link "/3c6/etape1/module2/theorie/heritage" "héritage «de base»" %}}
   - {{% link "/3c6/etape1/module2/theorie/polymorphisme" "polymorphisme" %}}
   - {{% link "/3c6/etape1/module2/theorie/attributs" "attributs, visibilité" %}}
   - {{% link "/3c6/etape1/module2/theorie/interface" "interface, classe abstraite" %}}
   - {{% link "/3c6/etape1/module2/theorie/constructeur" "constructeurs, `super`" %}}

1. Code fait en classe: https://gitlab.com/mathieu-bergeron/3c6_exemples

1. {{% link "/3c6/etape1/module2/entrevue" "Entrevue 1.2" %}}

1. Mini-test théorique

   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=323277" target="_blank">Mathieu Bergeron</a>
   - <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=410327" target="_blank">Marc-Olivier Tremblay</a>

1. Atelier

   - exercice A: {{% link "/3c6/etape1/module2/atelier/heritage" "héritage «de base»" %}}
   - exercice B: {{% link "/3c6/etape1/module2/atelier/polymorphisme" "polymorphisme" %}}
   - exercice C: {{% link "/3c6/etape1/module2/atelier/attributs" "attributs, visibilité" %}}
   - exercice D: {{% link "/3c6/etape1/module2/atelier/interface" "interface, classe abstraite" %}}
   - exercice E: {{% link "/3c6/etape1/module2/atelier/constructeur" "constructeurs, `super`" %}}

1. Remises
   - {{% link "/3c6/etape1/module2/atelier/remise_gitlab" "sur GitLab" %}}
   - {{% link "/3c6/etape1/module2/atelier/remise_moodle" "sur Moodle" %}}

<br>
<br>

{{% embed "/3c6/presentation/avertissement_plagiat.md" %}}
