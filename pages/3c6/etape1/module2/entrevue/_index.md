---
title: "Entrevue 1.2: rappels POO"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

1. Dessiner **à la main** la hierarchie de classe la plus logique pour les classes suivantes:

    * `Dalmatien`, `Chat`, `ShihTzu`, `Chien`, `Persan`, `Animal`, `Siamois`, `Bouledoge`

1. Choisir l'endroit le plus logique où placer les méthodes suivantes:
    * `public void manger()`
    * `public void dormir()`
    * `public String typeDeCri()`
    * `public void miauler()`
    * `public void japper()`

1. Indiquer la ou les classes abstraites avec un `*`

1. Indiquer la ou les méthodes abstraites avec un `*`

