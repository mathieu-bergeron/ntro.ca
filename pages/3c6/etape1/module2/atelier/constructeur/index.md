---
title: "Atelier 1.2 exercice E: constructeur et `super`"
weight: 1
bookHidden : true
---

{{% pageTitle %}}

## Objectifs

1. Je crée la classe `MonAtelier1_2_E` qui hérite de la classe `Atelier1_2_E`

1. Je recopie les classes suivantes de l'`atelier1_2_D`:
    * `Vehicule`
    * `Auto`
    * `Camion`
    * `Mobilette`
    * `Moto`

1. Je recopie les interfaces suivantes de l'`atelier1_2_D`:
    * `Rouleur`
    * `Formateur`

1. J'ajoute un constructeur `Vehicule(double totalKilometres)`

1. J'ajoute un constructeur `Camion(double totalKilometres, double chargementEnKilos)`

1. Dans `Auto`, je redéfinis la méthode `formater` pour retourner:
    * la même chose que `Vehicule.formater`, mais en ajoutant `" J'adore mon XXX!"`
        * où: `XXX` sera remplacé par le nom du véhicule (ici `auto`)

1. Dans `Mobilette`, je redéfinis la méthode `rouler` pour:
    * faire d'abord la même chose que `Vehicule.rouler`, puis
        * afficher `"La mobilette devrait déjà être remisée!"`
            * si la date actuelle est plus grande que le 1er octobre 2020
        * sinon afficher `"ATTENTION: il reste JOURS avant de remiser la mobilette"`
            * où `JOURS` est le nombre de jours qu'il reste avant le 1er octobre 2020

1. Je corrige les erreurs de compilation

1. J'ajoute une méthode `main` à la classe `MonAtelier1_2_E`:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/constructeur/MonAtelier1_2_E.java" 
    first-line="3"
    last-line="6"
    indent-level="1"
%}}
    ```

1. J'implante les méthodes pour remplir le contrat du `Atelier1_2_E`, p.ex:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/constructeur/MonAtelier1_2_E.java" 
    first-line="8"
    last-line="26"
    indent-level="1"
%}}
    ```

    * NOTE: il y a maintenant des paramètres à passer au constructeur

1. J'exécute mon projet et je valide mes classes et mes méthodes

1. J'ajoute les fichiers du projet dans Git 

1. Je fais un `commit` et un `push`

## Réalisation

### Étape 01: créer la classe `MonAtelier1_2_E`

1. Je crée une nouvelle classe nommée `MonAtelier1_2_E`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `MonAtelier1_2_E`

### Étape 02: hériter de Atelier1_2_E

1. J'ouvre `MonAtelier1_2_E` et j'ajoute `extends Atelier1_2_E`

1. J'utilise {{% key "Ctrl+1" %}} pour ajouter le `import` de `Atelier1_2_E`

1. J'utilise {{% key "Ctrl+1" %}} pour générer les méthodes manquantes
    * option `add unimplemented methods`

### Étape 03: copier les classes et les interfaces à partir de `atelier1_2_D`

1. Pour les classes `Auto`, `Camion`, `Mobilette`, `Moto` et `Vehicule`
    * je fais un copier-coller:
        * à partir de la classe située dans le paquet `atelier1_2_D`
        * vers le paquet `atelier1_2_E`

1. Pour les interfaces `Rouleur` et `Formateur`
    * je fais un copier-coller:
        * à partir de l'interface située dans le paquet `atelier1_2_D`
        * vers le paquet `atelier1_2_E`

### Étape 04: ajouter les constructeurs

1. Dans `Vehicule`, j'ajoute le constructeur suivant:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/constructeur/Vehicule.java" 
    first-line="5"
    last-line="7"
    indent-level="1"
%}}
    ```

1. Dans `Camion`, j'ajoute le constructeur suivant:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/constructeur/Camion.java" 
    first-line="7"
    last-line="11"
    indent-level="1"
%}}
    ```

1. J'utilise {{% key "Ctrl+1" %}} pour ajouter les constructeurs de
    * `Auto`, `Moto` et `Mobilette`

### Étape 05: redéfinir `formater` dans `Auto`

1. Dans `Auto`, je redéfinis `formater`:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/constructeur/Auto.java" 
    first-line="7"
    last-line="10"
    indent-level="1"
%}}
    ```

### Étape 06: redéfinir `rouler` dans `Mobilette`

* Dans `Mobilette`, j'ajoute un attribut pour la date limite:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/constructeur/Mobilette.java" 
    first-line="1"
    last-line="3"
    indent-level="1"
%}}
```

* Dans `Mobilette`, je redéfinis la méthode `rouler`

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/constructeur/Mobilette.java" 
    first-line="10"
    last-line="26"
    indent-level="1"
%}}
```


### Étape 07: ajouter la méthode `main`

1. J'ouvre `MonAtelier1_2_E` et j'ajoute la méthode `main`

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/constructeur/MonAtelier1_2_E.java" 
    first-line="1"
    last-line="6"
    indent-level="1"
%}}
```

### Étape 08: exécuter pour valider

1. J'exécute mon programme

1. Je vérifie que la validation est réussie:

    <center>
    <img src="validation.png" width="100%" />
    </center>
