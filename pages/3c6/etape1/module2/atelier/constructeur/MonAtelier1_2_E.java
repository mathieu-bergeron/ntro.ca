public class MonAtelier1_2_E extends Atelier1_2_E {
    
    public static void main(String[] args) {
        
        new MonAtelier1_2_E().valider();
    }

    @Override
    public Object creerMoto(double kilometrage) {
        return new Moto(kilometrage);
    }

    @Override
    public Object creerMobilette(double kilometrage) {
        return new Mobilette(kilometrage);
    }

    @Override
    public Object creerAuto(double kilometrage) {
        return new Auto(kilometrage);
    }

    @Override
    public Object creerCamion(double kilometrage, double chargementEnKilos) {
        return new Camion(kilometrage, chargementEnKilos);
    }
}
