public class Camion extends Vehicule {
    
    private double consommationDeBase = 14;
    private double chargementEnKilos = 100;

    public void accepterChargement(double chargementEnKilos) {
        this.chargementEnKilos = chargementEnKilos;
    }

    @Override
    protected double consommationLitresParKilometre() {
        return calculerConsommationSelonChargement();
    }

    @Override
    protected boolean siNomFeminin() {
        return false;
    }

    @Override
    protected String nomVehicule() {
        return "camion";
    }

    private double calculerConsommationSelonChargement() {
        return (1 + chargementEnKilos / 1E6) * consommationDeBase;
    }

}
