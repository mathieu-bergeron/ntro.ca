---
title: "Atelier 1.2 exercice C: visibilité"
weight: 1
bookHidden : true
---

{{% pageTitle %}}


## Objectifs

1. Je crée la classe `MonAtelier1_2_C` qui hérite de la classe `Atelier1_2_C`

1. Je crée les classes de la hiérarchie suivante:

    <center>
    <img width="75%" src="hierarchie_vehicules.svg"/>
    </center>

1. Je m'assure que `Vehicule implements Rouleur, Formateur`
    * j'ajoute les deux méthodes obligatoires
    * il s'agit des deux seules métodes publiques de `Vehicule`

1. Dans `Vehicule`, je définis cet attribut, **avec la bonne visibilité**
    * `double totalKilometres`
        * le nombre total de kilomètres parcourus depuis le début du programme

1. Dans la bonne classe, j'implante ou redéfini ces méthodes, **avec la bonne visibilité**
    1. `void rouler(double kilometres)`
        * ajoute `kilometres` au `totalKilometres`
    1. `String formater()`
        * retourne `"Ma/Mon VEHICULE a roulé TOTAL kilomètres et consomé LITRES litres d'essence."`
        * où:
            * `Ma/Mon` selon si le nom du véhicule est masculin ou féminin
            * `VEHICULE` est remplacé par le nom du véhicule
            * `TOTAL` est remplacé par le total des kilomètres parcourus
            * `LITRES` est remplacé par les litres d'essence consomés, au total
    1. `double consommationLitresParKilometre() `
        * retourne la consommation théorique en `litres/kilomètres`
            * `Mobilette`: `1.5`
            * `Moto`: `3.5`
            * `Auto`: `8.0`
            * `Fourgonnette`: `12.0`
            * `Camion`: selon un calcul (voir ci-bas)
    1. `String nomVehicule()`
        * retourne le nom du véhicule (en minuscule)
    1. `boolean siNomFeminin()`
        * retourne vrai si le nom du véhicule est féminin
        * retourne faux sinon
    1.  `double litresEssenceConsomes(double kilometres)`
        * calcule la consommation en litres pour un certain kilométrage

1. Dans `Camion`, je définis ces attributs, **avec la bonne visibilité**
    * `double consommationDeBase`
        * la consommation de base de `14`, en litres par kilomètre
    * `double chargementEnKilos`
        * le poids du chargement du camion, `100` kilos pour commencer

1. Dans `Camion`, j'implante ces méthodes, **avec la bonne visibilité**
    * `void accepterChargement(double chargementEnKilos)`
        * mémorise le poids d'un nouveau chargement
    * `double calculerConsommationSelonChargement`
        * calcule la consommation d'essence en fonction du poids du chargement
        * selon la formule: `(1 + chargementEnKilos / 1E6) * consommationDeBase`

1. J'ajoute une méthode `main` à la classe `MonAtelier1_2_C`:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/MonAtelier1_2_C.java" 
    first-line="3"
    last-line="6"
    indent-level="1"
%}}
    ```

1. J'implante les méthodes pour remplir le contrat du `Atelier1_2_C`, p.ex:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/MonAtelier1_2_C.java" 
    first-line="8"
    last-line="11"
    indent-level="1"
%}}
    ```

1. J'exécute mon projet et je valide mes classes et mes méthodes

1. J'ajoute les fichiers du projet dans Git 

1. Je fais un `commit` et un `push`

## Réalisation

### Étape 01: créer la classe `MonAtelier1_2_C`

1. Je crée une nouvelle classe nommée `MonAtelier1_2_C`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `MonAtelier1_2_C`

### Étape 02: hériter de Atelier1_2_C

1. J'ouvre `MonAtelier1_2_C` et j'ajoute `extends Atelier1_2_C`

1. J'utilise {{% key "Ctrl+1" %}} pour ajouter le `import` de `Atelier1_2_C`

1. J'utilise {{% key "Ctrl+1" %}} pour générer les méthodes manquantes
    * option `add unimplemented methods`

### Étape 03: créer la classe `Vehicule`

1. Je crée une nouvelle classe nommée `Vehicule`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `Vehicule`

1. J'ouvre `Vehicule` et j'ajoute `implements Rouleur, Formateur`

1. J'utilise {{% key "Ctrl+1" %}} pour ajouter les `import`

1. J'utilise {{% key "Ctrl+1" %}} pour générer les méthodes manquantes
    * option `add unimplemented methods`

### Étape 04: créer la classe `Moto`

1. Je crée une nouvelle classe nommée `Moto`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `Moto`

1. J'ouvre `Moto` et j'ajoute `extends Vehicule`

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Moto.java" 
    first-line="1"
    last-line="1"
    indent-level="1"
%}}
```

### Étape 05: créer la classe `Mobilette`

1. Je crée une nouvelle classe nommée `Mobilette`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `Mobilette`

1. J'ouvre `Mobilette` et j'ajoute `extends Vehicule`

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Mobilette.java" 
    first-line="1"
    last-line="1"
    indent-level="1"
%}}
```


### Étape 06: créer la classe `Auto`

1. Je crée une nouvelle classe nommée `Auto`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `Auto`

1. J'ouvre `Auto` et j'ajoute `extends Vehicule`

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Auto.java" 
    first-line="1"
    last-line="1"
    indent-level="1"
%}}
```

### Étape 07: créer la classe `Camion`

1. Je crée une nouvelle classe nommée `Camion`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `Camion`

1. J'ouvre `Camion` et j'ajoute `extends Vehicule`

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Camion.java" 
    first-line="1"
    last-line="1"
    indent-level="1"
%}}
```

### Étape 08: ajouter les attributs

1. J'ouvre `Vehicule` et j'ajoute:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Vehicule.java" 
    first-line="3"
    last-line="3"
    indent-level="1"
%}}
    ```

1. J'ouvre `Camion` et j'ajoute:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Camion.java" 
    first-line="3"
    last-line="4"
    indent-level="1"
%}}
    ```

### Étape 09: ajouter les méthodes *implantées*

1. J'ouvre `Vehicule` et j'ajoute:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Vehicule.java" 
    first-line="29"
    last-line="43"
    indent-level="1"
%}}
    ```

    * la dernière méthode est `private`. C'est un détail d'implantation.
    * les autres sont `protected` car les sous-classes vont les redéfinir

1. J'ouvre `Camion` et j'ajoute:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Camion.java" 
    first-line="6"
    last-line="8"
    indent-level="1"
%}}
    ```

    * la méthode est `public`. C'est ailleurs dans
    le programme qu'on va indiquer au camion d'accepter un nouveau chargement

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Camion.java" 
    first-line="25"
    last-line="27"
    indent-level="1"
%}}
    ```

    * la méthode est `private`. C'est un détail d'implantation.

### Étape 10: ajouter les méthodes *redéfinies*

1. J'ouvre `Vehicule` et je complète les méthodes suivantes:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Vehicule.java" 
    first-line="5"
    last-line="27"
    indent-level="1"
%}}
    ```

1. J'ouvre `Mobilette` et je redéfinis les méthodes suivantes:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Mobilette.java" 
    first-line="3"
    last-line="16"
    indent-level="1"
%}}
    ```

1. J'ouvre `Moto` et je redéfinis les méthodes suivantes:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Moto.java" 
    first-line="3"
    last-line="16"
    indent-level="1"
%}}
    ```

1. J'ouvre `Auto` et je redéfinis les méthodes suivantes:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Auto.java" 
    first-line="3"
    last-line="16"
    indent-level="1"
%}}
    ```

1. J'ouvre `Camion` et je redéfinis les méthodes suivantes:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/Camion.java" 
    first-line="10"
    last-line="23"
    indent-level="1"
%}}
    ```


### Étape 11: ajouter la méthode `main`

1. J'ouvre `MonAtelier1_2_C` et j'ajoute la méthode `main`

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/attributs/MonAtelier1_2_C.java"
    first-line="1"
    last-line="6"
    indent-level="1"
%}}
```


### Étape 12: exécuter pour valider

1. J'exécute mon programme

1. Je vérifie que la validation est réussie:

    <center>
    <img src="validation.png" width="100%" />
    </center>
