public class Mobilette extends Vehicule {

	@Override
	protected double consommationLitresParKilometre() {
		return 1.5;
	}

	@Override
	protected boolean siNomFeminin() {
		return true;
	}

	@Override
	protected String nomVehicule() {
		return "mobilette";
	}
}
