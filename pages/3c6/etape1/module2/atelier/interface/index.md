---
title: "Atelier 1.2 exercie 4: interface et classe abstraite"
weight: 1
bookHidden : true
---

{{% pageTitle %}}

## Objectifs

1. Je crée la classe `MonAtelier1_2_D` qui hérite de la classe `Atelier1_2_D`

1. Je crée moi-même les interfaces `Rouleur` et `Formateur`

1. Je recopie les classes suivantes de l'`atelier1_2_C`:
    * `Vehicule`
    * `Auto`
    * `Camion`
    * `Mobilette`
    * `Moto`

1. J'ajoute les bonnes méthodes dans les interfaces

1. Je transforme la classe `Vehicule` en classe abstraite

1. Je transforme les méthodes "vides" en méthodes abstraites

1. Je corrige les erreurs de compilation

1. J'ajoute une méthode `main` à la classe `MonAtelier1_2_D`:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/interface/MonAtelier1_2_D.java" 
    first-line="3"
    last-line="6"
    indent-level="1"
%}}
    ```

1. J'implante les méthodes pour remplir le contrat du `Atelier1_2_D`, p.ex:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/interface/MonAtelier1_2_D.java" 
    first-line="8"
    last-line="26"
    indent-level="1"
%}}
    ```

1. J'exécute mon projet et je valide mes classes et mes interfaces

1. J'ajoute les fichiers du projet dans Git 

1. Je fais un `commit` et un `push`

## Réalisation

### Étape 01: créer la classe `MonAtelier1_2_D`

1. Je crée une nouvelle classe nommée `MonAtelier1_2_D`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `MonAtelier1_2_D`

### Étape 02: hériter de Atelier1_2_D

1. J'ouvre `MonAtelier1_2_D` et j'ajoute `extends Atelier1_2_D`

1. J'utilise {{% key "Ctrl+1" %}} pour ajouter le `import` de `Atelier1_2_D`

1. J'utilise {{% key "Ctrl+1" %}} pour générer les méthodes manquantes
    * option `add unimplemented methods`

### Étape 03: créer les interfaces

1. Je crée une nouvelle interface nommée `Rouleur`
    * *Clique-droit* sur le projet => *New* => *Interface*
        *  *Name*: `Rouleur`

1. Je crée une nouvelle interface nommée `Formateur`
    * *Clique-droit* sur le projet => *New* => *Interface*
        *  *Name*: `Formateur`

### Étape 04: copier les classes à partir de `atelier1_2_C`

1. Pour les classes `Auto`, `Camion`, `Mobilette`, `Moto` et `Vehicule`
    * je fais un copier-coller:
        * à partir de la classe située dans le paquet `atelier1_2_C`
        * vers le paquet `atelier1_2_D`

### Étape 05: réparer les `import`

1. Dans `Vehicule`, j'efface les `import` de `Rouleur` et `Formateur`

### Étape 06: remplir les interfaces

* Dans `Rouleur`, j'ajoute la méthode `rouler`:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/interface/Rouleur.java" 
    first-line=""
    last-line=""
    indent-level="1"
%}}
```

1. Dans `Formateur`, j'ajoute la méthode `formter`:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/interface/Formateur.java" 
    first-line=""
    last-line=""
    indent-level="1"
%}}
```

### Étape 07: transformer `Vehicule` en classe abstraite

1. Dans `Vehicule`, j'ajoute le mot clé `abstract`:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/interface/Vehicule.java" 
    first-line="1"
    last-line="1"
    indent-level="1"
%}}
```

### Étape 08: transformer les méthodes "vides" en méthodes abstraites

1. Dans `Vehicule`, je considère les méthodes:
    * `consommationLitresParKilometre`
    * `nomVehicule`
    * `siNomFeminin`

1. Pour chaque, méthode ci-haut:
    * j'ajoute le mot clé `abstract`
    * j'efface le corps de la méthode (le code)

1. Voici le résultat:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/interface/Vehicule.java" 
    first-line="3"
    last-line="7"
    indent-level="1"
%}}
```

### Étape 09: ajouter la méthode `main`

1. J'ouvre `MonAtelier1_2_D` et j'ajoute la méthode `main`

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/interface/MonAtelier1_2_D.java" 
    first-line="1"
    last-line="6"
    indent-level="1"
%}}
```


### Étape 10: exécuter pour valider

1. J'exécute mon programme

1. Je vérifie que la validation est réussie:

    <center>
    <img src="validation.png" width="100%" />
    </center>
