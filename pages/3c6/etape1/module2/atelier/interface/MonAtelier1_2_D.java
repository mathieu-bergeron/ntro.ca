public class MonAtelier1_2_D extends Atelier1_2_D {
    
    public static void main(String[] args) {
        
        new MonAtelier1_2_D().valider();
    }

    @Override
    public Object fournirMoto() {
        return new Moto();
    }

    @Override
    public Object fournirMobilette() {
        return new Mobilette();
    }

    @Override
    public Object fournirAuto() {
        return new Auto();
    }

    @Override
    public Object fournirCamion() {
        return new Camion();
    }

}
