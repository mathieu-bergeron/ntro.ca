public abstract class Vehicule implements Rouleur, Formateur {

	protected abstract double consommationLitresParKilometre();

	protected abstract String nomVehicule();

	protected abstract boolean siNomFeminin();
}
