---
title: "Atelier 1.2 exercice A: héritage"
weight: 1
bookHidden : true
---

{{% pageTitle %}}

## Objectifs

* Créer la classe `MonAtelier1_2_A` qui hérite de la classe `Atelier1_2_A`

* Implanter les méthodes pour remplir le contrat de `Atelier1_2_A`

* Créer les classes de la hiérarchie suivante:

    <img class="figure" src="hierarchie.svg"/>

* Implanter ou redéfinir les méthodes suivantes, **dans les bonnes classes**
    * `public int nombreDeRoues()`
        * retourne le nombre de roues du véhicule
    * `public double consommationLitresParKilometre() `
        * retourne la consommation théorique en `litres/kilomètres`
            * `Mobilette`: `1.5`
            * `Moto`: `3.5`
            * `Auto`: `8.0`
            * `Fourgonnette`: `12.0`
            * `Camion`: `14.0`
    * `public double litresEssenceConsomes(double kilometres) `
        * calcule la consommation en litres pour un certain kilométrage

* Ajouter la méthode `main` à la classe `MonAtelier1_2_A`:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/MonAtelier1_2_A.java" 
    first-line="1"
    last-line="6"
%}}
```

* Dans `MonAtelier1_2_A`, compléter les méthodes `fournirMoto`, `fournirMobilette`, etc.

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/MonAtelier1_2_A.java" 
    first-line="8"
    last-line="31"
%}}
```

* Exécuter mon projet et valider mes classes et mes méthodes

## Réalisation

### Étape 01: créer la classe `MonAtelier1_2_A`

1. Je crée une nouvelle classe nommée `MonAtelier1_2_A`
    * *Clique-droit* sur le projet => *New* => *Class*
        * Nom de la classe: `MonAtelier1_2_A`

### Étape 02: hériter de Atelier1_2_A

1. J'ouvre `MonAtelier1_2_A` et j'ajoute `extends Atelier1_2_A`

1. J'utilise {{% key "Ctrl+1" %}} pour ajouter le `import` de `Atelier1_2_A`

1. J'utilise {{% key "Ctrl+1" %}} pour générer les méthodes manquantes
    * option `add unimplemented methods`

### Étape 03: créer la classe `Vehicule`

1. Je crée une nouvelle classe nommée `Vehicule`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  Nom de la classe: `Vehicule`

### Étape 04: créer la classe `DeuxRoues`

1. Je crée une nouvelle classe nommée `DeuxRoues`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  Nom de la classe: `DeuxRoues`

1. J'ouvre `DeuxRoues` et j'ajoute `extends Vehicule`

    ```java
    {{% embed 
        src="/3c6/etape1/module2/atelier/heritage/DeuxRoues.java" 
        first-line="1"
        last-line="1"
        indent-level="1"
    %}}
    ```

### Étape 05: créer la classe `QuatreRoues`

1. Je crée une nouvelle classe nommée `QuatreRoues`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  Nom de la classe: `QuatreRoues`

1. J'ouvre `QuatreRoues` et j'ajoute `extends Vehicule`

    ```java
    {{% embed 
        src="/3c6/etape1/module2/atelier/heritage/QuatreRoues.java" 
        first-line="1"
        last-line="1"
        indent-level="1"
    %}}

    ```

### Étape 06: créer la classe `Moto`

1. Je crée une nouvelle classe nommée `Moto`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  Nom de la classe: `Moto`

1. J'ouvre `Moto` et j'ajoute `extends DeuxRoues`

    ```java
    {{% embed 
        src="/3c6/etape1/module2/atelier/heritage/Moto.java" 
        first-line="1"
        last-line="1"
        indent-level="1"
    %}}
    ```

### Étape 07: créer la classe `Mobilette`

1. Je crée une nouvelle classe nommée `Mobilette`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  Nom de la classe: `Mobilette`

1. J'ouvre `Mobilette` et j'ajoute `extends DeuxRoues`

    ```java
    {{% embed 
        src="/3c6/etape1/module2/atelier/heritage/Mobilette.java" 
        first-line="1"
        last-line="1"
        indent-level="1"
    %}}

    ```

### Étape 08: créer la classe `Auto`

1. Je crée une nouvelle classe nommée `Auto`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  Nom de la classe: `Auto`

1. J'ouvre `Auto` et j'ajoute `extends QuatreRoues`

    ```java
    {{% embed 
        src="/3c6/etape1/module2/atelier/heritage/Auto.java" 
        first-line="1"
        last-line="1"
        indent-level="1"
    %}}

    ```

### Étape 09: créer la classe `Camion`

1. Je crée une nouvelle classe nommée `Camion`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  Nom de la classe: `Camion`

1. J'ouvre `Camion` et j'ajoute `extends QuatreRoues`

    ```java
    {{% embed 
        src="/3c6/etape1/module2/atelier/heritage/Camion.java" 
        first-line="1"
        last-line="1"
        indent-level="1"
    %}}

    ```

### Étape 10: créer la classe `Fourgonnette`

1. Je crée une nouvelle classe nommée `Fourgonnette`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  Nom de la classe: `Fourgonnette`

1. J'ouvre `Fourgonnette` et j'ajoute `extends QuatreRoues`

    ```java
    {{% embed 
        src="/3c6/etape1/module2/atelier/heritage/Fourgonnette.java" 
        first-line="1"
        last-line="1"
        indent-level="1"
    %}}

    ```

### Étape 11: ajouter les méthodes *implantées*

1. J'ouvre `Vehicule` et j'implante les méthodes suivantes:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/Vehicule.java" 
    first-line="3"
    last-line="13"
%}}
```

### Étape 12: ajouter les méthodes *redéfinies*

* J'ouvre `DeuxRoues` et je redéfinis la méthode suivante:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/DeuxRoues.java" 
    first-line="3"
    last-line="6"
%}}
```

* J'ouvre `QuatreRoues` et je redéfinis la méthode suivante:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/QuatreRoues.java" 
    first-line="3"
    last-line="6"
%}}
```

* J'ouvre `Mobilette` et je redéfinis la méthode suivante:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/Mobilette.java" 
    first-line="3"
    last-line="6"
%}}
```

* J'ouvre `Moto` et je redéfinis la méthode suivante:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/Moto.java" 
    first-line="3"
    last-line="6"
%}}
```

* J'ouvre `Auto` et je redéfinis la méthode suivante:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/Auto.java" 
    first-line="3"
    last-line="6"
%}}
```

* J'ouvre `Camion` et je redéfinis la méthode suivante:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/Camion.java" 
    first-line="3"
    last-line="6"
%}}
```

* J'ouvre `Fourgonnette` et je redéfinis la méthode suivante:

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/Fourgonnette.java" 
    first-line="3"
    last-line="6"
%}}
```


### Étape 13: ajouter la méthode `main`

1. J'ouvre `MonAtelier1_2_A` et j'ajoute la méthode `main`

```java
{{% embed 
    src="/3c6/etape1/module2/atelier/heritage/MonAtelier1_2_A.java" 
    first-line="1"
    last-line="6"
%}}
```


### Étape 14: exécuter pour valider

1. J'exécute mon programme

1. Je vérifie que la validation est réussie:

    <img class="figure" src="validation.png" />
