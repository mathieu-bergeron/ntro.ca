public class MonAtelier1_2_A extends Atelier1_2_A {
    
    public static void main(String[] args) {
        
        new MonAtelier1_2_A().valider();
    }

    @Override
    public Object fournirMoto() {
        return new Moto();
    }

    @Override
    public Object fournirMobilette() {
        return new Mobilette();
    }

    @Override
    public Object fournirAuto() {
        return new Auto();
    }

    @Override
    public Object fournirCamion() {
        return new Camion();
    }

    @Override
    public Object fournirFourgonnette() {
        return new Fourgonnette();
    }
