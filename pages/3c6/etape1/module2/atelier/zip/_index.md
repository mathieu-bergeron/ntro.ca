---
title: "Ajustement: utiliser Zip plutôt que 7z"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
* **RAISON**: 7z n'apparaît pas dans le menu contextuel en Window 11 (?) 
{{</excerpt>}}

1. **ATTENTION**: il faut quand même extraire le `.zip` **directement à la racine** du dépôt Git

    * Clic-droit sur le fichier => *Extraire tout*

        <img class="figure" src="./extraire_tout.png"/>

    * **Effacer `atelierX_Y` du chemin proposé**

        <img class="figure" src="./extraire_chemin.png"/>

    * Cliquer sur *Extraire*

    * Au besoin, choisir *Remplacer les fichiers dans la destination*

        <img class="small-figure" src="./remplacer_fichiers.png"/>

    * Vérifier que les fichiers sont **à la racine** du dépôt Git 

        <img class="figure" src="./extraire_verifier.png"/>
        
        * dans le même répertoire que `.git`

## On peut quand même utiliser 7z

1. Si j'ai 7z dans mon menu contextuel, je peux l'utiliser même pour un `.zip`

    * Clic-droit sur le fichier `.zip` => *7z* => *Extraire ici*

    * Au besoin, choisir *Oui pour tous* pour remplacer les fichiers déjà existants

1. Je peux aussi remettre sur Moodle en `.7z`
