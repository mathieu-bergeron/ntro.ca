---
title: "Ajustements: utiliser HTTPS pour Git"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
* **RAISON** le port 22 (port SSH) est bloqué par le pare-feu du Collège
{{</excerpt>}}
<br>
<br>

1. Pour cloner mon dépôt, utiliser plutôt l'adresse HTTPS

    ```bash
    $ git clone https://gitlab.com/NOM_USAGER/3c6_PRENOM_NOM
    ```

1. Pour un dépôt déjà cloné, modifier le fichier `.git/config`

    ```bash
    # remplacer cette ligne:
	url = git@gitlab.com:NOM_USAGER/3c6_PRENOM_NOM

    # par cette ligne:
	url = https://gitlab.com/NOM_USAGER/3c6_PRENOM_NOM

    # Donc
    git@gitlab.com: devient https://gitlab.com/

    # Attention
    le : devient /
    ```

1. Je devrai m'authentifier
    * par mot de passe
    * ou via un navigateur déjà connecté à GitLab

