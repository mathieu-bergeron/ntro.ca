public class MonAtelier1_2_B extends Atelier1_2_B {
    
    public static void main(String[] args) {
        
        new MonAtelier1_2_B().valider();
    }

    @Override
    public Accepteur fournirAccepteur() {
        return new MonAccepteur();
    }

    @Override
    public Formateur fournirFormateur() {
        return new MonFormateur();
    }

}
