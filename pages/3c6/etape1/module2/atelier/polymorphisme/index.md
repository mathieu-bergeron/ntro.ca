---
title: "Atelier 1.2 exercice B: polymorphisme"
weight: 1
bookHidden : true
---

{{% pageTitle %}}

## Objectifs

1. Je crée la classe `MonAtelier1_2_B` qui hérite de la classe `Atelier1_2_B`

1. Je crée la classe `MonAccepteur` qui implante l'interface `Accepteur`
    * la méthode `accepterSiDeuxRoues(Vehicule vehicule)` doit:
        * retourner vrai si le `vehicule` a deux roues
        * retourner faux sinon
    * la méthode `accepterSiEconomique(Vehicule vehicule)` doit:
        * retourner vrai si la consommation du `vehicule` est `6.0` ou moins
        * retourner faux sinon
    * la méthode `accepterSiMoto(Vehicule vehicule)` doit:
        * retourner vrai si le `vehicule` est une `Moto`
        * retourner faux sinon

1. Je crée la classe `MonFormateur` qui implante l'interface `Formateur`
    * la méthode `formater(Vehicule vehicule)` doit retourner:
        * `"Un/e VEHICULE est un véhicule à ROUES roues. Sa comsomation d'essence est CONSO litres par kilomètre."`
        * où:
            * `Un` ou `Une` selon le type de `vehicule`
            * `VEHICULE` est remplacé par le type de `vehicule`
            * `ROUES` est remplacé par le nombre de roues du `vehicule`
            * `CONSO` est remplacé par la consommation du `vehicule`

1. J'implante les méthodes `fournirAccepteur` et `fournirFormateur`

1. J'ajoute une méthode `main` à la classe `MonAtelier1_2_B`:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/polymorphisme/MonAtelier1_2_B.java" 
    first-line="3"
    last-line="6"
    indent-level="1"
%}}
    ```

1. J'exécute mon projet et je valide mes classes et mes méthodes

1. J'ajoute les fichiers du projet dans Git 

1. Je fais un `commit` et un `push`

## Réalisation

### Étape 01: créer la classe `MonAtelier1_2_B`

1. Je crée une nouvelle classe nommée `MonAtelier1_2_B`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `MonAtelier1_2_B`

### Étape 02: hériter de Atelier1_2_B

1. J'ouvre `MonAtelier1_2_B` et j'ajoute `extends Atelier1_2_B`

1. J'utilise {{% key "Ctrl+1" %}} pour ajouter le `import` de `Atelier1_2_B`


1. J'utilise {{% key "Ctrl+1" %}} pour générer les méthodes manquantes
    * option `add unimplemented methods`

### Étape 03: créer la classe `MonAccepteur`

1. Je crée une nouvelle classe nommée `MonAccepteur`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `MonAccepteur`

1. J'ouvre `MonAccepteur` et j'ajoute `implements Accepteur`

1. J'utilise {{% key "Ctrl+1" %}} pour ajouter le `import` de `Accepteur`


1. J'utilise {{% key "Ctrl+1" %}} pour générer les méthodes manquantes
    * option `add unimplemented methods`

1. Je remplis la méthode `accepterSiDeuxRoues`

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/polymorphisme/MonAccepteur.java" 
    first-line="9"
    last-line="12"
    indent-level="1"
%}}
    ```

1. Je remplis la méthode `accepterSiEconomique`

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/polymorphisme/MonAccepteur.java" 
    first-line="14"
    last-line="17"
    indent-level="1"
%}}
    ```

1. Je remplis la méthode `accepterSiMoto`

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/polymorphisme/MonAccepteur.java" 
    first-line="19"
    last-line="22"
    indent-level="1"
%}}
    ```

### Étape 04: créer la classe `MonFormateur`

1. Je crée une nouvelle classe nommée `MonFormateur`
    * *Clique-droit* sur le projet => *New* => *Class*
        *  *Name*: `MonFormateur`

1. J'ouvre `MonFormateur` et j'ajoute `implements Formateur`

1. J'utilise {{% key "Ctrl+1" %}} pour ajouter le `import` de `Formateur`

1. J'utilise {{% key "Ctrl+1" %}} pour générer les méthodes manquantes
    * option `add unimplemented methods`

1. Je remplis la méthode `formater` comme suit:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/polymorphisme/MonFormateur.java" 
    first-line="12"
    last-line="37"
    indent-level="1"
%}}
    ```

### Étape 05: remplir les méthdes de `MonAtelier1_2_B`

1. Je peux maintenant remplir `fournirAccepteur` comme suit:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/polymorphisme/MonAtelier1_2_B.java" 
    first-line="8"
    last-line="11"
    indent-level="1"
%}}
    ```

1. Et `fournirFormateur` comme suit:

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/polymorphisme/MonAtelier1_2_B.java" 
    first-line="13"
    last-line="16"
    indent-level="1"
%}}
    ```


### Étape 06: ajouter la méthode `main`

1. J'ouvre `MonAtelier1_2_B` et j'ajoute la méthode `main`

    ```java
{{% embed 
    src="/3c6/etape1/module2/atelier/polymorphisme/MonAtelier1_2_B.java" 
    first-line="3"
    last-line="6"
    indent-level="1"
%}}
    ```

### Étape 07: exécuter pour valider

1. J'exécute mon programme

1. Je vérifie que la validation est réussie:

    <center>
    <img src="validation.png" width="80%" />
    </center>
