---
title: "Atelier1.2: corriger coquille `atelier1_1`"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

<br>


1. Dans le projet `atelier1_1`, dans la classe `Procedure`
    * copier-coller la méthode `classeTriNaif()` afin de créer une nouvelle méthode
    * renommer cette nouvelle méthode `classeTriLibre()`
    * retirer les annotations `@Override`
