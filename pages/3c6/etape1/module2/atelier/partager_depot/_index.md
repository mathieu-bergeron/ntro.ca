---
title: "Ajustements: donner l'accès *Developer* au prof"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
* **RAISON**: l'accès *Guest* ne me permet pas au prof de cloner mon dépôt
{{</excerpt>}}
<br>
<br>

1. Sur la page du projet, cliquer sur *Project information* => *Members*

    <img class="small-figure" src="./members.png"/>

1. Sur la ligne pour *Mathieu Bergeron*, sélectionner *Developer* (colonne *Max role*)

    <img class="figure" src="./developer.png"/>
