---
title: "Correctif au dépôt Git"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{<excerpt class="note">}}
* à faire une seule fois
{{</excerpt>}}

1. Télécharger {{% download "3c6_depart.zip" "3c6_depart.zip" %}} 

1. Placer le fichier `3c6_depart.zip` à la racine de mon dépôt Git
    * (au besoin, écraser l'ancien fichier)

1. Ouvrir GitBash à la racine de mon dépôt Git
    * au besoin, glisser le chemin dans GitBash à partir du navigateur Windows

1. Extraire l'archive sur place avec l'option `-o`

    ```bash
    $ unzip -o 3c6_depart.zip
    ```

1. Ajouter les nouveaux fichiers et pousser sur GitLab

    ```bash
    $ git add .
    $ git commit -a -m"correctif"
    $ git push
    ```
