---
title: "Théorie 1.2.4: interface, classe abstraite"
weight: 1
bookHidden : true
---

{{% pageTitle %}}

<center>
<video width="50%" src="01.mp4" type="video/mp4" controls playsinline>
</center>

* On a vu que `extends` et `implements` peuvent spécifier un contrat:
    * une liste de méthodes qu'il faut implanter

* P.ex. pour `extends`:

<center>
    <img src="extends_add_unimplemented.png" width="100%"/>
</center>

* P.ex. pour `implements`:

<center>
    <img src="implements_add_unimplemented.png" width="100%"/>
</center>


* Vous avez déjà *rempli* ces contrats dans les tutoriels et les ateliers

* Maintenant, pour *créer* vos propres contrats, il faut *créer* soit:
    * une classe abstraite dans le cas de `extends`
    * une interface dans le cas de `implements`

## Interface

<center>
<video width="50%" src="02.mp4" type="video/mp4" controls playsinline>
</center>

* L'interface est le contrat le plus simple

* Il s'agit exclusivement de *signatures* de méthodes **publiques**

* L'interface ne contient **jamais** le code d'une méthode

* P.ex. voici deux interfaces:

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/Rouleur.java" first-line="" last-line="" %}}
```

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/Formateur.java" first-line="" last-line="" %}}
```

* Pour implanter l'interface `Rouleur`, il faut obligatoirement implanter la méthode `rouler`

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/MonRouleur.java" first-line="" last-line="" %}}
```


* Pour implanter l'interface `Formateur`, il faut obligatoirement implanter deux méthodes:

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/MonFormateur.java" first-line="" last-line="" %}}
```

* NOTE: le contrat concerne uniquement la *signature* et non le code de la méthode

## Classe abstraite

<center>
<video width="50%" src="03.mp4" type="video/mp4" controls playsinline>
</center>

* La classe abstraite est plus flexible

* Elle implante certaines méthodes:

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/Atelier.java" first-line="1" last-line="14" %}}
```

* Alors que d'autres sont *abstraites* et seulement la signature est spécifiée:

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/Atelier.java" first-line="16" last-line="18" %}}
```

* Au complet ça donne:

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/Atelier.java" first-line="" last-line="" %}}
```

* Quand on hérite de `Atelier` il faut obligatoirement implanter deux méthodes:

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/MonAtelier.java" first-line="" last-line="" %}}
```

* NOTE: une méthode abstraite peut être `protected`


## Impossible d'instancier une interface ou une classe abstraite

<center>
<video width="50%" src="04.mp4" type="video/mp4" controls playsinline>
</center>

* On ne peut pas créer un objet à partir d'une interface ou d'une classe abstraite

* P.ex. si on essaie d'instancier une classe abstraite:

<center>
    <img src="cannot_instantiate01.png"/>
</center>

* P.ex. si on essaie d'instancier une interface:

<center>
    <img src="cannot_instantiate02.png"/>
</center>

* Pour créer un objet qui remplit le contrat, il faut utiliser `extends` ou `implements`

## Quand utiliser une interface?

<center>
<video width="50%" src="05.mp4" type="video/mp4" controls playsinline>
</center>

* Quand on veut faire la liste des *méthodes publiques* d'une classe

* Typiquement, on utiliser une interface pour chaque thème: `rouler`, `formater`, `manger`

* Une classe peut implanter plusieurs interfaces (c-à-d plusieurs `implements`):

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/MonVehicule.java" first-line="" last-line="" %}}
```

## Interface et polymorphisme

<center>
<video width="50%" src="06.mp4" type="video/mp4" controls playsinline>
</center>

* Une interface est aussi une façon d'accéder à un comportement restreint d'un objet

* P.ex:

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/MonMain.java" first-line="" last-line="" %}}
```

* En utilisant la variable `Rouleur vehicule` (de type `Rouleur`), on dit:
    * j'ai seulement besoin des méthodes spécifiées dans `Rouleur`
    * les autres méthodes de `Vehicule` ne seront *pas accessibles*

* Notre code est alors plus général et plus lisible:
    * on a rendu explicite le contrat qu'on utilise
    * n'importe quel objet qui implante `Rouleur` est compatible avec notre code
        * (et non uniquement les objets de type `Vehicule`)
    

## Quand utiliser une classe abtraite?

<center>
<video width="50%" src="07.mp4" type="video/mp4" controls playsinline>
</center>


* Quand on veut obliger une sous-classe à redéfinir une méthode

* Typiquement, cette méthode n'a pas de sens dans la classe parent

* P.ex:

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/Vehicule.java" first-line="1" last-line="7" %}}
```


* Aucune des méthodes ci-haut n'a de sens dans `Vehicule`
    * il faut un type de véhicule en particulier (`Auto`, `Moto`) pour répondre

* Mais on veut utiliser ces méthodes dans `Vehicule`, p.ex:

```java
{{% embed src="/3c6/etape1/module2/theorie/interface/Vehicule.java" first-line="9" last-line="11" %}}
```

* Alors la classe abstraite *force* les sous-classes à avoir ces méthodes

* C'est le contrat de la classe abstraite

## Pourquoi ne pas toujours utiliser une classe abstraite?

<center>
<video width="50%" src="08.mp4" type="video/mp4" controls playsinline>
</center>

* Parce que ce n'est pas permis en Java ;-)

* Java ne supporte pas l'héritage multiple (c-à-d plusieurs `extends`)
