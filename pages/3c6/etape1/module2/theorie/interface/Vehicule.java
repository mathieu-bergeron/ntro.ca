public abstract class Vehicule extends Object implements Rouleur, Formateur {

	protected abstract double consommationLitresParKilometre();

	protected abstract String nomVehicule();

	protected abstract boolean siNomFeminin();

	private double litresEssenceConsomes() {
		return totalKilometres * consommationLitresParKilometre();
	}
}
