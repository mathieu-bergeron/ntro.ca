public class Vehicule {

	private double totalKilometres = 0;
	
	public void rouler(double kilometres) {
		totalKilometres += kilometres;
	}

	private double litresEssenceConsomes() {
	    double litresConsomes = 0;

	    if(this instanceof Camion){ // instanceof

	        double chargementEnKilos = ((Camion) this).getChargementEnKilos(); // getter

	        double consommationDeBase = ((Camion) this).getConsommationDeBase(); // getter

		    double consommationSelonChargement = (1 + chargementEnKilos / 1E6) * consommationDeBase;

	        litresConsomes = totalKilometres * consommationSelonChargement;

	    }else{

	        litresConsomes = totalKilometres * consommationLitresParKilometre();
	    }

		return litresConsomes;
	}
}
