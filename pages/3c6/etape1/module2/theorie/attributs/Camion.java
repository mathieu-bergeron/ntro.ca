public class Camion extends Vehicule {
	
	private double consommationDeBase = 14;
	private double chargementEnKilos = 0;

	public void accepterChargement(double chargementEnKilos) {
		this.chargementEnKilos = chargementEnKilos;
	}

	@Override
	protected double consommationLitresParKilometre() {
		return calculerConsommationSelonChargement();
	}

	private double calculerConsommationSelonChargement() {
		return (1 + chargementEnKilos / 1E6) * consommationDeBase;
	}
}
