public class Camion extends Vehicule {
	
	private double consommationDeBase = 14;
	private double chargementEnTonnes = 0; // TONNES

	public void accepterChargement(double chargementEnKilos) {
		this.chargementEnTonnes = chargementEnKilos / 1E3;  // TONNES
	}

	@Override
	protected double consommationLitresParKilometre() {
		return calculerConsommationSelonChargement();
	}

	private double calculerConsommationSelonChargement() {
		return (1 + chargementEnTonnes / 1E3) * consommationDeBase; // TONNES
	}
}
