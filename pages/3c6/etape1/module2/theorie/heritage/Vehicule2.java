public class Vehicule extends Object {
	
	public double consommationLitresParKilometre() {
		return 0.0;
	}
	
	public double litresEssenceConsomes(double kilometres) {
		return kilometres * consommationLitresParKilometre();
	}

	public int nombreDeRoues() {
		return 0;
	}

}
