---
title: "Théorie 1.2.5: constructeurs et `super`"
weight: 1
bookHidden : true
---

{{% pageTitle %}}

<center>
<video width="50%" src="01.mp4" type="video/mp4" controls playsinline>
</center>

* Un constructeur est une méthode spéciale appelée à la création d'un objet

* Le mot clé `super` permet d'accéder à la superclasse

## Constructeur

<center>
<video width="50%" src="02.mp4" type="video/mp4" controls playsinline>
</center>

* À la création d'un nouvel objet:

```java
{{% embed 
    src="/3c6/etape1/module2/theorie/constructeur/Principal.java" 
    first-line="3"
    last-line="6"
    %}}
```

* Un constructeur est appelé:

```java
{{% embed 
    src="/3c6/etape1/module2/theorie/constructeur/Vehicule.java" 
    first-line="1"
    last-line="7"
    %}}
```

* Typiquement, on veut créer un objet en spécifiant certaines valeurs:

```java
{{% embed 
    src="/3c6/etape1/module2/theorie/constructeur/Principal.java" 
    first-line="8"
    last-line="11"
    %}}
```

* Dans ce cas, le constructeur doit avoir un argument:

```java
{{% embed 
    src="/3c6/etape1/module2/theorie/constructeur/Vehicule.java" 
    first-line="9"
    last-line="11"
    %}}
```


## Constructeur et classe parent

<center>
<video width="50%" src="03.mp4" type="video/mp4" controls playsinline>
</center>


* Il faut toujours appeler le constructeur de la classe parent avec le mot clé `super`

```java
{{% embed 
    src="/3c6/etape1/module2/theorie/constructeur/Auto.java" 
    first-line=""
    last-line=""
    %}}
```


## Pourquoi utiliser le mot clé `super`?

<center>
<video width="50%" src="04.mp4" type="video/mp4" controls playsinline>
</center>

* Dans chaque constructeur d'une sous-classe (c'est obligé!)

* Mais aussi pour appeler explicitement une méthode de la classe parent:

```java
{{% embed 
    src="/3c6/etape1/module2/theorie/constructeur/Auto_super_simple.java" 
    first-line=""
    last-line=""
    %}}
```

* En particulier, c'est utile pour modifier le comportement d'une méthode parent:

```java
{{% embed 
    src="/3c6/etape1/module2/theorie/constructeur/Auto_super.java" 
    first-line=""
    last-line=""
    %}}
```

* C'est-à-dire:
    * on redéfini la méthode `formater` qui est implantée dans la classe `Vehicule`
    * dans `Auto`, le comportement de cette méthode devient:
        * on prend la valeur de `formater` que `Vehicule` aurait retournée
        * main on ajoute `"[AUTO]"` avant de retourner

