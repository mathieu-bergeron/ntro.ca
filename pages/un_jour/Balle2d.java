public class Balle2d extends ObjetPong2d {
    
    private static final double LARGEUR = 10;
    private static final double HAUTEUR = 10;
    
    private static final double EPSILON = 1;
    
    private transient Media sonPoc = new Media(Balle2d.class.getResource("/sons/poc.wav").toString());
    private transient Image image = new Image("/images/balle.png");
    
    private transient Palette2d paletteGauche;
    private transient Palette2d paletteDroite;

    public static List<Obstacle2d> obstacles = new ArrayList<>();
    private static int prochainIdObstacle = 0;

    public Palette2d getPaletteGauche() {
        return paletteGauche;
    }

    public void setPaletteGauche(Palette2d paletteGauche) {
        this.paletteGauche = paletteGauche;
    }

    public Palette2d getPaletteDroite() {
        return paletteDroite;
    }

    public void setPaletteDroite(Palette2d paletteDroite) {
        this.paletteDroite = paletteDroite;
    }

    public Balle2d() {
        super();
    }

    public Balle2d(Palette2d paletteGauche, Palette2d paletteDroite) {
        super();
        
        setPaletteGauche(paletteGauche);
        setPaletteDroite(paletteDroite);
    }

    @Override
    public void initialize() {
        setWidth(LARGEUR);
        setHeight(HAUTEUR);
        
        choisirEtatInitial();
    }

    public void choisirEtatInitial() {

        topLeftX = 160 + Ntro.random().nextInt(320);
        speedX = 100 + Ntro.random().nextInt(20);
        
        if(topLeftX > 320) {
            speedX = -speedX;
        }

        topLeftY = 5 + Ntro.random().nextInt(200);
        speedY = 100 - Ntro.random().nextInt(20);

    }

    @Override
    public void onTimePasses(double secondsElapsed) {
        super.onTimePasses(secondsElapsed);
        
        if(balleFrappeMurGauche()) {

            //balleRebondiSurMurGauche();
            ajouterPoint(Position.DROITE);
            
        } else if(balleFrappeMurDroit()) {

            //balleRebondiSurMurDroit();
            ajouterPoint(Position.GAUCHE);

        }else if(balleFrappePalette(paletteGauche)) {
            
            balleRebondiSurObjet(paletteGauche);

        }else if(balleFrappePalette(paletteDroite)) {
            
            balleRebondiSurObjet(paletteDroite);

        }else if(balleFrappePlafond()) {
            
            balleRebondiSurPlafond();
            
        }else if(balleFrappePlancher()) {

            balleRebondiSurPlancher();
        }
        
        Obstacle2d obstacle = obstacleFrappeParBalle();
        
        if(obstacle != null) {
            balleRebondiSurObjet(obstacle);
            //retirerObstacle(obstacle);
        }
    }
    

    private void retirerObstacle(Obstacle2d obstacle) {
        obstacles.remove(obstacle);
        world2d.removeObject(obstacle.id());
    }

    private Obstacle2d obstacleFrappeParBalle() {
        Obstacle2d obstacle = null;
        
        for(Obstacle2d candidat : obstacles) {
            if(collidesWith(candidat)) {
                obstacle = candidat;
                break;
            }
        }

        return obstacle;
    }

    private void ajouterPoint(Position position) {
        Ntro.newMessage(MsgAjouterPoint.class)
            .setPosition(position)
            .setMonde2d(this.getWorld2d())
            .send();
        
        choisirEtatInitial();
    }

    private void balleRebondiSurPlancher() {
        jouerSonPoc();

        setTopLeftY(getWorld2d().getHeight() - this.getHeight() - EPSILON);
        setSpeedY(-getSpeedY());
    }

    private void jouerSonPoc() {
        //new MediaPlayer(sonPoc).play();
    }

    private void balleRebondiSurPlafond() {
        jouerSonPoc();

        topLeftY = 0 + EPSILON;
        speedY = -getSpeedY();
    }

    private void balleRebondiSurMurDroit() {
        jouerSonPoc();

        topLeftX = world2d.width - this.width - EPSILON;
        speedX = -speedX;
    }

    private void balleRebondiSurMurGauche() {
        jouerSonPoc();

        topLeftX =0 + EPSILON;
        speedX = -speedX;
    }

    private boolean balleFrappePlancher() {
        return topLeftY + height >= world2d.height;
    }

    private boolean balleFrappePlafond() {
        return topLeftY <= 0;
    }

    private boolean balleFrappeMurDroit() {
        return topLeftX + width >= world2d.width;
    }

    private boolean balleFrappeMurGauche() {
        return topLeftX <= 0;
    }

    private boolean balleFrappePalette(Palette2d palette) {
        return collidesWith(palette);
    }

    private void balleRebondiSurObjet(ObjetPong2d objet2d) {
        jouerSonPoc();
        
        if(topLeftX < objet2d.topLeftX) {

            topLeftX = objet2d.topLeftX - width - EPSILON;

        }else {

            topLeftX = objet2d.topLeftX + objet2d.width + EPSILON;

        }
        
        speedX = -speedX;

    }

    @Override
    public void drawOnWorld(GraphicsContext gc) {
        gc.drawImage(image,
                     topLeftX,
                     topLeftY,
                     width,
                     height);
    }

    @Override
    protected boolean onMouseEvent(World2dMouseEventFx mouseEvent) {
        return false;
    }

    @Override
    public String id() {
        return "balle";

    }

    public static void ajouterObstacle(MondePong2d monde2d, double topLeftX, double topLeftY) {
        Obstacle2d obstacle = new Obstacle2d(++prochainIdObstacle, topLeftX, topLeftY);
        
        obstacles.add(obstacle);

        monde2d.addObject2d(obstacle);

    }
}
