---
weight: 200
title: "Atelier d'un jour"
bookHidden: false
bookCollapseSection: true
---

{{% pageTitle %}}

## Équipe d'animation 2025

* 8h00-9h45 &nbsp;&nbsp;  Charbel, Colin, Marwan
* 9h50-11h35 &nbsp;&nbsp; Nazim + effort de groupe
* 11h40-13h25 &nbsp;&nbsp; Sirine, Gregory

## Objectifs

1. Compléter l'implantation d'un jeu Pong

    {{% animation "/un_jour/pong.mp4" %}}

## Exécuter le projet une première fois

1. Télécharger {{% download "un_jour.zip" "un_jour.zip" %}}

1. Ouvrir le projet

1. Exécuter le projet... oups, il y a ni balle,  ni palette!

    <img src="un_jour01.png"/>

## Ajouter la balle

1. Ouvrir le fichier `MondePong2d` et ajouter le code suivant:

    ```java
    {{% embed src="MondePong2d01.java" indent-level="1" %}}
    ```

1. Sauvegarder et exécuter le projet

1. On a maintenant un balle!

    {{% animation "/un_jour/un_jour02.mp4" %}}

## Ajouter les palettes

1. Dans `MondePong2d`, ajouter le code suivant:

    ```java
    {{% embed src="MondePong2d02.java" indent-level="1" %}}

    ```

1. Sauvegarder et exécuter le projet

1. On a maintenant les palettes, mais la balle passe à travers

    {{% animation "/un_jour/un_jour03.mp4" %}}

    * NOTE: déplacer les palettes avec {{% key "w" %}}/{{% key "s" %}} et {{% key "↑" %}}/{{% key "↓" %}}

## Faire rebondir la balle sur les palettes

1. Ouvrir `Balle2d`, ajouter le code suivant:

    ```java
    {{% embed src="Balle2d01.java" indent-level="1" %}}

    ```

1. Sauvegarder et exécuter le projet

1. La balle devrait maintenant rebondir sur les palettes

    {{% animation "/un_jour/un_jour04.mp4" %}}

## Défi 1: grossir la balle et changer son image

1. Il faut ajouter une image dans le répertoire `pong/src/main/resources/images`

1. Il faut modifier un peu le code de `Balle2d`

    {{% animation "/un_jour/un_jour05.mp4" %}}

## Défi 2: changer la couleur des palettes

1. Il faut modifier un peu le code de `Palette2d`

    ```java
    {{% embed src="Palette2d01.java" indent-level="1" %}}
    ```

1. Il faut trouver ici comment changer la couleur de remplissage (fill)

    * https://docs.oracle.com/javase/8/javafx/api/javafx/scene/canvas/GraphicsContext.html
    * TRUC: chercher pour `setFill`

    {{% animation "/un_jour/un_jour06.mp4" %}}

## Défi 3: changer la couleur des palettes selon leur emplacement à l'écran

1. Il faut modifier un peu le code de `Palette2d`

    ```java
    {{% embed src="Palette2d01.java" indent-level="1" %}}
    ```

1. Il faut trouver comment créer une couleur avec des valeurs RGB

    * https://docs.oracle.com/javase/8/javafx/api/javafx/scene/paint/Color.html
    * BUT: faire varier les valeurs RGB selon la valeur de `topLeftY`

    {{% animation "/un_jour/un_jour07.mp4" %}}

## Finaliser le jeu: compter les points

1. Modifier le code de `Balle2d` 

    ```java
    {{% embed src="Balle2d02.java" indent-level="1" %}}
    ```

1. Re-exécuter le projet. Le pointage devrait fonctionner

    {{% animation "/un_jour/un_jour08.mp4" %}}




    



