public class MondePong2d extends World2dFx {
    
    public static final double LARGEUR_MONDE = 640;
    public static final double HAUTEUR_MONDE = 360;
    
    private Palette2d paletteGauche;
    private Palette2d paletteDroite;
    private Balle2d balle;
    

    public Palette2d getPaletteGauche() {
        return paletteGauche;
    }

    public void setPaletteGauche(Palette2d paletteGauche) {
        this.paletteGauche = paletteGauche;
    }

    public Palette2d getPaletteDroite() {
        return paletteDroite;
    }

    public void setPaletteDroite(Palette2d paletteDroite) {
        this.paletteDroite = paletteDroite;
    }

    public Balle2d getBalle() {
        return balle;
    }

    public void setBalle(Balle2d balle) {
        this.balle = balle;
    }

    @Override
    protected void initialize() {
        setWidth(LARGEUR_MONDE);
        setHeight(HAUTEUR_MONDE);

        paletteGauche = new Palette2d("gauche", 30);
        paletteDroite = new Palette2d("droite", LARGEUR_MONDE - 40);
        balle = new Balle2d(paletteGauche, paletteDroite);

        addObject2d(balle);
        
        addObject2d(paletteGauche);
        addObject2d(paletteDroite);
        
        
        for(int i = 0; i < 100; i++) {
            addObject2d(new Balle2d(paletteGauche, paletteDroite));
        }
    }

    @Override
    public void drawOn(ResizableWorld2dCanvasFx canvas) {
        canvas.drawOnWorld(gc -> {
            
            dessinerTerrain(gc);
        });

        super.drawOn(canvas);
    }

    private void dessinerTerrain(GraphicsContext gc) {
        
        gc.save();
        gc.setStroke(Color.LIGHTGREY);
        gc.setLineWidth(1);

        dessinerBordureTerrain(gc);
        dessinerLigneMediane(gc);

        gc.restore();
    }

    private void dessinerLigneMediane(GraphicsContext gc) {
        double ligneX = width / 2;
        double nombreCases = 64;
        double tailleCase = height / nombreCases;

        for(int i = 0; i < nombreCases; i++) {
            if(i%2 == 0) {
                double ligneY = i*tailleCase;
                gc.strokeLine(ligneX, 
                              ligneY, 
                              ligneX, 
                              ligneY + tailleCase);
            }
        }
    }

    private void dessinerBordureTerrain(GraphicsContext gc) {
        gc.strokeRect(0, 0, width, height);
    }
    
    public void appliquerActionJoueur(Position cadran, Action action) {
        Palette2d palette = paletteParPosition(cadran);

        appliquerActionJoueur(palette, action);
    }

    private void appliquerActionJoueur(Palette2d palette, Action action) {

        switch(action) {

            case MONTER:
                palette.monter();
                break;

            case DESCENDRE:
                palette.descendre();
                break;

            case ARRETER:
                palette.arreter();
        }
    }

    private Palette2d paletteParPosition(Position cadran) {
        Palette2d palette;

        switch(cadran) {

            case GAUCHE:
            default:
                palette = paletteGauche;
                break;

            case DROITE:
                palette = paletteDroite;
                break;
            }

        return palette;
    }

    @Override
    protected void onMouseEventNotConsumed(World2dMouseEventFx mouseEvent) {
        double x = mouseEvent.worldX();
        double y = mouseEvent.worldY();
        
        double topLeftX = x - Obstacle2d.LARGEUR / 2;
        double topLeftY = y - Obstacle2d.HAUTEUR / 2;
        
        Ntro.logger().info(String.format("clic: (%.0f, %.0f)", topLeftX, topLeftY));
        
        Balle2d.ajouterObstacle(this, topLeftX, topLeftY);
    }
}
