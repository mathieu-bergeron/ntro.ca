package pong.commun.monde2d;

import ca.ntro.app.fx.controls.World2dMouseEventFx;
import javafx.scene.canvas.GraphicsContext;

public class Palette2d extends ObjetPong2d {
	
	private static final double VITESSE_PALETTE = 200;
	
    private transient double minTopLeftY;
    private transient double maxTopLeftY;
    
    private transient String id;

	public Palette2d() {
		super();
	}

	public Palette2d(String id, double topLeftX) {
		super();
		
		this.id = id;

		setTopLeftX(topLeftX);
	}

	@Override
	public void initialize() {
		width = 10;
		height = 100;
		
		topLeftY = world2d.height/2 - height/2;
		
		minTopLeftY = 0 - height / 2;
		maxTopLeftY = world2d.height - height / 2;
	}

	public void onTimePasses(double secondsElapsed) {
		super.onTimePasses(secondsElapsed);
		
		if(topLeftY < minTopLeftY) {
			topLeftY = minTopLeftY;
		}
		
		if(topLeftY > maxTopLeftY) {
			topLeftY = maxTopLeftY;
		}
	}

	@Override
	public void drawOnWorld(GraphicsContext gc) {
		gc.save();
		
		gc.fillRect(topLeftX,
					topLeftY,
					width, 
					height);
		
		gc.restore();
	}

	public void monter() {
		if(speedY >= 0) {
			speedY = -VITESSE_PALETTE;
		}
	}

	public void descendre() {
		if(speedY <= 0) {
			speedY = +VITESSE_PALETTE;
		}
	}

	public void arreter() {
		speedY = 0;
	}


	@Override
	protected boolean onMouseEvent(World2dMouseEventFx mouseEvent) {
		return false;
	}

	@Override
	public String id() {
		return id;
	}

}
