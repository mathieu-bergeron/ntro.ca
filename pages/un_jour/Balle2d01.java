public class Balle2d extends ObjetPong2d {
    
    @Override
    public void onTimePasses(double secondsElapsed) {
        super.onTimePasses(secondsElapsed);
        
        if(balleFrappeMurGauche()) {

            balleRebondiSurMurGauche();
            
        } else if(balleFrappeMurDroit()) {

            balleRebondiSurMurDroit();

        // ajouter
        }else if(balleFrappePalette(paletteGauche)) {
            
            // ajouter
            balleRebondiSurObjet(paletteGauche);

        // ajouter
        }else if(balleFrappePalette(paletteDroite)) {
            
            // ajouter
            balleRebondiSurObjet(paletteDroite);

        }else if(balleFrappePlafond()) {
            
            balleRebondiSurPlafond();
            
        }else if(balleFrappePlancher()) {

            balleRebondiSurPlancher();
        }
    }
