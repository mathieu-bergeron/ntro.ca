public class MondePong2d extends World2dFx {

    // ...
    
    @Override
    protected void initialize() {
        setWidth(LARGEUR_MONDE);
        setHeight(HAUTEUR_MONDE);

        paletteGauche = new Palette2d("gauche", 30);
        paletteDroite = new Palette2d("droite", LARGEUR_MONDE - 40);
        balle = new Balle2d(paletteGauche, paletteDroite);

        addObject2d(balle);

        // ajouter
        addObject2d(paletteGauche);
        addObject2d(paletteDroite);
    }
