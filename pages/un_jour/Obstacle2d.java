package pong.commun.monde2d;

import ca.ntro.app.fx.controls.World2dMouseEventFx;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Obstacle2d extends ObjetPong2d {
	
	public static final double LARGEUR = 5;
	public static final double HAUTEUR = 20;
	
	private transient String id;

	public Obstacle2d() {
		super();
	}

	public Obstacle2d(int id, double topLeftX, double topLeftY) {
		super();
		
		this.id = String.valueOf(id);
		
		this.topLeftX = topLeftX;
		this.topLeftY = topLeftY;

	}

	@Override
	public void initialize() {
		setWidth(LARGEUR);
		setHeight(HAUTEUR);
	}

	@Override
	public String id() {
		return id;
	}

	@Override
	public void drawOnWorld(GraphicsContext gc) {
		gc.setFill(Color.DARKORANGE);
		gc.fillRect(topLeftX, 
				    topLeftY, 
				    width, 
				    height);
		
	}

	@Override
	protected boolean onMouseEvent(World2dMouseEventFx mouseEvent) {
		return false;
	}

}
