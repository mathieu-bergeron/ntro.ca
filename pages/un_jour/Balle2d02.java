public class Balle2d extends ObjetPong2d {
    
    @Override
    public void onTimePasses(double secondsElapsed) {
        super.onTimePasses(secondsElapsed);
        
        if(balleFrappeMurGauche()) {

            // effacer
            // balleRebondiSurMurGauche();

            // remplacer par
            ajouterPoint(Position.DROITE);
            choisirEtatInitial();
            
        } else if(balleFrappeMurDroit()) {

            // effacer
            // balleRebondiSurMurDroit();

            // remplacer par
            ajouterPoint(Position.GAUCHE);
            choisirEtatInitial();


        }else if(balleFrappePalette(paletteGauche)) {
            
            balleRebondiSurObjet(paletteGauche);

        }else if(balleFrappePalette(paletteDroite)) {
            
            balleRebondiSurObjet(paletteDroite);

        }else if(balleFrappePlafond()) {
            
            balleRebondiSurPlafond();
            
        }else if(balleFrappePlancher()) {

            balleRebondiSurPlancher();
        }
    }
