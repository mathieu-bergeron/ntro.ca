---
title: "F73: Génie civil"
weight: 20
# bookFlatSection: false
# bookToc: true
# bookHidden: false
bookCollapseSection: true
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}

## {{% link "./presentation" "Présentation du cours" %}}

## Liens rapides


1. Dépannage informatique (CCTI):
    * 450-975-6400
    * ccti@cmontmorency.qc.ca
    * https://ccti.cmontmorency.qc.ca
    
1. <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344">Page Moodle</a>

1. <a href="https://aiguilleur.ca/file_d_attente/mathieu.bergeron">File d'attente pour questions en classe</a>

1. {{% download "./presentation/420-F73-MO-A24-AP-MB.pdf" "Plan de cours" %}}


1. {{% link "./semaines/01/raccourcis/" "Astuces et raccourcis"  %}}


