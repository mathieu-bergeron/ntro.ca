---
title: "01: Windows et Word"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 10
---

{{% pageTitle %}}

* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-1">Semaine 01 sur Moodle</a>

* {{% link "./raccourcis/" "Astuces et raccourcis"  %}}


## À faire aujourd'hui

1. se connecter à mon courriel du Collège
    * même usager/mdp que pour l'ordi du Collège
1. se connecter la file d'attente du cours avec Firefox
    * visiter https://aiguilleur.ca/file_d_attente/mathieu.bergeron
    * s'incrire
    * saisir le code reçu par courriel
    * ajouter un mot de passe
1. se connecter à OneDrive
    * même usager/mdp que pour l'ordi du Collège
1. sauvegarder un document Word sur mon OneDrive
1. réaliser <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=410074">Exercices-Créer un document</a> disponible sur Moodle




