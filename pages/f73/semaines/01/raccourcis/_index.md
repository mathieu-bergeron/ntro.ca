---
title: "Astuces et raccourcis"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 10
---

{{% pageTitle %}}

## Windows

<table>
<tr>
<th>
Afin de...
</th>
<th>
Faire
</th>
</tr>

<tr>
<td>
démarrer une application
</td>
<td>

* {{% key "⊞" %}}
* commencer à saisir le nom de l'application
* {{% key "Entrée" %}} 

</td>
</tr>

<tr>
<td>

déplacer les fenêtres


</td>
<td>

* sélectionner la fenêtre
* faire:
    * {{% key "⊞+→" %}}
    * {{% key "⊞+←" %}}
    * {{% key "⊞+↑" %}}
    * {{% key "⊞+↓" %}}

OU

* glisser la fenêtre vers le côté ou le haut de l'écran

</td>
</tr>

<tr>
<td>

naviguer d'une fenêtre à l'autre


</td>
<td>


* {{% key "Alt+Tab" %}} 

</td>
</tr>



<tr>
<td>

naviguer rapidement dans les dossiers


</td>
<td>


* ouvrir l'explorateur Windows
* commencer à taper le nom du dossier. 
* faire {{% key "Entrée" %}} pour entrer dans le dossier

</td>
</tr>


</table>


## Application (Word, Excel, ...)

<table>
<tr>
<th>
Afin de...
</th>
<th>
Faire
</th>
</tr>

<tr>
<td>

bouger rapidement le curseur


</td>
<td>

* {{% key "Ctrl+→" %}}
* {{% key "Ctrl+←" %}} 

</td>
</tr>

<tr>
<td>

effacer rapidement du texte



</td>
<td>

* {{% key "Ctrl+Backspace" %}} (efface vers la gauche)
* {{% key "Ctrl+Del" %}} (efface vers la droite)

</td>
</tr>

<tr>
<td>

copier-coller


</td>
<td>

* sélectionner
* {{% key "Ctrl+C" %}} pour copier
* placer le curseur ailleurs
* {{% key "Ctrl+V" %}}  pour coller

</td>
</tr>

<tr>
<td>

annuler / revenir en arrière

</td>
<td>

* {{% key "Ctrl+Z" %}} 

</td>
</tr>

<tr>
<td>

refaire / répéter

</td>
<td>

* {{% key "Ctrl+Y" %}} 

</td>
</tr>


<tr>
<td>

sélectionner tout



</td>
<td>

* {{% key "Ctrl+A" %}}

</td>
</tr>

<tr>
<td>

sauvegarder le document


</td>
<td>

* {{% key "Ctrl+S" %}}

</td>
</tr>








<tr>
<td>

afficher l'aide

</td>
<td>

* {{% key "F1" %}}

</td>
</tr>



</table>



## Navigateur Web

<table>
<tr>
<th>
Afin de...
</th>
<th>
Faire
</th>
</tr>

<tr>
<td>

ouvrir une page Web dans un nouvel onglet

</td>
<td>

* clic-droit => ouvrir le lien dans un nouvel onglet 

OU

* clic du milieu

</td>
</tr>

<tr>
<td>

ré-ouvrir la dernière page Web qu'on a fermée


</td>
<td>

* {{% key "Ctrl+Shift+T" %}} 

</td>
</tr>




</table>










