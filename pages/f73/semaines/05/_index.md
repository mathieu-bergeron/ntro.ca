---
title: "05: PowerPoint"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 50
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-5">Semaine 05 sur Moodle</a>

## Démonstrations PowerPoint

1.  Mode plan
    * {{% key "Entrée" %}} ou {{% key "Ctrl+Entrée" %}} pour créer une nouvelle diapo
    * {{% key "Tab" %}} pour passer au niveau inférieur
    * {{% key "Maj+Tab" %}} pour revenir au niveau supérieur
    * cliquer dans la diapo pour accéder aux sections

    <img src="mode_plan01.png"/>


1. Types de diapositives

    <img src="nouvelle_diapo.png"/>


1. Masque des diapositives (créer son propre type de diapositive)

    <img src="masque.png"/>

1. Volet animation

    * Démarrer en cliquant Vs démarrer avec le précédent

    <img src="volet_animation.png"/>

1. SmartArt




## Travail pratique PowerPoint (15%)

* {{% link "./tp02" "énoncé" %}}
* date limite: 4 octobre **avant le cours**

