---
title: "TP02: PowerPoint (10%)"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 50
---

{{% pageTitle %}}


## Remise

Sur <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=410083">Moodle</a>


## Travail à effectuer

1. Préparer une présentation PowerPoint d'environs 6 à 10 diapositives

1. Sujet de votre choix

    * p.ex. un loisir que vous aimez: sport, jeu, livre, série télé, etc.
    * éviter les sujets trop personnels (p.ex. mon chat)
    * le lecteur doit apprendre quelque chose

1. Varier les techniques:

    * Application de thème.   
    * Animations.   
    * Transitions.   
    * Tableau.   
    * SmartArt.   
    * Images.   
    * Insertion de formes.   
    * Pied de page contenant votre nom.   
    * Numérotation de diapositives.   
    * Écriture du texte sous la forme d’une liste à puces (pas de bloc de textes longs).   

## Grille de correction


<table>
<tr>
<th>
Critère
</th>
<th>
Points
</th>
</tr>

<tr>
<td>

Utilisation d'au moins 4 techniques (image, animation, transition, etc.)

</td>
<td>
40pts
</td>

</tr>

<tr>
<td>

Structure de la présentation

* contenu lisible en mode plan
* utilisation de différents types de diapos

</td>
<td>
20pts
</td>

</tr>

<tr>
<td>

Apparence de la présentation

* originalité dans les choix de polices, de couleurs, etc.
* cohésions des différents éléments (p.ex. éviter d'utiliser 20 polices différentes)

</td>
<td>
20pts
</td>

</tr>


<tr>
<td>

Qualité du contenu

* Une présentation structurée, soignée et cohérente
* Des textes de qualité
* Maintien d'un fil conducteur

</td>
<td>
20pts
</td>

</tr>



</table>


### Guide pour l'évaluation


<table>

<tr>

<th>
&leq;50%
</th>

<th>
~65%
</th>

<th>
~85%
</th>

<th>
&geq;95%
</th>


</tr>

<tr>

<th>
Compétence non-observée
</th>

<th>
Compétence acceptable
</th>

<th>
Compétence atteinte
</th>

<th>
Compétence dépassée
</th>


</tr>

<tr>

<td>

Problème de remise

Travail à peine débuté


</td>

<td>

Travail complet, mais avec des lacunes (p.ex. n'utilisant pas de techniques du cours)

</td>

<td>

Travail complet, contenu pertinent et sans erreurs, techniques du cours biens utilisées.

</td>

<td>

Dépasse les attentes

</td>

</tr>



</table>



