---
title: "11: Examen Excel (20%)"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 110
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-11">Semaine 11 sur Moodle</a>

## Description Examen Excel

* Individuel

* Durée: 2h

* 3 exercices à faire sur Excel

    * remise sur Moodle

* Documentation permise: 
    
    * matériel du cours
    * aide d'Excel
    * recherche Web

* Rappels:

    * Ce qui est **important** ce sont **les techniques utilisées,** plus que le   résultat final.   

    * Une **formule** commence par `=`, p.ex. `=B13*C56`

    * Au besoin, utiliser une **référence absolue** (p.ex. `$H$16`)

    * Au besoin, utiliser une **fonction** acceptant une plage de valeurs (p.ex. `=MIN(A5:H18)`)

* Astuces:
    
    * s'assurer de télécharger les fichiers

        <img src="telecharger.png"/>

    * s'assurer d'ouvrir Excel (ne pas travailler dans le navigateur)

        <center>
        <img class="max-width-75" src="excel_lourd.png"/>
        </center>

    * au besoin, activer la modification

        <img src="activer_modification.png"/>

    * enregistrer sous sur OneDrive

        <center>
        <img class="max-width-75" src="enregistrer_onedrive.png"/>
        </center>

    * activer l'option de sauvegarde automatique

        <center>
        <img src="enregistrement_automatique01.png"/>
        </center>


