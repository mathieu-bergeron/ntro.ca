---
title: "Exercice Word: tableaux"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 30
---

{{% pageTitle %}}

## Créer un tableau

1.  Démarrez Word et ouvrez le fichier {{% download "F-3.docx" "F-3.docx" %}} et enregistrez-le sous le nom de `Rapport Nouvelle-Zélande.docx`

1.  Placez le curseur dans la ligne vierge au-dessus de la ligne qui se termine par «… pour l’été 2016.» au bas de la page 2

1.  Insérez un tableau comptant trois lignes et quatre colonnes

1.  Entrez les données suivantes dans le nouveau tableau

    <table class="f73">
    <tr>
    <th>
        Nom de l’excursion 
    </th>
    <th>
        Début 
    </th>
    <th>
        Fin 
    </th>
    <th>
        Prix 
    </th>
    </tr>

    <tr>
    <td>
        Parcs nationaux 
    </td>
    <td>
        15 juin 
    </td>
    <td>
        22 juin 
    </td>
    <td>
        1499 $ 
    </td>
    </tr>

    <tr>
    <td>
        Balades sur l’île du Sud 
    </td>
    <td>
        1er juillet 
    </td>
    <td>
        8 juillet 
    </td>
    <td>
        1199 $ 
    </td>
    </tr>
    </table>



1.  Enregistrez les modifications

## Insérer et supprimer des colonnes et des lignes dans un tableau 

1.  Insérez une nouvelle ligne au bas du tableau pour qu’elle devienne la dernière

1.  Entrez les données suivantes dans la nouvelle ligne

<table class="f73">
<tr>
<td>
        Aventures Kiwi 
</td>
<td>
        8 juillet 
</td>
<td>
        15 juillet 
</td>
<td>
        2499 $ 
</td>
</tr>
</table>

1.  Insérez une nouvelle ligne *en dessous de la première ligne* et tapez les données suivantes

<table class="f73">
<tr>
<td>
        Voir Wellington 
</td>
<td>
        8 juin 
</td>
<td>
        15 juin 
</td>
<td>
        1999 $ 
</td>
</tr>
</table>


1.  Supprimez la ligne qui commence par `Ballades sur l’île de Sud`

1.  Insérez une nouvelle colonne à droite de la colonne `Nom de l’excursion`

1.  Tapez les données suivantes dans la nouvelle colonne

    <table class="f73">
    <tr>
    <td>
        Type d’excursion 
    </td>
    </tr>
    <tr>
    <td>
        Ainés 
    </td>
    </tr>
    <tr>
    <td>
        Famille 
    </td>
    </tr>
    <tr>
    <td>
        Randonnée et kayak 
    </td>
    </tr>
    </table>


1.  Enregistrez les modifications

##  Mettre en forme un tableau

1. On vise la mise en forme suivante

    <img src="resultat.png"/>

1. Commencer par appliquer un style automatique

1. Augmentez la largeur de la 2<sup>ième</sup> colonne pour que « Type d’excursion » et « Randonnée et kayak » apparaissent sur une seule ligne

1. Modifier les bordures
    * (onglet «création de tableau», options avancées de bordures et trames)

1. S'assurer de centrer le texte horizontalement

1. S'assurer de centrer le texte verticalement
    * (onglet «mise en page» du tableau)

1. S'assurer d'utiliser un interligne simple 
    * (options avancés de paragraphe)

1. Enregistrez et fermez votre document

## Remise
  
Remettre le document sur <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=410078">Moodle</a>
   

