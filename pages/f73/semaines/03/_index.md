---
title: "03: Word"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 30
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-3">Semaine 03 sur Moodle</a>

## Démonstrations

1. Le document a une structure cachée (aide à la voir: <img src="afficher_symboles.png" style="height:1.5rem;"/>)
    * titres
    * paragraphes
    * caractères (police)
<br>
<br>


1. Éléments spéciaux
    * onglet insertion
    * affichés en gris quand le curseur y touche <img src="gris.png"/>
    * gérés par Word (p.ex. la date ci-haut sera mis à jour)
<br>
<br>

1. En-têtes
    <img src="mode_entete.png"/>
    <br>
    <br>

1. Menus contextuels (clic-droit)
<br>
<br>

1. Chercher les options avancées
    * flèches par en bas
    <img src="options_avancees01.png"/>
    <img src="options_avancees02.png"/>
    <img src="options_avancees03.png"/>
    <img src="options_avancees04.png"/>
    * points 
    <img src="options_avancees05.png"/>
    <img src="options_avancees06.png"/>
    <br>
    <br>

1. Onglets spécifiques aux tableaux

    <img src="onglets_tableau.png"/>
    <br>
    <br>

1. Astuce: tableau invisible pour mise en page avancée
<br>
<br>

1. Chercher dans l'aide de Word


## Exercices à effectuer

* {{% link "./exercice_insertion" "Insertion d'éléments spéciaux" %}}
* {{% link "./exercice_tableaux" "Tableaux" %}}

