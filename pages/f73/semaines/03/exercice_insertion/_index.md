---
title: "Exercice Word: insertion d'éléments spéciaux"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 30
---

{{% pageTitle %}}

## Ajouter des bordures et une trame de fond

1.  Démarrez Word, ouvrez le fichier {{% download "F-2.docx" "F-2.docx" %}} et enregistrez-le sous le nom `Infolettre Multi-Facettes.docx`

1. On vise un résultat comme ceci

    <center>
    <img src="resultat.png"/>
    <br>[...]<br>
    <img src="resultat02.png"/>
    </center>

1.  Tout en haut du document, insérez un paragraphe vide puis insérez les 4 lignes suivantes

    <table class="f73">
    <tr>
    <td>
    Studio artistique Multi-Facettes<br>
    12, boul. des Jésuites<br>
    Montréal, (Québec)<br>
    (514) 515-4466
    </td>
    </tr>
    </table>


1.  Appliquer une trame de fond de type Bleu, accentuation 1, plus clair 60% sur les lignes insérées

1.  Sélectionnez les 4 lignes et appliquez une bordure inférieure et supérieure

1.  Enregistrez votre document


## Ajouter un en-tête et un pied de page et modification

1.  Créez un pied de page en utilisant l’option vide

1.  Remplacez l’espace réservé [Tapez ici] par la date courante et utilisez un format s’affichant comme `Jeudi 5 septembre 2024`
    * astuce: l'option *mettre à jour automatiquement* permet d'avoir toujours la date du jour

1.  Ajouter un en-tête en utilisant l’option vide (3 colonnes)

1.  Remplacez le texte de l’espace réservé aligné à gauche par votre nom

1.  Supprimez les espaces réservés alignés du centre

1.  Insérez dans le texte de l’espace réservé aligné à droite un Numéro de page à la Position actuelle (style de votre choix)

1.  Fermer l’en-tête et le pied de page

## Ajouter des pages 

1.  Ajoutez une nouvelle page vide tout au début de votre document

1.  Mettez le curseur au début de cette nouvelle page, et tapez le texte : Début de l’exercice

1.  Ajouter une nouvelle page vide à la fin de votre document

1.  Écrivez dans cette page le texte : Fin de l’exercice

1.  Vous remarquerez que l’entête et le pied de page s’affichent sur les 3 pages de votre document

1.  Spécifiez que l’en-tête et pied de page doivent être différents sur la première page

1.  Changez le Format des numéros de page pour la 2ème page pour qu’elle commence avec le numéro 1

1.  Fermer l’en-tête et le pied de page

1.  Enregistrez et fermez votre document

## Remise
  
Remettre le document sur <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=410078">Moodle</a>
 
