---
title: "08: Excel"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 80
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-8">Semaine 08 sur Moodle</a>

## Démos Excel

1. Références absolues Vs relatives
    * (avec {{% key "F4"%}})

    * défi: formule pour la table de multiplication

        <img src="table.png"/>

1. Anatomie d'une fonction

    * `NOM ( ARG1 ; ARG2 ; ... ; ARGN )`

1. Anatomie d'une formule

    * `= EXPR`
    * où `EXPR` est soit
        * une fonction
        * un nombre
        * une référence à une cellule
        * une addition `EXPR1 + EXPR2`
        * une soustraction `EXPR1 - EXPR2`
        * une multiplication `EXPR1 * EXPR2`
        * une division `EXPR1 / EXPR2`
        * une mise en parenthèse `(EXPR1)`

    * exemples:

        * `= SOMME(A1:A10)`
        * `= 12`
        * `= B12`
        * `= 12 + 20`
        * `= 12 + B12`
        * `= SOMME(A1:A10) + MOYENNE(B1:B20)`
        * `= (1+3) / (3+2)`
        * etc.

1. À noter que les arguments d'une fonction peuvent aussi être des expressions

    * exemples:

        * `=MIN( 1+2 ; 4+3 ; 12*29)`
        * `=SOMME(1+2 ; 4+3 ; 12*29)`
        * `=SOMME(1+2 ; MIN( 1 ; 2 ; 3 ) ; 12*29)`
        * `=SOMME(1+2 ; MIN( 1 ; 2 ; SOMME(3;4;5;6) ); 12*29)`
        * etc.




1. Outils pour insérer une fonction
    * Analyse rapide
    * Bouton fonction
    * Bouton somme automatique
    * Saisir + auto-complétion

