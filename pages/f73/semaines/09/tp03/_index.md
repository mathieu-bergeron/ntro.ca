---
title: "TP03: Excel (15%)"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 50
---

{{% pageTitle %}}



## Remises

* Partie 1:  sur <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=410095">Moodle</a>
* Partie 2:  sur <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=410099">Moodle</a>

## Travail à effectuer

* Partie 1 (10%):  sur <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-9">Moodle</a>
* Partie 2 (5%):  sur <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-10">Moodle</a>

## Grille de correction


<table>
<tr>
<th>

&nbsp;


</th>
<th>
Critère
</th>
<th>
Points
</th>
</tr>

<tr>
<th rowspan="3">

Partie 1

</th>

<td>

Bonnes formules et fonctions

</td>


<td>
30pts
</td>

</tr>


<tr>

<td>
Bonne utilisation des références relatives et/ou absolues
</td>

<td>
20pts
</td>

</tr>

<tr>

<td>
Format des cellules et mise en forme
</td>

<td>
16pts
</td>

</tr>


<tr>
<th rowspan="3">

Partie 2


</th>

<td>
Création des graphiques
</td>

<td>
24pts
</td>


</tr>

<tr>

<td>
Ajout d'étiquettes et mise en forme
</td>

<td>
10pts
</td>

</tr>






</table>


### Guide pour l'évaluation


<table>

<tr>

<th>
&leq;50%
</th>

<th>
~65%
</th>

<th>
~85%
</th>

<th>
&geq;95%
</th>


</tr>

<tr>

<th>
Compétence non-observée
</th>

<th>
Compétence acceptable
</th>

<th>
Compétence atteinte
</th>

<th>
Compétence dépassée
</th>


</tr>

<tr>

<td>

Problème de remise

Travail à peine débuté


</td>

<td>

Travail complet, mais avec des lacunes (p.ex. n'utilisant pas les techniques du cours comme les formules, les fonctions, etc.)

</td>

<td>

Travail complet, utilisant les techniques du cours (formules, fonctions, références absolues, etc.), mais contenant des erreurs mineures

</td>

<td>

Dépasse les attentes: complet, sans erreurs, utilisant toujours les bonnes techniques.

</td>

</tr>

</table>



