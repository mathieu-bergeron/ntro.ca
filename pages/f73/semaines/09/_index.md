---
title: "09: Excel"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 90
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-9">Semaine 09 sur Moodle</a>

## TP03 Excel

* Partie 1 (10%) cette semaine (énoncé sur Moodle)
* {{% link "./tp03/" "Grille de correction" %}}

## Démos

1. Format de cellule (types de données)

1. Fusionner des cellules

1. Reproduire la mise en forme

1. Taille des cellules
    1. option a) double-clic
    1. option b) cliquer avec curseur <img width="24px" src="curseur_colonne.png"/> et déplacer
    1. option b) clic-droit => largeur de colonne / hauteur de ligne

1. Insérer des lignes / colonnes

1. Mise en forme conditonnelle


1. Rappel: mode page <img src="mode_page.png" width="100px"/>
    * pour la mise en page
    * pour voir les en-têtes et pied de page
    


