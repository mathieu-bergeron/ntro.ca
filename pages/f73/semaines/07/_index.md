---
title: "07: Excel"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 70
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-7">Semaine 07 sur Moodle</a>


## Démo Excel

1. Cellule
    * *A2*
    * *B10*
    * *Z100*
    * etc.

1. Plage de cellules
    * *A2:A10*
    * *B14:C17*
    * etc.

1. Sélection (curseur)

    <img src="selection01.png"/>

1. Formule

    <img src="formule01.png"/>

1. Poignée de propagation (bouton de recopie)

    <img src="poignee_propagation01.png"/>
    <img src="poignee_propagation02.png"/>
    <img src="poignee_propagation03.png"/>

1. Colonne trop petite

    <img src="colonne01.png"/>
    <img src="colonne02.png"/>
    <img src="colonne03.png"/>

1. Erreur dans une formule

    <img src="erreur01.png"/>

1. Raccourcis

    * {{% key "Esc" %}} annuler action en cours
    * {{% key "Ctrl+→" %}} aller à la fin de la page de valeur

