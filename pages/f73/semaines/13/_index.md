---
title: "13: Access"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 130
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-13">Semaine 13 sur Moodle</a>


## Théorie Access

1. Notion de relation

    <img src="../12/access_table_achats.png"/>

    <img src="../12/access_table_items.png"/>



    <img src="relation.png"/>

1. Notion de requête

    <img src="requete.png"/>

    <img src="requete_resultat.png"/>

1. Utilité d'une requête
    * sauvegarder la configuration des filtres
    * combiner l'information de plusieurs tables (relation)


## Démo Access

1. Tri et filtres

1. Supprimer un tri / un filtre

1. Creér une requête


