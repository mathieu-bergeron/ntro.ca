---
title: "TP01: Word (15%)"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 30
---

{{% pageTitle %}}

## Remise

Sur <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=410079">Moodle</a>

## Exercice 1 (30pts)

1. Débuter avec le fichier {{% download "E-5.docx" "E-5.docx" %}} et enregistrer sous `Certificat cadeau Jouvence.docx`

1. Créer une mise en forme similaire à celle-ci

    <img src="resultat01.png"/>

1. En particulier, s'assurer de

    1.  créez une mise en forme attrayante pour ce document de promotion en utilisant des polices, des tailles de police, des styles et effets de texte appropriés. 

    1.  Transformez certaines lignes du document en une liste à puces et choisissez le style de puces de votre choix. 

    1.  changez l’orientation du papier pour Paysage, puis ajustez les marges à des valeurs différentes.  
        * astuce) pour appliquer l’orientation Paysage
            * cliquez sur l’onglet *Mise en page* => *Orientation* => *Paysage*

    1.  tapez votre nom quelque part sur le certificat cadeau. 

    1.  enregistrez les changements. 


## Exercice 2 (40pts) 

1. Débuter avec un nouveal document et enregistrer sous `Menu Restaurant chez Henri.docx`

1. Reproduire le contenu ci-bas

    <img src="resultat02.png"/>

1. En particulier

    1. utiliser un **tableau** pour aligner correctement les prix

    1. utiliser les bordures du tableau pour souligner les sections du menu (`Hamburger`, `Breuvages`, ...)

    1. insérer votre nom dans un pied de page



## Exercice 3 (30pts) 

1. Ouvrir {{% download "F-4.docx" "F-4.docx" %}} et enregistrer sous `Journées de sensibilisation.docx`

1. On vise une mise en forme similaire à ceci

    <img src="resultat03.png"/>

1. En particulier, on veut

    1.  appliquer une trame de fond à la première ligne du document (Produits de cuisine Cinq Étoiles) en utilisant une couleur de remplissage

    1.  insérer un tableau au bas de la page contenant les informations suivantes 


        <table class="f73">

        <tr>
        <td>
        Activité bénévole 
        </td>
        <td>
        Organisme hôte 
        </td>
        <td>
        Adresse 
        </td>
        </tr>


        <tr>
        <td>
        Peinture 
        </td>
        <td>
        Hôpital St-Antoine 
        </td>
        <td>
        760, rue de l’Hôpital 
        </td>
        </tr>


        <tr>
        <td>
        Contes pour enfants  
        </td>
        <td>
        École primaire Polichinelle 
        </td>
        <td>
        12, avenue Larue 
        </td>
        </tr>


        <tr>
        <td>
        Soins animaliers 
        </td>
        <td>
        Refuge des Quatre pattes 
        </td>
        <td>
        44, de la Côte-Blanche 
        </td>
        </tr>



        <tr>
        <td>
        Soupe populaire et dons de denrées
        </td>
        <td>
        Accueil la lumière intérieure
        </td>
        <td>
        76, rue Principale 
        </td>
        </tr>

        <tr>
        <td>
        Repas livrés aux ainés 
        </td>
        <td>
        Les  repas  chauds  de Camille
        </td>
        <td>
        19, rue des Hérissons 
        </td>
        </tr>

        <tr>
        <td>
        Alphabétisation  et  lecture libre
        </td>
        <td>
        Église Sainte-Marie
        </td>
        <td>
        44, 1<sup>ère</sup> Avenue
        </td>
        </tr>

        </table>

    1.  redimensionner  les  colonnes pour  que le texte de  chaque  cellule  tienne  sur une  seule ligne

    1.  appliquer un style de tableau de votre choix au tableau du document

    1.  mettre en forme le texte du document pour lui donner un aspect chaleureux et amusant.  
        * choisir  des  polices  et  des  attributs  de  mise  en  forme  afin  de  démarquer  les informations clés dans le document

    1.  insérer un pied de page centré contenant votre nom. 

    1.  appliquer un jeu de style au document

    1.  enregistrer les modifications et prévisualiser le document

    1.  ajouter une page vide au tout début de votre document 

    1.  écrivez votre nom et prénom dans cette page et appliquer une mise en forme adéquate au texte que vous venez d’insérer 

    1.  appliquer une bordure sur la première page de votre document seulement 

    1.  ajoutez un en-tête contenant la numérotation de votre document 

    1.  faites-en sorte que l’entête ne s‘affiche pas sur la première page 


