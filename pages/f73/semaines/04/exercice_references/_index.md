---
title: "Exercice Word: références"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 30
---


{{% pageTitle %}}


## Préparation 

1. Ouvrez le fichier {{% download "G-3.docx" "G-3.docx" %}} et enregistrez-le sous le nom `VotreNom_Musees.docx`

## Insertion de liens hypertextes 

1. Pour les expressions `Musée des beaux-arts de Montréal`, `Musée McCord`

    * trouvez les adresses Internet et créez 2 liens hypertextes pour ces expressions

1. Assurez-vous que tous vos liens sont fonctionnels

## Insertion de note de bas de page

1. Insérer une note de bas de page à la fin du paragraphe se trouvant sous le titre   

    `Musée des beaux-arts de Montréal`

1. La texte de la note de bas de page indique 

        Voir les détails sur le site du Musée des beaux-arts de Montréal

## Insertion des citations 

1. Le dernier paragraphe est emprunté à l’ouvrage dont les informations bibliographiques sont indiquées ci-dessous

1. Ajouter une citation à la suite du  dernier paragraphe pour référencer la source en question.   

    * Type de source: Livre   
    * Auteurs: Luc Daignault, Bernard Schiele   
    * Titre: "Les musées et leurs publics : Savoirs et enjeux"
    * Année: 2005   

## Insertion d’une bibliographie

1. Ajoutez une bibliographie sur une nouvelle page à la fin du document, qui affiche la citation que vous venez de créer.   


## Insertion de table des matières 

### Étape 1

1. Appliquez les styles indiqués à chacun des paragraphes suivants :   

<table class="f73">
<tr>
<th>
Page
</th>
<th>
Texte du titre
</th>
<th>
Style
</th>
</tr>


<tr>
<td rowspan="7">
1   
</td>

<td>
« La Presse  »   
</td>

<td>

*Titre*  

</td>

</tr>

<tr>

<td>

« RENTRÉ CULTURELLE 2011 - ARTS VISUELS»   

</td>


<td>

*Titre 1*  

</td>

</tr>

<tr>

<td>

« Inde, mode et architecture italienne »   

</td>


<td>

*Titre 2*  

</td>

</tr>

<tr>

<td>

« Musée des beaux-arts de Montréal »   

</td>


<td>

*Titre 3*  

</td>

</tr>

<tr>

<td>

« Musée national des beaux-arts du Québec »   

</td>


<td>

*Titre 3*  

</td>

</tr>

<tr>

<td>

« Fondation DHC/ART»   


</td>


<td>

*Titre 3*  

</td>

</tr>

<tr>

<td>

« Galerie de l'UQAM »   

</td>


<td>

*Titre 3*  

</td>

</tr>



<tr>

<td rowspan="4">

2


</td>

<td>

« Musée McCord »   

</td>


<td>

*Titre 3*   

</td>

</tr>

<tr>

<td>

« Musée Pointe-à-Callière »   

</td>


<td>

*Titre 3*   

</td>

</tr>


<tr>

<td>

« Musée des beaux-arts du Canada »   


</td>


<td>

*Titre 3*   

</td>

</tr>

<tr>

<td>

« Les musées et leurs publics : »   


</td>


<td>

*Titre 3*   

</td>

</tr>


</table>
   

### Étape 2

1. Insérez un saut de page avant le titre `La Presse`, au début du document, pour créer une page vide.   

1. Insérez une **table des matières** automatique dans la cette première page vide.   

1. Appliquez le format **Officiel** à la table des matières.   

1. Modifiez la table des matières pour inclure les styles **titre, titre 1, titre 2 et titre 3**.    

1. Vous devez avoir une table de matières similaire à cette figure :   

![](enonce-2_1.png)  

## Remise

1. Sauvegardez le fichier `VotreNom_Musees.docx` et mettez-le sur <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=410081">Moodle</a>







