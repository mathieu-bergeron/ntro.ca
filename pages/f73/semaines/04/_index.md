---
title: "04: Word"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 40
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-4">Semaine 04 sur Moodle</a>

## Démonstrations

1. Retour sur OneDrive

1. Hyperliens

1. Notes en bas de page

1. Citations et bibliographie

1. Styles

## À effectuer

### Exercice

1. {{% link "./exercice_references" "Utiliser des références" %}}

### TP1

1. Travail pratique Word (15%)
    * {{% link "./tp01" "énoncé" %}}
    * date limite: 20 septembre **avant le cours** 
