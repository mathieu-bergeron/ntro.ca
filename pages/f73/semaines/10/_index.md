---
title: "10: Excel"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 100
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-10">Semaine 10 sur Moodle</a>

## Démos

### Graphiques

1. Créer un graphique
    * Par l'analyse rapide
    * Ongle *Inserstion*

1. Modifier un graphique
    * Onglet *Création de graphique*
    * Onglet *Mise en forme*

1. Comprendre les données d'un graphique
    * Un graphique a plusieurs *Série*
    * Chaque série a
        * une étiquette
        * des points (x,y ou uniquement y)

### Filtres

1. Ajouter les filtres

1. Utiliser la fonction `SOUS.TOTAL()`
