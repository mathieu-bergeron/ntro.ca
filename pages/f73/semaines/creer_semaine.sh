semaine=$1

if [ "$1" = "" ]; then
    echo "usage $0 XX <-- numéro de semaine"
    exit
fi

mkdir $semaine

poids=$(echo $semaine | bc)
poids=$(($poids * 10))

cat << EOF > $semaine/_index.md
---
title: "Semaine $semaine"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: $poids
---

{{% pageTitle %}}

EOF
