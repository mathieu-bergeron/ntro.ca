---
title: "06: Examen Word (20%)"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 60
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-6">Semaine 06 sur Moodle</a>

## Description Examen Word

* Individuel

* Durée: 2h

* 2 exercices à faire sur Word

    * remise sur Moodle

* Documentation permise: 
    
    * matériel du cours
    * aide de Word
    * recherche Web

* Rappel:

    * Ce qui est **important** ce sont **les techniques utilisées,** plus que le   résultat final.   

* Astuces:

    * s'assurer de télécharger le fichier 

        <img src="telecharger.png"/>

    * s'assurer d'ouvrir Word (ne pas travailler dans le navigateur)

        <img src="word_lourd.png"/>

    * activer la modification

        <img src="activer_modification.png"/>

    * enregistrer sous sur OneDrive

        <img src="enregistrer_onedrive.png"/>

    * activer l'option de sauvegarde automatique

        <img src="enregistrement_automatique01.png"/>

