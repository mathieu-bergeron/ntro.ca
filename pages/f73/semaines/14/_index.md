---
title: "14: Access"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 140
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-14">Semaine 14 sur Moodle</a>


## Théorie Access


1. Retour sur les relations
    * intégrité des données
    * clé primaire
    * clé étrangère
    * outil pour former le critère
        * clique-droit => Créer



1. Notion de champs calculé
    * en mode création, ajouter un champ de type `Caclulé`
    * modifier l'expression à l'aide de l'outil (clique-droit => Créer)
    * exemples:
        * `[nom champ1] * [nom champ2]`
        * `NbCar([nom champ])`
   
