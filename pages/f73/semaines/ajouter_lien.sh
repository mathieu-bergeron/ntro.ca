semaine=$1

if [ "$1" = "" ]; then
    echo "usage $0 XX <-- numéro de semaine"
    exit
fi

num=$(echo $semaine | bc)

cat << EOF >> $semaine/_index.md

* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-$num">Semaine $semaine sur Moodle</a>

EOF
