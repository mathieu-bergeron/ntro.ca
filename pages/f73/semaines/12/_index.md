---
title: "12: Access"
bookToc: true
bookHidden: false
bookCollapseSection: false
weight: 120
---

{{% pageTitle %}}


* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344#section-12">Semaine 12 sur Moodle</a>

NOTE:

* l'exercice demande de renommer la colonne `N˚`
    * au besoin, créer cette colonne et la placer au début



## Théorie Access

1. Access est un outil pour manipuler une base de données

1. Les base de données sont partout, p.ex.
    * les données de ColNet, Moodle, etc.
    * les données des réseaux sociaux comme Facebook, Twitter/X, etc.
    * les données internes des petites et grandes entreprises

1. Typiquement, la base de données réside sur un ou plusieurs serveurs
    * il n'y a pas d'interface graphique pour y accéder

1. Access fournit:
    * un interface graphique pour manipuler la base données
    * des outils pour faciliter l'entrée de données (p.ex. création de formulaires)

NOTE:

* les applications plus petites comme ColNet, Moodle, etc. utilisent souvent un serveur de base de données similaire à Access, mais sans interface graphique

* les applications de plus grandes tailles comme Facebook, Twitter/X etc. ont souvent leurs propres serveurs de données conçus spécifiquement pour leurs besoins


### Excel Vs Access

#### Excel

<table class="f73">
<tr>
<td>

On *peut* placer les données en tableau:

* chaque colonne représente une information
* chaque ligne est une entrée où chaque information a une valeur

<img src="excel_tableau.png"/>

</td>
<tr>
</table>

#### Access

<table class="f73">
<tr>
<td>

On **doit** placer les données en *table:*

* chaque champ (colonne) a une type de donnée (information)
* chaque ligne est une entrée où chaque champ a une valeur

<img src="access_table.png"/>


</td>
<tr>
</table>


NOTES:

* on dit autant «ligne d'une table» / «entrée» / «enregistrement»
* chaque ligne a un numéro (identifiant unique)
* il faut donner un nom à la table

#### Excel

<table class="f73">
<tr>
<td>

On modifie les données et le formattage en même temps

</tr>
</td>
</table>


#### Access

<table class="f73">
<tr>
<td>

On modifie:

* la structure de données en **mode création**

    <img src="access_mode_creation.png"/>

* les donées en mode **feuille de donnéées**

    <img src="access_feuille_donnees.png"/>

</tr>
</td>
</table>


#### Excel

<table class="f73">
<tr>
<td>

On place les données librement

</tr>
</td>
</table>

#### Access

<table class="f73">
<tr>
<td>

On **doit:**

* placer les données dans différentes tables
* utiliser des numéros pour faire référence aux données d'une autre table

<img src="access_table_items.png"/>

<br>
<br>

<img src="access_table_achats.png"/>



</td>
</tr>
</table>

NOTE:

* typiquement chaque ligne est identifié de façon unique par un numéro
* ce numéro est appelé la *clé primaire* 
* on dit aussi *identifiant* ou *id*

NOTE:

* le but d'utiliser plusieurs tables et d'éviter de **répéter les données** (ce qui est souvent nécessaire avec Excel)
* dans une base de données, on s'attend à stoquer de très grandes quantités de données
* ces considération sont importantes


## Démos Access

1. Mode feuille de données


1. Mode création

1. Créer un formulaire (mode page)

1. Utiliser un formulaire

