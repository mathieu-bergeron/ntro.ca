---
title: "Calendrier"
weight: 21
---

{{% pageTitle %}}

<table>
<tr>
  <th>Semaine
  </th>
  <th>Date
  </th>
  <th>Remise
  </th>
  <th>Activité
  </th>
</tr>

<tr>

<td>
1
</td>

<td>
23 août
</td>

<td>
</td>


<td>

* Présentation du cours
* Démo *Windows*
* Exercice *Créer un document*

</td>

</tr>

<tr>

<td>
2
</td>

<td>
30 août
</td>

<td>

</td>


<td>

* *annulée*


</td>

</tr>


<tr>

<td>
3
</td>

<td>
6 septembre
</td>


<td>
</td>

<td>

* Démo *Tableaux Word*
* Exercice *Éléments spéciaux et tableaux*
* Débuter **TP1-Word**

</td>




</tr>

<tr>

<td>
4
</td>

<td>
13 septembre
</td>

<td>

</td>


<td>


* Démo *Références en Word*
* Exercice *Références*
* Travail sur le TP1-Word

</td>




</tr>

<tr>

<td>
5
</td>

<td>
20 septembre
</td>

<td>

Remise **TP1-Word**
<br>**avant le cours**

</td>

<td>

* Démo *PowerPoint*
* Débuter **TP2-PowerPoint**

</td>




</tr>

<tr>

<td>
6
</td>

<td>

27 septembre

</td>

<td>
</td>


<td >

<center style="background-color:pink; padding:5px;">

**Examen Word**

</center>

</td>



</tr>

<tr>

<td>
7
</td>

<td>
4 octobre
</td>

<td>

Remise **TP2-PowerPoint**
<br>**avant le cours**

</td>

<td>

* Démo *Introduction à Excel*
* Exercice *Premier pas Excel*

</td>




</tr>

<tr>

<td>

</td>

<td>
11 octobre
</td>

<td>

</td>

<td>

* Pas de cours
* Journée de rattrapage

</td>


</tr>



<tr>

<td>
8
</td>

<td>
18 octobre
</td>

<td>

</td>


<td>

* Démo *Formules en Excel*
* Exercices *Utiliser les formules*

</td>




</tr>


<tr>

<td>
9
</td>

<td>

25 octobre

</td>

<td>

</td>


<td>

* Démo *Mise en forme Excel*
* Exercice *Mettre en forme une feuille de calcul*
* Débuter **TP3-Partie1**

</td>




</tr>

<tr>

<td>
10
</td>

<td>
1 novembre
</td>

<td>

</td>

<td>

* Démo *Graphiques et filtres en Excel*
* Exercices *Graphiques*
* Exercices *Filtres*
* Débuter **TP3-Partie2**


</td>




</tr>

<tr>

<td>
11
</td>

<td>
8 novembre
</td>

<td>

Remise **TP3-Excel**
<br>**avant le cours**

</td>


<td style="background-color:pink;padding:5px;">

<center>

**Examen Excel**

</center>

</td>




</tr>

<tr>

<td>
12
</td>

<td>
15 novembre
</td>

<td>

</td>


<td>

* Démo *Introduction à Access*
* Exercices *Créer une base de données*

</td>




</tr>

<tr>

<td>
13
</td>

<td>
22 novembre (levée de cours)
</td>

<td>

</td>

<td>


</td>



</tr>

<tr>

<td>
14
</td>

<td>
29 novembre (contenu semaine 13)
</td>

<td>

</td>


<td>

* Démo *Manipuler les données en Access*
* Exercices *Manipuler les données*
* Débuter **TP4-Access**


</td>




</tr>

<tr>

<td>
15
</td>

<td>
6 décembre (contenu semaine 14)
</td>

<td>

&nbsp;

</td>

<td>

* Démo *Liaisons et requêtes en Access*
* Exercices *Liaisons, requête et champs calculés*
* Test préparatoire à l'examen Access


</td>




</tr>


<tr>

<td>
ajout
</td>

<td>
13 décembre (reprise levée de cours du 22 novembre)
</td>

<td>

Remise **TP4-Access**
<br>**avant le cours**

</td>


<td style="background-color:pink;padding:5px;">

<center>

**Examen Access**

</center>

</td>




</tr>

</table>
