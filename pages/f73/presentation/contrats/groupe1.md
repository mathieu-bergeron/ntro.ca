---
title: "Contrat de classe, groupe 1"
weight: 1
bookHidden: true
bookCollapseSection: true
---


### L'enseignant s'engage aussi à:

1. Prof dynamique (énergie dans les explications devant la classe)
1. Flexibilité (s'adapter à l'étudiant; reformuler les explications)
1. Avoir préparé le cours
1. Respectueux



### L'étudiant.e s'engage aussi à:

1. Bonne écoute (être responsable de son apprentissage)
1. Entraide
1. Persévérant
1. Respectueux
    * des questions des autres
1. Assidu
1. Organisé

