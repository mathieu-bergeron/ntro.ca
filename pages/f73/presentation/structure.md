---
title: "Structure du cours"
weight: 21
---

{{% pageTitle %}}

## Une séance typique

1. 5min) accueil, rappel des objectifs
1. 15-30min) démonstration reliée aux objectifs
1. le reste de la séance) travail pratique, questions de la file d'attente
