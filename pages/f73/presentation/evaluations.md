---
title: "Évaluations"
weight: 21
---

{{% pageTitle %}}

<table>
<tr>
    <th>Semaine
    </th>
    <th>Examen
    </th>
    <th>Remises
    </th>
    <th>Points
    </th>
</tr>

<tr>
<td>
Semaine 5
</td>
<td>
&nbsp;
</td>
<td>
Remise TP1
</td>
<td>
10%
</td>
</tr>

<tr>
<td>
&nbsp;
</td>
</tr>

<tr>
<td>
Semaine 6
</td>
<td>
Examen 1
</td>
<td>
&nbsp;
</td>
<td>
20%
</td>
</tr>

<tr>
<td>
&nbsp;
</td>
</tr>

<tr>
<td>
Semaine 7
</td>
<td>
&nbsp;
</td>
<td>
Remise TP2
</td>
<td>
10%
</td>
</tr>

<tr>
<td>
&nbsp;
</td>
</tr>

<tr>
<td>
Semaine 11
</td>
<td>
Examen 2
</td>
<td>
&nbsp;
</td>
<td>
20%
</td>
</tr>


<tr>
<td>
Semaine 11
</td>
<td>
&nbsp;
</td>
<td>
Remise TP3
<td>
15%
</td>
</tr>

<tr style="background-color:transparent;">
<td>
&nbsp;
</td>
</tr>

<tr>
<td>
Semaine 15
</td>
<td>
Examen 3
</td>
<td>
&nbsp;
</td>
<td>
15%
</td>
</tr>


<tr>
<td>
Semaine 15
</td>
<td>
&nbsp;
</td>
<td>
Remise TP4
<td>
10%
</td>
</tr>


</table>
