---
title: "Le prof"
weight: 200
---

{{% pageTitle %}}


## Parcours
Avez-vous été un étudiant à Montmorency?
Dans quel programme avez-vous étudié au cégep?
Vous avez étudié dans quelle domaine? (Parcours scolaire)
Depuis combien de temps enseignez-vous ?

## Auto-évaluation

Sur une note de 1 à 10, à quelle point vous vous considérer comme un prof dynamique et apprécier vous donner votre cours?

