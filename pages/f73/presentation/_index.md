---
title: "Présentation du cours"
weight: 1
bookCollapseSection: true
---

{{% pageTitle %}}

## Sujets

<table>
<tr>
<th>
Windows au Collège
</th>
</tr>

<tr>
<th>
Microsoft Word
</th>
<td>
Documents
</td>
</tr>

<tr>
<th>
PowerPoint
</th>
<td>
Présentations
</td>
</tr>

<tr>
<th>
Excel
</th>
<td>
Calculs, graphiques
</td>
</tr>

<tr>
<th>
Access
</th>
<td>
Base de données
</td>
</tr>

</table>

## {{% link "./calendrier" "Calendrier" %}}

## {{% link "./structure" "Structure du cours" %}}

## {{% link "./evaluations" "Évaluations" %}}

## Autres liens

1. <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=9344">Page Moodle</a>
1. <a href="https://aiguilleur.ca/file_d_attente/mathieu.bergeron">File d'attente pour questions en classe</a>
1. {{% download "./presentation/420-F73-MO-A24-AP-MB.pdf" "Plan de cours" %}}

1. {{% link "../semaines/01/raccourcis/" "Astuces et raccourcis"  %}}

