---
title: "Problème de Monty Hall"
weight: 400
# bookFlatSection: false
# bookToc: true
# bookHidden: false
bookCollapseSection: true
# bookComments: false
# bookSearchExclude: false
---

{{% pageTitle %}}


## Explication

## Analyse

1. Stratégie *rester avec mon choix*

    <img class="figure" src="3portes_reste.png"/>

    * taux de réussite: 1/3

1. Stratégie *changer mon choix*

    <img class="figure" src="3portes_change.png"/>

    * taux de réussite: 2/3



## Simulation en Python

1. {{% download "monty.py" "monty.py" %}}

1. Simulation

    ```
    $ python monty.py 

        stratégie RESTE
            nombre essais: 4918
            nombre réussites: 1697
            nombre échecs: 3221
            taux de succès: 34.51%

        stratégie CHANGE
            nombre essais: 5082
            nombre réussites: 3389
            nombre échecs: 1693
            taux de succès: 66.69%
    ```


## Concepts mathématiques

1. `P(x|y)`: probabilité que `x` soit vrai si `y` est vrai
    * `P(x)`: probabilité de réussite 
    * `P(y)`: probabilité d'avoir fait le mauvais choix
    * `P(x|y)`: probabilité de réussite si j'ai fait le mauvais choix 
        * (l'impact de `y` sur les chances de `x`)

1. `P(x) = P(x|y) * P(y)`
    * la probabilité de `x` dépend de `P(y)` ET de `P(x|y)`

1. Pour le cas à 3 portes, on a `P(x|y) = 1`


