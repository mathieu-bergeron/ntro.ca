---
title: "Groupe 1"
weight: 20
bookCollapseSection: false
bookHidden: false
---


# Contrat de classe groupe 1

> «On embauche selon les compétences, mais quand on congédie c'est à cause de l'attitude»

{{<excerpt>}}

* 18% de votre note finale est reliée à l'attitude professionelle.

{{</excerpt>}}


## Étudiant.e

1. Tous mes écrans affichent du matériel pédagogique
1. Je parle à voix basse en tout temps
1. Ma musique est inaudible pour mes voisin.es
1. J'entre et je sors de la classe en silence
1. Je quitte la classe pour socialiser, manger, prendre une pause, etc.
1. Si en retard, j'attends qu'on m'ouvre et je rattrape mon retard sans déranger
1. J'utilise la file d'attente pour les questions (périodes de labo)
1. ...

## Équipe

1. Au besoin, je demande de l'aide mon équipe
1. Si possible, j'aide les personnes de mon équipe
1. Bonne communication
    * garder les échéances en tête (bien communiquer les retards)
1. Bonne répartition des tâches
    * respecter les compétences complémentaires


## Prof

1. J'explique le déroulement de chaque cours
1. Je fais respecter les règles de classe 
1. Je m'assure que l'ambiance est toujours propice au travail
1. Lancer les élèves dans des tâches réalistes et les laisser se débrouiller
1. Connaît bien sa matière, prend le temps de vérifier (capable de répondre)

