---
title: "Structure"
weight: 30
bookHidden: false
---


# Structure du cours 4F5

{{% video "/4f5/presentation/structure.mp4" %}}

## Aperçu




* Le cours est divisé en 3 étapes:
    1. Étape 1:
        * modules 1 à 6
        * 5 rencontres d'équipe obligatoires
        * **À chaque module**: objectifs pour avancer le TP#1
        * examen#1
        * remise du TP#1
    1. Étape 2:
        * modules 7 à 10
        * 5 rencontres d'équipe obligatoires
        * **à chaque module**: objectifs pour avancer le TP#2
        * examen#2 
        * remise du TP#2
    1. Étape 3: 
        * projet (aucun module)

## Détails


<!--
{{% video "./structure_details.mp4" "50%" %}}
-->


* Un module contient:
    1. un exposé théorique
    1. un tutoriel
    1. des objectifs à faire pour avancer le TP
    1. une auto-évaluation sur Moodle
        * **remise automatique** la veille de la rencontre d'équipe
        * (la note que s'attribue l'étudiant peut être modifiée par l'enseignant)

* Une rencontre d'équipe obligatoire est:
    1. 10 minutes, en équipe
    1. chaque membre de l'équipe présente son avancement
    1. il **faut communiquer les problèmes ou bloquants**
        * (même si on a pas le temps de les régler)

* Évaluation d'un TP et du projet
    1. Effectuée à partir d'un `$ git clone` neuf
        * doit compiler et doit s'exécuter
    1. Évaluation individuelle (chaque étudiant est respondable de son dépôt Git)
    1. Pour le projet: compte-rendu des contributions de chaque étudiant


## Pendant les heures de cours

<!--
{{% video "questions.mp4" "50%" %}}
-->

* Le prof va répondre aux questions, dans l'ordre
indiqué sur  <a href="https://aiguilleur.ca/file_d_attente/mathieu.bergeron">aiguilleur.ca/file_d_attente/mathieu.bergeron</a>

## En dehors des heurs de cours

* SVP poser vos questions par courriel directement à <a href="mailto:mathieu.bergeron@cmontmorency.qc.ca">mathieu.bergeron@cmontmorency.qc.ca</a>
