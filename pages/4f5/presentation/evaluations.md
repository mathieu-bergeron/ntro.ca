---
title: "Évaluations"
weight: 50
bookHidden: false
---


# Évaluations du cours 4F5

{{% video "/4f5/presentation/evaluations.mp4" %}}

{{<excerpt>}}

* AJOUTS aux critères d'évaluation:
    * code **respectant la méthodologie du cours**
    * fonctionne à partir `$ git clone` neuf

{{</excerpt>}}


## Tableau des évaluations

<table>
<tr>
	<th>Type d'évaluation
	</th>
	<th>Nombre
	</th>
	<th>%
	</th>
	<th>Détails
	</th>
</tr>
<tr>
<td style="text-align:center;">

### Examens

</td>
	<td style="text-align:center;">2
	</td>
	<td>50%
	</td>
	<td style="text-align:center;">
	Questions à choix de réponses sur Moodle
	</td>
</tr>
<tr>
<td style="text-align:center;">

### Auto-évaluations

</td>
	<td style="text-align:center;">9
	</td>
	<td>18%
	</td>
	<td style="text-align:center;">
	Chaque étudiant évalue son progrès<br>
	(le prof peut modifier la note)
	</td>
</tr>
<tr>
<td style="text-align:center;">

### (*) TPs

</td>
<td style="text-align:center;">2
	</td>
	<td>12%
	</td>
	<td style="text-align:center;">
	Évaluation des objectifs du TP à partir d'un <code>$&nbsp;git&nbsp;clone</code> neuf
	</td>
</tr>
<tr>
<td style="text-align:center;">

### (*) Projet

</td>
	<td style="text-align:center;">1
	</td>
	<td>20%
	</td>
	<td style="text-align:center;">
	Évaluation des objectifs du projet, selon un compte-rendu des contributions de chaque étudiant
	</td>
</tr>
</table>


{{<excerpt class="warning max-width-75">}}

(*) **Doit être remis via Git** 

* aucun autre type de remise sera acceptée

* si bogue avec Git: 

    * venir en classe dès que possible pour corriger avec le prof

{{</excerpt>}}


## Critères d'évaluation

<table>

<tr>

<th>
&leq;50%
</th>

<th>
~65%
</th>

<th>
~85%
</th>

<th>
&geq;95%
</th>


</tr>

<tr>

<th>
Compétence non-observée
</th>

<th>
Compétence acceptable
</th>

<th>
Compétence atteinte
</th>

<th>
Compétence dépassée
</th>


</tr>

<tr>

<td>

Remise Git non fonctionnelle

Code non fonctionnel

</td>

<td>

Code **ne respectant pas la méthodologie du cours**, mal organisé ou difficile à lire

Noms bizarres pour le projet choisi

* p.ex. `VueFileAttente` affiche des paramètres

Résultats minimalement satisfaisants

* p.ex. une règle CSS à peine visible

Projet brisé sur GitLab

</td>

<td>

Code **respectant la méthodologie du cours**, bien organisé et facile à lire

Noms pertinents pour le projet choisi

Fonctionalité claire et données bien modélisées

Projet fonctionne à partir d'un `$ git clone` neuf

</td>

<td>

Dépasse les attentes

</td>

</tr>



</table>
