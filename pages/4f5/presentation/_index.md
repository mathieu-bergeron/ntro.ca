---
title: "Présentation du cours 4F5"
weight: 1
bookCollapseSection: true
aliases: ["/4f5"]
---


{{% pageTitle %}}

> Vous allez développer une application graphique client/serveur.  À partir
> d'un projet vide, vous allez construire votre application étape par étape (en
> suivant des tutoriels et la théorie de cours).  Le travail s'effectuera en
> équipe de 4 (à noter que les remises sont néamoins **individuelles**).
> L'application va contenir plusieurs pages, dont une où vous implanterez un
> jeu à deux joueurs que votre équipe choisira.


{{% video "/4f5/presentation/bonjour.mp4" %}}

<div>

* {{% link "/4f5/presentation/projets2024" "Projets vedettes 2024" %}}
* {{% link "/4f5/presentation/projets2023" "Projets vedettes 2023" %}}
* {{% link "/4f5/presentation/projets2022" "Projets vedettes 2022" %}}
* {{% link "/4f5/presentation/survol" "Survol" %}}
* {{% link "/4f5/presentation/structure" "Structure" %}}
* {{% link "/4f5/presentation/calendrier" "Calendrier" %}}
* {{% link "/4f5/presentation/evaluations" "Évaluations" %}}
* {{% link "/4f5/presentation/exemples_de_jeu" "Exemples de jeu" %}}
* {{% link "/4f5/presentation/exemples_de_pages" "Exemples de pages" %}}
* <a href="https://www.cmontmorency.qc.ca/etudiants/services-aux-etudiants/aide-a-la-reussite/aide-techniques/centre-daide-en-informatique/?">Centre d'aide</a>

</div>
<div>

* {{% link "/4f5/presentation/equipes" "Équipes 2025" %}}

</div>


### Liens

* <a href="/4f5/presentation/plan/420-4F5-MO-H25_MB.pdf" download>Plan de cours</a>
* <a href="https://cmontmorency.moodle.decclic.qc.ca/course/view.php?id=7374">Page Moodle du cours</a>
* <a href="/4f5/presentation/NormesProgrammationJava.pdf" download>Normes de programmation</a>
* Prise de rendez-vous: <a href="https://aiguilleur.ca/file_d_attente/mathieu.bergeron">aiguilleur.ca/file_d_attente/mathieu.bergeron</a>

