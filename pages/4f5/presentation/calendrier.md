---
title: "Calendrier"
weight: 40
bookHidden: false
---

# Calendrier du cours 4F5

{{% video "/4f5/presentation/calendrier.mp4"  %}}

<br>

<table>
<tr>
	<th>Semaine
	</th>
	<th>Contenu
	</th>
	<th>Évaluation
	</th>
</tr>

<tr>
<td style="text-align:center;vertical-align:top;">

## Semaine&nbsp;1

</td>
<td>

{{% link "../modules/01/" "module 1" %}}

* Installation VSCode, JDK, Git, Gradle
* Création du dépôt Git
* Exécution de `testfx`
* Création du projet pour le cours

<img src="/4f5/presentation/tutoriel01.png"/>

</td>
<td>
Auto-évaluation&nbsp;1
</td>
</tr>


<tr>
<td style="text-align:center;vertical-align:top;">

## Semaine&nbsp;2

</td>
<td>

{{% link "../modules/02/" "module 2" %}}

* Documents de conception
    * maquette pour chaque page
    * spécification du modèle
    * plan de tests
* Patron MVC (1): création d'un frontal
    * Notion de graphe de tâches
* Afficher la fenêtre par défaut


<img src="/4f5/presentation/tutoriel02.png"/>

{{% animation "/4f5/presentation/tutoriel02.mp4" %}}


<td>
Auto-évaluation&nbsp;2
</td>
</tr>

<tr>
<td style="text-align:center;vertical-align:top;">

## Semaine&nbsp;3

</td>
<td>

{{% link "../modules/03/" "module 3" %}}

* Vue JavaFx (1): fxml de base, css, traductions
* Avancement du frontal (graphe de tâches)
* Affichage d'une vue principale

{{% animation "/4f5/presentation/tutoriel03_fr.mp4" %}}
{{% animation "/4f5/presentation/tutoriel03_en.mp4" %}}

</td>


<td>
Auto-évaluation&nbsp;3
</td>
</tr>


<tr>
<td style="text-align:center;vertical-align:top;">

## Semaine&nbsp;4

</td>
<td>

{{% link "../modules/04/" "module 4" %}}

* Patron MVC (1): événements et capteurs d'événements
* Passer d'une sous-vue à l'autre

{{% animation "/4f5/presentation/tutoriel04.mp4" %}}

</td>

<td>
Auto-évaluation&nbsp;4
</td>

</tr>


<tr>
<td style="text-align:center;vertical-align:top;">

## Semaine&nbsp;5

</td>
<td>

{{% link "../modules/05/" "module 5" %}}

* Patron MVC (3): notion de Modèle
* Implantation d'une fonctionnalité
    * création d'un dorsal *(backend)*
    * notion de message
    * observation et affichage du modèle

{{% animation "/4f5/presentation/tutoriel05.mp4" %}}

</td>

<td>
Auto-évaluation&nbsp;5
</td>

</tr>


<tr>
<td style="text-align:right;vertical-align:top;">

## Séance&nbsp;6.1

</td>
<td>
«module 6»

* travail libre
* pas de nouveaux objectifs
* pas d'auto-évaluation
</td>

</tr>

<tr>
<td style="text-align:right;background-color:pink;vertical-align:top;">

## Séance&nbsp;6.2

</td>
	<td colspan="2" style="text-align:right;background-color:pink">
	REMISE TP #1<br><br>
	EXAMEN #1
	</td>
</tr>


<tr>
<td style="text-align:center;vertical-align:top;">

## Semaine&nbsp;7

</td>
<td>

{{% link "../modules/07/" "module 7" %}}

* Vue JavaFx (2): contrôles personnalisés, affichage adaptatif

{{% animation "/4f5/presentation/tutoriel07.mp4" %}}

</td>
<td>
Auto-évaluation&nbsp;7
</td>
</td>
</tr>

<tr>
<td style="text-align:center;vertical-align:top;">

## Semaine&nbsp;8

</td>
<td>

{{% link "../modules/08/" "module 8" %}}

* Vue JavaFx (3): animations, dessins 2D
* implantation d'un jeu temps réel

{{% animation "/4f5/presentation/tutoriel08.mp4" %}}

</td>

<td>
Auto-évaluation&nbsp;8
</td>


</tr>

<tr>
<td style="text-align:center;vertical-align:top;">

## Semaine&nbsp;9

</td>
<td>

{{% link "../modules/09/" "module 9" %}}

* Patron MVC (4): 
    * communication client/serveur

{{% animation "/4f5/presentation/tutoriel09.mp4" %}}

</td>

<td>
Auto-évaluation&nbsp;9
</td>


</tr>

<tr>
<td style="text-align:center;vertical-align:top;">

## Semaine&nbsp;10

</td>
<td>

{{% link "../modules/10/" "module 10" %}}

* Patron MVC (5): 
    * modèles multiples
    * graphe de tâches dynamique

{{% animation "/4f5/presentation/tutoriel10.mp4" %}}

</td>
<td>
Auto-évaluation&nbsp;10
</td>
</tr>

<tr>
<td style="text-align:right;vertical-align:top;">

## Séance&nbsp;11.1

</td>
<td>
travail libre
</tr>
<tr>
<td style="text-align:right;background-color:pink;vertical-align:top;">

## Séance&nbsp;11.2

</td>
<td colspan="2" style="text-align:right;background-color:pink;">
REMISE TP #2 <br><br>
EXAMEN #2
</td>
</tr>

<tr>
<td colspan="3" style="text-align:left;vertical-align:top;">
<br>
<br>

## Semaines&nbsp;12-15:&nbsp;Projet

<br>
<br>&nbsp;
</td>
</tr>
<tr>
	<td colspan="3" style="text-align:right;background-color:pink">
	<br>
	REMISE PROJET: semaine 15, séance 2
	<br>&nbsp;
	</td>
</tr>

</table>


