---
title: "Calendrier des séances"
weight: 41
---

{{% pageTitle %}}

<table>
<tr>
  <th colspan="2">Date
  </th>
  <th rowspan="2">Activité
  </th>
  <th rowspan="2">À terminer
  </th>
  <th rowspan="2">Semaine.Séance
  </th>
</tr>

<tr>
<th>
gr&nbsp;3
</th>
<th>
gr&nbsp;1,2
</th>
</tr>

<tr style="border-top:solid gray 3px;">

<td>

<center>

21&nbsp;jan.

</center>

</td>

<td>

<center>

22&nbsp;jan.

</center>

</td>


<td>

* Premier cours
* Former équipe
* Brise glace

</td>

<td>


</td>

<td>
1.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

23&nbsp;jan.

</center>

</td>



<td>

* Tutoriel et objectifs #1

</td>

<td>

<p>

* Former l'équipe et choisir le jeu
* Créer le dépôt Git et remettre l'URL
* Créer le projet (avec le bon nom de jeu) et pousser sur GitLab

</p>

* Auto-évaluation 1<br>(date limite: 29 janv. 23h59)

</td>

<td>
1.2
</td>




</tr>

<tr style="border-top:solid gray 3px;">

<td>

<center>

28&nbsp;jan.

</center>

</td>

<td>

<center>

29&nbsp;jan.

</center>

</td>


<td>

* Théorie #2
* Tutoriel et objectifs #2


</td>

<td>



</td>

<td>
2.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

30&nbsp;jan.

</center>

</td>



<td>

* Rencontre d'équipe


</td>

<td>

* Auto-évaluation 2<br>(date limite: 5 fév. 23h59)

</td>

<td>
2.2
</td>




</tr>

<tr style="border-top:solid gray 3px;">

<td>

<center>

4&nbsp;fév.

</center>

</td>

<td>

<center>

5&nbsp;fév.

</center>

</td>


<td>

* Théorie #3
* Tutoriel et objectifs #3

</td>

<td>


</td>

<td>
3.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

6&nbsp;fév.

</center>

</td>



<td>


* Rencontre d'équipe

</td>

<td>

* Auto-évaluation 3<br>(date limite: 12 fév. 23h59)

</td>

<td>
3.2
</td>




</tr>

<tr style="border-top:solid gray 3px;">

<td>

<center>

11&nbsp;fév.

</center>

</td>

<td>

<center>

12&nbsp;fév.

</center>

</td>


<td>

* Théorie #4
* Tutoriel et objectifs #4


</td>

<td>



</td>

<td>
4.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

13&nbsp;fév.

</center>

</td>



<td>

* *RECONTRE ANNULÉE*
* *(tempête)*
* ignorer questions sur rencontre d'équipe dans l'auto-évaluation

</td>

<td>

* Auto-évaluation 4
* (date limite: 19 fév. 23h59)

</td>

<td>
4.2
</td>




</tr>

<tr style="border-top:solid gray 3px;">

<td>

<center>

18&nbsp;fév.

</center>

</td>

<td>

<center>

19&nbsp;fév.

</center>

</td>



<td>

* Théorie #5
* Tutoriel et objectifs #5


</td>

<td>


</td>

<td>
5.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

20&nbsp;fév.

</center>

</td>



<td>


* Rencontre d'équipe

</td>

<td>

* Auto-évaluation 5<br>(date limite: 26 fév. 23h59)

</td>

<td>
5.2
</td>




</tr>

<tr style="border-top:solid gray 3px;">

<td>

<center>

25&nbsp;fév.

</center>

</td>

<td>

<center>

26&nbsp;fév.

</center>

</td>


<td>

* Module 6: travail libre

</td>

<td>


</td>

<td>
6.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

<strong>27&nbsp;février</strong>

</center>

</td>




<td colspan="2" style="background-color:pink;">

* REMISE TP#1
* EXAMEN #1

</td>

<td>
6.2
</td>

</tr>


<tr style="border-top:solid gray 3px;">

<td colspan="4">

<center>

Semaine de rattrapage


</center>

<td>


</tr>

<tr style="border-top:solid gray 3px;">

<td>

<center>

11&nbsp;mars

</center>

</td>

<td>

<center>

12&nbsp;mars

</center>

</td>


<td>


* Théorie #7
* Tutoriel et objectifs #7

</td>

<td>


</td>

<td>
7.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

13&nbsp;mars

</center>

</td>




<td>

* Rencontre d'équipe

</td>

<td>


* Auto-évaluation 7 (date limite: 18 mars 23h59)

</td>

<td>
7.2
</td>

</tr>

<tr style="border-top:solid gray 3px;">

<td>

<center>

17&nbsp;mars

</center>

</td>

<td>

<center>

18&nbsp;mars

</center>

</td>


<td>

* Théorie #8
* Tutoriel et objectifs #8


</td>

<td>



</td>

<td>
8.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

19&nbsp;mars

</center>

</td>




<td>

* Rencontre d'équipe


</td>

<td>


* Auto-évaluation 8 (date limite: 26 mars 23h59)

</td>

<td>
8.2
</td>

</tr>

<tr style="border-top:solid gray 3px;">

<td>

<center>

25&nbsp;mars

</center>

</td>

<td>

<center>

26&nbsp;mars

</center>

</td>


<td>

* Théorie #9
* Tutoriel et objectifs #9

</td>

<td>



</td>

<td>
9.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

27&nbsp;mars

</center>

</td>




<td>

* Rencontre d'équipe


</td>

<td>


* Auto-évaluation 9 (date limite: 2 avril 23h59)

</td>

<td>
9.2
</td>

</tr>


<tr style="border-top:solid gray 3px;">

<td>

<center>

1&nbsp;avril

</center>

</td>

<td>

<center>

2&nbsp;avril

</center>

</td>



<td>

* Théorie #10
* Tutoriel et objectifs #10

</td>

<td>



</td>

<td>
10.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

3&nbsp;avril

</center>

</td>




<td>

* Travail libre


</td>

<td>



</td>

<td>
10.2
</td>

</tr>



<tr style="border-top:solid gray 3px;">

<td>

<center>

8&nbsp;avril

</center>

</td>

<td>

<center>

9&nbsp;avril

</center>

</td>


<td>

* Rencontre d'équipe


</td>

<td>


* Auto-évaluation 10 (date limite: 9 avril 23h59)

</td>

<td>
11.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

<strong>10&nbsp;avril</strong>

</center>

</td>





<td colspan="2" style="background-color:pink;">

* REMISE TP#2
* EXAMEN #2

</td>



<td>
11.2
</td>

</tr>


<tr style="border-top:solid gray 3px;">

<td>

<center>

15 avril

</center>

</td>

<td>

<center>

16 avril

</center>

</td>


<td>

* Rencontre d'équipe
* Début du projet<br>
  (date limite: mardi 14 mai)


</td>

<td>

* Travail sur le projet


</td>

<td>
12.1
</td>

</tr>



<tr>

<td colspan="2">

<center>

15&nbsp;avril - 21&nbsp;mai

</center>

</td>




<td>

* Travail sur le projet

</td>

<td >

* Travail sur le projet

</td>



<td>
12.2 - 15.2
</td>

</tr>


<tr>

<td colspan="2">

<center>

**21&nbsp;mai**

(reprise tempête)

</center>


</td>






<td colspan="2" style="background-color:pink;">

* REMISE PROJET

</td>




<td>
15.2
</td>

</tr>






</table>
