---
title: "Équipes"
weight: 1
bookCollapseSection: false
---

{{% pageTitle %}}

## Groupe 1 (5 équipes)

### Serpents-échelles

1. Marwan
1. Kailey
1. Raffi
1. Raphaël

### Puissance 4

1. Ashank
1. Yuan
1. Colin
1. Samuel F.

### Street fighter

1. Charbel
1. Hugo
1. Dylan

### Roulette russe

1. Toni
1. Farid
1. Bastien
1. Youssef

### Pacman

1. Anne-Sophie
1. Lucas
1. Julien
1. Goufran




## Groupe 2 (6 équipes)

### Plateforme

1. Loïc
1. Nazimzhon
1. Félix

### Ultimate cards

1. Fabrice
1. Denis
1. Samuel N.

### Snake

1. Mohamed
1. Adam
1. Mario

### Key dance (jeu de rythme)

1. Jonathan
1. Thomas
1. Guillaume
1. Nikola

### Roche-papier-ciseau

1. Badr
1. Calin
1. Caesar


### Tic-tac-toe

1. Muhammad
1. Umar
1. Samy



## Groupe 3 (7 équipes)

### Scrabble

1. Sirine
1. Destinée
1. Aichatou

### Flappy bird

1. Areej
1. Nermine
1. Aya

### Échecs

1. Félix-Etienne
1. Erik
1. Antoine

### Dames

1. Rayane

### TypeShark

1. Natali
1. Marshlee
1. Lina

### Tour GSP

1. Gregory
1. Nathan
1. Nicolas
1. Anas

### Roulette et Blackjack

1. Dhruv
1. Samuel G.
1. Charles
1. Jocelyn
