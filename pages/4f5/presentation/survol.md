---
title: "Survol"
weight: 20
bookHidden: false
---


# Survol du cours 4F5



<!--
{{% video "/4f5/presentation/objectifs.mp4" "50%" %}}
-->

<!--
<center>
<video width="50%" src="/cours/4f5/presentation/objectifs.mp4" type="video/mp4" controls playsinline>
</center>
-->


### Objectif du cours

* En équipe de 4, implanter un petit jeu 2D
    * (p.ex. échecs, bomberman)
    * jouable à 2
    * jouable en réseau

* Le jeu doit contenir 6 pages:
    * une page `FileAttente` est fournie par le prof (via les tutoriels)
    * la page où jouer une partie est le projet final (semaines 12-15)
    * chaque membre est responsable d'une page:
        * p.ex. paramètres, profil d'un joueur, etc.

* Chaque équipe **doit** implanter un jeu **différent**
    * validez votre choix avec le prof
    * {{% link "./exemples_de_jeu" "exemples de jeu" %}}

* Vous **devez** suivre la méthodologie du cours:
    * JDK 11+, VSCode, Git, Gradle, JavaFx (*)
    * librairie Ntro développée par le prof: 
        * {{% link "/ntro/api" "Javadoc de l'API" %}}
        * <a href="https://gitlab.com/mathieu-bergeron/ca.ntro" target="_blank">Code source sur Gitlab</a>

(*) testé jusqu'à JDK 21 et OpenJDK 21

