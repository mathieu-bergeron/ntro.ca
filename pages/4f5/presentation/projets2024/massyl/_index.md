---
bookHidden: true
---

## Massyl Boularas

<center>
<video style="max-width:400px;" controls="false" autoplay="true" loop="true">
    <source src="/4f5/presentation/projets2024/massyl/massyl01.webm">
    <source src="/4f5/presentation/projets2024/massyl/massyl01.mp4">
</video>
</center>

Points forts:

* Dessin des pions avec du code
* Gestion des mouvements des pions 
* Début d'implantation des règles (les pions ne peuvent reculer)

