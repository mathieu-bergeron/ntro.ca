rm -f to_mp4.sh
ag -g"mp4$" | while read i; 
do
    webm="$i"; 
    mp4=$(echo $webm | sed "s/.mp4/.webm/"); 
    if [ ! -e $mp4 ]; then
        cmd=$(echo ffmpeg -y -i \"$webm\" \"$mp4\"); 
        echo $cmd >> to_mp4.sh
    fi
done
