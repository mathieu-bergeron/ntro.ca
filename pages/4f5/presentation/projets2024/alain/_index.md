---
bookHidden: true
---

## Alain Khreis

<center>
<video style="max-width:100%;" controls="false" autoplay="true" loop="true">
    <source src="/4f5/presentation/projets2024/alain/alain.webm">
    <source src="/4f5/presentation/projets2024/alain/alain.mp4">
</video>
</center>

Points forts:

* Bonne utilisation des fragments et du CSS
* Affichage du nom de l'usager
* Saisie de texte qui s'adapte à la taille du texte entré
* Retrait des messages
* Animations
* Gestion de dates

