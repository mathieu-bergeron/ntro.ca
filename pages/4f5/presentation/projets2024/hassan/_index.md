---
bookHidden: true
---

## Hassan Rammal

<center>
<video style="max-width:100%;" controls="false" autoplay="true" loop="true">
    <source src="/4f5/presentation/projets2024/hassan/hassan.webm">
    <source src="/4f5/presentation/projets2024/hassan/hassan.mp4">
</video>
</center>

Points forts:

* Implantation des "tank controls" (tourner OU avancer, mais pas les deux en même temps)
* Implantation des boulets (fréquence de tir limitée, détection de collision)
* Ajout des points

