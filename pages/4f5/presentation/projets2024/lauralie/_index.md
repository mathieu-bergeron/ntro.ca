---
bookHidden: true
---

## Lauralie Dufour

<center>
<video style="max-width:100%;" controls="false" autoplay="true" loop="true">
    <source src="/4f5/presentation/projets2024/lauralie/lauralie.webm">
    <source src="/4f5/presentation/projets2024/lauralie/lauralie.mp4">
</video>
</center>

Points forts:

* Utilisation du monde2d pour afficher les images du tutoriels
* Dessiner la grille avec du code dans le monde2d
* Ajuster les boutons suivant/précédent selon le contexte

