---
bookHidden: true
---

## Youssef Birji

<center>
<video style="max-width:100%;" controls="false" autoplay="true" loop="true">
    <source src="/4f5/presentation/projets2024/youssef/youssef.webm">
    <source src="/4f5/presentation/projets2024/youssef/youssef.mp4">
</video>
</center>

Points forts:

* Gestion des usagers
    * ajout d'usager, connexion, profil, choix de couleurs différents pour chaque usager
* Intégration de plusieurs pages

