---
bookHidden: true
---

## Mélia Benoit

<center>
<video style="max-width:100%;" controls="false" autoplay="true" loop="true">
    <source src="/4f5/presentation/projets2024/melia/melia01.webm">
    <source src="/4f5/presentation/projets2024/melia/melia01.mp4">
</video>
</center>

Points forts:

* Création d'un Monopoly du Collège Montmorency
* Animation des pions 
* Implantation des règles du jeu 
    * y compris l'achat des terrains, l'effet des cartes, ...
* Synchronisation du monde2d en mode client/serveur

<center>
<video style="max-width:100%;" controls="false" autoplay="true" loop="true">
    <source src="/4f5/presentation/projets2024/melia/melia02.webm">
    <source src="/4f5/presentation/projets2024/melia/melia02.mp4">
</video>
</center>
