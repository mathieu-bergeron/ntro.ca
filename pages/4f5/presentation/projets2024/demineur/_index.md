---
bookHidden: true
---

## Démineur

* Hugo Beauregard, Jacob Fougères, Sami Amiar, Gabriel Arcand


<center>
<video style="max-width:70%;" controls="false" autoplay="true" loop="true">
    <source src="/4f5/presentation/projets2024/demineur/demineur.webm">
    <source src="/4f5/presentation/projets2024/demineur/demineur.mp4">
</video>
</center>

Points forts:

* Implantation complète de démineur (bravo!)
* La partie fonctionne en client/serveur
* Intégration de plusieurs pages
* Dessin de chat sur la page d'accueil

