---
bookHidden: true
---

## Sarah Kessi

<center>
<video style="max-width:100%;" controls="false" autoplay="true" loop="true">
    <source src="/4f5/presentation/projets2024/sarah/sarah01.webm">
    <source src="/4f5/presentation/projets2024/sarah/sarah01.mp4">
</video>
</center>

Points forts:

* Sauvegarde et restauration de profil d'options
* Ajustement du volume de la musique, du fond d'écran, de la taille de la fenêtre
* Animations

