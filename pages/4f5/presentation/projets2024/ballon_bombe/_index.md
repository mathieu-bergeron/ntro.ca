---
bookHidden: true
---

## Ballon-Bombe

* Rayan Mokdad, Édouard Cormier, Kalan Balaramanpillai, Charles-Antoine Laurence

<center>
<video style="max-width:70%;" controls="false" autoplay="true" loop="true">
    <source src="/4f5/presentation/projets2024/ballon_bombe/ballon_bombe.webm">
    <source src="/4f5/presentation/projets2024/ballon_bombe/ballon_bombe.mp4">
</video>
</center>

Points forts:

* Intégration de plusieurs pages (transition différente d'une page à l'autre)
* Implantation du jeu Ballon-Bombe
    * (suivi des points)

