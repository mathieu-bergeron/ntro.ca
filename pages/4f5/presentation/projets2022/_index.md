---
title: "Projets vedettes 2022"
weight: 10
bookHidden: false
---

{{% pageTitle %}}

## LightCycles

{{% video "/4f5/presentation/projets2022/light_cycles.mp4" %}}

* Équipe:
  * Andre Miguel Gonzales Loayza
  * Elouan Hildgen-Porier
  * Émilien Théoret
  * Sif Din Marchane

<br>
<br>
<br>
<br>

## Manipuler des cartes

{{% video "/4f5/presentation/projets2022/blackjack.mp4" %}}

* Charles Dufour

<br>
<br>
<br>
<br>

## Serpents et Échelles en client/serveur


{{% video "/4f5/presentation/projets2022/serpents_echelles.mp4" %}}

* Adrien Joséphine-Olivier


