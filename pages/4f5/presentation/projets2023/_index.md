---
title: "Projets vedettes 2023"
weight: 10
bookHidden: false
---

{{% pageTitle %}}


## Brandon Myre

* Points forts
    * logique complète d'inscription et de création de parties (y compris parties privées accessibles par code)
    * intégration de plusieurs pages
    * déploiement sur un serveur

    <video width="100%" autoplay="" muted="" loop="">            
        <source src="./brandon/brandon1.mp4" type="video/mp4">            
        <source src="./brandon/brandon1.webm" type="video/webm">            
    </video>

* Version déployée

    <video width="100%" autoplay="" muted="" loop="">            
        <source src="./brandon/brandon2.mp4" type="video/mp4">            
        <source src="./brandon/brandon2.webm" type="video/webm">            
    </video>

## Blackjack

* Équipe
    * Médéric Bélec
    * Liam Brouillard Adjaïlia
    * Samuel Ferragne
* Points forts
    * cartes animées
    * logique du jeu
    * implantation de boutons sur un Canvas

    <video width="100%" autoplay="" muted="" loop="">            
        <source src="./blackjack/blackjack.mp4" type="video/mp4">            
        <source src="./blackjack/blackjack.webm" type="video/webm">            
    </video>

## Justin Pagès

* Points forts
    * fragments dans une liste déroulante
    * affichage qui s'adapte à la taille de la fenêtre
    * esthétique de la page

    <video width="100%" autoplay="" muted="" loop="">            
        <source src="./justin/justin1.mp4" type="video/mp4">            
        <source src="./justin/justin1.webm" type="video/webm">            
    </video>

    <video width="100%" autoplay="" muted="" loop="">            
        <source src="./justin/justin2.mp4" type="video/mp4">            
        <source src="./justin/justin2.webm" type="video/webm">            
    </video>


## Lorenzo Mignacca

* Points forts
    * esthétique de la page
    * mémorisation de l'emplacement de la barre de défilement

    <video width="100%" autoplay="" muted="" loop="">            
        <source src="./lorenzo/lorenzo.mp4" type="video/mp4">            
        <source src="./lorenzo/lorenzo.webm" type="video/webm">            
    </video>


## Cynthia Tran

* Points forts
    * logique d'un tutoriel pour Uno
    * belle utilisation des fragments
    * messages à l'usager

    <video width="100%" autoplay="" muted="" loop="">            
        <source src="./cynthia/cynthia.mp4" type="video/mp4">            
        <source src="./cynthia/cynthia.webm" type="video/webm">            
    </video>


## Thomas Vo

* Points forts
    * utilisation de plusieurs Vues côte à côte dans la fenêtre
    * drag&drop
    * animation dans le monde2d

    <video width="100%" autoplay="" muted="" loop="">            
        <source src="./thomas/thomas1.mp4" type="video/mp4">            
        <source src="./thomas/thomas1.webm" type="video/webm">            
    </video>

    <video width="100%" autoplay="" muted="" loop="">            
        <source src="./thomas/thomas2.mp4" type="video/mp4">            
        <source src="./thomas/thomas2.webm" type="video/webm">            
    </video>
