---
title: "Exemples de pages"
weight: 60
bookHidden: false
---

{{% pageTitle %}}

* choix des couleurs du jeu
* page d'achats
* page pour réassigner les touches du jeu
* historique des parties
* tutoriel interactif
* profil du joueur
* autre page de paramètres
* n'importe quelle autre page pertinente pour votre jeu

