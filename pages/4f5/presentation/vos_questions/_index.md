---
title: "Réponses à vos questions"
weight: 100
bookHidden: false
---

{{% pageTitle %}}

## Général

### Vraiment général

* Orange juice or apple juice

> * orange

* Type de pâtes préféré et votre condiment (ketchup, moutarde, mayo) préféré ?

> * fettuccine, mayo

* C'est quoi votre show favori

> * Game of thrones (saisons 1-4)
> * The Expanse

### Jeux vidéos

* Est-ce que vous jouez à des jeux vidéos? Si oui, lesquels?
* Jouez vous aux jeux vidéos?
* Avez vous un jeu vidéo préféré.

> * Le vaillant petit page (The Plucky Squire)
> * Me faire battre à FC25


### Voyage

* Quel est le voyage que vous avez préféré faire/souhaitez faire ?
* Avez-vous voyagé durant les vacances 

> * Aller voir des aurores boréales

<center>
<img src="aurores.jpg"/>
<a href="https://commons.wikimedia.org/wiki/File:AuroreAurores-bor%C3%A9ales-7octobre2015-3-1024x657.jpg">source</a>
</center>

* Comment ont été vos vacances
* Avez vous fait des trucs constructifs pendant les vacances 

> * Bien, surtout relax
> * Lu un essai, travailler un peu sur un projet perso



## Cours

* Est-ce qu'on va travailler sur un seul projet durant toute la session ou plusieurs projets.
* Comment on va developper notre application (Duree de travaille)
* Pourquoi utiliser GitLab au lieu de GitHub qui est plus répandu et avec lequel les élèves sont plus familier.

> * Oui, tout au long de la session
> * GitLab s'approche plus de l'idéal pour ce cours: un serveur Git avec un inteface Web


## Mon parcours

* Quels sont les entreprises pour lesquelles vous avez travaillé ?
* Vos études
* Qu'est ce que t'aime le plus de ton emploi.
* Qu'est-ce qui vous a poussé a devenir un enseignant en informatique
* Quel est votre parcours professionnel?

* Avez-vous déjà travailler en tant que programmeur dans une ou des entreprises?

* Quelles sont vos expériences professionnelles ?
* Quelle est votre historique professionnel?
* Depuis combien de temps enseignez-vous à Montmorency? 
* Quel est ton parcours et ton cheminement qui t'aura permis à devenir professeur d'informatique ?

## Enseignement

* Pourquoi avez-vous choisi l'enseignement? 
* Pourqoui avez vous choisis d'être un professeur?
* Dans votre carrière en informatique, quel a été votre métier/job préféré?

## Informatique

* Votre language de programmation préfére

> * Ocaml, C 
    


* Comment s'habituer plus à coder sans nécessairement utiliser la sourie? Par exemple les petits shortcuts avec le clavier

    <center>
        <img src="vi-vim-cheat-sheet.png"/>
        <a href="http://pesin.space/imgs/posts/vi-vim-cheat-sheet.svg">source</a>
    </center>

* Votre language de programmation favoris


## Apprentissage

<center>
<img style="max-width:500px" src="./2019-06-19_effet_dunning_kruger.png"/><br>
<a href="https://fr.wikipedia.org/wiki/Effet_Dunning-Kruger" target="_blank">Article Wikipédia</a>
</center>

<br>
<br>
<br>

<center>
    <img style="max-width:500px;" src="./ai.png"/><br>
    <a href="https://youtu.be/6Lxk9NMeWHg" target="_blank">Lien vidéo</a>
</center>

## Projets personnels

* Est ce que vous vous êtes améliorer en blender

> Non, j'ai régressé en fait ;-)

* Quel est votre plus grand projet en développement ?
* Votre meilleur projet

> * En ce moment: coder un synthétiseur pour un jeu 

* Quel est le projet que vous auriez voulu accomplir dans votre vie (application à développer, logiciel, accomplissement, etc) ?

> * Smalltalk (Pharo)
> * GF (Grammatical Framework)


