---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: créer la `VuePartie`

## Créer la classe `VuePartie`

1. Dans le paquet `vues`, créer la classe `VuePartie`

1. Ajuster la signature de la classe, qui doit hériter de `ViewFx`:

    ```java
    {{% embed src="./VuePartie.java" first-line="1" last-line="1" indent-level="1" %}}
    ```

1. Avec {{% key "Ctrl+1" %}}, ajouter les `import` et la méthode obligatoire:

    ```java
    {{% embed src="./VuePartie.java" indent-level="1" %}}
    ```


    * NOTES: 
        * la méthode `initialize` est requise par Ntro
        * on va s'en servir pour vérifier que les références aux contrôles de la vue sont bien initialisés

1. S'assurer d'avoir l'arborescence suivante 

<center>
<img src="eclipse01.png" />
</center>

## Télécharger `tux.png`

1. Télécharger {{% download "tux.png" "tux.png" %}} et placer le fichier dans le répertoire `pong/src/main/resources/images/`


## Créer le fichier `partie.fxml`

1. Dans le répertoire `pong/src/main/resources/vues`, créer le fichier `partie.fxml`

1. Ajouter le code suivant dans au fichier

    ```xml
    {{% embed src="./partie.fxml" indent-level="1" %}}
    ```

1. S'assurer que `fx:controller` déclare bien le nom complet de la classe `VuePartie`
    * `pong.frontal.vues.VuePartie`

1. S'assurer d'avoir l'arborescence suivante 

<center>
<img src="eclipse02.png" />
</center>

## Ajouter le CSS et les traductions

1. Dans `dev.css`, ajouter la règle suivante:

    ```
    {{% embed src="./dev.css" indent-level="1" %}}
    ```

1. Dans `dev.css` et `prod.css`, ajouter les règles suivantes

    ```
    {{% embed src="./dev_et_prod.css" indent-level="1" %}}
    ```

1. Dans `fr.properties`, ajouter le texte pour `quitterPartie`

    ```
    quitterPartie=Quitter partie
    ```

1. Dans `en.properties`, ajouter le texte pour `quitterPartie`

    ```
    quitterPartie=Quit Game
    ```

## Déclarer la Vue dans le Frontal

1. Ouvrir `FrontalPong.java` et déclarer la `VuePartie`

    ```java
    {{% embed src="./FrontalPong.java" indent-level="1" %}}
    ```

## Ajouter la tâche pour créer la Vue

1. Dans `CreerVues.java`, ajouter la tâche suivante


    ```java
    {{% embed src="./CreerVues01.java" indent-level="1" %}}
    ```
