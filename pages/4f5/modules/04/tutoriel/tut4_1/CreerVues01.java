public class CreerVues {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("CreerVues")
        
             .waitsFor("ViewLoaders")

             .contains(subTasks -> {

                // ...

                // ajouter

                creerVuePartie(subTasks);

             });
    }

    // ...

    // ajouter
    private static void creerVuePartie(FrontendTasks subTasks) {

        subTasks.task(create(VuePartie.class))

                .waitsFor(viewLoader(VuePartie.class))

                .executesAndReturnsValue(inputs -> {
                 
                    ViewLoader<VuePartie> viewLoader = inputs.get(viewLoader(VuePartie.class));
                 
                    VuePartie vuePartie = viewLoader.createView();

                    return vuePartie;
                });
    }
