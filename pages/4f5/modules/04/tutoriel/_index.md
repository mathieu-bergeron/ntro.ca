---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: implanter la navigation

<center>
<div style="background-color:orange;width:80%;border:2px dashed black;padding:10px">
<strong>IMPORTANT</strong>
<div style="text-align:left">
<ul>
<li>Je m'assure d'avoir terminé le tutoriel 3 avant de commencer
</ul>
</div>
</center>

## Naviguer d'une page à l'autre

1. {{% link "./tut4_1/" "Créer la `VuePartie`" %}}

1. {{% link "./tut4_2/" "Implanter la gestion d'événements" %}}

1. {{% link "./tut4_3/" "Ajouter les tâches de navigation" %}}

## Utiliser la session pour mémoriser la vue courante

1. {{% link "./session01/" "Créer la SessionPong" %}} 

1. {{% link "./session02/" "Utiliser la session dans les tâches" %}} 

1. {{% link "./sauvegarder_session/" "Test et sauvegarder la session avec `$ sh gradlew saveJson`"  %}} 

## Créer le modèle

1. {{% link "./modele01/" "Créer le `ModeleFileAttente`" %}} 

1. {{% link "./modele02/" "Créer la valeur `RendezVous` et la listes de rendez-vous" %}} 

1. {{% link "./modele03/" "Créer la valeur `RendezVousComplet`" %}} 

## Afficher le modèle

1. {{% link "./vue02/" "Ajouter des contrôles à la `VueFileAttente`" %}}

1. {{% link "./frontal/" "Observer le modèle dans le frontal" %}}

1. {{% link "./afficher_rendez_vous/" "Afficher les rendez-vous en mode texte" %}} 

1. {{% link "./sauvegarder_modele/" "Sauvegarder le modèle avec `$ sh gradlew saveJson`" %}} 

## (optionnel) Capter les touches du clavier + zoomer

1. {{% link "./capter_touches/" "Capter les touches du clavier dans la `VueRacine`" %}}

1. {{% link "./zoomer/" "Zoomer en modifiant la taille de la police" %}}

## (optionnel) Changer de langue

1. {{% link "./combo_langues/" "Ajouter un menu de langue dans la `VueRacine`" %}}

1. {{% link "./changer_langue/" "Tâche pour changer de langue" %}}

## (optionnel) Animation pour changer de page

1. {{% link "./animation_vue_racine/" "Ajouter une animation à la `VueRacine`" %}}

