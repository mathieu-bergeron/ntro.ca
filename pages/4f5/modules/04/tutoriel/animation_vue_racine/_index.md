---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: animation quand on change de page

## Attributs

1. Dans la classe `VueRacine`, ajouter des attributs pour mémoriser la vue courante et l'animation

    ```java
    {{% embed src="./VueRacine01.java" indent-level="1" %}}
    ```

## Créer l'animation

1. Dans la classe `VueRacine`, ajouter des attributs pour mémoriser la vue courante et l'animation

    ```java
    {{% embed src="./VueRacine02.java" indent-level="1" %}}
    ```


## Tester

1. Exécuter pong et tester l'animation

    
    ```bash
    $ sh gradlew pong
    ```

    {{% animation src="/4f5/modules/04/tutoriel/animation_vue_racine/animation.mp4" %}}
