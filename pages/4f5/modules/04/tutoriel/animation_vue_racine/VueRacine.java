public class VueRacine extends ViewFx {

    // ...

    // modifier cette méthode
    public void afficherSousVue(ViewFx nouvelleVue) {
        if(transition.getStatus() == Status.RUNNING) return;

        if(vueCourante instanceof VueFileAttente 
                && nouvelleVue instanceof VuePartie) {

            vueCourante = null;
            transitionVersNouvelleVue(nouvelleVue, 1);
            
        } else if(vueCourante instanceof VuePartie 
                && nouvelleVue instanceof VueFileAttente) {

            vueCourante = null;
            transitionVersNouvelleVue(nouvelleVue, -1);

        } else {

            conteneurSousVue.getChildren().clear();
            conteneurSousVue.getChildren().add(nouvelleVue.rootNode());
            vueCourante = nouvelleVue;
            
        }
    }

    // ajouter cette méthode
    private void transitionVersNouvelleVue(ViewFx nouvelleVue, int direction) {

        Pane nouveauPane = nouvelleVue.rootNode();
        conteneurSousVue.getChildren().add(nouveauPane);

        Pane ancienPane = (Pane) conteneurSousVue.getChildren().get(0);
        
        double largeur = conteneurSousVue.getWidth();
        largeur = direction * largeur;
        int duree = 400;

        transition = new Timeline();
        transition.getKeyFrames().add(new KeyFrame(new Duration(0), new KeyValue(nouveauPane.translateXProperty(), largeur)));
        transition.getKeyFrames().add(new KeyFrame(new Duration(0), new KeyValue(nouveauPane.opacityProperty(), 0.0)));
        transition.getKeyFrames().add(new KeyFrame(new Duration(0), new KeyValue(ancienPane.translateXProperty(), 0)));
        transition.getKeyFrames().add(new KeyFrame(new Duration(0), new KeyValue(ancienPane.opacityProperty(), 1.0)));
        transition.getKeyFrames().add(new KeyFrame(new Duration(duree), new KeyValue(nouveauPane.translateXProperty(), 0, Interpolator.EASE_BOTH)));
        transition.getKeyFrames().add(new KeyFrame(new Duration(duree), new KeyValue(nouveauPane.opacityProperty(), 1.0, Interpolator.EASE_BOTH)));
        transition.getKeyFrames().add(new KeyFrame(new Duration(duree), new KeyValue(ancienPane.translateXProperty(), -largeur, Interpolator.EASE_BOTH)));
        transition.getKeyFrames().add(new KeyFrame(new Duration(duree), new KeyValue(ancienPane.opacityProperty(), 0.0, Interpolator.EASE_BOTH)));
        
        transition.setOnFinished(evtFx -> {
            conteneurSousVue.getChildren().remove(ancienPane);
            vueCourante = nouvelleVue;
        });
        
        transition.playFromStart();
    }

}
