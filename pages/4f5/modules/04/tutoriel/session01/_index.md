---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: créer la `SessionPong`

## Créer la classe `SessionPong`

1. Dans le paquet `frontal`, créer la classe `SessionPong`

1. S'assurer d'avoir l'arborescence suivante dans mon projet:

    <center>
    <img src="eclipse01.png" />
    </center>

1. Ajuster la signature de `SessionPong` qui doit hériter de `Session`

    ```java
    {{% embed src="./SessionPong01.java" indent-level="1" %}}
    ```

1. Au besoin, corriger les erreurs de compilation

## Ajouter un constructeur par défaut

1. Avant d'oublier, ajouter un constructeur par défaut à la `SessionPong`


    ```java
    {{% embed src="./SessionPong02.java" indent-level="1" %}}
    ```

## Déclarer la session modèle dans `FrontPong`

1. Ouvrir `FrontalPong` et ajouter le code pour déclarer la session

    ```java
    {{% embed src="./FrontalPong01.java" indent-level="1" %}}
    ```

1. Au besoin, corriger les erreurs de compilation


## Ajouter le code pour mémoriser la vue courante

1. Ajouter l'attribut `vueCourante`

    ```java
    {{% embed src="./SessionPong03.java" indent-level="1" %}}
    ```

1. Ajouter la méthode `memoriserVueCourante`

    ```java
    {{% embed src="./SessionPong04.java" indent-level="1" %}}
    ```
        

