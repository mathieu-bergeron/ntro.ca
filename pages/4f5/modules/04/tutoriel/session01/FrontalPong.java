public class FrontalPong implements FrontendFx {
    
    @Override
    public void createTasks(FrontendTasks tasks) {
        
        CreerVues.creerTaches(tasks);
        PremierAffichage.creerTaches(tasks);
        Navigation.creerTaches(tasks);
        AfficherFileAttente.creerTaches(tasks);

    }

    @Override
    public void registerEvents(EventRegistrar registrar) {
        registrar.registerEvent(EvtChangerLangue.class);
        registrar.registerEvent(EvtAfficherFileAttente.class);
        registrar.registerEvent(EvtAfficherPartie.class);
        registrar.registerEvent(EvtChangerTaillePolice.class);
    }

    @Override
    public void registerViews(ViewRegistrarFx registrar) {
        registrar.registerDefaultLocale(Ntro.buildLocale("fr"));
        registrar.registerTranslations(Ntro.buildLocale("fr"), "/traductions/fr.properties");
        registrar.registerTranslations(Ntro.buildLocale("en"), "/traductions/en.properties");
        
        if(Ntro.options().isProd()) {

            registrar.registerStylesheet("/style/prod.css");

        }else {

            registrar.registerStylesheet("/style/dev.css");

        }

        registrar.registerView(VueRacine.class, "/vues/racine.fxml");
        registrar.registerView(VueFileAttente.class, "/vues/file_attente.fxml");
        registrar.registerView(VuePartie.class, "/vues/partie.fxml");

    }


    @Override
    public void registerSessionClass(SessionRegistrar registrar) {
        registrar.registerSessionClass(SessionPong.class);
    }

}
