public class SessionPong extends Session<SessionPong> {
    
    private Class<?> vueCourante = VueFileAttente.class;
    
    public SessionPong() {
        super();
    }

    public SessionPong memoriserVueCourante(Class<?> vueCourante) {

        this.vueCourante = vueCourante;

        return this;
    }
    
    public void envoyerEvtPourAfficherVueCourante() {

      if(vueCourante.equals(VuePartie.class)) {
          
          Ntro.newEvent(EvtAfficherPartie.class)
              .trigger();

      }else {

          Ntro.newEvent(EvtAfficherFileAttente.class)
              .trigger();

      }

    }

}
