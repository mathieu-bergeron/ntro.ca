import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

public class AfficherFileAttente {
    
    public static void creerTaches(FrontendTasks tasks) {

        tasks.taskGroup("AfficherFileAttente")
        
             .waitsFor("afficherVueFileAttente")

             .contains(subTasks -> {
                 
                 // ajouter l'appel
                 afficherFileAttente(subTasks);

             });
    }

    private static void afficherFileAttente(FrontendTasks subTasks) {

        subTasks.task("afficherFileAttente")


                .waitsFor(created(VueFileAttente.class))
        
                .waitsFor(modified(ModeleFileAttente.class))

                .executes(inputs -> {
                 
                     VueFileAttente              vueFileAttente = inputs.get(created(VueFileAttente.class));
                     Modified<ModeleFileAttente> fileAttente    = inputs.get(modified(ModeleFileAttente.class));

                     ModeleFileAttente ancienneFile = fileAttente.previousValue();
                     ModeleFileAttente fileCourante = fileAttente.currentValue();

                     // Prêt à afficher les rendez-vous!
                     //
             });
    }
}
