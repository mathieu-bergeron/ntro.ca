---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: observer le modèle dans le frontal

1. Dans `frontal.taches` ajouter la classe `AfficherFileAttente`

1. En VSCode, s'assurer d'avoir l'arborescence suivante

    <center>
        <img src="eclipse01.png" />
    </center>

1. Ouvrir `AfficherFileAttente` et ajouter le `import static` suivant:

    * <span style="background-color:orange; padding:10px;border:2px dashed black;"> `import static ca.ntro.app.tasks.frontend.FrontendTasks.*;` </span>

1. Dans `AfficherFileAttente`, ajouter un groupe de tâches

    ```java
    {{% embed src="./AfficherFileAttente01.java" indent-level="1" %}}
    ```

    * NOTE:
        * le groupe de tâche au complet attend que la `VueFileAttente` soit créée, ce qui se fait dans le groupe de tâches `Initialisation`
        * dans le cas de `VuePartie`, la vue est plutôt créée dans le groupe de tâche `Navigation`

1. Ajouter la tâche `afficherFileAttente`

    ```java
    {{% embed src="./AfficherFileAttente02.java" indent-level="1" %}}
    ```

    * NOTES:
        * la tâche récupère la vue
        * la tâche observe le modèle
        * cette tâche sera donc appelée à chaque fois que le modèle est modifié

1. Dans `FrontalPong`, appeler `AfficherFileAttente.creerTaches`

    ```java
    {{% embed src="./FrontalPong01.java" indent-level="1" %}}
    ```

1. Exécuter `pong` et vérifier le mon graphe de tâches du frontal

        $ sh gradlew pong


    <center>
        <img width="100%" src="frontend.png"/>
    </center>


## Vérifier le fichier `.json`

1. Le fichier `.json` du modèle est dans `_storage/models/ModeleFileAttente.json`

1. Vérifier le contenu de `ModeleFileAttente.json`

    ```json
    {{% embed src="./ModeleFileAttente01.json" indent-level="1" %}}
    ```

## Ajouter des données au fichier `.json`

1. Ouvrir le fichier `ModeleFileAttente.json`

1. Ajouter *à la main* des rendez-vous

    ```json
    {{% embed src="./ModeleFileAttente02.json" indent-level="1" %}}
    ```

    * RAPPEL: par héritage, une `RendezVousComplet` a aussi les attributs d'un `RendezVous`

1. Ré-exécuter `pong`

        $ sh gradlew pong

1. Fermer la fenêtre afin de générer `ModeleFileAttente.png`

1. Observer que le modèle pour la file d'attente contient maitenant trois objets:

    <center>
        <img width="90%" src="ModeleFileAttente02.png">
    </center>

