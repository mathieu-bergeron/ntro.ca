---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: tester et sauvegarder la `SessionPong`


## Tester avec la session `pong:alice`

1. Lancer `pong` avec la session de `alice`

    ```bash
    $ sh gradlew pong:alice

        # doit afficher
        [INFO] session: alice
    ```

    * Naviguer à la page partie et fermer la fenêtre

    <img class="petite-figure" src="partie.png"/>

    ```bash
        # doit afficher
        [INFO] Writing JSON files
        [INFO] Generating graphs
        [INFO] Exiting
    ```

1. Vérifier que le fichier `pong/_storage/sessions/alice.json` a été créé

    ```json
    {{% embed src="alice.json" indent-level="1" %}}
    ```

    * observer que la `vueCourante` est `VuePartie`


1. Redémarrer `pong` avec la session `alice`

    ```bash
    $ sh gradlew pong:alice

        # doit afficher
        [INFO] session: alice
    ```

    * Devrait afficher directement la vue partie

    <img class="petite-figure" src="partie.png"/>


## Sauvegarder la session dans Git

1. Le répertoire `_storage` est ignoré par Git (à cause de notre `.gitignore`)

1. Pour sauvegarder la session dans Git, faire

    ```bash
    $ sh gradlew saveJson
    ```

1. Vérifier que le répertoire `pong/json/sessions` existe avec la session d'alice


1. Ajouter `alice.json` à Git

    ```bash
    $ git add .
    $ git commit -a -m"ajout de la session sauvegardée pour alice"
    $ git push
    ```

1. Pour utiliser la session sauvegardée, faire

    ```bash
    $ sh gradlew restoreJson
    ```

