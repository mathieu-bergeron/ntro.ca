---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: zoomer en modifiant la police

## Dans `Navigation`, ajouter la tâche `changerTaillePolice`

```java
{{% embed src="Navigation01.java" %}}
```

## Vérifier que ça fonctionne

```bash
$ sh gradlew pong
```

{{% animation src="/4f5/modules/04/tutoriel/zoomer/zoomer.mp4" %}}




