public class Navigation {

    public static void creerTaches(FrontendTasks tasks) {

        tasks.taskGroup("Navigation")
        
             .waitsFor("PremierAffichage")
             
             .contains(subTasks -> {

                 // ...

                 // ajouter
                 changerTaillePolice(subTasks);

             });
    }

    // ...

    private static void changerTaillePolice(FrontendTasks subTasks) {

        subTasks.task("changerTaillePolice")

                .waitsFor(window())

                .waitsFor(event(EvtChangerTaillePolice.class))

                .executes(inputs -> {
                  
                     EvtChangerTaillePolice evtChangerTaillePolice = inputs.get(event(EvtChangerTaillePolice.class));
                     Window                 window                 = inputs.get(window());
                  
                     evtChangerTaillePolice.appliquerA(window);

              });
    }
