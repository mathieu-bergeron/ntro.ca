private static void afficherVuePartie(FrontendTasks subTasks) {

    subTasks.task("afficherVuePartie")

         .waitsFor(created(VueRacine.class))
    
         .waitsFor(created(VuePartie.class))

         .waitsFor(event(EvtAfficherPartie.class))
          
         .executes(inputs -> {

             VueRacine         vueRacine         = inputs.get(created(VueRacine.class));
             VuePartie         vuePartie         = inputs.get(created(VuePartie.class));
             EvtAfficherPartie evtAfficherPartie = inputs.get(event(EvtAfficherPartie.class));

             evtAfficherPartie.appliquerA(vueRacine, vuePartie);

         });
}

private static void afficherVueFileAttente(FrontendTasks subTasks) {

    subTasks.task("afficherVueFileAttente")

         .waitsFor(created(VueRacine.class))

         .waitsFor(created(VueFileAttente.class))
    
         .waitsFor(event(EvtAfficherFileAttente.class))
          
         .executes(inputs -> {
              
             VueRacine      vueRacine                      = inputs.get(created(VueRacine.class));
             VueFileAttente vueFileAttente                 = inputs.get(created(VueFileAttente.class));
             EvtAfficherFileAttente evtAfficherFileAttente = inputs.get(event(EvtAfficherFileAttente.class));

             evtAfficherFileAttente.appliquerA(vueRacine, vueFileAttente);

         });
}
