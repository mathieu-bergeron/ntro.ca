---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: graphe de tâches

## Créer le groupe de tâches `Navigation`

1. Dans le paquet `taches`, créer la classe `Navigation`

1. Ajouter le `import static`
    
    * <span style="background-color:orange; padding:10px;border:2px dashed black;"> `import static ca.ntro.app.tasks.frontend.FrontendTasks.*;` </span>

1. Ajouter la méthode `creerTaches` 

    ```java
    {{% embed src="./Navigation01.java" indent-level="1" %}}
    ```

    * **ATTENTION** s'assurer d'avoir le `waitsFor`

1. S'assurer d'avoir l'arborescence suivante 

<center>
<img src="eclipse01.png" />
</center>

## Appeler `Navigation.creerTaches` dans le Frontal

1. J'ouvre `FontalPong` et j'ajoute l'appel à `Navigation.creerTaches`

    ```java
    {{% embed src="./FrontalPong.java" indent-level="1" %}}
    ```

1. Dans `Navigation`, coder les tâches

    ```java
    {{% embed src="./Navigation02.java" indent-level="1" %}}
    ```


1. Dans `Navigation.creerTaches`, ajouter appels de méthode

    ```java
    {{% embed src="./Navigation03.java" indent-level="1" %}}
    ```

## Ajouter les méthodes `appliquerA` dans les événements

1. Dans `EvtAfficherPartie`

    ```java
    {{% embed src="./EvtAfficherPartie01.java" indent-level="1" %}}
    ```

1. Dans `EvtAfficherFileAttente`

    ```java
    {{% embed src="./EvtAfficherFileAttente01.java" indent-level="1" %}}
    ```

## Tester que la navigation fonctionne

1. Exécuter `pong` et tester qu'on peut naviguer d'une page à l'autre

    ```bash
    $ sh gradlew pong
    ```

    {{% animation src="/4f5/modules/04/tutoriel/tut4_3/navigation.mp4" %}}

## Vérifier le graphe de tâches

1. S'assurer d'avoir le graphe de tâches suivant

    <center>
        <img width="100%" src="frontend.png"/>
    </center>

    * NOTE: le graphe est généré, alors l'emplacement exact des noeuds peut varier





