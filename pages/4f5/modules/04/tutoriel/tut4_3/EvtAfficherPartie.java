public class EvtAfficherPartie extends Event {

    public EvtAfficherPartie appliquerA(VueRacine vueRacine, VuePartie vuePartie) {
        
        vueRacine.afficherSousVue(vuePartie);
        
        return this;

    }

}
