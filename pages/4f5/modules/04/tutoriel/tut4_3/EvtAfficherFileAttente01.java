public class EvtAfficherFileAttente extends Event {

    // ajouter
    public EvtAfficherFileAttente appliquerA(VueRacine vueRacine, VueFileAttente vueFileAttente) {

        vueRacine.afficherSousVue(vueFileAttente);
        
        return this;
        
    }

}
