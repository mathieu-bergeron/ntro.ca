public static void creerTaches(FrontendTasks tasks) {

    tasks.taskGroup("Navigation")
    
         .waitsFor("PremierAffichage")

         .contains(subTasks -> {

             afficherVueFileAttente(subTasks);

             afficherVuePartie(subTasks);

         });
}
