public class EvtAfficherPartie extends Event {

    // ajouter
    public EvtAfficherPartie appliquerA(VueRacine vueRacine, VuePartie vuePartie) {
        
        vueRacine.afficherSousVue(vuePartie);
        
        return this;

    }

}
