public class EvtAfficherFileAttente extends Event {

    public EvtAfficherFileAttente appliquerA(VueRacine vueRacine, VueFileAttente vueFileAttente) {

        vueRacine.afficherSousVue(vueFileAttente);
        
        return this;
        
    }

}
