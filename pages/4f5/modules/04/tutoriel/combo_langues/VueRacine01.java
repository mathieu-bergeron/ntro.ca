public class VueRacine extends ViewFx {


    @FXML
    private ComboBox<String> comboLangues; // ajouter

    @FXML
    private StackPane conteneurSousVue; // ajouter
    
    @Override
    public void initialize() {
        // ajouter
        Ntro.assertNotNull(comboLangues);
        Ntro.assertNotNull(conteneurSousVue);

        // ...
    }

    // ajouter
    public void initialiserComboLangues() {
        comboLangues.setFocusTraversable(false);
        comboLangues.getItems().add("fr");
        comboLangues.getItems().add("en");

        comboLangues.getSelectionModel().select(Ntro.currentLocale().language());
        
        comboLangues.setOnAction(evtFx -> {

            String langue = comboLangues.getSelectionModel().getSelectedItem();

            Ntro.newEvent(EvtChangerLangue.class)
                .setLangue(langue)
                .trigger();

        });
    }

    // modifier cette méthode
    public void afficherSousVue(ViewFx nouvelleVue) {
        conteneurSousVue.getChildren().clear();
        conteneurSousVue.getChildren().add(nouvelleVue.rootNode());
    }
