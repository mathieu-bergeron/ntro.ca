public class CreerVues {
    
    // ...

    private static void creerVueRacine(FrontendTasks subTasks) {

        subTasks.task(create(VueRacine.class))

             .waitsFor(viewLoader(VueRacine.class))

             .executesAndReturnsValue(inputs -> {

                 ViewLoader<VueRacine> viewLoader = inputs.get(viewLoader(VueRacine.class));

                 VueRacine vueRacine = viewLoader.createView();

                 // ajouter
                 vueRacine.initialiserComboLangues();
                 
                 return vueRacine;
             });
    }
