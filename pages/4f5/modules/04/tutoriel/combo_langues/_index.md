---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: liste déroulante pour changer de langue

## Créer l'événement `EvtChangerLangue`

1. Dans le paquet `evenements`, créer la classe `EvtChangerLangue`

    ```java
    {{% embed src="EvtChangerLangue01.java" indent-level="1" %}}
    ```

1. Dans le `FrontalPong`, déclarer l'événement

    ```java
    {{% embed src="FrontalPong01.java" indent-level="1" %}}
    ```


## Dans `racine.fxml`, ajouter les éléments graphiques suivants

```xml
{{% embed src="racine01.fxml" %}}
```

## Dans `dev.css` et `prod.css`, ajouter le CSS suivant

```
{{% embed src="prod01.css" %}}
```

## Dans `VueRacine`, ajouter le code suivant

```java
{{% embed src="VueRacine01.java" %}}
```

## Modifier `CreerVues.creerVueRacine` pour appeler `initialiserComboLangues`

```java
{{% embed src="CreerVues01.java" %}}
```


