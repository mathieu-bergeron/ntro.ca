public class CreerVues {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("CreerVues")
        
             .waitsFor("ViewLoaders")

             .contains(subTasks -> {

                creerVueRacine(subTasks);
                creerVueFileAttente(subTasks);
                creerVuePartie(subTasks);

             });
    }

    private static void creerVuePartie(FrontendTasks subTasks) {

        subTasks.task(create(VuePartie.class))

             .waitsFor(viewLoader(VuePartie.class))

             .executesAndReturnsValue(inputs -> {
                 
                 ViewLoader<VuePartie> viewLoader = inputs.get(viewLoader(VuePartie.class));
                 
                 VuePartie vuePartie = viewLoader.createView();

                 return vuePartie;
             });
    }

    private static void creerVueRacine(FrontendTasks subTasks) {

        subTasks.task(create(VueRacine.class))

             .waitsFor(viewLoader(VueRacine.class))

             .executesAndReturnsValue(inputs -> {

                 ViewLoader<VueRacine> viewLoader = inputs.get(viewLoader(VueRacine.class));

                 VueRacine vueRacine = viewLoader.createView();
                 
                 return vueRacine;
             });
    }

    private static void creerVueFileAttente(FrontendTasks subTasks) {

        subTasks.task(create(VueFileAttente.class))

             .waitsFor(viewLoader(VueFileAttente.class))

             .executesAndReturnsValue(inputs -> {
                 
                 ViewLoader<VueFileAttente> viewLoaderFileAttente = inputs.get(viewLoader(VueFileAttente.class));
                 
                 VueFileAttente vueFileAttente = viewLoaderFileAttente.createView();
                 
                 return vueFileAttente;

             });
    }

}
