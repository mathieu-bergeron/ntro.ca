public class VuePartie extends ViewFx {
    
    @Override
    public void initialize() {
        
        // ...

        installerEvtAfficherFileAttente();
    }


    private void installerEvtAfficherFileAttente() {

        boutonQuitterPartie.setOnAction(evtFx -> {

            Ntro.logger().info("[VuePartie] clic: " + evtFx.getEventType());

        });
    }
}
