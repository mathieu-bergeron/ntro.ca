private static void creerVueRacine(FrontendTasks subTasks) {

    subTasks.task(create(VueRacine.class))

         .waitsFor(viewLoader(VueRacine.class))
         
         .executesAndReturnsValue(inputs -> {

             ViewLoader<VueRacine> viewLoader = inputs.get(viewLoader(VueRacine.class));
             
             VueRacine vueRacine = viewLoader.createView();
             
             return vueRacine;
         });
}

private static void installerVueRacine(FrontendTasks subTasks) {

    subTasks.task("installerVueRacine")
    
          .waitsFor(window())
          
          .waitsFor(created(VueRacine.class))
          
          .executes(inputs -> {
              
              VueRacine vueRacine = inputs.get(created(VueRacine.class));
              Window    window    = inputs.get(window());

              window.installRootView(vueRacine);
          });
}

private static void creerVueFileAttente(FrontendTasks subTasks) {

    subTasks.task(create(VueFileAttente.class))

         .waitsFor(viewLoader(VueFileAttente.class))

         .executesAndReturnsValue(inputs -> {
             
             ViewLoader<VueFileAttente> viewLoader = inputs.get(viewLoader(VueFileAttente.class));
             
             VueFileAttente vueFileAttente = viewLoader.createView();

             return vueFileAttente;
         });
}


private static void installerVueFileAttente(FrontendTasks subTasks) {

    subTasks.task("installerVueFileAttente")
    
          .waitsFor("installerVueRacine")

          .waitsFor(created(VueFileAttente.class))

          .executes(inputs -> {
              
              VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
              VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));

              vueRacine.afficherSousVue(vueFileAttente);
          });
}


private static void afficherFenetre(FrontendTasks subTasks) {
    subTasks.task("afficherFenetre")
    
         .waitsFor(window())
         
         .executes(inputs -> {

             Window window = (Window) inputs.get(window());

             window.show();
         });
}
