---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: gestion d'événements usager

## Créer les événements `Ntro`

1. En VSCode, créer le paquet `pong.frontal.evenements`

1. Dans le paquet `evenements`, créer les classes 
    * `EvtAfficherFileAttente`
    * `EvtAfficherPartie`

1. NOTE: la classe est nommée `EvtNomEvenement` par convention

1. Ajuster les signatures de ces classes, qui doivent hériter de `ca.ntro.app.Event`

    ```java
    {{% embed src="./EvtAfficherFileAttente.java" indent-level="1" %}}
    ```
    ```java
    {{% embed src="./EvtAfficherPartie.java" indent-level="1" %}}
    ```

1. Avec {{% key "Ctrl+1" %}}, ajouter les `import`

    * **ATTENTION** importer `ca.ntro.app.Event`

1. S'assurer d'avoir l'arborescence suivante 

<center>
<img src="eclipse01.png" />
</center>

## Déclarer les événements `Ntro` dans le Frontal


1. Déclarer les événements `Ntro` dans le Frontal

    ```java
    {{% embed src="./FrontalPong.java" indent-level="1" %}}
    ```


## Récupérer les contrôles des Vues JavaFx

1. S'assurer d'avoir l'attribut `fx:id` sur chaque bouton:

    * dans `file_attente.fxml`

        ```xml
        {{% embed src="./file_attente.fxml" indent-level="2" %}}
        ```

    * dans `partie.fxml`

        ```xml
        {{% embed src="./partie.fxml" indent-level="2" %}}
        ```

1. NOTE: le `fx:id` et `id` ne jouent pas le même rôle
    * `fx:id` identifiant JavaFx 
    * `id` identifiant CSS

1. Les classes `VueFileAttente` et `VuePartie`, ajouter un attribut pour le bouton

    {{<excerpt>}}

**ATTENTION** ci-bas il faut importer `javafx.scene.control.Button`

    {{</excerpt>}}

    * dans `VueFileAttente`

        ```java
        {{% embed src="./VueFileAttente01.java" indent-level="2" %}}
        ```

    * dans `VuePartie`

        ```java
        {{% embed src="./VuePartie01.java" indent-level="2" %}}
        ```


1. S'assurer que:
    * le nom de l'attribut correspond exactement au `fx:id` du fichier `.fxml`
    * l'annotation `@FXML` est directement au dessus de l'attribut
    * utiliser une ligne `Ntro.assertNotNull` pour détecter rapidement une erreur

## Créer les capteurs d'événements JavaFx

1. Dans les classes `VueFileAttente` et `VuePartie`, capter les événements JavaFx

    * dans `VueFileAttente`

        ```java
        {{% embed src="./VueFileAttente02.java" indent-level="2" %}}
        ```

    * dans `VuePartie`

        ```java
        {{% embed src="./VuePartie02.java" indent-level="2" %}}
        ```

1. Exécuter `pong` et je s'assurer que le capteur fonctionne

        $ sh gradlew pong

            # Dois afficher quand je cliques sur le bouton

            [INFO] [VueFileAttente] clic: ACTION
            [INFO] [VueFileAttente] clic: ACTION
            [INFO] [VueFileAttente] clic: ACTION
            

## Créer et déclencher les événements `Ntro`

1. Dans les classes `VueFileAttente` et `VuePartie`, ajouter

    * dans `VueFileAttente`

        ```java
        {{% embed src="./VueFileAttente03.java" indent-level="2" %}}
        ```

    * dans `VuePartie`

        ```java
        {{% embed src="./VuePartie03.java" indent-level="2" %}}
        ```

1. Observer qu'on crée et déclenche l'événement Ntro en même temps
