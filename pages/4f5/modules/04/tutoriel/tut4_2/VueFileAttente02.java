public class VueFileAttente extends ViewFx {
    
    @Override
    public void initialize() {

        // ...

        installerEvtAfficherPartie();
    }

    private void installerEvtAfficherPartie() {

        boutonDebuterPartie.setOnAction(evtFx -> {
            
            Ntro.logger().info("[VueFileAttente] clic:" + evtFx.getEventType());

        });
    }
}
