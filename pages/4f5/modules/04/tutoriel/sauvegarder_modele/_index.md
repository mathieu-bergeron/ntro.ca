---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: sauvegarder `ModeleFileAttente.json` dans Git


## Sauvegarder un modèle dans Git

1. Le répertoire `_storage` est ignoré par Git (à cause de notre `.gitignore`)

1. Pour sauvegarder un modèle dans Git, faire

    ```bash
    $ sh gradlew saveJson
    ```

1. Vérifier que le répertoire `pong/json/models` existe avec le modèle


1. Ajouter `ModeleFileAttente.json` à Git

    ```bash
    $ git add .
    $ git commit -a -m"ajout d'un modèle sauvegardé"
    $ git push
    ```

1. Pour utiliser le modèle sauvegardé, faire

    ```bash
    $ sh gradlew restoreJson
    ```

