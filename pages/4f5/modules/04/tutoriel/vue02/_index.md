---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: améliorer la `VueFileAttente`

1. Ajouter les contrôles suivants à `file_attente.fxml`

    ```xml
    {{% embed src="./file_attente.fxml" indent-level="1" %}}
    ```

1. Ajouter le CSS suivant dans `dev.css` et `prod.css` 

    ```
    {{% embed src="./dev.css" indent-level="1" %}}
    ```

1. Ajouter le texte suivant à `traductions/fr.properties`

    ```ini
    sInscrire=S'inscrire
    ```

1. Ajouter le texte suivant à `traductions/en.properties`

    ```ini
    sInscrire=Register
    ```

1. Ouvrir la classe `VueFileAttente` et ajouter l'attribut `labelRendezVous`

    ```java
    {{% embed src="./VueFileAttente01.java" indent-level="1" %}}
    ```


1. Je corrige les erreurs de compilation au besoin

    {{<excerpt>}}

**ATTENTION** il faut importer le `Label` de `javafx.scene.control`

    {{</excerpt>}}

1. Pour l'instant, on devrait avoir

    ```bash
    $ sh gradlew pong
    ```

    <center>
    <img class="figure" src="vue01.png"/>
    <center>
