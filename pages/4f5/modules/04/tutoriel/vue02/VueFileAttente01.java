public class VueFileAttente extends ViewFx {

    @FXML
    private Button boutonSInscrire;

    @FXML
    private Button boutonDebuterPartie;

    @FXML
    private Label labelRendezVous;

    @Override
    public void initialize() {

        // ajouter
        Ntro.assertNotNull(boutonSInscrire);
        Ntro.assertNotNull(boutonDebuterPartie);
        Ntro.assertNotNull(labelRendezVous);

        // ajouter
        labelRendezVous.setText("TEST\n\nTEST\n\nTEST\n\nTEST");

        // ...

    }

    // ...
