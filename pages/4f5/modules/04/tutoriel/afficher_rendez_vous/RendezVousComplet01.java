public class RendezVousComplet extends RendezVous {
    
    private Joueur deuxiemeJoueur;

    // ...

    @Override
    public String toString() {
        return premierJoueur().nomCourt() + " Vs " + deuxiemeJoueur.nomCourt();
    }
