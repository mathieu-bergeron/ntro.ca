public class VueFileAttente extends ViewFx {

    // ...

    @FXML
    private Label labelRendezVous;

    // ...

    public void afficherRendezVousEnTexte(String texteRendezVous) {
        labelRendezVous.setText(texteRendezVous);
    }
