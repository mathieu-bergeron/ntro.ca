---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: afficher en mode texte

1. Ajouter `ModeleFileAttente.afficherSur`

    ```java
    {{% embed src="./ModeleFileAttente01.java" indent-level="1" %}}
    ```

1. Ajouter `VueFileAttente.afficherRendezVousEnTexte`

    ```java
    {{% embed src="./VueFileAttente01.java" indent-level="1" %}}
    ```

1. Dans la tâche `afficherFileAttente`, ajouter l'appel à `afficherSur`

    ```java
    {{% embed src="./AfficherFileAttente01.java" indent-level="1" %}}
    ```

1. Vérifier que ça fonctionne


        $ sh gradlew pong

    <center>
        <img width="75%" src="vue01.png"/>
    </center>

1. Il reste définir `ModeleFileAttente.toString` correctement

    ```java
    {{% embed src="./ModeleFileAttente02.java" indent-level="1" %}}
    ```

1. Ce qui veut dire définir `RendezVous.toString` correctement

    ```java
    {{% embed src="./RendezVous01.java" indent-level="1" %}}
    ```

1. Puis, définir `RendezVousComplet.toString` correctement

    ```java
    {{% embed src="./RendezVousComplet01.java" indent-level="1" %}}
    ```

1. Finalement, ajouter ces méthodes à `Joueur`

    ```java
    {{% embed src="./Joueur01.java" indent-level="1" %}}
    ```

1. On devrait maintenant avoir l'affichage complet

        $ sh gradlew pong

    <center>
        <img width="75%" src="vue02.png"/>
    </center>

1. On peut aussi vérifier en modifiant le fichier `ModeleFileAttente.json`

    {{% animation src="/4f5/modules/04/tutoriel/afficher_rendez_vous/modifier_json.mp4" %}}


