public class Joueur implements ModelValue {

    private String id;
    private String prenom;
    private String nom;

    // ...

    public String nomComplet() {
        return prenom + " " + nom;
    }

    public String nomCourt() {
        return prenom;
    }
