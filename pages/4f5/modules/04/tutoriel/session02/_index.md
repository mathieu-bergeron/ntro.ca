---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: utiliser la `SessionPong`

## Ajouter une méthode `appliquerA` dans les événements

1. Dans `EvtAfficherPartie`

    ```java
    {{% embed src="./EvtAfficherPartie01.java" indent-level="1" %}}
    ```

1. Dans `EvtAfficherFileAttente`

    ```java
    {{% embed src="./EvtAfficherFileAttente01.java" indent-level="1" %}}
    ```

## Dans les tâche de `Navigation`, mémoriser la vue courante

1. Dans `Navigation`, modifier la méthode `afficherVuePartie`

    ```java
    {{% embed src="./Navigation01.java" indent-level="1" %}}
    ```

1. Dans `Navigation`, modifier la méthode `afficherVueFileAttente`

    ```java
    {{% embed src="./Navigation02.java" indent-level="1" %}}
    ```


## Dans les tâches du `PremierAffichage`, afficher la vue courante

1. Dans `SessionPong`, ajouter une méthode pour déclencher l'événement selon la vue courante

    ```java
    {{% embed src="./SessionPong01.java" indent-level="1" %}}
    ```

1. Dans `PremierAffichage.choisirPremiereVue`, appler la nouvelle métode

    ```java
    {{% embed src="./PremierAffichage01.java" indent-level="1" %}}
    ```

