public class PremierAffichage {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("PremierAffichage")
        
             .waitsFor("CreerVues")                    
             
             .contains(subTasks -> {

                installerVueRacine(subTasks);

                afficherFenetre(subTasks);

                choisirPremiereVue(subTasks);

             });
    }


    private static void choisirPremiereVue(FrontendTasks tasks) {

        tasks.task("choisirPremiereVue")
        
              .waitsFor(session(SessionPong.class))                

              .waitsFor(created(VueRacine.class))                

              .waitsFor(created(VueFileAttente.class))                

              .waitsFor(created(VuePartie.class))                
        
              .executes(inputs -> {

                  SessionPong session = inputs.get(session(SessionPong.class));
                  
                  session.envoyerEvtPourAfficherVueCourante();

              });
    }

    private static void installerVueRacine(FrontendTasks tasks) {

        tasks.task("installerVueRacine")
        
              .waitsFor(window())
              
              .waitsFor(created(VueRacine.class))
              
              .executes(inputs -> {
                  
                  VueRacine vueRacine = inputs.get(created(VueRacine.class));
                  Window    window    = inputs.get(window());

                  window.installRootView(vueRacine);
              });
    }


    private static void afficherFenetre(FrontendTasks tasks) {
        tasks.task("afficherFenetre")

             .waitsFor(window())

             .executes(inputs -> {

                 Window window = inputs.get(window());

                 window.resize(1200, 800);
                 
                 window.setTitle("Pong");
                 
                 window.show();

             });
    }

}
