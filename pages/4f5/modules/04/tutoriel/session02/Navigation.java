public class Navigation {

    public static void creerTaches(FrontendTasks tasks) {

        tasks.taskGroup("Navigation")
        
             .waitsFor("PremierAffichage")
             
             .contains(subTasks -> {

                 afficherVueFileAttente(subTasks);

                 afficherVuePartie(subTasks);

                 changerLangue(subTasks);

                 changerTaillePolice(subTasks);

             });
    }


    private static void afficherVuePartie(FrontendTasks subTasks) {

        subTasks.task("afficherVuePartie")
        
                .waitsFor(event(EvtAfficherPartie.class))

                .waitsFor(created(VueRacine.class))

                .waitsFor(created(VuePartie.class))

                .executes(inputs -> {

                    SessionPong       session           = Ntro.session();
                    EvtAfficherPartie evtAfficherPartie = inputs.get(event(EvtAfficherPartie.class));
                    VueRacine         vueRacine         = inputs.get(created(VueRacine.class));
                    VuePartie         vuePartie         = inputs.get(created(VuePartie.class));
                 

                    evtAfficherPartie.appliquerA(session)
                                     .appliquerA(vueRacine, vuePartie);

             });
        
    }

    private static void afficherVueFileAttente(FrontendTasks tasks) {

        tasks.task("afficherVueFileAttente")

              .waitsFor(event(EvtAfficherFileAttente.class))

              .waitsFor(created(VueRacine.class))

              .waitsFor(created(VueFileAttente.class))

              .executes(inputs -> {
                  
                  SessionPong            session                = Ntro.session();
                  EvtAfficherFileAttente evtAfficherFileAttente = inputs.get(event(EvtAfficherFileAttente.class));
                  VueRacine              vueRacine              = inputs.get(created(VueRacine.class));
                  VueFileAttente         vueFileAttente         = inputs.get(created(VueFileAttente.class));

                  evtAfficherFileAttente.appliquerA(session)
                                        .appliquerA(vueRacine, vueFileAttente);
                  
              });
    }

    private static void changerLangue(FrontendTasks tasks) {

        tasks.task("changerLangue")

              .waitsFor(event(EvtChangerLangue.class))

              .executes(inputs -> {

                  EvtChangerLangue evtChangerLangue = inputs.get(event(EvtChangerLangue.class));
                  
                  evtChangerLangue.appliquer();

              });
    }

    private static void changerTaillePolice(FrontendTasks tasks) {

        tasks.task("changerTaillePolice")

              .waitsFor(window())

              .waitsFor(event(EvtChangerTaillePolice.class))

              .executes(inputs -> {
                  
                  EvtChangerTaillePolice evtChangerTaillePolice = inputs.get(event(EvtChangerTaillePolice.class));
                  Window                 window                 = inputs.get(window());
                  
                  evtChangerTaillePolice.appliquerA(window);

              });
    }

}
