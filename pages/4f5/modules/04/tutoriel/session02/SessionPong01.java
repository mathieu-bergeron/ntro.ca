public class SessionPong extends Session<SessionPong> {

    // ...

    // ajouter
    public void envoyerEvtPourAfficherVueCourante() {

      if(vueCourante.equals(VuePartie.class)) {
          
          Ntro.newEvent(EvtAfficherPartie.class)
              .trigger();

      }else {

          Ntro.newEvent(EvtAfficherFileAttente.class)
              .trigger();
      }
    }
