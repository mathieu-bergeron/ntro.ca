private static void choisirPremiereVue(FrontendTasks subTasks) {

    subTasks.task("choisirPremiereVue")

            .waitsFor(created(VueRacine.class))                

            .waitsFor(created(VueFileAttente.class))                

             // ajouter
            .waitsFor(created(VuePartie.class))                
  
             // ajouter
            .waitsFor(session(SessionPong.class))                
    
            .executes(inputs -> {

                // remplacer le code de cette tâche par
              
                SessionPong session = inputs.get(session(SessionPong.class));
              
                session.envoyerEvtPourAfficherVueCourante();

          });
}
