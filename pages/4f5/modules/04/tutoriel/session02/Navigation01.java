private static void afficherVuePartie(FrontendTasks subTasks) {

    subTasks.task("afficherVuePartie")
    
         .waitsFor(event(EvtAfficherPartie.class))

         .waitsFor(created(VueRacine.class))

         .waitsFor(created(VuePartie.class))

         .executes(inputs -> {

             // ajouter
             SessionPong       session           = Ntro.session();

             EvtAfficherPartie evtAfficherPartie = inputs.get(event(EvtAfficherPartie.class));
             VueRacine         vueRacine         = inputs.get(created(VueRacine.class));
             VuePartie         vuePartie         = inputs.get(created(VuePartie.class));
             
             // modifier
             evtAfficherPartie.appliquerA(session)
                              .appliquerA(vueRacine, vuePartie);

         });
    
}
