public class EvtAfficherPartie extends Event {

    // ...

    // ajouter
    public EvtAfficherPartie appliquerA(SessionPong session) {

        session.memoriserVueCourante(VuePartie.class);

        return this;
    }
