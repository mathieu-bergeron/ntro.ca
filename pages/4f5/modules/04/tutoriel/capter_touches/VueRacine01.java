public class VueRacine extends ViewFx {

    // ...
    
    @Override
    public void initialize() {

        //...

        // ajouter
        installerRaccourcisClavier();
    }

    // ajouter
    private void installerRaccourcisClavier() {

        rootNode().addEventFilter(KeyEvent.KEY_PRESSED, evtFx -> {
            
            switch(evtFx.getCode()) {

                case EQUALS:
                    Ntro.newEvent(EvtChangerTaillePolice.class)
                        .setFacteur(1.1)
                        .trigger();
                    break;

                case MINUS:
                    Ntro.newEvent(EvtChangerTaillePolice.class)
                        .setFacteur(0.9)
                        .trigger();
                    break;
                    
                default:
                    Ntro.logger().info("key pressed: " + evtFx.getCode());
                    break;
            }
            
        });

    }
