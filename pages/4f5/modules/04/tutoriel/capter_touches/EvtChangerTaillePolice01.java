public class EvtChangerTaillePolice extends Event {
    
    private double facteur;

    public EvtChangerTaillePolice setFacteur(double facteur) {
        this.facteur = facteur;
        
        return this;
    }

    public void appliquerA(Window window) {
        window.multiplyFontSizeBy(facteur);
    }

}
