---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: capter les touches sur la `VueRacine`


## Créer l'événement `EvtChangerTaillePolice`

1. Dans le paquet `evenements`, créer la classe `EvtChangerTaillePolice`

    ```java
    {{% embed src="EvtChangerTaillePolice01.java" indent-level="1" %}}
    ```

1. Dans le `FrontalPong`, déclarer l'événement

    ```java
    {{% embed src="FrontalPong01.java" indent-level="1" %}}
    ```

## Capter les touches dans `VueRacine`

1. Ajouter le code suivant à `VueRacine`

    ```java
    {{% embed src="VueRacine01.java" indent-level="1" %}}
    ```
