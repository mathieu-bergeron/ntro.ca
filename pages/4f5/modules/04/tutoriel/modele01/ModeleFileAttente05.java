public class ModeleFileAttente implements Model, WatchJson, WriteObjectGraph {
    
    private String idFileAttente = "filePrincipale";

    public String getIdFileAttente() {
        return idFileAttente;
    }

    public void setIdFileAttente(String idFileAttente) {
        this.idFileAttente = idFileAttente;
    }

    // ...
}
