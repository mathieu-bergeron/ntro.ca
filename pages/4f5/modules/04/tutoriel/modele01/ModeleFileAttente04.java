public class ModeleFileAttente implements Model, WatchJson, WriteObjectGraph {

    private long prochainIdRendezVous = 1;

    public long getProchainIdRendezVous() {
        return prochainIdRendezVous;
    }

    public void setProchainIdRendezVous(long prochainIdRendezVous) {
        this.prochainIdRendezVous = prochainIdRendezVous;
    }
    
    // ...
