---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: créer le `ModeleFileAttente`

## Créer le paquet `commun`

1. Dans `pong`, créer le paquet `commun`

    * c'est le paquet racine pour ce qui est commun au frontal et au dorsal

## Créer le paquet `commun.modeles`

1. Dans `pong`, créer le paquet `commun.modeles`

    * les modèles sont communs au Dorsal et au Frontal

1. S'assurer d'avoir l'arborescence suivante dans mon projet:

    <center>
    <img src="eclipse01.png" />
    </center>

## Créer la classe `ModeleFileAttente`

1. Dans le paquet `modeles`, créer la classe `ModeleFileAttente`

1. S'assurer d'avoir l'arborescence suivante dans mon projet:

    <center>
    <img src="eclipse02.png" />
    </center>

1. Ajuster la signature de `ModeleFileAttente` qui doit implanter `Model`

    ```java
    {{% embed src="./ModeleFileAttente01.java" indent-level="1" %}}
    ```

1. Au besoin, corriger les erreurs de compilation

1. Ajuster la signature de `ModeleFileAttente` pour ajouter des options

    ```java
    {{% embed src="./ModeleFileAttente01b.java" indent-level="1" %}}
    ```

    * `WatchJson`: Ntro va recharger le fichier `.json` s'il change sur le disque
    * `WriteObjectGraph`: Ntro va générer le graphe d'objet en quittant

1. Au besoin, corriger les erreurs de compilation


## Ajouter un constructeur par défaut

1. Avant d'oublier, ajouter un constructeur par défaut au `ModeleFileAttente`


    ```java
    {{% embed src="./ModeleFileAttente02.java" indent-level="1" %}}
    ```

    * NOTES: 
        * le constructeur par défaut est **obligatoire**
        * c'est nécessaire afin de créer un objet Java à partir d'un fichier `.json`

## Déclarer le modèle dans `AppPong`

1. Ouvrir `AppPong` et ajouter le code pour déclarer le modèle

    ```java
    {{% embed src="./ClientPong01.java" indent-level="1" %}}
    ```

1. Au besoin, corriger les erreurs de compilation



## Ajouter un attribut au modèle

1. Ajouter l'attribut `prochainIdRendezVous` au modèle

    ```java
    {{% embed src="./ModeleFileAttente03.java" indent-level="1" %}}
    ```
        

