public class Navigation {

    public static void creerTaches(FrontendTasks tasks) {

        tasks.taskGroup("Navigation")
        
             .waitsFor("PremierAffichage")
             
             .contains(subTasks -> {

                 // ...
               
                 

                 // ajouter
                 changerLangue(subTasks);

             });
    }

    // ...


    // ajouter
    private static void changerLangue(FrontendTasks tasks) {

        tasks.task("changerLangue")

              .waitsFor(event(EvtChangerLangue.class))

              .executes(inputs -> {

                  EvtChangerLangue evtChangerLangue = inputs.get(event(EvtChangerLangue.class));
                  
                  evtChangerLangue.appliquer();

              });
    }

    // ...
