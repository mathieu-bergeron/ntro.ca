public class Navigation {

    public static void creerTaches(FrontendTasks subTasks) {

        subTasks.taskGroup("Navigation")
        
                .waitsFor("PremierAffichage")
             
                .contains(subTasks -> {

                    // ...
               
                 

                    // ajouter
                    changerLangue(subTasks);

             });
    }

    // ...


    // ajouter
    private static void changerLangue(FrontendTasks subTasks) {

        subTasks.task("changerLangue")

                .waitsFor(event(EvtChangerLangue.class))

                .executes(inputs -> {

                    EvtChangerLangue evtChangerLangue = inputs.get(event(EvtChangerLangue.class));
                  
                    evtChangerLangue.appliquer();

              });
    }

    // ...
