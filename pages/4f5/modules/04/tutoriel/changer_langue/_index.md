---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: tâche pour changer de langue

## Dans `Navigation`, ajouter la tâche suivante

```java
{{% embed src="Navigation01.java" indent-level="1" %}}
```

## Vérifier que ça fonctionne

```bash
$ sh gradlew pong
```

{{% animation src="/4f5/modules/04/tutoriel/changer_langue/changer_langue.mp4" %}}

