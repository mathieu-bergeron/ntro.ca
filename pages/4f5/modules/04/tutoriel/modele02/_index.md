---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: ajouter la liste de rendez-vous

## Créer le paquet `valeurs`

1. Dans `commun`, créer le paquet `valeurs`

1. S'assurer d'voir l'arborescence suivante 

    <center>
    <img src="eclipse01.png" />
    </center>

## Créer la valeur `Joueur`

1. Dans le paquet `valeurs`, créer la classe `Joueur`

1. S'assurer d'avoir l'arborescence suivante dans mon projet:

    <center>
    <img src="eclipse02.png" />
    </center>

1. Ajuster la signature de `Joueur` qui doit implanter `ModelValue`

    ```java
    {{% embed src="./Joueur01.java" indent-level="1" %}}
    ```

1. Au besoin, corriger les erreurs de compilation

1. Ajouter le constructeur par défaut:

    ```java
    {{% embed src="./Joueur01b.java" indent-level="1" %}}
    ```

1. Ajouter les attributs suivants

    ```java
    {{% embed src="./Joueur02.java" indent-level="1" %}}
    ```

1. Déclarer la valeur `Joueur` dans `AppPong`

    ```java
    {{% embed src="./AppPong00.java" indent-level="1" %}}
    ```

## Créer la valeur `RendezVous`

1. Dans le paquet `valeurs`, créer la classe `RendezVous`

1. S'assurer d'avoir l'arborescence suivante dans mon projet:

    <center>
    <img src="eclipse03.png" />
    </center>

1. Ajuster la signature de `RendezVous` qui doit implanter `ModelValue`

    ```java
    {{% embed src="./RendezVous01.java" indent-level="1" %}}
    ```

1. Au besoin, corriger les erreurs de compilation

1. Ajouter le constructeur par défaut:

    ```java
    {{% embed src="./RendezVous01b.java" indent-level="1" %}}
    ```

1. Ajouter les attributs d'un rendez-vous

    ```java
    {{% embed src="./RendezVous03.java" indent-level="1" %}}
    ```

1. Déclarer la valeur `RendezVous` dans `AppPong`

    ```java
    {{% embed src="./AppPong01.java" indent-level="1" %}}
    ```

1. Au besoin, corriger les erreurs de compilation


## Ajouter la liste `rendezVousDansOrdre`

1. Dans `ModeleFileAttente`, ajouter la liste de rendez-vous

    ```java
    {{% embed src="./ModeleFileAttente04.java" indent-level="1" %}}
    ```

1. Au besoin, corriger les erreurs de compilation

    * **ATTENTION** s'assurer d'importer `java.util.List`

