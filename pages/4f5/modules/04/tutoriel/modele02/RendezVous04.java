public class RendezVous implements ModelValue {
    
    private String idRendezVous;
    private Joueur premierJoueur;

    public Joueur getPremierJoueur() {
        return premierJoueur;
    }

    public void setPremierJoueur(Joueur premierJoueur) {
        this.premierJoueur = premierJoueur;
    }

    public String getIdRendezVous() {
        return idRendezVous;
    }

    public void setIdRendezVous(String idRendezVous) {
        this.idRendezVous = idRendezVous;
    }


    // ...

}
