---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 4: ajouter la valeur `RendezVousComplet`

## Créer la valeur `RendezVousComplet`

1. Dans le paquet `valeurs`, créer la classe `RendezVousComplet`

1. S'assurer d'avoir l'arborescence suivante dans mon projet:

    <center>
    <img src="eclipse01.png" />
    </center>

1. Ajuster la signature de `RendezVousComplet`, qui hérite de `RendezVous`

    ```java
    {{% embed src="./RendezVousComplet01.java" first-line="1" last-line="1" indent-level="1" %}}
    ```

1. Ajouter le constructeur par défaut de `RendezVous`

    ```java
    {{% embed src="./RendezVousComplet01.java" indent-level="1" %}}
    ```

1. Ajouter les attributs d'un rendez-vous complet

    ```java
    {{% embed src="./RendezVousComplet02.java" indent-level="1" %}}
    ```

    * NOTE: par héritage, on a déjà les attributs d'un `RendezVous`

1. Déclarer la valeur `RendezVousComplet` dans `AppPong`

    ```java
    {{% embed src="./ClientPong01.java" indent-level="1" %}}
    ```

1. Au besoin, corriger les erreurs de compilation

<!--


## Ajouter une `RendezVousComplet` dans le fichier `.json`

1. J'ouvre le fichier `ModeleFileAttente.json`

1. J'ajoute *à la main* une partie en cours

    ```json
    {{% embed src="./ModeleFileAttente.json" indent-level="1" %}}
    ```

    * RAPPEL: par héritage, une `RendezVousComplet` a aussi les attributs d'un `RendezVous`

1. J'exécute le client `pong`

        $ sh gradlew pong:local

1. Je quitte le client `pong` afin de générer `ModeleFileAttente.png`

1. J'observe que le modèle pour la file d'attente contient maitenant trois objets:

    <center>
        <img width="90%" src="ModeleFileAttente.png">
    </center>

-->
