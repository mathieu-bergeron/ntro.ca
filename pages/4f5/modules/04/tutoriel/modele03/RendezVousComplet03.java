public class RendezVousComplet extends RendezVous {

    // ...
    
    private Joueur deuxiemeJoueur;
    private String idPartie;

    public Joueur getDeuxiemeJoueur() {
        return deuxiemeJoueur;
    }

    public void setDeuxiemeJoueur(Joueur deuxiemeJoueur) {
        this.deuxiemeJoueur = deuxiemeJoueur;
    }

    public String getIdPartie() {
        return idPartie;
    }

    public void setIdPartie(String idPartie) {
        this.idPartie = idPartie;
    }
