
tasks.task("observerModele")

     .waitsFor(modified(MonModele.class))

     .executes(inputs -> {
         
         Modified<MonModele> modifiedMonModele = inputs.get(modified(MonModele.class));

         MonModele monModele = modifiedMonModele.currentValue();

     });
