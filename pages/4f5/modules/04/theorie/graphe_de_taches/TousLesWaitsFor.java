subTasks.task("choisirPremiereVue")

      // doit déclarer CHAQUE dépendence
      .waitsFor(created(VueRacine.class))
      .waitsFor(created(VueFileAttente.class))

      .executes(inputs -> {
          
          // inputs.get appelé UNIQUEMENT sur une dépendance déclarée
          VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
          VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));
          
          vueRacine.afficherSousVue(vueFileAttente);
          
      });
