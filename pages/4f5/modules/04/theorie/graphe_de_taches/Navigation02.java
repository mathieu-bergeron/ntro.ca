public class Navigation {

    public static void creerTaches(FrontendTasks tasks) {

        tasks.taskGroup("Navigation")
        
             .waitsFor("Initialisation")

             .contains(subTasks -> {

                 afficherVueFileAttente(subTasks);

                 creerVuePartie(subTasks);
                 afficherVuePartie(subTasks);

             });
    }
}
