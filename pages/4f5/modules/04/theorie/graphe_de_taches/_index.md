---
title: ""
weight: 1
bookHidden: true
---


# Graphe de tâches (2)

## Éviter dépendances cycliques

<center>
<video width="50%" src="dependances_cycliques.mp4" type="video/mp4" controls playsinline>
</center>

* Qu'est-ce qui se passe si on écrit la tâche suivante?

    ```java
    {{% embed src="Cycle01.java" ident-level="1" %}}
    ```
* Dans le graphe, on voit qu'on a créé une dépendance cyclique

    <center>
        <img src="cycle01.png"/>
    </center>

    * avant d'exécuter `VueRacine`, il faut terminer `VueRacine`... oups!

* Comme c'est impossible à exécuter, `Ntro` va donner un message d'erreur

        [FATAL] cyclic dependancy for task VueRacine
            Please correct graph.

## Il faut écrire tous les `waitsFor` d'une tâche

* Une tâche doit spécifier tous ses `waitsFor` de façon explicite

    ```java
    {{% embed src="TousLesWaitsFor.java" ident-level="1" %}}
    ```


## Main les `waitsFor` ne sont pas tous affichés

* Avec les groupes de tâches, un `waitsFor` déjà disponible ne sera pas affiché

    <center>
        <img width="100%" src="frontend.png"/>
    </center>

    * dans `PremierAffichage`, les `waitsFor` à `VueRacine` et `VueFileAttente` ne sont pas affichés
    * ces `waitsFor` sont déjà compris dans la dépendance à `CreerVues`


* Le graphe de tâche tel qu'affiché est donc un résumé, un aperçu, des tâches du programme

    * la vraie définition des tâches est dans le code
