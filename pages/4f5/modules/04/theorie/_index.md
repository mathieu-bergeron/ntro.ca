---
title: "Théorie module 4"
weight: 1
bookHidden: true
---


{{% pageTitle %}}




1. {{% link "./modele/" "Notion de modèle" %}}

1. {{% link "./json/" "Sauvegarde en JSON" %}}

1. {{% link "./graphe_de_taches/" "Graphe de tâches (2)" %}}

1. {{% link "./evenements/" "Réagir aux événements usagers" %}}

1. {{% link "./changer_page/" "Passer d'une page à l'autre en `Ntro`" %}}

1. {{% link "./session/" "Utiliser la session pour mémoriser la vue courante" %}}

1. {{% link "./observation/" "Observer un modèle dans le frontal" %}}

1. (optionnel) {{% link "./animation_image_cle/" "Animation par image clé" %}}

## NOTE

On est encore sur le Frontal (on commence le Dorsal au module 05)

<img class="figure" src="mvc_ntro.png"/>

<!--

1. {{% link "./inversion/" "Principe d'inversion des dépendances" %}}

-->
