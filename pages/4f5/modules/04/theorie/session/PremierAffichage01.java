private static void choisirPremiereVue(FrontendTasks tasks) {

    tasks.task("choisirPremiereVue")

          // dépendance à la session
          .waitsFor(session(SessionPong.class))                
    
          .executes(inputs -> {

              // récupérer la session 
              SessionPong session = inputs.get(session(SessionPong.class));

              // ....

          });
}
