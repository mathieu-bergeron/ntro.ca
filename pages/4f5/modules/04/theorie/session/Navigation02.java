private static void afficherVueFileAttente(FrontendTasks tasks) {

    tasks.task("afficherVueFileAttente")

          .waitsFor(event(EvtAfficherFileAttente.class))

          .waitsFor(created(VueRacine.class))

          .waitsFor(created(VueFileAttente.class))

          .executes(inputs -> {
              
              // ajouter
              SessionPong            session                = Ntro.session();
              EvtAfficherFileAttente evtAfficherFileAttente = inputs.get(event(EvtAfficherFileAttente.class));
              VueRacine              vueRacine              = inputs.get(created(VueRacine.class));
              VueFileAttente         vueFileAttente         = inputs.get(created(VueFileAttente.class));

              // modifier
              evtAfficherFileAttente.appliquerA(session)
                                    .appliquerA(vueRacine, vueFileAttente);
              
          });
}
