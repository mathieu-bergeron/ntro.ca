public class EvtAfficherFileAttente extends Event {

    // ...

    // ajouter
    public EvtAfficherFileAttente appliquerA(SessionPong session) {

      session.memoriserVueCourante(VueFileAttente.class);
      
      return this;

    }
