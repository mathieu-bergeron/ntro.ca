public class EvtAfficherFileAttente extends Event {

    public EvtAfficherFileAttente appliquerA(SessionPong session) {

      session.memoriserVueCourante(VueFileAttente.class);
      
      return this;

    }

    public EvtAfficherFileAttente appliquerA(VueRacine vueRacine, VueFileAttente vueFileAttente) {
        
      vueRacine.afficherSousVue(vueFileAttente);

      return this;

    }

}
