---
title: ""
weight: 1
bookHidden: true
---


# Théorie: session

## Informations spécifiques au Frontal

1. La session mémorise des informations spécifiques au Frontal, pex.

    * la vue courante / page courante
    * le dernier modèle affichée (p.ex. la dernière partie affichée).

1. En quittant l'application, Ntro va sauvegarder la session

1. La session permet donc de reprendre l'affichage «là où on était rendu»

## Pour déclarer la session

1. Il faut créer une classe qui hérite de `Session`
    
    ```java
    {{%  embed src="SessionPong01.java" indent-level="1" %}}
    ```


1. Ensuite, il faut déclarer la classe session dans le Frontal

    ```java
    {{%  embed src="FrontalPong01.java" indent-level="1" %}}
    ```

## Pour utiliser la session dans le code

1. Soit dans le graphe des tâches:

    ```java
    {{%  embed src="PremierAffichage01.java" indent-level="1" %}}
    ```

1. Soit ailleurs dans le code:

    ```java
    SessionPong session = Ntro.session();

    // autre version
    Ntro.session(SessionPong.class).memoriserPartieCourante(idPartie);
    ```

## Pour démarrer l'application avec une session

```bash
$ sh gradlew pong:alice

$ sh gradlew mon_projet:bob

$ sh gradlew mon_projet:charlie

# ajouter d'autres tâches dans build.gradle pour créer d'autres sessions
```

## Où sont sauvegarder les sessions?

1. Fichiers lus par Ntro: `mon_projet/_storage/sessions`

1. Pour les sauvegarder dans Git: `mon_projet/json`

    ```bash
    $ sh gradlew saveJson
    ```

1. Pour ré-installer les sessions sauvegardées dans Git

    ```bash
    $ sh gradlew restoreJson
    ```

    

