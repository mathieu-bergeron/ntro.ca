public class VueFileAttente extends ViewFx {
    
    @FXML
    private Button boutonDebuterPartie;
    
    // méthode appelée après la création de la vue
    @Override
    public void initialize() {

        // vérifier rapidement que boutonDebuterPartie est bien récupéré
        Ntro.assertNotNull(boutonDebuterPartie);

    }
}
