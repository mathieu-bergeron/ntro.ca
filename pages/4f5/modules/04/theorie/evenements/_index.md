---
title: ""
weight: 1
bookHidden: true
---


# Théorie: réagir aux événements usager

## Identifier les contrôles en FXML

<center>
<video width="50%" src="identifiant.mp4" type="video/mp4" controls playsinline>
</center>

* Dans le `.fxml`, on doit identifier chaque contrôle avec un attribut `fx:id`

    ```xml
    {{% embed src="file_attente.fxml" indent-level="1" %}}
    ```

* Le `fx:id` et `id` ne joue pas le même rôle
    * `fx:id` identifiant JavaFx 
    * `id` identifiant CSS

## Récupérer un contrôle dans une Vue

<center>
<video width="50%" src="attribut_java.mp4" type="video/mp4" controls playsinline></video><br>
(oubli: le type de l'attribut doit être la même classe que la balise)
</center>

* Pour accéder au contrôle en Java, on ajoute un attribut dans notre Vue

    ```java
    {{% embed src="VueFileAttente01.java" indent-level="1" %}}
    ```

* Le nom de l'attribut doit correspondre exactement au `fx:id` du fichier `.fxml`

* Le type de l'attribut doit être la même classe que la balise dans le fichier `.fxml`

* L'annotation `@FXML` doit être directement au dessus de l'attribut

* La ligne `Ntro.assertNotNull` est utile pour détecter rapidement les erreurs


## Installer un capteur d'événement en JavaFx

<center>
<video width="50%" src="evt_fx.mp4" type="video/mp4" controls playsinline>
</center>

* Voici comment capter un événement JavaFx

    ```java
    {{% embed src="VueFileAttente02.java" indent-level="1" %}}
    ```

    * quand l'usager clique, on devrait avoir l'affichage suivant à la console

            clic: ACTION
            clic: ACTION
            clic: ACTION

## Créer et déclencher un événement en `Ntro`

<center>
<video width="50%" src="creer_evt_ntro.mp4" type="video/mp4" controls playsinline>
</center>

* Il faut créer une classe qui hérite de `EventNtro`, p.ex.

    ```java
    {{% embed src="EvtAfficherFileAttente.java" indent-level="1" %}}
    ```

    * NOTE: par convention, on va appeler nos événements `EvtNomEvenement`

* Il faut déclarer l'événement dans le Frontal, p.ex.


    ```java
    {{% embed src="FrontalPong.java" indent-level="1" %}}
    ```

* On déclenche typiquement un événement `Ntro` à chaque fois qu'on capte un événement JavaFx

    ```java
    {{% embed src="VueFileAttente03.java" indent-level="1" %}}
    ```

* Noter qu'on va créer l'événement Ntro et le déclencher dans le même ligne
    * l'idée est de communiquer que l'événement est transitoire (ce n'est pas un objet qui "reste" dans le programme)
    * (on va utiliser la même convention pour les message)


## Gérer un événement en `Ntro`

<center>
<video width="50%" src="reagir_evt.mp4" type="video/mp4" controls playsinline>
</center>

1. Il faut créer une tâche pour gérer l'événement `Ntro`

    ```java
    {{% embed src="Navigation.java" indent-level="1" %}}
    ```


1. La tâche est déclenchée à chaque fois que l'événement est déclenché
    * (et que les autres conditions sont remplies)



