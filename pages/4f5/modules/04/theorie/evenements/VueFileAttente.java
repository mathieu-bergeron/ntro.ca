public class VueFileAttente extends ViewFx {
    
    
    @FXML
    private Button boutonJoindrePartie;
    
    @Override
    public void initialize() {

        Ntro.asserter().assertNotNull("boutonJoindrePartie", boutonJoindrePartie);
        
        installerEvtAfficherPartie();
    }

    private void installerEvtAfficherPartie() {
        
        EvtAfficherPartie evtNtro = Ntro.newEvent(EvtAfficherPartie.class);

        boutonJoindrePartie.setOnAction(evtFx -> {

            evtNtro.trigger();

        });
    }

}
