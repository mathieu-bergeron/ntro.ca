public class VueFileAttente extends ViewFx {
    
    
    @FXML
    private Button boutonDebuterPartie;
    
    
    @Override
    public void initialize() {

        Ntro.assertNotNull(boutonDebuterPartie);
        
        installerEvtAfficherPartie();
    }

    private void installerEvtAfficherPartie() {

        boutonDebuterPartie.setOnAction(evtFx -> {

            System.out.println("clic: " + evtFx.getEventType());

        });
    }

}
