---
title: ""
weight: 1
bookHidden: true
---


# Théorie: sauvegarde d'un modèle en JSON

<center>
<video width="50%" src="fichiers.mp4" type="video/mp4" controls playsinline>
</center>

1. Par défaut, `Ntro` entrepose les modèles dans des fichiers `.json`

1. Les fichiers sont ici

    `_storage/models/`

1. Par exemple, le modèle `ModeleTexte` est sauvegardé dans

    `_storage/models/ModeleTexte.json`

1. Utiliser des fichiers facilite le développement
    * on peut facilement inspecter les modèles
    * on peut modifier les modèles directement dans les fichiers

1. (en production, on peut sauvegarder dans base de données comme MongoDB)


## Ajouter un constructeur par défaut

<center>
<video width="50%" src="constructeur.mp4" type="video/mp4" controls playsinline>
</center>

1. Il faut **obligatoirement** ajouter un constructeur par défaut

    ```java
    {{% embed src="./ModeleTexte01.java" indent-level="1" %}}
    ```

1. Sans constructeur par défaut, `Ntro` ne pourra pas créer le modèle en Java


    
## Attributs temporaires 
 
1. Pour ajouter un attribut qui ne sera **pas** sauvegardé en JSON


    ```java
    {{% embed src="./ModeleTexte03.java" indent-level="1" %}}
    ```

## (optionnel) ajouter des accesseurs (méthodes *get/set*)

1. Au besoin, on peut ajouter des accesseurs à un modèle (ce n'est pas obligatoire)

    ```java
    {{% embed src="./ModeleTexte02.java" indent-level="1" %}}
    ```



## (optionnel) accesseur avec préfixe *get* et *set*

<center>
<video width="50%" src="eclipse.mp4" type="video/mp4" controls playsinline>
</center>


1. `Ntro` supporte **uniquement** les préfixes *get* et *set*

1. **ATTENTION** par défaut VSCode utilise le préfixe *is* pour les booléens

    * il faut modifier *is* pour *get*






