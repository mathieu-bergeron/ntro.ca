
public class AppPong implements NtroAppFx {

    // ...

    @Override
    public void registerModels(ModelRegistrar registrar) {

        // ...

        registrar.registerValue(Paragraphe.class);
        registrar.registerValue(Phrase.class);
    }
