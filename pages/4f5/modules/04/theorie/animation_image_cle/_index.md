---
title: ""
weight: 1
bookHidden: true
---


# JavaFX: animation par image clé (*keyframe*)

## Animation par image clé

1. On donne des valeurs à des points précis dans le temps
1. Les valeurs intermédiaires sont calculés par l'animation
1. Conceptuellement on  donne:

        Au temps 0: opacité est 1.0
        Au temps 10: opacité est 0.5
        Au temps 20: opacité est 1.0

1. Le système va générer:

        Au temps 0: opacité est 1.0
        Au temps 1: opacité est 0.95
        Au temps 2: opacité est 0.90
        Au temps 3: opacité est 0.85
        ...
        Au temps 10: opacité est 0.5
        Au temps 11: opacité est 0.55
        Au temps 12: opacité est 0.6
        ...
        Au temps 20: opacité est 1.0

## Créer l'animation

1. Créer l'animation

```java
{{% embed src="./vues06a.java" first-line="1" last-line="1" %}}
```

## Ajouter des images clés

1. Spécifier l'instant

    ```java
    {{% embed src="./vues06a.java" first-line="3" last-line="4" indent-level="1" %}}
    ```

1. Spécifier la valeur

    ```java
    {{% embed src="./vues06a.java" first-line="6" last-line="6" indent-level="1" %}}
    ```

1. Ajouter l'image clé à l'animation

    ```java
    {{% embed src="./vues06a.java" first-line="8" last-line="10" indent-level="1" %}}
    ```

1. Typiquement ont fait tout ça d'un coup:

    ```java
    {{% embed src="./vues06a.java" first-line="13" last-line="16" indent-level="1" %}}
    ```

## Contrôler l'animation

1. Spécifier le nombre de fois où l'animation va jouer

    ```java
    {{% embed src="./vues06a.java" first-line="18" last-line="18" indent-level="1" %}}
    ```

1. Spécifier une boucle indéfinie

    ```java
    {{% embed src="./vues06a.java" first-line="20" last-line="20" indent-level="1" %}}
    ```

1. Démarrer

    ```java
    {{% embed src="./vues06a.java" first-line="22" last-line="22" indent-level="1" %}}
    ```

1. Arrêter

    ```java
    {{% embed src="./vues06a.java" first-line="24" last-line="24" indent-level="1" %}}
    ```



