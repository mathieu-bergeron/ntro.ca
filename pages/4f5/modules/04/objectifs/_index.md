---
title: ""
weight: 1
bookHidden: true
---


# Objectifs 4: modèle et navigation

<!--
<center>
<video width="50%" src="presentation.mp4" type="video/mp4" controls playsinline>
</center>
-->

{{<excerpt class="objectifs">}}


**IMPORTANT**

1. mon modèle **doit** contenir
    * au moins une liste ou un map
    * au moins une Valeur (une classe que j'invente les item de la liste/map)

1. ma page **doit** afficher mon modèle en mode texte
    * je dois tester l'affichage en créant moi-même le `.json`
    * je dois sauvegarer le `.json` avec la commande `$ sh gradlew saveJson`

1. mon application **doit** doit utiliser la session

    * la commande `$ sh gradlew mon_projet:alice` doit générer `mon_projet/_storage/alice.session`

1. mon application **doit** utiliser la session pour mémoriser et naviguer vers la vue courante
    * la commande `$ sh gradlew mon_projet:alice` **doit** afficher automatiquement la dernière vue affichée par `alice`



{{</excerpt>}}


1. J'effectue le {{% link "../tutoriel/" "tutoriel" %}} pour comprendre
    * comment afficher un modèle en mode texte
    * comment gérer les événements usager
    * comment changer de page dans mon application
    * comment utiliser la session pour mémoriser la vue courante

1. J'implante une première version de mon modèle
    * modification du `.json`
    * affichage en mode texte

1. J'implante la navigation d'une page à l'autre
    * je **dois** utiliser la session pour mémoriser la vue courante

1. voici, **par exemple**, le genre de résultat qu'on cherche 

    {{% animation src="/4f5/modules/04/objectifs/objectifs04.mp4" %}}

    * **NOTE**: j'utilises du texte pertinent à mon projet!

1. Objectifs optionnels:
    * capter les touches du clavier et zoomer
    * changer de langue en cours d'exécution
    * ajouter une animation à la vue racine


1. Je m'assure d'avoir un graphe contenant des groupes de tâches
    * voici, <strong>par exemple</strong>, le genre de graphe que je devrais avoir

        <center>
            <img width="100%" src="frontend.png"/>
        </center>

1. Je m'assure que mes noms d'attribut/méthode sont adéquats **pour mon projet**
    * on ne **veut pas** de `boutonDebuterPartie` qui ouvre une page de paramètres!

1. Je sauvegarde la session et mon modèle avec

    ```bash
    $ sh gradlew saveJson
    ```

1. Je pousse mon projet sur GitLab, p.ex:

    ```bash
    $ git add .
    $ git commit -a -m module04
    $ git push 
    ```

1. Je vérifie que mes fichiers sont sur GitLab
    * **ATTENTION**: bien vérifier le répertoire `mon_projet/json`

1. Je vérifie que projet est fonctionnel avec un `$ git clone` neuf, p.ex:

    ```
    # effacer le répertoire tmp s'il existe

    $ mkdir ~/tmp  
    $ cd ~/tmp
    $ git clone https://gitlab.com:USAGER/4f5_prenom_nom
    $ cd 4f5_prenom_nom

    # Restaurer la session et le modèle sauvegardés

    $ sh gradlew restoreJson      


    # Valider

    $ sh gradlew mon_projet:alice

        # Doit afficher la vue courante selon la session, en français
        # Doit afficher mon modèle en mode texte
        # On doit pouvoir naviguer d'une page à l'autre


    $ sh gradlew mon_projet:bob

        # Doit afficher la vue courante selon la session, en anglais
        # Doit afficher mon modèle en mode texte
        # On doit pouvoir naviguer d'une page à l'autre
    ```
