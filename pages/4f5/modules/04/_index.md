---
title: "Module 4: modèle et navigation"
weight: 40
draft: false
---


{{% pageTitle %}}

<!--

{{<excerpt class="max-width-75">}}

* {{% link "/4f5/migrations/v04" "Migration à `v04` de Ntro" %}}

{{</excerpt>}}

-->



<!--

<center>
<video width="50%" src="rappel.mp4" type="video.webm" controls playsinline>
</center>

-->


* {{% link "./theorie" "Théorie" %}}

    * notions de modèle et de valeur
        * sauvegarde json 
        * observer un modèle
    * réagir aux événements usager
        * passer d'une page à l'autre
        * utiliser la session pour mémoriser la vue courante

* {{% link src="./tutoriel/" text="Tutoriel à faire avant les objectifs" %}}

    * créer et afficher un modèle (en mode texte)
    * navigation d'une page à l'autre

* {{% link src="./objectifs/" text="Objectifs pour avancer le TP#1" %}} 

    * création et affichage de **mon modèle**
    * ajout d'une page d'accueil **à mon projet**

<br>

* REMISES:
    * <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=300621">auto-évaluation pour ce module</a>


<br>
<div style="padding:5px;background:pink;border-style:dotted" >
<i>RAPPEL:</i> chaque étudiant.e est <strong>individuellement</strong> responsable de:
<ul>
<li>maintenir <i>son propre</i> dépôt Git
<li>présenter <i>son progrès</i> à la rencontre de travail
<li>faire son auto-évaluation
<li>s'assurer que son code est fonctionnel
</ul> 
<br>
<i>RAPPEL:</i> vous travaillez en équipe, mais les notes sont 100% <strong>individuelles</strong>
</div>
