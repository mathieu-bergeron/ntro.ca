---
title: ""
weight: 1
bookHidden: true
---


# Théorie: réagir au temps qui passe

## Déplacer un objet 2d

<center>
<video width="50%" src="deplacer.mp4" type="video/mp4" controls playsinline>
</center>

* L'objet 2d a aussi une vitesse
* Cette vitesse indique comment déplacer l'objet

<center>
    <img width="100%" src="objet2d.svg"/>
</center>

### En `Ntro`

* Choisir la vitesse dès le début

```java
{{% embed "./Balle2d01.java" %}}
```



## Réagir au temps qui passe

<center>
<video width="50%" src="temps_qui_passe.mp4" type="video/mp4" controls playsinline>
</center>

* Pour déplacer un objet on va:
    1. Attendre que le temps passe
    1. Déplacer un tout petit peu l'objet selon sa vitesse
    1. Ré-afficher le monde 2d

<center>
    <img width="100%" src="deplacer.svg"/>
</center>

### En `Ntro`

* Ajouter une tâche qui s'exécute à chaque `clock().nextTick()`
    * (c-à-d dès que possible)

    ```java
    {{% embed src="./AfficherPartie01.java" indent-level="1" %}}
    ```

* Appeler le `onTimePasses` du monde 2d (méthode héritée de `World2dFx`)

    ```java
    {{% embed src="./DonneesVuePartie01.java" indent-level="1" %}}
    ```


* `World2dFx.onTimePasses` va notifier chaque objet que le temps passe

    ```java
    {{% embed src="./World2d.java" indent-level="1" %}}
    ```

* Par défaut, un objet 2d se déplace selon sa vitesse

    ```java
    {{% embed src="./Object2d.java" indent-level="1" %}}
    ```

* On peut aussi redéfinir `onTimePasses` et ajouter du comportement

    ```java
    {{% embed src="./Balle2d02.java" indent-level="1" %}}
    ```
