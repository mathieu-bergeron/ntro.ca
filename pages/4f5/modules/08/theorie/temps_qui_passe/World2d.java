public abstract class World2dFx {

    public void onTimePasses(double secondsElapsed) {
        for(Object2d<?> objet2d : objects) {
            objet2d.onTimePasses(secondsElapsed);
        }
    }
