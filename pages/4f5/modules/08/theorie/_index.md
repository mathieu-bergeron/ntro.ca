---
title: ""
weight: 1
bookHidden: true
---


# Théorie: jeu 2d

## Faire un jeu en 2d

* {{% link "./monde2d/" "Créer un monde 2d" %}}  

* {{% link "./dessiner_monde2d/" "Dessiner le monde 2d" %}}  

* {{% link "./canvas/" "Dessiner sur un canvas de type `ResizableWorld2dCanvasFx`" %}}

* {{% link "./temps_qui_passe/" "Réagir au temps qui passe" %}}  


* {{% link "./evenements/" "Réagir aux événements" %}}  

* {{% link "./animation/" "Créer une animation" %}}  

* {{% link "./donnees_de_vue/" "Données d'une Vue" %}}

* {{% link "./modele_partie/" "Sauvegarder le monde 2d dans un modèle" %}}  

<!--

## Bonus

* {{% link "./animation_image_cle/" "Animation par image clé" %}}
-->
