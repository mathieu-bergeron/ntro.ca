public class CanvasPartie extends ResizableWorld2dCanvasFx {

    @Override
    protected void initialize() {
        setInitialWorldSize(MondePong2d.LARGEUR_MONDE, 
                            MondePong2d.HAUTEUR_MONDE);
    }
