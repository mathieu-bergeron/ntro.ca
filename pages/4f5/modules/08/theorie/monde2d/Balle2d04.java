public class Balle2d extends ObjetPong2d {

    // ...

    @Override
    public void drawOnWorld(GraphicsContext gc) {
            gc.fillArc(getTopLeftX(),
                       getTopLeftY(),
                       getWidth(), 
                       getHeight(), 
                       0,
                       360, 
                       ArcType.CHORD);
    }
}
