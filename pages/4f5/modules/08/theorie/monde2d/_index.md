---
title: ""
weight: 1
bookHidden: true
---


# Théorie: créer un monde 2d

## Le monde 2d


<center>
<video width="50%" src="monde2d.mp4" type="video/mp4" controls playsinline>
</center>

* Un monde 2d est un rectangle avec une largeur et une hauteur
* Le monde 2d contient des objets 2d
* Un objet 2d occupe un rectangle dans lequel son dessin est contenu

<center>
    <img width="100%" src="monde2d.svg"/>
</center>

### En `Ntro`

* Définir un monde 2d

```java
{{% embed "./MondePong2d01.java" %}}
```


* Ajouter des objets 2d

```java
{{% embed "./MondePong2d02.java" %}}
```



## Décrire les objets

<center>
<video width="50%" src="objet2d.mp4" type="video/mp4" controls playsinline>
</center>

* Chaque objet 2d a aussi une largeur (`w`) et une hauteur (`h`)
* La position de l'objet 2d est indiquée par son coin haut/gauche (`topLeft`)

<center>
    <img width="100%" src="objet2d.svg"/>
</center>

### En `Ntro`

* Un classe abstraite pour tous les objets 2d

```java
{{% embed "./ObjetPong2d.java" %}}
```

* Définir un objet 2d

```java
{{% embed "./Balle2d.java" %}}
```

## Afficher le monde 2d

<center>
<video width="50%" src="afficher.mp4" type="video/mp4" controls playsinline>
</center>

* On va afficher le monde 2d en deux étapes
    1. (optionnel) dessiner une image de fond
    1. itérer chaque objet 2d et demander à cet objet de se dessiner

### En `Ntro`

* Définir un canvas pour afficher le monde 2d

```java
{{% embed "./CanvasPartie.java" %}}
```

* Initialiser le CanvasPartie 

```java
{{% embed "./CanvasPartie01.java" %}}
```

* Appeler la méthode `drawOn` (héritée de `World2dFx`)

```java
{{% embed "./VuePartie02.java" %}}
```

* La méthode `drawOnWorld` de chaque objet2d va être appelée:

```java
{{% embed "./Balle2d04.java" %}}
```

* NOTES
    * le `CanvasPartie` va redimensionner le monde 2d selon le conteneur
    * le ratio largeur/hauteur est préservé automatiquemen

<center>
<video width="40%" src="redimensionner.mp4" type="video/mp4" loop nocontrols autoplay>
</center>

## Détecter les collisions

<center>
<video width="50%" src="collision.mp4" type="video/mp4" controls playsinline>
</center>

* La collision est calculée selon le rectangle qui entoure l'objet

<center>
    <img width="100%" src="collides.svg"/>
</center>

### En `Ntro`

* Collision avec un autre objet

```java
{{% embed "./Balle2d02.java" %}}
```

* Collision avec un rectangle

```java
{{% embed "./Balle2d03.java" %}}
```




