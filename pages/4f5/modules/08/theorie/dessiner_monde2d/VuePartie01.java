public class VuePartie extends ViewFx {

    @FXML
    private CanvasPartie canvasPartie;

    // ...

    public void viderCanvas() {
        canvasPartie.clearCanvas();
    }

    public void afficherImagesParSeconde(String fps) {
        canvasPartie.afficherFps(fps);
    }

    public void afficherPong2d(MondePong2d mondePong2d) {
        mondePong2d.drawOn(canvasPartie);
    }
