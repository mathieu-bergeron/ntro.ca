---
title: ""
weight: 1
bookHidden: true
---


# Dessiner le monde 2d

## Créer une tâche pour l'affichage temps-réel

```java
{{% embed src="./AfficherPartie01.java" indent-level="0" %}}
```

* la tâche `prochaineImagePartie` sera appelée à chaque `clock().nextTick()`
* ce qui veut dire: le plus souvent possible
* (en JavaFx, typiquement 60 fois par secondes)

## Afficher monde 2d

```java
{{% embed src="./VuePartie01.java" indent-level="0" %}}
```
