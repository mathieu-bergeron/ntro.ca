public static void creerTaches(FrontendTasks tasks) {
    
    tasks.taskGroup("AfficherPartie")

         .waitsFor(created(DonneesVuePartie.class))

         .contains(subTasks -> {

            prochaineImagePartie(subTasks);

         });

}

private static void prochaineImagePartie(FrontendTasks subTasks) {

    subTasks.task("prochaineImagePartie")

             .waitsFor(created(VuePartie.class))

             .waitsFor(created(DonneesVuePartie.class))

             .waitsFor(clock().nextTick())

             .executes(inputs -> {
                
                VuePartie        vuePartie        = inputs.get(created(VuePartie.class));

                vuePartie.afficherMonde2d(monde2d);

            });
}
