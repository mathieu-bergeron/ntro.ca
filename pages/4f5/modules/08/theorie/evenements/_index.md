---
title: ""
weight: 1
bookHidden: true
---


# Théorie: réagir aux événements


<center>
<video width="50%" src="evenement.mp4" type="video/mp4" controls playsinline>
</center>


* Il suffit de changer les attributs de l'objet 2d
* P.ex. on pourrait modifier sa vitesse en recevant la touche {{% key "←" %}}

<center>
    <img width="100%" src="evenement.svg"/>
</center>

* L'effet est que l'objet va changer de direction

### En `Ntro`

* Capter les événements du clavier et lancer un événement Ntro

    ```java
    {{% embed src="./VuePartie.java" indent-level="1" %}}
    ```

* Ajouter une tâche pour l'événement Ntro

    ```java
    {{% embed src="./AfficherPartie.java" indent-level="1" %}}
    ```

* Appliquer l'événement, p.ex.
    * appeler `palette.monter()` pour la touche `W`
    * appeler `palette.descendre()` pour la touche `S`
    * appeler `palette.arreter()` quand la touche est relâchée

    ```java
    {{% embed src="./Palette2d.java" indent-level="1" %}}
    ```
