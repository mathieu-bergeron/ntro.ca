public class VuePartie extends ViewFx {
    
    // ...
   
    @Override
    public void initialize() {
        // ...
        
        installerEvtActionJoueur();
    }

    private void installerEvtActionJoueur() {
        
        canvasPartie.addEventFilter(KeyEvent.KEY_PRESSED, evtFx -> {
            
            if(evtFx.getCode().equals(KeyCode.W)) {
                
                Ntro.newEvent(EvtActionJoueur.class)
                    .setAction(new ActionJoueur(Position.GAUCHE, Action.HAUT))
                    .evtNtro.trigger();

            }else if(evtFx.getCode().equals(KeyCode.S)) {

                // ...
        });
    }
