subTasks.task("reagirActionJoueur")

        .waitsFor(event(EvtActionJoueur.class))
        
        .executes(inputs -> {
            
            DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
            EvtActionJoueur  evtActionJoueur  = inputs.get(event(EvtActionJoueur.class));
            
            evtActionJoueur.appliquerA(donneesVuePartie);

        });
