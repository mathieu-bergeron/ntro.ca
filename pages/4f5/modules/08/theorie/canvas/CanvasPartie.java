public class CanvasPartie extends ResizableWorld2dCanvasFx {
    
    private static final double LARGEUR = 640;
    private static final double HAUTEUR = 360;

    @Override
    protected void initialize() {
        setInitialWorldSize(LARGEUR, CANVAS_HEIGHT);

        installerOnRedraw();
    }


    private void installerOnRedraw() {
        onRedraw(() -> {
            dessinerSurLeViewport();
            dessinerSurLeMonde();
            dessinerSurLeViewscreen();
            dessinerSurLeCanvas();
        });
    }

    private void dessinerSurLeCanvas() {
        drawOnCanvas(gc -> {
            
            gc.fillText("drawOnCanvas", 0, 12);
            
        });
    }

    private void dessinerSurLeViewscreen() {
        drawOnViewscreen(gc -> {
            gc.setFill(Color.BEIGE);
            gc.fillText("drawOnViewscreen", 0, 12);
            
        });
    }

    private void dessinerSurLeViewport01() {
        drawOnViewport(gc -> {
            
            gc.setFill(Color.DARKBLUE);

            gc.fillRect(0, 
                        0,
                        getViewportWidth(),
                        getViewportHeight());

            gc.strokeRect(0, 
                          0,
                          getViewportWidth(),
                          getViewportHeight());
        });
    }

    private void dessinerSurLeViewport02() {
        drawOnViewport(gc -> {

            gc.setFill(Color.WHITE);
            gc.fillText("drawOnViewport", 0, getViewportHeight() - 6);
            
        });
    }

    private void dessinerSurLeMonde() {
        drawOnWorld(gc -> {

            // pour un jeu, dessiner surtout ici

        });
    }

