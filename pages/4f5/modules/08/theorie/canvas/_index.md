---
title: "Théorie: dessiner sur un ResizableWorld2dCanvasFx"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

{{% video src="/4f5/modules/08/theorie/canvas/presentation.mp4" %}}

## Observer un monde2d via un viewport

1. On veut dessiner notre jeu sur un monde2d

<img src="concept_world2dcanvas.png"/>

1. L'affichage sera échelonnée (agrandie ou rapetissée)

1. Avec un viewport, on est pas obligé d'afficher le monde2d en entier:

    <img src="mario.webp"/>

## Dessiner sur différentes régions

1. Voici les différentes régions:

<img src="world2dcanvas.png"/>

1. Le plus courant est de dessiner sur le monde2d:

```java
{{% embed src="DessinerMonde2d.java" %}}
```

1. On peut dessiner sur le viewport comme suit:

```java
{{% embed src="DessinerViewport.java" %}}
```

1. On peut dessiner sur le viewscreen comme suit:

```java
{{% embed src="DessinerViewscreen.java" %}}
```

1. Finalement, on peut dessiner directement sur le canvas:

```java
{{% embed src="DessinerCanvas.java" %}}
```

## En action

{{% animation src="/4f5/modules/08/theorie/canvas/world2dcanvas.mp4" %}}

* Quand la zone orange se déplace, c'est que le `viewport` est déplacé
    * (le monde2d este inchangé)
* Quand la zone orange grandit, c'est que le `viewport` est rapetissé
    * (le monde2d reste inchangé)
* Quand l'image est déplacée, est elle déplacée à l'intérieur du monde2d
* Quand la fenêtre grandit, le `canvas` grandit (et du même coup le `viewscreen`)
    * (le `world2d` et le `viewport` restent inchangés)

