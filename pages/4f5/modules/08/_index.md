---
title: "Module 8: jeu en 2d"
weight: 90
draft: false
---

{{% pageTitle %}}

<center>
<div style="background-color:orange;width:80%;border:2px dashed black;padding:10px">
<strong>NOTE</strong>
<div style="text-align:left">
<ul>
<li>JavaFx et Ntro ne sont <strong>pas conçus</strong> pour programmer des jeux
<ul>
<li>la performance va laisser à désirer<br><br>
</ul>
<li>Si la programmation de jeux vous intéresse, essayer p.ex.
    <ul>
        <li><a href="https://jmonkeyengine.org/" target="_blank">jMonkeyEngine</a> (Java)
        <li><a href="https://godotengine.org/" target="_blank">Godot</a> (C++, C#, GDScript)
    </ul>
</ul>
</div>
</center>

<!--

<center>
<video width="50%" src="rappel.mp4" type="video.webm" controls playsinline>
</center>

-->

* {{% link "./theorie" "Théorie" %}}

    * notion de monde 2d
    * notion d'objet 2d
    * canvas pour afficher un monde 2d
    * réagir au temps qui passe

* {{% link src="./tutoriel/" text="Tutoriel à faire avant les objectifs" %}}

    * implanter le jeu en 2d pour `pong`


* {{% link src="./objectifs/" text="Objectifs pour avancer le TP#2" %}} 

    * ajouter une page pour la partie (et une façon de naviguer vers cette page)
    * créer un monde 2d
    * afficher au moins un objet 2d **en mouvement**


* REMISES:
    * <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=305387">auto-évaluation pour ce module</a>

<br>
<div style="padding:5px;background:pink;border-style:dotted" >
<i>RAPPEL:</i> chaque étudiant.e est <strong>individuellement</strong> responsable de:
<ul>
<li>maintenir <i>son propre</i> dépôt Git
<li>présenter <i>son progrès</i> à la rencontre de travail
<li>faire son auto-évaluation
<li>s'assurer que son code est fonctionnel
</ul> 
<br>
<i>RAPPEL:</i> vous travaillez en équipe, mais les notes sont 100% <strong>individuelles</strong>
</div>
