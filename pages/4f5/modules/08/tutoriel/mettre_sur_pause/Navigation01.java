public class Navigation {

    // ...
    
    // ajouter
    private static Task afficherVuePartie;

    // ajouter
    private static Task afficherVueFileAttente;

    // ...
    

    // modifier cette méthode
    private static void afficherVuePartie(FrontendTasks subTasks, FrontendTasks tasks) {

        // ajouter
        afficherVuePartie = subTasks.task("afficherVuePartie")
        
             .waitsFor(event(EvtAfficherPartie.class))

             .waitsFor(created(VueRacine.class))

             .waitsFor(created(VuePartie.class))

             .executes(inputs -> {

                 SessionPong       session           = Ntro.session();
                 EvtAfficherPartie evtAfficherPartie = inputs.get(event(EvtAfficherPartie.class));
                 VueRacine         vueRacine         = inputs.get(created(VueRacine.class));
                 VuePartie         vuePartie         = inputs.get(created(VuePartie.class));

                 // ajouter
                 afficherVueFileAttente.cancel();
                 
                 evtAfficherPartie.appliquerA(session)
                                  .appliquerA(vueRacine, vuePartie);

             // ajouter .getTask()
             }).getTask();
        
    }

    // modifier cette méthode
    private static void afficherVueFileAttente(FrontendTasks tasks) {

        afficherVueFileAttente = tasks.task("afficherVueFileAttente")

              .waitsFor(event(EvtAfficherFileAttente.class))

              .waitsFor(created(VueRacine.class))

              .waitsFor(created(VueFileAttente.class))

              .executes(inputs -> {
                  
                  SessionPong            session                = Ntro.session();
                  EvtAfficherFileAttente evtAfficherFileAttente = inputs.get(event(EvtAfficherFileAttente.class));
                  VueRacine              vueRacine              = inputs.get(created(VueRacine.class));
                  VueFileAttente         vueFileAttente         = inputs.get(created(VueFileAttente.class));

                  // ajouter
                  afficherVuePartie.cancel();
                  
                  evtAfficherFileAttente.appliquerA(session)
                                        .appliquerA(session, vueFileAttente)
                                        .appliquerA(vueRacine, vueFileAttente);

                  
             // ajouter .getTask()
              }).getTask();
    }

    // ...
