public class Navigation {

    // ...
    
    private static void afficherVuePartie(FrontendTasks subTasks, FrontendTasks tasks) {

        subTasks.task("afficherVuePartie")
        
             .waitsFor(event(EvtAfficherPartie.class))
              
             .executes(inputs -> {

                 VueRacine vueRacine = inputs.get(created(VueRacine.class));
                 VuePartie vuePartie = inputs.get(created(VuePartie.class));
                  
                 vueRacine.afficherSousVue(vuePartie);
                 
                 // retirer
                 //AfficherPartie.partieSurPause = false;
             });
    }

    private static void afficherVueFileAttente(FrontendTasks tasks) {

        tasks.task("afficherVueFileAttente")

              .waitsFor(event(EvtAfficherFileAttente.class))
              
              .executes(inputs -> {
                  
                  VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
                  VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));
                  
                  vueRacine.afficherSousVue(vueFileAttente);

                  // retirer
                  //AfficherPartie.partieSurPause = true;

              });
    }
}
