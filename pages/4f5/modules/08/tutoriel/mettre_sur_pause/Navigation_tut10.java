public class Navigation {
    
    public static void creerTaches(FrontendTasks tasks) {

        tasks.taskGroup("Navigation")
        
             .waitsFor("Initialisation")

             .contains(subTasks -> {

                 afficherVueFileAttente(subTasks);

                 afficherVuePartie(subTasks, tasks);

             });
    }

    private static void afficherVuePartie(FrontendTasks subTasks, FrontendTasks tasks) {

        subTasks.task("afficherVuePartie")
        
             .waitsFor(event(EvtAfficherPartie.class))
              
             .executes(inputs -> {

                 VueRacine vueRacine                 = inputs.get(created(VueRacine.class));
                 VuePartie vuePartie                 = inputs.get(created(VuePartie.class));
                 EvtAfficherPartie evtAfficherPartie = inputs.get(event(EvtAfficherPartie.class));
                  
                 vueRacine.afficherSousVue(vuePartie);

                 // à ajouter
                 AfficherPartie.creerTaches(tasks, MaquetteSession.usagerCourant().getId());
             });
    }

    private static void afficherVueFileAttente(FrontendTasks tasks) {

        tasks.task("afficherVueFileAttente")

              .waitsFor(event(EvtAfficherFileAttente.class))
              
              .executes(inputs -> {
                  
                  VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
                  VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));
                  
                  vueRacine.afficherSousVue(vueFileAttente);

                  // à ajouter
                  AfficherPartie.supprimerTaches();

              });
    }
}
