public class AfficherPartie {
    
    // ajouter
    private static Task premiereTache = null;
    
    public static void creerTaches(FrontendTasks tasks, String idPartie) {

        // ajouter
        premiereTache = creerDonneesVuePartie(tasks, idPartie);

        tasks.taskGroup("AfficherPartie")

             .waitsFor(created(DonneesVuePartie.class))
             
             .contains(subTasks -> {

                observerModelePartie(subTasks, idPartie);
                
                reagirActionJoueur(subTasks, idPartie);

                reagirActionAutreJoueur(subTasks, idPartie);

                reagirClicSouris(subTasks);

                prochaineImagePartie(subTasks);

             });
    }

    // ajouter la méthode
    public static void supprimerTaches() {
        if(premiereTache != null) {
            premiereTache.removeFromGraph();
        }
    }

    // ajouter le type de retour Task
    private static Task creerDonneesVuePartie(FrontendTasks tasks, String idPartie) {

        // ajouter le return
        return tasks.task(create(DonneesVuePartie.class))
               
                    // ...

             }).getTask();
             // ajouter .getTask();
    }
