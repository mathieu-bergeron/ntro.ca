public class FrontalPong implements FrontendFx {
    
    @Override
    public void createTasks(FrontendTasks tasks) {
        
        Initialisation.creerTaches(tasks);
        Navigation.creerTaches(tasks);
        AfficherFileAttente.creerTaches(tasks);

        // à retirer: devient dynamique
        //AfficherPartie.creerTaches(tasks, MaquetteSession.usagerCourant().getId());

    }
