public class AfficherPartie {
    
    private static Task premiereTache = null;
    
    public static void creerTaches(FrontendTasks tasks, String idPartie) {

        premiereTache = creerDonneesVuePartie(tasks, idPartie);

        tasks.taskGroup("AfficherPartie")

             .waitsFor(created(DonneesVuePartie.class))
             
             .contains(subTasks -> {

                observerModelePartie(subTasks, idPartie);
                
                reagirActionJoueur(subTasks, idPartie);

                reagirActionAutreJoueur(subTasks, idPartie);

                reagirClicSouris(subTasks);

                prochaineImagePartie(subTasks);

             }).getTask();
    }

    public static void supprimerTaches() {
        if(premiereTache != null) {
            premiereTache.removeFromGraph();
        }
    }

    private static Task creerDonneesVuePartie(FrontendTasks tasks, String idPartie) {

        return tasks.task(create(DonneesVuePartie.class))

             //.waitsFor(snapshot(ModelePartie.class, idPartie))

             .waitsFor("Initialisation")
        
             .executesAndReturnsCreatedValue(inputs -> {

                       //Snapshot<ModelePartie> snapshotPartie = inputs.get(snapshot(ModelePartie.class, idPartie));
                       //VuePartie              vuePartie      = inputs.get(created(VuePartie.class));
                       
                       //ModelePartie modelePartie = snapshotPartie.currentValue();

                       DonneesVuePartie donneesVuePartie = new DonneesVuePartie();
                       
                       donneesVuePartie.memoriserIdPartie(idPartie);
                     
                       //modelePartie.copierDonneesDans(donneesVuePartie);
                       
                       //modelePartie.afficherInfoPartieSur(vuePartie);
                                       
                       return donneesVuePartie;

             }).getTask();
    }

    private static void prochaineImagePartie(FrontendTasks subTasks) {

        subTasks.task("prochaineImagePartie")

                .waitsFor(clock().nextTick())
                
                .waitsFor(created(VuePartie.class))

                .waitsFor(created(DonneesVuePartie.class))
        
                .executes(inputs -> {
                    
                    Tick             tick             = inputs.get(clock().nextTick());
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    VuePartie        vuePartie        = inputs.get(created(VuePartie.class));
                    
                    donneesVuePartie.reagirTempsQuiPasse(tick.elapsedTime());
                    donneesVuePartie.afficherSur(vuePartie);
                });
    }

    private static void reagirActionJoueur(FrontendTasks subTasks, String idPartie) {

        subTasks.task("reagirActionJoueur")

                .waitsFor(event(EvtActionJoueur.class))
                
                .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    EvtActionJoueur  evtActionJoueur  = inputs.get(event(EvtActionJoueur.class));
                    
                    boolean nouvelleAction = evtActionJoueur.appliquerA(donneesVuePartie);
                    
                    if(nouvelleAction) {
                        evtActionJoueur.diffuserMsgActionAutreJoueur(idPartie);
                    }

                });
    }

    private static void reagirActionAutreJoueur(FrontendTasks subTasks, String idPartie) {

        subTasks.task("reagirActionAutreJoueur")

                .waitsFor(message(MsgActionAutreJoueur.class, idPartie))
                
                .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie         = inputs.get(created(DonneesVuePartie.class));
                    MsgActionAutreJoueur msgActionAutreJoueur = inputs.get(message(MsgActionAutreJoueur.class, idPartie));
                    
                    msgActionAutreJoueur.appliquerA(donneesVuePartie);
                });
    }

    private static void reagirClicSouris(FrontendTasks subTasks) {

        subTasks.task("reagirClicSouris")

                .waitsFor(event(EvtClicSouris.class))
                
                .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    EvtClicSouris    evtClicSouris    = inputs.get(event(EvtClicSouris.class));

                    evtClicSouris.appliquerA(donneesVuePartie);

                });
    }
    
    private static void observerModelePartie(FrontendTasks subTasks, String idPartie) {

        subTasks.task("observerModelePartie")

                .waitsFor(modified(ModelePartie.class, idPartie))

                .executes(inputs -> {
                    
                    VuePartie              vuePartie        = inputs.get(created(VuePartie.class));
                    DonneesVuePartie       donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    Modified<ModelePartie> modifiedPartie   = inputs.get(modified(ModelePartie.class, idPartie));
                    
                    ModelePartie modelePartie = modifiedPartie.currentValue();
                    
                    MaquetteSession.memoriserCadranCourant(modelePartie.getIdPremierJoueur(), modelePartie.getIdDeuxiemeJoueur());
                    
                    vuePartie.afficherIdPartie(idPartie);

                    modelePartie.afficherInfoPartieSur(vuePartie);
                    
                    if(modelePartie.estPlusRecenteQue(donneesVuePartie)) {

                        modelePartie.copierDonneesDans(donneesVuePartie);
                    }

                });
    }

}
