---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: meilleure façon de mettre sur pause

## Le problème: `prochaineImagePartie` est encore exécutée

{{% animation src="/4f5/modules/08/tutoriel/mettre_sur_pause/pas_de_pause.mp4" %}}

* **NOTE** on voit que la tâche est encore exécutée environs 60 fois par secondes

## La solution: appeler `.cancel()` sur la tâche `afficherVuePartie`

* En annulant (`cancel`) la tâche `afficherVuePartie`, ça va bloque la tâche `prochaineImagePartie` parce qu'elle dépend de `afficherVuePartie`

* Dans `frontal.taches.Navigation`

    ```java
    {{% embed src="Navigation01.java" indent-level="2" %}}
    ```

## Vérifier que ça fonctionne

* Exécuter pour vérifier

    ```bash
    $ sh gradlew pong
    ```

    {{% animation src="/4f5/modules/08/tutoriel/mettre_sur_pause/mettre_sur_pause.mp4" %}}

    * **NOTE** on voit que `prochaineImagePartie` n'est pas exécutée quand on affiche la file d'attente

