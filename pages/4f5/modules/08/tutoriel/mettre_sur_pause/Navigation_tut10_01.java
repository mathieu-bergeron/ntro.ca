public class Navigation {
    
    // ...

    private static void afficherVuePartie(FrontendTasks subTasks, FrontendTasks tasks) {

        subTasks.task("afficherVuePartie")
        
             .waitsFor(event(EvtAfficherPartie.class))
              
             .executes(inputs -> {

                 VueRacine vueRacine                 = inputs.get(created(VueRacine.class));
                 VuePartie vuePartie                 = inputs.get(created(VuePartie.class));
                 EvtAfficherPartie evtAfficherPartie = inputs.get(event(EvtAfficherPartie.class));
                  
                 vueRacine.afficherSousVue(vuePartie);

                 // à ajouter
                 AfficherPartie.creerTaches(tasks, MaquetteSession.usagerCourant().getId());
             });
    }
