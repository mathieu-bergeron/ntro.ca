public class Navigation {

    // ...

    private static void afficherVueFileAttente(FrontendTasks tasks) {

        tasks.task("afficherVueFileAttente")

              .waitsFor(event(EvtAfficherFileAttente.class))
              
              .executes(inputs -> {
                  
                  VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
                  VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));
                  
                  vueRacine.afficherSousVue(vueFileAttente);

                  // à ajouter
                  AfficherPartie.supprimerTaches();

              });
    }
