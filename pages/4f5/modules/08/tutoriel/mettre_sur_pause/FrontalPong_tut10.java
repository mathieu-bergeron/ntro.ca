package pong.frontal;

import ca.ntro.app.Ntro.
import ca.ntro.app.frontend.FrontendFx;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.frontend.events.EventRegistrar;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import pong.frontal.donnees.DonneesVuePartie;
import pong.frontal.evenements.EvtActionJoueur;
import pong.frontal.evenements.EvtAfficherFileAttente;
import pong.frontal.evenements.EvtAfficherPartie;
import pong.frontal.evenements.EvtClicSouris;
import pong.frontal.fragments.FragmentRendezVousComplet;
import pong.frontal.fragments.FragmentRendezVous;
import pong.frontal.taches.Initialisation;
import pong.frontal.taches.AfficherFileAttente;
import pong.frontal.taches.AfficherPartie;
import pong.frontal.taches.Navigation;
import pong.frontal.vues.VueFileAttente;
import pong.frontal.vues.VuePartie;
import pong.frontal.vues.VueRacine;
import pong.maquettes.MaquetteSession;

public class FrontalPong implements FrontendFx {
	
	@Override
	public void createTasks(FrontendTasks tasks) {
		
		Initialisation.creerTaches(tasks);
		Navigation.creerTaches(tasks);
		AfficherFileAttente.creerTaches(tasks);

		// à retirer: devient dynamique
		//AfficherPartie.creerTaches(tasks, MaquetteSession.usagerCourant().getId());

	}

	@Override
	public void registerEvents(EventRegistrar registrar) {
		registrar.registerEvent(EvtAfficherFileAttente.class);
		registrar.registerEvent(EvtAfficherPartie.class);
		registrar.registerEvent(EvtActionJoueur.class);
		registrar.registerEvent(EvtClicSouris.class);
	}

	@Override
	public void registerViews(ViewRegistrarFx registrar) {
		registrar.registerDefaultResources("/chaines_fr.properties");
		registrar.registerResources(Ntro.locale("en"), "/chaines_en.properties");
		
		//registrar.registerStylesheet("/dev.css");
		registrar.registerStylesheet("/prod.css");

		registrar.registerView(VueRacine.class, "/racine.fxml");
		registrar.registerView(VueFileAttente.class, "/file_attente.fxml");
		registrar.registerView(VuePartie.class, "/partie.fxml");
		registrar.registerView(FragmentRendezVous.class, "/fragments/rendez_vous.fxml");
		registrar.registerView(FragmentRendezVousComplet.class, "/fragments/partie_en_cours.fxml");

		registrar.registerViewData(DonneesVuePartie.class);
	}


	@Override
	public void execute() {
		
		Ntro.newEvent(EvtAfficherFileAttente.class).trigger();

	}

}
