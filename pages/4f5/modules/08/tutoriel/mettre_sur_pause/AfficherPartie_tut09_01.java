public class AfficherPartie {
    
    // retirer
    //public static boolean partieSurPause = true;
    
    private static void prochaineImagePartie(FrontendTasks subTasks) {

        subTasks.task("prochaineImagePartie")

                .waitsFor(clock().nextTick())
                
                .waitsFor(created(VuePartie.class))

                .waitsFor(created(DonneesVuePartie.class))
        
                .executes(inputs -> {
                    
                    // retirer le if
                    //if(!partieSurPause) {

                        Tick             tick             = inputs.get(clock().nextTick());
                        DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                        VuePartie        vuePartie        = inputs.get(created(VuePartie.class));
                        
                        donneesVuePartie.reagirTempsQuiPasse(tick.elapsedTime());
                        donneesVuePartie.afficherSur(vuePartie);
                    //}
                });
    }
