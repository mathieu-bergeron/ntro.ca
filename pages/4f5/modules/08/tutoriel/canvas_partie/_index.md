---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: créer et utiliser le `CanvasPartie`

## Créer la classe `CanvasPartie`

1. Dans le paquet `pong.frontal.controles`, créer la classe `CanvasPartie`

1. En VSCode, s'assurer d'avoir l'arborescence suivante:

    <center>
        <img src="eclipse01.png">
    </center>

    * NOTE: le `FlexBoxRendezVous.java` est optionnel

1. Ajuster la signature de `CanvasPartie` 

    ```java
    {{% embed src="./CanvasPartie01.java" indent-level="1" %}}
    ```

1. Avec {{% key "Ctrl+1" %}}, ajouter le `import` et les méthodes obligatoires

1. Ne rien afficher pour l'instant

    ```java
    {{% embed src="./CanvasPartie02.java" indent-level="1" %}}
    ```
 
## Utiliser le `CanvasPartie` dans le `.fxml`

1. Modifier `partie.fxml` comme suit:

    ```xml
    {{% embed src="./partie.fxml" indent-level="1" %}}
    ```
