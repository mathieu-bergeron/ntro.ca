---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 8: implanter un jeu 2d


## Objectif: implanter le jeu de pong

<center>
    <video width="50%" src="pong.mp4" type="video/mp4" controls="true">
</center>


## Étapes obligatoires

1. {{% link "./naviguer_vers_partie/" "S'assurer de naviguer vers la page partie" %}}

1. {{% link "./canvas_partie/" "Créer un `CanvasPartie` qui hérite de `ResizableWorld2dCanvasFx`" %}}

1. {{% link "./monde2d/" "Créer un monde 2d et des objets 2d" %}}

1. {{% link "./donnees_vue_partie/" "Créer un `ViewData` nommé `DonneesVuePartie`" %}}

1. {{% link "./afficher_monde2d/" "Afficher le monde 2d... 60 fois par secondes" %}}

1. {{% link "./temps_qui_passe/" "Réagir au temps qui passe et faire rebondir la balle" %}} 

1. {{% link "./palettes/" "Ajouter les palettes" %}} 

1. {{% link "./actions_joueur/" "Réagir aux actions du joueur" %}}

## Étapes optionnelles

1. {{% link "./modele_partie/" "Créer un `ModelePartie` et suivre le pointage" %}}


1. {{% link "./clics_souris/" "Réagir aux clics de souris" %}}

1. {{% link "./image/" "Afficher l'image d'une balle" %}}

1. {{% link "./effets/" "Ajouter des effets et des animations" %}}

1. {{% link "./sons/" "Ajouter des sons" %}}

1. {{% link "./mettre_sur_pause/" "Mettre la partie sur pause" %}}

