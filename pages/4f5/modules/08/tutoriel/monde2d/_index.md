---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: créer un monde 2d

## Créer le monde et les objets

1. En VSCode, créer le paquet `pong.commun.monde2d`

1. Dans le paquet `monde2d`, créer les classes suivantes

    * `MondePong2d`
    * `ObjetPong2d`
    * `Balle2d`

1. En VSCode, s'assurer d'avoir l'arborescence suivante

    <center>
        <img src="eclipse00.png" />
    </center>


1. Ouvrir `MondePong2d` et ajuster la signature

    ```java
    {{% embed src="./MondePong2d01.java" first-line="1" last-line="1" indent-level="1" %}}
    ```

1. Ouvrir `Objet2d` et ajuster la signature

    ```java
    {{% embed src="./ObjetPong2d01.java" first-line="1" last-line="1" indent-level="1" %}}
    ```

    * **ATTENTION**: c'est une classe abstraite

1. Ouvrir `Balle2d` et ajuster la signature

    ```java
    {{% embed src="./Balle2d.java" first-line="1" last-line="1" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour ajouter les `import` et les méthodes obligatoires

1. Ajouter le code suivant dans `MondePong2d`

    ```java
    {{% embed src="./MondePong2d01.java" indent-level="1" %}}
    ```

1. Ajouter le code suivant dans `Balle2d`

    ```java
    {{% embed src="./Balle2d01.java" indent-level="1" %}}
    ```

## Initialiser le `CanvasPartie`

1. Ouvrir `CanvasPartie` et ajouter le code suivant

    ```java
    {{% embed src="./CanvasPartie01.java" indent-level="1" %}}
    ```

## Initialiser la `VuePartie`

1. Ouvrir `VuePartie` et ajouter le code suivant

    ```java
    {{% embed src="./VuePartie01.java" indent-level="1" %}}
    ```


1. Utiliser {{% key "Ctrl+1" %}} pour corriger les erreurs de compliation

1. S'assurer que l'application s'exécute sans erreur

    ```bash
    $ sh gradlew pong
    ```
    * NOTES:
        * c'est normal si le canvas n'affiche rien
        * on a pas encore appelé la méthode `drawOn` pour que le monde2d se dessine sur le canvas
