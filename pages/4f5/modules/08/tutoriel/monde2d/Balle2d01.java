public class Balle2d extends ObjetPong2d {

    public Balle2d() {
        super();
    }

    @Override
    public void initialize() {
        setWidth(10);
        setHeight(10);
        setTopLeftX(100);
        setTopLeftY(100);
    }

    @Override
    public void drawOnWorld(GraphicsContext gc) {
        gc.fillArc(getTopLeftX(),
                   getTopLeftY(),
                   getWidth(), 
                   getHeight(), 
                   0, 
                   360, 
                   ArcType.CHORD);
    }

    @Override
    protected boolean onMouseEvent(World2dMouseEventFx mouseEvent) {
        return false;
    }

    @Override
    public String id() {
        return "balle";
    }
}
