public class MsgAjouterPoint extends Message<MsgAjouterPoint> {
    
    private Position position;
    
    protected Position getPosition() {
        return position;
    }
    
    public MsgAjouterPoint setPosition(Position position) {
        this.position = position;
        
        return this;
    }

    public MsgAjouterPoint ajouterPointA(ModelePartie partie) {
        partie.ajouterPointPour(position);
        
        return this;
    }

    public MsgAjouterPoint envoyerMsgAjouterScoreAuRendezVous(ModelePartie partie) {
        partie.envoyerMsgAjouterPointAuRendezVous();

        return this;
    }

}
