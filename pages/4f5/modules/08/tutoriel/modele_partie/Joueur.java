public class Joueur implements ModelValue {
    
    private String id;
    private String prenom;
    private String nom;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Joueur() {
    }

    public Joueur(String id, String prenom, String nom) {
        setId(id);
        setPrenom(prenom);
        setNom(nom);
    }

    @Override
    public String toString() {
        return nomComplet();
    }
    
    public String nomComplet() {
        return prenom + " " + nom;
    }
    
    @Override
    public boolean equals(Object autre) {
        boolean siEgal = false;
        
        if(autre == this) {

            siEgal = true;

        }else if(autre instanceof Joueur) {
            
            siEgal = siEgal((Joueur) autre);

        }

        return siEgal;
    }
    
    private boolean siEgal(Joueur autre) {
        return siMemeIdQue(autre)
                && siMemePrenomQue(autre)
                && siMemeNomQue(autre);
    }
    
    private boolean siMemeIdQue(Joueur autre) {
        boolean siMemeId = false;
        
        if(id == null && autre.id == null) {
            
            siMemeId = true;

        }else if(id != null) {
            
            siMemeId = id.equals(autre.id);

        }
        
        return siMemeId;
    }

    private boolean siMemePrenomQue(Joueur autre) {
        boolean siMemePrenom = false;
        
        if(prenom == null && autre.prenom == null) {
            
            siMemePrenom = true;

        }else if(prenom != null) {
            
            siMemePrenom = prenom.equals(autre.prenom);

        }
        
        return siMemePrenom;
    }

    private boolean siMemeNomQue(Joueur autre) {
        boolean siMemeNom = false;
        
        if(nom == null && autre.nom == null) {
            
            siMemeNom = true;

        }else if(nom != null) {
            
            siMemeNom = nom.equals(autre.nom);

        }
        
        return siMemeNom;
    }

    public boolean estJoueur(String idJoueur) {
        return this.id.equals(idJoueur);
    }


    public boolean siIdEst(String autreId) {
        boolean siIdEst = false;
        
        if(id != null) {
            siIdEst = id.equals(autreId);
        }
        
        return siIdEst;
    }

    public void sEnvoyerMsgActionAutreJoueur(Position position, Action action) {
        Ntro.newMessage(MsgActionAutreJoueur.class)
            .setPosition(position)
            .setAction(action)
            .sendTo(id);
    }

    public void afficherSur(VuePartie vuePartie, Position position) {
        vuePartie.afficherNomJoueur(position, prenom);
        
    }

    public void memoriserIdJoueur(Map<Position, String> idJoueurParPosition, Position position) {
        idJoueurParPosition.put(position, id);
    }

}
