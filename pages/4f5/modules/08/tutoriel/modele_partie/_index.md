---
title: ""
weight: 1
bookHidden: true
---


# Optionnel: créer un `ModelePartie`

## Modèle et valeur pour mémoriser le score

1. Dans `valeurs`, créer la classe suivante

    ```java
    {{% embed src="./InfoJoueur01.java" indent-level="1" %}}
    ```

1. Dans `modeles`, créer la classe suivante

    ```java
    {{% embed src="./ModelePartie01.java" indent-level="1" %}}
    ```

1. Avec `registrar.registerModel`, **déclarer** le modèle et la valeur dans `AppPong`

    ```java
    {{% embed src="./AppPong01.java" indent-level="1" %}}
    ```

## Code pour afficher le score

1. Dans `partie.fxml`, ajouter le FXML pour afficher les noms et le pointage

    ```xml
    {{% embed src="./partie01.fxml" indent-level="1" %}}
    ```

1. Dans `VuePartie`, ajouter le code suivant

    ```java
    {{% embed src="./VuePartie01.java" indent-level="1" %}}
    ```

1. Dans `AfficherPartie`, ajouter la tâche suivante

    ```java
    {{% embed src="./AfficherPartie01.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `ModelePartie.afficherSur`

1. Dans `ModelePartie`, ajouter le code pour afficher

    ```java
    {{% embed src="./ModelePartie02.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `InfoJoueur.afficherSur`

1. Dans `InfoJoueur`, ajouter le code pour afficher

    ```java
    {{% embed src="./InfoJoueur02.java" indent-level="1" %}}
    ```
1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `Joueur.afficherSur`

1. Dans `Joueur`, ajouter le code pour afficher

    ```java
    {{% embed src="./Joueur01.java" indent-level="1" %}}
    ```


1. Vérifier que ça fonctionne

    ```bash
    $ sh gradlew pong
    ```

    <img class="figure" src="resultat01.png" />




## Message pour ajouter un point


1. Dans `messages`, ajouter la classe suivante

    ```java
    {{% embed src="./MsgAjouterPoint01.java" indent-level="1" %}}
    ```

1. Avec `registrar.registerMessage`, **déclarer** le message dans `AppPong`

    ```java
    {{% embed src="./AppPong02.java" indent-level="1" %}}
    ```

1. Dans `dorsal.taches`, ajouter la classe suivante

    ```java
    {{% embed src="./ModifierPartie01.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `MsgAjouterPoint.ajouterPointA`

    ```java
    {{% embed src="./MsgAjouterPoint02.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `ModelePartie.ajouterPointPour`

    ```java
    {{% embed src="./ModelePartie03.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `InfoJoueur.incrementerScore`

    ```java
    {{% embed src="./InfoJoueur03.java" indent-level="1" %}}
    ```


1. **Appeler** `ModifierPartie.creerTaches()` dans `DorsalPong`

    ```java
    {{% embed src="./DorsalPong01.java" indent-level="1" %}}
    ```

1. Vérifier le graphe du dorsal

    <center>
        <img width="100%" src="backend.png" />
    </center>


1. Dans `Balle2d`, ajouter le code pour compter des points

    ```java
    {{% embed src="./Balle2d.java" indent-level="1" %}}
    ```


1. Vérifier que ça fonctionne

<center>
<video width="50%" src="points.mp4" type="video/mp4" loop nocontrols autoplay>
</center>

## Problème: `ModelePartie` a pas les bons noms de joueurs!

<center>
<video width="50%" src="erreur_noms.mp4" type="video/mp4" loop nocontrols autoplay>
</center>

1. Les joueurs sont choisi au hasard dans le constructeur de `ModelePartie`

    * on devrait plutôt reprendre les mêmes joueurs que le rendez-vous

1. On va régler ce problème au module 9

    
