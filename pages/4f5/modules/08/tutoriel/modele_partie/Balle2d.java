public class Balle2d extends ObjetPong2d {

    // ...

    @Override
    public void initialize() {
        setWidth(10);
        setHeight(10);
        
        choisirEtatInitial();
    }

    private void choisirEtatInitial() {

        setTopLeftY(10);
        setSpeedY(100 + Ntro.random().nextInt(20));

        if(Ntro.random().nextBoolean()) {

            setTopLeftX(100);
            setSpeedX(100 + Ntro.random().nextInt(20));
            
        }else {

            setTopLeftX(MondePong2d.LARGEUR_MONDE - 100 - getWidth());
            setSpeedX(-100 - Ntro.random().nextInt(20));

        }
    }
    
    @Override
    public void onTimePasses(double secondsElapsed) {
        super.onTimePasses(secondsElapsed);
        
        if(balleFrappeMurGauche()) {

            // modifier
            choisirEtatInitial();
            ajouterPoint(Position.DROITE);
            
        } else if(balleFrappeMurDroit()) {

            // modifier
            choisirEtatInitial();
            ajouterPoint(Position.GAUCHE);

        } 

        // ...
        
    }

    // ajouter
    private void ajouterPoint(Position position) {
        Ntro.newMessage(MsgAjouterPoint.class)
            .setPosition(position)
            .send();
    }

