public class VuePartie extends ViewFx {

    // ...


    @FXML
    private Label labelNomPremierJoueur;

    @FXML
    private Label labelNomDeuxiemeJoueur;

    @FXML
    private Label labelScorePremierJoueur;

    @FXML
    private Label labelScoreDeuxiemeJoueur;
    
    private Map<Position, Label> labelsNoms = new HashMap<>();
    private Map<Position, Label> labelsScores = new HashMap<>();
    
    // ...

    @Override
    public void initialize() {

        // ...

        Ntro.assertNotNull(labelNomPremierJoueur);
        Ntro.assertNotNull(labelNomDeuxiemeJoueur);
        
        labelsNoms.put(Position.GAUCHE, labelNomPremierJoueur);
        labelsNoms.put(Position.DROITE, labelNomDeuxiemeJoueur);

        Ntro.assertNotNull(labelScorePremierJoueur);
        Ntro.assertNotNull(labelScoreDeuxiemeJoueur);
        
        labelsScores.put(Position.GAUCHE, labelScorePremierJoueur);
        labelsScores.put(Position.DROITE, labelScoreDeuxiemeJoueur);

        // ...
    }

    // ...

    public void afficherNomJoueur(Position position, String nom) {
        
        Label labelNom = labelsNoms.get(position);
        
        labelNom.setText(nom);

    }

    public void afficherScore(Position position, int score) {

        Label labelScore = labelsScores.get(position);

        labelScore.setText(String.valueOf(score));
    }
