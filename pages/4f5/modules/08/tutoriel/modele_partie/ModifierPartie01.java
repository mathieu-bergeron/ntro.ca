// ...

import static ca.ntro.app.tasks.backend.BackendTasks.*;

public class ModifierPartie {
    
    public static void creerTaches(BackendTasks tasks) {
        
        tasks.taskGroup("ModifierPartie")
        
             .waitsFor(model(ModelePartie.class))
        
              .contains(subTasks -> {

                    ajouterPoint(subTasks);

              });
    }

    private static void ajouterPoint(BackendTasks tasks) {

        tasks.task("ajouterPoint")

             .waitsFor(model(ModelePartie.class))

             .waitsFor(message(MsgAjouterPoint.class))

             .executes(inputs -> {
                 
                 MsgAjouterPoint msgAjouterPoint = inputs.get(message(MsgAjouterPoint.class));
                 ModelePartie    partie          = inputs.get(model(ModelePartie.class));
                 
                 msgAjouterPoint.ajouterPointA(partie);

             });
    }

}
