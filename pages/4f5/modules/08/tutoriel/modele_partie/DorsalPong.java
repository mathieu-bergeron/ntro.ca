public class DorsalPong extends LocalBackendNtro {

    @Override
    public void createTasks(BackendTasks tasks) {

        Initialisation.creerTaches(tasks);
        ModifierFileAttente.creerTaches(tasks);
        ModifierPartie.creerTaches(tasks);
    }


}
