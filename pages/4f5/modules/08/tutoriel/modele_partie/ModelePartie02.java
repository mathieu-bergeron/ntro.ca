public class ModelePartie implements Model {

    // ...
    //
    public void afficherSur(VuePartie vuePartie) {
        for(Map.Entry<Position, InfoJoueur> entry : infoJoueurParPosition.entrySet()) {
            
            Position position = entry.getKey();
            InfoJoueur infoJoueur = entry.getValue();
            
            infoJoueur.afficherSur(vuePartie, position);
        }
    }
