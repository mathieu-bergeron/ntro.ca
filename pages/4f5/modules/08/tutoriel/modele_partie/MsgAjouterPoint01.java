public class MsgAjouterPoint extends Message<MsgAjouterPoint> {
    
    private Position position;
    
    public MsgAjouterPoint setPosition(Position position) {
        this.position = position;
        
        return this;
    }

}
