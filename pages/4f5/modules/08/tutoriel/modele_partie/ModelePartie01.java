public class ModelePartie implements Model {
    
    private String                     idRendezVous          = null;
    private Map<Position, InfoJoueur>  infoJoueurParPosition = new HashMap<>(); 

    public ModelePartie(){
        ajouterJoueur(Position.GAUCHE, MaquetteJoueurs.joueurAleatoire());
        ajouterJoueur(Position.DROITE, MaquetteJoueurs.joueurAleatoire());
    }

    public ModelePartie setIdRendezVous(String idRendezVous) {
        this.idRendezVous = idRendezVous;
        
        return this;
    }

    public ModelePartie ajouterJoueur(Position position, Joueur joueur) {
        infoJoueurParPosition.put(position, new InfoJoueur(joueur));
        
        return this;
    }


    // ...
