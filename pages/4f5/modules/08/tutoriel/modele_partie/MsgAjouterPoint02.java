public class MsgAjouterPoint extends Message<MsgAjouterPoint> {

    // ...

    // remplir la méthode
    public MsgAjouterPoint ajouterPointA(ModelePartie partie) {
        partie.ajouterPointPour(position);
        
        return this;
    }
