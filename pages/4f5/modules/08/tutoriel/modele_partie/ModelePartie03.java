public class ModelePartie implements Model {
    
    // ...


    // remplir la méthode
    public void ajouterPointPour(Position position) {
        
        InfoJoueur infoJoueur = infoJoueurParPosition.get(position);
        
        if(infoJoueur != null) {
            
            infoJoueur.incrementerScore();
        }
    }
