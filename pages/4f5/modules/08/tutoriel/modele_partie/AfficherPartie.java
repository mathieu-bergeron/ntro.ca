public class AfficherPartie {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        creerDonneesVuePartie(tasks);
        
        tasks.taskGroup("AfficherPartie")

             .waitsFor("afficherVuePartie")

             .waitsFor(created(DonneesVuePartie.class))
             
             .andContains(subTasks -> {

                reagirActionJoueur(subTasks);

                reagirActionAutreJoueur(subTasks);

                reagirClicSouris(subTasks);

                prochaineImagePartie(subTasks);

                afficherInfoPartie(subTasks);

                prochaineBalle(subTasks);

             });
    }

    private static void creerDonneesVuePartie(FrontendTasks tasks) {

        tasks.task(create(DonneesVuePartie.class))
        
             .executesAndReturnsValue(inputs -> {

                    DonneesVuePartie       donneesVuePartie = new DonneesVuePartie();

                    return donneesVuePartie;

             });
    }

    private static void prochaineImagePartie(FrontendTasks subTasks) {

        subTasks.task("prochaineImagePartie")

                .waitsFor("afficherVuePartie")

                .waitsFor(clock().nextTick())
                
                .waitsFor(created(VuePartie.class))

                .waitsFor(created(DonneesVuePartie.class))
        
                .executes(inputs -> {
                    
                    Tick             tick             = inputs.get(clock().nextTick());
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    VuePartie        vuePartie        = inputs.get(created(VuePartie.class));
                    
                    donneesVuePartie.reagirTempsQuiPasse(tick.elapsedTime());
                    donneesVuePartie.afficherSur(vuePartie);

                });
    }

    private static void afficherInfoPartie(FrontendTasks subTasks) {

        subTasks.task("afficherInfoPartie")

                .waitsFor(modified(ModelePartie.class))

                .waitsFor(created(VuePartie.class))

        
                .executes(inputs -> {
                    
                    Modified<ModelePartie> partie           = inputs.get(modified(ModelePartie.class));
                    VuePartie              vuePartie        = inputs.get(created(VuePartie.class));
                    
                    partie.currentValue().afficherSur(vuePartie);

                });
    }

    private static void prochaineBalle(FrontendTasks subTasks) {

        subTasks.task("prochaineBalle")

                .waitsFor(modified(ModelePartie.class))

                .waitsFor(created(DonneesVuePartie.class))

        
                .executes(inputs -> {

                    Modified<ModelePartie> partie           = inputs.get(modified(ModelePartie.class));
                    DonneesVuePartie       donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    
                    partie.currentValue().lancerBalle(donneesVuePartie);

                });
    }

    private static void reagirActionJoueur(FrontendTasks subTasks) {

        subTasks.task("reagirActionJoueur")

                .waitsFor(event(EvtActionJoueur.class))

                .waitsFor(created(DonneesVuePartie.class))

                .waitsFor(modified(ModelePartie.class))
                
                .executes(inputs -> {
                    
                    SessionPong            session          = Ntro.session();
                    Modified<ModelePartie> partie           = inputs.get(modified(ModelePartie.class));
                    DonneesVuePartie       donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    EvtActionJoueur        evtActionJoueur  = inputs.get(event(EvtActionJoueur.class));
                    
                    boolean siNouvelleAction =  evtActionJoueur.appliquerA(donneesVuePartie);

                    if(siNouvelleAction) {
                        
                        partie.currentValue().diffuserMsgActionAutreJoueur(evtActionJoueur, session);

                    }

                });
    }

    private static void reagirClicSouris(FrontendTasks subTasks) {

        subTasks.task("reagirClicSouris")

                .waitsFor(event(EvtClicSouris.class))

                .waitsFor(created(DonneesVuePartie.class))
                
                .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    EvtClicSouris    evtClicSouris    = inputs.get(event(EvtClicSouris.class));

                    evtClicSouris.appliquerA(donneesVuePartie);

                });
    }

    private static void reagirActionAutreJoueur(FrontendTasks subTasks) {

        subTasks.task("reagirActionAutreJoueur")

                .waitsFor(created(DonneesVuePartie.class))

                .waitsFor(message(MsgActionAutreJoueur.class))
                
                .executes(inputs -> {
                    
                    SessionPong      session                  = Ntro.session();
                    DonneesVuePartie donneesVuePartie         = inputs.get(created(DonneesVuePartie.class));
                    MsgActionAutreJoueur msgActionAutreJoueur = inputs.get(message(MsgActionAutreJoueur.class));
                    
                    msgActionAutreJoueur.appliquerA(session, donneesVuePartie);
                });
    }
}
