public class ModifierPartie {
    
    public static void creerTaches(BackendTasks tasks) {
        
        tasks.taskGroup("ModifierPartie")
        
             .waitsFor(model(ModelePartie.class))
        
              .contains(subTasks -> {

                    creerPartie(subTasks);

                    rejoindrePartie(subTasks);
                  
                    ajouterPoint(subTasks);

                    quitterPartie(subTasks);

              });
    }

    private static void creerPartie(BackendTasks tasks) {

        tasks.task("creerPartie")

             .waitsFor(model(ModelePartie.class))

             .waitsFor(message(MsgCreerPartie.class))
             
             .executes(inputs -> {
                 
                 MsgCreerPartie msgCreerPartie = inputs.get(message(MsgCreerPartie.class));
                 ModelePartie   partie         = inputs.get(model(ModelePartie.class));
                 
                 msgCreerPartie.appliquerA(partie);

             });
    }

    private static void rejoindrePartie(BackendTasks tasks) {

        tasks.task("rejoindrePartie")

             .waitsFor(model(ModelePartie.class))

             .waitsFor(message(MsgRejoindrePartie.class))
             
             .executes(inputs -> {
                 
                 MsgRejoindrePartie msgRejoindrePartie = inputs.get(message(MsgRejoindrePartie.class));
                 ModelePartie       partie             = inputs.get(model(ModelePartie.class));
                 
                 msgRejoindrePartie.appliquerA(partie);

             });
    }

    private static void ajouterPoint(BackendTasks tasks) {

        tasks.task("ajouterPoint")

             .waitsFor(model(ModelePartie.class))

             .waitsFor(message(MsgAjouterPoint.class))

             .executes(inputs -> {
                 
                 MsgAjouterPoint msgAjouterPoint = inputs.get(message(MsgAjouterPoint.class));
                 ModelePartie    partie          = inputs.get(model(ModelePartie.class));
                 
                 msgAjouterPoint.ajouterPointA(partie)
                                .envoyerMsgAjouterScoreAuRendezVous(partie);
             });
    }

    private static void quitterPartie(BackendTasks tasks) {

        tasks.task("quitterPartie")

             .waitsFor(model(ModelePartie.class))

             .waitsFor(message(MsgQuitterPartie.class))
             
             .executes(inputs -> {
                 

                 MsgQuitterPartie msgQuitterPartie = inputs.get(message(MsgQuitterPartie.class));
                 ModelePartie     partie           = inputs.get(model(ModelePartie.class));
                 
                 msgQuitterPartie.appliquerA(partie);

             });
    }

}
