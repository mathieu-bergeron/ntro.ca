public class AfficherPartie {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        creerDonneesVuePartie(tasks);
        
        tasks.taskGroup("AfficherPartie")

             .waitsFor("afficherVuePartie")

             .waitsFor(created(DonneesVuePartie.class))
             
             .contains(subTasks -> {

                 // ...

                 // ajouter
                 afficherInfoPartie(subTasks);

             });
    }

    // ...

    private static void afficherInfoPartie(FrontendTasks subTasks) {

        subTasks.task("afficherInfoPartie")

                .waitsFor(modified(ModelePartie.class))

                .waitsFor(created(VuePartie.class))
        
                .executes(inputs -> {
                    
                    Modified<ModelePartie> partie           = inputs.get(modified(ModelePartie.class));
                    VuePartie              vuePartie        = inputs.get(created(VuePartie.class));
                    
                    partie.currentValue().afficherSur(vuePartie);

                });
    }
