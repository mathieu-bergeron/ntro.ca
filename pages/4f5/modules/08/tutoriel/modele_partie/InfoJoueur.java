public class InfoJoueur implements ModelValue {

    private Joueur            joueur  = null;
    private transient boolean siActif = false;
    private int               score   = 0;
    
    public InfoJoueur(Joueur joueur) {
        this.joueur = joueur;
    }

    public boolean siActif() {
        return siActif;
    }

    public boolean siIdJoueurEst(String idJoueur) {
        boolean siIdJoueurEst = false;
        
        if(joueur != null) {
            siIdJoueurEst = joueur.siIdEst(idJoueur);
        }
        
        return siIdJoueurEst;

    }

    public void devenirActif() {
        this.siActif = true;
    }

    public void devenirInactif() {
        this.siActif = false;
    }

    public void incrementerScore() {
        score++;
    }

    public void diffuserMsgActionAutreJoueur(Position position, Action action) {
        joueur.sEnvoyerMsgActionAutreJoueur(position, action);
    }

    public void afficherSur(VuePartie vuePartie, Position position) {
        vuePartie.afficherScore(position, score);
        joueur.afficherSur(vuePartie, position);
    }

    public void memoriserIdJoueur(Map<Position, String> idJoueurParPosition, Position position) {
        joueur.memoriserIdJoueur(idJoueurParPosition, position);
    }


}
