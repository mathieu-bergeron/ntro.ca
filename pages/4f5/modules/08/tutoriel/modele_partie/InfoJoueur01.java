public class InfoJoueur implements ModelValue {

    private Joueur            joueur  = null;
    private int               score   = 0;
    
    public InfoJoueur(Joueur joueur) {
        this.joueur = joueur;
    }

    public void afficherSur(VuePartie vuePartie, Position position) {
        vuePartie.afficherScore(position, score);
        joueur.afficherSur(vuePartie, position);
    }

}
