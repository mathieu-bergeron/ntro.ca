public class SessionPong extends Session<SessionPong> {

    // ...

    public SessionPong envoyerMsgAjouterPoint(Position position) {
        Ntro.newMessage(MsgAjouterPoint.class)
            .setPosition(position)
            .send();
        
        return this;
    }

    // ...
