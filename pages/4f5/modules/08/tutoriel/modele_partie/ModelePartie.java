public class ModelePartie implements Model {
    
    private String                     idRendezVous          = null;
    private Map<Position, InfoJoueur>  infoJoueurParPosition = new HashMap<>(); 
    private Balle2d                    prochaineBalle        = null;

    public ModelePartie setIdRendezVous(String idRendezVous) {
        this.idRendezVous = idRendezVous;
        
        return this;
    }

    public ModelePartie ajouterJoueur(Position position, Joueur joueur) {
        infoJoueurParPosition.put(position, new InfoJoueur(joueur));
        
        return this;
    }

    public void ajouterPointPour(Position position) {
        
        InfoJoueur infoJoueur = infoJoueurParPosition.get(position);
        
        if(infoJoueur != null) {
            
            infoJoueur.incrementerScore();
            
            Ntro.newMessage(MsgAjouterPointSurRendezVous.class)
                .setModelSelection(ModeleFileAttente.class, idRegion(idRendezVous))
                .setIdRendezVous(idRendezVous)
                .setPosition(position)
                .send();

        }
        
        if(siTousLesJoueursSontActifs()) {
            prochaineBalle = creerProchaineBalle();
        }
    }
    
    private String idRegion(String idRendezVous) {
        String idRegion = null;

        if(idRendezVous != null) {
            
            String[] segments = idRendezVous.split("¤");

            if(segments.length > 1) {
                idRegion = segments[0];
            }
        }
        
        return idRegion;
    }

    public void afficherSur(VuePartie vuePartie) {
        for(Map.Entry<Position, InfoJoueur> entry : infoJoueurParPosition.entrySet()) {
            
            Position position = entry.getKey();
            InfoJoueur infoJoueur = entry.getValue();
            
            infoJoueur.afficherSur(vuePartie, position);
        }
    }
    
    public void reagirJoueurDevientActif(Position position) {
        InfoJoueur infoJoueur = infoJoueurParPosition.get(position);

        if(infoJoueur != null) {
            infoJoueur.devenirActif();
        }

        if(siTousLesJoueursSontActifs()) {
            prochaineBalle = creerProchaineBalle();
        }
    }
    
    public Balle2d creerProchaineBalle() {
        Balle2d balle = new Balle2d();
        balle.choisirEtatInitial();

        return balle;
    }

    public void reagirJoueurDevientInactif(Position position) {
        InfoJoueur infoJoueur = infoJoueurParPosition.get(position);

        if(infoJoueur != null) {
            infoJoueur.devenirInactif();
        }
        
        if(!siTousLesJoueursSontActifs()) {
            prochaineBalle = null;
        }
    }

    private boolean siTousLesJoueursSontActifs() {
        boolean siTousActifs = true;
        
        for(Position position : Position.values()) {

            InfoJoueur infoJoueur = infoJoueurParPosition.get(position);
            
            boolean siActif = false;
            if(infoJoueur != null) {
                siActif = infoJoueur.siActif();
            }
            
            if(!siActif) {
                siTousActifs = false;
                break;
            }
            
        }
        
        return siTousActifs;
    }

    public void lancerBalle(DonneesVuePartie donneesVuePartie) {
        donneesVuePartie.lancerBalle(prochaineBalle);
    }

    public void diffuserMsgActionAutreJoueur(EvtActionJoueur evtActionJoueur, 
                                             SessionPong session) {
        
        session.diffuserMsgActionAutreJoueur(evtActionJoueur);
    }

}
