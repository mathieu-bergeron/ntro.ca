public class AppPong implements NtroAppFx {

    // ...
    
    @Override
    public void registerModels(MessageRegistrar registrar) {

        // ...

        registrar.registerModel(ModelePartie.class);
        registrar.registerValue(InfoJoueur.class);
    }

    // ...
