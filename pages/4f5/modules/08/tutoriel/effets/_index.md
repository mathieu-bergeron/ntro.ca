---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: ajouter des effets et des animations


1. Dans `Palette2d`, ajouter ce code

    ```java
    {{% embed src="./Palette2d.java" indent-level="1" %}}
    ```

1. Dans `Balle2d`, ajouter ce code

    ```java
    {{% embed src="./Balle2d.java" indent-level="1" %}}
    ```

1. Dans `MondePong2d` ajouter ce code

    ```java
    {{% embed src="./MondePong2d.java" indent-level="1" %}}
    ```

1. Vérifier que ça fonctionne

<center>
<video width="100%" src="optionnel05.mp4" type="video/mp4" loop nocontrols autoplay>
</center>
