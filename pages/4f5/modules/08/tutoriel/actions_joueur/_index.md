---
title: ""
weight: 1
bookHidden: true
---


# Réagir aux actions du joueur

## Représenter les actions

1. Créer le paquet `commun.enums`

1. Dans `commun.enums`, créer l'enum suivant

    ```java
    {{% embed src="./Action.java" indent-level="1" %}}
    ```

1. Dans `commun.enums`, créer l'enum suivant

    ```java
    {{% embed src="./Position.java" indent-level="1" %}}
    ```

1. S'assurer d'avoir l'arborescence suivante

    <img src="eclipse01.png"/>

## Déclencher les événements selon les actions actives

1. NOTE: Dans un jeu vidéo, si on appuie sur {{% key "↑" %}} et {{% key "↓" %}} en même temps, il faut mémoriser les
   deux actions. Si on relâche {{% key "↑" %}} tout en gardant {{% key "↓"%}} enfoncé, on s'attend à ce que l'action `↓` soit encore active.

1. Dans `frontal.evenements`, créer la classe suivante:

    ```java
    {{% embed src="./EvtActionJoueur01.java" indent-level="1" %}}
    ```

1. Avec `registrar.registerEvent`, **déclarer** l'événement dans le frontal

    ```java
    {{% embed src="./FrontalPong01.java" indent-level="1" %}}
    ```

1. Créer le paquet `frontal.utils`

1. Dans `frontal.utils`, créer la classe suivante:

    ```java
    {{% embed src="./ActionsActives01.java" indent-level="1" %}}
    ```

1. Dans `frontal.utils`, créer la classe suivante:

    ```java
    {{% embed src="./ActionsActivesParPosition01.java" indent-level="1" %}}
    ```

1. S'assurer d'avoir l'arborescence suivante

    <img src="eclipse02.png"/>

## Capter les touches du clavier et traduire en actions actives

1. Dans `partie.fxml`, ajouter un `fx:id` au conteneur principal

    ```xml
    {{% embed src="./partie.fxml" indent-level="1" %}}
    ```

1. Dans `VuePartie`, ajouter le code suivant

    ```java
    {{% embed src="./VuePartie01.java" indent-level="1" %}}
    ```

## Ajouter une tâche pour réagir aux événements `EvtActionJoueur`

1. Dans `AfficherPartie`, ajouter la tâche suivante

    ```java
    {{% embed src="./AfficherPartie01.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `EvtActionJoueur.appliquerA`

1. Coder la méthode `EvtActionJoueur.appliquerA`

    ```java
    {{% embed src="./EvtActionJoueur02.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `DonneesVuePartie.appliquerActionJoueur`

    ```java
    {{% embed src="./DonneesVuePartie01.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `MondePong2d.appliquerActionJoueur`, puis ajouter ce code

    ```java
    {{% embed src="./MondePong2d02.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer les méthodes `Palettes2d.monter`, `Palette2d.descendre` et `Palette2d.arreter`; puis ajouter ce code

    ```java
    {{% embed src="./Palette2d02.java" indent-level="1" %}}
    ```

## Vérifier



1. Vérifier que ça fonctionne

<center>
<video width="50%" src="actions_joueur.mp4" type="video/mp4" loop nocontrols autoplay>
</center>

<!--
1. Dans `ModifierPartie` ajouter la tâche suivante

    ```java
    {{% embed src="./ModifierPartie.java" indent-level="1" %}}
    ```
    -->
