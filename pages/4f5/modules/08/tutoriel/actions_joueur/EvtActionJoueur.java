public class EvtActionJoueur extends Event {
    
    private Position position;
    private Action action;

    public EvtActionJoueur setPosition(Position position) {
        this.position = position;
        
        return this;
    }
    
    public EvtActionJoueur setAction(Action action) {
        this.action = action;
        
        return this;
    }

    public EvtActionJoueur() {
    }

    public boolean appliquerA(DonneesVuePartie donneesVuePartie) {
        return donneesVuePartie.appliquerActionJoueur(position, action);
    }

    public void diffuserMsgActionAutreJoueur(String idPartie) {
        Ntro.newMessage(MsgActionAutreJoueur.class)
            .setIdPartie(idPartie)
            .setPosition(position)
            .setAction(action)
            .broadcast();
    }


}
