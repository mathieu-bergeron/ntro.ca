public class EvtActionJoueur extends Event {
    
    private Position position;
    private Action action;

    public EvtActionJoueur setPosition(Position position) {
        this.position = position;
        
        return this;
    }
    
    public EvtActionJoueur setAction(Action action) {
        this.action = action;
        
        return this;
    }
}
