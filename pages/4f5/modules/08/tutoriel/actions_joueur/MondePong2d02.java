public class MondePong2d extends World2dFx {

    // ...

    public void appliquerActionJoueur(Position position, Action action) {
        Palette2d palette = paletteParPosition(position);

        appliquerActionJoueur(palette, action);
    }

    private Palette2d paletteParPosition(Position position) {
        Palette2d palette;

        switch(position) {
            case GAUCHE:
            default:
                palette = paletteGauche;
                break;

            case DROITE:
                palette = paletteDroite;
                break;
        }

        return palette;
    }

    private void appliquerActionJoueur(Palette2d palette, Action action) {

        switch(action) {
            case MONTER:
                palette.monter();
                break;

            case DESCENDRE:
                palette.descendre();
                break;

            case ARRETER:
            default:
                palette.arreter();
        }
    }

    // ...
