public class AfficherPartie {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        // ...
        
        tasks.taskGroup("AfficherPartie")

             .waitsFor("afficherVuePartie")

             .waitsFor(created(DonneesVuePartie.class))
             
             .contains(subTasks -> {

                // ...

                reagirActionJoueur(subTasks);

                // ...

             });
    }

    private static void reagirActionJoueur(FrontendTasks subTasks) {

        subTasks.task("reagirActionJoueur")

                .waitsFor(event(EvtActionJoueur.class))

                .waitsFor(created(DonneesVuePartie.class))

                .executes(inputs -> {
                    
                    DonneesVuePartie       donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    EvtActionJoueur        evtActionJoueur  = inputs.get(event(EvtActionJoueur.class));
                    
                    evtActionJoueur.appliquerA(donneesVuePartie);

                });
    }

    // ...
