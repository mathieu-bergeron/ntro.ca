public class VuePartie extends ViewFx {

    // ...

    @FXML
    private Pane conteneurVuePartie;

    // ...
    
    private ActionsActivesParPosition actionsActives = new ActionsActivesParPosition();

    @Override
    public void initialize() {

        // ...
        
        Ntro.assertNotNull(conteneurVuePartie);


        // ...
        
        installerEvtActionJoueur();
    }

    private void installerEvtActionJoueur() {
        conteneurVuePartie.addEventFilter(KeyEvent.KEY_PRESSED, evtFx -> {

            Position position = positionPourTouche(evtFx.getCode());
            Action action = actionPourTouche(evtFx.getCode());
            
            actionsActives.activer(position, action);
        });

        conteneurVuePartie.addEventFilter(KeyEvent.KEY_RELEASED, evtFx -> {

            Position position = positionPourTouche(evtFx.getCode());
            Action action = actionPourTouche(evtFx.getCode());

            actionsActives.desactiver(position, action);
        });
    }

    private Position positionPourTouche(KeyCode code) {
        Position position = null;

        if(code.equals(KeyCode.W) || code.equals(KeyCode.S)) {

            position = Position.GAUCHE;

        } else if(code.equals(KeyCode.UP) || code.equals(KeyCode.DOWN)) {

            position = Position.DROITE;

        }

        return position;
    }

    private Action actionPourTouche(KeyCode code) {
        Action action = null;
        
        if(code.equals(KeyCode.W) || code.equals(KeyCode.UP)) {
            
            action = Action.MONTER;
            
        } else if(code.equals(KeyCode.S) || code.equals(KeyCode.DOWN)) {

            action = Action.DESCENDRE;

        }
        
        return action;
    }

    // ...
