public class DonneesVuePartie implements ViewData {
    
    private static long CALCULER_FPS_A_CHAQUE_X_MILLISECONDES = 200;

    private long horodatageDernierCalculFps = NtroCore.time().nowMilliseconds();
    private long imagesAfficheesDepuisDernierCalculFps = 0;
    private String fpsCourant = "0";

    private MondePong2d mondePong2d = new MondePong2d();

    public void reagirTempsQuiPasse(double secondesEcoulees) {
        mondePong2d.onTimePasses(secondesEcoulees);
    }

    public void afficherSur(VuePartie vuePartie) {
        calculerFpsSiNecessaire();

        vuePartie.viderCanvas();
        vuePartie.afficherImagesParSeconde("FPS " + fpsCourant);
        vuePartie.afficherPong2d(mondePong2d);
        
        imagesAfficheesDepuisDernierCalculFps++;
    }


    private void calculerFpsSiNecessaire() {
        long horodatageMaintenant = NtroCore.time().nowMilliseconds();
        long millisecondesEcoulees = horodatageMaintenant - horodatageDernierCalculFps;
        
        if(millisecondesEcoulees > CALCULER_FPS_A_CHAQUE_X_MILLISECONDES) {
            calculerFpsMaintenant(millisecondesEcoulees);

            imagesAfficheesDepuisDernierCalculFps = 0;
            horodatageDernierCalculFps = horodatageMaintenant;
        }
    }
    
    private void calculerFpsMaintenant(long millisecondesEcoulees) {
        double secondesEcoulees = millisecondesEcoulees / 1E3;
        double fps = imagesAfficheesDepuisDernierCalculFps / secondesEcoulees;
        fpsCourant = String.valueOf(Math.round(fps));
    }

    public boolean appliquerActionJoueur(Position position, Action action) {
        return mondePong2d.appliquerActionJoueur(position, action);
    }

    public void reagirClicSouris(World2dMouseEventFx mouseEvent) {
        mondePong2d.dispatchMouseEvent(mouseEvent);
    }

    public void lancerBalle(Balle2d balle) {
        mondePong2d.lancerBalle(balle);
    }

    public void diffuserMsgActionAutreJoueur(EvtActionJoueur evtActionJoueur) {
        
    }

}
