public class AfficherPartie {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        creerDonneesVuePartie(tasks);
        
        tasks.taskGroup("AfficherPartie")

             .waitsFor(created(DonneesVuePartie.class))

             .contains(subTasks -> {


                 // ...
                

                 // ajouter
                reagirClicSouris(subTasks);

             });
    }


    // ajouter
    private static void reagirClicSouris(FrontendTasks tasks) {

        tasks.task("reagirClicSouris")

                .waitsFor(created(DonneesVuePartie.class))

                .waitsFor(event(EvtClicSouris.class))
                
                .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    EvtClicSouris    evtClicSouris    = inputs.get(event(EvtClicSouris.class));

                    evtClicSouris.appliquerA(donneesVuePartie);

                });
    }
