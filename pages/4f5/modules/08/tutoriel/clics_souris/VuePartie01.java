public class VuePartie extends ViewFx {

    @Override
    public void initialize() {

        // ...

        installerEvtClicSouris();
    }

    // ...

    private void installerEvtClicSouris() {

        canvasPartie.onMouseEvent(mouseEventNtro -> {
            
            if(mouseEventNtro.mouseEventFx().getEventType().equals(MouseEvent.MOUSE_CLICKED)) {

                Ntro.newEvent(EvtClicSouris.class)
                    .setMouseEvent(mouseEventNtro)
                    .trigger();
            }
        });
    }
