---
title: ""
weight: 1
bookHidden: true
---


# Optionnel: réagir aux clics de souris


1. Dans `evenements`, ajouter la classe suivante

    ```java
    {{% embed src="./EvtClicSouris.java" indent-level="1" %}}
    ```

1. Avec `registrar.registerEvent`, **déclarer** l'événement dans le frontal

    ```java
    {{% embed src="./FrontalPong01.java" indent-level="1" %}}
    ```

1. Créer la méthode `reagirClicSouris` avec {{% key "Ctrl+1" %}}

1. Dans `VuePartie`, ajouter le code suivant

    ```java
    {{% embed src="./VuePartie01.java" indent-level="1" %}}
    ```

1. Dans `AfficherPartie`, ajouter la tâche suivante

    ```java
    {{% embed src="./AfficherPartie.java" indent-level="1" %}}
    ```

1. Dans `DonneesVuePartie`, ajouter le code suivant

    ```java
    {{% embed src="./DonneesVuePartie.java" indent-level="1" %}}
    ```

1. Dans `Palette2d`, ajouter le code suivant

    ```java
    {{% embed src="./Palette2d.java" indent-level="1" %}}
    ```

1. Dans `MondePong2d`, ajouter le code suivant

    ```java
    {{% embed src="./MondePong2d.java" indent-level="1" %}}
    ```

1. Vérifier que ça fonctionne

<center>
<video width="100%" src="optionnel03.mp4" type="video/mp4" loop nocontrols autoplay>
</center>
