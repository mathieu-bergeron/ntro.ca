public class Palette2d extends ObjetPong2d {

    // ajouter l'attribut
    private boolean selectionnee;



    @Override
    public void drawOnWorld(GraphicsContext gc) {
        gc.save();

        // ajouter
        if(selectionnee) {
            gc.setFill(Color.CYAN);
        }

        gc.fillRect(getTopLeftX(),
                    getTopLeftY(),
                    getWidth(), 
                    getHeight());

        gc.restore();
    }
    

    @Override
    protected boolean onMouseEvent(World2dMouseEventFx mouseEvent) {
        selectionnee = !selectionnee;

        return true;
    }

    public void deselectionner() {
        selectionnee = false;
    }

    // ...
   
}
