public class VuePartie extends ViewFx {

    @FXML
    private Pane conteneurVuePartie;
    
    @FXML
    private CanvasPartie canvasPartie;
    
    @FXML
    private Button boutonQuitterPartie;

    @FXML
    private Label labelNomPremierJoueur;

    @FXML
    private Label labelNomDeuxiemeJoueur;

    @FXML
    private Label labelScorePremierJoueur;

    @FXML
    private Label labelScoreDeuxiemeJoueur;

    @Override
    public void initialize() {
        Ntro.assertNotNull("boutonQuitterPartie", boutonQuitterPartie);
        Ntro.assertNotNull("canvasPartie", canvasPartie);

        Ntro.assertNotNull("labelNomPremierJoueur", labelNomPremierJoueur);
        Ntro.assertNotNull("labelNomDeuxiemeJoueur", labelNomDeuxiemeJoueur);

        Ntro.assertNotNull("labelScorePremierJoueur", labelScorePremierJoueur);
        Ntro.assertNotNull("labelScoreDeuxiemeJoueur", labelScoreDeuxiemeJoueur);
        
        Ntro.assertNotNull(conteneurVuePartie);

        installerEvtClicSouris();
        installerEvtQuitterPartie();
        installerEvtActionJoueur();
    }

    @SuppressWarnings("unchecked")
    private void installerEvtClicSouris() {

        EvtClicSouris evtClicSouris = Ntro.newEvent(EvtClicSouris.class);
        
        
        canvasPartie.onMouseEvent(mouseEventNtro -> {
            
            if(mouseEventNtro.mouseEventFx().getEventType().equals(MouseEvent.MOUSE_CLICKED)) {

                evtClicSouris.setMouseEvent(mouseEventNtro);
                evtClicSouris.trigger();
            }
        });
    }

    private void installerEvtQuitterPartie() {
        
        EvtAfficherFileAttente evtAfficherFileAttente = Ntro.newEvent(EvtAfficherFileAttente.class);
        EvtQuitterPartie       evtQuitterPartie       = Ntro.newEvent(EvtQuitterPartie.class);

        boutonQuitterPartie.setOnAction(evtFx -> {

            evtAfficherFileAttente.trigger();
            evtQuitterPartie.trigger();
        });
    }

    private void installerEvtActionJoueur() {
        
        EvtActionJoueur evtNtro = Ntro.newEvent(EvtActionJoueur.class);
        
        conteneurVuePartie.addEventFilter(KeyEvent.KEY_PRESSED, evtFx -> {
            
            if(evtFx.getCode().equals(KeyCode.W)) {
                
                evtNtro.setAction(new ActionJoueur(Cadran.GAUCHE, Action.HAUT));
                evtNtro.trigger();

            }else if(evtFx.getCode().equals(KeyCode.S)) {

                evtNtro.setAction(new ActionJoueur(Cadran.GAUCHE, Action.BAS));
                evtNtro.trigger();

            }else if(evtFx.getCode().equals(KeyCode.UP)) {

                evtNtro.setAction(new ActionJoueur(Cadran.DROITE, Action.HAUT));
                evtNtro.trigger();

            }else if(evtFx.getCode().equals(KeyCode.DOWN)) {

                evtNtro.setAction(new ActionJoueur(Cadran.DROITE, Action.BAS));
                evtNtro.trigger();
            }
        });

        conteneurVuePartie.addEventFilter(KeyEvent.KEY_RELEASED, evtFx -> {
            
            if(evtFx.getCode().equals(KeyCode.W)
                    || evtFx.getCode().equals(KeyCode.S)) {

                evtNtro.setAction(new ActionJoueur(Cadran.GAUCHE, Action.ARRET));
                evtNtro.trigger();

            }else if(evtFx.getCode().equals(KeyCode.UP)
                    || evtFx.getCode().equals(KeyCode.DOWN)) {

                evtNtro.setAction(new ActionJoueur(Cadran.DROITE, Action.ARRET));
                evtNtro.trigger();
            }
        });
    }

    public void viderCanvas() {
        canvasPartie.clearCanvas();
    }

    public void afficherPong2d(MondePong2d mondePong2d) {
        mondePong2d.drawOn(canvasPartie);
    }

    public void afficherImagesParSeconde(String imagesParSeconde) {
        canvasPartie.displayFps(imagesParSeconde);
    }


    public void afficherNomPremierJoueur(String nomPremierJoueur) {
        labelNomPremierJoueur.setText(nomPremierJoueur);
    }


    public void afficherNomDeuxiemeJoueur(String nomDeuxiemeJoueur) {
        labelNomDeuxiemeJoueur.setText(nomDeuxiemeJoueur);
    }


    public void afficherScorePremierJoueur(String scorePremierJoueur) {
        labelScorePremierJoueur.setText(scorePremierJoueur);
    }


    public void afficherScoreDeuxiemeJoueur(String scoreDeuxiemeJoueur) {
        labelScoreDeuxiemeJoueur.setText(scoreDeuxiemeJoueur);
    }

}
