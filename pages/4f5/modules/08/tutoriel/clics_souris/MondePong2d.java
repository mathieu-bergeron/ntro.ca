public class MondePong2d extends World2dFx<ObjetPong2d, MondePong2d> {

    // ...

    @Override
    protected void onMouseEventNotConsumed(World2dMouseEventFx mouseEvent) {
        paletteGauche.deselectionner();
        paletteDroite.deselectionner();
    }

