---
title: ""
weight: 1
bookHidden: true
---


# Optionnel: ajouter des sons


1. Télécharger {{% download "poc.wav" "poc.wav" %}} et placer le fichier dans `resources`

    * source: https://freesound.org/people/InspectorJ/sounds/411642/

1. Dans `Balle2d`, ajouter ce code

    ```java
    {{% embed src="./Balle2d.java" indent-level="1" %}}
    ```

1. Vérifier que ça fonctionne

<center>
<video width="100%" src="optionnel06.mp4" type="video/mp4" controls="true">
</center>
