---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: créer `DonneesVuePartie`

## Créer le `ViewData` nommé `DonneesVuePartie`

1. En VSCode, créer le paquet `frontal.donnees`

1. Dans le paquet `donnees`, créer la classe `DonneesVuePartie`

1. En VSCode, je m'assure d'avoir l'arborescence suivante

    <center>
        <img src="eclipse00.png"/>
    </center>

1. Ouvrir `DonneesVuePartie` et ajuster la signature

    ```java
    {{% embed src="./DonneesVuePartie01.java" first-line="1" last-line="1" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour ajouter l'`import` de `ViewData`

1. Dans `DonneesVuePartie`, ajouter les attributs suivants

    ```java
    {{% embed src="./DonneesVuePartie01.java" indent-level="1" %}}
    ```

    * NOTES:
        * le `fpsCourant` est une exemple de données spécifique à la Vue
        * on ne veux pas sauvegarder `fpsCourant` dans le fichier `.json`

## Déclarer `DonneesVuePartie`

1. Ouvrir `FrontalPong` et ajouter ce code

    ```java
    {{% embed src="./FrontalPong01.java" indent-level="1" %}}
    ```

    * NOTES
        * on déclare `DonneesVuePartie` dans le frontal
        * c'est spécifique au frontal, le dorsal n'a pas accès à cet classe

1. Utiliser {{% key "Ctrl+1" %}} pour ajouter l'`import` de `DonneesVuePartie`

## Instancier notre `DonneesVuePartie` avec une tâche

1. Dans le paquet `frontal.taches`, créer la classe `AfficherPartie`

1. En VSCode, je s'assurer d'avoir l'arborescence suivante

    <center>
        <img src="eclipse01.png"/>
    </center>

1. Ouvrir `AfficherPartie` et ajouter le code suivant

    ```java
    {{% embed src="./AfficherPartie01.java" indent-level="1" %}}
    ```

    * NOTES
        * ne pas oublier le `import static`
        <br>
        <br>
        <br>
        <span style="background-color:orange; padding:10px;border:2px dashed black;"> `import static ca.ntro.app.tasks.frontend.FrontendTasks.*;` </span>

1. Utiliser {{% key "Ctrl+1" %}} et je corriger les erreurs de compilation


1. Ouvrir `FrontalPong` et ajouter l'appel à la méthode `AfficherPartie.creerTaches`

    ```java
    {{% embed src="./FrontalPong02.java" indent-level="1" %}}
    ```


## Tester que ça fonctionne

1. S'assurer que `AppPong` s'exécute sans erreurs

    ```bash
    $ sh graldew pong
    ```

1. Vérifier le graphe de mon frontal:

    <center>
        <img width="100%" src="frontend.png" />
    </center>

    * NOTE: les tâches suivantes sont optionnelles:
        * `changerTaillePolice`
        * `changerLangue` 
        * `viewLoader[FragmentUneLangue]`
