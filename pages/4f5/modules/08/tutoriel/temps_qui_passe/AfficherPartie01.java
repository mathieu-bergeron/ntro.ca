subTasks.task("prochaineImagePartie")

         .waitsFor(created(VuePartie.class))
         .waitsFor(created(DonneesVuePartie.class))

         // s'assurer d'avoir
         .waitsFor(clock().nextTick())

         .executes(inputs -> {
            

            DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
            VuePartie        vuePartie        = inputs.get(created(VuePartie.class));

            // s'assurer d'avoir
            Tick             tick             = inputs.get(clock().nextTick());
                

            // ajouter
            donneesVuePartie.reagirTempsQuiPasse(tick.elapsedTime());

            donneesVuePartie.afficherSur(vuePartie);

        });
