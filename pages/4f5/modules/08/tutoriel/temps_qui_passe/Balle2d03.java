public class Balle2d extends ObjetPong2d {
    
    // ajouter
    private static final double EPSILON = 1;

    // ...

    // implanter la méthode
    private boolean balleFrappePlancher() {
        return getTopLeftY() + getHeight() >= getWorld2d().getHeight();
    }

    // implanter la méthode
    private boolean balleFrappePlafond() {
        return getTopLeftY() <= 0;
    }

    // implanter la méthode
    private boolean balleFrappeMurDroit() {
        return getTopLeftX() + getWidth() >= getWorld2d().getWidth();
    }

    // implanter la méthode
    private boolean balleFrappeMurGauche() {
        return getTopLeftX() <= 0;
    }

    // implanter la méthode
    private void balleRebondiSurPlancher() {
        setTopLeftY(getWorld2d().getHeight() - this.getHeight() - EPSILON);
        setSpeedY(-getSpeedY());
    }


    // implanter la méthode
    private void balleRebondiSurPlafond() {
        setTopLeftY(0 + EPSILON);
        setSpeedY(-getSpeedY());
    }

    // implanter la méthode
    private void balleRebondiSurMurDroit() {
        setTopLeftX(getWorld2d().getWidth() - this.getWidth() - EPSILON);
        setSpeedX(-getSpeedX());
    }

    // implanter la méthode
    private void balleRebondiSurMurGauche() {
        setTopLeftX(0 + EPSILON);
        setSpeedX(-getSpeedX());
    }

