---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: réagir au temps qui passe

## Animer le monde 2d

1. Ouvrir `AfficherPartie` et compléter la tâche `prochaineImagePartie` comme suit

    ```java
    {{% embed src="./AfficherPartie01.java" indent-level="1" %}}
    ```


1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `DonneesVuePartie.reagirTempsQuiPasse`

1. Dans `DonneesVuePartie`, compléter la méthode `reagirTempsQuiPasse` comme suit

    ```java
    {{% embed src="./DonneesVuePartie01.java" indent-level="1" %}}
    ```

## Faire bouger et rebondir la balle

1. Ouvrir `Balle2d` et spécifier une vitesse pour la balle

    ```java
    {{% embed src="./Balle2d01.java" indent-level="1" %}}
    ```

1. Dans `Balle2d` ajouter ce code pour donner un comportement à la balle

    ```java
    {{% embed src="./Balle2d02.java" indent-level="1" %}}
    ```

    * NOTES
        * le `super.onTimePasses` correspond au comportement par défaut d'un objet 2d
        * (c'est-à-dire se déplacer selon sa vitesse)

    
1. Utiliser {{% key "Ctrl+1" %}} pour créer les méthodes suivantes
    * `balleFrappeMurGauche`
    * `balleRebondiSurMurGauche`
    * `balleFrappeMurDroit`
    * `balleRebondiSurMurDroit`
    * `balleFrappePlafond`
    * `balleRebondiSurPlafond`
    * `balleFrappePlancher`
    * `balleRebondiSurPlancher`

1. Compléter le code de `Balle2d` comme suit

    
    ```java
    {{% embed src="./Balle2d03.java" indent-level="1" %}}
    ```

    * NOTE: le `EPSILON` permet de s'assurer que la balle est distancée du mur
        * (sinon elle peut rester «collée»)
       



## Tester que ça fonctionne

1. Exécuter `AppPong` pour vérifier que la balle rebondi sur les murs

    ```bash
    $ sh gradlew pong
    ```

    <center>
        <video width="50%" src="tut09.mp4" type="video/mp4" loop nocontrols autoplay>
    </center>

1. On peut aussi tester avec plusieurs balles en ajoutant ce code dans `MondePong2d`

    ```java
    {{% embed src="./MondePong2d01.java" indent-level="1" %}}
    ```

    ```bash
    $ sh gradlew pong
    ```

    <center>
        <video width="50%" src="tut09_100balles.mp4" type="video/mp4" loop nocontrols autoplay>
    </center>
