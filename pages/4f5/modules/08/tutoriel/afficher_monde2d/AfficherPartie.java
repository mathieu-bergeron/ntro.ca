public class AfficherPartie {

    public static void creerTaches(FrontendTasks tasks) {
        
        creerDonneesVuePartie(tasks);

        // ajouter le groupe de tâches
        tasks.taskGroup("AfficherPartie")

             .waitsFor("Initialisation")

             .waitsFor(created(DonneesVuePartie.class))

             .contains(subTasks -> {

                prochaineImagePartie(subTasks);

             });

    }

    // ...


    // ajouter la méthode
    private static void prochaineImagePartie(FrontendTasks subTasks) {

        subTasks.task("prochaineImagePartie")

                 .waitsFor(clock().nextTick())

                 .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    VuePartie        vuePartie        = inputs.get(created(VuePartie.class));
                        
                    donneesVuePartie.afficherSur(vuePartie);

                });
    }

}
