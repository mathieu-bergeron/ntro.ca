public class AfficherPartie {

    public static void creerTaches(FrontendTasks tasks) {
        
        creerDonneesVuePartie(tasks);

        // ajouter le groupe de tâches suivant
        tasks.taskGroup("AfficherPartie")

             .waitsFor(created(DonneesVuePartie.class))

             .waitsFor("afficherVuePartie")

             .contains(subTasks -> {

                prochaineImagePartie(subTasks);

             });

    }

    // ...


    // ajouter la méthode
    private static void prochaineImagePartie(FrontendTasks subTasks) {

        subTasks.task("prochaineImagePartie")

                 .waitsFor("afficherVuePartie")

                 .waitsFor(created(DonneesVuePartie.class))

                 .waitsFor(created(VuePartie.class))

                 .waitsFor(clock().nextTick())

                 .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    VuePartie        vuePartie        = inputs.get(created(VuePartie.class));
                    Tick             tick             = inputs.get(clock().nextTick());

                    // TODO: afficher le monde 2d
                });
    }
}
