public class AfficherPartie {

    // ...

    private static void prochaineImagePartie(FrontendTasks subTasks) {

        subTasks.task("prochaineImagePartie")

                 .waitsFor("afficherVuePartie")

                 .waitsFor(created(DonneesVuePartie.class))

                 .waitsFor(created(VuePartie.class))

                 .waitsFor(clock().nextTick())

                 .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    VuePartie        vuePartie        = inputs.get(created(VuePartie.class));
                    Tick             tick             = inputs.get(clock().nextTick());
                        
                    // ajouter
                    donneesVuePartie.afficherSur(vuePartie);

                });
    }

}
