---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: afficher le monde 2d

## Créer une tâche pour l'affichage temps-réel

1. Ouvrir `AfficherPartie` et ajouter ce code

    ```java
    {{% embed src="./AfficherPartie01.java" indent-level="1" %}}
    ```

    * NOTES
        * la tâche `prochaineImagePartie` sera appelée à chaque `clock().nextTick()`
        * ce qui veut dire: le plus souvent possible
        * (en JavaFx, typiquement 60 fois par secondes)


1. S'assurer que `AppPong` s'exécute

    ```bash
    $ sh gradlew pong
    ```

1. Vérifier le graphe de tâches du frontal

    <center>
        <img width="100%" src="frontend.png"/>
    </center>


## Coder l'affichage du monde 2d

1. Dans la tâche `prochaineImagePartie`, ajouter l'appel suivant

    ```java
    {{% embed src="./AfficherPartie02.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `DonneesVuePartie.afficherSur`


1. Dans `DonneesVuePartie`, ajouter ce code pour la méthode `afficherSur`

    ```java
    {{% embed src="./DonneesVuePartie01.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer les méthodes suivantes:
    * `VuePartie.viderCanvas`
    * `VuePartie.afficherImagesParSecondes`
    * `VuePartie.afficherPong2d`


1. Dans `VuePartie`, ajouter le code suivant

    ```java
    {{% embed src="./VuePartie01.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode suivante:
	* `CanvasPartie.afficherFps`

1. Dans `CanvasPartie`, ajouter le code suivant

    ```java
    {{% embed src="./CanvasPartie01.java" indent-level="1" %}}
    ```

1. S'assurer que `AppPong` s'exécute et affiche le monde 2d

    ```bash
    $ sh gradlew pong
    ```
    <center>
        <img width="100%" src="resultat_fps_0.png"/>
    </center>

    * NOTES
        * c'est normal que le compteur de FPS soit encore à 0


## Coder un compteur de FPS (images par seconde)

1. Ouvrir `DonneesVuePartie` et ajouter le code suivant:

    ```java
    {{% embed src="./DonneesVuePartie02.java" indent-level="1" %}}
    ```

1. S'assurer que `AppPong` s'exécute et affiche maintenant le compteur FPS

    ```bash
    $ sh gradlew pong
    ```

    <center>
        <img width="100%" src="resultat_fps_60.png"/>
    </center>

    * NOTE
    	* La plupart des implentations de JavaFX bloque le FPS à 60
