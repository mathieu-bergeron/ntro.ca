public class MondePong2d extends World2dFx {

    // ...
    
    // ajouter
    private Palette2d paletteGauche;
    private Palette2d paletteDroite;

    // ...

    @Override
    protected void initialize() {
        setWidth(LARGEUR_MONDE);
        setHeight(HAUTEUR_MONDE);

        // ...

        // ajouter
        paletteGauche = new Palette2d("gauche", 30);
        paletteDroite = new Palette2d("droite", LARGEUR_MONDE - 40);
        
        // ajouter
        addObject2d(paletteGauche);
        addObject2d(paletteDroite);

        // modifier pour que la balle mémorise les palettes
        balle = new Balle2d(paletteGauche, paletteDroite);
        addObject2d(balle);
        
    }

    // ...

