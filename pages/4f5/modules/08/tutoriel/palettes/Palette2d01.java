public class Palette2d extends ObjetPong2d {

    private String id;

    public Palette2d() {
        super();
    }

    public Palette2d(String id, double topLeftX) {
        super();
        
        this.id = id;

        setTopLeftX(topLeftX);
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public void initialize() {
        setWidth(10);
        setHeight(100);
        
        setTopLeftY(getWorld2d().getHeight()/2 - getHeight()/2);
    }

    @Override
    public void drawOnWorld(GraphicsContext gc) {
        gc.fillRect(getTopLeftX(),
                    getTopLeftY(),
                    getWidth(), 
                    getHeight());
    }
}
