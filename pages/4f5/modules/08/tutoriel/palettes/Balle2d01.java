public class Balle2d extends ObjetPong2d {

    // ...

    @Override
    public void onTimePasses(double secondsElapsed) {
        super.onTimePasses(secondsElapsed);
        
        if(balleFrappeMurGauche()) {

            balleRebondiSurMurGauche();
            
        } else if(balleFrappeMurDroit()) {

            balleRebondiSurMurDroit();

        // ajouter
        }else if(balleFrappePalette(paletteGauche)) {
            
            // ajouter
            balleRebondiSurPalette(paletteGauche);

        //ajouter
        }else if(balleFrappePalette(paletteDroite)) {
            
            // ajouter
            balleRebondiSurPalette(paletteDroite);

        }else if(balleFrappePlafond()) {
            
            balleRebondiSurPlafond();
            
        }else if(balleFrappePlancher()) {

            balleRebondiSurPlancher();
        }
    }

    // ajouter
    private boolean balleFrappePalette(Palette2d palette) {
        return collidesWith(palette);
    }

    // ajouter
    private void balleRebondiSurPalette(Palette2d palette) {

        if(getTopLeftX() < palette.getTopLeftX()) {

            setTopLeftX(palette.getTopLeftX() - getWidth() - EPSILON);

        }else {

            setTopLeftX(palette.getTopLeftX() + palette.getWidth() + EPSILON);

        }
        
        setSpeedX(-getSpeedX());
    }
