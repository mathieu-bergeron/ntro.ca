---
title: "Ajouter les palettes"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


1. Dans `commun.monde2d`, créer la classe `Palette2d`

    ```java
    {{% embed src="./Palette2d01.java" indent-level="1" %}}
    ```

1. Dans `Balle2d`, ajouter ce code pour mémoriser les palettes

    ```java
    {{% embed src="./Balle2d00.java" indent-level="1" %}}
    ```

1. Dans `Monde2Pongd`, ajouter les palettes

    ```java
    {{% embed src="./MondePong2d01.java" indent-level="1" %}}
    ```

1. Dans `Balle2d`, ajouter le code ci-bas pour rebondir sur les palettes

    ```java
    {{% embed src="./Balle2d01.java" indent-level="1" %}}
    ```

1. Vérifier que ça fonctionne

    ```bash
    $ sh gradlew pong
    ```

    <center>
        <video width="50%" src="palettes.mp4" type="video/mp4" loop nocontrols autoplay>
    </center>


1. Ou plus réaliste avec une seule balle:

    <center>
        <video width="50%" src="palettes_une_balle.mp4" type="video/mp4" loop nocontrols autoplay>
    </center>




