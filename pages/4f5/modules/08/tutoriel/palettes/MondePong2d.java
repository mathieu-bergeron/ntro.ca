public class MondePong2d extends World2dFx {
    
    public static final double LARGEUR_MONDE = 640;
    public static final double HAUTEUR_MONDE = 360;
    
    private Palette2d paletteGauche;
    private Palette2d paletteDroite;
    private Balle2d balle;

    public Palette2d getPaletteGauche() {
        return paletteGauche;
    }

    public void setPaletteGauche(Palette2d paletteGauche) {
        this.paletteGauche = paletteGauche;
    }

    public Palette2d getPaletteDroite() {
        return paletteDroite;
    }

    public void setPaletteDroite(Palette2d paletteDroite) {
        this.paletteDroite = paletteDroite;
    }

    public Balle2d getBalle() {
        return balle;
    }

    public void setBalle(Balle2d balle) {
        this.balle = balle;
    }

    @Override
    protected void initialize() {
        setWidth(LARGEUR_MONDE);
        setHeight(HAUTEUR_MONDE);

        paletteGauche = new Palette2d("gauche", 30);
        paletteDroite = new Palette2d("droite", LARGEUR_MONDE - 40);
        
        addObject2d(paletteGauche);
        addObject2d(paletteDroite);
        
        /*
        for(int i = 0; i < 100; i++) {
            addObject2d(new Balle2d(paletteGauche, paletteDroite));
        }
        */
    }

    @Override
    public void drawOn(ResizableWorld2dCanvasFx canvas) {
        canvas.drawOnWorld(gc -> {
            
            dessinerTerrain(gc);
        });

        super.drawOn(canvas);
    }

    private void dessinerTerrain(GraphicsContext gc) {
        
        gc.save();
        gc.setStroke(Color.LIGHTGREY);
        gc.setLineWidth(1);

        dessinerBordureTerrain(gc);
        dessinerLigneMediane(gc);

        gc.restore();
    }

    private void dessinerLigneMediane(GraphicsContext gc) {
        double ligneX = getWidth() / 2;
        double nombreCases = 64;
        double tailleCase = getHeight() / nombreCases;

        for(int i = 0; i < nombreCases; i++) {
            if(i%2 == 0) {
                double ligneY = i*tailleCase;
                gc.strokeLine(ligneX, 
                              ligneY, 
                              ligneX, 
                              ligneY + tailleCase);
            }
        }
    }

    private void dessinerBordureTerrain(GraphicsContext gc) {
        gc.strokeRect(0, 0, getWidth(), getHeight());
    }
    
    public boolean appliquerActionJoueur(Position cadran, Action action) {
        Palette2d palette = paletteDuCadran(cadran);

        return appliquerActionJoueur(palette, action);
    }

    private boolean appliquerActionJoueur(Palette2d palette, Action action) {
        boolean siNouvelleAction = false;

        switch(action) {
        case MONTER:
            siNouvelleAction = palette.monter();
            break;

        case DESCENDRE:
            siNouvelleAction = palette.descendre();
            break;

        case ARRETER:
            siNouvelleAction = palette.arreter();
        }

        return siNouvelleAction;
    }

    private Palette2d paletteDuCadran(Position cadran) {
        Palette2d palette;

        switch(cadran) {
        case GAUCHE:
        default:
            palette = paletteGauche;
            break;

        case DROITE:
            palette = paletteDroite;
            break;
        }

        return palette;
    }

    @Override
    protected void onMouseEventNotConsumed(World2dMouseEventFx mouseEvent) {
        paletteGauche.deselectionner();
        paletteDroite.deselectionner();
        if(balle != null) {
            balle.demarrerAnimation();
        }
    }

    public void lancerBalle(Balle2d balle) {
        removeObject("balle");

        if(balle != null) {
            this.balle = balle;
            this.balle.setPaletteGauche(paletteGauche);
            this.balle.setPaletteDroite(paletteDroite);
            addObject2d(balle);
        }
    }


}
