public class Palette2d extends ObjetPong2d {
    
    private static final double SECONDES_UN_EFFET = 0.4;
    private static final double VITESSE_PALETTE = 200;
    
    private boolean selectionnee;
    private double secondesRestantesPourEffet = 0;
    private transient InnerShadow innerShadow = new InnerShadow();
    
    private transient double minTopLeftY;
    private transient double maxTopLeftY;
    
    private String id;

    public Palette2d() {
        super();
    }

    public Palette2d(String id, double topLeftX) {
        super();
        
        this.id = id;

        setTopLeftX(topLeftX);
    }

    @Override
    public void initialize() {
        setWidth(10);
        setHeight(100);
        
        setTopLeftY(getWorld2d().getHeight()/2 - getHeight()/2);
        
        minTopLeftY = 0 - this.getHeight() / 2;
        maxTopLeftY = getWorld2d().getHeight() - this.getHeight() / 2;
    }

    public void onTimePasses(double secondsElapsed) {
        super.onTimePasses(secondsElapsed);
        
        if(getTopLeftY() < minTopLeftY) {
            setTopLeftY(minTopLeftY);
        }
        
        if(getTopLeftY() > maxTopLeftY) {
            setTopLeftY(maxTopLeftY);
        }
        
        if(secondesRestantesPourEffet > 0) {
            secondesRestantesPourEffet -= secondsElapsed;
        }
    }

    @Override
    public void drawOnWorld(GraphicsContext gc) {
        gc.save();

        if(selectionnee) {
            gc.setFill(Color.CYAN);
        }
        
        if(secondesRestantesPourEffet > 0) {
            innerShadow.setOffsetX(getWidth() * secondesRestantesPourEffet / SECONDES_UN_EFFET);
            innerShadow.setOffsetY(0);
            innerShadow.setColor(Color.DARKBLUE);
            gc.setEffect(innerShadow);
        }

        gc.fillRect(getTopLeftX(),
                    getTopLeftY(),
                    getWidth(), 
                    getHeight());
        
        gc.restore();
    }
    
    public void insererEffet() {
        secondesRestantesPourEffet = SECONDES_UN_EFFET;
    }
    

    public void deselectionner() {
        selectionnee = false;
    }

    public boolean monter() {
        boolean siNouvelleAction = false; 

        if(getSpeedY() >= 0) {
            setSpeedY(-VITESSE_PALETTE);
            siNouvelleAction = true;
        }

        return siNouvelleAction;
    }

    public boolean descendre() {
        boolean siNouvelleAction = false; 

        if(getSpeedY() <= 0) {
            setSpeedY(+VITESSE_PALETTE);
            siNouvelleAction = true;
        }

        return siNouvelleAction;
    }

    public boolean arreter() {
        boolean siNouvelleAction = false; 

        if(getSpeedY() != 0) {
            setSpeedY(0);
            siNouvelleAction = true;
        }

        return siNouvelleAction;
    }


    @Override
    protected boolean onMouseEvent(World2dMouseEventFx mouseEvent) {
        selectionnee = !selectionnee;

        return true;
    }

    @Override
    public String id() {
        return id;
    }

}
