---
title: ""
weight: 1
bookHidden: true
---


# Optionnel: afficher l'image d'une balle

1. Télécharger {{% download "balle.png" "balle.png" %}} et placer le fichier dans `resources/images`

    * source: https://www.educol.net/coloriage-balle-pour-baseball-i10387.html

1. Dans `Balle2d`, ajouter ce code

    ```java
    {{% embed src="./Balle2d.java" indent-level="1" %}}
    ```

1. Vérifier que ça fonctionne

<center>
<video width="100%" src="optionnel04.mp4" type="video/mp4" loop nocontrols autoplay>
</center>
