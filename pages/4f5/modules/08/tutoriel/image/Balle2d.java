import javafx.scene.image.Image;

public class Balle2d extends ObjetPong2d {

    private Image image = new Image("/images/balle.png");

    //...

    @Override
    public void drawOnWorld(GraphicsContext gc) {

        /* retirer
        gc.fillArc(getTopLeftX(),
                   getTopLeftY(),
                   getWidth(), 
                   getHeight(), 
                   0, 
                   360, ArcType.CHORD);
                   */
        
        // ajouter
        gc.drawImage(image,
                     getTopLeftX(),
                     getTopLeftY(),
                     getWidth(),
                     getHeight());
    }
