---
title: ""
weight: 1
bookHidden: true
---


# Optionnel: mettre la partie en pause

1. Pour l'instant, la partie continue de se dérouler même quand on ne l'affiche pas:

    <center>
    <video width="100%" src="tut08_sans_pause.mp4" type="video/mp4" loop nocontrols autoplay>
    </center>

    * p.ex. le pointage passe de 2-4 à 2-5 pendant que la partie n'est pas affichée


1. Dans `AfficherPartie`, ajouter ce code

    ```java
    {{% embed src="./AfficherPartie.java" indent-level="1" %}}
    ```

1. Dans `Navigation`, ajouter ce code

    ```java
    {{% embed src="./Navigation.java" indent-level="1" %}}
    ```

1. Vérifier que ça fonctionne

    <center>
    <video width="100%" src="tut08_avec_pause.mp4" type="video/mp4" loop nocontrols autoplay>
    </center>

**NOTES**

* On va voir une meilleure façon de mettre la partie sur pause au module 10
