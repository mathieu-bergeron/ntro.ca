public class Navigation {

    // ...

    private static void afficherVuePartie(FrontendTasks tasks) {

        tasks.task("afficherVuePartie")
        
             .waitsFor(event(EvtAfficherPartie.class))
              
             .executes(inputs -> {

                 VueRacine vueRacine = inputs.get(created(VueRacine.class));
                 VuePartie vuePartie = inputs.get(created(VuePartie.class));
                  
                 vueRacine.afficherSousVue(vuePartie);
                 
                 // ajouter
                 AfficherPartie.partieSurPause = false;
             });
    }

    private static void afficherVueFileAttente(FrontendTasks tasks) {

        tasks.task("afficherVueFileAttente")
        
              .waitsFor(event(EvtAfficherFileAttente.class))
              
              .executes(inputs -> {

                  
                  VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
                  VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));
                  
                  vueRacine.afficherSousVue(vueFileAttente);

                  // ajouter
                  AfficherPartie.partieSurPause = true;

              });
    }

    // ...
