---
title: "Naviguer vers la partie"
weight: 1
bookHidden: true
---


# Tutoriel: s'assurer de pouvoir naviguer vers la partie

1. Dans `FragmentRendezVous`, ajouter le code suivant:

    ```java
    {{% embed src="./FragmentRendezVous.java"  indent-level="1" %}}
    ```
