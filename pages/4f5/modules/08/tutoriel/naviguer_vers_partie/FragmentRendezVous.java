public class FragmentRendezVous extends ViewFx {

    // ...

    @FXML
    private Button boutonDebuterPartie; // s'assurer d'avoir

    
    // s'assurer d'avoir
    protected Button getBoutonDebuterPartie() {
        return boutonDebuterPartie;
    }

    @Override
    public void initialize() {

        Ntro.assertNotNull(boutonDebuterPartie);
        Ntro.assertNotNull(boutonRetirerRendezVous);
        Ntro.assertNotNull(labelNomPremierJoueur);

        // ajouter
        installerEvtAfficherPartie();
    }

    // ajouter
    protected void installerEvtAfficherPartie() {

        getBoutonDebuterPartie().setOnAction(evtFx -> {
            
            Ntro.newEvent(EvtAfficherPartie.class)
                .trigger();

        });
    }
