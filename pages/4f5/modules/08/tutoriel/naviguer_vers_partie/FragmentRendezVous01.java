package pong.frontal.fragments;

import java.util.List;

import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.ViewFx;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import pong.commun.messages.MsgRejoindrePartie;
import pong.commun.messages.MsgRetirerRendezVous;
import pong.commun.modeles.ModelePartie;
import pong.commun.valeurs.RendezVous;
import pong.frontal.SessionPong;
import pong.frontal.evenements.EvtAfficherPartie;

public class FragmentRendezVous extends ViewFx {

    @FXML
    private Button boutonDebuterPartie;

    @FXML
    private Button boutonRetirerRendezVous;
    
    @FXML
    private Label labelNomPremierJoueur;
    
    protected Button getBoutonDebuterPartie() {
        return boutonDebuterPartie;
    }

    @Override
    public void initialize() {

        Ntro.assertNotNull(boutonDebuterPartie);
        Ntro.assertNotNull(boutonRetirerRendezVous);
        Ntro.assertNotNull(labelNomPremierJoueur);

    }

    private void installerMsgRejoindreRendezVous(String idRendezVous) {

        boutonDebuterPartie.setOnAction(evtFx -> {

            Ntro.session(SessionPong.class)
                .envoyerMsgRejoindreRendezVous(idRendezVous);

        });
    }
    
    protected void installerMsgRetirerRendezVous(String idPartie) {
        
        boutonRetirerRendezVous.setOnAction(evtFx -> {

            Ntro.newMessage(MsgRetirerRendezVous.class)
                .setIdRendezVous(idPartie) 
                .send();

        });

    }

    public void afficherNomPremierJoueur(String nomPremierJoueur) {
        labelNomPremierJoueur.setText(nomPremierJoueur);
    }

    public void memoriserIdRendezVous(String idRendezVous) {
        installerMsgRejoindreRendezVous(idRendezVous);
        installerMsgRetirerRendezVous(idRendezVous);
    }

}
