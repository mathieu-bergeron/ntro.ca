---
title: ""
weight: 1
bookHidden: true
---

# Objectifs: créer une `VuePartie` simple


<br>
<br>

{{<excerpt>}}

Dans mon application, il faut au minimum **deux vues**

* Ma Vue personnalisée
* Ma vue acceuil, qui peut aussi servir de vue partie

OPTIONNEL) 

* ajouter une nouvelle vue pour afficher la partie
* s'assurer de pouvoir naviguer vers toutes les vues

{{</excerpt>}}

1. Effectuer le {{% link "../tutoriel/" "tutoriel" %}} pour comprendre comment
    * créer des données de Vue (`ViewData`)
    * créer et afficher un monde 2d
    * réagir au temps qui passe (animer le monde 2d)

* (OPTIONNEL) ajouter une nouvelle vue pour afficher la partie

1. Créer un `CanvasPartie` qui hérite de `ResizableWorld2dCanvasFx`

1. Sur ma vue accueil (ou ma nouvelle vue partie), ajouter un `CanvasPartie`

1. Créer mon propre monde2d

1. Créer mon propre objet2d

1. Créer mon propre `ViewData` (p.ex. `DonneesVuePartie`)

1. Ajouter le groupe de tâches `AfficherPartie` et des tâches pour:
    * créer une instance de mon `ViewData`
    * réagir au temps qui passe avec un `waitsFor(clock().nextTick())`

1. Afficher au moins un objet2d **en mouvement**

1. (OPTIONNEL) commencer à implanter le jeu
    * créer un `ModelePartie`
    * réagir aux actions du joueur
    * réagir aux événements de la souris
    * ajouter des effets
    * etc.

1. S'assurer que les noms d'attribut/méthode sont adéquats **pour mon projet**
    * on ne veut **pas de** `MondePong2d` dans un jeu d'échecs!

1. Pousser sur GitLab, p.ex:

    ```bash
    $ git add .
    $ git commit -a -m module08
    $ git push 
    ```

1. Vérifier que mes fichiers sont sur GitLab

1. Vérifier que mon projet est fonctionnel avec un `$ git clone` neuf, p.ex:

    ```bash
    # effacer le répertoire tmp s'il existe

    $ mkdir ~/tmp

    $ cd ~/tmp

    $ git clone https://gitlab.com/USAGER/4f5_prenom_nom

    $ cd 4f5_prenom_nom

    $ sh gradlew mon_projetFr

        # Doit afficher en français
        # Doit permettre de naviguer vers l'accueil (ou une page partie)
        # Doit afficher au moins un objet en mouvement

    $ sh gradlew mon_projetEn

        # Doit afficher en anglais
        # Doit permettre de naviguer vers l'accueil (ou une page partie)
        # Doit afficher au moins un objet en mouvement
    ```
