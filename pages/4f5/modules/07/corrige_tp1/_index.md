---
title: ""
weight: 1
bookHidden: true
---


# Utiliser le corrigé du TP1

## Version `mon_projet`

1. En VSCode, supprimer le projet `mon_projet`

1. Faire une copie de sauvegarde de mon répertoire `mon_projet`

        $ mv mon_projet _vieux_mon_projet

1. Télécharger {{% link "corrige_tp1.zip" "corrige_tp1.zip" %}}

1. Placer le fichier `corrige_tp1.zip` à la racine de mon dépôt Git

1. Décompresser l'archive sur place

    ```bash
    $ unzip -o corrige_tp1.zip
    ```
1. Restaurer les fichiers `.json`

    ```bash
    $ sh gradlew restoreJson
    ```

1. Vérifier en exécutant `mon_projet`

    ```bash
    $ sh gradlew mon_projet
    ```

    <img class="small-figure" src="verifier00.png"/>

## Renommer `mon_projet` vers mon jeu

1. Utiliser le script `renommer_corrige.sh` pour renommer le corrigé

    ```bash
    $ sh scripts/renommer_corrige.sh 

        # il faudra fournir le nom du paquet p.ex. tic_tac_toe
    ```

1. Ouvrir le workspace dans VSCode

    ```bash
    $ code 4f5.code-workspace
    ```


1. <span style="padding:10px;background-color:orange;border-width:2px;border-color:black;border-style:dotted">ATTENTION, il faut ensuite renommer plusieurs fichiers et classes</span>

    * la classe `AppMonProjet` en Java **et** dans `mon_projet/build.gradle`
    * les classes `ClientMonProjet` et `ServeurMonProjet` dans `mon_projet/build.gradle`
    * le fichier `ma_page.fxml`
    * la vue `VueMaPage`
    * le modèle `ModeleMaPage` et la valeur `MaValeur`
    * la maquette `MaquetteValeurs`
    * les attributs du modèles
    * l'événement `EvtAfficherVueMaPage`
    * plusieurs tâches, p.ex. `afficherVueMaPage`

