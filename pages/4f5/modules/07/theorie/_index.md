---
title: ""
weight: 1
bookHidden: true
---


# Théorie 7: améliorations des vues

<center>
<video width="50%" src="presentation.mp4" type="video/mp4" controls playsinline>
</center>

1. {{% link "./fragment/" "Notion de fragment" %}}

1. {{% link "./tailles_elastiques/" "Tailles elastiques" %}}

1. {{% link "./controles_personnalises/" "Contrôles personnalisés" %}}

<!--
1. {{% link "./canvas/" "Introduction au canvas" %}}

1. {{% link "./canvas_partie/" "Notre `CanvasPartie`" %}}
-->
