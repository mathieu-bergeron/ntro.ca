---
title: ""
weight: 1
bookHidden: true
---


# JavaFX: contrôle graphique personnalisé

## En Java

<center>
<video width="50%" src="java.mp4" type="video/mp4" controls playsinline>
</center>

1. Créer une classe qui hérite d'un composant graphique

```java
{{% embed src="./vues05a.java" first-line="1" last-line="7" %}}
```


1. Ajouter des attributs nommés au constructeur:

```java
{{% embed src="./vues05a.java" first-line="9" last-line="12" %}}
```


## En FXML

<center>
<video width="50%" src=".fxml.mp4" type="video/mp4" controls playsinline>
</center>

1. Importer la balise:

```xml
{{% embed src="./vues05a.fxml" first-line="1" last-line="1" %}}
```


1. Utiliser la balise comme les autres:

```xml
{{% embed src="./vues05a.fxml" first-line="3" last-line="6" %}}
```


1. Utiliser les attributs nommés:

```xml
{{% embed src="./vues05a.fxml" first-line="8" last-line="13" %}}
```


## Exemples du tutoriel 07

<center>
<video width="50%" src="flexbox.mp4" type="video/mp4" controls playsinline>
</center>

1. On définit un `FlexBoxRendezVous` 

    <video width="100%" src="tut07_flexbox.mp4" type="video/mp4" loop nocontrols autoplay>

    * NOTES:
        * JavaFx ne fournit pas de conteneur de type FlexBox
        * L'implantation de `Ntro` est simple:
            * on commence dans un `HBox` 
            * quand l'espace horizontal manque, on bascule en mode `VBox`
            * l'attribut `gap` indique l'espace horizontal on aimerait avoir

1. Voici comment utiliser notre `FlexBoxRendezVous`

    ```xml
    {{% embed src="./exemple_flexbox.fxml" indent-level="1" %}}
    ```

<center>
<video width="50%" src="canvas.mp4" type="video/mp4" controls playsinline>
</center>

