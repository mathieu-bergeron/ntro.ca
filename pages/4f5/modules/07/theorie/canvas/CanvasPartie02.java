onRedraw(() -> {

    drawOnWorld(gc -> {

        gc.setFill(Color.web("#9d4024"));

        gc.fillRect(0,
                    0, 
                    640, 
                    320);
    }

});
