---
title: ""
weight: 1
bookHidden: true
---


# Fonctionnement de notre `CanvasPartie`


<center>
<video width="50%" src="canvas_partie.mp4" type="video/mp4" controls playsinline>
</center>

{{<excerpt>}}

la taille par défaut est de  640 x **360** 

{{</excerpt>}}




1. Le `CanvasPartie` du tutoriel hérite de `ResizableWorld2dCanvasFx` qui est fourni par Ntro

1. Il faut l'initialiser avec `onRedraw`

    ```java
    {{% embed src="./CanvasPartie01.java" indent-level="1" %}}
    ```

    * `onRedraw` est appelé automatiquement quand la canvas change de taille

1. Il faut appeler `drawOnWorld` pour dessiner:

    ```java
    {{% embed src="./CanvasPartie02.java" indent-level="1" %}}
    ```

    * par défaut, le monde2d sur lequel on dessine a une taille de 640x360
    * l'image sera ensuite ajustée automatiquement selon la taille réelle de l'affichage

1. Plus de détails au module 8!

    



