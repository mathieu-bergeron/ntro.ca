public class FragmentRendezVous extends ViewFragmentFx {

    @FXML
    // ...
    
    @Override
    public void initialize() {

        // ...

}


// Dans le Frontal
@Override
public void registerViews(ViewRegistrarFx registrar) {

    // ...

    registrar.registerView(FragmentRendezVous.class, "/fragments/rendez_vous.fxml")

}

// Dans les tâches du Frontal

viewLoader(FragmentRendezVous.class)

// et

ViewLoader<FragmentRendezVous> viewLoaderRendezVous = inputs.get(viewLoader(FragmentRendezVous.class));
