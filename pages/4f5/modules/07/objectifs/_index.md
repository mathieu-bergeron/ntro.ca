---
title: ""
weight: 1
bookHidden: true
---


# Objectifs 8: améliorer votre Vue


<center>
<div style="background-color:orange;width:80%;border:2px dashed black;padding:10px">
<strong>IMPORTANT</strong>
<div style="text-align:left">
<ul>
<li>Créer au moins un fragment pour afficher ma Valeur personnalisée
<li>Améliorer ma vue avec au moins
    <ul>
        <li>un conteneur pour ajouter des fragments 
        <li>2 éléments graphiques avec des tailles élastiques
    </ul>
<li>Afficher mon modèle de façon graphique
</ul>

</div>
</center>

1. Effectuer le {{% link "../tutoriel/" "tutoriel" %}} pour comprendre comment
    * afficher une valeur personnalisée avec un fragment
    * utiliser des tailles élastiques

1. Sur ma Vue, ajouter un conteneur où insérer mes fragments
    * optionnel: insérer les fragments dans un `ComboBox` ou autre contrôle de JavaFx

1. Créer un fragment pour afficher ma valeur personnalisée

1. Implanter l'affichage des valeurs via le fragment

1. Utiliser des tailles élastiques pour au moins 2 éléments graphiques

1. M'assurer que tout fonctionne
    * je peux ajouter plusieurs valeurs personnalisées
    * ces valeurs s'affichent avec des fragments

    <div style="max-width:500px;">
    {{% animation src="/4f5/modules/07/objectifs/objectifs7.mp4" %}}
    </div>

1. M'assurer d'avoir des noms d'attribut/méthode sont adéquats **pour mon projet**
    * on ne veut **pas de** `FragmentRendezVous` dans une page paramètres!

1. Pousser mon projet sur GitLab, p.ex:

        $ git add .
        $ git commit -a -m module07
        $ git push 

1. Vérifier que mes fichiers sont sur GitLab

1. Vérifier que mon projet est fonctionnel avec un `$ git clone` neuf, p.ex:

        # effacer le répertoire tmp s'il existe

        $ mkdir ~/tmp

        $ cd ~/tmp

        $ git clone https://gitlab.com/USAGER/4f5_prenom_nom

        $ cd 4f5_prenom_nom

        $ sh gradlew restoreJson

        $ sh gradlew mon_projetFr

            # Doit afficher ma page directement, en français
            # Doit permettre d'ajouter ou retirer quelque chose au modèle
            # Doit afficher le modèle en mode graphique, avec des fragments
            # (on doit voir les modifications au modèle)

        $ sh gradlew mon_projetEn

            # Doit afficher ma page directement, en anglais
            # Doit permettre d'ajouter ou retirer quelque chose au modèle
            # Doit afficher le modèle en mode graphique, avec des fragments
            # (on doit voir les modifications au modèle)
