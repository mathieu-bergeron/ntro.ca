---
title: "Module 7: améliorer l'affichage"
weight: 80
draft: false
---


{{% pageTitle %}}



{{<excerpt class="note">}}

* optionnel: {{% link "./corrige_tut05" "corrigé tutoriel5" %}}
* optionnel: {{% link "./corrige_tp1" "corrigé TP1" %}}
    * `sh scripts/renommer_corrige.sh`

* **ATTENTION**: 
    * il n'y aura **pas** d'autre corrigé
    * (ni pour le TP#2, ni pour les tutoriels de l'étape#2)
{{</excerpt>}}


* {{% link "./theorie" "Théorie" %}}

    * vues qui s'adapte à la taille de la fenêtre
    * utiliser des *fragments* pour afficher le modèle
    * contrôles personnalisés: ajouter ses balises au FXML

* {{% link src="./tutoriel/" text="Tutoriel à faire avant les objectifs" %}}

    * créer les fragments `FragmentRendezVous` et `FragmentRendezVousComplet`
    * ajouter les `VBox.vgrow` et `HBox.hgrow` au FXML
    * ajouter les `-fx-min-width`/`-fx-max-width` au CSS

* {{% link src="./objectifs/" text="Objectifs pour avancer le TP#2" %}} 

    * afficher mon modèle à l'aide d'au moins un fragment
    * s'assurer que ma vue s'adapte à la taille de la fenêtre

* REMISES:
    * <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=304121">auto-évaluation pour ce module</a>

<br>
<div style="padding:5px;background:pink;border-style:dotted" >
<i>RAPPEL:</i> chaque étudiant.e est <strong>individuellement</strong> responsable de:
<ul>
<li>maintenir <i>son propre</i> dépôt Git
<li>présenter <i>son progrès</i> à la rencontre de travail
<li>faire son auto-évaluation
<li>s'assurer que son code est fonctionnel
</ul> 
<br>
<i>RAPPEL:</i> vous travaillez en équipe, mais les notes sont 100% <strong>individuelles</strong>
</div>
