package pong.maquettes;

import java.util.List;

import ca.ntro.core.initialization.Ntro;
import pong.commun.valeurs.Joueur;

public class MaquetteJoueurs {
	
	public static boolean modeTest = true;
	
	private static Joueur usagerCourant = usagerAleatoire();
	
	public static boolean siJoueurLocal(Joueur usager) {
		boolean siLocal = false;
		
		if(modeTest) {

			siLocal = true;

		}else if(usagerCourant.equals(usager)) {
			
			siLocal = true;
		}

		return siLocal;
	}

	public static Joueur usagerCourant() {
		return usagerCourant;
	}

	public static void prochainJoueur() {
		usagerCourant = eviterRepetitionDePrenom(usagerAleatoire());
	}

	private static Joueur usagerAleatoire() {
		Joueur usager = new Joueur();
		
		usager.setId(idAleatoire());
		usager.setPrenom(prenomAleatoire());
		usager.setNom(nomAleatoire());
		
		return usager;
	}

	private static Joueur eviterRepetitionDePrenom(Joueur usagerAleatoire) {

		while(usagerAleatoire.getPrenom().equals(usagerCourant.getPrenom())) {

			usagerAleatoire.setPrenom(prenomAleatoire());
		}
		
		return usagerAleatoire;
	}


	private static String idAleatoire() {
		return Ntro.random().nextId(4);
	}
	
	private static String prenomAleatoire() {

		List<String> choixDeNoms = List.of("Alice", 
				                           "Bob", 
				                           "Chaaya", 
				                           "Dominic", 
				                           "Élisabeth", 
				                           "Firas", 
				                           "Gregson",
				                           "Mehdi",
				                           "Louis",
				                           "Marcel",
				                           "Ashwin",
				                           "Ichiro",
				                           "Jun");

		return Ntro.random().choice(choixDeNoms);
	}

	private static String nomAleatoire() {

		List<String> choixDeNoms = List.of("Abdenouri", 
				                           "Ahmadi", 
				                           "Augustin", 
				                           "Chaussé", 
				                           "Delisle", 
				                           "Heer", 
				                           "Lagrois",
				                           "Daverna",
				                           "Gonzales",
				                           "Medjoubi",
				                           "Castillo",
				                           "Josan",
				                           "Yi");

		return Ntro.random().choice(choixDeNoms);
	}

	public static void initialize(String[] args) {
		String prenom = null;

		if(args.length > 0) {

			prenom = args[0];
			modeTest = false;

		}else {
			
			prenom = prenomAleatoire();
			
		}

		usagerCourant = new Joueur(idAleatoire(), 
				                   prenom, 
				                   nomAleatoire());
	}

}
