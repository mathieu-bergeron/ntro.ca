package pong;

import ca.ntro.app.NtroAppFx;
import ca.ntro.app.backend.BackendRegistrar;
import ca.ntro.app.frontend.FrontendRegistrarFx;
import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import pong.commun.messages.MsgAjouterRendezVous;
import pong.commun.modeles.ModeleFileAttente;
import pong.commun.valeurs.RendezVousComplet;
import pong.commun.valeurs.RendezVous;
import pong.commun.valeurs.Joueur;
import pong.dorsal.DorsalPong;
import pong.frontal.FrontalPong;


public class AppPong implements NtroAppFx {
	
	public static void main(String[] args) {
		NtroAppFx.launch(args);
	}

	@Override
	public void registerFrontend(FrontendRegistrarFx registrar) {

		registrar.registerFrontend(FrontalPong.class);

	}

	@Override
	public void registerMessages(MessageRegistrar registrar) {

		registrar.registerMessage(MsgAjouterRendezVous.class);

	}

	@Override
	public void registerModels(ModelRegistrar registrar) {

		registrar.registerModel(ModeleFileAttente.class);

		registrar.registerValue(RendezVous.class);
		registrar.registerValue(RendezVousComplet.class);
		registrar.registerValue(Joueur.class);

	}

	@Override
	public void registerBackend(BackendRegistrar registrar) {

		registrar.registerBackend(new DorsalPong());

	}

}
