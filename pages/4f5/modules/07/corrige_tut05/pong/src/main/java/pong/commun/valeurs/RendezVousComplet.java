package pong.commun.valeurs;

public class RendezVousComplet extends RendezVous {
	
	private String idPartie;
	private Joueur deuxiemeJoueur;

	public Joueur getDeuxiemeJoueur() {
		return deuxiemeJoueur;
	}

	public void setDeuxiemeJoueur(Joueur deuxiemeJoueur) {
		this.deuxiemeJoueur = deuxiemeJoueur;
	}

	public String getIdPartie() {
		return idPartie;
	}

	public void setIdPartie(String idPartie) {
		this.idPartie = idPartie;
	}
	
	public RendezVousComplet() {
		super();
	}

	public RendezVousComplet(String idRendezVous, 
			             Joueur premierJoueur,
			             Joueur deuxiemeJoueur,
			             String idPartie) {

		super(idRendezVous, premierJoueur);

		setIdPartie(idPartie);
		setDeuxiemeJoueur(deuxiemeJoueur);
	}

	
	@Override
	public String toString() {
		return getPremierJoueur().getPrenom() + " Vs " + deuxiemeJoueur.getPrenom();
	}
}
