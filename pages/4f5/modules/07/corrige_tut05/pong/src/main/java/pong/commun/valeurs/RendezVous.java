package pong.commun.valeurs;

import ca.ntro.app.models.ModelValue;

public class RendezVous implements ModelValue {
	
	private String idRendezVous;
	private Joueur premierJoueur;

	public Joueur getPremierJoueur() {
		return premierJoueur;
	}

	public void setPremierJoueur(Joueur premierJoueur) {
		this.premierJoueur = premierJoueur;
	}

	public String getIdRendezVous() {
		return idRendezVous;
	}

	public void setIdRendezVous(String idRendezVous) {
		this.idRendezVous = idRendezVous;
	}


	public RendezVous() {
	}

	public RendezVous(String idRendezVous, Joueur premierJoueur) {
		setIdRendezVous(idRendezVous);
		setPremierJoueur(premierJoueur);
	}

	@Override
	public String toString() {
		return premierJoueur.toString();
	}

}
