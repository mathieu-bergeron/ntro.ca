package pong.frontal.vues;

import ca.ntro.app.Ntro.
import ca.ntro.app.views.ViewFx;
import ca.ntro.core.initialization.Ntro;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import pong.frontal.evenements.EvtAfficherFileAttente;

public class VuePartie extends ViewFx {
	
	
	@FXML
	private Button boutonQuitterPartie;
	

	@Override
	public void initialize() {

		Ntro.assertNotNull("boutonQuitterPartie", boutonQuitterPartie);
		
		installerEvtAfficherFileAttente();
	}


	private void installerEvtAfficherFileAttente() {
		
		EvtAfficherFileAttente evtNtro = Ntro.newEvent(EvtAfficherFileAttente.class);

		boutonQuitterPartie.setOnAction(evtFx -> {

			evtNtro.trigger();
		});
	}

}
