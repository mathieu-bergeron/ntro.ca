---
title: ""
weight: 1
bookHidden: true
---

# Utiliser le corrigé du tutoriel 05

1. En VSCode, supprimer le projet `pong`

1. Faire une copie de sauvegarde de mon répertoire `pong`

    ```bash
    $ mv pong _vieux_pong
    ```

1. Télécharger {{% download "corrige_tut05.zip" "corrige_tut05.zip" %}}

1. Placer le fichier `corrige_tut05` à la racine de mon dépôt Git

1. Décompresser l'archive sur place

    ```bash
    $ unzip -o corrige_tut05.zip
    ```

1. Restaurer les fichiers `.json`

    ```bash
    $ sh gradlew restoreJson
    ```

1. Vérifier en exécutant `mon_projet`

    ```bash
    $ sh gradlew pong
    ```

    <img class="small-figure" src="verifier00.png"/>

1. Ouvrir le workspace en VSCode

    ```bash
    $ code 4f5.code-workspace
    ```

