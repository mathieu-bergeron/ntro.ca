public class VueRacine extends ViewFx {

    // ...

    // ajouter
    private ViewLoader<FragmentUneLangue> viewLoaderUneLangue;

    public void setViewLoaderUneLangue(ViewLoader<FragmentUneLangue> viewLoaderUneLangue) {
        this.viewLoaderUneLangue = viewLoaderUneLangue;
    }

    // ajouter
    private class ListCellUneLangue extends ListCell<String> {

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            
            if(item == null || empty) {
                
                setGraphic(null);
                
            }else {
                
                FragmentUneLangue fragmentUneLangue = viewLoaderUneLangue.createView();
                fragmentUneLangue.afficherLangue(item);
                setGraphic(fragmentUneLangue.rootNode());
            }
        }
    }

    // ...

    // modifier
    public void initialiserComboLangues() {
        // ajouter au début de la méthode
        comboLangues.setButtonCell(new ListCellUneLangue());
        comboLangues.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCellUneLangue();
            }
        });

        // ...
    }
