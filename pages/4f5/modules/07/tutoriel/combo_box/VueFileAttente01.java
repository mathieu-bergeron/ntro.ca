public class VueFileAttente extends ViewFx {

    @FXML
    private ComboBox<RendezVous> comboRendezVous;
    
    private class ListCellRendezVous extends ListCell<RendezVous> {

        @Override
        protected void updateItem(RendezVous rendezVous, boolean empty) {
            super.updateItem(rendezVous, empty);
            
            if(rendezVous == null || empty) {

                setGraphic(null);

            }else {
                
                FragmentRendezVous fragment = rendezVous.creerFragment(viewLoaderRendezVous, viewLoaderRendezVousComplet);
                rendezVous.afficherSur(fragment);
                setGraphic(fragment.rootNode());

            }
        }
    }

    @Override
    public void initialize() {

        // ...
        
        // ajouter
        initialiserComboRendezVous();
    }

    private void initialiserComboRendezVous() {
        comboRendezVous.setButtonCell(new ListCellRendezVous());
        comboRendezVous.setCellFactory(new Callback<ListView<RendezVous>, ListCell<RendezVous>>() {
            @Override
            public ListCell<RendezVous> call(ListView<RendezVous> param) {
                return new ListCellRendezVous();
            }
        });
    }

    public void ajouterRendezVous(RendezVous rendezVous) {
        comboRendezVous.getItems().add(rendezVous);
        comboRendezVous.setValue(comboRendezVous.getItems().get(comboRendezVous.getItems().size() - 1));
    }

    public void viderListeRendezVous() {
        comboRendezVous.getItems().clear();
    }
