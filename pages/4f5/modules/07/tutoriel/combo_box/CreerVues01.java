public class CreerVues {
    
    //...

    private static void creerVueRacine(FrontendTasks tasks) {

        tasks.task(create(VueRacine.class))

             .waitsFor(viewLoader(VueRacine.class))

             // ajouter
             .waitsFor(viewLoader(FragmentUneLangue.class))
             
             .executesAndReturnsValue(inputs -> {

                 ViewLoader<VueRacine>          viewLoader          = inputs.get(viewLoader(VueRacine.class));

                 // ajouter
                 ViewLoader<FragmentUneLangue>  viewLoaderUneLangue = inputs.get(viewLoader(FragmentUneLangue.class));
                 

                 VueRacine vueRacine = viewLoader.createView();

                 // ajouter
                 vueRacine.setViewLoaderUneLangue(viewLoaderUneLangue);
                 
                 // s'assurer d'avoir
                 vueRacine.initialiserComboLangues();
                 
                 return vueRacine;
             });
    }
