public class FrontalPong implements FrontendFx {
    
    // ...

    @Override
    public void registerViews(ViewRegistrarFx registrar) {

        // ...

        registrar.registerFragment(FragmentUneLangue.class, "/fragments/une_langue.fxml");
    }

