---
title: ""
weight: 1
bookHidden: true
---

# Tutoriel: `ComboBox` avec des fragments

## Objectif

Transformer ça

<img src="tut05.png" />

en ça

<img src="tut07.png" />

où le ComboBox contient maintenant des fragments plutôt que du texte


## Télécharger les images

1. Télécharger {{% download "fr.png" "fr.png" %}} et {{% download "en.png" "en.png" %}} et placer les fichiers dans `pong/src/main/resources/images`

1. Sources:
    * fr: https://commons.wikimedia.org/wiki/File:Flag_of_Quebec_%28modified_for_visibility%29.svg
    * en: https://commons.wikimedia.org/wiki/File:Flag_of_Canada_(Pantone).svg

## Créer le fragment

1. Créer `pong/src/main/resources/fragments/une_langue.fxml`

    ```xml
    {{% embed src="./une_langue01.fxml" indent-level="1" %}}
    ```

1. Creér la classe `pong.frontal.fragments.FragmentUneLangue`

    ```java
    {{% embed src="./FragmentUneLangue01.java" indent-level="1" %}}
    ```

1. Déclarer le fragment

    ```java
    {{% embed src="./FrontalPong01.java" indent-level="1" %}}
    ```

## Modifier `VueRacine` pour utiliser le fragment dans le ComboBox

```java
{{% embed src="./VueRacine01.java" indent-level="0" %}}
```

## Charger le fragment

1. Modifier la tâche `CreerVues.creerVueRacine`

```java
{{% embed src="./CreerVues01.java" indent-level="0" %}}
```


## Ajouter du CSS

1. Dans `prod.css` et `dev.css`

    ```
    {{% embed src="./prod01.css" indent-level="1" %}}
    ```

## Vérifier que ça fonctionne

```bash
$ sh gradlew pong
```

{{% animation src="/4f5/modules/07/tutoriel/combo_box/combo_box.mp4" %}}
