public class FragmentUneLangue extends ViewFx {
    
    @FXML
    private Label labelLangue;

    @FXML
    private ResizableImage imageLangue;

    @FXML
    private Pane espace;

    @Override
    public void initialize() {
        Ntro.assertNotNull(labelLangue);
        Ntro.assertNotNull(imageLangue);
    }
    
    public void afficherLangue(String langue) {
        labelLangue.setText(langue);
        
        String cheminImage = "/images/" + langue + ".png";
        
        URL image = FragmentUneLangue.class.getResource(cheminImage);
        
        if(image != null) {

            espace.setVisible(true);
            imageLangue.setVisible(true);
            imageLangue.setImage(new Image(cheminImage));
            
        }else {

            espace.setVisible(false);
            imageLangue.setVisible(false);
        }
    }

}
