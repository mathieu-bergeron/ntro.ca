---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 8: améliorer les vues

{{<excerpt class="max-width-75">}}

* S'assurer d'avoir terminé le tutoriel 05 (ou utiliser le {{% link "../corrige_tut05" "corrigé" %}})
{{</excerpt>}}

### Objectif

* Afficher les rendez-vous avec des éléments graphiques plutôt que du texte

* Transformer ça:

<center>
<img width="50%" src="tut05.png">
</center>


* En ça:

<center>
<img width="50%" src="tut07.png">
</center>

* NOTE: c'est les mêmes données


## Étapes obligatoires

1. {{% link "./conteneur_rendez_vous/" "Remplacer le `labelRendezVous` par un `conteneurRendezVous`" %}}

1. {{% link "./fragments/" "Créer les fragments `FragmentRendezVous` et `FragmentRendezVousComplet`" %}}

1. {{% link "./charger_les_fragments/" "Dans le frontal, charger les fragments" %}}

1. {{% link "./afficher_rendez_vous/" "Afficher chaque rendez-vous avec un fragment" %}}


1. {{% link "./tailles_elastiques/" "Utiliser des tailles elastiques" %}}

## Étapes optionnelles

1. {{% link "./retirer_rendez_vous/" "implanter la fonctionnalité «retirer un rendez-vous»" %}}

1. {{% link "./ajouter_avatar/" "ajouter un `ResizableImage` à la page des rendez-vous" %}}
    * (il y aussi un `ResizableAvatar`)

1. {{% link "./flexbox/" "créer et utiliser le contrôle `FlexBoxRendezVous`" %}}

1. {{% link "./combo_box/" "utiliser des fragments dans un `ComboBox`" %}}





