---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: utiliser des tailles élastiques

## Ajouter les tailles élastiques

1. Ouvrir `file_attente.fxml` et ajouter les `VBox.vgrow` et les `HBox.hgrow`

    ```xml
    {{% embed src="./file_attente.fxml" indent-level="1" %}}
    ```

1. Ouvrir `rendez_vous.fxml` ajouter les `HBox.hgrow`

    ```xml
    {{% embed src="./rendez_vous.fxml" indent-level="1" %}}
    ```

1. Ouvrir `rendez_vous_complet.fxml` et ajouter les `HBox.hgrow`

    ```xml
    {{% embed src="./partie_en_cours.fxml" indent-level="1" %}}
    ```


## Vérifier l'effet

1. Exécuter `AppPong` et vérifier l'effet des tailles élastiques

        $ sh gradlew pong

    <center>
        <img width="400px" src="resultat01.png"/>
    </center>

    <center>
        <img width="800px" src="resultat02.png"/>
    </center>
