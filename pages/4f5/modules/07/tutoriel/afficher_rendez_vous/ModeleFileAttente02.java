public class ModeleFileAttente implements Model {


	public void afficherSur(VueFileAttente vueFileAttente, 
			                ViewLoader<FragmentRendezVous> viewLoaderRendezVous,
			                ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet) {
		
		vueFileAttente.viderListeRendezVous();
		
		for(RendezVous rendezVous : rendezVousDansOrdre) {
			
			FragmentRendezVous fragmentRendezVous = rendezVous.creerFragment(viewLoaderRendezVous, viewLoaderRendezVousComplet);

			rendezVous.afficherSur(fragmentRendezVous);

			vueFileAttente.ajouterRendezVous(fragmentRendezVous);
		}
	}
