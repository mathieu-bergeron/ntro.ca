public class RendezVousComplet extends RendezVous {

    // ...

    // ajouter
    @Override
    public void afficherSur(FragmentRendezVous fragmentRendezVous) {
        super.afficherSur(fragmentRendezVous);
        
        FragmentRendezVousComplet fragmentRendezVousComplet = (FragmentRendezVousComplet) fragmentRendezVous;
        fragmentRendezVousComplet.afficherNomDeuxiemeJoueur(deuxiemeJoueur.nomCourt());
    }
