---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: afficher les rendez-vous

## Modifier `FragmentRendezVous` pour afficher le nom du premier joueur

```java
{{% embed src="./FragmentRendezVous01.java" indent-level="1" %}}
```

## Modifier `FragmentRendezVousComplet` pour afficher le nom du deuxième joueur

```java
{{% embed src="./FragmentRendezVousComplet01.java" indent-level="1" %}}
```

## Modifier `RendezVous` pour créer le bon fragment

```java
{{% embed src="./RendezVous01.java" indent-level="1" %}}
```

## Modifier `RendezVousComplet` pour créer le bon fragment

```java
{{% embed src="./RendezVousComplet01.java" indent-level="1" %}}
```

## Modifier `RendezVous` pour afficher sur un fragment

```java
{{% embed src="./RendezVous02.java" indent-level="1" %}}
```

## Modifier `RendezVousComplet` pour afficher sur un fragment

```java
{{% embed src="./RendezVousComplet02.java" indent-level="1" %}}
```

## Modifier `VueFileAttente` pour afficher avec des fragments

```java
{{% embed src="./VueFileAttente01.java" indent-level="1" %}}
```


## Modifier `ModeleFileAttente` pour afficher avec des fragments

```java
{{% embed src="./ModeleFileAttente01.java" indent-level="1" %}}
```
## Tester que ça fonctionne

1. Copier les données suivantes dans `_storage/models/ModeleFileAttente.json`

    ```json
    {{% embed src="ModeleFileAttente.json" indent-level="1" %}}
    ```

1. Exécuter `AppPong` et vérifier que le modèle s'affiche avec des éléments graphiques

    ```bash
    $ sh gradlew pong
    ```

    <center>
        <img width="75%" src="resultat.png"/>
    </center>

    * NOTES:
        * on voit que les tailles ne sont pas encore élastiques


    




