package pong.frontal.fragments;


import ca.ntro.app.Ntro;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import pong.frontal.SessionPong;
import pong.frontal.evenements.EvtAfficherPartie;

public class FragmentRendezVousComplet extends FragmentRendezVous {
	
	@FXML
	private Label labelNomDeuxiemeJoueur;

	@Override
	public void initialize() {
		super.initialize();

		Ntro.assertNotNull(labelNomDeuxiemeJoueur);
	}

	public void afficherNomDeuxiemeJoueur(String nomDeuxiemeJoueur) {
		labelNomDeuxiemeJoueur.setText(nomDeuxiemeJoueur);
	}

	private void installerEvtAfficherPartie(String idPartie, 
			                                String idPremierJoueur,
			                                String idDeuxiemeJoueur) {

		getBoutonOuvrirPartie().setOnAction(evtFx -> {
			
			Ntro.session(SessionPong.class)
			    .memoriserPartieCourante(idPartie, 
			    		                 idPremierJoueur, 
			    		                 idDeuxiemeJoueur)
			    .envoyerMessageRejoindrePartie();
			
			Ntro.newEvent(EvtAfficherPartie.class)
			    .setIdPartie(idPartie)
			    .trigger();

		});
	}
	
	public void installerMessages(String idRendezVous) {
		installerMsgRetirerRendezVous(idRendezVous);
	}

	public void installerMessages(String idPartie, 
			                      String idPremierJoueur, 
			                      String idDeuxiemeJoueur) {

		installerEvtAfficherPartie(idPartie,
				                   idPremierJoueur,
				                   idDeuxiemeJoueur);
	}
	

}
