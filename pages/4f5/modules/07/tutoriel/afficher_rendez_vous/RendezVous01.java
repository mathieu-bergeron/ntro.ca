public class RendezVous implements ModelValue {

    // ...

    // ajouter
    public FragmentRendezVous creerFragment(ViewLoader<FragmentRendezVous> viewLoaderRendezVous, 
                                            ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet) {
        
        return viewLoaderRendezVous.createView();
    }
