public class RendezVousComplet extends RendezVous {

    // ...

    // ajouter
    @Override
    public FragmentRendezVous creerFragment(ViewLoader<FragmentRendezVous> viewLoaderRendezVous, 
                                            ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet) {
        
        return viewLoaderRendezVousComplet.createView();
    }
