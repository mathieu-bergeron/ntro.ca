public class ModeleFileAttente implements Model , WatchJson, WriteObjectGraph {

    public void afficherSur(VueFileAttente vueFileAttente) {

        // ajouter
        vueFileAttente.viderListeRendezVous();
        
        // ajouter
        for(RendezVous rendezVous : rendezVousDansOrdre) {
            
            vueFileAttente.ajouterRendezVous(rendezVous);
        }
    }
