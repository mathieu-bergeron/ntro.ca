public class VueFileAttente extends ViewFx {
    
    // ...
    
    // ajouter
    public void ajouterRendezVous(RendezVous rendezVous) {
        FragmentRendezVous fragment = rendezVous.creerFragment(viewLoaderRendezVous, viewLoaderRendezVousComplet);
        rendezVous.afficherSur(fragment);

        conteneurRendezVous.getChildren().add(fragment.rootNode());
    }
    
    // ajouter
    public void viderListeRendezVous() {
        conteneurRendezVous.getChildren().clear();
    }
