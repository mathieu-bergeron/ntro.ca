public class AfficherFileAttente {

    // ...

	private static void afficherFileAttente(FrontendTasks tasks) {

		tasks.task("afficherFileAttente")
		
			 .waitsFor(modified(ModeleFileAttente.class))

			 .waitsFor(viewLoader(FragmentRendezVous.class))

			 .waitsFor(viewLoader(FragmentRendezVousComplet.class))

		     .executes(inputs -> {
		    	 
		    	 VueFileAttente                    vueFileAttente          = inputs.get(created(VueFileAttente.class));
		    	 Modified<ModeleFileAttente>       fileAttente             = inputs.get(modified(ModeleFileAttente.class));

		    	 // ajouter
		    	 ViewLoader<FragmentRendezVous>    viewLoaderRendezVous    = inputs.get(viewLoader(FragmentRendezVous.class));
		    	 ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet = inputs.get(viewLoader(FragmentRendezVousComplet.class));

		    	 // Prêt à afficher avec des fragments
		    	 
		     });
	}
}
