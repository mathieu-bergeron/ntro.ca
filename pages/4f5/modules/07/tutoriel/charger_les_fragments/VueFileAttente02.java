public class VueFileAttente extends ViewFx {

    // ...

    public void ajouterRendezVous(RendezVous rendezVous) {

        FragmentRendezVous fragment = rendezVous.creerFragment(viewLoaderRendezVous, viewLoaderRendezVousComplet);

        rendezVous.afficherSur(fragment);

        conteneurRendezVous.getChildren().add(fragment.rootNode());
    }
    

    public void viderListeRendezVous() {
        conteneurRendezVous.getChildren().clear();
    }
