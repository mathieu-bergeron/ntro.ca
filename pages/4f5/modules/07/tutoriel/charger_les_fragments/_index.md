---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: charger les fragments

## Charger les fragments

1. Dans `VueFileAttente`, ajouter des attributs pour mémoriser un `ViewLoader` pour chaque fragment

    ```java
    {{% embed src="VueFileAttente01.java" indent-level="1" %}}
    ```

    * générer les accesseurs avec {{% key "Shift+Alt+S" %}} => *Generate Setters and Getters*

1. Dans `CreerVues.java`, dans la tâche `creerVueFileAttente`, charger les `ViewLoader` des fragments

    ```java
    {{% embed src="Initialisation01.java" indent-level="1" %}}
    ```

1. Exécuter pour vérifier le graphe de tâches

    ```bash
    $ sh gradlew pong
    ```

    <center>
        <img width="100%" src="frontend.png"/>
    </center>

    en particulier le chargement des fragments:

    <center>
        <img class="small-figure" src="frontend_viewloaders.png"/>
    </center>
