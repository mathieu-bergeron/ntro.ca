public class RendezVousComplet extends RendezVous {
    
    private String idPartie;
    private Joueur deuxiemeJoueur;

    public Joueur getDeuxiemeJoueur() {
        return deuxiemeJoueur;
    }

    public void setDeuxiemeJoueur(Joueur deuxiemeJoueur) {
        this.deuxiemeJoueur = deuxiemeJoueur;
    }

    public String getIdPartie() {
        return idPartie;
    }

    public void setIdPartie(String idPartie) {
        this.idPartie = idPartie;
    }
    
    public RendezVousComplet() {
        super();
    }

    public RendezVousComplet(String idRendezVous, 
                         Joueur premierJoueur,
                         Joueur deuxiemeJoueur,
                         String idPartie) {

        super(idRendezVous, premierJoueur);

        setIdPartie(idPartie);
        setDeuxiemeJoueur(deuxiemeJoueur);
    }


    @Override
    public FragmentRendezVous creerFragment(ViewLoader<FragmentRendezVous> viewLoaderRendezVous, 
                                            ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet) {
        
        return viewLoaderRendezVousComplet.createView();
    }

    public void afficherSur(FragmentRendezVous fragmentRendezVous) {
        super.afficherSur(fragmentRendezVous);
        
        FragmentRendezVousComplet fragmentRendezVousComplet = (FragmentRendezVousComplet) fragmentRendezVous;
        fragmentRendezVousComplet.afficherNomDeuxiemeJoueur(deuxiemeJoueur.getPrenom());

    }
    
    @Override
    public String toString() {
        return getPremierJoueur().getPrenom() + " Vs " + deuxiemeJoueur.getPrenom();
    }

    @Override
    protected boolean siFragmentDuBonType(FragmentRendezVous fragmentRendezVous) {
        return fragmentRendezVous.getClass().equals(FragmentRendezVousComplet.class);
    }

}
