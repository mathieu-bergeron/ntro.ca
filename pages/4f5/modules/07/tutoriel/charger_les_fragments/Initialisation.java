public class Initialisation {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("Initialisation")

             .contains(subTasks -> {

                creerVueRacine(subTasks);
                creerVueFileAttente(subTasks);
                creerVuePartie(subTasks);
                 
                installerVueRacine(subTasks);
                installerVueFileAttente(subTasks);

                afficherFenetre(subTasks);

             });
    }

    private static void creerVuePartie(FrontendTasks tasks) {

        tasks.task(create(VuePartie.class))

             .waitsFor(viewLoader(VuePartie.class))

             .executesAndReturnsValue(inputs -> {
                 
                 ViewLoader<VuePartie> viewLoader = inputs.get(viewLoader(VuePartie.class));
                 
                 VuePartie vuePartie = viewLoader.createView();

                 return vuePartie;
             });
    }

    private static void creerVueRacine(FrontendTasks tasks) {

        tasks.task(create(VueRacine.class))

             .waitsFor(viewLoader(VueRacine.class))
             
             .executesAndReturnsValue(inputs -> {

                 ViewLoader<VueRacine> viewLoader = inputs.get(viewLoader(VueRacine.class));
                 
                 VueRacine vueRacine = viewLoader.createView();
                 
                 return vueRacine;
             });
    }

    private static void installerVueRacine(FrontendTasks tasks) {

        tasks.task("installerVueRacine")
        
              .waitsFor(window())
              
              .waitsFor(created(VueRacine.class))
              
              .executes(inputs -> {
                  
                  VueRacine vueRacine = inputs.get(created(VueRacine.class));
                  Window    window    = inputs.get(window());

                  window.installRootView(vueRacine);
              });
    }

    private static void creerVueFileAttente(FrontendTasks tasks) {

        tasks.task(create(VueFileAttente.class))

             .waitsFor(viewLoader(VueFileAttente.class))

             .waitsFor(viewLoader(FragmentRendezVous.class))

             .waitsFor(viewLoader(FragmentRendezVousComplet.class))

             .executesAndReturnsValue(inputs -> {
                 
                 ViewLoader<VueFileAttente>        viewLoaderFileAttente   = inputs.get(viewLoader(VueFileAttente.class));
                 ViewLoader<FragmentRendezVous>    viewLoaderRendezVous    = inputs.get(viewLoader(FragmentRendezVous.class));
                 ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet = inputs.get(viewLoader(FragmentRendezVousComplet.class));
                 
                 VueFileAttente vueFileAttente = viewLoaderFileAttente.createView();
                 
                 vueFileAttente.setViewLoaderRendezVous(viewLoaderRendezVous);
                 vueFileAttente.setViewLoaderRendezVousComplet(viewLoaderRendezVousComplet);

                 return vueFileAttente;
             });
    }


    private static void installerVueFileAttente(FrontendTasks tasks) {

        tasks.task("installerVueFileAttente")
        
              .waitsFor("installerVueRacine")

              .waitsFor(created(VueFileAttente.class))

              .executes(inputs -> {
                  
                  VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
                  VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));
                  
                  vueRacine.afficherSousVue(vueFileAttente);
              });
    }


    private static void afficherFenetre(FrontendTasks tasks) {
        tasks.task("afficherFenetre")
        
             .waitsFor(window())
             
             .executes(inputs -> {

                 Window window = (Window) inputs.get(window());

                 window.show();
             });
    }

}
