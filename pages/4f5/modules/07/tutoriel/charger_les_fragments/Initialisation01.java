public class Initialisation {

    private static void creerVueFileAttente(FrontendTasks tasks) {

        tasks.task(create(VueFileAttente.class))

             .waitsFor(viewLoader(VueFileAttente.class))

             // ajouter
             .waitsFor(viewLoader(FragmentRendezVous.class))

             // ajouter
             .waitsFor(viewLoader(FragmentRendezVousComplet.class))

             .executesAndReturnsValue(inputs -> {
                 
                 ViewLoader<VueFileAttente>        viewLoaderFileAttente   = inputs.get(viewLoader(VueFileAttente.class));
                 //ajouter
                 ViewLoader<FragmentRendezVous>    viewLoaderRendezVous    = inputs.get(viewLoader(FragmentRendezVous.class));
                 ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet = inputs.get(viewLoader(FragmentRendezVousComplet.class));
                 
                 VueFileAttente vueFileAttente = viewLoaderFileAttente.createView();
                 
                 // ajouter
                 vueFileAttente.setViewLoaderRendezVous(viewLoaderRendezVous);
                 vueFileAttente.setViewLoaderRendezVousComplet(viewLoaderRendezVousComplet);

                 return vueFileAttente;
             });
    }
