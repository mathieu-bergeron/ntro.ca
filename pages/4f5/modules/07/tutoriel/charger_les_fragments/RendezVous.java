public class RendezVous implements ModelValue {
    
    private String idRendezVous;
    private Joueur premierJoueur;

    public Joueur getPremierJoueur() {
        return premierJoueur;
    }

    public void setPremierJoueur(Joueur premierJoueur) {
        this.premierJoueur = premierJoueur;
    }

    public String getIdRendezVous() {
        return idRendezVous;
    }

    public void setIdRendezVous(String idRendezVous) {
        this.idRendezVous = idRendezVous;
    }


    public RendezVous() {
    }

    public RendezVous(String idRendezVous, Joueur premierJoueur) {
        setIdRendezVous(idRendezVous);
        setPremierJoueur(premierJoueur);
    }

    @Override
    public String toString() {
        return premierJoueur.toString();
    }

    public void afficherSur(FragmentRendezVous fragmentRendezVous) {
        fragmentRendezVous.memoriserIdRendezVous(idRendezVous);
        fragmentRendezVous.afficherNomPremierJoueur(premierJoueur.getPrenom());
    }

    public FragmentRendezVous creerFragment(ViewLoader<FragmentRendezVous> viewLoaderRendezVous, 
                                  ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet) {
        
        return viewLoaderRendezVous.createView();
    }

    public RendezVousComplet creerRendezVousComplet(Joueur deuxiemeJoueur, String idPartie) {

        return new RendezVousComplet(idRendezVous, 
                                 premierJoueur, 
                                 deuxiemeJoueur,
                                 idPartie);
    }

    public boolean siIdEst(String idRendezVous) {
        return this.idRendezVous.equals(idRendezVous);
    }

    public boolean estLeRendezVous(String idRendezVous) {
        return getIdRendezVous().equals(idRendezVous);
    }

    public boolean siContenuDans(ModeleFileAttente fileAttente) {
        return fileAttente.siContientRendezVous(idRendezVous);
    }

    protected boolean siFragmentDuBonType(FragmentRendezVous fragmentRendezVous) {
        return fragmentRendezVous.getClass().equals(FragmentRendezVous.class);
    }
}
