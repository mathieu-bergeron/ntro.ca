package pong.frontal.taches;


import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.core.reflection.observer.Modified;
import pong.frontal.vues.VueFileAttente;
import pong.frontal.vues.fragments.FragmentRendezVousComplet;
import pong.frontal.vues.fragments.FragmentRendezVous;
import pong.modeles.ModeleFileAttente;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import ca.ntro.app.frontend.ViewLoader;


public class AfficherFileAttente {
	
	public static void creerTaches(FrontendTasks tasks) {

		tasks.taskGroup("AfficherFileAttente")
		
		     .waitsFor("Initialisation")

		     .contains(subTasks -> {
		    	 
		    	 afficherFileAttente(subTasks);

		     });
	}

	private static void afficherFileAttente(FrontendTasks tasks) {

		tasks.task("afficherFileAttente")
		
			 .waitsFor(modified(ModeleFileAttente.class))

			 .waitsFor(viewLoader(FragmentRendezVous.class))

			 .waitsFor(viewLoader(FragmentRendezVousComplet.class))

		     .executes(inputs -> {
		    	 
		    	 VueFileAttente               vueFileAttente          = inputs.get(created(VueFileAttente.class));
		    	 Modified<ModeleFileAttente>  fileAttente             = inputs.get(modified(ModeleFileAttente.class));
		    	 ViewLoader<FragmentRendezVous>    viewLoaderRendezVous    = inputs.get(viewLoader(FragmentRendezVous.class));
		    	 ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet = inputs.get(viewLoader(FragmentRendezVousComplet.class));

		    	 fileAttente.currentValue().afficherSur(vueFileAttente, 
		    			                                viewLoaderRendezVous,
		    			                                viewLoaderRendezVousComplet);
		     });
	}
}