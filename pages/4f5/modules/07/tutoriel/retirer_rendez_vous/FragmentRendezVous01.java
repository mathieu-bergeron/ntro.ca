public class FragmentRendezVous extends ViewFragmentFx {

    // ...
    
    // ajouter
    @FXML
    private Button boutonRetirerRendezVous;


    @Override
    public void initialize() {

        // ajouter
        Ntro.assertNotNull(boutonRetirerRendezVous);

        // ...
        
    }

    // ajouter
    public void memoriserIdRendezVous(String idRendezVous) {
        installerMsgRetirerRendezVous(idRendezVous);
    }

    // ajouter
    protected void installerMsgRetirerRendezVous(String idRendezVous) {

        boutonRetirerRendezVous.setOnAction(evtFx -> {

            Ntro.newMessage(MsgRetirerRendezVous.class)
                .setIdRendezVous(idRendezVous)
                .send();

        });
    }
