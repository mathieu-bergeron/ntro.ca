public class AppPong implements NtroAppFx {
    
    public static void main(String[] args) {
        NtroAppFx.launch(args);
    }
    
    @Override
    public void registerFrontend(FrontendRegistrarFx registrar) {
        registrar.registerFrontend(FrontalPong.class);
    }

    @Override
    public void registerMessages(MessageRegistrar registrar) {
        registrar.registerMessage(MsgAjouterRendezVous.class);

        // ajouter
        registrar.registerMessage(MsgRetirerRendezVous.class);
    }

    @Override
    public void registerModels(ModelRegistrar registrar) {
        registrar.registerModel(ModeleFileAttente.class);
        registrar.registerValue(RendezVous.class);
        registrar.registerValue(RendezVousComplet.class);
        registrar.registerValue(Joueur.class);
    }

    @Override
    public void registerBackend(BackendRegistrar registrar) {
        registrar.registerBackend(DorsalPong.class);
    }

}
