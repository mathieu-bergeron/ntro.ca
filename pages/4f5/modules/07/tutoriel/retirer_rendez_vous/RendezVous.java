public class RendezVous implements ModelValue {
    
    private String idRendezVous;
    private Joueur premierJoueur;

    protected String getIdPartie() {
        return idRendezVous;
    }

    protected Joueur getPremierJoueur() {
        return premierJoueur;
    }

    public RendezVous() {
    }

    public RendezVous(String idRendezVous, Joueur premierJoueur) {
        this.idRendezVous = idRendezVous;
        this.premierJoueur = premierJoueur;
    }

    @Override
    public String toString() {
        return premierJoueur.toString();
    }
    
    public void afficherSur(FragmentRendezVous fragmentRendezVous) {
        fragmentRendezVous.installerMessages(idRendezVous);
        fragmentRendezVous.afficherNomPremierJoueur(premierJoueur.nomCourt());
    }

    public FragmentRendezVous creerFragment(ViewLoader<FragmentRendezVous> viewLoaderRendezVous, 
                                  ViewLoader<FragmentRendezVousComplet> viewLoaderPartieEnCours) {
        
        return viewLoaderRendezVous.createView();
    }

    public RendezVousComplet creerPartieEnCours(Joueur deuxiemeJoueur, String idPartie) {

        return new RendezVousComplet(idPartie, 
                                 premierJoueur, 
                                 deuxiemeJoueur);
    }

    public boolean siIdEst(String idRendezVous) {
        return this.idRendezVous.equals(idRendezVous);
    }

    public boolean siContenuDans(ModeleFileAttente fileAttente) {
        return fileAttente.siContientRendezVous(idRendezVous);
    }

    protected boolean siFragmentDuBonType(FragmentRendezVous fragmentRendezVous) {
        return fragmentRendezVous.getClass().equals(FragmentRendezVous.class);
    }
}
