public class MsgRetirerRendezVous extends Message<MsgRetirerRendezVous> {
    
    private String idRendezVous;

    public String getIdRendezVous() {
        return idRendezVous;
    }

    public MsgRetirerRendezVous setIdRendezVous(String idRendezVous) {

        this.idRendezVous = idRendezVous;
        
        return this;
    }

    public String retirerDe(ModeleFileAttente fileAttente) {
        fileAttente.retirerRendezVous(idRendezVous);
        
        return idRendezVous;
    }
}
