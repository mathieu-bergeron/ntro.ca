public class ModeleFileAttente implements Model, WatchJson, WriteObjectGraph {

    // ...
    
    // ajouter
    private int indiceRendezVousParId(String idRendezVous) {
        int indice = -1;
        
        for(int i = 0; i < rendezVousDansOrdre.size(); i++) {
            if(rendezVousDansOrdre.get(i).siIdEst(idRendezVous)) {
                indice = i;
                break;
            }
        }
        
        return indice;
    }

    // ajouter
    public void retirerRendezVous(String idRendezVous) {
        int indiceRendezVous = indiceRendezVousParId(idRendezVous);
        
        if(indiceRendezVous >= 0) {
            rendezVousDansOrdre.remove(indiceRendezVous);
        }
    }
