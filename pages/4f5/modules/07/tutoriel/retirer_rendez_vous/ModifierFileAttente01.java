public class ModifierFileAttente {
    
    public static void creerTaches(BackendTasks tasks) {

        tasks.taskGroup("ModifierFileAttente")

             .waitsFor(model(ModeleFileAttente.class))
        
             .contains(subTasks -> {
                 
                  
                 ajouterRendezVous(subTasks);

                 // ajouter
                 retirerRendezVous(subTasks);

             });
    }

    // ...

    // ajouter
    private static void retirerRendezVous(BackendTasks subTasks) {
        subTasks.task("retirerRendezVous")

             .waitsFor(model(ModeleFileAttente.class))

             .waitsFor(message(MsgRetirerRendezVous.class))
             
             .executes(inputs -> {

                 MsgRetirerRendezVous msgRetirerRendezVous = inputs.get(message(MsgRetirerRendezVous.class));
                 ModeleFileAttente    fileAttente          = inputs.get(model(ModeleFileAttente.class));

                 msgRetirerRendezVous.retirerDe(fileAttente);
             });

    }
