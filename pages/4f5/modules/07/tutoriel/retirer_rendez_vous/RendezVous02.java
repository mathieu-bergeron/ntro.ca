public class RendezVous implements ModelValue {
    
    // ...
    
    public void afficherSur(FragmentRendezVous fragmentRendezVous) {
        // ajouter
        fragmentRendezVous.memoriserIdRendezVous(idRendezVous);

        fragmentRendezVous.afficherNomPremierJoueur(premierJoueur.nomCourt());
    }
