---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: retirer les rendez-vous

## Ajouter une méthode `RendezVous.siIdEst`

```java
{{% embed src="RendezVous01.java" indent-level="0" %}}
```

## Ajouter une méthode `ModeleFileAttente.retirerRendezVous`

```java
{{% embed src="ModeleFileAttente01.java" indent-level="0" %}}
```

## Ajouter le `MsgRetirerRendezVous` 

<img src="eclipse01.png"/>

```java
{{% embed src="MsgRetirerRendezVous01.java" indent-level="0" %}}
```

```java
{{% embed src="AppPong01.java" indent-level="0" %}}
```

## Modifier `FragmentRendezVous` pour envoyer un `MsgRetirerRendezVous`

```java
{{% embed src="FragmentRendezVous01.java" indent-level="0" %}}
```

## Modifier `RendezVous` pour mémoriser le `idRendezVous`

```java
{{% embed src="RendezVous02.java" indent-level="0" %}}
```

## Ajouter une tâche au dorsal dans le groupe `ModifierFileAttente`

```java
{{% embed src="ModifierFileAttente01.java" indent-level="0" %}}
```



## Vérifier que ça fonctionne

```bash
$ sh gradlew pong
```

{{% animation src="/4f5/modules/07/tutoriel/retirer_rendez_vous/retirer_rendez_vous.mp4" %}}
