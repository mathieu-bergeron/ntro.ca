public class FragmentRendezVous extends ViewFragmentFx {

    @FXML
    private Button boutonJoindrePartie;

    @FXML
    private Button boutonRetirerRendezVous;
    
    @FXML
    private Label labelNomPremierJoueur;
    
    private String idRendezVous;
    
    protected Button getBoutonOuvrirPartie() {
        return boutonJoindrePartie;
    }
    
    @Override
    public void initialize() {

        Ntro.assertNotNull(boutonJoindrePartie);
        Ntro.assertNotNull(boutonRetirerRendezVous);
        Ntro.assertNotNull(labelNomPremierJoueur);

        installerEvtAfficherPartie();
        
    }

    private void installerEvtAfficherPartie() {
        
        EvtAfficherPartie evtNtro = Ntro.newEvent(EvtAfficherPartie.class);

        boutonJoindrePartie.setOnAction(evtFx -> {
            
            evtNtro.trigger();
        });
    }
    
    protected void installerMsgRetirerRendezVous(String idRendezVous) {

        MsgRetirerRendezVous msgRetirerRendezVous = Ntro.newMessage(MsgRetirerRendezVous.class);
        msgRetirerRendezVous.setIdRendezVous(idRendezVous);
        
        boutonRetirerRendezVous.setOnAction(evtFx -> {
            msgRetirerRendezVous.send();
        });
        
    }

    public void afficherNomPremierJoueur(String nomPremierJoueur) {
        labelNomPremierJoueur.setText(nomPremierJoueur);
    }

    public void memoriserIdRendezVous(String idRendezVous) {
        this.idRendezVous = idRendezVous;
        installerMsgRetirerRendezVous(idRendezVous);
    }

    public boolean estDansListe(List<RendezVous> rendezVousDansOrdre) {
        boolean estDansListe = false;
        
        for(RendezVous candidat : rendezVousDansOrdre) {

            if(candidat.siIdEst(this.idRendezVous)) {

                estDansListe = true;
                break;
            }
        }

        return estDansListe;
    }

    public boolean siIdRendezVousEst(String idRendezVous) {
        return idRendezVous.equals(this.idRendezVous);
    }
}
