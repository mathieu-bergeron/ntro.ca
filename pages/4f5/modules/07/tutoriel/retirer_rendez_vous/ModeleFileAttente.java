public class ModeleFileAttente implements Model, WatchJson, WriteObjectGraph {

    private long prochainIdRendezVous = 1;

    private List<RendezVous> rendezVousDansOrdre = new ArrayList<>();

    public ModeleFileAttente() {
    }

    public void ajouterRendezVous(Joueur premierJoueur) {
        String idRendezVous = genererIdRendezVous();
        RendezVous rendezVous = new RendezVous(idRendezVous, premierJoueur);

        rendezVousDansOrdre.add(rendezVous);
    }

    private String genererIdRendezVous() {
        String idRendezVous = String.valueOf(prochainIdRendezVous);
        prochainIdRendezVous++;

        return idRendezVous;
    }
    
    public void afficherSur(VueFileAttente vueFileAttente) {
        
        vueFileAttente.viderListeRendezVous();
        
        for(RendezVous rendezVous : rendezVousDansOrdre) {
            
            vueFileAttente.ajouterRendezVous(rendezVous);
        }
    }
    
    public String toString() {

        StringBuilder builder = new StringBuilder();
        int numeroRendezVous = 1;
        
        for(RendezVous rendezVous : rendezVousDansOrdre) {

            builder.append(numeroRendezVous);
            builder.append(". ");
            builder.append(rendezVous.toString());
            builder.append("\n");

            numeroRendezVous++;
        }

        return builder.toString();
    }

    public RendezVousComplet creerRendezVousComplet(String idRendezVous, Joueur deuxiemeJoueur) {

        int indiceRendezVous = indiceRendezVousParId(idRendezVous);
        RendezVous rendezVous = rendezVousDansOrdre.get(indiceRendezVous);
        
        RendezVousComplet rendezVousComplet = null;

        if(rendezVous != null) {

            rendezVousComplet = rendezVous.creerPartieEnCours(deuxiemeJoueur, idRendezVous);
            
            rendezVousDansOrdre.set(indiceRendezVous, rendezVousComplet);

        }
        
        return rendezVousComplet;
    }
    
    private RendezVous rendezVousParId(String idRendezVous) {
        RendezVous rendezVous = null;
        
        for(RendezVous candidat : rendezVousDansOrdre) {
            if(candidat.siIdEst(idRendezVous)) {
                rendezVous = candidat;
                break;
            }
        }

        return rendezVous;
    }

    private int indiceRendezVousParId(String idRendezVous) {
        int indice = -1;
        
        for(int i = 0; i < rendezVousDansOrdre.size(); i++) {
            if(rendezVousDansOrdre.get(i).siIdEst(idRendezVous)) {
                indice = i;
                break;
            }
        }
        
        return indice;
    }

    public void retirerRendezVous(String idRendezVous) {
        int indiceRendezVous = indiceRendezVousParId(idRendezVous);
        
        if(indiceRendezVous >= 0) {
            rendezVousDansOrdre.remove(indiceRendezVous);
        }
    }

    public List<RendezVousComplet> partiesEnCours() {
        List<RendezVousComplet> partiesEnCours = new ArrayList<>();
        
        for(RendezVous rendezVous : rendezVousDansOrdre) {
            if(rendezVous instanceof RendezVousComplet) {
                partiesEnCours.add((RendezVousComplet) rendezVous);
            }
        }
        
        return partiesEnCours;
    }

    public boolean siContientRendezVous(String idRendezVous) {
        return rendezVousParId(idRendezVous) != null;
    }

    public void ajouterPointSurRendezVous(String idRendezVous, Position position) {
        RendezVous rendezVous = rendezVousParId(idRendezVous);
        
        if(rendezVous instanceof RendezVousComplet) {
            
            RendezVousComplet rendezVousComplet = (RendezVousComplet) rendezVous;
            
            rendezVousComplet.incrementerScore(position);
        }
        
    }

}
