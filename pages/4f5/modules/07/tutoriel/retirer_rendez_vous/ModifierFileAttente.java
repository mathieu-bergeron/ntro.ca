public class ModifierFileAttente {
    
    public static void creerTaches(BackendTasks tasks) {

        tasks.taskGroup("ModifierFileAttente")

             .waitsFor(model(ModeleFileAttente.class))
        
             .contains(subTasks -> {
                 
                  
                 ajouterRendezVous(subTasks);
                 retirerRendezVous(subTasks);

             });
    }

    private static void ajouterRendezVous(BackendTasks subTasks) {
        subTasks.task("ajouterRendezVous")

             .waitsFor(model(ModeleFileAttente.class))

             .waitsFor(message(MsgAjouterRendezVous.class))
             
             .executesAndReturnsValue(inputs -> {

                 MsgAjouterRendezVous msgAjouterRendezVous = inputs.get(message(MsgAjouterRendezVous.class));
                 ModeleFileAttente    fileAttente          = inputs.get(model(ModeleFileAttente.class));

                 return msgAjouterRendezVous.ajouterA(fileAttente);
             });
    }


    private static void retirerRendezVous(BackendTasks subTasks) {
        subTasks.task("retirerRendezVous")

             .waitsFor(model(ModeleFileAttente.class))

             .waitsFor(message(MsgRetirerRendezVous.class))
             
             .executesAndReturnsValue(inputs -> {

                 MsgRetirerRendezVous msgRetirerRendezVous = inputs.get(message(MsgRetirerRendezVous.class));
                 ModeleFileAttente    fileAttente          = inputs.get(model(ModeleFileAttente.class));

                 return msgRetirerRendezVous.retirerDe(fileAttente);
             });

    }
}
