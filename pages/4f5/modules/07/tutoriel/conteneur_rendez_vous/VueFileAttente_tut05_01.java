public class VueFileAttente extends ViewFx {
    
    @FXML
    private Button boutonSInscrire;

    @FXML
    private Button boutonDebuterPartie; // retirer

    @FXML                                
    private Label labelRendezVous;      // retirer

    @Override
    public void initialize() {
        Ntro.assertNotNull(boutonSInscrire);

        // retirer
        Ntro.assertNotNull(boutonDebuterPartie);

        // retirer
        Ntro.assertNotNull(labelRendezVous);          

        // retirer
        installerEvtAfficherPartie();
                                                      
        //...
    }

    // ...

    // retirer la méthode
    private void installerEvtAfficherPartie() {
        boutonDebuterPartie.setOnAction(evtFx -> {
            
            Ntro.newEvent(EvtAfficherPartie.class)
                .trigger();
        });
    }


    public void afficherRendezVousEnTexte(String texteRendezVous) {
        // retirer
        labelRendezVous.setText(texteRendezVous); 
    }
}
