public class VueFileAttente extends ViewFx {
    
    @FXML
    private Button boutonSInscrire;

    @FXML
    private ResizableImage logo;

    @FXML
    private VBox conteneurRendezVous;

    private ViewLoader<FragmentRendezVousPartiel> viewLoaderRendezVous;
    private ViewLoader<FragmentRendezVousComplet> viewLoaderPartieEnCours;

    public ViewLoader<FragmentRendezVousPartiel> getViewLoaderRendezVous() {
        return viewLoaderRendezVous;
    }

    public void setViewLoaderRendezVous(ViewLoader<FragmentRendezVousPartiel> viewLoaderRendezVous) {
        this.viewLoaderRendezVous = viewLoaderRendezVous;
    }

    public ViewLoader<FragmentRendezVousComplet> getViewLoaderPartieEnCours() {
        return viewLoaderPartieEnCours;
    }

    public void setViewLoaderPartieEnCours(ViewLoader<FragmentRendezVousComplet> viewLoaderPartieEnCours) {
        this.viewLoaderPartieEnCours = viewLoaderPartieEnCours;
    }

    @Override
    public void initialize() {
        Ntro.assertNotNull(boutonSInscrire);
        Ntro.assertNotNull(conteneurRendezVous);
        Ntro.assertNotNull(logo);
        
        logo.setImage(new Image("/images/logo.png"));
        //logo.setBackgroundColor(Color.web("#ffff7a"));
        
        initialiserBoutonSInscrire();
    }

    private void initialiserBoutonSInscrire() {

        boutonSInscrire.setOnAction(evtFx -> actionBoutonSInscrire());

    }

    private void actionBoutonSInscrire() {
        
        SessionPong session = Ntro.session();
        
        session.envoyerMsgAjouterRendez();


    }

    public void ajouterRendezVous(RendezVous rendezVous) {
        FragmentRendezVousPartiel fragment = rendezVous.creerFragment(viewLoaderRendezVous, viewLoaderPartieEnCours);
        rendezVous.afficherSur(fragment);
        conteneurRendezVous.getChildren().add(fragment.rootNode());
    }
    

    public void viderListeRendezVous() {
        conteneurRendezVous.getChildren().clear();
    }

    public void afficherRendezVousEnTexte(String string) {

        
    }

}
