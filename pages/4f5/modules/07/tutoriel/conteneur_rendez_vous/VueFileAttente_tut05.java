public class VueFileAttente extends ViewFx {
    
    @FXML
    private Button boutonSInscrire;

    @FXML
    private Button boutonDebuterPartie;

    @FXML
    private Label labelRendezVous;

    @Override
    public void initialize() {
        Ntro.assertNotNull(boutonSInscrire);
        Ntro.assertNotNull(boutonDebuterPartie);
        Ntro.assertNotNull(labelRendezVous);
        
        installerEvtAfficherPartie();
        
        initialiserBoutonSInscrire();
    }

    private void initialiserBoutonSInscrire() {
        
        SessionPong session = Ntro.session();

        boutonSInscrire.setOnAction(evtFx -> {

            session.envoyerMsgAjouterRendezVous();

        });
    }

    private void installerEvtAfficherPartie() {
        boutonDebuterPartie.setOnAction(evtFx -> {
            
            Ntro.newEvent(EvtAfficherPartie.class)
                .trigger();
        });
    }

    public void afficherRendezVousEnTexte(String texteRendezVous) {
        labelRendezVous.setText(texteRendezVous);
    }
}
