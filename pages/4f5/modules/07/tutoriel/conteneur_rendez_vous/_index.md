---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: ajouter le `conteneurRendezVous`

## Retirer les anciens éléments graphiques 

1. Dans `file_attente.fxml`, retirer le `boutonDebuterPartie`

    ```xml
    {{% embed src="./file_attente_tut05_02.fxml" indent-level="1" %}}
    ```

1. Dans `file_attente.fxml`, retirer le `labelRendezVous`

    ```xml
    {{% embed src="./file_attente_tut05_01.fxml" indent-level="1" %}}
    ```

## Modifier le `.css`

1. Retirer la taille forcée pour le `.defilement` (`dev.css` et `prod.css`)

    ```
    {{% embed src="./prod02.css" indent-level="1" %}}
    ```

## Ajouter le `conteneurRendezVous`

1. Ajouter plutôt un `conteneurRendezVous`

    ```xml
    {{% embed src="./file_attente_tut07_01.fxml" indent-level="1" %}}
    ```

## Modifier la classe `VueFileAttente` 

1. Dans `VueFileAttente`, retirer les attributs `boutonDebuterPartie` et `labelRendezVous`
    * retirer aussi le code qui utilise ces attributs

    ```java
    {{% embed src="./VueFileAttente_tut05_01.java" indent-level="1" %}}
    ```

1. Ajouter l'attribut `conteneurRendezVous`

    ```java
    {{% embed src="./VueFileAttente_tut07_01.java" indent-level="1" %}}
    ```
## Tester que ça fonctionne

1. Exécuter `AppPong` et tester l'affichage (avec `dev.css`)

    ```bash
    $ sh gradlew pong
    ```

    <center>
        <img style="figure" src="resultat.png"/>
    </center>

    * NOTES:
        * le `conteneurRendezVous` est vide (ligne noire sous le bouton s'inscrire)
        * les rectangles roses sont les espaces (pas élastiques pour l'instant)



