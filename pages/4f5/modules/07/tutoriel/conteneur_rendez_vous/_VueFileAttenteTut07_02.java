public class VueFileAttente extends ViewFx {

    @Override
    public void initialize() {


        // ...

        // À retirer
        installerEvtAfficherPartie();
    }


    // À retirer
    private void installerEvtAfficherPartie() {
        
        EvtAfficherPartie evtNtro = Ntro.newEvent(EvtAfficherPartie.class);

        boutonJoindrePartie.setOnAction(evtFx -> {
            
            evtNtro.trigger();
        });
    }

    // À retirer
    public void afficherRendezVousEnTexte(String message) {
        labelRendezVous.setText(message);
    }


    // ...
}
