---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: ajouter une barre de défilement

1. Ouvrir `file_attente.fxml` et modifier le FXML comme suit:

    ```xml
    {{% embed src="./file_attente.fxml" indent-level="1" %}}
    ```

1. Dans `dev.css` et `proc.css`, ajouter le CSS suivant

    ```
    {{% embed src="./prod01.css" indent-level="1" %}}
    ```

    * NOTE: je peux télécharger {{% download "prod01.css" "cet extrait" %}}


1. J'exécute le client et je vérifie que le FlexBox fonctionne

    ```bash
    $ sh gradlew pong:local
    ```

    <video width="100%" src="defilement.mp4" type="video/mp4" loop nocontrols autoplay>

    
