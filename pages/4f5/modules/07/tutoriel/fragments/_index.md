---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: créer `FragmentRendezVous` et `FragmentRendezVousComplet`

## Créer les fichiers `.fxml`

1. Dans le répertoires `pong/src/main/resources/fragments`, créer les fichiers
    * `rendez_vous.fxml`
    * `rendez_vous_complet.fxml`

1. S'assurer d'avoir l'arborescence suivante:

    <center>
        <img src="eclipse01.png">
    </center>

1. Ouvrir `rendez_vous.fxml` ajouter le code suivant:

    ```xml
    {{% embed src="./rendez_vous.fxml" indent-level="1" %}}
    ```

1. Ouvrir `rendez_vous_complet.fxml` et ajouter le code suivant:

    ```xml
    {{% embed src="./partie_en_cours.fxml" indent-level="1" %}}
    ```

    * NOTES:
        * il n'y a pas de notion d'héritage pour le `.fxml`
        * on doit alors recopier des portions de `rendez_vous` 


## Ajouter le CSS

1. Ajouter le CSS suivant à `dev.css` et `prod.css`

    ```
    {{% embed src="./prod01.css" indent-level="1" %}}
    ```

## Ajouter les traductions

1. Dans `fr.properties`, ajouter

    ```ini
    joindrePartie=Joindre partie
    retirerRendezVous=✗
    ```

1. Dans `en.properties`, ajouter

    ```ini
    joindrePartie=Join Game
    retirerRendezVous=✗
    ```

## Créer les classes

1. En VSCode, créer le paquet `pong.frontal.fragments`

1. Dans `fragments`, créer les classes:
    * `FragmentRendezVous`
    * `FragmentRendezVousComplet`

1. S'assurer d'avoir l'arborescence suivante:

    <center>
        <img src="eclipse02.png">
    </center>

1. Ouvrir `FragmentRendezVous` et ajouter ce code

    ```java
    {{% embed src="./FragmentRendezVous01.java" indent-level="1" %}}
    ```

1. Ouvrir `FragmentRendezVousComplet` et ajouter ce code

    ```java
    {{% embed src="./FragmentRendezVousComplet01.java" indent-level="1" %}}
    ```

    * NOTES:
        * ici on peut utiliser l'héritage pour réutiliser `FragmentRendezVous`



## Déclarer les fragments

1. Dans `FrontalPong`, déclarer les fragments comme des vues

    ```java
    {{% embed src="./FrontalPong.java" indent-level="1" %}}
    ```
