public class FrontalPong implements FrontendFx {

    // ...
    

    @Override
    public void registerViews(ViewRegistrarFx registrar) {

        // ...
        
        registrar.registerFragment(FragmentRendezVous.class, "/fragments/rendez_vous.fxml");
        registrar.registerFragment(FragmentRendezVousComplet.class, "/fragments/rendez_vous_complet.fxml");
    }
