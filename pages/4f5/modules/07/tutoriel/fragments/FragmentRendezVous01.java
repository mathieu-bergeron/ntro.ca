public class FragmentRendezVous extends ViewFx {

    @FXML
    private Button boutonDebuterPartie;

    @FXML
    private Button boutonRetirerRendezVous;
    
    @FXML
    private Label labelNomPremierJoueur;
    
    protected Button getBoutonOuvrirPartie() {
        return boutonDebuterPartie;
    }

    @Override
    public void initialize() {
        Ntro.assertNotNull(boutonDebuterPartie);
        Ntro.assertNotNull(boutonRetirerRendezVous);
        Ntro.assertNotNull(labelNomPremierJoueur);

    }

}
