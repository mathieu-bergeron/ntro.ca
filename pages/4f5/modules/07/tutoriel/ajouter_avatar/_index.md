---
title: "Tutoriel: ajouter un Avatar"
weight: 1
bookHidden: true
---


{{% pageTitle %}}

## Ajouter un avatar

1. Dans `file_attente.fxml`, ajouter un contrôle `ResizableImage`

    ```xml
    {{% embed src="./file_attente01.fxml" indent-level="1" %}}
    ```

    * NOTE: on peut aussi utiliser `ResizableImage` si on ne veut pas le «look» avatar

1. Dans `VueFileAttente.java`, initialiser le contrôle:

    ```java
    {{% embed src="./VueFileAttente01.java" indent-level="1" %}}
    ```

1. Ajouter du CSS pour ce contrôle:

    ```
    {{% embed src="./prod01.css" indent-level="1" %}}
    ```

1. Le résultat devrait être comme suit:

{{% animation src="/4f5/modules/07/tutoriel/ajouter_avatar/resizable_image.mp4" %}}

1. Version avec `ResizableAvatar` et `tux.png`:

{{% animation src="/4f5/modules/07/tutoriel/ajouter_avatar/resizable_avatar.mp4" %}}

