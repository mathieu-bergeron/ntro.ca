public class VueFileAttente extends ViewFx {
    
    @FXML
    private Button boutonAjouterRendezVous;

    @FXML
    private ResizableAvatar logo;

    @FXML
    private VBox conteneurRendezVous;

    private ViewLoader<FragmentRendezVous> viewLoaderRendezVous;
    private ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet;

    public ViewLoader<FragmentRendezVous> getViewLoaderRendezVous() {
        return viewLoaderRendezVous;
    }

    public void setViewLoaderRendezVous(ViewLoader<FragmentRendezVous> viewLoaderRendezVous) {
        this.viewLoaderRendezVous = viewLoaderRendezVous;
    }

    public ViewLoader<FragmentRendezVousComplet> getViewLoaderRendezVousComplet() {
        return viewLoaderRendezVousComplet;
    }

    public void setViewLoaderRendezVousComplet(ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet) {
        this.viewLoaderRendezVousComplet = viewLoaderRendezVousComplet;
    }

    @Override
    public void initialize() {
        Ntro.assertNotNull("boutonAjouterRendezVous", boutonAjouterRendezVous);
        Ntro.assertNotNull("conteneurRendezVous", conteneurRendezVous);
        Ntro.assertNotNull(logo);
        
        logo.setImage(new Image("/images/avatar.png"));
        
        installerMsgAjouterRendezVous();
    }

    private void installerMsgAjouterRendezVous() {

        MsgAjouterRendezVous msgAjouterRendezVous = Ntro.newMessage(MsgAjouterRendezVous.class);

        boutonAjouterRendezVous.setOnAction(evtFx -> {

            msgAjouterRendezVous.setPremierJoueur(MaquetteJoueurs.usagerCourant());
            msgAjouterRendezVous.send();

            if(MaquetteJoueurs.modeTest) {
                MaquetteJoueurs.prochainJoueur();
            }
        });
    }

    public void ajouterRendezVous(RendezVous rendezVous) {
        FragmentRendezVous fragment = rendezVous.creerFragment(viewLoaderRendezVous, viewLoaderRendezVousComplet);
        rendezVous.afficherSur(fragment);
        conteneurRendezVous.getChildren().add(fragment.rootNode());
    }
    

    public void viderListeRendezVous() {
        conteneurRendezVous.getChildren().clear();
    }

    public void afficherRendezVousEnTexte(String string) {
        // TODO Auto-generated method stub
        
    }



}
