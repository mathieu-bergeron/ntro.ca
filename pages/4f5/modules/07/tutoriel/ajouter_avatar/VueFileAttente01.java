public class VueFileAttente extends ViewFx {

    // ...

    @FXML
    private ResizableImage logo;  // ajouter (autre option ResizableAvatar
    
    @Override
    public void initialize() {
        // ...
        
        // ajouter
        Ntro.assertNotNull(logo);
        logo.setImage(new Image("/images/avatar.png"));

        // si ResizableAvatar
        // logo.setBackgroundColor(Color.web("#ffff7a"));

        // ...
    }
