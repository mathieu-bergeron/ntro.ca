---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: créer et utiliser le `CanvasPartie`

## Créer la classe `CanvasPartie`

1. Dans le paquet `pong.frontal.controles`, créer la classe `CanvasPartie`

1. En VSCode, s'assurer d'avoir l'arborescence suivante:

    <center>
        <img src="eclipse01.png">
    </center>

1. Ajuster la signature de `CanvasPartie` 

    ```java
    {{% embed src="./CanvasPartie01.java" indent-level="1" %}}
    ```

1. Avec {{% key "Ctrl+1" %}}, ajouter le `import` et les méthodes obligatoires

1. Charger une image dès l'initialisation

    ```java
    {{% embed src="./CanvasPartie02.java" indent-level="1" %}}
    ```

1. Ajouter un dessin très simple

    ```java
    {{% embed src="./CanvasPartie03.java" indent-level="1" %}}
    ```
 
## Utiliser le `CanvasPartie` dans le `.fxml`

1. Modifier `partie.fxml` comme suit:

    ```xml
    {{% embed src="./partie.fxml" indent-level="1" %}}
    ```

## Tester que ça fonctionne

1. Exécuter l'application et vérifier que le dessin s'affiche

    ```bash
    $ sh gradlew pong:local
    ```

    {{% animation src="/4f5/modules/07/tutoriel/canvas_partie/canvas_partie.mp4" %}}

    * NOTES:
        * par défaut, notre `CanvasPartie` préserve le rapport hauteur/largeur du dessin
        * par défaut, l'alignement est centré
