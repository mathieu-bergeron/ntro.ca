---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: créer et utiliser le `FlexBoxRendezVous`

## Créer la classe `FlexBoxRendezVous`

1. Dans le paquet `pong.frontal.controles`, créer la classe `FlexBoxRendezVous`

1. En VSCode, s'assurer d'avoir l'arborescence suivante:

    <center>
        <img src="eclipse01.png">
    </center>

1. Ouvrir `FlexBoxRendezVous` et ajuster la signature:

    ```java
    {{% embed src="./FlexBoxRendezVous01.java" indent-level="1" %}}
    ```

1. Avec {{% key "Ctrl+1" %}}, ajouter le `import` et les méthodes obligatoires

    ```java
    {{% embed src="./FlexBoxRendezVous02.java" indent-level="1" %}}
    ```
 
## Utiliser le `FlexBoxRendezVous` dans le `.fxml`

1. Ouvrir `rendez_vous.fxml` et utiliser la balise `FlexBoxRendezVous`

    ```xml
    {{% embed src="./rendez_vous.fxml" indent-level="1" %}}
    ```

1. Ouvrir `rendez_vous_complet.fxml` et utiliser la balise `FlexBoxRendezVous`

    ```xml
    {{% embed src="./partie_en_cours.fxml" indent-level="1" %}}
    ```

## Tester que ça fonctionne


1. Exécuter `AppPong` et vérifier que le FlexBox fonctionne

    ```bash
    $ sh gradlew pong
    ```

    {{% animation src="/4f5/modules/07/tutoriel/flexbox/flexbox.mp4" %}}

    * NOTE: désolé pour le clignotement, l'implantation du `SimpleFlexBox` est loin d'être parfaite!
