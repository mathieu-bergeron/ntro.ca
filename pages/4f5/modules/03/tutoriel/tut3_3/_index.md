---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 3.3: installer le CSS

## Préparer les fichiers `.fxml`

1. Ajouter des `id` et des `styleClass` à mon FXML

1. Dans `racine.fxml`, ajouter les `id` et `styleClass`

    ```xml
    {{% embed src="./racine.fxml" indent-level="1" %}}
    ```

1. Dans `file_attente.fxml`, ajouter les `id` et `styleClass`

    ```xml
    {{% embed src="./file_attente.fxml" indent-level="1" %}}
    ```

## Créer les fichiers `.css`

1. Créer deux fichier `.css`
    * `dev.css` pour afficher les conteneurs et faciliter les tests
    * `prod.css` pour l'apparence finale de ma Vue

1. Dans `pong/src/main/resources/style` ajouter
    * `dev.css`
    * `prod.css`

1. S'assurer d'avoir l'arborescence suivante dans mon projet:

<center>
<img src="eclipse01.png" />
</center>


## Déclarer le CSS de l'application

* L'application a un seul fichier CSS

* Il faut le déclarer dans le Frontal

* Ouvrir `FrontalPong.java` et ajouter

    ```java
    {{% embed src="./FrontalPong01.java" indent-level="1" %}}
    ```

## Ajouter des propriétés CSS

* Dans `dev.css`, ajouter graduellement des propriétés pour voir l'effet

* Ajouter:

    ```
    {{% embed src="./dev01.css" indent-level="1" %}}
    ```

    <center>
        <img width="50%" src="css01.png">
    </center>

    * On voit que le conteneur `VueRacine` prend toute la fenêtre


* Ajouter:

    ```
    {{% embed src="./dev02.css" indent-level="1" %}}
    ```

    <center>
        <img width="50%" src="css02.png">
    </center>

    * On voit que `VueFileAttente` qui prend tout l'espace à l'intérieur de `VueRacine`

* Expérimenter avec différents alignements à l'intérieur des conteneurs

    ```
    {{% embed src="./dev03.css" indent-level="1" %}}
    ```

    <table>
    <tr>
    <td>
        <code>top-right</code>
    </td>
    <td>
        <center>
            <img width="50%" src="css03_top_right.png">
        </center>
    </td>
    </tr>

    <tr>
    <td>
        <code>bottom-left</code>
    </td>
    <td>
        <center>
            <img width="50%" src="css03_bottom_left.png">
        </center>
    </td>
    </tr>

    <tr>
    <td>
        <code>center</code>
    </td>
    <td>
        <center>
            <img width="50%" src="css03_center.png">
        </center>
    </td>
    </tr>
    </table>


    * NOTES: 
        * il existe évidemment d'autres options. 
        * on va utiliser `center` ici.


* Ajouter du style pour les états d'un `gros-bouton`

    ```
    {{% embed src="./dev04.css" indent-level="1" %}}
    ```

    <center>
    <video width="50%" src="gros-bouton.mp4" type="video/mp4" loop nocontrols autoplay/>
    </center>

    * NOTE: le bouton ne fait toujours rien, ce n'est que du style ;-)


* Choisir ou créer un style de bouton, p.ex.

    ```
    {{% embed src="./dev05.css" indent-level="1" %}}
    ```

    ```css
    ```
    * adatpé de:
        * http://fxexperience.com/2011/12/styling-fx-buttons-with-css/ 

    <center>
        <img width="50%" src="css05.png"/>
    </center>


* Finalement, le `prod.css` est similaire, sauf que:
    * on ne montre pas les conteneurs
    * on ajoute une couleur de fond

    ```java
    {{% embed src="./FrontalPong02.java" indent-level="1" %}}
    ```

    <center>
        <img width="50%" src="css06.png"/>
    </center>









