public class PremierAffichage {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("PremierAffichage")
        
             .waitsFor("CreerVues")                    
        
             .contains(subTasks -> {

                installerVueRacine(subTasks);

                afficherFenetre(subTasks);

                // ajouter
                choisirPremiereVue(subTasks);

             });
    }


    // ajouter
    private static void choisirPremiereVue(FrontendTasks subTasks) {

        subTasks.task("choisirPremiereVue")
        
              .waitsFor(created(VueRacine.class))
        
              .waitsFor(created(VueFileAttente.class))

              .executes(inputs -> {
                  
                  VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
                  VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));
                  
                  vueRacine.afficherSousVue(vueFileAttente);
                  
              });
    }
