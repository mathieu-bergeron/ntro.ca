public class CreerVues {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("CreerVues")
        
             .waitsFor("ViewLoaders")
        
             .contains(subTasks -> {
                 
                creerVueRacine(subTasks);

                // ajouter
                creerVueFileAttente(subTasks);

             });
    }

    // ajouter
    private static void creerVueFileAttente(FrontendTasks subTasks) {

        subTasks.task(create(VueFileAttente.class))

             .waitsFor(viewLoader(VueFileAttente.class))

             .executesAndReturnsValue(inputs -> {
                 
                 ViewLoader<VueFileAttente> viewLoader = inputs.get(viewLoader(VueFileAttente.class));
                 
                 VueFileAttente vueFileAttente = viewLoader.createView();

                 return vueFileAttente;
             });
    }
