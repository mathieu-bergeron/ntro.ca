private static void creerVueFileAttente(FrontendTasks tasks) {

    tasks.task(create(VueFileAttente.class))

         .waitsFor(viewLoader(VueFileAttente.class))

         .executesAndReturnsValue(inputs -> {
             
             ViewLoader<VueFileAttente> viewLoader = inputs.get(viewLoader(VueFileAttente.class));
             
             VueFileAttente vueFileAttente = viewLoader.createView();

             return vueFileAttente;
         });
}

private static void installerVueFileAttente(FrontendTasks tasks) {

    tasks.task("installerVueFileAttente")

          .waitsFor(created(VueRacine.class))
    
          .waitsFor(created(VueFileAttente.class))

          .executes(inputs -> {
              
              VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
              VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));
              
              vueRacine.afficherSousVue(vueFileAttente);
              
          });
}
