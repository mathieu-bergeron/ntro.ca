public class PremierAffichage {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("PremierAffichage")
        
             .waitsFor("CreerVues")                    
        
             .contains(subTasks -> {

                installerVueRacine(subTasks);

                afficherFenetre(subTasks);

                choisirPremiereVue(subTasks);

             });
    }


    private static void choisirPremiereVue(FrontendTasks subTasks) {

        subTasks.task("choisirPremiereVue")
        
              //.waitsFor("installerVueRacine")

              .waitsFor(created(VueRacine.class))
        
              .waitsFor(created(VueFileAttente.class))

              .executes(inputs -> {
                  
                  VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
                  VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));
                  
                  vueRacine.afficherSousVue(vueFileAttente);
                  
              });
    }

    private static void installerVueRacine(FrontendTasks subTasks) {

        subTasks.task("installerVueRacine")
        
              .waitsFor(window())
              
              .waitsFor(created(VueRacine.class))
              
              .executes(inputs -> {
                  
                  VueRacine vueRacine = inputs.get(created(VueRacine.class));
                  Window    window    = inputs.get(window());

                  window.installRootView(vueRacine);
              });
    }


    private static void afficherFenetre(FrontendTasks subTasks) {
        subTasks.task("afficherFenetre")

             .waitsFor(window())

             .executes(inputs -> {

                 Window window = inputs.get(window());

                 window.resize(1200, 800);

                 window.show();

             });
    }

}
