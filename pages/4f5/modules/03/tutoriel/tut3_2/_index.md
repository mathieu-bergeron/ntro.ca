---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 3.2: installer la `VueFileAttente`

## Créer la classe `VueFileAttente`

1. Dans le paquet `vues`, créer la classe `VueFileAttente`

1. Ajuster la signature de la classe, qui doit hériter de `ViewFx`

    ```java
    {{% embed src="./VueFileAttente.java" first-line="1" last-line="1" indent-level="1" %}}
    ```

1. Avec {{% key "Ctrl+1" %}}, ajouter les `import` et la méthode obligatoire

    ```java
    {{% embed src="./VueFileAttente.java" indent-level="1" %}}
    ```

    * NOTES: 
        * la méthode `initialize` est requise
        * on va s'en servir au module 4

1. S'assurer d'avoir l'arborescence suivante 

<center>
<img src="eclipse01.png" />
</center>

## Créer le fichier `.fxml`


1. Dans le répertoire `pong/src/main/resources/vues`, créer le fichier `file_attente.fxml`


1. Pour commencer, ajouter le code suivant dans ce fichier:

    ```xml
    {{% embed src="./file_attente.fxml" indent-level="1" %}}
    ```

    * NOTES: 
        * l'attribut `fx:controller` déclare notre classe `VueFileAttente`
        * on a une image et un bouton dans un VBox:
            * l'image devrait être au dessus et le bouton en bas

1. Télécharger {{% download "logo.png" "logo.png" %}} et je copie le fichier dans `pong/src/main/resources/images`

1. S'assurer d'avoir l'arborescence suivante

<center>
<img src="eclipse02.png" />
</center>

## Déclarer la Vue dans le Frontal

1. Ouvrir `FrontalPong.java` et déclarer la `VueFileAttente`

    ```java
    {{% embed src="./FrontalPong.java" indent-level="1" %}}
    ```

## Créer la méthode `VueRacine.afficherSousVue`

1. Dans la `VueRacine` on a besoin d'une métode pour afficher une page (une sous-vue)

1. Ouvrir `VueRacine.java` et ajouter cette méthode

    ```java
    {{% embed src="./VueRacine.java" indent-level="1" %}}
    ```

    * NOTE:
        * on fait `clear()` pour retirer une page serait déjà affichée
        * le résultat est qu'on affiche une page à la fois
        * (à adapter selon vos besoins pour votre jeu)

## Créer les tâches pour charger la Vue

1. Dans `CreerVues`, ajouter la tâche suivante

    ```java
    {{% embed src="./CreerVues01.java" indent-level="1" %}}
    ```
    * Utiliser {{% key "Ctrl+1" %}} pour ajouter les `import` requis

1. Dans `PremierAffichage`, ajouter les tâches suivantes

    ```java
    {{% embed src="./PremierAffichage01.java" indent-level="1" %}}
    ```

## Vérifier que la Vue s'affiche et vérifier le graphe des tâches

1. Exécuter `pong`

    ```bash
    $ sh gradlew pong
    ```

1. Vérifier que la VueFileAttente s'affiche

    <center>
        <img width="50%" src="file_attente.png">
    </center>

1. Fermer la fenêtre Pong (pour générer le graphe des tâches)

1. Vérifier le graphe des tâches `pong/_storage/graphs/frontend.png`

<center>
    <img width="90%" src="frontend.png">
</center>
