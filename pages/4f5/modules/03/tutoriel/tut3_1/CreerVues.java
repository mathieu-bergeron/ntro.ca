public class CreerVues {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("CreerVues")
        
             .waitsFor("ViewLoaders")
        
             .contains(subTasks -> {
                 
                creerVueRacine(subTasks);
                creerVueFileAttente(subTasks);

             });
    }

    private static void creerVueRacine(FrontendTasks subTasks) {

        subTasks.task(create(VueRacine.class))

                .waitsFor(viewLoader(VueRacine.class))
             
                .executesAndReturnsValue(inputs -> {

                    ViewLoader<VueRacine> viewLoader = inputs.get(viewLoader(VueRacine.class));
                 
                    VueRacine vueRacine = viewLoader.createView();
                 
                    return vueRacine;
             });
    }

    private static void creerVueFileAttente(FrontendTasks subTasks) {

        subTasks.task(create(VueFileAttente.class))

                .waitsFor(viewLoader(VueFileAttente.class))

                .executesAndReturnsValue(inputs -> {
                 
                    ViewLoader<VueFileAttente> viewLoader = inputs.get(viewLoader(VueFileAttente.class));
                 
                    VueFileAttente vueFileAttente = viewLoader.createView();

                    return vueFileAttente;
             });
    }

}
