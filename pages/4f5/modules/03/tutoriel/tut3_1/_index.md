---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 3.1: créer et installer la `VueRacine`

## Créer la classe `VueRacine`

1. En VSCode, créer le paquet `pong.frontal.vues`

1. Dans le paquet `vues`, créer la classe `VueRacine`

1. Ajuster la signature de la classe, qui doit hériter de `ViewFx`:

    ```java
    {{% embed src="./VueRacine.java" first-line="1" last-line="1" indent-level="1" %}}
    ```

1. Avec {{% key "Ctrl+1" %}}, ajouter les `import` et la méthode obligatoire:

    ```java
    {{% embed src="./VueRacine.java" indent-level="1" %}}
    ```

    * NOTES: 
        * la méthode `initialize` est requise
        * on va s'en servir au module 4


1. S'assurer d'avoir l'arborescence suivante:

<center>
<img src="eclipse01.png" />
</center>


## Créer le fichier `.fxml`

1. Dans le répertoire `pong/src/main/resources/vues`, créer le fichier `racine.fxml`

1. Pour commencer, ajouter le code suivant dans ce fichier:

    ```xml
    {{% embed src="./racine.fxml" indent-level="1" %}}
    ```

    * NOTES: 
        * l'attribut `fx:controller` déclare notre classe `VueRacine`
        * JavaFx utilise le terme `controller`, mais en `Ntro` on dirait Vue

1. S'assurer d'avoir l'arborescence suivante:

<center>
<img src="eclipse02.png" />
</center>


## Déclarer la Vue dans le Frontal

1. Ouvrir `FrontalPong.java` et déclarer la `VueRacine`

    ```java
    {{% embed src="./FrontalPong01.java" indent-level="1" %}}
    ```
## Créer les tâches pour charger la Vue

1. Dans le paquet `taches`, créer la classe `CreerVues`

1. S'assurer d'avoir l'arborescence suivante:


    <img src="eclipse03.png"/>
    

1. Dans le `FrontalPong`, ajouter un appel à `CreerVues.creerTaches(task)`

    ```java
    {{% embed src="./FrontalPong02.java" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour créer la méthode `CreerVues.creerTaches(tasks)`

1. Dans `CreerVues`, ajouter le `import static` pour les tâches:

<br>
<center>

<span style="background-color:orange; padding:10px;border:2px dashed black;"> `import static ca.ntro.app.tasks.frontend.FrontendTasks.*;` </span>

</center>
<br>

1. Dans `CreerVues`, créer les tâches suivantes:

    ```java
    {{% embed src="./CreerVues01.java" indent-level="1" %}}
    ```

1. Dans la classe `PremierAffichage`, ajouter la dépendance au groupe de tâche `"CreerVues"`

    ```java
    {{% embed src="./PremierAffichage00.java" indent-level="1" %}}
    ```

1. Dans la classe `PremierAffichage`, ajouter la tâche suivante

    ```java
    {{% embed src="./PremierAffichage01.java" indent-level="1" %}}
    ```

## Vérifier que la Vue s'affiche et vérifier le graphe des tâches

1. Exécuter `pong`

    ```bash
    $ sh gradlew pong
    ```

1. Vérifier que la VueRacine s'affiche

    <center>
        <img width="50%" src="racine.png">
    </center>

1. Fermer la fenêtre Pong (pour générer le graphe des tâches)

1. Vérifier le graphe des tâches `pong/_storage/graphs/frontend.png`

<center>
    <img width="90%" src="frontend.png">
</center>
