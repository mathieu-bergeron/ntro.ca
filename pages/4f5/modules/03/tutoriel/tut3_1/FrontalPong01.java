public class FrontalPong implements FrontendFx {

    // [...]
    
    @Override
    public void registerViews(ViewRegistrarFx registrar) {

        registrar.registerView(VueRacine.class, "/vues/racine.fxml");

    }

    // [...]
}
