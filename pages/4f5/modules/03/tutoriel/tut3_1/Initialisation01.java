public class Initialisation {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("Initialisation")
        
             .contains(subTasks -> {
                 
                // ajouter
                creerVueRacine(subTasks);

                // ajouter
                installerVueRacine(subTasks);

                afficherFenetre(subTasks);
             });
    }

    private static void creerVueRacine(FrontendTasks tasks) {

        tasks.task(create(VueRacine.class))

             .waitsFor(viewLoader(VueRacine.class))
             
             .executesAndReturnsValue(inputs -> {

                 ViewLoader<VueRacine> viewLoader = inputs.get(viewLoader(VueRacine.class));
                 
                 VueRacine vueRacine = viewLoader.createView();
                 
                 return vueRacine;
             });
    }

    private static void installerVueRacine(FrontendTasks tasks) {

        tasks.task("installerVueRacine")
        
              .waitsFor(window())
              
              .waitsFor(created(VueRacine.class))
              
              .executes(inputs -> {
                  
                  VueRacine vueRacine = inputs.get(created(VueRacine.class));
                  Window    window    = inputs.get(window());

                  window.installRootView(vueRacine);
              });
    }

    // ...
}

