public class PremierAffichage {
    
    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("PremierAffichage")
        
             .waitsFor("CreerVues")                    
        
             .contains(subTasks -> {

                // ajouter
                installerVueRacine(subTasks);

                afficherFenetre(subTasks);

             });
    }

    // ajouter
    private static void installerVueRacine(FrontendTasks subTasks) {

        subTasks.task("installerVueRacine")
        
                .waitsFor(window())
              
                .waitsFor(created(VueRacine.class))
              
                .executes(inputs -> {
                  
                    VueRacine vueRacine = inputs.get(created(VueRacine.class));
                    Window    window    = inputs.get(window());

                    window.installRootView(vueRacine);
              });
    }
