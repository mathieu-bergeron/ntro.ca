public class FrontalPong implements FrontendFx {

    // ...


    @Override
    public void registerViews(ViewRegistrarFx registrar) {


        // ...
        registrar.registerDefaultLocale(Ntro.buildLocale("fr"));
        registrar.registerTranslations(Ntro.buildLocale("fr"), "/traductions/fr.properties");
        registrar.registerTranslations(Ntro.buildLocale("en"), "/traductions/en.properties");

    }

