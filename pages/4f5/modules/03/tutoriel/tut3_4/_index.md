---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 3.4: ajouter les traductions

## Créer les fichiers `.properties`

1. Dans `pong/src/main/resources/traductions`, ajouter
    * `fr.properties`
    * `en.properties`

1. S'assurer d'avoir l'arborescence suivante 

<center>
<img src="eclipse01.png" />
</center>

1. Dans chaque fichier, donner une valeur à `debuterPartie`

1. Dans `fr.properties`, saisir

    ```ini
        debuterPartie=Débuter partie
    ```

1. Dans `en.properties`, saisir

    ```ini
        debuterPartie=Start Game
    ```


## Modifier les fichiers `.fxml`

1. On va maintenant utiliser la valeur `debuterPartie`

1. Modifier `file_attente.fxml` comme suit:

    ```xml
    {{% embed src="./file_attente.fxml" indent-level="1" %}}
    ```
    * c-à-d le texte du bouton est maintenant `%debuterPartie`

## Déclarer les traductions dans le Frontal

* Ouvrir `FrontalPong.java` et ajouter

    ```java
    {{% embed src="./FrontalPong01.java" indent-level="1" %}}
    ```


## Vérifier que ça fonctionne


1. Exécuter `pong` en français

    ```bash
    $ sh gradlew pongFr
    ```

    <center>
        <img width="50%" src="clientFr.png" />
    </center>

1. Exécuter `pong` en anglais

    ```bash
    $ sh gradlew pongEn
    ```
    <center>
        <img width="50%" src="clientEn.png" />
    </center>
