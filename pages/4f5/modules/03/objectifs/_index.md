---
title: ""
weight: 1
bookHidden: true
---


# Objectifs 3: créer la Vue de ma page

<!--
<center>
<video width="50%" src="presentation.mp4" type="video/mp4" controls playsinline>
</center>
-->

{{<excerpt class="note">}}

**IMPORTANT**

* importer la `VueRacine` dans mon projet

* par contre, ne **pas** importer la `VueFileAttente`
    * il faut plutôt créer une vue pour **ma page**

* je peux implanter une <strong>version simplifiée</strong> de ma Vue
    * mais **je dois** avoir 
        * 2-3 éléments graphiques (bouton, image, texte)
        * du CSS 
        * du texte traduit français/anglais

{{</excerpt>}}

1. J'effectue le {{% link "../tutoriel/" "tutoriel" %}} pour comprendre comment créer une vue

1. Je commence à créer ma Vue
    * voici, <strong>par exemple</strong>, le genre de résultat qu'on cherche pour l'instant

        <img src="exemple_fr.png" style="max-height:400px;"/>

        <img src="exemple_en.png" style="max-height:400px;"/>

    * **NOTES**: 
        * j'utilise du texte pertinent à mon projet
        * j'ai bien sûr le droit que ma page soit plus jolie ;-)

1. Je crée la classe Java pour ma Vue
    * je copie la classe `VueRacine` à partir du tutoriel
    * je peux aussi copier et adapter d'autres classes

1. Je crée les fichiers `.css`, `.fxml` et `.properties` pour mon projet
    * je copie `racine.fxml` à partir du tutoriel 
    * je peux aussi copier et adapter d'autres fichiers

1. Je commence à remplir le `.fxml` pour ma Vue
    * <span style="background-color:orange; padding:10px;border:2px dashed black;">je m'assure d'avoir au moins un bouton ou autre élément</span>

1. Je déclare ma nouvelle vue dans le frontal

1. Je code les tâches pour afficher ma page comme page principale
    * je copie les tâches pour afficher la `VueRacine`
    * je peux aussi copier et adapter d'autres tâches
    * voici le genre de graphe de tâches qu'on vise

        <img src="frontend.png"/>

        * NOTE: utiliser des noms pertinents à **mon projet**

1. Dans VSCode, je m'assure d'avoir la même arborescence que pour le tutoriel
    * (mais avec des fichiers spécifiques à mon projet)

1. J'expérimente avec le CSS en JavaFx

    * <span style="background-color:orange; padding:10px;border:2px dashed black;">je spécifie au moins 5 propriétés CSS spécifiques à mon projet</span>

1. J'expérimente avec les traductions en JavaFx

    * <span style="background-color:orange; padding:10px;border:2px dashed black;">je m'assure d'avoir au moins un élément avec du texte traduit</span>

1. Je pousse mon projet sur GitLab, p.ex:

        $ git add .
        $ git commit -a -m module03
        $ git push 

1. Je vérifie que mes fichiers sont sur GitLab

1. Je vérifie que projet est fonctionnel avec un `$ git clone` neuf, p.ex:

        # effacer le répertoire tmp s'il existe

        $ mkdir ~/tmp  
        $ cd ~/tmp
        $ git clone https://gitlab.com:USAGER/4f5_prenom_nom
        $ cd 4f5_prenom_nom

        $ sh gradlew mon_projetFr

            # Doit afficher ma page directement, en français

        $ sh gradlew mon_projetEn

            # Doit afficher ma page directement, en anglais

