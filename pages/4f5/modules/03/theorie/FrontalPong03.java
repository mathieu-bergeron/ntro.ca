public class FrontalPong implements FrontendFx {

    @Override
    public void registerViews(ViewRegistrarFx registrar) {

        // Langue par défaut
        registrar.registerDefaultLocale(Ntro.buildLocale("fr"));

        // Traductions
        registrar.registerTranslations(Ntro.buildLocale("fr"), "/traductions/fr.properties");
        registrar.registerTranslations(Ntro.buildLocale("en"), "/traductions/en.properties");
    }
}
