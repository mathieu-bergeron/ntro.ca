tasks.task("installerVueRacine")

	  .waitsFor(window())
	  
	  .waitsFor(created(VueRacine.class))
	  
	  .executes(inputs -> {
		  
		  VueRacine vueRacine = inputs.get(created(VueRacine.class));
		  Window    window    = inputs.get(window());

		  window.installRootView(vueRacine);
      });
