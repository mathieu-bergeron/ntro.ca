registrar.registerTranslations(Ntro.buildLocale("fr"), "/traductions/fr.properties");
registrar.registerTranslations(Ntro.buildLocale("en"), "/traductions/en.properties");

registrar.registerStylesheet("/style/dev.css");
registrar.registerStylesheet("/style/prod.css");
