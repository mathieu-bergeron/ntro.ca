---
title: "Module 3: vues NtroFx"
weight: 20
draft: false
---


{{% pageTitle %}}

<!--

{{<excerpt class="max-width-75">}}

* {{% link "/4f5/migrations/v03" "Migration à `v03` de Ntro" %}}

{{</excerpt>}}

-->


<!--

<center>
<video width="50%" src="rappel.mp4" type="video.webm" controls playsinline>
</center>

-->

* {{% link "./theorie" "Théorie" %}}
    
    * Notion de Vue
    * Vue en FXML

* {{% link src="./tutoriel/" text="Tutoriel à faire avant les objectifs" %}}

    * dans projet `pong`, créer une Vue file d'attente
    * ajouter du CSS
    * traduire le texte

* {{% link src="./objectifs/" text="Objectifs pour avancer le TP#1" %}} 

    * dans mon projet, créer la Vue pour **ma page**
    * ajouter du CSS
    * traduire le texte

<br>

* REMISES:
    * <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=300342">auto-évaluation pour ce module</a>


<br>
<div style="padding:5px;background:pink;border-style:dotted" >
<i>RAPPEL:</i> chaque étudiant.e est <strong>individuellement</strong> responsable de:
<ul>
<li>maintenir <i>son propre</i> dépôt Git
<li>présenter <i>son progrès</i> à la rencontre de travail
<li>faire son auto-évaluation
<li>s'assurer que son code est fonctionnel
</ul> 
<br>
<i>RAPPEL:</i> vous travaillez en équipe, mais les notes sont 100% <strong>individuelles</strong>
</div>
