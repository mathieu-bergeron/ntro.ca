---
title: "Rédiger le document de conception"
weight: 1
bookHidden: true
---


{{% pageTitle %}}

<center>
<video width="50%" src="presentation.mp4" type="video/mp4" controls playsinline>
</center>

1. Choisir un jeu à implanter (doit être unique à votre équipe)
    * {{% link "/4f5/presentation/exemples_de_jeu" "Exemples de jeu" %}}

1. Chaque membre de l'équipe est responsable d'une page différente
    * Envoyer un courriel au prof pour faire approuver vos pages!

    * Exemples de page:
        * profil du joueur
        * page d'achats
        * choix des couleurs du jeu
        * historique des parties
        * tutoriel interactif
        * page pour réassigner les touches du jeu
        * autre page de paramètres
        * n'importe quelle autre page pertinente pour votre jeu


1. Chaque membre de l'équipe doit rédiger un document de conception contenant:

    1. une maquette de votre Vue (affichage de votre page), avec:
        * au moins une façon d'ajouter/retirer des données

    1. votre Modèle (données de votre page), avec:
        * au moins une Liste ou un Map
            * (pour ajouter/retirer des données)
        * au moins une valeur personnalisée (un type de données que vous imaginez)

    1. au moins un test fonctionnel, c'est-à-dire:
        * une action que l'usager peut faire avec votre page
            * (décrire l'action et le résultat attendu)


