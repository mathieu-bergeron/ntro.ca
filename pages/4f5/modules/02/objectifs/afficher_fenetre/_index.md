---
title: "Objectif: afficher la fenêtre"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

<center>
<div style="background-color:orange;width:80%;border:2px dashed black;padding:10px">
<strong>IMPORTANT</strong>
<div style="text-align:left">

* Il faut d'abord faire le tutoriel: on réutilise le même genre de code
* Il faut s'assurer de <strong>bien nommer</strong> les méthodes, les tâches, etc.
    * chaque nom doit être <strong>pertinent</strong> pour mon projet

</div>
</div>
</center>

## Créer des tâches

* Utiliser le même graphe de tâche que dans le tutoriel

    <img src="./frontend.png"/>


## Afficher la fenêtre pour mon projet

* Dans la tâche `afficherFenetre`, ajouter des appels à `window` pour

    * afficher le titre du projet
    * afficher plein écran
    * OU afficher la fenêtre à une certaine taille
    * OU expérimenter avec les décorations de fenêtre


## Remettre sur GitLab et vérifier la remise

1. Pousser mon projet sur GitHub, p.ex:

        $ git add .
        $ git commit -a -m"objectifs #1"
        $ git push 

1. Vérifer que mes fichiers sont sur GitLab

1. Vérifier que mon projet est fonctionnel avec un `$ git clone` neuf, p.ex:

        # effacer le répertoire tmp s'il existe

        $ mkdir ~/tmp
        $ cd ~/tmp
        $ git clone https://gitlab.com:USAGER/4f5_prenom_nom
        $ cd 4f5_prenom_nom
        $ sh gradlew mon_projet

            # Doit afficher la fenêtre avec le titre
