---
title: "Objectifs 2: conception, fenêtre et tâches"
weight: 1
bookHidden: true
---

{{% pageTitle %}}


* {{% link "./concevoir_application" "Rédiger le document de conception" %}}

* {{% link "./afficher_fenetre" "Afficher la fenêtre" %}} (s'inspirer du tutoriel)
