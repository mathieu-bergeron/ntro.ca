---
title: "Tutoriel 2: conception, fenêtre et tâches"
weight: 1
bookHidden: true
---


{{% pageTitle %}}

1. Lire cet {{% link "./exemple_document_conception" "exemple d'un document de conception" %}}

1. Continuer à coder le projet `pong`
   * {{% link "./afficher_fenetre" "afficher la fenêtre et les tâches" %}}

