---
title: ""
weight: 1
bookHidden: true
---


# Document conception pour les tutoriels

{{% video "/4f5/modules/02/tutoriel/exemple_document_conception/modeles01.mp4" %}}



* Erratum: c'est bien la semaine 8 pour commencer à regarder le `ModelePartie`

## Vue


<img width="100%" src="VueFileAttente.png"/>


### NOTES

* «S'inscrire» ajoute un rendez-vous pour l'usager courant
* «Voir» permet d'observer une partie déjà en cours
* «Jouer» permet de débuter une partie contre un usager en attente

<br>
<br>
<br>
<br>

<img width="100%" src="VuePartie.png"/>

### NOTES

* «Quitter» revient à la file d'attente


## Fonctionnalités

<center>
<video width="50%" src="fonctionalites.mp4" type="video/mp4" controls playsinline>
</center>

### Boutons «s'inscrire» et «jouer»

1. Si l'usager est déjà dans la file d'attente:
    * le bouton «s'inscrire» et les boutons «jouer» doivent être cachés

### S'inscire dans la file d'attente

1. Action: l'usager clique sur le bouton «s'inscrire»
1. Effet: la `VueFileAttente` affiche un nouveau rendez-vous de type 
    * `Ee est en attente «Jouer»`

### Débuter une partie


1. Action: un usager *qui n'est pas* dans la file clique sur un bouton «Jouer»
1. Effet: on crée une nouvelle partie et on l'ouvre automatiquement


### Observer une partie

1. Action: un usager *qui n'est pas* dans la file clique sur un bouton «Voir»
1. Effet: on crée une nouvelle `VuePartie` pour observer une partie qui existe déjà

### Quitter une partie

1. Action: dans la `VuePartie`, l'usager clique sur «Quitter»
1. Effet: on ferme la `VuePartie` et on revient à la `VueFileAttente`





## Modèles

### `ModeleFileAttente`

<img width="100%" src="ModeleFileAttente01.png"/>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<img width="100%" src="ModeleFileAttente02.png"/>


<br>
<br>
<br>
<br>

### `ModelePartie`

<img width="100%" src="ModelePartie.png"/>

<br>
<br>
<br>
<br>


### NOTES

<center>
<video width="50%" src="modeles02.mp4" type="video/mp4" controls playsinline>
</center>

* On peut aussi spécifier les modèles directement en Java:

```java
{{% embed src="ModeleFileAttente.java" %}}
```

```java
{{% embed src="RendezVous.java" %}}
```

```java
{{% embed src="RendezVousComplet.java" %}}
```

```java
{{% embed src="Joueur.java" %}}
```

```java
{{% embed src="ModelePartie.java" %}}
```

```java
{{% embed src="Position.java" %}}
```

```java
{{% embed src="InfoJoueur.java" %}}
```

* Ou encore avec un diagramme:

<img width="100%" src="ModeleFileAttente.svg"/>

<br>
<br>
<br>
<br>
<br>
<br>

<img width="100%" src="ModelePartie.svg"/>

* Voire même avec un exemple en Json


```json
{{% embed "ModeleFileAttente.json" %}}
```

<br>
<br>
<br>
<br>
<br>


```json
{{% embed "ModelePartie.json" %}}
```


* L'important est de préciser le genre de données qu'on va sauvegarder et afficher




<br>
<br>
<br>
<br>



