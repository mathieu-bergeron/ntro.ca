public class FrontalPong implements FrontendFx {
    
    @Override
    public void createTasks(FrontendTasks tasks) {
        
        PremierAffichage.creerTaches(tasks);

    }

    @Override
    public void registerEvents(EventRegistrar registrar) {
        
    }

    @Override
    public void registerViews(ViewRegistrarFx registrar) {
        
    }

}
