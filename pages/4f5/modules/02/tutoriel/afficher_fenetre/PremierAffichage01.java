public class PremierAffichage {

    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("PremierAffichage")
        
             .contains(subTasks -> {
                 
                 afficherFenetre(subTasks);

             });
    }
}
