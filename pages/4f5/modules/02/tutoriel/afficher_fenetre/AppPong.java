public class AppPong implements NtroAppFx {

    public static void main(String[] args) {
        NtroAppFx.launch(args);
    }

    @Override
    public void registerFrontend(FrontendRegistrarFx registrar) {
        registrar.registerFrontend(FrontalPong.class);
    }

    @Override
    public void registerMessages(MessageRegistrar registrar) {

    }

    @Override
    public void registerModels(ModelRegistrar registrar) {

    }

    @Override
    public void registerBackend(BackendRegistrar registrar) {

    }

}
