---
title: "Afficher la fenêtre et le graphe des tâches"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer des paquets pour le Frontal

* En VSCode, ajouter le paquet `frontal` au paquet `pong`

* En VSCode, ajouter le paquet `taches` au paquet `pong.frontal`


## Créer des classes pour le frontal

* En VSCode, ajouter la classe `FrontalPong` au paquet `frontal`

* En VSCode, ajouter la classe `PremierAffichage` au paquet `frontal.taches`

## Vérifier l'arborescence du projet `pong`

<img src="arborescence.png"/>

## Créer une tâche pour afficher la fenêtre

* Ouvrir `PremierAffichage.java`

* Avant d'ajouter des tâches à un frontal, copier le `import static` suivant:

<br>
<center>

<span style="background-color:orange; padding:10px;border:2px dashed black;"> `import static ca.ntro.app.tasks.frontend.FrontendTasks.*;` </span>

</center>
<br>

* Ajouter une méthode `creerTaches` qui va créer un groupe de tâches contenant les tâches d'initialisation

    ```java
    {{% embed 
        src="PremierAffichage01.java"
        indent-level="1"
    %}}
    ```

* Ajouter une méthode `afficherFenetre` qui va créer la tâche d'affichage de la fenêtre

    ```java
    {{% embed 
        src="PremierAffichage02.java"
        indent-level="1"
    %}}


{{<excerpt class="note" style="max-width-75">}}

* Attention d'importer `ca.ntro.app.services.Window`

{{</excerpt>}}

## Ajouter la tâche au frontal

* Ouvrir `FrontalPong.java` et s'assurer que la classe implante `FrontendFx`

* Utiliser {{% key "Ctrl+1" %}} pour ajouter le `import FrontendFx`

* Utiliser {{% key "Ctrl+1" %}}, pour ajouter les méthodes obligatoires de `FrontendFx`

* Dans la méthode `createTasks`, appeler `PremierAffichage.creerTaches` afin d'ajouter les tâches d'initialisation

    ```java
    {{% embed 
        src="FrontalPong01.java"
        indent-level="1"
    %}}
    ```

## Déclarer le frontal dans `AppPong`

* J'ouvre `AppPong.java` 

* Dans la méthode `registerFrontend`, s'assurer de déclarer le frontal du client `pong`

    ```java
    {{% embed 
        src="AppPong01.java"
        indent-level="1"
    %}}
    ```

* Utiliser {{% key "Ctrl+1" %}} pour importer `FrontalPong`

## Exécuter `pong` 

* Exécuter le projet en VSCode ou en GitBash

    ```bash
    $ sh gradlew pong
    ```

    <img class="figure" src="fenetre.png"/>

* Observer que:
    * l'application affiche maintenant une fenêtre (il y a un frontal)
    * on peut maintenant quitter l'application en fermant la fenêtre

* Fermer fenêtre pour quitter l'application

## Afficher le graphe de tâches

* On a maintenant un graphe de tâches pour le frontal

* Ce graphe est sauvegardé dans `pong/_storage/graphs/frontend.png`

* Afficher mon graphe de tâches

    <center>
        <img src="frontend.png" />
    </center>

    * on a bien un groupe de tâches qui contient
        * la tâche `window` 
        * la tâche `afficherFenetre` qui attend que la fenêtre existe avant de l'afficher
