---
title: "Frontal Vs dorsal"
weight: 1
bookHidden: true
---

{{% pageTitle %}}



<center>
<video width="50%" src="/4f5/modules/02/theorie/frontal_dorsal/dorsal_frontal.mp4" type="video/mp4" controls playsinline>
</center>

<center>
<img width="50%" src="frontal_dorsal.png" />
</center>

* Le Frontal est la couche de présentation d'une application
    * afficher les données (Vues)
    * interagir avec l'usager (via des Événements)

* Le Dorsal est l'engin (ou moteur) d'une application
    * charge les données (Modèles)
    * effectue les calculs compliqués

* Le Frontal et le Dorsal peuvent 
    * cohabiter dans le même programmes
    * ou habiter dans deux programmes différents

## Client et serveur

<center>
<video width="50%" src="/4f5/modules/02/theorie/frontal_dorsal/client_serveur.mp4" type="video/mp4" controls playsinline>
</center>

<center>
<img width="50%" src="client_serveur.png" />
</center>

* Le Client et le Serveur sont toujours deux programmes différents

* Le Client est le programme qui s'exécute sur la machine l'usager

* Le Serveur est le programme qui s'exécute sur une machine distance, accessible via le réseau



## Frontal/Dorsal Vs Client/Serveur

<center>
<video width="50%" src="/4f5/modules/02/theorie/frontal_dorsal/client_serveur_frontal_dorsal.mp4" type="video/mp4" controls playsinline>
</center>

* Les notions de Frontal/Dorsal et Client/Serveur ne sont pas identiques!

* Une application peut avoir un Frontal et un Dorsal, sans avoir de Serveur

* Un Serveur peut exécuter la majeur partie du Frontal 
    * (dans ce cas le client est un programme minimaliste)

<br>
<br>

* Voici quelques exemples:

<br>


<center>
<img width="100%" src="client_serveur_frontal_dorsal.png" />
</center>

