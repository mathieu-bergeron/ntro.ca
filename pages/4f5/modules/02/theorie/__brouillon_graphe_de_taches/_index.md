---
title: "Graphe de tâches (1)"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

* D'aboord, il faut ajouter cet `import static`

```java
{{% embed src="./Tasks.java" first-line="1" last-line="1" %}}
```

* Ensuite, on doit avoir un objet de type `FrontendTasks`, p.ex.




* Ensuite, on va utiliser un objet de type `FrontendTasks`:

```java
{{% embed src="./Tasks.java" first-line="4" last-line="21" %}}
```

* Si on décortique le code ci-haut, on a 

    * Créer une tâche nommée `afficherVuePartie`

        ```java
        {{% embed src="./Tasks.java" first-line="22" last-line="22" indent-level="2" %}}
        ```

    * Cette tâche attend que la `VueRacine` soit créée:

        ```java
        {{% embed src="./Tasks.java" first-line="24" last-line="24" indent-level="2" %}}
        ```

    * Cette tâche attend aussi que la `VuePartie` soit créée:

        ```java
        {{% embed src="./Tasks.java" first-line="26" last-line="26" indent-level="2" %}}
        ```

    * Cette tâche se déclenche suite à l'événement `EvtAfficherPartie`

        ```java
        {{% embed src="./Tasks.java" first-line="28" last-line="28" indent-level="2" %}}
        ```

    * Finalement, voici le code ce que la tâche exécute:

        ```java
        {{% embed src="./Tasks.java" first-line="30" last-line="36" indent-level="2" %}}
        ```

    * La tâche commence par récupérer les résultats créés par les autres tâches

        * Récupérer la `VueRacine`

            ```java
            {{% embed src="./Tasks.java" first-line="38" last-line="38" indent-level="3" %}}
            ```

        * Récupérer la `VuePartie`

            ```java
            {{% embed src="./Tasks.java" first-line="39" last-line="39" indent-level="3" %}}
            ```


* REMARQUE: on doit avoir les mêmes valeurs dans `waitsFor` et dans `inputs.get`
    * P.ex. `created(VueRacine.class)` se répète comme suit:

    ```java
    {{% embed src="./Tasks.java" first-line="24" last-line="24" indent-level="1" %}}
    ```
    ```java
    {{% embed src="./Tasks.java" first-line="38" last-line="38" indent-level="1" %}}
    ```

    * On peut dire: 

        * `waitsFor` c'est ce dont j'ai besoin pour m'exécuter
        * `inputs.get` me permet de le récupérer
        


## Interpréter un graphe de tâches

<center>
<video width="50%" src="/cegep/420-4F5-MO/modules/02/theorie/graphe_de_taches03.mp4" type="video/mp4" controls playsinline>
</center>

* Voici un extrait du graphe des tâches du tutoriel 04:

<center>
<img width="100%" src="frontend.png" />
</center>

* Chaque flèche est une dépendance

* On peut dire:
    * la tâche `viewLoader[VuePartie]` doit s'exécuter avant la tâche `VuePartie`

* On peut aussi dire:
    * la tâche `VuePartie` va s'exécuter dès que `viewLoader[VuePartie]` est terminée

* Une tâche peut produire un résultat (générer une valeur)

* Une tâche a accès aux résultats produits par les tâches précédentes

* Par exemple, la tâche `afficherVuePartie` a accès aux résultats des tâches:
    * `VuePartie`
    * `viewLoader[VuePartie]`
    * `event[EvtAfficherPartie]`

* Finalement, le graphe des tâches permet de gérer les événements et les messages

* Par exemple, la tâche `afficherVuePartie` va s'exécuter à chaque fois où:
    * la `VuePartie` existe
    * l'événement `EvtAfficherPartie` est déclenché

