---
title: "Afficher la fenêtre"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

<center>
<video width="50%" src="/4f5/modules/02/theorie/fenetre/ntro_window.mp4" type="video/mp4" controls playsinline>
</center>

* La classe `Window` représente la fenêtre principale de l'application

* Voici l'interface:

    ```java
    {{% embed src="./Window.java" indent-level="1" %}}
    ```


* `resize` permet de changer la taille de la fenêtre (en pixels)

* `setTitle` permet de changer le titre de la fenêtre

* `show` permet d'afficher la fenêtre (qui est cachée par défaut)

* `fullscreen` premet d'afficher en plein écran

* `decorations` permet d'afficher ou non la barre et les boutons qu'ajoute l'OS

* `installRootView` permet d'afficher la Vue racine (la page principale)
