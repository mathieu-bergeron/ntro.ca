public interface Window {

    void resize(int width, int height);

    void setTitle(String title);

    void show();

    void fullscreen(boolean isFullscreen);

    void decorations(boolean hasDecorations);

    void installRootView(View<?> rootView);

}
