---
title: "Cadriciel Ntro"
weight: 1
bookHidden: true
---


{{% pageTitle %}}

<center>
<video width="50%" src="/4f5/modules/02/theorie/ntro/cadriciel_ntro.mp4" type="video/mp4" controls playsinline>
</center>

1. Un cadriciel permet 
    * d'organiser le code
    * de simplifier certaines tâches courantes (charger un modèle, créer une vue)

1. `Ntro` est développé par le prof à des fins pédagogiques

1. Le but est d'arrimer les concepts vu en cours avec le code, p.ex:
    * on voit les concepts de Client et de Frontal
    * en `Ntro`, on doit définir une classe `Client` et une classe `Frontal`

1. Un autre objectif est de visualiser le plus possible la logique de l'application

1. `Ntro` a aussi la particularité de supporter à la fois Java et Javascript (via JSweet)
    * p.ex. presque tout sur <a href="https://aiguilleur.ca">aiguilleur.ca</a> est programmé en Java:
        * le serveur Web
        * le dorsal, qui s'exécute sur le serveur
        * le frontal, qui peut s'exécuter
            * sur le serveur (en Java)
            * ou dans le navigateur (en Javascript tiré du même code Java)


## Patron *MVC* en `Ntro`

<center>
<video width="50%" src="/4f5/modules/02/theorie/ntro/mvc_ntro.mp4" type="video/mp4" controls playsinline>
</center>

* De façon général, le patron *MVC* signifie qu'on organise le code en trois parties:
    * Les *Modèles*: 
        * les données
        * la logique *du domaine d'application* (p.ex. les règles d'un jeu)
    * Les *Vues*: 
        * affichage des données
        * interactions avec l'usager
    * Le *Contrôleur*:
        * la logique *du programme*, p.ex.
        * comment charger les modèles (base de données? fichiers?)
        * quand crééer les vues (dès le début? dynamiquement?)

* Typiquement, un cadriciel propose sa propre version du partron *MVC*

* Voici la version proposée par Ntro

    <center>
    <img width="75%" src="mvc_ntro.png" />
    </center>

    * On a une notion explicite de *Frontal* et de *Dorsal*
    * Le Frontal réagit aux *événements* usagers
    * Le Frontal envoit des *messages* au Dorsal
    * Seul le Dorsal peut modifier un modèle
    * Le Frontal reçoit des mises-à-jour quand le modèle est modifié
        * on dit que le frontal *observe* le modèle
        * via les mises-à-jour, le frontal peut afficher les données du modèle
        * (le Frontal ne manipule jamais le modèle au complet)

## Le client en `Ntro`

<center>
<video width="50%" src="/4f5/modules/02/theorie/ntro/client_ntro.mp4" type="video/mp4" controls playsinline>
</center>

* La classe principale d'un client `Ntro` doit implanter `NtroAppFx` (pour JavaFx)

```java
{{% embed src="./ClientPong.java" first-line="1" last-line="1" %}}
```

* Cette classe doit appeler `launch` pour démarrer l'application

```java
{{% embed src="./ClientPong.java" first-line="3" last-line="5" %}}
```

* Elle doit aussi déclarer un frontal, des modèles, des messages et un dorsal

```java
{{% embed src="./ClientPong.java" first-line="7" last-line="26" %}}
```


## Le frontal en `Ntro`

<center>
<video width="50%" src="/4f5/modules/02/theorie/ntro/frontal_ntro.mp4" type="video/mp4" controls playsinline>
</center>

* Le frontal est une classe qui implante `Frontend`

```java
{{% embed src="./FrontalPong.java" first-line="1" last-line="1" %}}
```

* Le frontal doit aussi créer des tâches, déclarer des événements et des vues:

```java
{{% embed src="./FrontalPong.java" first-line="3" last-line="16" %}}
```

## Le dorsal en `Ntro`

<center>
<video width="50%" src="/4f5/modules/02/theorie/ntro/dorsal_ntro.mp4" type="video/mp4" controls playsinline>
</center>

* Le dorsal de base est une classe qui hérite de `LocalBackendNtro`

```java
{{% embed src="./DorsalPong.java" first-line="1" last-line="1" %}}
```

* Le dorsal doit aussi créer des tâches

```java
{{% embed src="./DorsalPong.java" first-line="3" last-line="6" %}}
```

* La méthode `execute` va être appelée quand l'application est prête:

```java
{{% embed src="./DorsalPong.java" first-line="8" last-line="11" %}}
```


## Le contrôleur en `Ntro`: un graphe de tâches

<center>
<video width="50%" src="/4f5/modules/02/theorie/ntro/graphe_de_taches01.mp4" type="video/mp4" controls playsinline>
</center>

* `Ntro` n'a pas de notion explicite de contrôleur

* On va plutôt définir des graphes de tâches 
    * un pour le frontal et un pour le dorsal

* Le frontal va créé les tâches concernant les vues et les événements usager

* Le dorsal va créé les tâches pour recevoir les messages et modifier les modèles
