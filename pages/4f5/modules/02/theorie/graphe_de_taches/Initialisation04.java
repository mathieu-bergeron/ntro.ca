tasks.taskGroup("PremierAffichage")

     .contains(subTasks -> {

        subTasks.task("afficherFenetre")
        
                .waitsFor(window())
             
                .executes(inputs -> {

                    Window window = inputs.get(window());

                    window.setTitle("Pong");

                    window.show();
             });
     });
}
