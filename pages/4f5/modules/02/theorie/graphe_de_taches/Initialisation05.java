public class PremierAffichage {

    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("PremierAffichage")
        
             .contains(subTasks -> {
                 
                 afficherFenetre(subTasks);

             });
    }
    
    private static void afficherFenetre(FrontendTasks subTasks) {

        subTasks.task("afficherFenetre")
        
             .waitsFor(window())
             
             .executes(inputs -> {

                 Window window = inputs.get(window());

                 window.setTitle("Pong");

                 window.show();
             });
    }

}
