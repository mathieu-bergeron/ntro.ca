---
title: "Graphe de tâches (1)"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer un graphe de tâches de base

* D'abord, il faut ajouter cet `import static`

<br>
<center>

<span style="background-color:orange; padding:10px;border:2px dashed black;"> `import static ca.ntro.app.tasks.frontend.FrontendTasks.*;` </span>

</center>
<br>


* Ensuite, on doit recevoir un objet de type `FrontendTasks` en paramètre

    ```java
    {{% embed 
        src="./Initialisation01.java"
        indent-level="1"
    %}}
    ```

* Avec l'objet `tasks`, on peut ajouter un groupe de tâches comme suit:

    ```java
    {{% embed 
        src="./Initialisation02.java"
        indent-level="1"
    %}}
    ```

    * `taskGroup` permet de nommer le groupe de tâches

    * `contains` permet d'ajouter une sous-tâche

* Pour l'instant, le graphe est comme suit:

    <img src="frontend01.png"/>

    * on a un groupe de tâches qui ne contient aucune tâche

* En utilisant `subTasks`, on peut ajouter une sous-tâche

    ```java
    {{% embed 
        src="./Initialisation03.java"
        indent-level="1"
    %}}
    ```

    * `task` permet de nommer la tâche

    * `waitsFor` indique que la tâche doit s'exécuter *après* la tâche `window()`

    * `executes` permet d'ajouter le comportement de la tâche

* Le graphe de tâche est maintenant commet suit:

    <img src="frontend02.png"/>

    * on a le groupe de tâches `Initialisation` contenant deux tâches

        * `window`: une tâche par défaut de Ntro 

        * `afficherFenetre`: une tâche de notre application

    * on peut lire le graphe comme suit:

        * attendre que la fenêtre soit disponible (tâche `window`)
            * puis afficher la fenêtre (tâche `afficherFenetre`)

* Finalement, on utilise `inputs` pour récupérer une valeur 

    ```java
    {{% embed 
        src="./Initialisation04.java"
        indent-level="1"
    %}}
    ```

    * `inputs.get` permet de récupérer une valeur demandée via un `waitsFor`

    * on a `waitsFor(window())` et donc on a `inputs.get(window())`
    

## Définir des méthodes

* En réalité, on va utiliser des méthodes dès que possible:

    ```java
    {{% embed 
        src="./Initialisation05.java"
        indent-level="1"
    %}}
    ```

    * le code est mieux organisé ainsi


## Aperçu de la suite

* On va aussi ajouter des tâches pour
    * réagir à un Événement 
    * recevoir un Message
    * charger une Vue
    * créer des Données
    * observer un Modèle
    * afficher la prochaine image de la partie



* Par exemple, voici le graphe de tâches du frontal du tutoriel 10

    <img src="frontend_tut10.png"/>


* On va aussi utiliser une graphe de tâches pour le dorsal


## NOTE: pourquoi utiliser des groupes de tâches?

* Ce n'est pas obligatoire de définir des groupes de tâche avec `taskGroup`

* Par contre, sans groupes de tâches, le graphe est plus difficle à lire

    <img src="frontend_plat.png"/>

