public class Initialisation {

    public static void creerTaches(FrontendTasks tasks) {
        
        tasks.taskGroup("Initialisation")
        
             .contains(subTasks -> {
                 
                 afficherFenetre(subTasks);

             });
    }
    
    private static void afficherFenetre(FrontendTasks subTasks) {

        subTasks.task("afficherFenetre")
        
             .waitsFor(window())
             
             .executes(inputs -> {

                 Window window = inputs.get(window());

                 window.show();
             });
    }

}
