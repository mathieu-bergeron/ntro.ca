---
title: "Module 2: concevoir l'application"
weight: 20
draft: false
---

{{% pageTitle %}}


{{<excerpt class="max-width-75">}}

* {{% link "/4f5/migrations/v02" "Migration à `v02` de Ntro" %}}

{{</excerpt>}}


<!--

<center>
<video width="50%" src="rappel.mp4" type="video.webm" controls playsinline>
</center>

-->

* {{% link "./theorie" "Théorie" %}}
    
    * frontal Vs Dorsal + cadriciel Ntro
    * afficher la fenêtre


* {{% link src="./tutoriel/" text="Tutoriel à faire avant les objectifs" %}}

    * exemple d'un document de conception
    * afficher la fenêtre principale du projet `pong`

* {{% link src="./objectifs/" text="Objectifs pour avancer le TP#1" %}} 

    * rédiger le document de conception pour ma page
    * afficher la fenêtre principale pour mon projet

* REMISES:
    * Le PDF de mon <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/assign/view.php?id=291482">document de conception sur Moodle</a>
    * <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=291488">auto-évaluation pour ce module</a>

<br>
<div style="padding:5px;background:pink;border-style:dotted" >
<i>RAPPEL:</i> chaque étudiant.e est <strong>individuellement</strong> responsable de:
<ul>
<li>maintenir <i>son propre</i> dépôt Git
<li>présenter <i>son progrès</i> à la rencontre de travail
<li>faire son auto-évaluation
<li>s'assurer que son code est fonctionnel
</ul> 
<br>
<i>RAPPEL:</i> vous travaillez en équipe, mais les notes sont 100% <strong>individuelles</strong>
</div>
