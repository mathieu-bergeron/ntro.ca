public class MaquetteJoueurs {

    public static Joueur creerJoueur(String id) {
        Joueur joueur = new Joueur();

        if(id.equals("alice")) {

            joueur = creerJoueur(id, "Alice", "Aram");

        }else if(id.equals("bob")) {

            joueur = creerJoueur(id, "Bob", "Bérancourt");

        }else if(id.equals("charlie")) {

            joueur = creerJoueur(id, "Charlie", "Chen");

        }else {

            joueur = joueurAleatoire(id);

        }

        return joueur;
    }

    // ...
