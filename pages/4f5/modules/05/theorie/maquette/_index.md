---
title: "Théorie: notion de maquette"
weight: 1
bookHidden: true
---


{{% pageTitle %}}

<center>
<video width="50%" src="maquette.mp4" type="video/mp4" controls playsinline>
</center>


* Une maquette (en anglais: *mock*) est tout simplement une classe qui simule une fonctionnalité qui n'est pas encore implanté pour vrai

* On utilise souvent des maquettes pour les tests unitaires

* Dans le cours, vous devez créer une maquette pour générer des données pour votre page

## Exemple: `MaquetteJoueurs`

* Dans les tutoriels, on utiliser une classe `MaquetteJoueurs` pour créer des joueurs au besoin

* Par exemple, on a 

```java
{{% embed src="MaquetteJoueurs01.java" %}}
```


* Et on l'utilise pour simuler que différents joueurs s'inscrivent à la file d'attente

```java
{{% embed src="SessionPong01.java" %}}
```

* Dans l'application finale, on pourra remplacer les appels à la maquette à la vrai logique de l'usager courant
