public class VueFileAttente extends ViewFx {

    @FXML
    private Button boutonJoindrePartie;

    @FXML
    private Button boutonSInscrire;

    @FXML
    private Label labelRendezVous;

    @Override
    public void initialize() {

        Ntro.assertNotNull("boutonOuvrirPartie", boutonJoindrePartie);
        Ntro.assertNotNull("boutonAjouterRendezVous", boutonSInscrire);
        Ntro.assertNotNull("labelRendezVous", labelRendezVous);

        installerEvtAfficherPartie();
        installerMsgAjouterRendezVous();
    }


    private void installerEvtAfficherPartie() {
        
        EvtAfficherPartie evtNtro = Ntro.newEvent(EvtAfficherPartie.class);

        boutonJoindrePartie.setOnAction(evtFx -> {
            
            evtNtro.trigger();
        });
    }

    private void installerMsgAjouterRendezVous() {

        MsgAjouterRendezVous msgAjouterRendezVous = Ntro.newMessage(MsgAjouterRendezVous.class);

        boutonSInscrire.setOnAction(evtFx -> {

            msgAjouterRendezVous.setPremierJoueur(MaquetteJoueurs.usagerCourant());
            msgAjouterRendezVous.send();
            
            MaquetteJoueurs.prochainJoueur();
        });
    }

    public void afficherRendezVousEnTexte(String message) {
        labelRendezVous.setText(message);
    }

}
