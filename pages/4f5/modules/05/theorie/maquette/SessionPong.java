public class SessionPong extends Session<SessionPong> {
    
    private Class<?>              vueCourante         = VueFileAttente.class;
    private Joueur                joueurCourant       = null;
    private String                idPartieCourante    = null;
    private List<Position>        positionsLocales    = new ArrayList<>();
    private Map<Position, String> idJoueurParPosition = new HashMap<>();

    public SessionPong memoriserVueCourante(Class<?> vueCourante) {

        this.vueCourante = vueCourante;

        return this;
    }
    
    public SessionPong memoriserPartieCourante(String idPartieCourante) {

        this.idPartieCourante = idPartieCourante;
        
        return this;
    }

    public void afficherRegionCourante(VueFileAttente vueFileAttente) {

        String idRegionCourante = this.getModelSelection(ModeleFileAttente.class);

        vueFileAttente.afficherRegionCourante(idRegionCourante);

    }

    public void envoyerEvtPourAfficherVueCourante() {

      if(vueCourante.equals(VuePartie.class)) {
          
          Ntro.newEvent(EvtAfficherPartie.class)
              .setIdPartie(idPartieCourante)
              .trigger();

      }else {

          Ntro.newEvent(EvtAfficherFileAttente.class)
              .trigger();

      }

    }
    
    private void initialiserSession() {
        if(joueurCourant == null) {
            joueurCourant = MaquetteJoueurs.creerJoueur(sessionId());
        }
    }

    protected Joueur joueurCourant() {
        return joueurCourant;
    }

    public void envoyerMsgAjouterRendez() {
        if(Ntro.options().isProd()) {

            envoyerMsgAjouterRendezVousProd();
            
        }else {

            envoyerMsgAjouterRendezVousDev();
            
        }

    }

    private void envoyerMsgAjouterRendezVousProd() {
        Ntro.newMessage(MsgAjouterRendezVous.class)
            .setPremierJoueur(joueurCourant())
            .send();
    }

    private void envoyerMsgAjouterRendezVousDev() {
        Ntro.newMessage(MsgAjouterRendezVous.class)
            .setPremierJoueur(MaquetteJoueurs.joueurAleatoire(this.sessionId()))
            .send();
    }

    public void envoyerMsgAjouterPoint(Position position) {
        if(estCePositionLocale(position)) {
            Ntro.newMessage(MsgAjouterPoint.class)
                .setPosition(position)
                .send();
        }
    }

    public boolean estCePositionLocale(Position position) {
        return positionsLocales.contains(position);
    }

    public boolean estCePositionDistante(Position position) {
        return !estCePositionLocale(position);
    }
    
    private List<Position> positionsDistantes(){
        List<Position> positionsDistantes = new ArrayList<>();
        
        for(Position position : Position.values()) {
            if(estCePositionDistante(position)) {
                positionsDistantes.add(position);
            }
        }
        
        return positionsDistantes;
    }

    private List<Position> positionLocales(){
        return positionsLocales;
    }


    public void envoyerMsgRejoindreRendezVous(String idRendezVous) {
        if(Ntro.options().isProd()) {
            
            envoyerMsgRejoindreRendezVousProd(idRendezVous);
            
        }else {

            envoyerMsgRejoindreRendezVousDev(idRendezVous);
            
            
        }
    }

    public void envoyerMsgRejoindreRendezVousProd(String idRendezVous) {
        Ntro.newMessage(MsgRejoindreRendezVous.class)
            .setIdRendezVous(idRendezVous)
            .setJoueur(joueurCourant())
            .send();
    }

    public void envoyerMsgRejoindreRendezVousDev(String idRendezVous) {
        Ntro.newMessage(MsgRejoindreRendezVous.class)
            .setIdRendezVous(idRendezVous)
            .setJoueur(MaquetteJoueurs.joueurAleatoire(sessionId()))
            .send();
    }

    public SessionPong envoyerMessageRejoindrePartie() {

        for(Position position : positionsLocales) {
            Ntro.newMessage(MsgRejoindrePartie.class)
                .setModelSelection(ModelePartie.class, idPartieCourante)
                .setPosition(position)
                .send();
        }

        return this;
    }

    public void oublierPartieCourante() {
        idPartieCourante = null;
        positionsLocales.clear();
        idJoueurParPosition.clear();
    }

    public SessionPong memoriserPartieCourante(String idPartie,
                                               String idPremierJoueur,
                                               String idDeuxiemeJoueur) {
        

        if(Ntro.isApp()) {
            
            memoriserPartieCouranteApp(idPartie, 
                                       idPremierJoueur, 
                                       idDeuxiemeJoueur);


        }else {

            memoriserPartieCouranteClient(idPartie, 
                                          idPremierJoueur, 
                                          idDeuxiemeJoueur);
            
        }

        return this;
    }

    private void memoriserPartieCouranteCommun(String idPartie, 
                                               String idPremierJoueur, 
                                               String idDeuxiemeJoueur) {
        oublierPartieCourante();
        this.idPartieCourante = idPartie;
        
        idJoueurParPosition.put(Position.GAUCHE, idPremierJoueur);
        idJoueurParPosition.put(Position.DROITE, idDeuxiemeJoueur);
    }

    private void memoriserPartieCouranteApp(String idPartie,
                                            String idPremierJoueur,
                                            String idDeuxiemeJoueur) {

        memoriserPartieCouranteCommun(idPartie, 
                                      idPremierJoueur, 
                                      idDeuxiemeJoueur);

        for(Position position : Position.values()) {
            positionsLocales.add(position);
        }
    }

    private void memoriserPartieCouranteClient(String idPartie,
                                               String idPremierJoueur,
                                               String idDeuxiemeJoueur) {

        memoriserPartieCouranteCommun(idPartie, 
                                      idPremierJoueur, 
                                      idDeuxiemeJoueur);

        if(joueurCourant.estJoueur(idPremierJoueur)) {
            positionsLocales.add(Position.GAUCHE);
        }

        if(joueurCourant.estJoueur(idDeuxiemeJoueur)) {
            positionsLocales.add(Position.DROITE);
        }
    }
    

    public SessionPong diffuserMsgActionAutreJoueur(EvtActionJoueur evtActionJoueur) {

        if(!Ntro.isApp()) {

            diffuserMsgActionAutreJoueurClient(evtActionJoueur);
        }

        return this;
    }

    public void diffuserMsgActionAutreJoueurClient(EvtActionJoueur evtActionJoueur) {
        for(Position position : positionsDistantes()) {
            String idAutreJoueur = idJoueurParPosition.get(position);

            if(idAutreJoueur != null) {
                evtActionJoueur.diffuserMsgActionAutreJoueur(idAutreJoueur);
            }
        }
    }

    public SessionPong envoyerMsgQuitterPartie() {
        for(Position position : positionLocales()) {
            Ntro.newMessage(MsgQuitterPartie.class)
                .setModelSelection(ModelePartie.class, idPartieCourante)
                .setPosition(position)
                .send();
        }
        
        return this;
    }

    public void declancherEvtActionJoueur(KeyCode code) {
        
    }

}
