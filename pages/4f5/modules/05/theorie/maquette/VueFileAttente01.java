public class VueFileAttente extends ViewFx {

    // ...
    
    private void installerMsgAjouterRendezVous() {

        MsgAjouterRendezVous msgAjouterRendezVous = Ntro.newMessage(MsgAjouterRendezVous.class);

        boutonSInscrire.setOnAction(evtFx -> {

                                                  // inscrire l'usager courant
            msgAjouterRendezVous.setPremierJoueur(MaquetteJoueurs.usagerCourant());
            msgAjouterRendezVous.send();
            
            // ensuite créer un nouvel usager
            MaquetteJoueurs.prochainJoueur();
        });
    }
