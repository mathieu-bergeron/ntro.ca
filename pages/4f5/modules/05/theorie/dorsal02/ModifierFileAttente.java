tasks.task("modifierModele")

     .waitsFor(modele(MonModele.class))

     .executes(inputs -> {

         MonModele monModele = input.gets(model(MonModele.class);


         monModele.ajouterQuelqueChose("valeur");

     });
