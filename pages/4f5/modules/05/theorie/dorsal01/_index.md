---
title: ""
weight: 1
bookHidden: true
---


# Théorie: créer un dorsal

<center>
<video width="50%" src="dorsal01.mp4" type="video/mp4" controls playsinline>
</center>


1. Notre client doit **obligatoirement** déclarer un dorsal

1. Si le dorsal est dans le client, on va déclarer un dorsal *local*

1. Si le dorsal s'exécute sur le serveur, on va déclarer un dorsal *distant*

## Créer un dorsal *local*

<center>
<video width="50%" src="local01.mp4" type="video/mp4" controls playsinline>
</center>

1. Un dorsal local hérite de `LocalBackendNtro`

```java
{{% embed src="./DorsalPong01.java" first-line="1" last-line="1" %}}
```

1. Un dorsal local doit déclarer des tâches

```java
{{% embed "./DorsalPong01.java" %}}
```

1. On utilise un `BackendTasks` pour créer les tâches du dorsal

    * ce n'est pas le même genre de tâches qu'un frontal

1. On va aussi faire un `import static` différent

    * <span style="background-color:orange; padding:10px;border:2px dashed black;"> `import static ca.ntro.app.tasks.backend.BackendTasks.*;` </span>



## Créer un dorsal *distant*

<center>
<video width="50%" src="distant01.mp4" type="video/mp4" controls playsinline>
</center>

1. Un dorsal distant hérite de `RemoteBackendNtro`

```java
{{% embed src="./DorsalPong02.java" first-line="1" last-line="1" %}}
```

1. Un dorsal distant doit déclarer un serveur auquel se connecter

```java
{{% embed "./DorsalPong02.java" %}}
```

1. Le client se connecte alors à un seveur où s'exécute le vrai dorsal

    * les modèles sont entroposés et modifiés sur le serveur



## Déclarer le dorsal

<center>
<video width="50%" src="declarer01.mp4" type="video/mp4" controls playsinline>
</center>

1. Qu'il soit local ou distant, le client doit déclarer le dorsal:

```java
{{% embed "./ClientPong01.java" %}}
```

## Ajouter des tâches au dorsal

<center>
<video width="50%" src="taches01.mp4" type="video/mp4" controls playsinline>
</center>

1. Les tâches d'un dorsal sont typiquement plus simples que celles du frontal

1. Typiquement, le dorsal va recevoir un message et modifier le modèle:

    <center>
        <img width="100%" src="backend01.png"/>
    </center>

1. Pour `pong`, le backend complet va être

    <center>
        <img width="100%" src="backend_pong.png"/>
    </center>

