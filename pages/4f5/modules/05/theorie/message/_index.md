---
title: ""
weight: 1
bookHidden: true
---


# Théorie: notion de message


## Définir et déclarer un message

<center>
<video width="50%" src="declarer.mp4" type="video/mp4" controls playsinline>
</center>

1. Le frontal et le dorsal communique à travers des messages

1. En `Ntro`, un message est une classe qui hérite de `MessageNtro`, p.ex.

    ```java
    {{% embed src="./MsgAjouterRendezVous.java" indent-level="1" %}}
    ```

1. On doit aussi déclarer le message, p.ex.

    ```java
    {{% embed src="./ClientPong01.java" indent-level="1" %}}
    ```


1. NOTE: le message est envoyé sur le réseau au format `.json`

## Création et envoi d'un message

<center>
<video width="50%" src="creer.mp4" type="video/mp4" controls playsinline>
</center>

1. On fait un appel `newMessage` pour créer un message

    ```java
    {{% embed src="./VueFileAttente.java" first-line="1" last-line="1" indent-level="1" %}}
    ```

1. Par convention, on va créer, initialiser et envoyer le message d'un seul coup

    ```java
    {{% embed src="./VueFileAttente.java" first-line="3" last-line="5" indent-level="1" %}}
    ```


## Reception d'un message

<center>
<video width="50%" src="recevoir.mp4" type="video/mp4" controls playsinline>
</center>

1. Le message est reçu dans une tâche

1. Dans une tâche du dorsal, on va recevoir le message explicitement. P.ex.

    ```java
    {{% embed src="./ModifierFileAttente.java" indent-level="1" %}}
    ```

    * la tâche va s'exécuter à chaque fois qu'on reçoit un message de type `MsgAjouterRendezVous`


1. Dans une tâche du frontal, on reçoit deux types de messages
    * l'observation d'un modèle (qui implicitement est un message transitant par le réseau)
    * les messages d'autres joueurs dans le cas d'une partie réseau

## Implantation client/serveur

<center>
<video width="50%" src="client_serveur.mp4" type="video/mp4" controls playsinline>
</center>

1. Si le dorsal et le frontal sont les deux dans le client, on a:
    * chaque message est conservé dans une file d'attente
    * dès que possible, le message est reçu dans une tâche

1. Si le dorsal est sur un serveur (et le frontal sur le client), on a:
    * chaque message est d'abord sauvegardé en `.json`
    * le message est ensuite envoyé sur le réseau
    * de l'autre côté, le `.json` est reçu 
    * le message est construit à partir du `.json`
    * ensuite, le message est reçu dans une tâche

1. NOTES: 
    * le code réseau est caché dans la libraire `Ntro`
    * le/la programmeur/programmeuse d'application n'a pas à le faire
