---
title: "Théorie module 5"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

1. {{% link "./fonctionnalite/" "Patron MVC: qu'est-ce qu'une fonctionnalité?" %}}

1. {{% link "./dorsal01/" "Créer un dorsal" %}}

1. {{% link "./message/" "Notion de Message" %}}

1. {{% link "./dorsal02/" "Modifier un modèle dans le dorsal" %}}

1. {{% link "./maquette/" "Notion de maquette" %}}

