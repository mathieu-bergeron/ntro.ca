---
title: "Module 5: ajouter le dorsal, modifier le modèle"
weight: 70
draft: false
---


{{% pageTitle %}}

<!--

<center>
<video width="50%" src="rappel.mp4" type="video.webm" controls playsinline>
</center>

-->

{{<excerpt class="note">}}

* dernier module avant le TP1 et l'examen1

{{</excerpt>}}

* {{% link "./theorie" "Théorie" %}}
    
    * retour sur la notion de MVC: qu'est-ce qu'une fonctionnalité?
    * notion de maquette
    * notion de dorsal
    * notion de message

* {{% link src="./tutoriel/" text="Tutoriel à faire avant les objectifs" %}}

    * créer un dorsal
    * modifier le modèle

* {{% link src="./objectifs/" text="Objectifs pour avancer le TP#1" %}} 

    * ajout d'un dorsal
    * ajout d'une fonctionnalité à **mon projet**

<br>


ÉVALUATIONS
<table>

<tr>
<td>
<a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=302223">Auto-évaluation pour ce module</a>
</td>

<td>
lundi 26 février 23h59
</td>


</tr>

<tr>
<td>

{{% link "/4f5/enonces/tp1/" "TP1" %}} 

</td>

<td>
vendredi 1er mars 23h59
</td>

</tr>

<tr>
<td>

{{% link "/4f5/enonces/examen1/" "Examen1" %}}

</td>

<td>
vendredi 1er mars, dans le cours
</td>
</tr>
</table>


<br>
<div style="padding:5px;background:pink;border-style:dotted" >
<i>RAPPEL:</i> chaque étudiant.e est <strong>individuellement</strong> responsable de:
<ul>
<li>maintenir <i>son propre</i> dépôt Git
<li>présenter <i>son progrès</i> à la rencontre de travail
<li>faire son auto-évaluation
<li>s'assurer que son code est fonctionnel
</ul> 
<br>
<i>RAPPEL:</i> vous travaillez en équipe, mais les notes sont 100% <strong>individuelles</strong>
</div>
