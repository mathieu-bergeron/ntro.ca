---
title: ""
weight: 1
bookHidden: true
---


# Objectifs 5: implanter une fonctionnalité

<!--

<center>
<video width="50%" src="presentation.mp4" type="video/mp4" controls playsinline>
</center>

-->

<center>
<div style="background-color:orange;width:80%;border:2px dashed black;padding:10px">
<strong>IMPORTANT</strong>
<div style="text-align:left">
<ul>
<li>Je commence à implanter <strong>ma fonctionnalité</strong>
<li>Je peux implanter une version simplifiée de ma fonctionnalité
    <ul>
        <li>mais <strong>je dois</strong> ajouter (ou retirer) des données à ma liste (ou mon map)
    </ul>
    
</ul>
</div>
</center>

1. Effectuer le {{% link "../tutoriel/" "tutoriel" %}} pour comprendre comment
    * envoyer un message au dorsal
    * recevoir le message dans le dorsal et modifier le modèle 
    * observer et afficher le modèle dans le frontal

1. Sur ma Vue, ajouter un bouton correspondant à une fonctionnalité
    * cette fonctionnalité doit <strong>ajouter quelque chose à ma liste</strong>
    * (je peux générer des données au hasard comme dans le tutoriel)
    * (je pourrais aussi lire des données d'un fichier ou d'un tableau)

1. Implanter un message correspondant à ma fonctionnalité

1. Dans mon dorsal, recevoir le message et modifier le modèle

    <img src="backend.png"/>

    * utiliser des **noms pertinents** pour ma page
    * (donc autre chose que `MsgAjouterValeurBidon`!)


1. Dans mon frontal, observer le modèle et l'afficher 
    * (devrait déjà fonctionner après le module 4)

1. S'assurer que tout fonctionne
    * les données **doivent s'ajouter** au fichier `.json` de mon modèle
    * l'ajout au modèle doit s'afficher à chaque fois qu'on appuie sur le bouton

    <div style="max-width:300px;">
    {{% animation src="/4f5/modules/05/objectifs/objectifs5.mp4" %}}
    </div>


1. S'assurer que les noms d'attribut/méthode sont adéquats **pour mon projet**
    * on ne veut **pas de** `MsgAjouterRendezVous` dans une page paramètres!

1. Pousser mon projet sur GitLab, p.ex:

        $ git add .
        $ git commit -a -m module05
        $ git push 

1. Vérifier que mes fichiers sont sur GitLab

1. Je vérifie que projet est fonctionnel avec un `$ git clone` neuf, p.ex:

        # effacer le répertoire tmp s'il existe

        $ mkdir ~/tmp

        $ cd ~/tmp

        $ git clone https://gitlab.com/USAGER/4f5_prenom_nom

        $ cd 4f5_prenom_nom

        $ sh gradlew mon_projetFr

            # Doit afficher ma page en français
            # Doit permettre d'ajouter ou retirer quelque chose au modèle
            # Doit afficher le modèle en mode texte 
            # (on doit voir les modifications au modèle)

        $ sh gradlew mon_projetEn

            # Doit afficher ma page en anglais
            # Doit permettre d'ajouter ou retirer quelque chose au modèle
            # Doit afficher le modèle en mode texte 
            # (on doit voir les modifications au modèle)
