---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: `boutonSInscrire`

1. Dans `file_attente.fxml`, vérifier qu'on a le `boutonSInscrire`

    ```xml
    {{% embed src="./file_attente01.fxml" indent-level="1" %}}
    ```


1. Dans `fr.properties`, s'assurer d'avoir le texte suivant

    ```
    sInscrire=S'inscrire
    ```

1. Dans `en.properties`, s'assurer d'avoir le texte suivant

    ```
    sInscrire=Register
    ```

1. Dans `VueFileAttente` s'assurer d'avoir l'attribut `boutonSInscrire`

    ```java
    {{% embed src="./VueFileAttente01.java" indent-level="1" %}}
    ```


1. Au besoin, corriger les erreurs de compilation

1. En français, je devrais voir

        $ sh gradlew pongFr

    <center>
    <img width="75%" src="fr.png"/>
    <center>

    * la liste des rendez-vous sera différente selon votre fichier `.json` pour le modèle


1. En anglais, je devrais voir

        $ sh gradlew pongEn

    <center>
    <img width="75%" src="en.png"/>
    <center>


