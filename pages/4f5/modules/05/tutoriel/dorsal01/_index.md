---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: créer le dorsal

## Créer le paquet `dorsal`

1. Dans `pong`, créer le paquet `dorsal`

1. S'assurer d'avoir l'arborescence suivante

    <center>
    <img src="eclipse01.png" />
    </center>

1. NOTE: le paquet `dorsal` va directement sous `pong`
    * le dorsal est un élément de l'application, au même niveau que le Frontal

## Créer la classe `DorsalPong`

1. Dans le paquet `dorsal`, créer la classe `DorsalPong`

1. S'assurer d'avoir l'arborescence suivante 

    <center>
    <img src="eclipse02.png" />
    </center>

1. Ajuster la signature de `DorsalPong` qui doit hériter de `LocalBackendNtro`


    ```java
    {{% embed src="./DorsalPong01.java" first-line="1" last-line="1" indent-level="1" %}}
    ```

1. Utiliser {{% key "Ctrl+1" %}} pour ajouter les `import` et créer les méthodes obligatoires

1. S'assurer d'avoir la méthode suivante

    ```java
    {{% embed src="./DorsalPong01.java" indent-level="1" %}}
    ```


1. NOTE: dans le Dorsal, la méthode `createTasks` reçoit un `BackendTasks`
    * (et non un `FrontendTasks`)
    * dans un dorsal, on va créer des tâches spécifiques à un dorsal

## Déclarer le dorsal dans `AppPong`

1. Ouvrir `AppPong` et ajouter le code pour déclarer le dorsal

    ```java
    {{% embed src="./ClientPong01.java" indent-level="1" %}}
    ```

1. Au besoin, corriger les erreurs de compilation
