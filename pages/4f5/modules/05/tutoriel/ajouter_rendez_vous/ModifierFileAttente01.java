private static void ajouterRendezVous(BackendTasks subTasks) {
    subTasks.task("ajouterRendezVous")

         .waitsFor(model(ModeleFileAttente.class))

         .waitsFor(message(MsgAjouterRendezVous.class))
         
         .executes(inputs -> {

             MsgAjouterRendezVous msgAjouterRendezVous = inputs.get(message(MsgAjouterRendezVous.class));
             ModeleFileAttente    fileAttente          = inputs.get(model(ModeleFileAttente.class));

             // ajouter
             msgAjouterRendezVous.ajouterA(fileAttente);

         });
}
