---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel 5: ajouter un rendez-vous

1. Ajouter `MsgAjouterRendezVous.ajouterA`

    ```java
    {{% embed src="./MsgAjouterRendezVous01.java" indent-level="1" %}}
    ```

1. Implanter `ajouterRendezVous`

    ```java
    {{% embed src="./ModeleFileAttente01.java" indent-level="1" %}}
    ```

1. Implanter le constructeur pour `RendezVous`

    ```java
    {{% embed src="./RendezVous01.java" indent-level="1" %}}
    ```

    * NOTES:
        * je **dois conserver** le constructeur par défaut
        * mais j'ai le droit d'ajouter d'autres constructeurs


1. Ajouter l'appel à `ajouterA` au bon endroit dans `ModifierFileAttente`

    ```java
    {{% embed src="./ModifierFileAttente01.java" indent-level="1" %}}
    ```

1. Vérifier que l'ajout d'un rendez-vous fonctionne

    {{% animation src="/4f5/modules/05/tutoriel/ajouter_rendez_vous/ajouter_rendez_vous.mp4" %}}

