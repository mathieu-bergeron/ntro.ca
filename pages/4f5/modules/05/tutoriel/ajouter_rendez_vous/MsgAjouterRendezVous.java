public class MsgAjouterRendezVous extends Message<MsgAjouterRendezVous> {
    
    private Joueur premierJoueur;

    public Joueur getPremierJoueur() {
        return premierJoueur;
    }

    public MsgAjouterRendezVous setPremierJoueur(Joueur premierJoueur) {
        this.premierJoueur = premierJoueur;
        
        return this;
    }

    public String ajouterA(ModeleFileAttente fileAttente) {

        return fileAttente.ajouterRendezVous(premierJoueur);
    }

}
