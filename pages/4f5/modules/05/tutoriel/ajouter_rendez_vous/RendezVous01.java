public class RendezVous implements ModelValue {
    
    private String idRendezVous;
    private Joueur premierJoueur;

    // ...

    public RendezVous() {
    }

    public RendezVous(String idRendezVous, Joueur premierJoueur) {
        this.idRendezVous = idRendezVous;
        this.premierJoueur = premierJoueur;
    }
