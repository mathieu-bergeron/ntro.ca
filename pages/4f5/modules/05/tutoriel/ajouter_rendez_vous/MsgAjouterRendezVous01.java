public class MsgAjouterRendezVous extends Message<MsgAjouterRendezVous> {
    
    private Joueur premierJoueur;

    // ...
    

    // ajouter
    public void ajouterA(ModeleFileAttente fileAttente) {

        fileAttente.ajouterRendezVous(premierJoueur);
    }

}
