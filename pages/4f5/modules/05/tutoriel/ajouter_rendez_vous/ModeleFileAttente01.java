public class ModeleFileAttente implements Model, Watch, WriteObjectGraph {

    private long prochainIdRendezVous = 1;
    private List<RendezVous> rendezVousDansOrdre = new ArrayList<>();

    // ...

    public void ajouterRendezVous(Joueur premierJoueur) {

        String idRendezVous = genererIdRendezVous();

        RendezVous rendezVous = new RendezVous(idRendezVous, premierJoueur);

        rendezVousDansOrdre.add(rendezVous);
    }

    private String genererIdRendezVous() {
        String idRendezVous = String.valueOf(prochainIdRendezVous);
        prochainIdRendezVous++;

        return idRendezVous;
    }

