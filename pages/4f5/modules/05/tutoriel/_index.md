---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: implanter une fonctionnalité

<center>
<div style="background-color:orange;width:80%;border:2px dashed black;padding:10px">
<strong>IMPORTANT</strong>
<div style="text-align:left">
<ul>
<li>Je m'assure d'avoir terminé le tutoriel 4 avant de commencer
</ul>
</div>
</center>

1. {{% link "./dorsal01/" "Créer le dorsal" %}} 

1. {{% link "./vue01/" "S'assurer d'avoir le `boutonSInscrire` dans  `VueFileAttente`" %}}

1. {{% link "./maquette_joueurs/" "Créer une maquette pour simuler les joueurs" %}}

1. {{% link "./message/" "Créer et envoyer le message `MsgAjouterRendezVous`" %}}

1. {{% link "./dorsal02/" "Modifier le modèle dans le dorsal" %}} 

1. {{% link "./ajouter_rendez_vous/" "Ajouter un rendez-vous à la file d'attente" %}} 

<!--
* (exemple d'inversion des dépendances)
-->

