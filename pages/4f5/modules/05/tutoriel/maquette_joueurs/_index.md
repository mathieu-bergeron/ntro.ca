---
title: "Tutoriel: maquette usagers"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Créer le paquet `maquettes` et la classe `MaquetteJoueurs`

1. Dans `pong`, je crée le paquet `maquettes`


1. Dans le paquet `pong.maquettes`, créer la classe `MaquetteJoueurs`

1. Je m'assure d'avoir l'arborescence suivante dans mon projet:

    <center>
    <img src="eclipse01.png" />
    </center>

1. Au besoin, ajouter les setters à la classe Joueur

    ```java
    {{% embed src="Joueur01.java" indent-level="1" %}}
    ```

1. Ouvrir `MaquetteJoueurs.java` et ajouter ce code:

    ```java
    {{% embed src="MaquetteJoueurs01.java" indent-level="1" %}}
    ```

