
    private void installerMsgAjouterRendezVous() {

        MsgAjouterRendezVous msgAjouterRendezVous = Ntro.newMessage(MsgAjouterRendezVous.class);

        boutonSInscrire.setOnAction(evtFx -> {

            // l'usager courant s'inscrit
            msgAjouterRendezVous.setPremierJoueur(MaquetteJoueurs.usagerCourant());
            msgAjouterRendezVous.send();

            // à chaque clic, on passe à un nouvel usager
            MaquetteJoueurs.prochainJoueur();

        });
    }
