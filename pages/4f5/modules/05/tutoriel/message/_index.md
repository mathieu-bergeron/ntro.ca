---
title: ""
weight: 1
bookHidden: true
---


## Définir et déclarer le message

1. Dans le paquet `pong.commun`, créer le paquet `messages`

1. Dans le paquet `message`, créer la classe `MsgAjouterRendezVous`

1. Dans VSCode, s'assurer que l'arborescence de mon projet est comme suit

    <center>
    <img src="eclipse01.png"/>
    <center>

1. Ajuster la signature de `MsgAjouterRendezVous` qui doit hériter de `Message`

    ```java
    {{% embed src="./MsgAjouterRendezVous01.java" indent-level="1" %}}
    ```

1. Ajouter l'attribut `premierJoueur` et une méthode pour mémoriser sa valeur

    ```java
    {{% embed src="./MsgAjouterRendezVous02.java" indent-level="1" %}}
    ```

1. Dans `AppPong`, déclarer le message

    ```java
    {{% embed src="./ClientPong01.java" indent-level="1" %}}
    ```

    * NOTES:
        * le message est partagé entre le frontal et le dorsal


1. Au besoin, corriger les erreurs de compilation


## Créer et envoyer le message

1. Dans `VueFileAttente`, créer une méthode d'où envoyer les messages

    ```java
    {{% embed src="./VueFileAttente01.java" indent-level="1" %}}
    ```

1. Ajouter le capteur d'événement pour `boutonSInscrire`

    ```java
    {{% embed src="./VueFileAttente02.java" indent-level="1" %}}
    ```


1. Pour chaque clic, demander à la session d'envoyer un message

    ```java
    {{% embed src="./VueFileAttente03.java" indent-level="1" %}}
    ```

    * NOTE: on délègue à la session, parce qu'on veut que le joueur mémorise le `sessionId`


1. Dans `SessionPong`, créer la méthode pour envoyer le message

    ```java
    {{% embed src="./SessionPong01.java" indent-level="1" %}}
    ```
