public class SessionPong extends Session<SessionPong> {

    // ...
    

    public void envoyerMsgAjouterRendezVous() {
        Ntro.newMessage(MsgAjouterRendezVous.class)
            .setPremierJoueur(MaquetteJoueurs.joueurAleatoire(this.sessionId()))
            .send();
    }
