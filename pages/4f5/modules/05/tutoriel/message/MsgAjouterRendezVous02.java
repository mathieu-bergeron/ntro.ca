public class MsgAjouterRendezVous extends Message<MsgAjouterRendezVous> {
    
    // ajouter
    private Joueur premierJoueur;

    public MsgAjouterRendezVous setPremierJoueur(Joueur premierJoueur) {
        this.premierJoueur = premierJoueur;
        
        return this;
    }

    // ...

