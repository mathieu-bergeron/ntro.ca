package pong;

import ca.ntro.app.NtroAppFx;
import ca.ntro.app.backend.BackendRegistrar;
import ca.ntro.app.frontend.FrontendRegistrarFx;
import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;
import pong.dorsal.DorsalPong;
import pong.frontal.FrontalPong;
import pong.messages.MsgAjouterRendezVous;
import pong.modeles.ModeleFileAttente;
import pong.modeles.valeurs.RendezVousComplet;
import pong.modeles.valeurs.RendezVous;


public class ClientPong implements NtroAppFx {
	
	public static void main(String[] args) {
		NtroAppFx.launch(args);
	}

	@Override
	public void registerFrontend(FrontendRegistrarFx registrar) {

		registrar.registerFrontend(FrontalPong.class);

	}

	@Override
	public void registerMessages(MessageRegistrar registrar) {

		registrar.registerMessage(MsgAjouterRendezVous.class);

	}

	@Override
	public void registerModels(ModelRegistrar registrar) {

		registrar.registerModel(ModeleFileAttente.class);

		registrar.registerValue(RendezVous.class);
		registrar.registerValue(RendezVousComplet.class);

	}

	@Override
	public void registerBackend(BackendRegistrar registrar) {

		registrar.registerBackend(new DorsalPong());

	}

}
