import static ca.ntro.app.tasks.backend.BackendTasks.*;

public class ModifierFileAttente {
    
    public static void creerTaches(BackendTasks tasks) {
        
        tasks.taskGroup("ModifierFileAttente")
        
             .waitsFor(model(ModeleFileAttente.class))
        
             .contains(subTasks -> {
                  
                // XXX: ajouter l'appel!
                ajouterRendezVous(subTasks);

              });
    }

    private static void ajouterRendezVous(BackendTasks subTasks) {
        subTasks.task("ajouterRendezVous")

             .waitsFor(model(ModeleFileAttente.class))

             .waitsFor(message(MsgAjouterRendezVous.class))
             
             .executes(inputs -> {

                 MsgAjouterRendezVous msgAjouterRendezVous = inputs.get(message(MsgAjouterRendezVous.class));
                 ModeleFileAttente    fileAttente          = inputs.get(model(ModeleFileAttente.class));

                 // Prêt à ajouter un rendez-vous!
               
             });
    }
}
