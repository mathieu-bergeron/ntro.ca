---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: modifier le modèle dans le dorsal


1. Dans `dorsal.taches` ajouter la classe `ModifierFileAttente`

1. En VSCode, s'assurer d'avoir l'arborescence suivante

    <center>
        <img src="eclipse01.png" />
    </center>


1. Ouvrir `ModifierFileAttente` et ajouter le `import static` suivant:

    * <span style="background-color:orange; padding:10px;border:2px dashed black;"> `import static ca.ntro.app.tasks.backend.BackendTasks.*;` </span>

1. Dans `ModifierFileAttente`, ajouter un groupe de tâches

    ```java
    {{% embed src="./ModifierFileAttente01.java" indent-level="1" %}}
    ```

    * NOTE:
        * le groupe de tâche au complet attend que le modèle soit chargé


1. Ajouter la tâche `ajouterRendezVous`

    ```java
    {{% embed src="./ModifierFileAttente02.java" indent-level="1" %}}
    ```

    * NOTES:
        * la tâche reçoit le message
        * la tâche récupère le modèle

1. Dans `DorsalPong`, appeler `ModifierFileAttente.creerTaches`

    ```java
    {{% embed src="./DorsalPong01.java" indent-level="1" %}}
    ```
1. Exécuter `pong` et vérifier le graphe de tâches pour le Dorsal

        $ sh gradlew pong

    <center>
        <img width="100%" src="backend.png"/>
    </center>

