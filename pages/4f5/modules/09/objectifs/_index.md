---
title: ""
weight: 1
bookHidden: true
---


# Objectifs: version client/serveur


<center>
<div style="background-color:orange;width:80%;border:2px dashed black;padding:10px">
<strong>IMPORTANT</strong>
<div style="text-align:left">
<ul>
<li>Il faut pouvoir ajouter des informations à ma vue personnalisée
<ul>
    <li>dans un premier client, et le deuxième client l'affiche aussi
    <li>dans le deuxième client, et le premier l'affiche aussi
</ul>
</ul>

</div>
</center>

<br>
<br>

{{% animation "/4f5/modules/09/objectifs/objectifs09.mp4" %}}

<br>
<br>



1. Effectuer {{% link "../tutoriel/" "tutoriel" %}} pour comprendre comment

    * transformer mon application en mode client/serveur

1. S'assurer que mes noms d'attribut/méthode sont adéquats **pour mon projet**
    * on ne veut **pas de** `ServeurPong` dans un jeu de dames!

1. Je pousse mon projet sur GitLab, p.ex:

        $ git add .
        $ git commit -a -m module09
        $ git push 

1. Je vérifie que mes fichiers sont sur GitLab

1. Je vérifie que projet est fonctionnel avec un `$ git clone` neuf, p.ex:

    ```bash
    # effacer le répertoire tmp s'il existe

    $ mkdir ~/tmp

    $ cd ~/tmp

    $ git clone https://gitlab.com/USAGER/4f5_prenom_nom

    $ cd 4f5_prenom_nom

    $ sh gradlew mon_projet:serveur

    # Dans un deuxième GitBash
    $ sh gradlew mon_projet:clientBob

    # Dans un troisième GitBash
    $ sh gradlew mon_projet:clientAlice

        # Doit afficher deux clients 
        # Les deux clients doivent afficher la même information
        # Je dois pouvoir ajouter une information dans un client 
        #    de sorte qu'elle est propagée dans l'autre
    ```
