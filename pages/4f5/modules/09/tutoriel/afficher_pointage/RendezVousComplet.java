public class RendezVousComplet extends RendezVous {
    
    private String idPartie;
    private Joueur deuxiemeJoueur;

    private int scorePremierJoueur = 0;
    private int scoreDeuxiemeJoueur = 0;

    public Joueur getDeuxiemeJoueur() {
        return deuxiemeJoueur;
    }

    public void setDeuxiemeJoueur(Joueur deuxiemeJoueur) {
        this.deuxiemeJoueur = deuxiemeJoueur;
    }

    public String getIdPartie() {
        return idPartie;
    }

    public void setIdPartie(String idPartie) {
        this.idPartie = idPartie;
    }

    public int getScorePremierJoueur() {
        return scorePremierJoueur;
    }

    public void setScorePremierJoueur(int scorePremierJoueur) {
        this.scorePremierJoueur = scorePremierJoueur;
    }

    public int getScoreDeuxiemeJoueur() {
        return scoreDeuxiemeJoueur;
    }

    public void setScoreDeuxiemeJoueur(int scoreDeuxiemeJoueur) {
        this.scoreDeuxiemeJoueur = scoreDeuxiemeJoueur;
    }

    public RendezVousComplet() {
        super();
    }

    public RendezVousComplet(String idRendezVous, 
                         Joueur premierJoueur,
                         Joueur deuxiemeJoueur,
                         String idPartie) {

        super(idRendezVous, premierJoueur);

        setIdPartie(idPartie);
        setDeuxiemeJoueur(deuxiemeJoueur);
    }


    @Override
    public FragmentRendezVous creerFragment(ViewLoader<FragmentRendezVous> viewLoaderRendezVous, 
                                            ViewLoader<FragmentRendezVousComplet> viewLoaderRendezVousComplet) {
        
        return viewLoaderRendezVousComplet.createView();
    }

    public void afficherSur(FragmentRendezVous fragmentRendezVous) {
        super.afficherSur(fragmentRendezVous);

        FragmentRendezVousComplet fragmentRendezVousComplet = (FragmentRendezVousComplet) fragmentRendezVous;
        fragmentRendezVousComplet.afficherNomDeuxiemeJoueur(deuxiemeJoueur.getPrenom());
        fragmentRendezVousComplet.afficherScore(scorePremierJoueur + "-" + scoreDeuxiemeJoueur);

    }
    
    @Override
    public String toString() {
        return getPremierJoueur().getPrenom() + " (" + scorePremierJoueur + ")" + " Vs " + deuxiemeJoueur.getPrenom() + " (" + scoreDeuxiemeJoueur + ")";
    }

    @Override
    protected boolean siFragmentDuBonType(FragmentRendezVous fragmentRendezVous) {
        return fragmentRendezVous.getClass().equals(FragmentRendezVousComplet.class);
    }

    public void mettreAJourModelePartie(ModelePartie modelePartie) {
    }

    public void envoyerMsgModifierInfoPartie() {

        MsgModifierInfoPartie msgModifierInfoPartie = Ntro.newMessage(MsgModifierInfoPartie.class);
        
        msgModifierInfoPartie.setIdPartie(idPartie);

        msgModifierInfoPartie.setIdPremierJoueur(getPremierJoueur().getId());
        msgModifierInfoPartie.setIdDeuxiemeJoueur(deuxiemeJoueur.getId());

        msgModifierInfoPartie.setNomPremierJoueur(getPremierJoueur().getPrenom());
        msgModifierInfoPartie.setNomDeuxiemeJoueur(deuxiemeJoueur.getPrenom());

        msgModifierInfoPartie.setScorePremierJoueur(scorePremierJoueur);
        msgModifierInfoPartie.setScoreDeuxiemeJoueur(scoreDeuxiemeJoueur);

        msgModifierInfoPartie.send();

    }

}
