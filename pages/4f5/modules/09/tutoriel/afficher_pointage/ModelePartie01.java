public class ModelePartie implements Model {

    // ...
    

    // ajouter
    public void envoyerMsgModifierPointage() {

        MsgModifierPointage msgModifierPointage = Ntro.newMessage(MsgModifierPointage.class);
        
        msgModifierPointage.setIdRendezVous(idPartie);
        msgModifierPointage.setScorePremierJoueur(scorePremierJoueur);
        msgModifierPointage.setScoreDeuxiemeJoueur(scoreDeuxiemeJoueur);
        
        msgModifierPointage.send();

    }
}
