public class ModifierFileAttente {
    
    public static void creerTaches(BackendTasks tasks) {

        tasks.taskGroup("ModifierFileAttente")

             .waitsFor(model(ModeleFileAttente.class))
        
             .contains(subTasks -> {
                 
                 // ...
                 

                 // ajouter
                 modifierPointage(subTasks);

             });
    }

    // ...
    

    // ajouter
    private static void modifierPointage(BackendTasks subTasks) {
        subTasks.task("modifierPointage")

             .waitsFor(message(MsgModifierPointage.class))

             .executes(inputs -> {

                 MsgModifierPointage msgModifierPointage = inputs.get(message(MsgModifierPointage.class));
                 ModeleFileAttente  fileAttente          = inputs.get(model(ModeleFileAttente.class));

                 msgModifierPointage.appliquerA(fileAttente);

             });
    }
