public class ModifierPartie {
    
    // ...


    // modifier
    private static void ajouterPoint(BackendTasks tasks) {

        tasks.task("ajouterPoint")

             .waitsFor(message(MsgAjouterPoint.class))


             .executes(inputs -> {
                 
                 MsgAjouterPoint   msgAjouterPoint = inputs.get(message(MsgAjouterPoint.class));
                 ModelePartie      partie          = inputs.get(model(ModelePartie.class));
                 
                 if(msgAjouterPoint.estPlusRecentQue(partie)) {

                     // ...

                     // ajouter
                     partie.envoyerMsgModifierPointage();
                 }
             });
    }
