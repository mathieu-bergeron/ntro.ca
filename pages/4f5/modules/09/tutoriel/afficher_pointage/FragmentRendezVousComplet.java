public class FragmentRendezVousComplet extends FragmentRendezVous {
    
    @FXML
    private Label labelNomDeuxiemeJoueur;

    @FXML
    private Label labelScore;

    @Override
    public void initialize() {
        super.initialize();

        Ntro.assertNotNull("labelNomDeuxiemeJoueur", labelNomDeuxiemeJoueur);
        Ntro.assertNotNull(labelScore);
    }

    public void afficherNomDeuxiemeJoueur(String nomDeuxiemeJoueur) {
        labelNomDeuxiemeJoueur.setText(nomDeuxiemeJoueur);
    }

    private void installerEvtAfficherPartie() {

        EvtAfficherPartie evtAfficherPartie = Ntro.newEvent(EvtAfficherPartie.class);
        
        getBoutonOuvrirPartie().setOnAction(evtFx -> {
            
            evtAfficherPartie.trigger();
        });
    }
    
    public void memoriserIdRendezVous(String idRendezVous) {
        super.memoriserIdRendezVous(idRendezVous);
        installerEvtAfficherPartie();
    }

    public void afficherScore(String score) {
        labelScore.setText(score);
    }

}
