---
title: ""
weight: 1
bookHidden: true
---

# Tutoriel: afficher le pointage sur la file d'attente


## Afficher le pointage sur le `FragmentRendezVousComplet`

* Ajouter une zone de texte dans le FXML

    ```xml
    {{% embed src="partie_en_cours01.fxml" indent-level="1" %}}
    ```

* Récupérer le contrôle en Java et ajouter une méthode pour afficher le pointage

    ```java
    {{% embed src="FragmentRendezVousComplet01.java" indent-level="1" %}}
    ```

## Ajouter le pointage à la valeur `RendezVousComplet`

* Ajouter les attributs, les getter/setter, puis modifier `afficherSur`

    ```java
    {{% embed src="RendezVousComplet01.java" indent-level="1" %}}
    ```

## Ajouter le  `MsgModifierPointage`

* Envoyer le message du dorsal à lui-même quand on ajoute un point

* Modifier `ModelePartie`

    ```java
    {{% embed src="ModelePartie01.java" indent-level="1" %}}
    ```

* Modifier `dorsal.taches.ModifierPartie`

    ```java
    {{% embed src="ModifierPartie01.java" indent-level="1" %}}
    ```

* Ajouter la tâche `dorsal.taches.ModifierPartie.modifierPointage`

    ```java
    {{% embed src="ModifierFileAttente01.java" indent-level="1" %}}
    ```

