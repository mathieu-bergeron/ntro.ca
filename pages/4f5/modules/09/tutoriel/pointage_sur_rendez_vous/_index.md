---
title: "Score sur le rendez-vous"
weight: 1
bookHidden: true
---


# Optionnel) ajouter score au rendez-vous


## Créer la méthode `ModeleFileAttente.ajouterScoreAuRendezVous`

1. Créer les méthodes suivantes


    ```java
    {{% embed src="./ModeleFileAttente01.java" indent-level="1" %}}
    ```


## Créer le `MsgAjouterScoreAuRendezVous`

1. Dans `messages`, créer la classe suivante

    ```java
    {{% embed src="./MsgAjouterScoreAuRendezVous01.java" indent-level="1" %}}
    ```

## Créer la méthode `ModelePartie.envoyerMsgAjouterScoreAuRendezVous `

1. Créer les méthodes suivantes:

    ```java
    {{% embed src="./ModelePartie01.java" indent-level="1" %}}
    ```

## Quand on ajoute un point, ajouter le score au rendez-vous

1. Modifier la tâche `ajouterPoint` et créer la tàche `envoyerMsgAjouterScoreAuRendezVous`

    ```java
    {{% embed src="./ModifierPartie01.java" indent-level="1" %}}
    ```

1. Le graphe de tâche du dorsal devrait maintenant contenir:

    <img src="backend01.png"/>

