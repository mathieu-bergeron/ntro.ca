public class ModifierPartie {
    
    public static void creerTaches(BackendTasks tasks) {
        
        tasks.taskGroup("ModifierPartie")
        
             .waitsFor(model(ModelePartie.class))
        
              .contains(subTasks -> {


                    // ...
                  
                    // modifier
                    ajouterPoint(subTasks);
                    
                    // ajouter
                    envoyerMsgAjouterScoreAuRendezVous(subTasks);

              });
    }

    private static void ajouterPoint(BackendTasks tasks) {

        tasks.task("ajouterPoint")

             .waitsFor(model(ModelePartie.class))

             .waitsFor(message(MsgAjouterPoint.class))

             // modifier
             .executesAndReturnsValue(inputs -> {
                 
                 MsgAjouterPoint msgAjouterPoint = inputs.get(message(MsgAjouterPoint.class));
                 ModelePartie    partie          = inputs.get(model(ModelePartie.class));
                 
                 msgAjouterPoint.ajouterPointA(partie);

                 
                 // ajouter
                 return partie;
             });
    }

    // ajouter
    private static void envoyerMsgAjouterScoreAuRendezVous(BackendTasks subTasks) {

        subTasks.task("envoyerMsgAjouterScoreAuRendezVous")

                .waitsFor(model(ModelePartie.class))

                .waitsFor("ajouterPoint")

                .executes(inputs -> {
                 
                    ModelePartie    partie          = inputs.get("ajouterPoint");

                    partie.envoyerMsgAjouterScoreAuRendezVous();

             });
    }

