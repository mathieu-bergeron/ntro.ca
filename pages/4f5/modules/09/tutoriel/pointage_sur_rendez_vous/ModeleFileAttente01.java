public class ModeleFileAttente implements Model, WatchJson, WriteObjectGraph {

    // ...
    

    // ajouter
    private RendezVous rendezVousParId(String idRendezVous) {
        RendezVous rendezVous = null;
        
        for(RendezVous candidat : rendezVousDansOrdre) {
            if(candidat.siIdEst(idRendezVous)) {
                rendezVous = candidat;
                break;
            }
        }

        return rendezVous;
    }

    // ajouter
    public void ajouterScoreAuRendezVous(String idRendezVous, 
                                         Map<Position, Integer> scoreParPosition) {

        RendezVous rendezVous = rendezVousParId(idRendezVous);
        
        if(rendezVous instanceof RendezVousComplet) {
            
            RendezVousComplet rendezVousComplet = (RendezVousComplet) rendezVous;
            
            rendezVousComplet.ajouterScore(scoreParPosition);
        }
        
    }

}
