public class ModelePartie implements Model, Watch, WriteObjectGraph  {
    

    // ...
    

    // ajouter
    private Map<Position, Integer> creerScoreParPosition() {
        Map<Position, Integer> scoreParPosition = new HashMap<>();
        
        for(Position position : infoJoueurParPosition.keySet()) {
            InfoJoueur infoJoueur = infoJoueurParPosition.get(position);
            infoJoueur.copierScoreDans(scoreParPosition, position);
        }
        
        return scoreParPosition;
    }


    // ajouter
    public void envoyerMsgAjouterScoreAuRendezVous() {
        Ntro.newMessage(MsgAjouterScoreAuRendezVous.class)
            .setModelSelection(ModeleFileAttente.class, idRegion(idRendezVous))
            .setIdRendezVous(idRendezVous)
            .setScoreParPosition(creerScoreParPosition())
            .send();
    }
