public class SessionPong extends Session<SessionPong> {
    
    public SessionPong diffuserMsgActionAutreJoueur(EvtActionJoueur evtActionJoueur) {
        evtActionJoueur.diffuserMsgActionAutreJoueur();

        return this;
    }
