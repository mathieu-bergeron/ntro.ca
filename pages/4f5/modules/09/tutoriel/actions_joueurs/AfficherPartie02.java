public class AfficherPartie {

    public static void creerTaches(FrontendTasks tasks) {
        
        creerDonneesVuePartie(tasks);
        
        tasks.taskGroup("AfficherPartie")

             .waitsFor(created(DonneesVuePartie.class))

             .contains(subTasks -> {

                // ...

                // ajouter
                reagirActionAutreJoueur(subTasks);

             });
    }

    // ...

    private static void reagirActionAutreJoueur(FrontendTasks subTasks) {
        subTasks.task("reagirActionAutreJoueur")

                .waitsFor(created(DonneesVuePartie.class))

                .waitsFor(message(MsgActionAutreJoueur.class))
             
                .executes(inputs -> {

                    DonneesVuePartie        donneesVuePartie     = inputs.get(created(DonneesVuePartie.class));
                    MsgActionAutreJoueur    msgActionAutreJoueur = inputs.get(message(MsgActionAutreJoueur.class));
                    
                    msgActionAutreJoueur.appliquerA(donneesVuePartie);

             });
    }

