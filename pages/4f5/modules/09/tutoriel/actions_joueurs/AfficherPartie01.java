public class AfficherPartie {


    //...

    private static void reagirActionJoueur(FrontendTasks subTasks) {

        subTasks.task("reagirActionJoueur")

                // ...

                // s'assurer d'avoir
                .waitsFor(EvtActionJoueur.class)
                
                .executes(inputs -> {
                    
                    // ...

                    // s'assurer d'avoir
                    EvtActionJoueur    evtActionJoueur  = inputs.get(EvtActionJoueur.class);

                    // ...
                    
                    // ajouter
                    evtActionJoueur.diffuserMsgActionAutreJoueur();

                });
    }
