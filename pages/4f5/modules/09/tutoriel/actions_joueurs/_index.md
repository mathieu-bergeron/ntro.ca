---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: propager les actions joueurs

1. Créer le message `MsgActionAutreJoueur`

    ```java
    {{% embed src="./MsgActionAutreJoueur.java" indent-level="1" %}}
    ```

1. **Déclarer** le message dans `Declarations`

    ```java
    {{% embed src="./Declarations.java" indent-level="1" %}}
    ```


1. Dans `AfficherPartie`, ajouter ce code

    ```java
    {{% embed src="./AfficherPartie01.java" indent-level="1" %}}
    ```

1. Créer la méthode `EvtActionJoueur.diffuserMsgActionAutreJoueur`

    ```java
    {{% embed src="./EvtActionJoueur.java" indent-level="1" %}}
    ```

1. Dans `AfficherPartie`, ajouter ce code

    ```java
    {{% embed src="./AfficherPartie02.java" indent-level="1" %}}
    ```


1. Vérifier que ça fonctionne

        $ sh gradlew pong:serveur
        $ sh gradlew pong:clientAlice
        $ sh gradlew pong:clientBob
