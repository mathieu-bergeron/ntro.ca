public class EvtActionJoueur extends Event {

    // ... 
    
    // ajouter
    public void diffuserMsgActionAutreJoueur() {
        Ntro.newMessage(MsgActionAutreJoueur.class)
            .setPosition(position)
            .setAction(action)
            .broadcast();
    }


}
