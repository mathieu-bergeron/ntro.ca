public class MsgModifierInfoPartie extends MessageNtro {
    
    private String idPartie;

    private String idPremierJoueur = "";
    private String idDeuxiemeJoueur = "";

    private String nomPremierJoueur = "Alice";
    private String nomDeuxiemeJoueur = "Bob";

    private int scorePremierJoueur = 0;
    private int scoreDeuxiemeJoueur = 0;

    public String getIdPremierJoueur() {
        return idPremierJoueur;
    }

    public void setIdPremierJoueur(String idPremierJoueur) {
        this.idPremierJoueur = idPremierJoueur;
    }

    public String getIdDeuxiemeJoueur() {
        return idDeuxiemeJoueur;
    }

    public void setIdDeuxiemeJoueur(String idDeuxiemeJoueur) {
        this.idDeuxiemeJoueur = idDeuxiemeJoueur;
    }

    public String getIdPartie() {
        return idPartie;
    }

    public void setIdPartie(String idPartie) {
        this.idPartie = idPartie;
    }

    public String getNomPremierJoueur() {
        return nomPremierJoueur;
    }

    public void setNomPremierJoueur(String nomPremierJoueur) {
        this.nomPremierJoueur = nomPremierJoueur;
    }

    public String getNomDeuxiemeJoueur() {
        return nomDeuxiemeJoueur;
    }

    public void setNomDeuxiemeJoueur(String nomDeuxiemeJoueur) {
        this.nomDeuxiemeJoueur = nomDeuxiemeJoueur;
    }

    public int getScorePremierJoueur() {
        return scorePremierJoueur;
    }

    public void setScorePremierJoueur(int scorePremierJoueur) {
        this.scorePremierJoueur = scorePremierJoueur;
    }

    public int getScoreDeuxiemeJoueur() {
        return scoreDeuxiemeJoueur;
    }

    public void setScoreDeuxiemeJoueur(int scoreDeuxiemeJoueur) {
        this.scoreDeuxiemeJoueur = scoreDeuxiemeJoueur;
    }
    
    public void appliquerA(ModelePartie partie) {
        partie.setIdPartie(idPartie);

        partie.setIdPremierJoueur(idPremierJoueur);
        partie.setIdDeuxiemeJoueur(idDeuxiemeJoueur);

        partie.setNomPremierJoueur(nomPremierJoueur);
        partie.setNomDeuxiemeJoueur(nomDeuxiemeJoueur);

        partie.setScorePremierJoueur(scorePremierJoueur);
        partie.setScoreDeuxiemeJoueur(scoreDeuxiemeJoueur);
    }


}
