public class MsgCreerRendezVousComplet extends MessageNtro {
    
    private String idRendezVous;
    private Joueur deuxiemeJoueur;

    public String getIdRendezVous() {
        return idRendezVous;
    }

    public void setIdRendezVous(String idRendezVous) {
        this.idRendezVous = idRendezVous;
    }

    public Joueur getDeuxiemeJoueur() {
        return deuxiemeJoueur;
    }

    public void setDeuxiemeJoueur(Joueur deuxiemeJoueur) {
        this.deuxiemeJoueur = deuxiemeJoueur;
    }

    public RendezVousComplet creerRendezVousComplet(ModeleFileAttente fileAttente) {
        return fileAttente.creerRendezVousComplet(idRendezVous, deuxiemeJoueur);
    }

    public void miseAJourPartie(ModelePartie modelePartie) {
        modelePartie.setNomDeuxiemeJoueur(deuxiemeJoueur.getPrenom());
    }
}
