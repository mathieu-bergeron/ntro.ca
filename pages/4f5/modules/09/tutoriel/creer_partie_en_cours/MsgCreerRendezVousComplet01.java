public class MsgCreerRendezVousComplet extends MessageNtro {
    
    private String idRendezVous;
    private Joueur deuxiemeJoueur;

    // générer les setter/getter

    public RendezVousComplet creerRendezVousComplet(ModeleFileAttente fileAttente) {
        return fileAttente.creerRendezVousComplet(idRendezVous, deuxiemeJoueur);
    }

    public void miseAJourPartie(ModelePartie modelePartie) {
        modelePartie.setNomDeuxiemeJoueur(deuxiemeJoueur.getPrenom());
    }
}
