---
title: ""
weight: 1
bookHidden: true
---


# Optionnel: ajouter les bons joueurs à la partie

## Ajouter les méthodes pour créer la partie en cours

* Modifier `ModeleFileAttente`

    ```java
    {{% embed src="ModeleFileAttente01.java" indent-level="1" %}}
    ```


## Ajouter le message `MsgCreerRendezVousComplet` et `MsgModifierInfoPartie`

* Créer `MsgCreerRendezVousComplet`

    ```java
    {{% embed src="MsgCreerRendezVousComplet01.java" indent-level="1" %}}
    ```

* Créer `MsgModifierInfoPartie`

    ```java
    {{% embed src="MsgModifierInfoPartie01.java" indent-level="1" %}}
    ```

## Ajouter la tâche `ModifierPartie.modifierInfoPartie`

* Dans `dorsal.taches.ModifierPartie`

    ```java
    {{% embed src="ModifierPartie01.java" indent-level="1" %}}
    ```

## Ajouter la tâche `ModifierFileAttente.creerRendezVousComplet`

* Dans `dorsal.taches.ModifierFileAttente`

    ```java
    {{% embed src="ModifierFileAttente01.java" indent-level="1" %}}
    ```

    * NOTE: on envoi un message du dorsal à lui-même avec ce code:

        ```java
        {{% embed src="RendezVousComplet.java" indent-level="2" %}}
        ```

## Envoyer `MsgCreerRendezVousComplet` à partir du frontal

* Dans le `FragmentRendezVous`, envoyer le message quand l'usager veut joindre la partie

    ```java
    {{% embed src="FragmentRendezVous.java" indent-level="1" %}}
    ```

## Envoyer `MsgModifierInfoPartie` du dorsal à lui-même

* Déjà fait ci-haut


