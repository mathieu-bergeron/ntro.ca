public class ModifierFileAttente {
    
    public static void creerTaches(BackendTasks tasks) {

        tasks.taskGroup("ModifierFileAttente")

             .waitsFor(model(ModeleFileAttente.class))
        
             .contains(subTasks -> {

                 // ...
                 
                 // ajouter
                 creerRendezVousComplet(subTasks);

             });
    }

    // ajouter
    private static void creerRendezVousComplet(BackendTasks subTasks) {
        subTasks.task("creerRendezVousComplet")

             .waitsFor(message(MsgCreerRendezVousComplet.class))

             .executes(inputs -> {

                 MsgCreerRendezVousComplet msgCreerRendezVousComplet = inputs.get(message(MsgCreerRendezVousComplet.class));
                 ModeleFileAttente     fileAttente           = inputs.get(model(ModeleFileAttente.class));
                 
                 RendezVousComplet partieEnCours = msgCreerRendezVousComplet.creerRendezVousComplet(fileAttente);
                 
                 partieEnCours.envoyerMsgModifierInfoPartie();

             });
    }
