public class MsgModifierInfoPartie extends MessageNtro {
    
    private String idPartie;

    private String idPremierJoueur = "";
    private String idDeuxiemeJoueur = "";

    private String nomPremierJoueur = "Alice";
    private String nomDeuxiemeJoueur = "Bob";

    private int scorePremierJoueur = 0;
    private int scoreDeuxiemeJoueur = 0;

    // générer les getter/setter
    


    // ajouter
    public void appliquerA(ModelePartie partie) {
        partie.setIdPartie(idPartie);

        partie.setIdPremierJoueur(idPremierJoueur);
        partie.setIdDeuxiemeJoueur(idDeuxiemeJoueur);

        partie.setNomPremierJoueur(nomPremierJoueur);
        partie.setNomDeuxiemeJoueur(nomDeuxiemeJoueur);

        partie.setScorePremierJoueur(scorePremierJoueur);
        partie.setScoreDeuxiemeJoueur(scoreDeuxiemeJoueur);
    }
