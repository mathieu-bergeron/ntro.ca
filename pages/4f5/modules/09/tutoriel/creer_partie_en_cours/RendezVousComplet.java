public class RendezVousComplet extends RendezVous {

    // ...
    



    // ajouter
    public void envoyerMsgModifierInfoPartie() {

        MsgModifierInfoPartie msgModifierInfoPartie = Ntro.newMessage(MsgModifierInfoPartie.class);
        
        msgModifierInfoPartie.setIdPartie(idPartie);

        msgModifierInfoPartie.setIdPremierJoueur(getPremierJoueur().getId());
        msgModifierInfoPartie.setIdDeuxiemeJoueur(deuxiemeJoueur.getId());

        msgModifierInfoPartie.setNomPremierJoueur(getPremierJoueur().getPrenom());
        msgModifierInfoPartie.setNomDeuxiemeJoueur(deuxiemeJoueur.getPrenom());

        msgModifierInfoPartie.setScorePremierJoueur(scorePremierJoueur);
        msgModifierInfoPartie.setScoreDeuxiemeJoueur(scoreDeuxiemeJoueur);

        msgModifierInfoPartie.send();

    }
