public class ModifierFileAttente {
    
    public static void creerTaches(BackendTasks tasks) {

        tasks.taskGroup("ModifierFileAttente")

             .waitsFor(model(ModeleFileAttente.class))
        
             .contains(subTasks -> {
                 
                 ajouterRendezVous(subTasks);
                 
                 creerRendezVousComplet(subTasks);

                 retirerRendezVous(subTasks);

                 modifierPointage(subTasks);

             });
    }

    private static void ajouterRendezVous(BackendTasks subTasks) {
        subTasks.task("ajouterRendezVous")

             .waitsFor(model(ModeleFileAttente.class))

             .waitsFor(message(MsgAjouterRendezVous.class))
             
             .executesAndReturnsValue(inputs -> {

                 MsgAjouterRendezVous msgAjouterRendezVous = inputs.get(message(MsgAjouterRendezVous.class));
                 ModeleFileAttente    fileAttente          = inputs.get(model(ModeleFileAttente.class));

                 return msgAjouterRendezVous.ajouterA(fileAttente);
             });
    }

    private static void creerRendezVousComplet(BackendTasks subTasks) {
        subTasks.task("creerRendezVousComplet")

             .waitsFor(message(MsgCreerRendezVousComplet.class))

             .executes(inputs -> {

                 MsgCreerRendezVousComplet msgCreerRendezVousComplet = inputs.get(message(MsgCreerRendezVousComplet.class));
                 ModeleFileAttente     fileAttente           = inputs.get(model(ModeleFileAttente.class));
                 
                 RendezVousComplet partieEnCours = msgCreerRendezVousComplet.creerRendezVousComplet(fileAttente);
                 
                 partieEnCours.envoyerMsgModifierInfoPartie();

             });
    }

    private static void modifierPointage(BackendTasks subTasks) {
        subTasks.task("modifierPointage")

             .waitsFor(message(MsgModifierPointage.class))

             .executes(inputs -> {

                 MsgModifierPointage msgModifierPointage = inputs.get(message(MsgModifierPointage.class));
                 ModeleFileAttente  fileAttente          = inputs.get(model(ModeleFileAttente.class));

                 msgModifierPointage.appliquerA(fileAttente);

             });
    }



    private static void retirerRendezVous(BackendTasks subTasks) {
        subTasks.task("retirerRendezVous")

             .waitsFor(model(ModeleFileAttente.class))

             .waitsFor(message(MsgRetirerRendezVous.class))
             
             .executesAndReturnsValue(inputs -> {

                 MsgRetirerRendezVous msgRetirerRendezVous = inputs.get(message(MsgRetirerRendezVous.class));
                 ModeleFileAttente    fileAttente          = inputs.get(model(ModeleFileAttente.class));

                 return msgRetirerRendezVous.retirerDe(fileAttente);
             });

    }
}
