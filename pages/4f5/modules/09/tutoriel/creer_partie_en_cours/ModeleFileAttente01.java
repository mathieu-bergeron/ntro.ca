public class ModeleFileAttente implements Model {

    // ...


    // ajouter
    public RendezVousComplet creerRendezVousComplet(String idRendezVous, Joueur deuxiemeJoueur) {
        RendezVous rendezVous = rendezVousParId(idRendezVous);
        RendezVousComplet partieEnCours = null;
        
        if(rendezVous != null) {
            
            int indiceRendezVous = rendezVousDansOrdre.indexOf(rendezVous);
            rendezVousDansOrdre.remove(indiceRendezVous);

            partieEnCours = rendezVous.creerRendezVousComplet(deuxiemeJoueur, idRendezVous);

            rendezVousDansOrdre.add(indiceRendezVous, partieEnCours);

        }

        return partieEnCours;
    }

    // ajouter
    private RendezVous rendezVousParId(String idRendezVous) {
        RendezVous rendezVous = null;
        
        for(RendezVous candidat : rendezVousDansOrdre) {
            if(candidat.getId().equals(idRendezVous)) {
                rendezVous = candidat;
                break;
            }
        }

        return rendezVous;
    }
