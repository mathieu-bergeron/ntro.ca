public class ModifierPartie {
    
    public static void creerTaches(BackendTasks tasks) {
        
        tasks.taskGroup("ModifierPartie")
        
             .waitsFor(model(ModelePartie.class))
        
              .contains(subTasks -> {

                    // ...
                    
                    // ajouter
                    modifierInfoPartie(subTasks);
                    
              });
    }

    // ajouter
    private static void modifierInfoPartie(BackendTasks tasks) {

        tasks.task("modifierInfoPartie")

             .waitsFor(message(MsgModifierInfoPartie.class))

             .executes(inputs -> {
                 
                 MsgModifierInfoPartie msgModifierInfoPartie = inputs.get(message(MsgModifierInfoPartie.class));
                 ModelePartie          partie                = inputs.get(model(ModelePartie.class));
                 
                 msgModifierInfoPartie.appliquerA(partie);

             });
    }
