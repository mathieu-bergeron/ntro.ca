public class ModeleFileAttente implements Model {

    private long prochainIdRendezVous = 1;

    private List<RendezVous> rendezVousDansOrdre = new ArrayList<>();

    public long getProchainIdRendezVous() {
        return prochainIdRendezVous;
    }

    public void setProchainIdRendezVous(long prochainIdRendezVous) {
        this.prochainIdRendezVous = prochainIdRendezVous;
    }

    public List<RendezVous> getLesRendezVous() {
        return rendezVousDansOrdre;
    }

    public void setLesRendezVous(List<RendezVous> rendezVousDansOrdre) {
        this.rendezVousDansOrdre = rendezVousDansOrdre;
    }


    public ModeleFileAttente() {
    }


    public String ajouterRendezVous(Joueur premierJoueur) {
        String idRendezVous = genererIdRendezVous();
        RendezVous rendezVous = new RendezVous(idRendezVous, premierJoueur);

        rendezVousDansOrdre.add(rendezVous);
        
        return idRendezVous;
    }

    private String genererIdRendezVous() {
        String idRendezVous = String.valueOf(prochainIdRendezVous);
        prochainIdRendezVous++;

        return idRendezVous;
    }
    

    public void afficherSur(VueFileAttente vueFileAttente) {
        
        vueFileAttente.viderListeRendezVous();
        
        for(RendezVous rendezVous : rendezVousDansOrdre) {
            
            vueFileAttente.ajouterRendezVous(rendezVous);
        }
    }
    
    public String toString() {

        StringBuilder builder = new StringBuilder();
        int numeroRendezVous = 1;
        
        for(RendezVous rendezVous : rendezVousDansOrdre) {

            builder.append(numeroRendezVous);
            builder.append(". ");
            builder.append(rendezVous.toString());
            builder.append("\n");

            numeroRendezVous++;
        }

        return builder.toString();
    }

    public RendezVousComplet creerRendezVousComplet(String idRendezVous, Joueur deuxiemeJoueur) {
        RendezVous rendezVous = rendezVousParId(idRendezVous);
        RendezVousComplet partieEnCours = null;
        
        if(rendezVous != null) {
            
            int indiceRendezVous = rendezVousDansOrdre.indexOf(rendezVous);
            rendezVousDansOrdre.remove(indiceRendezVous);

            partieEnCours = rendezVous.creerRendezVousComplet(deuxiemeJoueur, idRendezVous);

            rendezVousDansOrdre.add(indiceRendezVous, partieEnCours);

        }

        return partieEnCours;
    }


    private RendezVous rendezVousParId(String idRendezVous) {
        RendezVous rendezVous = null;
        
        for(RendezVous candidat : rendezVousDansOrdre) {
            if(candidat.siIdEst(idRendezVous)) {
                rendezVous = candidat;
                break;
            }
        }

        return rendezVous;
    }

    public void retirerRendezVous(String idRendezVous) {
        int indiceRendezVous = -1;
        
        for(int i = 0; i < rendezVousDansOrdre.size(); i++) {
            if(rendezVousDansOrdre.get(i).estLeRendezVous(idRendezVous)) {
                indiceRendezVous = i;
                break;
            }
        }
        
        if(indiceRendezVous >= 0) {
            rendezVousDansOrdre.remove(indiceRendezVous);
        }
    }

    public List<RendezVousComplet> partiesEnCours() {
        List<RendezVousComplet> partiesEnCours = new ArrayList<>();

        for(RendezVous rendezVous : rendezVousDansOrdre) {
            if(rendezVous instanceof RendezVousComplet) {
                partiesEnCours.add((RendezVousComplet) rendezVous);
            }
        }
        
        return partiesEnCours;
    }

    public boolean siContientRendezVous(String idRendezVous) {
        boolean siContient = false;

        for(RendezVous candidat : rendezVousDansOrdre) {
            if(candidat.siIdEst(idRendezVous)) {
                siContient = true;
                break;
            }
        }

        return siContient;
    }

    public void modifierPointagePour(String idRendezVous, 
                                       int scorePremierJoueur, 
                                       int scoreDeuxiemeJoueur) {

        RendezVous rendezVous = rendezVousParId(idRendezVous);
        
        if(rendezVous instanceof RendezVousComplet) {
            RendezVousComplet partieEnCours = (RendezVousComplet) rendezVous;
            
            partieEnCours.setScorePremierJoueur(scorePremierJoueur);
            partieEnCours.setScoreDeuxiemeJoueur(scoreDeuxiemeJoueur);
        }
    }



}
