public class FragmentRendezVous extends ViewFx {

    // ...


    // ajouter
    private void installerMsgJoindrePartie(String idRendezVous) {
        MsgCreerRendezVousComplet msgJoindrePartie = Ntro.newMessage(MsgCreerRendezVousComplet.class);

        boutonJoindrePartie.setOnAction(evtFx -> {
            
            msgJoindrePartie.setIdRendezVous(idRendezVous);
            msgJoindrePartie.setDeuxiemeJoueur(MaquetteSession.usagerCourant());
            
            msgJoindrePartie.send();
        });
    }

    // modifier
    public void memoriserIdRendezVous(String idRendezVous) {
        // ...

        // ajouter
        installerMsgJoindrePartie(idRendezVous);
    }
