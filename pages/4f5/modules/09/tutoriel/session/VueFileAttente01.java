public class VueFileAttente extends ViewFx {

    // ...

    private void installerMsgAjouterRendezVous() {

        MsgAjouterRendezVous msgAjouterRendezVous = Ntro.newMessage(MsgAjouterRendezVous.class);

        boutonAjouterRendezVous.setOnAction(evtFx -> {

            // modifier pour utiliser Session.nomJoueur
            msgAjouterRendezVous.setNomPremierJoueur(Session.nomJoueur);
            msgAjouterRendezVous.send();

        });
    }
