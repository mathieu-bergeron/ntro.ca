---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: simuler la session

## Ajouter `LES_DEUX` à l'enum `Cadran`

```java
{{% embed src="Cadran.java" indent-level="0" %}}
```


## Ajouter `MaquetteSession` et modifier `MaquetteJoueurs`

1. Modifier `MaquetteJoueurs` comme suit:

    ```java
    {{% embed src="MaquetteJoueurs01.java" indent-level="1" %}}
    ```

1. Dans `pong.maquettes`, créer la classe `MaquetteSession`

    ```java
    {{% embed src="MaquetteSession01.java" indent-level="1" %}}
    ```

## Dans le `ClientPong`, initialiser la `MaquetteSession`


```java
{{% embed src="ClientPong01.java" indent-level="0" %}}
```

## Utiliser Gradle pour simuler des usagers

1. Dans `pong/build.gradle`, ajouter `alice`

    ```groovy
    task(alice, dependsOn: 'classes', type: JavaExec) {
       configure clientConfig
       args("aaaa", "Alice","Yi")
    }
    ```

1. Dans `pong/build.gradle`, ajouter `bob`

    ```groovy
    task(bob, dependsOn: 'classes', type: JavaExec) {
       configure clientConfig
       args("bbbb", "Bob","Castillo")
    }
    ```

1. Dans `pong/build.gradle`, ajouter `charlie`

    ```groovy
    task(charlie, dependsOn: 'classes', type: JavaExec) {
       configure clientConfig
       args("cccc", "Charlie","Ahmadi")
    }
    ```

## Vérifier que ça fonctionne

1. Dans **4 GitBash différents**, exécuter

        $ sh gradlew pong:serveur
        $ sh gradlew pong:alice
        $ sh gradlew pong:bob
        $ sh gradlew pong:charlie

1. Ajouter des rendez-vous et vérifier les noms


    <center>
     <video width="100%" src="sessions.mp4" type="video/mp4" loop nocontrols autoplay>
    </center>



