---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: client/serveur

<center>
    <video width="100%" src="client_serveur.mp4" type="video/mp4" loop nocontrols autoplay>
</center>


## Étapes obligatoires

1. {{% link "./trois_programmes/" "Créer la version client/serveur" %}}

1. {{% link "./actions_joueurs/" "Propager les actions des joueurs" %}}

## Étapes optionnelles

Deux exemples de message du dorsal à lui-même (modifier un deuxième modèle)

1. {{% link "./creer_partie/" "Ajouter les bons joueurs à la partie" %}}

1. {{% link "./pointage_sur_rendez_vous/" "Afficher le score sur le rendez-vous" %}}



