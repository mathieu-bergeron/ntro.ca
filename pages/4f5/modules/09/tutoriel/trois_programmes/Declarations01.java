public class Declarations {

    public static void declarerMessages(MessageRegistrar registrar) {

        registrar.registerMessage(MsgAjouterRendezVous.class);
        registrar.registerMessage(MsgRetirerRendezVous.class);
        registrar.registerMessage(MsgAjouterPoint.class);
    }

    public static void declarerModeles(ModelRegistrar registrar) {

        registrar.registerModel(ModeleFileAttente.class);
        registrar.registerValue(RendezVous.class);
        registrar.registerValue(RendezVousComplet.class);
        
        registrar.registerModel(ModelePartie.class);
    }
}
