public class ServeurPong implements NtroServerFx {
    
    public static void main(String[] args) {
        NtroServerFx.launch(args);
    }

    @Override
    public void registerMessages(MessageRegistrar registrar) {
        Declarations.declarerMessages(registrar);
    }

    @Override
    public void registerModels(ModelRegistrar registrar) {
        Declarations.declarerModeles(registrar);
    }

    @Override
    public void registerServer(ServerRegistrarJdk registrar) {
        Declarations.declarerServeur(registrar);
    }

    @Override
    public void registerBackend(BackendRegistrar registrar) {
        registrar.registerBackend(DorsalPong.class);
    }

}
