public class ClientPong implements NtroClientFx {

    public static void main(String[] args) {
        NtroClientFx.launch(args);
    }

    @Override
    public void registerFrontend(FrontendRegistrarFx registrar) {
        registrar.registerFrontend(FrontalPong.class);
    }

    @Override
    public void registerModels(ModelRegistrar registrar) {
        Declarations.declarerModeles(registrar);
    }

    @Override
    public void registerMessages(MessageRegistrar registrar) {
        Declarations.declarerMessages(registrar);
    }

    // XXX: utiliser un dorsal distant
    @Override
    public void registerBackend(BackendRegistrar registrar) {
        registrar.registerBackend(DorsalPongDistant.class);
    }

}
