---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: ajouter la version client/serveur

## Regrouper les déclarations communes

1. Dans le paquet `commun`, créer la classe `Declarations`

1. S'assurer d'avoir l'arborescence suivante:

    <center>
        <img src="eclipse01.png"/>
    </center>

1. Ouvrir `AppPong` dans le but d'en extraire les déclarations
    * on va les transférer dans des méthodes statiques de `Declarations`

### Extraire la déclaration des messages

1. Dans `AppPong`, sélectionner l'intérieur de la méthode `registerMessages`

1. Avec {{% key "Shift+Alt+M" %}}, extraire une méthode pour ce code

1. Nommer cette méthode `declarerMessages`, cocher `public`, puis *OK*

    <center>
        <img src="eclipse02.png"/>
    </center>

1. Ajuster la signature de la nouvelle méthode `declarerMessage` pour qu'elle soit `public static`

    ```java
    {{% embed src="./Declarations01.java" first-line="3" last-line="3" indent-level="1" %}}
    ```

1. Placer mon curseur sur la nouvelle méthode `declarerMessage`

1. Avec {{% key "Shift+Alt+V" %}}, déplacer la méthode dans `Declarations`

    <center>
        <img src="eclipse03.png"/>
    </center>

### Extraire la déclaration des modèles

1. Dans `AppPong`, sélectionner l'intérieur de la méthode `registerModels`

1. Avec {{% key "Shift+Alt+M" %}}, extraire une méthode pour ce code

1. Nommer cette méthode `declarerModeles`, cocher `public`, puis *OK*

    <center>
        <img src="eclipse04.png"/>
    </center>

1. Ajuster la signature de la nouvelle méthode `declarerModeles` pour qu'elle soit `public static`

    ```java
    {{% embed src="./Declarations01.java" first-line="10" last-line="10" indent-level="1" %}}
    ```

1. Placer mon curseur sur la nouvelle méthode `declarerModeles`

1. Avec {{% key "Shift+Alt+V" %}}, déplacer la méthode dans `Declarations`

    <center>
        <img src="eclipse03.png"/>
    </center>

### Vérifier

1. Je devrais maintenant avoir à peu près ce code dans `AppPong`

    ```java
    {{% embed src="./ClientPong01.java" indent-level="1" %}}
    ```

1. Je devrais maintenant avoir à peu près ce code dans `Declarations`

    ```java
    {{% embed src="./Declarations01.java" indent-level="1" %}}
    ```

1. Je peux encore exécuter l'application

    ```bash
    $ sh gradlew pong
    ```


## Créer la version client/serveur

1. Dans `dorsal`, créer la classe `DorsalPongDistant`

1. S'assurer d'avoir l'arborescence suivante:

    <center>
        <img src="eclipse05.png"/>
    </center>

1. Ouvrir `DorsalPongDistant` et ajouter ce code:

    ```java
    {{% embed src="./DorsalPongDistant01.java" indent-level="1" %}}
    ```

1. Avec {{% key "Ctrl+1" %}}, créer la méthode `Declarations.declarerServeur`

1. Dans `Declarations`, compléter le code de cette méthode

    ```java
    {{% embed src="./Declarations02.java" indent-level="1" %}}
    ```

1. Dans le paquet `pong`, créer la classe `ClientPong`

1. S'assurer d'avoir l'arborescence suivante:

    <center>
        <img src="eclipse05a.png"/>
    </center>

1. Le code de `ClientPong` doit être comme celui de `AppPong`, sauf:

    ```java
    {{% embed src="./ClientPong02.java" indent-level="1" %}}
    ```

1. Exécuter le client (devrais échouer sur une erreur de connexion)

    ```bash
    $ sh gradlew pong:client

        # Devrait quitté sur une erreur
        [FATAL] failed to connect to ws://localhost:8002


        [INFO] Exiting
    ```

    <img class="figure" src="connexion_refusee.png"/>

## Créer le serveur

1. Dans `pong`, ajouter la classe `ServeurPong`

1. S'assurer d'avoir l'arborescence suivante:

    <center>
        <img src="eclipse06.png"/>
    </center>

1. Ouvrir `ServeurPong` ajouter le code suivant

    ```java
    {{% embed src="./ServeurPong01.java" indent-level="1" %}}
    ```

    * NOTES
        * c'est maintenant le serveur qui exécute le `DorsalPong` «normal»

## Exécuter le serveur

1. Exécuter le serveur

    ```bash
    sh gradlew pong:serveur

    # devrait afficher
    [INFO] App running. Press Enter here to close App
    [INFO] Listening on ws://localhost:8002
    ```

## Exécuter en mode client/serveur

1. Avec le serveur qui roule, ouvrir un autre GitBash pour faire

    ```bash
    $ sh gradlew pong:clientAlice

    # devrait maintenant afficher
    [INFO] connected to ws://localhost:8002
    ```

1. Avec le serveur et un premier client qui roulent, ouvrir un troisème GitBash pour faire

    ```bash
    $ sh gradlew pong:clientBob
    ```

1. Je devrais déjà voir que `ModeleFileAttente` est synchronisé via la serveur
    
    <center>
     <video width="100%" src="client_serveur.mp4" type="video/mp4" loop nocontrols autoplay>
    </center>

    * NOTES
        * toujours utiliser plusieurs GitBash pour lancer plusieurs programmes
        * attendre que le serveur soit bien lancé avant de lancer un client
        * attendre qu'un soit bien lancé avant d'en lancer un autre

1. En cas d'erreur du type `java.net.BindException: Adresse déjà utilisée`, faire

        
        # ERREUR!

        java.net.BindException: Adresse déjà utilisée


        [FATAL] Cannot listen on ws://localhost:8002
                Port already in use?


        # Faire
        $ sh gradlew --stop

        # Puis relancer le serveur
        $ sh gradlew pong:serveur

## Vérifier qu'on peut toujours exécuter la version locale

1. La version locale devrait toujours fonctionner

    ```bash
    $ sh gradlew pong
    ```
