---
title: "Ajouter les joueurs à la partie"
weight: 1
bookHidden: true
---


# Optionnel) ajouter les bons joueurs à la partie

## Rejoindre un rendez-vous (créer un rendez-vous complet)

1. Dans `ModeleFileAttente`, ajouter `creerRendezVousComplet`

    ```java
    {{% embed src="./ModeleFileAttente01.java" indent-level="1" %}}
    ```


1. Créer le `MsgRejoindreRendezVous`

    ```java
    {{% embed src="./MsgRejoindreRendezVous01.java" indent-level="1" %}}
    ```

1. Déclarer le message dans `Declarations`

    ```java
    {{% embed src="./Declarations01.java" indent-level="1" %}}
    ```

1. Sur un rendez-vous partiel, envoyer `MsgRejoindreRendezVous`

    ```java
    {{% embed src="./FragmentRendezVous01.java" indent-level="1" %}}
    ```

1. Sur un rendez-vous complet, afficher la partie

    ```java
    {{% embed src="./FragmentRendezVousComplet01.java" indent-level="1" %}}
    ```

1. Dans `SessionPong`, ajouter la méthode `envoyerMsgRejoindreRendezVous`

    ```java
    {{% embed src="./SessionPong01.java" indent-level="1" %}}
    ```
## Ajouter les joueurs dans la partie

1. Dans `ModelePartie`, ajouter

    ```java
    {{% embed src="./ModelePartie01.java" indent-level="1" %}}
    ```


## Créer la partie (en réaction à la création d'un rendez-vous complet)

1. Créer le `MsgCreerPartie`

    ```java
    {{% embed src="./MsgCreerPartie01.java" indent-level="1" %}}
    ```

1. Déclarer le message dans `Declarations`

    ```java
    {{% embed src="./Declarations02.java" indent-level="1" %}}
    ```

1. Dans `ModifierFileAttente`, ajouter

    ```java
    {{% embed src="./ModifierFileAttente01.java" indent-level="1" %}}
    ```


1. Dans `ModifierPartie`, ajouter

    ```java
    {{% embed src="./ModifierPartie01.java" indent-level="1" %}}
    ```

1. Le graphe de tâche du Dorsal devrait contenir


    <img src="backend01.png"/>



