public class FragmentRendezVousComplet extends FragmentRendezVous {
    
    @FXML
    private Label labelNomDeuxiemeJoueur;

    @FXML
    private Label labelScore;

    @Override
    public void initialize() {
        super.initialize();

        Ntro.assertNotNull(labelNomDeuxiemeJoueur);
        Ntro.assertNotNull(labelScore);
    }

    public void afficherNomDeuxiemeJoueur(String nomDeuxiemeJoueur) {
        labelNomDeuxiemeJoueur.setText(nomDeuxiemeJoueur);
    }

    private void installerEvtAfficherPartie(String idPartie, 
                                            String idPremierJoueur,
                                            String idDeuxiemeJoueur) {

        getBoutonDebuterPartie().setOnAction(evtFx -> {
            
            Ntro.logger().info("EvtAfficher: " + idPremierJoueur + " " + idDeuxiemeJoueur);
            
            Ntro.session(SessionPong.class)
                .memoriserPartieCourante(idPartie, 
                                         idPremierJoueur, 
                                         idDeuxiemeJoueur)
                .envoyerMsgRejoindrePartie();
            
            Ntro.newEvent(EvtAfficherPartie.class)
                .setIdPartie(idPartie)
                .trigger();

        });
    }
    
    public void memoriserIdRendezVous(String idRendezVous) {
        installerMsgRetirerRendezVous(idRendezVous);
    }

    public void installerMessages(String idPartie, 
                                  String idPremierJoueur, 
                                  String idDeuxiemeJoueur) {

        installerEvtAfficherPartie(idPartie,
                                   idPremierJoueur,
                                   idDeuxiemeJoueur);
    }
    
    public void afficherScore(String score) {
        labelScore.setText(score);
    }
    

}
