public class RendezVous implements ModelValue {
    
    private String idRendezVous;
    private Joueur premierJoueur;

    protected String getIdRendezVous() {
        return idRendezVous;
    }

    protected Joueur getPremierJoueur() {
        return premierJoueur;
    }

    public RendezVous() {
    }

    public RendezVous(String idRendezVous, Joueur premierJoueur) {
        this.idRendezVous = idRendezVous;
        this.premierJoueur = premierJoueur;
    }

    @Override
    public String toString() {
        return premierJoueur.toString();
    }
    
    public void afficherSur(FragmentRendezVous fragmentRendezVous) {
        fragmentRendezVous.memoriserIdRendezVous(idRendezVous);
        fragmentRendezVous.afficherNomPremierJoueur(premierJoueur.getPrenom());
    }

    public FragmentRendezVous creerFragment(ViewLoader<FragmentRendezVous> viewLoaderRendezVous, 
                                  ViewLoader<FragmentRendezVousComplet> viewLoaderPartieEnCours) {
        
        return viewLoaderRendezVous.createView();
    }

    public RendezVousComplet creerRendezVousComplet(Joueur deuxiemeJoueur) {

        return new RendezVousComplet(idRendezVous, premierJoueur, deuxiemeJoueur);
    }

    public boolean siIdEst(String idRendezVous) {
        return this.idRendezVous.equals(idRendezVous);
    }

    public boolean siContenuDans(ModeleFileAttente fileAttente) {
        return fileAttente.siContientRendezVous(idRendezVous);
    }

    protected boolean siFragmentDuBonType(FragmentRendezVous fragmentRendezVous) {
        return fragmentRendezVous.getClass().equals(FragmentRendezVous.class);
    }
}
