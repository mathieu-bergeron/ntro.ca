public class SessionPong extends Session<SessionPong> {

    // ...


    // ajouter
    public SessionPong envoyerMsgRejoindreRendezVous(String idRendezVous) {
        Ntro.newMessage(MsgRejoindreRendezVous.class)
            .setIdRendezVous(idRendezVous)
            .setJoueur(MaquetteJoueurs.joueurAleatoire(this.sessionId()))
            .send();
        
        return this;
    }
