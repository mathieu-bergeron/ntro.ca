public class RendezVousComplet extends RendezVous {

    // ...
    
    // ajouter
    public RendezVousComplet(String idRendezVous, 
                             Joueur premierJoueur,
                             Joueur deuxiemeJoueur) {

        super(idRendezVous, premierJoueur);
        
        this.deuxiemeJoueur = deuxiemeJoueur;
    }
