public class FragmentRendezVousComplet extends FragmentRendezVous {

    // ...

    @Override
    public void initialize() {
        super.initialize();

        // ...

        // ajouter
        installerEvtAfficherPartie();
    }

    private void installerEvtAfficherPartie() {

        getBoutonDebuterPartie().setOnAction(evtFx -> {

            Ntro.newEvent(EvtAfficherPartie.class)
                .trigger();

        });
    }
