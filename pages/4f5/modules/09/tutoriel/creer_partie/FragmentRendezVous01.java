public class FragmentRendezVous extends ViewFx {

    // ...
    
    // s'assurer d'avoir
    protected Button getBoutonDebuterPartie() {
        return boutonDebuterPartie;
    }

    // s'assurer d'avoir
    public void memoriserIdRendezVous(String idRendezVous) {

        // ...

        // ajouter
        installerMsgRejoindreRendezVous(idRendezVous);
    }

    private void installerMsgRejoindreRendezVous(String idRendezVous) {

        boutonDebuterPartie.setOnAction(evtFx -> {

            Ntro.session(SessionPong.class)
                .envoyerMsgRejoindreRendezVous(idRendezVous);

        });
    }
