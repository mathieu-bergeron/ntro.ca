public class FragmentRendezVous extends ViewFx {

    @FXML
    private Button boutonDebuterPartie;

    @FXML
    private Button boutonRetirerRendezVous;
    
    @FXML
    private Label labelNomPremierJoueur;
    
    protected Button getBoutonDebuterPartie() {
        return boutonDebuterPartie;
    }

    @Override
    public void initialize() {

        Ntro.assertNotNull(boutonDebuterPartie);
        Ntro.assertNotNull(boutonRetirerRendezVous);
        Ntro.assertNotNull(labelNomPremierJoueur);

    }

    private void installerMsgRejoindreRendezVous(String idRendezVous) {

        boutonDebuterPartie.setOnAction(evtFx -> {

            Ntro.session(SessionPong.class)
                .envoyerMsgRejoindreRendezVous(idRendezVous);

        });
    }
    
    protected void installerMsgRetirerRendezVous(String idPartie) {
        
        boutonRetirerRendezVous.setOnAction(evtFx -> {

            Ntro.newMessage(MsgRetirerRendezVous.class)
                .setIdRendezVous(idPartie) 
                .send();

        });

    }

    public void afficherNomPremierJoueur(String nomPremierJoueur) {
        labelNomPremierJoueur.setText(nomPremierJoueur);
    }

    public void memoriserIdRendezVous(String idRendezVous) {
        installerMsgRejoindreRendezVous(idRendezVous);
        installerMsgRetirerRendezVous(idRendezVous);
    }

}
