public class ModelePartie implements Model {
    
    // s'assurer d'avoir
    private String                     idRendezVous          = null;

    // s'assurer d'avoir
    private Map<Position, InfoJoueur>  infoJoueurParPosition = new HashMap<>(); 

    
    // ajouter
    public ModelePartie setIdRendezVous(String idRendezVous) {
        this.idRendezVous = idRendezVous;
        
        return this;
    }


    // ajouter
    public ModelePartie ajouterJoueur(Position position, Joueur joueur) {
        infoJoueurParPosition.put(position, new InfoJoueur(joueur));
        
        return this;
    }
