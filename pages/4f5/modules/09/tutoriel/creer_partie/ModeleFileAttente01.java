public class ModeleFileAttente implements Model, WatchJson, WriteObjectGraph {

    // ...
    
    // s'assurer d'avoir 
    private int indiceRendezVousParId(String idRendezVous) {
        int indice = -1;
        
        for(int i = 0; i < rendezVousDansOrdre.size(); i++) {
            if(rendezVousDansOrdre.get(i).siIdEst(idRendezVous)) {
                indice = i;
                break;
            }
        }
        
        return indice;
    }
    
    // ajouter
    public RendezVousComplet creerRendezVousComplet(String idRendezVous, Joueur deuxiemeJoueur) {

        int indiceRendezVous = indiceRendezVousParId(idRendezVous);
        RendezVous rendezVous = rendezVousDansOrdre.get(indiceRendezVous);
        
        RendezVousComplet rendezVousComplet = null;

        if(rendezVous != null) {

            rendezVousComplet = rendezVous.creerRendezVousComplet(deuxiemeJoueur);
            
            rendezVousDansOrdre.set(indiceRendezVous, rendezVousComplet);

        }
        
        return rendezVousComplet;
    }
    
}
