public class ModifierPartie {
    
    public static void creerTaches(BackendTasks tasks) {
        
        tasks.taskGroup("ModifierPartie")
        
             .waitsFor(model(ModelePartie.class))
        
              .contains(subTasks -> {

                    // ajouter
                    creerPartie(subTasks);

                    // ...

              });
    }

    // ajouter
    private static void creerPartie(BackendTasks subTasks) {

        subTasks.task("creerPartie")

                .waitsFor(model(ModelePartie.class))

                .waitsFor(message(MsgCreerPartie.class))
             
                .executes(inputs -> {
                 
                    MsgCreerPartie msgCreerPartie = inputs.get(message(MsgCreerPartie.class));
                    ModelePartie   partie         = inputs.get(model(ModelePartie.class));
                 
                    msgCreerPartie.appliquerA(partie);

             });
    }
