public class MsgCreerPartie extends Message<MsgCreerPartie> {
    
    private String idRendezVous;
    private Joueur premierJoueur;
    private Joueur deuxiemeJoueur;

    public MsgCreerPartie setIdRendezVous(String idRendezVous) {

        this.idRendezVous = idRendezVous;

        return this;
    }
    
    public MsgCreerPartie setPremierJoueur(Joueur premierJoueur) {
        this.premierJoueur = premierJoueur;
        
        return this;
    }
    
    public MsgCreerPartie setDeuxiemeJoueur(Joueur deuxiemeJoueur) {
        this.deuxiemeJoueur = deuxiemeJoueur;
        
        return this;
    }

    public void appliquerA(ModelePartie partie) {

        partie.setIdRendezVous(idRendezVous)
              .ajouterJoueur(Position.GAUCHE, premierJoueur)
              .ajouterJoueur(Position.DROITE, deuxiemeJoueur);

    }


}
