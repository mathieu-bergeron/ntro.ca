public class Declarations {

    public static void declarerMessages(MessageRegistrar registrar) {
        registrar.registerMessage(MsgAjouterRendezVous.class);
        registrar.registerMessage(MsgRetirerRendezVous.class);
        registrar.registerMessage(MsgRejoindreRendezVous.class);
        registrar.registerMessage(MsgCreerPartie.class);
        registrar.registerMessage(MsgRejoindrePartie.class);
        registrar.registerMessage(MsgQuitterPartie.class);
        registrar.registerMessage(MsgAjouterPoint.class);
        registrar.registerMessage(MsgActionAutreJoueur.class);
        registrar.registerMessage(MsgAjouterScoreAuRendezVous.class);
        registrar.registerMessage(MsgInitialiserFileAttente.class);
    }

    public static void declarerModeles(ModelRegistrar registrar) {

        registrar.registerModel(ModeleFileAttente.class);
        registrar.registerValue(RendezVous.class);
        registrar.registerValue(RendezVousComplet.class);
        registrar.registerValue(Joueur.class);

        registrar.registerModel(ModelePartie.class);
        registrar.registerValue(InfoJoueur.class);
        registrar.registerValue(MondePong2d.class);
        registrar.registerValue(Palette2d.class);
        registrar.registerValue(Balle2d.class);
    }

    public static void declarerServeur(ServerRegistrar registrar) {
        registrar.registerName("localhost");
        registrar.registerPort(8002);
    }

}
