public class ModifierFileAttente {
    
    public static void creerTaches(BackendTasks tasks) {

        tasks.taskGroup("ModifierFileAttente")

             .waitsFor(model(ModeleFileAttente.class))
        
             .contains(subTasks -> {
                  
                 // ...

                 // s'assurer d'avoir
                 rejoindreRendezVous(subTasks);

                 // ajouter
                 envoyerMsgCreerPartie(subTasks);

             });
    }

    private static void rejoindreRendezVous(BackendTasks subTasks) {
        subTasks.task("rejoindreRendezVous")

             .waitsFor(model(ModeleFileAttente.class))

             .waitsFor(message(MsgRejoindreRendezVous.class))

             // modifier
             .executesAndReturnsValue(inputs -> {
                 
                 MsgRejoindreRendezVous msgRejoindreRendezVous = inputs.get(message(MsgRejoindreRendezVous.class));
                 ModeleFileAttente      fileAttente            = inputs.get(model(ModeleFileAttente.class));
                 
                 RendezVousComplet rendezVousComplet = msgRejoindreRendezVous.appliquerA(fileAttente);
                 
                 // modifier
                 return rendezVousComplet;

             });
    }

    // ajouter
    private static void envoyerMsgCreerPartie(BackendTasks subTasks) {
        subTasks.task("envoyerMsgCreerPartie")

             .waitsFor("rejoindreRendezVous")

             .executes(inputs -> {
                 
                 RendezVousComplet rendezVousComplet = inputs.get("rejoindreRendezVous");
                 
                 if(rendezVousComplet != null) {

                     rendezVousComplet.envoyerMsgCreerPartie();

                 }
             });
    }
