public class ModelePartie implements Model {
    
    private MondePong2d mondePong2d = new MondePong2d();
    
    private String idPartie;

    private String idPremierJoueur = "";
    private String idDeuxiemeJoueur = "";

    private String nomPremierJoueur = "Alice";
    private String nomDeuxiemeJoueur = "Bob";

    private int scorePremierJoueur = 0;
    private int scoreDeuxiemeJoueur = 0;

    public String getIdPremierJoueur() {
        return idPremierJoueur;
    }

    public void setIdPremierJoueur(String idPremierJoueur) {
        this.idPremierJoueur = idPremierJoueur;
    }

    public String getIdDeuxiemeJoueur() {
        return idDeuxiemeJoueur;
    }

    public void setIdDeuxiemeJoueur(String idDeuxiemeJoueur) {
        this.idDeuxiemeJoueur = idDeuxiemeJoueur;
    }

    public String getIdPartie() {
        return idPartie;
    }

    public void setIdPartie(String idPartie) {
        this.idPartie = idPartie;
    }

    public MondePong2d getMondePong2d() {
        return mondePong2d;
    }

    public void setMondePong2d(MondePong2d mondePong2d) {
        this.mondePong2d = mondePong2d;
    }

    public String getNomPremierJoueur() {
        return nomPremierJoueur;
    }

    public void setNomPremierJoueur(String nomPremierJoueur) {
        this.nomPremierJoueur = nomPremierJoueur;
    }

    public String getNomDeuxiemeJoueur() {
        return nomDeuxiemeJoueur;
    }

    public void setNomDeuxiemeJoueur(String nomDeuxiemeJoueur) {
        this.nomDeuxiemeJoueur = nomDeuxiemeJoueur;
    }

    public int getScorePremierJoueur() {
        return scorePremierJoueur;
    }

    public void setScorePremierJoueur(int scorePremierJoueur) {
        this.scorePremierJoueur = scorePremierJoueur;
    }

    public int getScoreDeuxiemeJoueur() {
        return scoreDeuxiemeJoueur;
    }

    public void setScoreDeuxiemeJoueur(int scoreDeuxiemeJoueur) {
        this.scoreDeuxiemeJoueur = scoreDeuxiemeJoueur;
    }
    
    
    public void afficherInfoPartieSur(VuePartie vuePartie) {
        vuePartie.afficherNomPremierJoueur(nomPremierJoueur);
        vuePartie.afficherNomDeuxiemeJoueur(nomDeuxiemeJoueur);

        vuePartie.afficherScorePremierJoueur(String.valueOf(scorePremierJoueur));
        vuePartie.afficherScoreDeuxiemeJoueur(String.valueOf(scoreDeuxiemeJoueur));
    }

    public void copierDonneesDans(DonneesVuePartie donneesVuePartie) {
        donneesVuePartie.copierDonnesDe(mondePong2d);
    }

    /*
    public void appliquerActionJoueur(Cadran cadran, Action action) {
        mondePong2d.appliquerActionJoueur(cadran, action);
    }*/

    public void copierDonnesDe(MondePong2d mondePong2d) {
        this.mondePong2d.copyDataFrom(mondePong2d);
        this.mondePong2d.setVersion(mondePong2d.getVersion());
    }

    public void ajouterPointPour(Cadran cadran) {
        switch(cadran) {
        case GAUCHE:
        default:
            scorePremierJoueur++;
            break;

        case DROITE:
            scoreDeuxiemeJoueur++;
            break;
        }
    }

    public void envoyerMsgModifierPointage() {

        MsgModifierPointage msgModifierPointage = Ntro.newMessage(MsgModifierPointage.class);
        
        msgModifierPointage.setIdRendezVous(idPartie);
        msgModifierPointage.setScorePremierJoueur(scorePremierJoueur);
        msgModifierPointage.setScoreDeuxiemeJoueur(scoreDeuxiemeJoueur);
        
        msgModifierPointage.send();

    }

    public boolean estPlusRecenteQue(DonneesVuePartie donneesVuePartie) {
        return mondePong2d.estPlusRecentQue(donneesVuePartie.getMondePong2d());
    }
    

}
