public class ModifierPartie {
    
    public static void creerTaches(BackendTasks tasks) {
        
        tasks.taskGroup("ModifierPartie")
        
             .waitsFor(model(ModelePartie.class))
        
              .contains(subTasks -> {
                  
                    ajouterPoint(subTasks);
                  
                    //sauvegarderPartie(subTasks);
                    
                    modifierInfoPartie(subTasks);
                    
              });
    }


    private static void sauvegarderPartie(BackendTasks tasks) {
        tasks.task("sauvegarderPartie")

             .waitsFor(message(MsgSauvegarderPartie.class))
             
             .executes(inputs -> {

                 MsgSauvegarderPartie msgEtatPong2d = inputs.get(message(MsgSauvegarderPartie.class));
                 ModelePartie         partie        = inputs.get(model(ModelePartie.class));
                 
                 msgEtatPong2d.appliquerA(partie);
             });
    }

    private static void ajouterPoint(BackendTasks tasks) {

        tasks.task("ajouterPoint")

             .waitsFor(message(MsgAjouterPoint.class))


             .executes(inputs -> {
                 
                 MsgAjouterPoint   msgAjouterPoint = inputs.get(message(MsgAjouterPoint.class));
                 ModelePartie      partie          = inputs.get(model(ModelePartie.class));
                 
                 if(msgAjouterPoint.estPlusRecentQue(partie)) {

                     msgAjouterPoint.copierDonneesDans(partie);
                     msgAjouterPoint.ajouterPointA(partie);

                     partie.envoyerMsgModifierPointage();
                 }
             });
    }

    private static void modifierInfoPartie(BackendTasks tasks) {

        tasks.task("modifierInfoPartie")

             .waitsFor(message(MsgModifierInfoPartie.class))


             .executes(inputs -> {
                 
                 MsgModifierInfoPartie msgModifierInfoPartie = inputs.get(message(MsgModifierInfoPartie.class));
                 ModelePartie          partie                = inputs.get(model(ModelePartie.class));
                 
                 msgModifierInfoPartie.appliquerA(partie);

             });
    }

}
