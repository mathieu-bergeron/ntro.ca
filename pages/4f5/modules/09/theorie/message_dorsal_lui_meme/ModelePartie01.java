public class ModelePartie implements Model {

    // ...
    
    public void envoyerMsgAjouterScoreAuRendezVous() {
        Ntro.newMessage(MsgAjouterScoreAuRendezVous.class)
            .setIdRendezVous(idRendezVous)
            .setScoreParPosition(creerScoreParPosition())
            .send();
    }

    private Map<Position, Integer> creerScoreParPosition() {
        Map<Position, Integer> scoreParPosition = new HashMap<>();

        for(Position position : infoJoueurParPosition.keySet()) {
            InfoJoueur infoJoueur = infoJoueurParPosition.get(position);
            infoJoueur.copierScoreDans(scoreParPosition, position);
        }

        return scoreParPosition;
    }

