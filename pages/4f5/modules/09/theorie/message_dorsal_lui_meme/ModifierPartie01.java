public class ModifierPartie {
    
    public static void creerTaches(BackendTasks tasks) {
        
        tasks.taskGroup("ModifierPartie")
        
             .waitsFor(model(ModelePartie.class))
        
              .contains(subTasks -> {
                  
                    ajouterPoint(subTasks);

                    envoyerMsgAjouterScoreAuRendezVous(subTasks);

                    // ...
                    
              });
    }

    private static void ajouterPoint(BackendTasks tasks) {

        tasks.task("ajouterPoint")

             // ...

             // modifier
             .executesAndReturnsValue(inputs -> {

                 // ...

                 // propager la partie vers la prochaine
                 // tâche qui va envoyer le bon message
                 return partie;
             });
    }

    private static void envoyerMsgAjouterScoreAuRendezVous(BackendTasks subTasks) {

        subTasks.task("envoyerMsgAjouterScoreAuRendezVous")

            .waitsFor("ajouterPoint")

            .executes(inputs -> {

                ModelePartie    partie          = inputs.get("ajouterPoint");

                partie.envoyerMsgAjouterScoreAuRendezVous();

        });
    }
