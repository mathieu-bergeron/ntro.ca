---
title: "Message dorsal à lui-même"
weight: 1
bookHidden: true
---


# Théorie: message du dorsal à lui-même

{{% video src="/4f5/modules/09/theorie/message_dorsal_lui_meme/presentation.mp4" %}}

## Afficher le pointage sur la `VueFileAttente`?

* Quand ajoute un point à la partie, on voudrait aussi modifier le pointage dans le `ModeleFileAttente`

    {{% animation src="/4f5/modules/09/theorie/message_dorsal_lui_meme/message_dorsal_lui_meme.mp4" %}}

    * NOTE: le pointage passe à `21-16` en même temps pour les deux clients


## Envoyer un message du dorsal à lui-même

* La tâche qui ajoute le point va aussi envoyer un message pour mettre à jour la file d'attente

    <img src="backend_annote_crop.png"/>

* Dans les tâches du dorsal:

    ```java
    {{% embed src="ModifierPartie01.java" indent-level="1" %}}
    ```

* Dans le `ModelePartie`:

    ```java
    {{% embed src="ModelePartie01.java" indent-level="1" %}}
    ```
