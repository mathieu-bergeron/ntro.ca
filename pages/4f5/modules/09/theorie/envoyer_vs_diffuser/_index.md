---
title: ""
weight: 1
bookHidden: true
---


# Théorie: envoyer Vs diffuser

<center>
<video width="50%" src="intro.mp4" type="video/mp4" controls playsinline>
</center>


* Il y a deux façons d'envoyer un message

    * envoyer

        ```java
        {{% embed src="./Envoyer.java" indent-level="2" %}}
        ```

    * diffuser

        ```java
        {{% embed src="./Diffuser.java" indent-level="2" %}}
        ```


## Envoyer

<center>
<video width="50%" src="envoyer.mp4" type="video/mp4" controls playsinline>
</center>

* Envoyer veut dire: «envoyer vers le dorsal»

* En mode client/serveur, ça donne

    <center>
        <img width="100%" src="send.png"/>
    </center>

## Diffuser

<center>
<video width="50%" src="diffuser.mp4" type="video/mp4" controls playsinline>
</center>

* Diffuser veut dire: «envoyer vers le frontal»

* En mode client/serveur, diffuser est soit

    * envoyer vers les autres clients (à travers le serveur)

        <center>
            <img width="100%" src="broadcast01.png"/>
        </center>

    * envoyer vers les clients (à partir du serveur)

        <center>
            <img width="100%" src="broadcast02.png"/>
        </center>

        
       