---
title: ""
weight: 1
bookHidden: true
---


# Théorie: créer un serveur

<center>
<video width="50%" src="serveur.mp4" type="video/mp4" controls playsinline>
</center>

* Pour créer un serveur, on fait

    ```java
    {{% embed src="./ServeurPong.java" indent-level="1" %}}
    ```


* On exécute notre «vrai» dorsal sur le serveur
    * les messages vont arriver des clients
    * on définit des tâches
    * on gère les modèles
    * les modifications aux modèles seront poussés vers les clients