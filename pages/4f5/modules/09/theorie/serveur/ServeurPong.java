public class ServeurPong implements NtroServerFx {

    public static void main(String[] args) {
        NtroServeFx .launch(args);
    }

    @Override
    public void registerMessages(MessageRegistrar registrar) {
        // même chose
    }

    @Override
    public void registerModels(ModelRegistrar registrar) {
        // même chose
    }

    @Override
    public void registerBackend(BackendRegistrar registrar) {
        // le dorsal local, mais cette fois-ci
        // on l'exécute sur le serveur
        registrar.registerBackend(DorsalPong.class);
    }

    @Override
    public void registerServer(ServerRegistrarJdk registrar) {
        // spécifier le serveur auquel
        // se connecter
        registrar.registerPort(8002);
        registrar.registerName("localhost");
    }
}
