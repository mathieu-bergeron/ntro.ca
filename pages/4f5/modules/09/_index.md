---
title: "Module 9: client/serveur"
weight: 100
draft: false
---

# Module 9: client/serveur

<!--


{{<excerpt class="max-width-75">}}

* Optionnel: {{% link "/4f5/migrations/v05" "Migration à `v05` de Ntro" %}}

{{</excerpt>}}

-->



* {{% link "./theorie" "Théorie" %}}

    * patron MVC en client/serveur
    * envoyer un message Vs diffuser un message
    * serveur et dorsal distant
    * messages du dorsal vers lui-même

* {{% link src="./tutoriel/" text="Tutoriel à faire avant les objectifs" %}}

    * faire fonctionner `pong` en mode client/serveur

* {{% link src="./objectifs/" text="Objectifs pour avancer le TP#2" %}} 

    * faire fonctionner **mon projet** en mode client/serveur


* REMISES:
    * <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=306649">auto-évaluation pour ce module</a>

<br>
<div style="padding:5px;background:pink;border-style:dotted" >
<i>RAPPEL:</i> chaque étudiant.e est <strong>individuellement</strong> responsable de:
<ul>
<li>maintenir <i>son propre</i> dépôt Git
<li>présenter <i>son progrès</i> à la rencontre de travail
<li>faire son auto-évaluation
<li>s'assurer que son code est fonctionnel
</ul> 
<br>
<i>RAPPEL:</i> vous travaillez en équipe, mais les notes sont 100% <strong>individuelles</strong>
</div>


<br>
<br>
<br>
<br>
<br>


