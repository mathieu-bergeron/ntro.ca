public class AfficherFileAttente {

    // ...
    
    // modifier cette méthode
    private static void afficherFileAttente(FrontendTasks tasks) {
        
        tasks.task("afficherFileAttente")
        
             .waitsFor(modified(ModeleFileAttente.class))
             
             .waitsFor(created(VueFileAttente.class))

             .executes(inputs -> {
                 
                 // ajouter
                 SessionPong                 session        = Ntro.session();
                 
                 VueFileAttente              vueFileAttente = inputs.get(created(VueFileAttente.class));
                 Modified<ModeleFileAttente> fileAttente    = inputs.get(modified(ModeleFileAttente.class));
                 
                 fileAttente.currentValue().afficherSur(vueFileAttente);

                 // ajouter
                 session.afficherRegionCourante(vueFileAttente);
             });
    }
