public class SessionPong extends Session<SessionPong> {

    // ...
    
    // ajouter
    private Region                regionCourante      = Region.AMERIQUE;

    // ...

    // ajouter
    public SessionPong() {
        super();
        observerRegionCourante();
    }

    // ...


    // ajouter
    public SessionPong memoriserRegionCourante(Region region) {
        this.regionCourante = region;

        
        return this;
    }

    // ajouter
    public SessionPong observerRegionCourante() {
        this.setModelSelection(ModeleFileAttente.class, regionCourante.name());
        
        return this;
    }
