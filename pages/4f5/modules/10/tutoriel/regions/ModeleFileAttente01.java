public class ModeleFileAttente implements Model, WatchJson, WriteObjectGraph {

    // ...


    // ajouter
    private Region region = null;

    // ...


    // ajouter
    public void initialiserRegion(Region region) {
        this.region = region;
    }

    // ...


    // modifier cette méthode
    private String genererIdRendezVous() {
        String idRendezVous = String.valueOf(prochainIdRendezVous);
        prochainIdRendezVous++;
        
        // ajouter
        if(region != null) {
            idRendezVous = region.name() + "¤" + idRendezVous;
        }

        return idRendezVous;
    }
