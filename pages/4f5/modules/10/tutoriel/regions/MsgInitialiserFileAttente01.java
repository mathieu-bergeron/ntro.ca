public class MsgInitialiserFileAttente extends Message<MsgInitialiserFileAttente> {
    
    private Region region;
    
    public MsgInitialiserFileAttente setRegion(Region region) {
        this.region = region;

        return this;
    }

    public MsgInitialiserFileAttente() {
    }

    public void appliquerA(ModeleFileAttente fileAttente) {
        fileAttente.initialiserRegion(region);
    }
}
