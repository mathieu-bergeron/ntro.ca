public class VueFileAttente extends ViewFx {

    // ...
    
    @FXML
    private ComboBox<String> comboRegion; // ajouter
    
    // ajouter
    private Map<Region,String> nomRegionParRegion = new HashMap<>();
    private Map<String,Region> regionParNomRegion = new HashMap<>();

    // ...

    @Override
    public void initialize() {

        // ...

        // ajouter
        Ntro.assertNotNull(comboRegion);

        // ajouter
        initialiserRegions();

        // ajouter
        initialiserComboRegion();
    }

    // ...

    // ajouter
    private void initialiserRegions() {
        for(Region region : Region.values()) {
            String nomRegion = resources().getString(region.name());
            nomRegionParRegion.put(region, nomRegion);
            regionParNomRegion.put(nomRegion, region);
        }
    }

    // ajouter
    private void initialiserComboRegion() {
        comboRegion.setFocusTraversable(false);
        
        for(Region region : Region.values()) {
            comboRegion.getItems().add(nomRegionParRegion.get(region));
        }

        comboRegion.setOnAction(evtFx -> actionComboRegion());

    }

    // ajouter
    private void actionComboRegion() {

        String nomRegion = comboRegion.getSelectionModel().getSelectedItem();
        
        Region region = regionParNomRegion.get(nomRegion);
        
        if(region != null) {
            
            Ntro.newEvent(EvtChangerRegion.class)
                .setRegion(region)
                .trigger();
        }
    }
