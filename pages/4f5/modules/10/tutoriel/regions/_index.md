---
title: "Plusieurs files d'attente"
weight: 1
bookHidden: true
---

# Plusieurs files d'attente (une par region)


## Les régions et les noms de région traduits

1. Dans le paquet `pong.commun.enums`, ajouter `Region`

    ```java
    {{% embed src="./Region.java" indent-level="1" %}}
    ```

1. Pour traduire les régions, ajouter les variables suivantes à `fr.properties`

    ```ini
    {{% embed src="./fr.properties" indent-level="1" %}}
    ```

1. Ajouter les variables suivantes à `en.properties`

    ```ini
    {{% embed src="./en.properties" indent-level="1" %}}
    ```

## Ajouter une tâche du Dorsal pour initialiser les régions

1. Le `ModeleFileAttente` mémorise maintenant la région et l'utilise pour les id de rendez-vous

    ```java
    {{% embed src="./ModeleFileAttente01.java" indent-level="1" %}}
    ```

1. Dans `pong.commun.messages`, ajouter `MsgInitialiserFileAttente`

    ```java
    {{% embed src="./MsgInitialiserFileAttente01.java" indent-level="1" %}}
    ```

1. Déclarer le message dans `Declarations`

    ```java
    {{% embed src="./Declarations01.java" indent-level="1" %}}
    ```

1. Dans `pong.dorsal.taches.ModifierFileAttente`, ajouter

    ```java
    {{% embed src="./ModifierFileAttente01.java" indent-level="1" %}}
    ```

1. Dans `pong.dorsal.taches`, ajouter `Initialisation`

    ```java
    {{% embed src="./Initialisation01.java" indent-level="1" %}}
    ```

1. S'assurer d'appeler `Initialisation.creerTaches` dans le Dorsal

    ```java
    {{% embed src="./DorsalPong01.java" indent-level="1" %}}
    ```

1. Exécuter et vérifier que la tâche s'exécute

    ```bash
    $ sh gradlew pong

    # doit afficher
    [TASK] initialiserFilesAttentes
    [...]
    [TASK] initialiser (4)
    ```

1. Vérifier les fichiers .json

    ```bash
    _storage
    ├── models
    │   ├── ModeleFileAttente
    │   │   ├── AFRIQUE.json
    │   │   ├── AMERIQUE.json
    │   │   ├── ASIE.json
    │   │   └── EUROPE.json
    ```

1. Vérifier le contenu de `AFRIQUE.json`

    ```java
    {{% embed src="./AFRIQUE.json" indent-level="1" %}}
    ```

1. Vérifier le graphe du dorsal

    <img src="backend01.png"/>




## Avec la Session, mémoriser et observer la région courante

1. Ajouter le code suivant dans `SessionPong`

    ```java
    {{% embed src="./SessionPong01.java" indent-level="1" %}}
    ```

## Ajouter un `EvtChangerRegion`

1. Dans `pong.frontal.evenements`, ajouter

    ```java
    {{% embed src="./EvtChangerRegion01.java" indent-level="1" %}}
    ```

1. Déclarer l'événement dans le Frontal:

    ```java
    {{% embed src="./FrontalPong01.java" indent-level="1" %}}
    ```

## Ajouter une tâche pour changer de région

1. Dans `AfficherFileAttente`, ajouter la tâche suivante

    ```java
    {{% embed src="./AfficherFileAttente01.java" indent-level="1" %}}
    ```

## Afficher les régions dans un ComboBox

1. Dans `file_attente.fxml`, ajouter un ComboBox pour afficher les régions

    ```xml
    {{% embed src="./file_attente01.fxml" indent-level="1" %}}
    ```

1. Dans `VueFileAttente`, récupérer et initialiser le ComboBox

    ```java
    {{% embed src="./VueFileAttente01.java" indent-level="1" %}}
    ```

1. En français

    {{% animation src="/4f5/modules/10/tutoriel/regions/region_fr.mp4" %}}

1. En anglais

    {{% animation src="/4f5/modules/10/tutoriel/regions/region_en.mp4" %}}

## S'assurer d'afficher la région courante (même au premier affichage)

1. Créer les méthodes pour afficher la région courante

    ```java
    {{% embed src="./SessionPong02.java" indent-level="1" %}}
    ```

    ```java
    {{% embed src="./VueFileAttente02.java" indent-level="1" %}}
    ```

1. Ajouter le code suivant dans la tâche pour afficher la file d'attente

    ```java
    {{% embed src="./AfficherFileAttente02.java" indent-level="1" %}}
    ```



