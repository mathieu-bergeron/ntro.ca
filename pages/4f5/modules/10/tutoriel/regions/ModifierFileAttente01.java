public class ModifierFileAttente {
    
    public static void creerTaches(BackendTasks tasks) {

        tasks.taskGroup("ModifierFileAttente")

             .waitsFor(model(ModeleFileAttente.class))
        
             .contains(subTasks -> {
                  
                 // ajouter
                 initialiser(subTasks);

                 // ...

             });
    }

    // ...

    // ajouter
    private static void initialiser(BackendTasks subTasks) {
        subTasks.task("initialiser")

             .waitsFor(model(ModeleFileAttente.class))

             .waitsFor(message(MsgInitialiserFileAttente.class))

             .executes(inputs -> {
                 
                 MsgInitialiserFileAttente msgInitialiserFileAttente = inputs.get(message(MsgInitialiserFileAttente.class));
                 ModeleFileAttente         fileAttente               = inputs.get(model(ModeleFileAttente.class));

                 msgInitialiserFileAttente.appliquerA(fileAttente);
             });
    }
