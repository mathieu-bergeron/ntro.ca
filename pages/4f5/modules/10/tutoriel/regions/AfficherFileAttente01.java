public class AfficherFileAttente {
    
    public static void creerTaches(FrontendTasks tasks) {

        tasks.taskGroup("AfficherFileAttente")
        
             .waitsFor("afficherVueFileAttente")

             .contains(subTasks -> {
                 
                 // ...

                 // ajouter
                 changerRegion(subTasks);

             });
    }

    // ...
    
    // ajouter
    private static void changerRegion(FrontendTasks subTasks) {

        subTasks.task("changerRegion")

                .waitsFor(event(EvtChangerRegion.class))

                .executes(inputs -> {
                  
                    SessionPong      session          = Ntro.session();
                    EvtChangerRegion evtChangerRegion = inputs.get(event(EvtChangerRegion.class));
                  
                    evtChangerRegion.appliquerA(session);

              });
    }
