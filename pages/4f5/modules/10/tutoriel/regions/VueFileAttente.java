public class VueFileAttente extends ViewFx {
    
    @FXML
    private Button boutonSInscrire;

    @FXML
    private ComboBox<String> comboRegion;
    
    private Map<Region,String> nomRegionParRegion = new HashMap<>();
    private Map<String,Region> regionParNomRegion = new HashMap<>();

    @FXML
    private ResizableImage logo;

    @FXML
    private VBox conteneurRendezVous;

    private ViewLoader<FragmentRendezVous> viewLoaderRendezVous;
    private ViewLoader<FragmentRendezVousComplet> viewLoaderPartieEnCours;

    public ViewLoader<FragmentRendezVous> getViewLoaderRendezVous() {
        return viewLoaderRendezVous;
    }

    public void setViewLoaderRendezVous(ViewLoader<FragmentRendezVous> viewLoaderRendezVous) {
        this.viewLoaderRendezVous = viewLoaderRendezVous;
    }

    public ViewLoader<FragmentRendezVousComplet> getViewLoaderPartieEnCours() {
        return viewLoaderPartieEnCours;
    }

    public void setViewLoaderPartieEnCours(ViewLoader<FragmentRendezVousComplet> viewLoaderPartieEnCours) {
        this.viewLoaderPartieEnCours = viewLoaderPartieEnCours;
    }

    @Override
    public void initialize() {
        Ntro.assertNotNull(boutonSInscrire);
        Ntro.assertNotNull(conteneurRendezVous);
        Ntro.assertNotNull(logo);
        Ntro.assertNotNull(comboRegion);
        
        logo.setImage(new Image("/images/logo.png"));
        //logo.setBackgroundColor(Color.web("#ffff7a"));
        
        initialiserBoutonSInscrire();
        
        initialiserRegions();
        
        initialiserComboRegion();
    }


    private void initialiserRegions() {
        for(Region region : Region.values()) {
            String nomRegion = resources().getString(region.name());
            nomRegionParRegion.put(region, nomRegion);
            regionParNomRegion.put(nomRegion, region);
        }
    }

    private void initialiserComboRegion() {
        comboRegion.setFocusTraversable(false);
        
        for(Region region : Region.values()) {
            comboRegion.getItems().add(nomRegionParRegion.get(region));
        }

        comboRegion.setOnAction(evtFx -> actionComboRegion());

    }

    private void actionComboRegion() {

        String nomRegion = comboRegion.getSelectionModel().getSelectedItem();
        
        Region region = regionParNomRegion.get(nomRegion);
        
        if(region != null) {
            
            Ntro.newEvent(EvtChangerRegion.class)
                .setRegion(region)
                .trigger();
        }
    }

    private void initialiserBoutonSInscrire() {

        boutonSInscrire.setOnAction(evtFx -> actionBoutonSInscrire());

    }

    private void actionBoutonSInscrire() {
        
        SessionPong session = Ntro.session();
        
        session.envoyerMsgAjouterRendez();


    }

    public void ajouterRendezVous(RendezVous rendezVous) {
        FragmentRendezVous fragment = rendezVous.creerFragment(viewLoaderRendezVous, viewLoaderPartieEnCours);
        rendezVous.afficherSur(fragment);
        conteneurRendezVous.getChildren().add(fragment.rootNode());
    }
    

    public void viderListeRendezVous() {
        conteneurRendezVous.getChildren().clear();
    }

    public void afficherRendezVousEnTexte(String string) {

        
    }

    public void afficherRegionCourante(String idRegion) {
        Region region = null;

        if(idRegion != null) {
            region = Region.valueOf(idRegion);
        }
        
        if(region == null) {
            
            comboRegion.getSelectionModel().clearSelection();
            
        }else {
            
            comboRegion.getSelectionModel().select(nomRegionParRegion.get(region));

        }
    }




}
