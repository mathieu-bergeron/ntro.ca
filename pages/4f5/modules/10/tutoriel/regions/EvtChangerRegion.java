public class EvtChangerRegion extends Event {
    
    private Region region;

    public EvtChangerRegion setRegion(Region region) {
        this.region = region;
        
        return this;
    }

    public void appliquerA(SessionPong session) {

        session.memoriserRegionCourante(region)
               .observerRegionCourante();

    }

}
