public class VueFileAttente extends ViewFx {

    // ...
    


    // ajouter
    public void afficherRegionCourante(String idRegion) {
        Region region = null;

        if(idRegion != null) {
            region = Region.valueOf(idRegion);
        }
        
        if(region == null) {
            
            comboRegion.getSelectionModel().clearSelection();
            
        }else {
            
            comboRegion.getSelectionModel().select(nomRegionParRegion.get(region));

        }
    }
