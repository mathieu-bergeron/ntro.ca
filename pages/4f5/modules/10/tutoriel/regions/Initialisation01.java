public class Initialisation {
    
    public static void creerTaches(BackendTasks tasks) {

        tasks.taskGroup("Initialisation")

             .contains(subTasks -> {
                 
                 initialiserFilesAttentes(subTasks);

             });
    }

    private static void initialiserFilesAttentes(BackendTasks subTasks) {

        subTasks.task("initialiserFilesAttentes")

             .executes(inputs -> {

                 for(Region region : Region.values()) {

                     Ntro.newMessage(MsgInitialiserFileAttente.class)
                         .setModelSelection(ModeleFileAttente.class, region.name())
                         .setRegion(region)
                         .send();
                 }
             });
    }
}
