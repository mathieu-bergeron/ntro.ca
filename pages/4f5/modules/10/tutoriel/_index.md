---
title: ""
weight: 1
bookHidden: true
---


# Tutoriel: plusieurs versions d'un Modèle


## Étape obligatoire

* {{% link "./regions/" "Plusieurs files d'attente (une par région)" %}}

## Étapes optionnelles

* {{% link "./parties/" "Plusieurs parties (créées dynamiquement)" %}}


