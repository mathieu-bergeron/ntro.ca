public class RendezVousComplet extends RendezVous {

    // ...

    // modifier cette méthode
    public void envoyerMsgCreerPartie() {

        Ntro.newMessage(MsgCreerPartie.class)
            .setModelSelection(ModelePartie.class, getIdRendezVous())   // ajouter la sélection de modèle
            .setIdRendezVous(getIdRendezVous())
            .setPremierJoueur(getPremierJoueur())
            .setDeuxiemeJoueur(deuxiemeJoueur)
            .send();
    }


