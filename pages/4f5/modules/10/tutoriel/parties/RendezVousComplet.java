public class RendezVousComplet extends RendezVous {
    
    private Joueur                 deuxiemeJoueur;
    private Map<Position, Integer> scoreParPosition = new HashMap<>();

    public RendezVousComplet() {
        super();
        
        initialiserScoreParPosition();
    }

    public RendezVousComplet(String idRendezVous, 
                             Joueur premierJoueur,
                             Joueur deuxiemeJoueur) {

        super(idRendezVous, premierJoueur);
        
        this.deuxiemeJoueur = deuxiemeJoueur;
        
        initialiserScoreParPosition();
    }
    
    private void initialiserScoreParPosition() {
        scoreParPosition.put(Position.GAUCHE, 0);
        scoreParPosition.put(Position.DROITE, 0);
    }


    @Override
    public FragmentRendezVous creerFragment(ViewLoader<FragmentRendezVous> viewLoaderRendezVous, 
                                            ViewLoader<FragmentRendezVousComplet> viewLoaderPartieEnCours) {
        
        return viewLoaderPartieEnCours.createView();
    }

    public void afficherSur(FragmentRendezVous fragmentRendezVous) {
        super.afficherSur(fragmentRendezVous);
        
        FragmentRendezVousComplet fragmentRendezVousComplet = (FragmentRendezVousComplet) fragmentRendezVous;

        fragmentRendezVousComplet.afficherNomDeuxiemeJoueur(deuxiemeJoueur.getPrenom());
        
        fragmentRendezVousComplet.afficherScore(texteDuScore());
        
        fragmentRendezVousComplet.installerMessages(getIdRendezVous(), 
                                                    getPremierJoueur().getId(),
                                                    deuxiemeJoueur.getId());
    }
    
    @Override
    public String toString() {
        return getPremierJoueur().getPrenom() + " Vs " + deuxiemeJoueur.getPrenom();
    }

    @Override
    protected boolean siFragmentDuBonType(FragmentRendezVous fragmentRendezVous) {
        return fragmentRendezVous.getClass().equals(FragmentRendezVousComplet.class);
    }

    public void envoyerMsgCreerPartie() {

        Ntro.newMessage(MsgCreerPartie.class)
            .setModelSelection(ModelePartie.class, getIdRendezVous())
            .setIdRendezVous(getIdRendezVous())
            .setPremierJoueur(getPremierJoueur())
            .setDeuxiemeJoueur(deuxiemeJoueur)
            .send();

    }

    public void ajouterScore(Map<Position, Integer> scoreParPosition) {
        this.scoreParPosition = scoreParPosition;
    }
    
    private String texteDuScore() {
        String score = "";

        Integer scoreGauche = scoreParPosition.get(Position.GAUCHE);
        Integer scoreDroit  = scoreParPosition.get(Position.DROITE);
        
        if(scoreGauche != null && scoreDroit != null) {
            score = scoreGauche + "-" + scoreDroit;
        }

        return score;
    }

}
