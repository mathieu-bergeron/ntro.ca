public class EvtAfficherPartie extends Event {
    
    private String idPartie;

    public EvtAfficherPartie setIdPartie(String idPartie) {
        this.idPartie = idPartie;
        return this;
    }

    public EvtAfficherPartie appliquerA(SessionPong session) {
        
        session.memoriserPartieCourante(idPartie)
               .observerPartieCourante()
               .memoriserVueCourante(VuePartie.class);
         
        return this;

    }

    public EvtAfficherPartie appliquerA(VueRacine vueRacine, VuePartie vuePartie) {

         vueRacine.afficherSousVue(vuePartie);
         
         return this;

    }

}
