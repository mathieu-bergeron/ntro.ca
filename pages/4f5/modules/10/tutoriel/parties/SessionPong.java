public class SessionPong extends Session<SessionPong> {
    
    private Class<?>              vueCourante         = VueFileAttente.class;
    private Region                regionCourante      = Region.AMERIQUE;
    private String                idPartieCourante    = null;
    private List<Position>        positionsLocales    = new ArrayList<>();

    
    public SessionPong() {
        super();
        observerRegionCourante();
    }
    
    public SessionPong memoriserVueCourante(Class<?> vueCourante) {

        this.vueCourante = vueCourante;

        return this;
    }
    
    public SessionPong memoriserRegionCourante(Region region) {
        this.regionCourante = region;

        
        return this;
    }

    public SessionPong observerRegionCourante() {
        this.setModelSelection(ModeleFileAttente.class, regionCourante.name());
        
        return this;
    }

    public SessionPong memoriserPartieCourante(String idPartie) {
        this.idPartieCourante = idPartie;
        
        return this;
    }

    public SessionPong observerPartieCourante() {
        this.setModelSelection(ModelePartie.class, idPartieCourante);

        return this;
    }

    public SessionPong afficherRegionCourante(VueFileAttente vueFileAttente) {

        vueFileAttente.afficherRegionCourante(regionCourante.name());

        return this;
    }

    public SessionPong envoyerEvtPourAfficherVueCourante() {

      if(vueCourante.equals(VuePartie.class)) {
          
          Ntro.newEvent(EvtAfficherPartie.class)
              .setIdPartie(idPartieCourante)
              .trigger();

      }else {

          Ntro.newEvent(EvtAfficherFileAttente.class)
              .trigger();
      }
      
      return this;
    }
    
    public SessionPong envoyerMsgAjouterRendez() {
        Ntro.newMessage(MsgAjouterRendezVous.class)
            .setPremierJoueur(MaquetteJoueurs.joueurAleatoire(this.sessionId()))
            .send();
        
        return this;
    }

    public SessionPong envoyerMsgAjouterPoint(Position position) {
        if(estCePositionLocale(position)) {
            Ntro.newMessage(MsgAjouterPoint.class)
                .setPosition(position)
                .send();
        }
        
        return this;
    }

    public boolean estCePositionLocale(Position position) {
        boolean positionLocale = false;
        
        if(position != null) {
            positionLocale = positionsLocales.contains(position);
        }
        
        return positionLocale;
    }

    private List<Position> positionLocales(){
        return positionsLocales;
    }

    public SessionPong envoyerMsgRejoindreRendezVous(String idRendezVous) {
        Ntro.newMessage(MsgRejoindreRendezVous.class)
            .setIdRendezVous(idRendezVous)
            .setJoueur(MaquetteJoueurs.joueurAleatoire(this.sessionId()))
            .send();
        
        return this;
    }

    public SessionPong envoyerMsgRejoindrePartie() {

        for(Position position : positionsLocales) {

            envoyerMsgRejoindrePartie(position);
        }

        return this;
    }

    private void envoyerMsgRejoindrePartie(Position position) {
        Ntro.newMessage(MsgRejoindrePartie.class)
            .setModelSelection(ModelePartie.class, idPartieCourante)
            .setPosition(position)
            .send();
    }

    public SessionPong oublierPartieCourante() {
        idPartieCourante = null;
        positionsLocales.clear();
        
        return this;
    }

    public SessionPong memoriserPartieCourante(String idPartie,
                                               String idPremierJoueur,
                                               String idDeuxiemeJoueur) {

        oublierPartieCourante();
        this.idPartieCourante = idPartie;

        if(sessionId().equals(idPremierJoueur)) {
            positionsLocales.add(Position.GAUCHE);
        }

        if(sessionId().equals(idDeuxiemeJoueur)) {
            positionsLocales.add(Position.DROITE);
        }

        return this;
    }

    public SessionPong diffuserMsgActionAutreJoueur(EvtActionJoueur evtActionJoueur) {
        evtActionJoueur.diffuserMsgActionAutreJoueur(idPartieCourante);

        return this;
    }

    public SessionPong envoyerMsgQuitterPartie() {

        for(Position position : positionLocales()) {

            envoyerMsgQuitterPartie(position);
        }
        
        return this;
    }

    private void envoyerMsgQuitterPartie(Position position) {

        Ntro.newMessage(MsgQuitterPartie.class)
            .setModelSelection(ModelePartie.class, idPartieCourante)
            .setPosition(position)
            .send();

    }

    public boolean estCePartieCourante(String idPartie) {
        return idPartie.equals(idPartieCourante);
    }

}
