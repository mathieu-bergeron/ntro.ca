public class SessionPong extends Session<SessionPong> {

    // ...
    
    // ajouter
    private String                idPartieCourante    = null;

    // ...

    // ajouter
    public SessionPong memoriserPartieCourante(String idPartie) {
        this.idPartieCourante = idPartie;
        
        return this;
    }

    // ajouter
    public SessionPong observerPartieCourante() {
        this.setModelSelection(ModelePartie.class, idPartieCourante);

        return this;
    }
