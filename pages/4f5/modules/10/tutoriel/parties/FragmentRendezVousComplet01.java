public class FragmentRendezVousComplet extends FragmentRendezVous {
    
    @FXML
    private Label labelNomDeuxiemeJoueur;

    @FXML
    private Label labelScore;

    @Override
    public void initialize() {
        super.initialize();

        Ntro.assertNotNull(labelNomDeuxiemeJoueur);
        Ntro.assertNotNull(labelScore);
    }

    public void memoriserIdRendezVous(String idRendezVous) {
        // ...

        // ajouter
        installerEvtAfficherPartie(idRendezVous);
    }

    // ajouter
    private void installerEvtAfficherPartie(String idPartie) {

        getBoutonDebuterPartie().setOnAction(evtFx -> {
            
            Ntro.session(SessionPong.class)
                .memoriserPartieCourante(idPartie)
                .observerPartieCourante();
            
            Ntro.newEvent(EvtAfficherPartie.class)
                .setIdPartie(idPartie)
                .trigger();
        });
    }
