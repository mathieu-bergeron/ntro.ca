public class EvtAfficherPartie extends Event {
    
    // ajouter
    private String idPartie;

    // ajouter
    public EvtAfficherPartie setIdPartie(String idPartie) {
        this.idPartie = idPartie;
        return this;
    }

    // ajouter
    public EvtAfficherPartie appliquerA(SessionPong session) {
        
        session.memoriserPartieCourante(idPartie)
               .observerPartieCourante()
               .memoriserVueCourante(VuePartie.class);
         
        return this;
    }

    // ...
