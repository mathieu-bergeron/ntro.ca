---
title: "Plusieurs parties"
weight: 1
bookHidden: true
---

# Optionnel) plusieurs parties (créées dynamiquement)


## Créer une partie spécifique à la région

1. {{% link "/4f5/modules/09/tutoriel/creer_partie" "Faire les étapes pour créer la partie" %}}

1. Ajouter le `setModelSelection` à l'envoi du `MsgCreerPartie`

    ```java
    {{% embed src="./RendezVousComplet01.java" indent-level="1" %}}
    ```

1. Vérifier que les parties créées sont spécifiques à la région

    ```bash
    $ sh gradlew pong
    ```

    {{% animation src="/4f5/modules/10/tutoriel/parties/creer_partie01.mp4" %}}

    ```bash
    pong
    └── _storage
        ├── models
        │   ├── ModelePartie
        │   │   └── AMERIQUE¤1.json
    ```

## Observer la bonne partie

1. Utiliser `SessionPong` pour mémoriser et observer la partie courante

    ```java
    {{% embed src="./SessionPong01.java" indent-level="1" %}}
    ```

1. Ajouter un `idPartie` au `EvtAfficherPartie`

    ```java
    {{% embed src="./EvtAfficherPartie.java" indent-level="1" %}}
    ```

1. Transmettre le `idPartie` au moment de déclencher le `EvtAfficherPartie`

    ```java
    {{% embed src="./FragmentRendezVousComplet01.java" indent-level="1" %}}
    ```

1. Vérifier qu'on affiche la bonne partie

    ```bash
    $ sh gradlew pong
    ```

    {{% animation src="/4f5/modules/10/tutoriel/parties/creer_partie02.mp4" %}}
