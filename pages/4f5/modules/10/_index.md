---
title: "Module 10: plusieurs instances du même modèle"
weight: 140
draft: false
---


{{% pageTitle %}}

Objectif: supporter plusieurs file d'attentes (une par région)

{{% animation src="/4f5/modules/10/tutoriel/regions/region_fr.mp4" %}}

* {{% link "./theorie" "Théorie" %}}

    * plusieurs instances du modèle (Singleton Vs Collection)

* {{% link src="./tutoriel/" text="Tutoriel à faire avant les objectifs" %}}

    * supporter plusieurs parties (plusieurs instances du `ModelePartie`)

* {{% link src="./objectifs/" text="Objectifs pour finaliser le TP#2" %}} 

    * trois versions de votre modèle


ÉVALUATIONS
<table>

<tr>
<td>
<a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=306650">Auto-évaluation pour ce module</a>
</td>

<td>
lundi 15 avril 23h59
</td>


</tr>

<tr>
<td>

{{% link "/4f5/enonces/tp2/" "TP2" %}} 

</td>

<td>
mardi 16 avril 23h59
</td>

</tr>

<tr>
<td>

{{% link "/4f5/enonces/examen2/" "Examen2" %}}

<center
</td>

<td>
mardi 16 avril, dans le cours
</td>
</tr>
</table>


<br>
<div style="padding:5px;background:pink;border-style:dotted" >
<i>RAPPEL:</i> chaque étudiant.e est <strong>individuellement</strong> responsable de:
<ul>
<li>maintenir <i>son propre</i> dépôt Git
<li>présenter <i>son progrès</i> à la rencontre de travail
<li>faire son auto-évaluation
<li>s'assurer que son code est fonctionnel
</ul> 
<br>
<i>RAPPEL:</i> vous travaillez en équipe, mais les notes sont 100% <strong>individuelles</strong>
</div>
