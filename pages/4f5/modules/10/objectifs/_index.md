---
title: ""
weight: 1
bookHidden: true
---


# Objectifs: plusieurs versions du modèle


{{<excerpt class="max-width-75">}}

**IMPORTANT**

* Afficher une version `alice` et une version `bob` de **mon modèle**
* Modifier le modèle d'`alice` sans affecter celui de `bob` (et vice-versa)
* Le tout doit fonctionner en mode client/serveur


(NOTE: les `id` d'instance peuvent être autre chose que `alice` et `bob`, selon vos besoins)
{{</excerpt>}}

<br>
<br>

{{% animation "/4f5/modules/10/objectifs/objectifs10.mp4" %}}

* La version `alice` est à gauche
* La version `bob` est affichée dans les deux fenêtres à droite
* **NOTE**: je peux utiliser d'autres *id* au besoin
    * p.ex. `AMERIQUE` et `ASIE` ou encore `partie01` et `partie02`

<br>
<br>



1. Effectuer {{% link "../tutoriel/" "tutoriel" %}} pour comprendre comment

    * charger différentes versions de mon modèle personalisé

1. Vérifier que j'ai plusieurs version du modèle dans `_storage/models`, p.ex.

    ```bash
    └── models
        ├── ModeleMaPage
        │   ├── alice.json
        │   └── bob.json
    ```

1. S'assurer que mes noms d'attribut/méthode sont adéquats **pour mon projet**
    * on ne veut **pas de** `MaquetteParties` pour des pages paramètres!

1. Je pousse mon projet sur GitLab, p.ex:

        $ git add .
        $ git commit -a -m module10
        $ git push 

1. Je vérifie que mes fichiers sont sur GitLab

1. Je vérifie que projet est fonctionnel avec un `$ git clone` neuf, p.ex:

    ```bash
    # effacer le répertoire tmp s'il existe

    $ mkdir ~/tmp

    $ cd ~/tmp

    $ git clone https://gitlab.com/USAGER/4f5_prenom_nom

    $ cd 4f5_prenom_nom

    # démarre le serveur
    $ sh gradlew mon_projet:serveur

    # fênetre pour alice
    $ sh gradlew mon_projet:clientAlice

    # première fenêtre de bob
    $ sh gradlew mon_projet:clientBob

    # deuxième fenêtre de bob
    $ sh gradlew mon_projet:clientBob

        # S'assurer d'avoir:

        #     1. alice et bob ont un modèle différent

        #     2. on peut modifier le modèle d'alice sans affecter celui de bob

        #     3. on peut modifier le modèle de bob et les modifications 
        #        s'affichent dans les deux fenêtres de bob
    ```
