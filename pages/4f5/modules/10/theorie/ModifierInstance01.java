// observer l'instance AMERIQUE
Ntro.session(SessionPong.class)
    .setModelSelection(ModeleFileAttente.class, "AMERIQUE");

// comme AMERIQUE est observée
// le message ci-bas va modifier l'instance AMERIQUE
Ntro.newMessage(MsgAjouterRendezVous.class)
    .setPremierJoueur(/* ... */)
    .send();
