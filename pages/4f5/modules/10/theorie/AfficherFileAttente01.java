private static void afficherFileAttente(FrontendTasks tasks) {
    
    tasks.task("afficherFileAttente")
    
         .waitsFor(modified(ModeleFileAttente.class))  // va charger une instance du modèle
         
         .waitsFor(created(VueFileAttente.class))

         .executes(inputs -> {
             
             VueFileAttente              vueFileAttente = inputs.get(created(VueFileAttente.class));
             Modified<ModeleFileAttente> fileAttente    = inputs.get(modified(ModeleFileAttente.class));
             
             // le code ne change pas, mais il s'applique à une autre instance
             // selon l'instance observée en ce moment
             fileAttente.currentValue().afficherSur(vueFileAttente);

         });
}

