public class SessionPong extends Session<SessionPong> {

    // ...

    private Region                regionCourante      = Region.AMERIQUE;

    public SessionPong memoriserRegionCourante(Region region) {
        this.regionCourante = region;

        
        return this;
    }
    
    public SessionPong observerRegionCourante() {

        this.setModelSelection(ModeleFileAttente.class, regionCourante.name());
        
        return this;
    }
