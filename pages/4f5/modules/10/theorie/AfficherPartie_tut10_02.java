// dans le Frontal
.waitsFor(modified(ModelePartie.class, "aaaa"))

.executes(inputs -> {

    //...
    Modified<ModelePartie> modifiedPartie   = inputs.get(modified(ModelePartie.class, "aaaa"));

    // prêt à afficher l'instance aaaa.json du ModelePartie
    
});
