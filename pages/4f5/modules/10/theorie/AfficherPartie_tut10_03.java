// dans le Frontal
.waitsFor(modified(ModelePartie.class, idPartie))

.executes(inputs -> {

    //...
    Modified<ModelePartie> modifiedPartie   = inputs.get(modified(ModelePartie.class, idPartie));

    // prêt à afficher l'instance <idPartie>.json du ModelePartie
    
});
