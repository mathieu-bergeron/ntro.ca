---
title: "Théorie: singleton Vs collection"
bookHidden: true
---


{{% pageTitle %}}


{{% video src="/4f5/modules/10/theorie/presentation.mp4" %}}



## Singleton 

* Jusqu'à maintenant on a travaillé avec une seule instance de chaque modèle
    * (singleton veut dire «une seule instance»)

    ```bash
    └── models
        ├── ModeleFileAttente.json
        └── ModelePartie.json
    ```

## Collection

* Souvent, un modèle correspond plutôt à une collection d'instances

* Par exemple, on peut avoir une file d'attente pour chacune des régions ci-bas

* On a alors plusieurs version du modèle, chacune identifée par un *id* de région unique

    ```java
    {{% embed src="Region01.java" indent-level="1" %}}
    ```

* Dans le tutoriel 10, on transforme `ModeleFileAttente` en Collection:

    ```
    └── models
        ├── ModeleFileAttente
            ├── AFRIQUE.json
            ├── AMERIQUE.json
            ├── ASIE.json
            └── EUROPE.json
        └── ModelePartie
    ```

    * NOTES: 
        * `ModeleFileAttente` est maintenant un répertoire (une collection)
        * chaque fichier JSON du répertoire est une instance du modèle

## Observer une instance en particulier

* Pour observer l'instance `AMEMRIQUE`, faire p.ex.

    ```java
    {{% embed src="ObserverInstance.java" indent-level="1" %}}
    ```

* De façon plus générale, dans une méthode

    ```java
    {{% embed src="SessionPong01.java" indent-level="1" %}}
    ```


## Modifier une instance en particulier

* Si on envoi un message, c'est l'instance observée qui sera modifiée

    ```java
    {{% embed src="ModifierInstance01.java" indent-level="1" %}}
    ```

* On peut aussi préciser quelle instance modifier pour chaque message

    ```java
    {{% embed src="ModifierInstance02.java" indent-level="1" %}}
    ```

## Les tâches et le reste du code ne change pas

1. On a le même graphe de tâche, p.ex.

    <img src="frontend.png"/>

1. L'instance du modèle est chargée avant que le code s'exécute:


    ```java
    {{% embed src="AfficherFileAttente01.java" indent-level="1" %}}
    ```

