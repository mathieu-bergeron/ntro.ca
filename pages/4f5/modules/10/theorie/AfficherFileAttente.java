public class AfficherFileAttente {
    
    public static void creerTaches(FrontendTasks tasks) {

        tasks.taskGroup("AfficherFileAttente")
        
             .waitsFor("afficherVueFileAttente")

             .contains(subTasks -> {
                 
                 afficherFileAttente(subTasks);

                 changerRegion(subTasks);

             });
    }

    private static void afficherFileAttente(FrontendTasks tasks) {
        
        tasks.task("afficherFileAttente")
        
             .waitsFor(modified(ModeleFileAttente.class))
             
             .waitsFor(created(VueFileAttente.class))

             .executes(inputs -> {
                 
                 VueFileAttente              vueFileAttente = inputs.get(created(VueFileAttente.class));
                 Modified<ModeleFileAttente> fileAttente    = inputs.get(modified(ModeleFileAttente.class));
                 
                 fileAttente.currentValue().afficherSur(vueFileAttente);

             });
    }

    private static void changerRegion(FrontendTasks tasks) {

        tasks.task("changerRegion")

              .waitsFor(event(EvtChangerRegion.class))

              .executes(inputs -> {
                  
                  SessionPong      session          = Ntro.session();
                  EvtChangerRegion evtChangerRegion = inputs.get(event(EvtChangerRegion.class));
                  
                  evtChangerRegion.appliquerA(session);

              });
    }
}
