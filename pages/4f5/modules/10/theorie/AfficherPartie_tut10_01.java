public class AfficherPartie {
    
    public static void creerTaches(FrontendTasks tasks, String idPartie) {

        // ...
        
        tasks.taskGroup("AfficherPartie")

             .waitsFor(created(DonneesVuePartie.class))
             
             .contains(subTasks -> {

                observerModelePartie(subTasks, idPartie);

                // ...

             });
    }

    private static void observerModelePartie(FrontendTasks subTasks, String idPartie) {

        subTasks.task("observerModelePartie")

                .waitsFor(modified(ModelePartie.class, idPartie))

                .executes(inputs -> {

                    //...
                    Modified<ModelePartie> modifiedPartie   = inputs.get(modified(ModelePartie.class, idPartie));

                    // ...
                   
                });
    }
}
