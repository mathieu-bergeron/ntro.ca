public class Balle2d extends ObjetPong2d {

    // ...

    private void ajouterPoint(Cadran cadran) {
        MsgAjouterPoint msgAjouterPoint = Ntro.newMessage(MsgAjouterPoint.class);
        msgAjouterPoint.setMondePong2d(getWorld2d());
        msgAjouterPoint.setCadran(cadran);
        msgAjouterPoint.send();
    }
