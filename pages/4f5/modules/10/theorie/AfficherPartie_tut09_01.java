public class AfficherPartie {
    
    public static void creerTaches(FrontendTasks tasks) {

        // ...
        
        tasks.taskGroup("AfficherPartie")

             .waitsFor(created(DonneesVuePartie.class))
             
             .contains(subTasks -> {

                observerModelePartie(subTasks);

                // ...

             });
    }

    private static void observerModelePartie(FrontendTasks subTasks) {

        subTasks.task("observerModelePartie")

                .waitsFor(modified(ModelePartie.class))

                .executes(inputs -> {

                    //...
                    Modified<ModelePartie> modifiedPartie   = inputs.get(modified(ModelePartie.class));

                    // ...
                   
                });
    }
}
