public class ModifierPartie {
    
    public static void creerTaches(BackendTasks tasks, String idPartie) {
        
        tasks.taskGroup("ModifierPartie" + "" + idPartie)
        
             .waitsFor(model(ModelePartie.class, idPartie))
        
              .contains(subTasks -> {

                    // ...
                    
              });
    }
