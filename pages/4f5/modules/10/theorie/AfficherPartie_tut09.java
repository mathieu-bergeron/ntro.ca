public class AfficherPartie {
    
    public static boolean partieSurPause = true;
    
    public static void creerTaches(FrontendTasks tasks) {
        
        creerDonneesVuePartie(tasks);
        
        tasks.taskGroup("AfficherPartie")

             .waitsFor(created(DonneesVuePartie.class))
             
             .contains(subTasks -> {

                observerModelePartie(subTasks);
                
                reagirActionJoueur(subTasks);

                reagirActionAutreJoueur(subTasks);

                reagirClicSouris(subTasks);

                prochaineImagePartie(subTasks);

             });
    }

    private static void creerDonneesVuePartie(FrontendTasks tasks) {

        tasks.task(create(DonneesVuePartie.class))

             //.waitsFor(snapshot(ModelePartie.class))
             .waitsFor("Initialisation")
        
             .executesAndReturnsCreatedValue(inputs -> {

                       //Snapshot<ModelePartie> snapshotPartie = inputs.get(snapshot(ModelePartie.class));
                       
                       //ModelePartie modelePartie = snapshotPartie.currentValue();

                       DonneesVuePartie donneesVuePartie = new DonneesVuePartie();
                     
                       //modelePartie.copierDonneesDans(donneesVuePartie);
                                       
                       return donneesVuePartie;

             });
    }

    private static void prochaineImagePartie(FrontendTasks subTasks) {

        subTasks.task("prochaineImagePartie")

                .waitsFor(clock().nextTick())
                
                .waitsFor(created(VuePartie.class))

                .waitsFor(created(DonneesVuePartie.class))
        
                .executes(inputs -> {
                    
                    if(!partieSurPause) {

                        Tick             tick             = inputs.get(clock().nextTick());
                        DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                        VuePartie        vuePartie        = inputs.get(created(VuePartie.class));
                        
                        donneesVuePartie.reagirTempsQuiPasse(tick.elapsedTime());
                        donneesVuePartie.afficherSur(vuePartie);
                    }
                });
    }

    private static void reagirActionJoueur(FrontendTasks subTasks) {

        subTasks.task("reagirActionJoueur")

                .waitsFor(event(EvtActionJoueur.class))
                
                .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    EvtActionJoueur  evtActionJoueur  = inputs.get(event(EvtActionJoueur.class));
                    
                    boolean nouvelleAction = evtActionJoueur.appliquerA(donneesVuePartie);
                    
                    if(nouvelleAction) {
                        evtActionJoueur.diffuserMsgActionAutreJoueur();
                    }

                });
    }

    private static void reagirActionAutreJoueur(FrontendTasks subTasks) {

        subTasks.task("reagirActionAutreJoueur")

                .waitsFor(message(MsgActionAutreJoueur.class))
                
                .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie         = inputs.get(created(DonneesVuePartie.class));
                    MsgActionAutreJoueur msgActionAutreJoueur = inputs.get(message(MsgActionAutreJoueur.class));
                    
                    msgActionAutreJoueur.appliquerA(donneesVuePartie);
                });
    }

    private static void reagirClicSouris(FrontendTasks subTasks) {

        subTasks.task("reagirClicSouris")

                .waitsFor(event(EvtClicSouris.class))
                
                .executes(inputs -> {
                    
                    DonneesVuePartie donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    EvtClicSouris    evtClicSouris    = inputs.get(event(EvtClicSouris.class));

                    evtClicSouris.appliquerA(donneesVuePartie);

                });
    }
    
    private static void observerModelePartie(FrontendTasks subTasks) {

        subTasks.task("observerModelePartie")

                .waitsFor(modified(ModelePartie.class))

                .executes(inputs -> {
                    
                    VuePartie              vuePartie        = inputs.get(created(VuePartie.class));
                    DonneesVuePartie       donneesVuePartie = inputs.get(created(DonneesVuePartie.class));
                    Modified<ModelePartie> modifiedPartie   = inputs.get(modified(ModelePartie.class));
                    
                    ModelePartie modelePartie = modifiedPartie.currentValue();
                    
                    MaquetteSession.memoriserCadranCourant(modelePartie.getIdPremierJoueur(), modelePartie.getIdDeuxiemeJoueur());

                    modelePartie.afficherInfoPartieSur(vuePartie);
                    
                    if(modelePartie.estPlusRecenteQue(donneesVuePartie)) {

                        modelePartie.copierDonneesDans(donneesVuePartie);
                    }

                });
    }
}
