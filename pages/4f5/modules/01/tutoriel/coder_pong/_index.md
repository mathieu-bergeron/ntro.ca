---
title: "Commencer à coder `pong`"
bookHidden: true
---

{{% pageTitle %}}

* En VSCode, ajouter la classe suivante au projet `pong`
    * *Package*: `pong`
    * Nom de la classe: `AppPong`

* Ajuster la signature de la classe

    ```java
    {{% embed src="./AppPong01.java" 
        indent-level="1" 
        first-line="1"
        last-line="2"
    %}}
    ```

* Utiliser {{% key "Ctrl+1" %}} pour importer `NtroAppFx`

    <img src="ctrl_1_import.png"/>

* Utiliser {{% key "Ctrl+1" %}} pour ajouter les méthodes obligatoires

    <img src="ctrl_1_methods.png"/>

* Retirer les exceptions que VSCode ajoute

    ```java
    // Retirer chaque ligne de ce genre
    throw new UnsupportedOperationException("Unimplemented method 'registerFrontend'");
    ```

* Ajouter l'instruction suivante à la méthode `main`

    ```java
    {{% embed src="./AppPong01.java" 
        indent-level="1" 
        first-line="3"
        last-line="5"
    %}}
    ```

* Exécuter le projet en VSCode

* On peut aussi exécuter le projet en GitBash

    ```bash
    $ sh gradlew pong
    ```

* Dans les deux cas, la console devrait afficher

    <img src="console_eclipse.png"/>

    * **SVP** ignorer le premier avertissement en rouge

* Dans les deux cas, on va avoir la fenêtre suivante:

    <img src="fatal.png"/>

    




