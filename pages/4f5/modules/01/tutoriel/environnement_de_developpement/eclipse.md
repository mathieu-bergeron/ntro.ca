---
title: "Optionnel: installer VSCode"
bookHidden: true
---

{{% pageTitle %}}


## Installer VSCode

* Visiter <a href="https://www.eclipse.org/downloads/" target="_blank">https://www.eclipse.org/downloads/</a>

* Télécharger et exécuter la version *Download x86_64*

    * sélectionner *VSCode IDE for Java Developers* 


## Configurer VSCode pour le cours

1. Démarrer VSCode
    * Choisir un Workspace sur le `C:\`
    * (**jamais** le même répertoire que les projets ou le dépôt Git)

1. S'assurer qu'VSCode utiliser ma version du JDK
	* *Window* => *Preferences*
	* *Java* => *Installed JREs*

		<center>
			<img class="small-figure" src="eclipse_jre.png">
		</center>

	* Idéallement, VSCode utilise **uniquement** ma version du JDK

	* Faire *Remove* sur tous les JDK sauf ma version

	* Si ma version du JDK n'est pas là, je l'ajoute:
		* *Add* =>
		* *Standard VM* => *Next*
		* Sélectionner le répertoire racine de mon *JDK*, p.ex: 
			* `C:\Program Files\Java\jdk-XXX`
		* *Finish*
		* *Apply and Close*

	* S'assurer que ma version du  JDK est le défaut (en gras)
    


