---
title: "Installer l'environnement de développement"
bookHidden: true
---

{{% pageTitle %}}

## Installer Git

* Visiter <a href="https://git-scm.com/download/win" target="_blank">https://git-scm.com/download/win</a>
* Télécharger et exécuter la version *64-bit Git for Windows Setup*
    * utiliser les options par défaut, **sauf** choisir `nano` si je n'aime pas `vim`:
        <img width="50%" src="git_vim.png"/>

## Configurer Git

* Ouvrir GitBash

* En GitBash, ajouter mon nom

    ```bash
    $ git config --global user.name "PRENOM NOM"
    ```

* Ajouter mon courriel

    ```bash
    $ git config --global user.email "USAGER@SERVEUR.CA"
    ```

* *(optionnel)* Spécifier `notepad` comme éditeur pour les commentaires de commit

    ```bash
    $ git config --global core.editor "notepad.exe"
    ```

* Choisir le mode de fusion par défaut

    ```bash
    $ git config --global pull.rebase false
    ```


* Vérifier les configurations

    ```bash
    $ git config --list
    ```

## Installer un JDK, version 11 ou plus

* Version recommandée: JDK 17
    * (testé jusqu'à jdk 23)

* Visiter <a href="https://www.oracle.com/ca-en/java/technologies/downloads/#jdk17" target="_blank">https://www.oracle.com/ca-en/java/technologies/downloads/#jdk17</a>

* Télécharger et installer un JDK

<!--
* Ouvrir les variables d'environnement de Windows
    * {{% key "Fenetre+I" %}} pour ouvrir les paramètres
    * Système =>
        * Informations systèmes =>
        * Informations systèmes *encore une fois* =>
        * Paramètres système avancés =>
        * Variables d'environnement

* Ajouter la variable d'environnement `JAVA_HOME`
    * Cliquer sur *Nouvelle*

        <img class="small-figure" src="variables01.png"/>

    * Saisir `JAVA_HOME`

    * Cliquer sur Parcourir le répertoire => naviguer jusqu'au répertoire `C:\Program Files\Java\jdk-XXX`
        * où `XXX` correspond à la version du JDK que j'ai installé

        <img class="small-figure" src="variables02.png"/>

    * Cliquer sur *Ok*

* Modifier la variable d'environnement `Path`

    * Sélectionner `Path` et cliquer sur *Modifier*

        <img class="small-figure" src="variables03.png"/>

    * Cliquer sur *Nouveau*

        <img class="small-figure" src="variables04.png"/>

    * Saisir `%JAVA_HOME%\bin`

    * Sélectionner `%JAVA_HOME%\bin` et cliquer sur *Déplacer vers le haut*

        <img class="small-figure" src="variables05.png"/>

* Ouvrir un GitBash et vérifier la version de Java

    ```bash
    $ echo $JAVA_HOME
        # doit afficher le chemin vers ma version du JDK

    $ java --version
        # doit afficher version 11 ou plus
    ```

-->

## Installer VSCode

1. https://code.visualstudio.com/Download
    * exécuter le fichier `VSCodeUserSetup-x64-XXX.exe`
    * accepter les termes et cliquer sur *Suivant*
    * utiliser les options par défaut


