---
title: "Ajouter les fichiers de départ"
bookHidden: true
---

{{% pageTitle %}}

## Cloner le dépôt afin d'avoir une copie locale

* Ouvrir un GitBash dans un répertoire *normal* 
    * c-à-d **pas** dans un Workspace, **pas** dans un projet Java

* Cloner mon dépôt Git 

    ```bash
    $ git clone https://gitlab.com/USAGER/DEPOT
    ```

## Ajouter les fichiers de départ au dépôt Git local

* Télécharger {{% download "4f5_depart.zip" "4f5_depart.zip" %}}

* Placer le fichier `.zip` à la racine de mon dépôt Git

* Extraire tout **directement à la racine** du dépôt Git

    * Via GitBash

        ```bash
        $ unzip 4f5_depart.zip
        ```
    
    * Ou avec Windows:
        
        * Clique-droit sur le fichier => Extraire tout

            <img class="small-figure" src="extraire_tout00.png"/>

        * Dans le chemin, supprimer le répertoire `4f5_depart` ajouté par Windows

            <img class="figure" src="extraire_tout01.png"/>

        * Cliquer sur *Extraire*

            <img class="figure" src="extraire_tout02.png"/>

* Vérifier que votre dépôt Git contient les fichiers suivant:

    <img class="figure" src="fichiers_de_depart01.png"/>

## Pousser les fichiers sur le serveur GitLab

* Ouvrir GitBash à la racine du dépôt Git

* Ajouter les fichiers à Git et les pousser ver GitLab

    ```bash
    $ git add .
    $ git commit -m"fichiers de depart"
    $ git push
    ```

* Vérifier que les fichiers sont sur GitLab

    <img class="figure" src="fichiers_de_depart02.png"/>

