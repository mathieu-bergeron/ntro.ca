---
title: "Créer le dépôt Git"
bookHidden: true
---

{{% pageTitle %}}

## Se connecter à GitLab

* Au besoin, {{% link "./inscription" "je m'inscris à GitLab" %}}

## Créer le dépôt Git pour la session

1. Sur GitLab, cliquer sur *New project*

    <img class="small-figure" src="./new_project.png"/>

1. Choisir *Create blank project*

    <img class="small-figure" src="./blank_project.png"/>

1. Remplir le formulaire

    <img class="figure" src="./formulaire.png"/>

    * le nom de projet doit être `4f5_prenom_nom`
        * remplacer `prenom` par mon prénom
        * remplacer `nom` par mon nom
    * pour le reste, utiliser les options par défaut:
        * doit être coché: *Private*
        * doit être coché: *Initialize repository with a README*

1. Cliquer sur *Create project*

    <img class="small-figure" src="./create_project.png"/>

1. Sur la page du projet, cliquer sur *Code*

    <img class="small-figure" src="./code.png"/>

    <!--
    * copier l'URL SSH
        <img class="small-figure" src="./url_ssh.png"/> 
    -->

    * copier l'URL HTTPS

        <img class="small-figure" src="./url_https.png"/>


    * coller cet URL dans un document pour le mémoriser

## Partager mon dépôt Git avec le prof

* Sur la page du projet, cliquer sur *Gestion* => *Membres*

    <img class="small-figure" src="./members.png"/>

* Cliquer sur *Inviter des membres*

    <img class="figure" src="./invite_members.png"/>

* Saisir et sélectionner `mathieu-bergeron`

    <img class="small-figure" src="./mathieu-bergeron.png"/>

* Sous *Select a role*, sélectionner *Developer* 

    <img class="small-figure" src="./developer.png"/>

* Cliquer sur *Invite*

    <img class="small-figure" src="./invite.png"/>
