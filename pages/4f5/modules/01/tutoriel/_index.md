---
title: "Tutoriel 1: créer les projets"
bookHidden: true
---


# Tutoriel 1: créer les projets

<center>
<div style="background-color:orange;width:80%;border:2px dashed black;padding:10px">
<strong>IMPORTANT</strong>
<div style="text-align:left">

* Il faut s'assurer de renommer <strong>tous</strong> les noms bidons 
    * p.ex. `4f5_prenom_nom`, `mon_projet`, `AppMonProjet`, etc.

</div>
</div>
</center>



<!--
<center>
<video width="50%" src="presentation.mp4" type="video/mp4" controls playsinline>
</center>
-->

1. Sur mon ordinateur seulement
    * {{% link "./environnement_de_developpement" "installer l'environnement de développement" %}}

1. Une seule fois dans la sesssion
    * {{% link "./creer_depot_git" "créer mon dépôt Git pour le cours" %}}
    * {{% link "./fichiers_de_depart" "copier les fichiers de départ dans mon dépôt Git" %}}


<!--
    * {{% link "./tester_environnement" "tester l'environnement de développement" %}}
    -->

1. Une seule fois avant de travailler

    * {{% link "./preparer_poste" "cloner mon dépôt Git et ouvrir le workspace VSCode" %}}
    * (à répéter pour travailler sur un autre poste)

1. Commencer à {{% link "./coder_pong" "coder le projet `pong`" %}}


