---
title: "Cloner le dépôt Git et créer les projets VSCode"
bookHidden: true
---

{{% pageTitle %}}


## Cloner le dépôt afin d'avoir une copie locale

* Ouvrir un GitBash dans un répertoire *normal* 
    * c-à-d **pas** dans un Workspace, **pas** dans un projet Java

* Cloner mon dépôt Git 

    ```bash
    $ git clone https://gitlab.com/USAGER/DEPOT
    ```

## Ouvrir en VSCode et vérifier le projet


* En VSCode, faire
    * *Fichier* => *Ouvrir l'espace de travail à partir du fichier*
    * Sélectionner le fichier `4f5.code-workspace`

* Dans la sections *Java Projects*, s'assurer d'avoir
    * projet `mon_projet` (à renommer bientôt!)
    * projet `pong`

    <img src="java_projects.png"/>

