---
title: "Tester l'environnement de développement"
bookHidden: true
---

{{% pageTitle %}}

* Ouvrir un GitBash dans mon dépôt Git local

* Exécuter la commande suivante

    ```bash
    $ sh gradlew testfx
    ```

* Devrait afficher la fenêtre suivante:


{{% animation "/4f5/modules/01/tutoriel/tester_environnement/testfx.mp4" %}}




