---
title: "Objectif: créer mon projet"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

<center>
<div style="background-color:orange;width:80%;border:2px dashed black;padding:10px">
<strong>IMPORTANT</strong>
<div style="text-align:left">

* Il faut d'abord faire le tutoriel: on utilise le même dépôt Git
* Il faut s'assurer de renommer <strong>tous</strong> les noms bidons 
    * p.ex. `4f5_prenom_nom`, `mon_projet`, `AppMonProjet`, etc.

</div>
</div>
</center>

## Configurer le projet Gradle

1. Dans mon dépôt Git, renommer le répertoire `mon_projet` vers le nom de mon projet
    * p.ex. `serpents_echelles`

1. Dans `settings.gradle`, renommer
    * ligne 4: `mon_projet` vers le nom de mon projet

1. En VSCode, recharger le workspace

    * Faire *Clean Workspace*

        <img src="clean_workspace01.png"/>

    * Cliquer sur *Reload and delete*

        <img src="clean_workspace02.png"/>

    * Dans la sections *Java Projects*, s'assurer d'avoir
        * mon application (avec le bon nom de projet)
        * projet `pong`

        <img src="java_projects_renomme.png"/>


1. Dans `mon_projet/build.gradle`, renommer

    * ligne 23: `mon_projet.AppMonProjet` vers le vrai nom de paquet et de classe de mon projet

        * p.ex. `serpents_echelles.AppSerpentsEchelles`

    * ligne 63: `mon_projet` vers p.ex. `serpents_echelles`

    * ligne 67: `mon_projetFr`  vers p.ex. `serpents_echellesFr`

    * ligne 69: `mon_projetEn` vers p.ex. `serpents_echellesEn`


{{<excerpt class="warning, max-width-75">}}

**EN GÉNÉRAL**

* Quand le prof utilise un nom bidon, je dois adapter à mon projet en préservant le même style 
    * exemples:
        * `NOM_BIDON` devient `SERPENTS_ECHELLES`
        * `nom_bidon` devient `serpents_echelles`
        * `NomBidon` devient `SerpentsEchelles`
        * `<nom bidon>` devient `serpents échelles`


{{</excerpt>}}


## Commencer à coder mon projet

1. En VSCode:
    * Créer ma classe `AppMonProjet` dans le paquet `mon_projet`
    * Compléter la classe (s'inspirer du tutoriel)

1. En VSCode, exécuter le projet

1. Vérifier que la console affiche

    <img class="figure" src="console_eclipse.png"/>

    * **SVP** ignorer le premier avertissement en rouge

1. Vérifier que la fenêtre affiche:

    <img src="fatal.png"/>



## Remettre sur GitLab et vérifier la remise

1. Pousser mon projet sur GitHub, p.ex:

        $ git add .
        $ git commit -a -m"premier commit pour <nom de mon projet>"
        $ git push 

1. Vérifer que mes fichiers sont sur GitLab

1. Vérifier que mon projet est fonctionnel avec un `$ git clone` neuf, p.ex:

        $ mkdir ~/tmp
        $ cd ~/tmp
        $ git clone https://gitlab.com:USAGER/4f5_prenom_nom
        $ cd 4f5_prenom_nom

        $ sh gradlew mon_projet

            # Doit afficher à la console

                [INFO] Ntro version 0.1
                [INFO] Locale: 'fr_CA'

                [FATAL] No Frontend. Please register a Frontend


            # Doit afficher la fenêtre rouge "Please register a frontend"
