---
title: "Module 1: créer le projet"
weight: 10
draft: false
---


{{% pageTitle %}}

<!--
<center>
<video width="50%" src="presentation.mp4" type="video/mp4" controls playsinline>
</center>
-->


* {{% link "/4f5/presentation/" "Description du cours" %}}
    * (remplace la théorie pour cette semaine)

* {{% link "./tutoriel/" "Tutoriel à faire avant les objectifs" %}}
    * créer mon dépôt Git et commencer à coder `pong`

<br>

* **Former une équipe de 4**
    * choisir un jeu et avertir le prof ({{% link "/4f5/presentation/equipes" "JEUX DÉJÀ CHOISIS" %}})

<br>

* {{% link "./objectifs/" "Objectifs pour avancer le TP#1" %}}
    * créer mon projet et commencer à coder mon application

* REMISES: 
    * URL de mon dépôt <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/questionnaire/view.php?id=291483">Git sur Moodle</a>.
    * <a href="https://cmontmorency.moodle.decclic.qc.ca/mod/quiz/view.php?id=291484">auto-évaluation pour ce module</a>

<br>
<div style="padding:5px;background:pink;border-style:dotted" >
<i>RAPPEL:</i> chaque étudiant.e est <strong>individuellement</strong> responsable de:
<ul>
<li>maintenir <i>son propre</i> dépôt Git
<li>présenter <i>son progrès</i> à la rencontre de travail
<li>faire son auto-évaluation
<li>s'assurer que son code est fonctionnel
</ul> 
<br>
<i>RAPPEL:</i> vous travaillez en équipe, mais les notes sont 100% <strong>individuelles</strong>
</div>
