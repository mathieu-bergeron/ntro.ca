---
title: "4F5: Application client/serveur"
weight: 20
bookCollapseSection: true
---

{{% pageTitle %}}

## {{% link "./presentation/projets2024" "Projets vedettes 2024" %}}

## {{% link "./presentation/projets2023" "Projets vedettes 2023" %}}

## {{% link "./presentation" "Présentation du cours" %}}

<!--

## Migration vers Ntro v03

* Suivre les {{% link "/4f5/modules/07/migration_v03" "instructions ici" %}}

## Migration vers Ntro v02

* Télécharger {{% download "maj_v02.zip" "maj_v02.zip" %}}

* Placer le fichier **à la racine** de votre dépôt Git

* Ouvrir un GitBash et faire la commande suivante pour décompresser l'archive

    ```bash
    $ unzip -o maj_v02.zip

    # -o veut dire d'écraser les fichiers existants
    ```

* Vérifier que `gradle.properties` contient maintenant `version=v02`

* Faire la commande suivante:

```
$ sh scripts/ajouter_atelier.sh
```

* Pousser cette configuration sur GitLab

```
$ git add .
$ git commit -a -m"maj v02"
$ git push
```

## En cas d'erreur réseau

Cette erreur Gradle est en fait une erreur de connexion réseau (malheureusement fréquente via Wi-Fi ou dans une VM):

```
Could not determine the dependencies of task ':pong:local'.
> Could not resolve all dependencies for configuration ':pong:runtimeClasspath'.
   > Could not list available versions for Git repository at https://gitlab.com/mathieu-bergeron/ca.ntro.git.
```

### Méthode 1 pour régler l'erreur

Faire la commande suivante

```
$ sh scripts/ajouter_atelier.sh
```

Puis ajouter `--offline` à vos commandes `gradlew`, par exemple:

```
$ sh gradlew pong:local --offline
```

Si l'erreur ci-haut persiste, faire la méthode 2.

### Méthode 2 pour régler l'erreur


Pour régler cette erreur:

* Télécharger le fichier {{% download "dot_gradle.zip" "dot_gradle.zip" %}}

* Placer le `.zip` **à la racine** du dépôt Git

* Ouvrir un GitBash et faire la commande suivante pour décompresser l'archive

    ```
    $ unzip -o dot_gradle.zip

    # -o veut dire d'écraser les fichiers existants
    ```

* Ajouter `--offline` à la commande pour exécuter le projet

    ```
    $ sh gradlew mon_projet:localFr --offline
    ```

-->
