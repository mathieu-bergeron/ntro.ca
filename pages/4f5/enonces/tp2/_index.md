---
title: "TP2"
weight: 30
draft: false
---

# Énoncé TP2

## Instruction de remise TP#2

1. Dans mon dépôt Git, je m'assure d'avoir le répertoire `tp1` qui contient:

    * Un fichier `README.md` avec les commandes à faire pour lancer le jeu en mode client/serveur, p.ex:

            $ sh gradlew mon_projet:serveur
            $ sh gradlew mon_projet:clientAlice
            $ sh gradlew mon_projet:clientBob
    
    * Mes graphes de tâches (`frontal.png` et `dorsal.png`)

    * Un modèle contenant une valeur personalisée (fichier `.json` et `.png`)

    * Si je l'ai modifiée, la version la plus récente de ma maquette (document de conception)

1. J'ouvre un GitBash à la racine de mon dépôt Git 

1. Je fais un commit avec le commentaire `TP2`

        $ git add .
        $ git commit -a --allow-empty -mTP2
        $ git push

1. Je m'assure que mon commit est bien sur GitLab

    * y compris le répertoire `tp2`

## Grille de correction TP#2

### 20pts) Exécution à partir d'un `$ git clone neuf`

* Le projet doit être fonctionnel à partir d'un `$ git clone neuf`

        $ mkdir ~/tmp

        $ cd ~/tmp

        $ git clone git@github.com:USAGER/4f5_prenom_nom

        $ cd 4f5_prenom_nom

        # si nécessaire
        $ git checkout <le commit le plus récent avec commentaire TP2>    

        # dans un 1er GitBash
        $ sh gradlew mon_projet:serveur

        # dans un 2ième GitBash
        $ sh gradlew mon_projet:clientAlice

        # dans un 3ième GitBash
        $ sh gradlew mon_projet:clientAlice

            # Doit afficher 2 clients synchronisés via le serveur
            # On peut naviguer vers votre page personnalisée
            # Sur votre page, on peut ajouter une valeur sur les deux clients
            # Au moins une valeur est affichée dans un fragment
            # L'affichage s'adapte à la taille de la fenêtre (tailles élastiques) et/ou l'esthétique est très travaillée
            # On peut naviguer vers une vue contenant un objet2d en mouvement


### 40pts) Fonctionnalités

* 10pts) On peut ajouter/retirer/modifier des données p.ex. avec un bouton

* 10pts) Les données s'affichent dans un fragment

* 10pts) On peut naviguer vers une Vue contenent un objet2d en mouvement, puis revenir

* 10pts) Les données s'affichent dans les deux clients
    * BONUS: votre modèle personalisé est une Collection plutôt qu'un Singleton (module 10)

### 40pts) Respect de la méthodologie Ntro et qualité du code

* 20pts) Respect de la méthodologie Ntro, p.ex.
    * graphes de tâches adéquat
    * utilisation de messages
    * modification du modèle **uniquement dans le dorsal**
    * observation d'un modèle **dans le frontal**
    * etc.

* 10pts) Noms pertinents pour les classes, méthodes et attributs

* 10pts) Qualité du code (respect des {{% download "/4f5/presentation/NormesProgrammationJava.pdf" "normes de programmation" %}})


### Ré-évaluation des auto-évalutions

* Le prof va compiler les notes des auto-évaluations et ajuster au besoin

* Ces notes seront disponibles en même temps que celle du TP#2

