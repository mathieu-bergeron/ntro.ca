public static void creerTaches(BackendTasks tasks, String idVersion) {
    declencherObservation(tasks, idVersion);

    // ...
}

private static void declencherObservation(BackendTasks tasks, String idVersion) {
    tasks.task("declencherObservation")

         .waitsFor(model(ModeleFileAttente.class, idVersion))

         .waitsFor(message(MsgDeclencherObservation.class, idVersion))

         .executes(inputs -> {

             // rien à faire; une observation est envoyée
             // automatiquement après l'exécution de la
             // tâche
             
         });
}
