---
title: "Migration vers Ntro v10"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Problème: une tâche `modified` ne s'exécute pas toujours du premier coup

Le problème affecte principalement:

* Le mode client/serveur
* Avec les collections, p.ex. `modified(ModeleFoo.class, idVersion)`
* La première fois que la page s'affiche

    {{% animation src="/4f5/enonces/projet/migration_v10/bogue_v10.mp4" %}}

    * **NOTES**: 
        * après une action usager, la page fonctionne normalement
        * (une nouvelle observation est envoyée au client et le `modified` s'exécute)


## Solution de contournement: forcer le dorsal à ré-envoyer une observation

1. Créer un message du genre `MsgDeclencherObservation`

1. Dans le dorsal, ajouter une tâche qui 
    * `waitsFor` le modèle a rafraîchir
    * `waitsFor` le message
    * p.ex.

        ```java
        {{% embed src="Tache.java" indent-level="2" %}}
        ```

1. Dans le frontal, envoyer le message avant d'afficher
    * dès le début:

        ```java
        {{% embed src="FrontalPong.java" indent-level="2" %}}
        ```
    * et/ou lors de la navigation:

        ```java
        {{% embed src="Navigation.java" indent-level="2" %}}
        ```

## Autre solution: migration à `v10`

Je pense avoir réglé le problème, qui était lié au délai de connexion au serveur
(le frontal commençait à s'exécuter en mode local en attendant la connexion, ce qui n'est pas le comportement désiré).

Pour migrer à `v10`:

1. Modifier `gradle.properties` et s'assurer d'avoir:

    ```groovy
    version=v10
    ```

1. Re-créer les projets VSCode

    ```bash
    $ sh scripts/ajouter_atelier.sh
    ```

    * **NOTES**: 
        * c'est normal d'avoir `BUILD FAILED`
        * continuer les étapes ci-bas pour corriger les erreurs

## Comme pour `v09`: remplacer `drawOn` par `drawOnWorld`

* `drawOn(ResizableWorld2dCanvasFx convas)` doit devenir `drawOnWorld(GraphicsContext gc)`

    ```java
    {{% embed src="./Palette2d.java" indent-level="1" %}}
    ```
