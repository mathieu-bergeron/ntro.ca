public class Navigation {
    
    public static void creerTaches(FrontendTasks tasks) {

        tasks.taskGroup("Navigation")
        
             .waitsFor("Initialisation")

             .andContains(subTasks -> {

                 afficherVueFileAttente(subTasks);

                 // ...
                 
             });
    }

    // ...

    private static void afficherVueFileAttente(FrontendTasks tasks) {

        tasks.task("afficherVueFileAttente")

              .waitsFor(created(VueFileAttente.class))
        
              .waitsFor(event(EvtAfficherFileAttente.class))
              
              .thenExecutes(inputs -> {

                  VueRacine      vueRacine      = inputs.get(created(VueRacine.class));
                  VueFileAttente vueFileAttente = inputs.get(created(VueFileAttente.class));
                  
                  vueRacine.afficherSousVue(vueFileAttente);

                  Ntro.newMessage(MsgDeclencherObservation.class, MaquetteSession.idVersion).send();
              });
    }
}
