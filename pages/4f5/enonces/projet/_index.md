---
title: "Projet de fin de session"
weight: 60
draft: false
---

{{% pageTitle %}}


## Instructions

* Date de remise à la dernière journée de cours

* À remettre sur GitLab

    * Code 

    * Fichier `README.md` d'au plus 50 lignes

        * description courte de l'application

        * fonctionnalités que **vous avez implantées**

## Projet par défaut: finaliser la page personnalisée

<center>
<div style="background-color:orange;width:80%;border:2px dashed black;padding:10px">
<div style="text-align:left">
<ul>
<li>Finaliser ma page personnalisée <strong>au mieux de mes habiletés</strong>
<ul>
    <li>intégrer les éléments suivants uniquement <strong>si pertinent</strong> 
    <ul>
        <li>une List ou un Map dans le Modèle
        <li>un Fragment dans la Vue
        <li>des tailles élastiques
        <li>un Canvas et/ou des animations
        <li>une Collection de modèles plutôt qu'un Singleton
    </ul>
    <li>je peux choisir de ne pas les utiliser
    </ul>
<br>
<li>Par contre, <strong>il faut suivre</strong> la méthodologie <code>Ntro</code>
</ul>
</ul>
</div>
</center>

* Finaliser la Vue personnalisée

    * Au moins 10 éléments graphiques *distincts*

        * (bouton, texte, zone de saisi de texte, etc.)

    * Au moins 5 règles CSS *évidentes*

* Finaliser le Modèle personnalisé

    * Au moins 5 éléments *distincts* à sauvegarder
    
* Implanter au moins 5 fonctionnalités, c-à-d

    * ajouter/modifier/supprimer une donnée du modèle via
        * un bouton (ou autre événements usager)
        * ET un message envoyé au dorsal

    * afficher la nouvelle version du modèle

    * (vérifier que le modèle est réellement modifié dans le `.json`)


## Projet personnalisé

1. Doit être **approuvé par le prof** au plus tard **lundi 24 avril**

    * **doit suivre** la méthologie du cours, en partculier:
        * utiliser une classe `Maquette` pour implanter ce qui n'est pas supporté par la librairie Ntro
        * documenter toute solution de contournement que vous avez inventée, p.ex.

            ```java
            {{% embed src="Contournement01.java" indent-level="3" %}}
            ```

    * **au besoin**, ré-implanter certaines classes comme p.ex. `World2dFx` ou `ResizableWorld2dCanvasFx`
        * les sources sont ici: https://gitlab.com/mathieu-bergeron/ca.ntro




1. Pour que le projet soit approuvé il faut

    * que le niveau de difficulté soit équivalent au projet par défaut

    * que l'équipe et/ou l'étudiant·e ait bien réussi le TP#2






       
## Grille de correction 

1. 40pts) Fonctionnalités adéquates

    * pour le projet par défaut:
        * au moins 10 éléments graphiques
        * au moins 5 règles CSS 
        * au moins 5 éléments à sauvegarder
        * au moins 5 actions usagers qui 
            * modifient d'abord le fichier `.json`
            * modifient ensuite l'afficage

    * pour un projet personnalisée
        * selon l'entente avec le prof

1. 40pts) Respect de la méthodologie `Ntro`
    * division en dorsal/frontal
    * utilisation d'événements et de messages
    * observation du modèle
    * etc.

1. 20pts) Qualité du code
    * noms adéquats pour les classes/méthodes/attributs
    * respect des {{% download "/4f5/presentation/NormesProgrammationJava.pdf" "normes de programmation" %}}


<!--

## OPTIONNEL) travail de rattrapage sur la théorie

### À qui s'adresse ce travail?

* Aux étudiant.es à risque d'échouer à cause du double seuil 
    * c-à-d si la **note théorique** est **moins que 55**

* Permettra de récupérer des points perdus aux examens
    * **à la hauteur nécessaire** pour obtenir une note de 55 pour le volet théorique

### À effectuer et remettre

* Un document `.pdf` contenant la description de **deux nouvelles fonctionnalités**
    * (il doit s'agir de fonctionnalités ajoutées durant le projet)
    * vous devez décrire, étape par étape, ce qui se passe dans le code
        * à partir d'une action usager, jusqu'à un affichage graphique
        * avec les extraits de code pertinents

* Une vidéo où 
    * vous démontrer une fonctionnalité **sur votre application**
    * vous décrivez les étapes qui se produisent dans le code
        * montrer au moins **deux extraits de code** dans la vidéo

### Liens utiles

* Logiciel OBS pour capter la vidéo: https://obsproject.com/fr
* Vous pouvez vous inspirer de la question 7 (groupe 1) ou 15 (groupe 2) de l'examen:
    * *Dans la vidéo ci-bas, porter attention à la création de ...*
-->


