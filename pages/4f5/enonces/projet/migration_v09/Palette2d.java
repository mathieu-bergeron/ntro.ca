public class Palette2d extends ObjetPong2d {

    @Override
    // modifier
    // public void drawOn(ResizableWorld2dCanvasFx canvas) {
    // pour devenir:
    public void drawOnWorld(GraphicsContext gc) {

        // retirer
        //canvas.drawOnWorld(gc -> {
            
            gc.save();


            gc.fillRect(getTopLeftX(),
                        getTopLeftY(),
                        getWidth(), 
                        getHeight());
        
            gc.restore();
            
        // retirer
        //});
    }
