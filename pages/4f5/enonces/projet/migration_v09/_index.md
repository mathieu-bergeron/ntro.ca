---
title: "Migration vers Ntro v09"
weight: 1
bookHidden: true
---

{{% pageTitle %}}

## Gradle

1. Modifier `gradle.properties` et s'assurer d'avoir:

    ```groovy
    version=v09
    ```

1. Re-créer les projets VSCode

    ```bash
    $ sh scripts/ajouter_atelier.sh
    ```

    * **NOTES**: 
        * c'est normal d'avoir `BUILD FAILED`
        * continuer les étapes ci-bas pour corriger les erreurs


## Pour chaque `Objet2d`, remplacer `drawOn` par `drawOnWorld`

* `drawOn(ResizableWorld2dCanvasFx convas)` doit devenir `drawOnWorld(GraphicsContext gc)`

    ```java
    {{% embed src="./Palette2d.java" indent-level="1" %}}
    ```
