---
title: "TP1"
weight: 10
draft: false
---

# Énoncé TP1

## Instruction de remise TP#1

1. Dans mon dépôt Git, je m'assure d'avoir le répertoire `tp1` qui contient:

    * mes graphes de tâches (`frontal.png` et `dorsal.png`)

    * un modèle contenant une valeur personnalisée (fichiers `.json` et `.png`)

    * la version la plus récente de ma maquette (document de conception)

1. J'ouvre un GitBash à la racine de mon dépôt Git 

1. Je fais un commit avec le commentaire `TP1`

        $ git add .
        $ git commit -a --allow-empty -mTP1
        $ git push

1. Je m'assure que mon commit est bien sur GitLab

    * y compris le répertoire `tp1`

## Grille de correction TP#1

### 10pts) Maquette (document de conception)

* 5pts) La Vue contient une façon d'ajouter des éléments

* 5pts) Le Modèle contient une Liste ou un Map

### 90pts) Implantation partielle de la maquette

#### 20pts) Compilation et exécution à partir d'un `$ git clone neuf`

* Le projet doit être fonctionnel à partir d'un `$ git clone neuf`

        $ mkdir ~/tmp

        $ cd ~/tmp

        $ git clone git@github.com:USAGER/4f5_prenom_nom

        $ cd 4f5_prenom_nom

        
        $ sh gradlew restoreJson

            # Va copier mes fichiers sauvegardés .json dans _storage

        $ sh gradlew mon_projetFr

            # Doit afficher une page personnalisée, en français
            # Doit permettre d'ajouter (ou retirer) des données au modèle
            # Doit afficher le modèle en mode texte
            # Doit supporter la navigation aller-retour vers une autre page
            # Doit générer
            #      _storage/models/MODELE_PERSONNALISE.json
            #      _storage/graphs/MODELE_PERSONNALISE.png

        $ sh gradlew mon_projetEn

            # Doit afficher une page personnalisée, en anglais
            # Doit supporter la navigation aller-retour vers une autre page
            # Doit permettre d'ajouter (ou retirer) des données au modèle
            # Doit afficher le modèle en mode texte
            # Doit générer
            #      _storage/models/MODELE_PERSONNALISE.json
            #      _storage/graphs/MODELE_PERSONNALISE.png



#### 40pts) Fonctionnalités

* 5pts) L'application démarre sur une Vue personnalisée 

* 5pts) On peut naviguer vers une autre page et revenir

* 10pts) Sur la Vue personnalisée, il y au moins
    * 2 éléments graphiques distincts
    * 2 règles de CSS apparentes
    * un texte traduit en anglais

* 10pts) Affichage en mode texte d'un modèle personnalisé
    * tâche pour observer le modèle
    * affichage en mode texte dans la vue personnalisée
    * remise d'un fichier de test `.json` contenant une valeur personnalisée
    * génération du fichier `.png` à partir du fichier `.json` remis

* 10pts) L'usager peut ajouter (ou retirer) des données au modèle

#### 30pts) Code (respect de la méthodologie Ntro)

* 10pts) Respect de l'arborescence demandée

* 10pts) Noms pertinents pour les classes, méthodes et attributs

* 10pts) Qualité du code (respect des {{% download "/4f5/presentation/NormesProgrammationJava.pdf" "normes de programmation" %}})


### Ré-évaluation des auto-évalutions

* Le prof va compiler les notes des auto-évaluations et ajuster au besoin

* Ces notes seront disponibles en même temps que celle du TP1

