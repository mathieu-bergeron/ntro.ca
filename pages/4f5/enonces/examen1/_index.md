---
title: "Examen 1"
weight: 20
draft: false
---

# Examen 1

## Consignes

* Individuel

* 100% questions à choix de réponse sur Moodle

* Documentation permise: Moodle, Google, **votre code**

* Durée: 1h45

* **ATTENTION**: remise automatique sur Moodle une fois le temps limite atteint

## Préambule

* On va imaginer une application similaire à celle du cours

* Les détails lors de l'examen
   

## Structure de l'examen


### Partie 1: 5 questions avec extraits de code

* en prenant l'application du préambule comme exemple

* 50pts

* planifier environs 60 minutes

### Partie 2: 7 questions sur les notions théoriques

* en prenant l'application du préambule comme exemple

* 50pts

* planifier environs 40 minutes


