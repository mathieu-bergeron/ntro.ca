---
title: ""
weight: 1
bookHidden: true
draft: true
---

# Séquence de modules avec SQL à l'étape 2

## Tutoriel

1. Page admin pour gérer les usagers
    * créer/supprimer/modifier infos
    * pour chaque usager, 0 ou 1 «partie en cours»
    * créer une partie avec 2 usagers

1. NOTE: la librairie contient déjà du code d'auth 
    * session
    * usager

## Étape 1: NoSQL, données bidons / à la main, chacun sa page

1. module 1
    * présentation du cours
    * formation des équipes
    * installation + créer le projet
        * un main() de type App maintenant
        * tester d'emblée le mode client/serveur
        * tester d'emblée les sessions et les usagers (mode DEV)
    * explorer les fichiers et la DB que Ntro va créer par défaut
        * les sessions et les usagers


    * Est-ce qu'on voudrait une tâche spéciale
      qui initialise la Db (ou les modèles)

2. module 2
    * dessiner la maquette de la vue
    * données en Java (plus d'exemples de page)
    * code pour créer les données bidon .json
        * créer des sélecteurs (créer un modèle)
        * tâche pour remplir le modèle au hasard selon le sélecteur

    * NOTE: c'est un main() différent (pas le main de l'App)

3. module 3

    * retour au main() de l'App
    * vue FXML avec traductions
    * modèle en mode graphique
    * (doit changer l'affichage selon les données .json)

4. module 4
    * implanter une fonctionnalité en mode NoSQL
    * réception d'un message + modifier le modèle en Java (p.ex. ajouter usager)
    * doit vérifier que le .json change aussi
    * doit fonctionner en mode client/serveur (juste à démarrer en mode client/serveur)

5. module 5: améliorer l'affichage
    * tailles élastiques
    * fragments

## Étape 2: SQL, navigation, mettre ensemble

6. module 5
    * créer un schéma SQL pour MA PAGE
    * intégrer le schéma SQL d'une AUTRE PAGE
        * (celle du tutoriel OU celle d'un.e co-équipier)
    * optionnel: schéma SQL pour toute l'application
    * code pour remplir MON MODÈLE à partir de la DB (selon le sélecteur)

7. module 6: refaire la fonctionnalité en mode SQL
    * requête SQL pour modifier la DB

8. module 7: 
    * navigation (vers la page partie pour le tutoriel)
    * intégrer une AUTRE PAGE 
        * (celle du tutoriel OU celle d'un.e co-équipier)
    * changer de langue
    * etc.

9. ancien module 8: monde2d

## Extras: mettre tous les optionnels dans un module complétement optionnel (à référencer)

* spécilisations: SessionPong, OptionPong, BdPong, SelectionPong
* pagination en utilisant son propre sélecteur
* animations
* client/serveur avancé
* déploiement
* etc.



## Projet

Comme avant:

1. Améliorer l'application le plus possible



