---
title: "Migration à Ntro v04"
weight: 20
draft: false
bookHidden: true
---

{{% pageTitle %}}

## Étape 1) mise à jour des `.jar` et de `gradle.properties`

* Télécharger {{% download "ntro04.zip" "ntro04.zip" %}}

* Placer le fichier `.zip` à la racine de mon dépôt Git

* Extraire tout **directement à la racine** du dépôt Git **en écrasant** `gradle.properties`

    * Via GitBash

        ```bash
        $ unzip -o ntro04.zip
        ```

        NOTE: le `-o` est pour *overwrite* (écraser)

    * Via Windows:

        * Clique-droit sur le fichier => Extraire tout

            <img class="small-figure" src="extraire_tout00.png"/>

        * Dans le chemin, supprimer le répertoire `ntro04` ajouté par Windows

            <img class="figure" src="extraire_tout01.png"/>

        * Cliquer sur *Extraire*

            <img class="figure" src="extraire_tout02.png"/>

        * **Remplacer** le fichier `gradle.properties`

            <img class="figure" src="extraire_tout03.png"/>

* Vérifier les fichiers suivants:

    * `gradle.properties` fait référence à la nouvelle version:

        <img class="figure" src="verifier01.png"/>

    * le répertoire `libs` contient les nouveaux `.jar`

    <img class="figure" src="verifier02.png"/>


## Étape 3) pousser sur GitLab

1. Dans un GitBash à la racine du dépôt Git

    ```bash
    $ git add .
    $ git commit -a -m "migration ntro v04"
    $ git push
    ```

## Étape 4) recréer les projets VSCode

1. Dans un GitBash à la racine du dépôt Git

    ```bash
    $ sh gradlew eclipse --continue
    ```

1. En VSCode

    * rafraîchir ou ré-importer les projets

## Étape 5) vérifier

1. Exécuter `AppPong` et vérifier la version de Ntro:

    <img class="figure" src="verifier03.png"/>
