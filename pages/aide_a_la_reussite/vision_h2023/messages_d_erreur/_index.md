---
title: "brouillon) exercices interactifs de lecture"
weight: 30
bookHidden: true
---

{{% pageTitle %}}


## Exemples de messages d'erreur

### Java01

```
java.lang.NullPointerException
        at pong.frontal.taches.AfficherFileAttente.lambda$afficherFileAttente$1(AfficherFileAttente.java:40)
        at ca.ntro.app.tasks.SimpleTaskCreatorNtro.executeTask(SimpleTaskCreatorNtro.java:81)
        at ca.ntro.app.tasks.SimpleTaskCreatorNtro.lambda$registerExecutionHandler$0(SimpleTaskCreatorNtro.java:74)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskTraceNtro.executeTask(TaskTraceNtro.java:150)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskTraceNtro.executeOneStep(TaskTraceNtro.java:128)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskGraphTraceNtro.lambda$executeLoop$1(TaskGraphTraceNtro.java:179)
        at ca.ntro.core.stream.StreamNtro.lambda$reduceToResult$10(StreamNtro.java:196)
        at ca.ntro.core.stream.StreamNtro.lambda$applyReducer$0(StreamNtro.java:44)
        at ca.ntro.core.stream.StreamForMapValues.forEach_(StreamForMapValues.java:36)
        at ca.ntro.core.stream.StreamNtro.applyReducer(StreamNtro.java:42)
        at ca.ntro.core.stream.StreamNtro.reduceToResult(StreamNtro.java:194)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskGraphTraceNtro.executeLoop(TaskGraphTraceNtro.java:175)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskGraphTraceNtro.executeAfterResult(TaskGraphTraceNtro.java:252)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskGraphTraceNtro.registerNewResult(TaskGraphTraceNtro.java:198)
        at ca.ntro.core.task_graphs.generic_task_graph.GenericTaskGraphNtro.registerNewResult(GenericTaskGraphNtro.java:254)
        at ca.ntro.core.task_graphs.generic_task_graph.GenericSimpleTaskNtro.addResult(GenericSimpleTaskNtro.java:93)
        at services.MessageServiceFx.lambda$addObservationToObservationHandlerTask$1(MessageServiceFx.java:42)
        at com.sun.javafx.application.PlatformImpl.lambda$runLater$10(PlatformImpl.java:457)
        at java.base/java.security.AccessController.doPrivileged(Native Method)
        at com.sun.javafx.application.PlatformImpl.lambda$runLater$11(PlatformImpl.java:456)
        at com.sun.glass.ui.InvokeLaterDispatcher$Future.run(InvokeLaterDispatcher.java:96)
        at com.sun.glass.ui.gtk.GtkApplication._runLoop(Native Method)
        at com.sun.glass.ui.gtk.GtkApplication.lambda$runLoop$11(GtkApplication.java:316)
        at java.base/java.lang.Thread.run(Thread.java:829)
```

* la variable `fileAttente` (ligne 40) contient la valeur `null`
* correctif: s'assurer de donner une valeur à `fileAttente` **avant** d'appeler une méthode avec `fileAttente.XXX`

### Java02

```
javafx.fxml.LoadException:
/home/mbergeron/__ca.ntro/4f5/tut04/build/resources/main/file_attente.xml

        at javafx.fxml.FXMLLoader.constructLoadException(FXMLLoader.java:2707)
        at javafx.fxml.FXMLLoader.loadImpl(FXMLLoader.java:2685)
        at javafx.fxml.FXMLLoader.loadImpl(FXMLLoader.java:2548)
        at javafx.fxml.FXMLLoader.load(FXMLLoader.java:2516)
        at ca.ntro.app.views.ViewLoaderFx.createView(ViewLoaderFx.java:145)
        at pong.frontal.taches.Initialisation.lambda$creerVueFileAttente$3(Initialisation.java:76)
        at ca.ntro.app.tasks.SimpleTaskCreatorNtro.executeTaskForValue(SimpleTaskCreatorNtro.java:118)
        at ca.ntro.app.tasks.SimpleTaskCreatorNtro.lambda$registerExecutionForValueHandler$1(SimpleTaskCreatorNtro.java:111)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskTraceNtro.executeTask(TaskTraceNtro.java:150)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskTraceNtro.executeOneStep(TaskTraceNtro.java:128)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskGraphTraceNtro.lambda$executeLoop$1(TaskGraphTraceNtro.java:179)
        at ca.ntro.core.stream.StreamNtro.lambda$reduceToResult$10(StreamNtro.java:196)
        at ca.ntro.core.stream.StreamNtro.lambda$applyReducer$0(StreamNtro.java:44)
        at ca.ntro.core.stream.StreamForMapValues.forEach_(StreamForMapValues.java:36)
        at ca.ntro.core.stream.StreamNtro.applyReducer(StreamNtro.java:42)
        at ca.ntro.core.stream.StreamNtro.reduceToResult(StreamNtro.java:194)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskGraphTraceNtro.executeLoop(TaskGraphTraceNtro.java:175)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskGraphTraceNtro.execute(TaskGraphTraceNtro.java:153)
        at ca.ntro.app.tasks.TaskFactoryNtro.executeTasks(TaskFactoryNtro.java:86)
        at ca.ntro.app.frontend.FrontendRegistrarNtro.executeTasks(FrontendRegistrarNtro.java:149)
        at ca.ntro.app.AppWrapperFx.start(AppWrapperFx.java:99)
        at com.sun.javafx.application.LauncherImpl.lambda$launchApplication1$9(LauncherImpl.java:847)
        at com.sun.javafx.application.PlatformImpl.lambda$runAndWait$12(PlatformImpl.java:484)
        at com.sun.javafx.application.PlatformImpl.lambda$runLater$10(PlatformImpl.java:457)
        at java.base/java.security.AccessController.doPrivileged(Native Method)
        at com.sun.javafx.application.PlatformImpl.lambda$runLater$11(PlatformImpl.java:456)
        at com.sun.glass.ui.InvokeLaterDispatcher$Future.run(InvokeLaterDispatcher.java:96)
        at com.sun.glass.ui.gtk.GtkApplication._runLoop(Native Method)
        at com.sun.glass.ui.gtk.GtkApplication.lambda$runLoop$11(GtkApplication.java:316)
        at java.base/java.lang.Thread.run(Thread.java:829)
Caused by: java.lang.NullPointerException
        at pong.frontal.vues.VueFileAttente.installerEvtAfficherPartie(VueFileAttente.java:35)
        at pong.frontal.vues.VueFileAttente.initialize(VueFileAttente.java:28)
        at javafx.fxml.FXMLLoader.loadImpl(FXMLLoader.java:2655)
        ... 28 more
```

* L'attribut `boutonOuvrirPartie` est `null`
* Correctif: 
    * il faut s'assurer que le nom de l'attribut correspond au `fx:id` dans le fichier `.fxml`
    * il faut s'assurer d'utiliser `@FXML` au dessus de nom de l'attribut

### Java03

```
javafx.fxml.LoadException:
/home/mbergeron/__ca.ntro/4f5/tut04/build/resources/main/file_attente.xml

        at javafx.fxml.FXMLLoader.constructLoadException(FXMLLoader.java:2707)
        at javafx.fxml.FXMLLoader.importClass(FXMLLoader.java:2949)
        at javafx.fxml.FXMLLoader.processImport(FXMLLoader.java:2793)
        at javafx.fxml.FXMLLoader.processProcessingInstruction(FXMLLoader.java:2758)
        at javafx.fxml.FXMLLoader.loadImpl(FXMLLoader.java:2624)
        at javafx.fxml.FXMLLoader.loadImpl(FXMLLoader.java:2548)
        at javafx.fxml.FXMLLoader.load(FXMLLoader.java:2516)
        at ca.ntro.app.views.ViewLoaderFx.createView(ViewLoaderFx.java:145)
        at pong.frontal.taches.Initialisation.lambda$creerVueFileAttente$3(Initialisation.java:76)
        at ca.ntro.app.tasks.SimpleTaskCreatorNtro.executeTaskForValue(SimpleTaskCreatorNtro.java:118)
        at ca.ntro.app.tasks.SimpleTaskCreatorNtro.lambda$registerExecutionForValueHandler$1(SimpleTaskCreatorNtro.java:111)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskTraceNtro.executeTask(TaskTraceNtro.java:150)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskTraceNtro.executeOneStep(TaskTraceNtro.java:128)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskGraphTraceNtro.lambda$executeLoop$1(TaskGraphTraceNtro.java:179)
        at ca.ntro.core.stream.StreamNtro.lambda$reduceToResult$10(StreamNtro.java:196)
        at ca.ntro.core.stream.StreamNtro.lambda$applyReducer$0(StreamNtro.java:44)
        at ca.ntro.core.stream.StreamForMapValues.forEach_(StreamForMapValues.java:36)
        at ca.ntro.core.stream.StreamNtro.applyReducer(StreamNtro.java:42)
        at ca.ntro.core.stream.StreamNtro.reduceToResult(StreamNtro.java:194)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskGraphTraceNtro.executeLoop(TaskGraphTraceNtro.java:175)
        at ca.ntro.core.task_graphs.task_graph_trace.TaskGraphTraceNtro.execute(TaskGraphTraceNtro.java:153)
        at ca.ntro.app.tasks.TaskFactoryNtro.executeTasks(TaskFactoryNtro.java:86)
        at ca.ntro.app.frontend.FrontendRegistrarNtro.executeTasks(FrontendRegistrarNtro.java:149)
        at ca.ntro.app.AppWrapperFx.start(AppWrapperFx.java:99)
        at com.sun.javafx.application.LauncherImpl.lambda$launchApplication1$9(LauncherImpl.java:847)
        at com.sun.javafx.application.PlatformImpl.lambda$runAndWait$12(PlatformImpl.java:484)
        at com.sun.javafx.application.PlatformImpl.lambda$runLater$10(PlatformImpl.java:457)
        at java.base/java.security.AccessController.doPrivileged(Native Method)
        at com.sun.javafx.application.PlatformImpl.lambda$runLater$11(PlatformImpl.java:456)
        at com.sun.glass.ui.InvokeLaterDispatcher$Future.run(InvokeLaterDispatcher.java:96)
        at com.sun.glass.ui.gtk.GtkApplication._runLoop(Native Method)
        at com.sun.glass.ui.gtk.GtkApplication.lambda$runLoop$11(GtkApplication.java:316)
        at java.base/java.lang.Thread.run(Thread.java:829)
Caused by: java.lang.ClassNotFoundException: javafx.scene.layout.Vbox
        at java.base/jdk.internal.loader.BuiltinClassLoader.loadClass(BuiltinClassLoader.java:581)
        at java.base/jdk.internal.loader.ClassLoaders$AppClassLoader.loadClass(ClassLoaders.java:178)
        at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:522)
        at javafx.fxml.FXMLLoader.loadTypeForPackage(FXMLLoader.java:3017)
        at javafx.fxml.FXMLLoader.loadType(FXMLLoader.java:3006)
        at javafx.fxml.FXMLLoader.importClass(FXMLLoader.java:2947)
        ... 31 more
```

* La classe `Vbox` n'existe pas
* Correctif: il faut importer `VBox` et non `Vbox`


### Linux

### Windows

### Outils réseau



## Exercices interactifs

1. Le prof doit fournir une version balisée du message d'erreur

1. Une page Web va afficher le message d'erreur

1. L'étudiant·e va pouvoir 
    * cacher et afficher des sections (selon les balises fournies par l'enseignant·e)
    * identifier les sections importantes
    * construire des hypothèses (à l'aide de choix de réponses fournis par l'enseignant·e)

1. L'étudiant·e marque des points lorsque les bonnes actions sont prises
    * (tentative de rendre l'exercice ludique)

## Livrable

1. la petite application Web qui interpréte les balises (HTML+Javascript?)
1. environs 10 exemples de messages d'erreur annotés avec des balises
1. une démo en vidéo du fonctionnement pour l'étudiant·e
1. un catalogue de messages d'erreur colligés par les enseignant·es d'info (mais pas encore annotés avec des balises)


