---
title: "Vision H2023 (dept. info)"
weight: 20
---

{{% pageTitle %}}

## Vision du département en 180 mots

Nous définissons la réussite en trois points: i) acquérir la pensée
informatique; ii) soutenir la motivation; et iii) vaincre le vertige du *je ne
comprends rien*.

L'étudiant·e en informatique doit maîtriser certaines notions *sur le bout des
doigts*, ce qui requiert beaucoup de répétions et de renforcement.  En
programmation, pensons à la capacité à prédire, pas à pas, l'exécution du code.
En réseautique, pensons à la capacité à interpréter un message d'erreur lié à
l'exécution d'une commande.  Ces compétences forment la base de ce qui est
souvent appelée la *pensée informatique* («computational thinking») et qui
permet de comprendre le comportement des systèmes informatiques.

Les répétitions requises pour acquérir cette pensée informatique
sont parfois ennuyantes. Les exercices ludiques ou autres 
tactiques servant à soutenir la motivation sont un élément important 
de la réussite en informatique.

Finalement, l'informatique peut engendrer le vertige du *je ne comprends rien*
(comme s'il s'agissait d'une langue étrangère indéchiffrable).  Les étudiant·es
doivent apprendre à surpasser ce vertige, par exemple en se concentrant sur des
apprentissages plus petits, jusqu'à ce qu'émerge une compréhension plus large des
systèmes informatiques.


## Enjeux à prioriser selon le département d'informatique

1. Difficultés de lecture spécifique aux messages d'erreur (*littératie disciplinaire*)
    * L'étudiant·e doit apprendre à:
        1. rester calme devant le message d'erreur
        1. décortiquer le message, isoler les informations clés
        1. produire des hypothèses à propos de l'erreur

1. Transition d'Excel vers Python dans le nouveau programme de Sciences de la nature
    * on veut assister les profs voulant effectuer cette transition, mais rencontrant des bloquants
    * on veut les assister 
        * le plus tôt possible (dès la rédaction des exercices)
        * le plus rapidement possible (p.ex. en quelques jours ouvrables)

1. Autres enjeux identifiés
    * difficultés d'organisation
    * difficultés à se former un modèle mental de l'exécution d'un programme informatique
    * difficultés à utiliser les outils spécifiques à l'informatique

## Projets d'aide à la réussite

* P1) {{% link "/aide_a_la_reussite/exercices_interactifs_de_lecture/" "exercices interactifs sur la lecture des messages d'erreur" %}}
    * ({{% link "messages_d_erreur" "premier brouillon" %}})
* P2) {{% link "/aide_a_la_reussite/transition_excel_python/" "assistance pour la transition d'Excel vers Python dans le nouveau programme de Sciences de la nature" %}}

