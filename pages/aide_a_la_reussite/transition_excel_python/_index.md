---
title: "P2) transition Excel vers Python"
weight: 30
---

{{% pageTitle %}}

## Problématique ciblée

Les étudiant.e.s de chimie sont confronté.e.s à plusieurs défis, tels que l'analyse de données expérimentales et la résolution de problèmes mathématiques associés. L’utilisation d’Excel présente des limites quant à l’automatisation de l’analyse de données et la modélisation de systèmes chimiques complexes. 

Python est facile à apprendre et offre des outils d’analyse et de visualisation des données. Son enseignement aidera les étudiants à devenir plus efficaces et permettra aux enseignants de proposer des exemples innovants. 

## Description du projet proposé


Le projet consiste à analyser des données expérimentales qui peuvent être trouvées sur divers sites web (gouvernement ou d'une organisation publique) ou fournies par les enseignant.es. Les étudiants seraient amenés à utiliser des bibliothèques Python pour l'analyse des données et la visualisation graphique des données. 

La solution proposée serait un site web sur lequel l’étudiant.e et l’enseignant.e pourront: 

1. Télécharger les données scientifiques dans un format compatible à Python (ex: CSV, URL) 

1. Importer les données dans Python à l'aide d'une librairie Python (ex: pandas) 

1. Nettoyer les données à l’aide de Python pour s'assurer qu'elles sont utilisables pour l'analyse. 

1. Générer des graphiques ou des diagrammes, à l'aide d'une librairie Python (ex: matplotlib, seaborn) pour visualiser les données. 

## Nombre d’étudiants ciblés

Les étudiants en Sciences de la Nature, particulièrement pour les cours de chimie. 

## Résultats attendus 

L'utilisation de Python dans les applications de chimie offre plusieurs avantages par rapport à Excel. Python permet l’automatisation avancée et la gestion de grandes quantités de données, ce qui aidera les étudiants à gagner du temps et à éviter des erreurs de manipulation de données.  

Python aidera les étudiants à acquérir des compétences en programmation et en analyse de données, qui sont de plus en plus demandées dans les carrières en chimie et en science. 

## Moyens pour pérenniser les solutions mises en place

Pour pérenniser les solutions mises en place pour l'utilisation de Python dans les applications de chimie, plusieurs moyens peuvent être pris. Il est important : 

1. D'offrir une formation continue aux enseignants pour maintenir leurs compétences en Python à jour. 

1. De maintenir et développer les ressources pédagogiques pour l'enseignement de Python en chimie.  

1. De collaborer entre enseignants, étudiants et professionnels de l'industrie pour partager les meilleures pratiques et pouvoir innover dans l’offre d’exemples pratiques. 

## Retombées anticipées dans le milieu

Les retombées des résultats de l'utilisation de Python en chimie sont élevées: 

* Les compétences acquises dans l'utilisation de Python peuvent être transférées dans d'autres domaines scientifiques. 

* Les applications de Python peuvent être utilisées dans d'autres programmes du collège.  

* Les compétences en analyse de données acquises grâce à l'utilisation de Python peuvent être transférées dans des carrières en chimie, en science des données et dans d'autres domaines connexes, offrant ainsi de nombreuses opportunités pour les étudiants. 




## Autres modalité(s) proposée(s) pour diffuser les résultats et les retombées du projet à l’interne ou à l’externe

Voici quelques modalités envisageables: 

* Des sondages auprès des enseignants et des étudiants pour recueillir leurs commentaires face à l'utilisation de Python. 

* Des rapports pourraient être partagés auprès des enseignants, des étudiants et du personnel administratif du collège. 

* Des publications pourraient être écrites pour partager les résultats du projet avec un public plus large, lors d'événements tels que: 

    * portes ouvertes 
    * visites dans les écoles secondaires 
    * étudiants d'un jour 
    * présentations de stagiaires  

## Ressources additionnelles souhaitées

* Logiciel Python + librairies 
* Hébergement web interne ou externe pour le site 
