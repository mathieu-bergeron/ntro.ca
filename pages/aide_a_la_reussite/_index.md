---
title: "Aide à la réussite"
weight: 30
bookCollapseSection: true
aliases: ["reussite"]
---

{{% pageTitle %}}


## Vision du département d'informatique 

* {{% link "/aide_a_la_reussite/vision_h2023/" "Notre vision de la réussite" %}}


## Projets pour 2023-2024

* P1) {{% link "/aide_a_la_reussite/exercices_interactifs_de_lecture/" "exercices interactifs sur la lecture des messages d'erreur" %}}
* P2) {{% link "/aide_a_la_reussite/transition_excel_python/" "assistance pour la transition d'Excel vers Python dans le nouveau programme de Sciences de la nature." %}}


## Résultats du sondage 

* {{% link "/aide_a_la_reussite/sondage_h2023" "Sondage au département d'informatique sur la réussite" %}}



