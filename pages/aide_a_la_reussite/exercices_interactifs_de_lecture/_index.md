---
title: "P1) exercices interactifs de lecture"
weight: 30
---

{{% pageTitle %}}

## Par Soumaya Medini, Ph.D.

Mon expérience en recherche sur les traces d’exécution des systèmes
informatiques m’amène à proposer un outil qui offre des exercices interactifs.
Cet outil est une contribution pédagogique permettant aux étudiant·e·s de
première année d’apprendre à comprendre les messages d’erreurs et à appliquer
une stratégie pour résoudre les problèmes signalés.

La solution proposée est composée de 4 étapes :

1. L'enseignant.e va fournir une version balisée du message d’erreur.
1. La page Web va afficher le message d’erreur, ce qui permettra à l’étudiant·e·s de&nbsp;:
    1. Cacher et afficher des sections (selon les balises fournies par l’enseignant.e)&nbsp;;
    1. Identifier les sections importantes&nbsp;;
    1. Afficher d’autres informations fournies par l’enseignant·e (p. ex. le contenu d’un fichier)&nbsp;;
    1. Construire des hypothèses (à l’aide de choix de réponses fournis par l’enseignant·e).

Afin de rendre l’exercice plus ludique, l’étudiant·e marquera des points lorsque les bonnes actions sont prises pour faire avancer son niveau de compétence.
