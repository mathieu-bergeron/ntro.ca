### Exploration 1

Dans le fichier `explorations12.xlsx`, considérer la feuille `exploration1`

Prendre 5 minutes pour:

* Analyser la table de données: expliquer quel est l'erreur dans les données
* Corriger l'erreur
