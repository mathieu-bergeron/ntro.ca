### Démonstration 3

Dans le fichier `explorations12.xlsx`, considérer la feuille `exploration3`

#### Comment ajouter un bouton sur la feuille?


* Suivre $[proc ../../proc/inserer_controle_bouton](les étapes suivantes)

#### Comment assigner une macro à ce bouton?

* En ajoutant un bouton, on peut affecter une macro

* Aussi: clique-droit sur le bouton et choisir *Affecter une macro*

#### Quel est l'avantage d'utiliser un bouton?

1. Le bouton permet d'exécuter rapidement la macro, sans avoir à la choisir dans la liste de macros
