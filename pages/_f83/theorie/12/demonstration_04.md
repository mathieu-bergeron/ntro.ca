### Démonstration 4

Dans le fichier `explorations12.xlsx`, considérer la feuille `exploration4`

#### Comment modifier le code d'une macro?

1. Suivre $[proc ../../proc/macro_modifier](les étapes suivantes)

1. **NOTE**: pour $[proc ../../proc/macro_sauvegarder](sauvegarder la macro), vous devez $[proc ../../proc/enregistrer_sous_format](enregistrer sous) au **format `.xlsm`**
    * sinon la macro est perdue une fois le document fermée

#### Dans le code, comment répéter 5000 fois une instruction?

1. Dans le code, on ajoute
    * Cette ligne au début:

             For i = 1 to 5000

    * Cette ligne à la fin

             Next i
