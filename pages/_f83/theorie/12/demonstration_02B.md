### Démonstration 2B

Dans le fichier `explorations12.xlsx`, considérer la feuille `exploration2`

1. $[proc ../../proc/macro_enregistrer_references_relatives](Enregistrer la macro) suivante:
    * Utiliser des *références relatives*
    * Sélectionner *F2* avant d'enregistrer
    * Nom de la macro: `CorrigerLigne`
    * Actions de la macro:
        1. Sélectionner la plage *F2:G2*
        1. Déplacer la plage sélectionnée de deux cellules vers la gauche
        1. Sélectionner *F4* (prochaine ligne à réparer)

1. **S'assurer** d'arrêter l'enregistrement

1. $[proc ../../proc/macro_executer](Exécuter la macro) plusieurs fois
