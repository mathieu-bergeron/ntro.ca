### Exploration 3

Dans le fichier `explorations12.xlsx`, considérer la feuille `exploration3`

Prendre 5 minutes pour répondre aux questions suivantes:

* Comment ajouter un bouton sur la feuille?
* Comment assigner une macro à ce bouton?
* Quel est l'avantage d'utiliser un bouton?
