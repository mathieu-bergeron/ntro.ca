### Démonstration 2A

Dans le fichier `explorations12.xlsx`, considérer la feuille `exploration2`

#### Qu'est-ce qu'une macro?

Une liste d'actions que Excel va ré-exécuter automatiquement

#### Comment enregistrer une macro?

1. D'abord, il faut $[proc ../../proc/ajouter_onglet_developpeur](ajouter l'onget développeur)

1. Ensuite, suivre les $[proc ../../proc/macro_enregistrer_references_relatives](étapes suivantes). À noter:
    * vous devez choisir d'utiliser des références *absolues* ou *relatives*
    * vous devez nommer votre macro
    * ne pas oubliez d'arrêter l'enregistrement


#### Quelle est la différence entre:

* Macro utilisant les références absolues
    * les actions sont toujours exécutées au même endroit

* Macro utilisant les références relatives
    * les actions sont exécutées à des endroits différents, selon où se trouve le curseur au moment où la macro est activée

