### Exploration 2A

Dans le fichier `explorations12.xlsx`, considérer la feuille `exploration2`

Prendre 5 minutes pour répondre aux questions suivantes:

* Qu'est-ce qu'une macro?
* Comment enregistrer une macro?
* Quelle est la différence entre:
    * une macro utilisant les références absolues
    * une macro utilisant les références relatives

