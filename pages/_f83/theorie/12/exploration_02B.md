### Exploration 2B

Dans le fichier `explorations12.xlsx`, considérer la feuille `exploration2`

Prendre 5 minutes pour:

* Enregistrer une macro pour corriger une ligne de données
* Exécuter la macro plusieurs fois pour corriger plusieurs lignes


#### Note

* Utiliser les réféfences relatives


