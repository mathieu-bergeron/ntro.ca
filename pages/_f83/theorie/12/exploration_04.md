### Exploration 4

Dans le fichier `explorations12.xlsx`, considérer la feuille `exploration4`

Prendre 5 minutes pour répondre aux questions suivantes:

* Comment modifier le code d'une macro?
* Dans le code, comment:
    * répéter 5000 fois une instruction?
