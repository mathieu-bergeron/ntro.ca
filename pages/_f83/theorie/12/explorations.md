# Macro et contrôles de formulaire

## Partie 1: enregistrer une macro

$[embed ./exploration_01]

$[embed ./exploration_02A]

$[embed ./exploration_02B]

## Partie 2: insérer des contrôles

$[embed ./exploration_03]

## Partie 3: modifier le code d'une macro

$[embed ./exploration_04]

