# Macro et contrôles de formulaire

## Partie 1: enregistrer une macro

$[embed ./demonstration_01]

$[embed ./demonstration_02A]

$[embed ./demonstration_02B]

## Partie 2: insérer des contrôles

$[embed ./demonstration_03]

## Partie 3: modifier le code d'une macro

$[embed ./demonstration_04]

