### Démonstration 1

Dans le fichier `explorations12.xlsx`, considérer la feuille `exploration1`

* L'erreur est la suivante:
    * À partir de la ligne 2 et à chaque deux lignes:
        * les valeurs des champs `PRIX_TERRAIN` et `AIRE_TERRAIN` sont décalés de deux cellules
          vers la droite

* Pour corriger l'erreur
    * Sélectionner *F2:G2* et déplacer ces cellules de deux places vers la gauche
    * Répéter la même opération à chaque deux linges, c-à-d:
        * ligne 4
        * ligne 6
