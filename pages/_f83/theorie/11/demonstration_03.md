### Démonstration 03

Voici comment faire:

1. $[proc ../../proc/importation_delimite](Importer) le fichier `.csv` (délimité)

1. $[proc ../../proc/supprimer_doublons](Supprimer) les doublons

1. Corriger la colonne `PRIX_RESIDENCE`
    * $[proc ../../proc/colonne_inserer](Insérer) une nouvelle colonne
    * Saisir `PRIX_TERRAIN` en haut de la nouvelle colonne
    * Saisir la correction des trois premiers prix au format `123,45$`:

            162837,96$
            121510,80$
            264977,39$

    * $[proc ../../proc/remplissage](Remplir automatiquement) le reste de la colonne
    * Vérifier


1. $[proc ../../proc/colonne_formater](Formater) les colonnnes

#### Réponse

Les champs de la table sont:

* `ID` au format `standard`
* `DATE` au format `date courte`
* `TERRAIN` au format `standard`
* `PRIX_RESIDENCE` au format `monétaire`
* `AIRE_RESIDENCE` au format `nombre`
