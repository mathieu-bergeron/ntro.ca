### Exploration 2

Prendre 5 minutes pour importer la table suivante:

<center>
<img src='../../captures/table_ventes_residence.png'/>
</center>

#### Instructions

1. **Télécharger** d'abord le fichier `exercice11_ventes_residence.csv` disponible sur Moodle

1. Voici un aperçu du fichier:

        ID;DATE;TERRAIN;PRIX_RESIDENCE;AIRE_RESIDENCE
        res1522;2011/07/17;ter751;$ 162837.96;1321
        res1522;2011/07/17;ter751;$ 162837.96;1321
        res1522;2011/07/17;ter751;$ 162837.96;1321
        res5873;2010/05/05;ter6853;$ 121510.80;1120
        res5872;2009/10/01;ter8886;$ 264977.39;1737

1. **Importer** le fichier dans Excel
    * Vous devriez avoir *8001* lignes

1. **Répondre** aux questions suivantes:
    * Quelles options avez-vous utilisées?
    * Quels sont les champs de la table?
    * Avez-vous modifié les données? Comment?
