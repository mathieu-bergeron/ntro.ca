### Démonstration 02

Voici comment faire:

1. $[proc ../../proc/importation_largeur_fixe](Importer) le fichier `.csv` (largeur fixe)

1. Corriger la colonne `DATE`
    * $[proc ../../proc/colonne_inserer](Insérer) une nouvelle colonne
    * Saisir `DATE` en haut de la nouvelle colonne
    * Saisir la correction des trois premières dates au format `ANNÉE-MOIS-JOUR`

            2008-10-12
            2012-05-18
            2015-06-21

    * $[proc ../../proc/remplissage](Remplir automatiquement) le reste de la colonne
    * Vérifier
    * $[proc ../../proc/colonne_supprimer](Supprimer) l'ancienne colonne

1. Corriger la colonne `PRIX_TERRAIN`
    * $[proc ../../proc/colonne_inserer](Insérer) une nouvelle colonne
    * Saisir `PRIX_TERRAIN` en haut de la nouvelle colonne
    * Saisir la correction des trois premiers prix au format `123,45$`:

            134405,67$
            151128,34$
            315695,81$

    * $[proc ../../proc/remplissage](Remplir automatiquement) le reste de la colonne
    * Vérifier


1. $[proc ../../proc/colonne_formater](Formater) les colonnnes

#### Réponse

Les champs de la table sont:

* `ID` au format `standard`
* `DATE` au format `date courte`
* `ID_QUARTIER` au format `standard`
* `PRIX_TERRAIN` au format `monétaire`
* `AIRE_TERRAIN` au format `nombre`
