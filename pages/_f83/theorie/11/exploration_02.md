### Exploration 2

Prendre 5 minutes pour importer la table suivante:

<center>
<img src='../../captures/table_ventes_terrain.png'/>
</center>

#### Instructions

1. **Télécharger** d'abord le fichier `exercice11_ventes_terrain.csv` disponible sur Moodle

1. Voici un aperçu du fichier:

        ID        DATE           ID_QUARTIER    PRIX_TERRAIN        AIRE_TERRAIN
        ter157    12/10/2008     Q14            $134405.67          23779
        ter156    18/05/2012     Q7             $151128.34          22815
        ter155    21/06/2015     Q28            $315695.81          30219

1. **Importer** le fichier dans Excel
    * Vous devriez avoir *10001* lignes

1. **Répondre** aux questions suivantes:
    * Comment avez-vous fait?
    * Quelles options avez-vous utilisées?
    * Quels sont les champs de la table?
    * Avez-vous modifié les données? Comment?
