### Exploration 1

Prendre 5 minutes pour importer la table suivante dans Excel:

* liste des arrondissements de la ville de Montréal (nom et code)

<center>
<img src='../../captures/table_arrondissements.png'/>
</center>

#### Instructions

1. Il faut d'abord **trouver** un fichier à importer sur le site de  *données ouvertes* de la ville de Montréal:
    * <a target='_blank' href='http://donnees.ville.montreal.qc.ca/'>http://donnees.ville.montreal.qc.ca/</a>

1. **Ouvrir** ce fichier dans Excel

1. Au besoin, **modifier** les données importées

1. Vous devriez avoir 37 lignes:
    * *ligne 1*: nom des champs
    * *lignes 2-37*: les arrondissements
