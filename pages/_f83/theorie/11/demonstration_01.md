### Démonstration 01

Voici comment faire:

1. $[proc ./proc/demo01](Télécharger) le fichier
1. $[proc ../../proc/ouvrir_xml](Ouvrir) le fichier en Excel
1. Modifier les données:
    1. $[proc ./proc/demo01A](Supprimer) les colonnes de trop
    1. $[proc ../../proc/table_convertir_en_plage](Convertir en plage)
    1. $[proc ./proc/demo01B](Renommer) les champs
    1. $[proc ./proc/demo01C](Déplacer) les colonnes
