### Démonstration 3

Pour répondre à la question, il faut:

1. $[proc ../../proc/graphique_croise_inserer](Insérer) un graphique croisé dynamique
1. $[proc ../../proc/graphique_croise_ajouter_categorie](Ajouter la catégorie) `NOM_QUARTIER`
1. $[proc ../../proc/graphique_croise_ajouter_valeur](Ajouter la valeur) `PRIX_ACTU`
1. $[proc ../../proc/graphique_croise_modifier_valeur](Modifier les paramètres) de la valeur `PRIX_ACTU`. **Choisir** `moyenne`
1. $[proc ./proc/demo03_reponse](Prendre en note) la réponse

Notre réponse:

* Vieux-Montréal
* Prix moyen: `382 212,71 $`

Notes:

* Le graphique serait plus facile à utiliser si les prix étaient affichés en ordre de grandeur
* Les prix seraient plus facile à lire en format monétaire

