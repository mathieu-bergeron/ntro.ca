### Exploration 4

Prendre 5 minutes pour répondre aux questions suivantes:

1. Comment trier les quartiers selon les valeurs?
1. Comment utiliser le format `monétaire` dans le graphique?
1. Quel champ utiliser pour compter le nombre de ventes?

