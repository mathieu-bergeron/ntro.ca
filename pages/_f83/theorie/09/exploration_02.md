### Exploration 2

Prendre 5 minutes pour répondre aux questions suivantes:

1. Pourquoi utiliser `SOUS.TOTAL` plutôt que `MOYENNE`?
1. Comment considérer seulement les quartiers dont le nom commence par `Saint`?
1. Comment annuler un filtre?
1. Peut-on appliquer plus d'un filtre? Qu'est-ce qui se passe?
