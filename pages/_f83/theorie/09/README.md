* La théorie est écrite sous forme de:
    * **exploration**: les étudiants tentent de répondre à une question par eux-mêmes
    * **démonstration**: l'enseignant montre comment répondre à la question avec les outils du cours
