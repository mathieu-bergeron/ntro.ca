### Démonstration 2

1. Avec les filtres, on doit utiliser `SOUS.TOTAL`, car
    * `MOYENNE` fait la moyenne de toute la colonne, sans considérer le filtre
    * `SOUS.TOTAL` fait la moyenne des lignes affichées par le filtre

1. On doit utiliser le *filtre textuel*. Voici comment utiliser $[proc ../../proc/filtres_textuel](filtre textuel).

1. Voici comment $[proc ../../proc/filtres_effacer](effacer un filtre).

1. Tous les filtres choisis sont appliqués. Avec deux filtres, on pourrait par exemple avoir:
    * Les ventes de plus de `100 000,00$` dans le quartier `Verdun`
    * Les ventes en `2015` où l'aire est moins de `25 000` pieds carrés



