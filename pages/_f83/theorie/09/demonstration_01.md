### Démonstration 1

Pour répondre à la question, il faut:

1. $[proc ./proc/demo01_formule](Saisir la formule) `SOUS.TOTAL()` pour calculer la moyenne
1. $[proc ../../proc/filtres_inserer](Ajouter les filtres) au tableau de données
1. $[proc ./proc/demo01_filtre](Sélectionner) le filtre `Lachine`

La réponse:

* `239 423,27 $`



