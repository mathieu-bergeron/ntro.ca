### Démonstration 4

Pour répondre à la question, il faut:

1. Voici comment $[proc ../../proc/graphique_croise_trier](trier) les valeurs dans le tableau et le graphique
1. Voici comment $[proc ../../proc/graphique_croise_modifier_format_valeur](changer le format) d'une valeur
1. Pour le nombre, n'importe quel champ fera l'affaire. C'est le nombre de lignes qui est compté

