# Analyser une table de données avec Excel

Considérer le fichier `exercice09A.xlsx`

* La feuille `ventes_terrain` est une table de 10 000 ventes de terrains à Montréal
    * voici la signification de chaque champ:
        * DATE: date de la vente
        * NOM_QUARTIER: le quartier où se situe le terrain
        * AIRE: la surface du terrain (pied carré)
        * PRIX_ACTU: le prix de vente (actualisé en dollars d'aujourd'hui)

## Partie 1: isoler un quartier

```embed
{{% embed "./exploration_01.md" %}}
```

```embed
{{% embed "./exploration_02.md" %}}
```

## Partie 2: comparer tous les quartiers

```embed
{{% embed "./exploration_03.md" %}}
```

```embed
{{% embed "./exploration_04.md" %}}
```


