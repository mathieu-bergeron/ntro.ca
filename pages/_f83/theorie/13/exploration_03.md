### Exploration 3

Ouvrir le fichier `exercice13.xlsx`

Prendre 5 minutes pour:

* Ajouter le champ `MOIS_ECOULES` à la table `ventes_residence`
    * Ce champ représente le nombre de mois écoulés depuis la vente

Comment avez-vous fait?
