### Démonstration 1

Pour répondre à la question suivante:

* Quel arrondissement contient le plus de quartiers?

Il faut:

1. $[proc ../../proc/tableau_inserer](insérer) un tableau pour la table `quartiers`
    1. $[proc ../../proc/tableau_renommer](renommer) ce tableau `Quartier`

1. $[proc ../../proc/tableau_inserer](insérer) un tableau pour la table `arrondissements`
    1. $[proc ../../proc/tableau_renommer](renommer) ce tableau `Arrondissement`


1. $[proc ../../proc/relation_ajouter](ajouter la relation) suivante:
    * Table: `Quartier` , colonne (externe): `ID_ARROND`
    * Table associée: `Arrondissement` , colonne associée (principale): `ID`

1. $[proc ../../proc/tableau_croise_inserer](insérer un tableau croisé)
    1. $[proc ../../proc/tableau_croise_plus_de_tables](ajouter) les autres tables (*PLUS DE TABLES*)

1. **Utiliser** le tableau croisé pour répondre à la question

