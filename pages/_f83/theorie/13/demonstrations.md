# Tableau croisé à plusieurs tables & champs calculés avec PowerPivot

## Partie 1: Tableau croisé à plusieurs tables

$[embed ./demonstration_01]

$[embed ./demonstration_02]

## Partie 2: Champs calculés avec PowerPivot

$[embed ./demonstration_03]

