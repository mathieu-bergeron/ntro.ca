# Tableau croisé à plusieurs tables & champs calculés avec PowerPivot

## Partie 1: Tableau croisé à plusieurs tables

$[embed ./exploration_01]

$[embed ./exploration_02]

## Partie 2: Champs calculés avec PowerPivot

$[embed ./exploration_03]

