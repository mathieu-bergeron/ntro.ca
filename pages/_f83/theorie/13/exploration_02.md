### Exploration 2

Ouvrir le fichier `exercice13.xlsx`

Prendre 5 minutes pour répondre à la question suivante:

1. Quel est l'arrondissement où le prix moyen des résidences est le plus élevé?

Comment avez-vous fait?
