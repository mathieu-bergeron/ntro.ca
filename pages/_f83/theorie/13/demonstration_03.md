### Démonstration 3

NOTE:

* On ajoute un champ à la table `ventes_residence`
* Pour la calcul, on utilise la date contenue dans la table `ventes_terrain`

Il faut utiliser l'ajout *PowerPivot*:

1. $[proc ../../proc/pivot_activer](Activer PowerPivot)
1. $[proc ../../proc/pivot_donnees](Ouvrir la vue de données) et naviguer jusqu'à la table `VenteResidence`
1. $[proc ../../proc/pivot_champ_calcule](Ajouter le champ calculé) suivant:
    * formule: `=(YEAR(NOW()) - YEAR(RELATED(VenteTerrain[DATE]))) * 12 + MONTH(NOW()) - MONTH(RELATED(VenteTerrain[DATE]))`
    * nom du champ: `MOIS_ECOULES`

En *PowerPivot*:

* Valeur d'un champ de la table courante: `[NOM_DU_CHAMP]`
* Valeur d'un champ d'un autre table: `RELATED(AutreTable[NOM_DU_CHAMP])`
* Les noms de fonction sont en anglais
