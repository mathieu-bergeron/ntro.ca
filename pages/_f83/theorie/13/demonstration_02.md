### Démonstration 2

Pour répondre à la question suivante:

1. Quel est l'arrondissement où le prix moyen des résidences est le plus élevé?

Il faut:

1. $[proc ../../proc/tableau_inserer](insérer) un tableau pour la table `ventes_residence`
    1. $[proc ../../proc/tableau_renommer](renommer) ce tableau `VenteResidence`

1. $[proc ../../proc/relation_ajouter](ajouter la relation) suivante:
    * Table: `VenteTerrain` , colonne (externe): `ID_QUARTIER`
    * Table associée: `Quartier` , colonne associée (principale): `ID`

1. $[proc ../../proc/relation_ajouter](ajouter la relation) suivante:
    * Table: `VenteResidence` , colonne (externe): `ID_TERRAIN`
    * Table associée: `VenteTerrain` , colonne associée (principale): `ID`

1. **Utiliser** un tableau croisé pour répondre à la question
