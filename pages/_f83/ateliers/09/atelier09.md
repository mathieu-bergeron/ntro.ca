# Atelier 09: filtre et tableau croisé dynamique

## Préambule

* Démarrer Excel et ouvrir le fichier `atelier_09.xlsx`
* La feuille `ventes_residence` est une table de 10 000 ventes de residences à Montréal
    * voici la signification de chaque champ:
        * `ANNEE`: année de la vente
        * `NOM_QUARTIER`: le quartier où se situe la résidence
        * `PRIX_RESIDENCE_ACTU`: le prix de vente de la résidence (actualisé)
        * `PRIX_TERRAIN_ACTU`: le prix de vente du terrain (actualisé)
        * `AIRE_RESIDENCE`: la surface habitable de la résidence (pieds carrés)

## Travail à effectuer

* À l'aide d'Excel, analyser les données
* Sur Moodle, répondre au quizz `Atelier09`

## Partie 1: ajout de champs calculés

Sur la feuille `ventes_residence`

1. En *F1*, saisir le nom du nouveau champ: `PRIX_TOTAL_ACTU`, le prix actualisé total de la vente (terrain + résidence)

1. En *F2*, saisir une formule pour calculer ce `PRIX_TOTAL_ACTU`

1. $[proc ../../proc/propager_formule](Propager) la formule en *F2* à la plage *F3:F8001*
    * TRUC: $[proc ../../proc/selection](sélectionner) la plage avec la deuxième méthode

1. En *G1*, saisir le nom du nouveau champ: `PRIX_RESIDENCE_PAR_PC`, c'est à dire le prix de la *résidence* par pied carré
    * ATTENTION: utiliser `PRIX_RESIDENCE_ACTU` pour ce calcul (et **non** le prix total)

1. En *G2*, saisir une formule pour calculer ce `PRIX_RESIDENCE_PAR_PC`

1. $[proc ../../proc/propager_formule](Propager) la formule en *G2* à la plage *G3:G8001*

## Partie 2: utilisation de filtres

Sur la feuille `ventes_residence`

1. En *K1*, $[proc ../../proc/saisir_formule](saisir une formule) pour calculer le nombre de valeurs de la plage *A2:A8001*
    * Utiliser la fonction `SOUS.TOTAL`

1. En *M1*, $[proc ../../proc/saisir_formule](saisir une formule) pour calculer la moyenne prix au pied carré (plage *G2:G8001*)
    * Utiliser la fonction `SOUS.TOTAL`

1. $[proc ../../proc/selection](Sélectionner) la cellule *A1* et $[proc ../../proc/filtres_inserer](ajouter les filtres) à la table de données

<br>
### Questions

À l'aide de filtre, répondre aux questions suivantes:

1. En `2015`, quel est le prix au pied carré moyen dans le quartier `Vieux-Montréal`?
1. En `2013`, combien de ventes avait un prix au pied carré inférieur à `200 $`?

## Partie 3: utilisation du tableau croisé dynamique

Sur la feuille `ventes_residence`

1. $[proc ../../proc/selection](Sélectionner) la cellule *A1* et $[proc ../../proc/graphique_croise_inserer](insérer) un graphique croisé dynamique

1. $[proc ../../proc/feuille_renommer](Renommer) la feuille où se trouve le tableau. **Appeler** cette feuille: `analyse`

1. Pour chaque question ci-bas:
    1. $[proc ../../proc/graphique_croise_ajouter_categorie](ajouter une catégorie)
    1. $[proc ../../proc/graphique_croise_ajouter_valeur](ajouter une valeur)
        1. $[proc ../../proc/graphique_croise_modifier_valeur](modifier les paramètres) de la valeur
        1. $[proc ../../proc/graphique_croise_modifier_format_valeur](modifier le format) de la valeur
        1. $[proc ../../proc/graphique_croise_trier](trier) la valeur au besoin
    1. $[proc ../../proc/graphique_croise_ajouter_filtre](ajouter un filtre)
    1. Noter la réponse
    1. Recommencer les étapes au besoin

<br>
### Questions

Voici les questions:


1. Quelle était l'année où, en moyenne, les prix au pied carré étaient le plus élevés?
1. Pour le quartier `Saint-Henri` , quelle était l'année où, en moyenne, les prix totaux le plus élevés?
1. Pour le quartier `Hochelaga-Maisonneuve` quelle était l'année où il y a eu le plus de ventes?
1. Quel est le quartier où il y a le plus de ventes?
1. En `2013`, quel est le quartier où il y a le plus de ventes?
1. En `2011`, quel quartier avait le prix au prix carré le plus élevé, en moyenne?

