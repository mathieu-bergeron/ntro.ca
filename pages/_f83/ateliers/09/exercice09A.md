---
bookHidden: true
---

# Exercice 09A: filtre et tableau croisé dynamique

## Préambule

* Télécharger le fichier {{% download "exercice_09A.xlsx" "exercice_09A.xlsx" %}}
* Copier le fichier dans un répertoire de travail
* Démarrer Excel et ouvrir le fichier `exercice_09A.xlsx`
* La feuille `ventes_terrain` est une table de 10 000 ventes de terrains à Montréal
    * voici la signification de chaque champ:
        * `DATE`: date de la vente
        * `NOM_QUARTIER`: le quartier où se situe le terrain
        * `AIRE`: la surface du terrain (pied carré)
        * `PRIX_ACTU`: le prix de vente (actualisé en dollars d'aujourd'hui)

## Travail à effectuer

* À l'aide d'Excel, analyser les données
* Sur Moodle, répondre au quizz `Exercice09A`

## Partie 1: utilisation de filtres

Sur la feuille `ventes_terrain`

1. En *G1*, **saisir une formule** pour calculer le nombre de valeurs de la plage *A2:A10001*
    * Utiliser la fonction `SOUS.TOTAL`

1. En *J1*, **saisir une formule** pour calculer la moyenne des prixs (plage *D2:D10001*)
    * Utiliser la fonction `SOUS.TOTAL`

1. **Sélectionner** la cellule *A1* et **ajouter les filtres** à la table de données

<br>

## Questions

À l'aide de filtre, répondre aux questions suivantes:

1. Dans le quartier Lachine, quel est le prix moyen des terrains?
1. Dans le quartier Rosemont, combien y a-t-il eu de ventes?

## Partie 2: utilisation du tableau croisé dynamique

Sur la feuille `ventes_terrain`

1. **Sélectionner** la cellule *A1* et **insérer** un graphique croisé dynamique

1. **Renommer** la feuille où se trouve le tableau. **Appeler** cette feuille: `analyse`

1. **Ajouter la catégorie** `NOM_QUARTIER`

1. **Ajouter la valeur** `PRIX_ACTU`

1. **Modifier les paramètres** de la valeur `PRIX_ACTU`. **Choisir** `moyenne`

1. **Modifier le format** de la valeur `PRIX_ACTU`. **Choisir** `monétaire`

1. **Trier** la valeur `PRIX_ACTU` du plus grand au plus petit

1. Pour répondre aux questions ci-bas, **choisir** différents paramètres pour le champ `PRIX_ACTU`: moyenne, somme, nombre, etc.

<br>

## Questions

1. Quel est le quartier où, en moyenne, les prix sont le plus élevés?
1. Quel est le quartier où, en moyenne, les prix sont les moins élevés?
1. Quel est le quartier où il y a le plus de ventes?
1. Quel est le quartier où il y a le moins de ventes?
1. Quel quartier a le plus gros poids économique?
    * Autrement dit: quel quartier a vu le plus d'argent changer de main?
1. Quel quartier a le plus petit poids économique?
