## Partie 1: utilisation de filtres

1. Dans le quartier Lachine, quel est le prix moyen des terrains?
    * RÉPONSE:  `239 423,27  $`
1. Dans le quartier Rosemont, combien y a-t-il eu de ventes?
    * RÉPONSE: `386`

## Partie 2: utilisation du tableau croisé dynamique

1. Quel est le quartier où, en moyenne, les prix sont le plus élevés?
    * RÉPONSE: `Vieux-Montréal`
1. Quel est le quartier où, en moyenne, les prix sont les moins élevés?
    * RÉPONSE: `Saint-Henri`
1. Quel est le quartier où il y a le plus de ventes?
    * RÉPONSE: `Parc-Extension`
1. Quel est le quartier où il y a le moins de ventes?
    * RÉPONSE: `Vieux-Montréal`
1. Quel quartier a le plus gros poids économique?
    * RÉPONSE: `Le Plateau-Mont-Royal`
1. Quel quartier a le plus petit poids économique?
    * RÉPONSE: `Centre-Sud`
