# Exercice 09B: filtre et tableau croisé dynamique

## Préambule

* Démarrer Excel et ouvrir le fichier `exercice_09B.xlsx`
* La feuille `ventes_residence` est une table de 10 000 ventes de residences à Montréal
    * voici la signification de chaque champ:
        * `DATE`: date de la vente
        * `ANNEE`: année de la vente
        * `NOM_QUARTIER`: le quartier où se situe la résidence
        * `APPR_MOIS`: le taux d'appréciation mensuel pour ce quartier
        * `PRIX`: le prix de vente au moment de la vente

## Travail à effectuer

* À l'aide d'Excel, analyser les données
* Sur Moodle, répondre au quizz `Exercice09B`

## Partie 1: ajout de champs calculés

Sur la feuille `ventes_residence`

1. En *F1*, **saisir** le nom du nouveau champ: `MOIS_ECOULES`, c'est à dire le nombre de mois écoulés depuis la vente

1. En *F2*, **saisir** la formule ci-bas, qui va calculer le nombre de mois écoulés:
    * `=JOURS360(A2;AUJOURDHUI())/30`

1. $[proc ../../proc/propager_formule](Propager) la formule en *F2* à la plage *F3:F8001*
    * TRUC: $[proc ../../proc/selection](sélectionner) la plage avec la deuxième méthode

1. En *G1*, **saisir** le nom du nouveau champ: `PRIX_ACTU`, c'est à dire le prix actualisé (en date d'aujourd'hui)

1. En *G2*, **saisir** la formule ci-bas, qui va calculer le prix actualisé:
    * `=PUISSANCE((1+D2);F2) * E2`

1. $[proc ../../proc/propager_formule](Propager) la formule en *G2* à la plage *G3:G8001*

## Partie 2: utilisation de filtres

Sur la feuille `ventes_residence`

1. En *K1*, $[proc ../../proc/saisir_formule](saisir une formule) pour calculer le nombre de valeurs de la plage *A2:A8001*
    * Utiliser la fonction `SOUS.TOTAL`

1. En *M1*, $[proc ../../proc/saisir_formule](saisir une formule) pour calculer la moyenne des prix de vente actualisé (plage *G2:G8001*)
    * Utiliser la fonction `SOUS.TOTAL`

1. $[proc ../../proc/selection](Sélectionner) la cellule *A1* et $[proc ../../proc/filtres_inserer](ajouter les filtres) à la table de données

<br>
### Questions

À l'aide de filtre, répondre aux questions suivantes:

1. En `2012`, quel était le prix de vente moyen dans le quartier `Verdun`?
1. En `2009`, combien y a-t-il eut de vente dans les quartiers `Mercier Ouest` et `Mercier Est` où le prix actualisé était supérieur à `150 000 $`

## Partie 3: utilisation du tableau croisé dynamique

Sur la feuille `ventes_residence`

1. $[proc ../../proc/selection](Sélectionner) la cellule *A1* et $[proc ../../proc/graphique_croise_inserer](insérer) un graphique croisé dynamique

1. $[proc ../../proc/feuille_renommer](Renommer) la feuille où se trouve le tableau. **Appeler** cette feuille: `analyse`

1. $[proc ../../proc/graphique_croise_ajouter_categorie](Ajouter une catégorie) vous permettant de répondre aux questions ci-bas

1. $[proc ../../proc/graphique_croise_ajouter_valeur](Ajouter la valeur) `PRIX_ACTU`

1. $[proc ../../proc/graphique_croise_ajouter_filtre](Ajouter le filtre) `NOM_QUARTIER`

1. $[proc ../../proc/graphique_croise_modifier_valeur](Modifier les paramètres) de la valeur `PRIX_ACTU`. **Choisir** `moyenne`

1. $[proc ../../proc/graphique_croise_modifier_format_valeur](Modifier le format) de la valeur `PRIX_ACTU`. **Choisir** `monétaire`

1. $[proc ../../proc/graphique_croise_trier](Trier) la valeur `PRIX_ACTU` du plus grand au plus petit

1. Pour répondre aux questions ci-bas:
    * **choisir** différents paramètres pour le champ `PRIX_ACTU`: moyenne, somme, nombre, etc.
    * **choisir** différents quartiers pour le filtre `NOM_QUARTIER`

<br>
### Questions

Voici les questions:

1. Quelle était l'année où, en moyenne, les prix étaient le plus élevés?
1. Pour le quartier `Rosemont`, quelle était l'année où, en moyenne, les prix étaient le plus élevés?
1. Quelle était l'année où il y a eu le moins de ventes?
1. Pour le quartier `Verdun`, quelle était l'année où il y a eu le moins de ventes?
1. En quelle année est-ce que le quartier `Parc-Extension` a eu le plus gros poids économique?
1. En quelle année est-ce que le quartier `Peter McGill` a eu le plus petit poids économique?
