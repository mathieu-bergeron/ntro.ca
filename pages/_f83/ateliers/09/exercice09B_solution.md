# Partie 2

1. En `2012`, quel était le prix de vente moyen dans le quartier `Verdun`?
    * RÉPONSE: `209 752,72 $`

1. En `2009`, combien y a-t-il eut de vente dans les quartiers `Mercier Ouest` et `Mercier Est` où le prix actualisé était supérieur à `150 000 $`
    * RÉPONSE: `48`

# Partie 3


1. Quelle était l'année où, en moyenne, les prix étaient le plus élevés?
    * RÉPONSE: `2008`

1. Pour le quartier `Rosemont`, quelle était l'année où, en moyenne, les prix étaient le plus élevés?
    * RÉPONSE: `2011`

1. Quelle était l'année où il y a eu le moins de ventes?
    * RÉPONSE: `2015`

1. Pour le quartier `Verdun`, quelle était l'année où il y a eu le moins de ventes?
    * RÉPONSE: `2011`

1. En quelle année est-ce que le quartier `Parc-Extension` a eu le plus gros poids économique?
    * RÉPONSE: `2010`

1. En quelle année est-ce que le quartier `Peter McGill` a eu le plus petit poids économique?
    * RÉPONSE: `2011`

