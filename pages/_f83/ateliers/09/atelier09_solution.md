# Partie 2

1. En `2015`, quel est le prix au pied carré moyen dans le quartier `Vieux-Montréal`?
    * RÉPONSE: `390,82 $`

1. En `2013`, combien de ventes avait un prix au pied carré moyen inférieur à `200 $`?
    * RÉPONSE: `461`

# Partie 3

1. Quelle était l'année où, en moyenne, les prix au pied carré étaient le plus élevés?
    * REPONSE: `2008`

1. Pour le quartier `Saint-Henri` , quelle était l'année où, en moyenne, les prix totaux le plus élevés?
    * REPONSE: `2015`

1. Pour le quartier `Hochelaga-Maisonneuve` quelle était l'année où il y a eu le plus de ventes?
    * REPONSE: `2013`

1. Quel est le quartier où il y a le plus de ventes?
    * REPONSE: `Parc-Extension`

1. En `2013`, quel est le quartier où il y a le plus de ventes?
    * REPONSE: `Hochelaga-Maisonneuve`


1. En `2011`, quel quartier avait le prix au prix carré le plus élevé, en moyenne?
    * REPONSE: `Vieux-Montréal`


