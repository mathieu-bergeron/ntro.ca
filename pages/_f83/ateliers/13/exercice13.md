# Atelier 13: relations et champs calculés avec PowerPivot

## Partie 1: tableau croisé avec plusieurs tables

### Création des tables de données

Feuille `ventes_terrain`:

1. $[proc ../../proc/tableau_inserer](Insérer) un tableau
1. $[proc ../../proc/tableau_renommer](Renommer) ce tableau `VenteTerrain`

Feuille `ventes_residence`:

1. $[proc ../../proc/tableau_inserer](Insérer) un tableau
1. $[proc ../../proc/tableau_renommer](Renommer) ce tableau `VenteResidence`

Feuille `quartiers`:

1. $[proc ../../proc/tableau_inserer](Insérer) un tableau
1. $[proc ../../proc/tableau_renommer](Renommer) ce tableau `Quartier`

Feuille `arrondissements`:

1. $[proc ../../proc/tableau_inserer](Insérer) un tableau
1. $[proc ../../proc/tableau_renommer](Renommer) ce tableau `Arrondissement`

### Ajouter les relations

1. $[proc ../../proc/relation_ajouter](Ajouter la relation) suivante:
    * Table: `VenteTerrain` , colonne (externe): `ID_QUARTIER`
    * Table associée: `Quartier` , colonne associée (principale): `ID`

1. $[proc ../../proc/relation_ajouter](Ajouter la relation) suivante:
    * Table: `VenteResidence` , colonne (externe): `ID_TERRAIN`
    * Table associée: `VenteTerrain` , colonne associée (principale): `ID`

1. $[proc ../../proc/relation_ajouter](Ajouter la relation) entre les tables `Quartier` et `Arrondissement`
    * à vous de trouver les champs qui servent à relier ces tables!

### Insérer un tableau croisé

À partir de la feuille `ventes_terrain`:

1. $[proc ../../proc/tableau_croise_inserer](Insérer un tableau croisé)
    * ne **pas** insérer directement un *graphique croisé* sinon les étapes ci-bas ne fonctionnent pas

Sur la feuille du tableau:

1. $[proc ../../proc/tableau_croise_plus_de_tables](Ajouter) les autres tables (*PLUS DE TABLES*)

Vous avez maintenant une nouvelle feuille avec un tableau contenant toutes les tables

1. $[proc ../../proc/feuille_renommer](Renommer) la feuille où se trouve le tableau avec toutes les tables.
    * **Appeler** cette feuille: `analyse`

1. $[proc ../../proc/feuille_supprimer](Supprimer) la feuille où se trouve le tableau avec seulement les champs de `ventes_terrain`

Sur la feuille `analyse`

1. $[proc ../../proc/tableau_croise_inserer_graphique](Insérer) un graphique pour complémenter votre tableau


### Répondre aux questions

Sur une feuille `RÉPONSES`, répondre aux questions suivantes:

1. Quel sont les deux arrondissements avec le plus de quartier?

1. Quel est l'arrondissement où le prix moyen des résidences est le plus élevé?


## Partie 2: champs calculé avec PowerPivot

Pour répondre correctement à la dernière question, il faut calculer le prix actualisé.

Le hic est que l'appréciation mensuelle (`APPR_MOIS`) se trouve dans la table `Quartier`, la date de vente dans la table `VenteTerrain` et le prix dans
la table `VenteResidence`

Pour entrer une formule qui *recherche* l'information dans plusieurs tables, nous utilisons l'ajout `PowerPivot`

### Activer PowerPivot

3. $[proc ../../proc/pivot_activer](Activer PowerPivot)

4. On a maintenant l'onglet *PowerPivot*

### Ouvrir PowerPivot et vérifier les relations

1. $[proc ../../proc/pivot_ouvrir](Ouvrir) PowerPivot

1. $[proc ../../proc/pivot_diagramme](Ouvrir) la vue de `diagramme` et vérifier que vos tables et vos relations sont correctes


### Ajouter le champ calculé `PRIX_RESIDENCE_ACTU` à la table `VenteResidence`

1. $[proc ../../proc/pivot_donnees](Ouvrir la vue de données) et $[proc ../../proc/pivot_donnees_table](naviguer) jusqu'à la table `VenteResidence`

1. $[proc ../../proc/pivot_champ_calcule](Ajouter le champ calculé) suivant:
    * formule: `=(YEAR(NOW()) - YEAR(RELATED(VenteTerrain[DATE]))) * 12 + MONTH(NOW()) - MONTH(RELATED(VenteTerrain[DATE]))`
    * nom du champ: `MOIS_ECOULES`

1. $[proc ../../proc/pivot_champ_calcule](Ajouter le champ calculé) suivant:
    * formule: `=POWER(1+RELATED(Quartier[APPR_MOIS]);[MOIS_ECOULES])`
    * nom du champ: `FACTEUR_APPR`

1. $[proc ../../proc/pivot_champ_calcule](Ajouter le champ calculé) suivant:
    * formule: `[PRIX_RESIDENCE] * [FACTEUR_APPR]`
    * nom du champ: `PRIX_RESIDENCE_ACTU`

### Répondre aux questions

Revenir à la feuille `analyse`, les nouveaux champs devraient apparaître.

Sur la feuille `RÉPONSES`, répondre aux questions suivantes:

1. Quel est l'arrondissement où le prix moyen des résidences est le plus élevé?
    * Cette fois-ci, on utilise le champ calculé `PRIX_RESIDENCE_ACTU`. Est-ce que la réponse est différente?
