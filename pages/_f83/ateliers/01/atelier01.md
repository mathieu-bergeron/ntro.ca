# Atelier 1: environnement de travail au Collège

## Matière couverte

1. Recherche Web
1. Partage de fichiers au Collège
1. Archive de fichiers
1. Fichier Word
1. Fichier Excel
1. Courriel

## Utilisation de la billeterie

Vous **devez** utiliser la <a href="https://ciboulot.ca/billeterie/f83" target="_blank">billeterie</a> pour chaque demande d'aide, y compris en classe.

Votre utilisation de la billeterie est **évaluée** (voir *Journalisation* dans plan de cours)



## Tâches à effectuer

### Étape 0) S'incrire au système de billeterie

1. Naviguer sur <a href="https://ciboulot.ca/billeterie/f83" target="_blank">https://ciboulot.ca/billeterie/f83</a>
1. Cliquer sur créer un compte et entrer vos info


### Étape 1) Sur le Web, trouver comment...

1. Créer un nouveau dossier
1. Créer un nouveau document Word

### Étape 2) Sur votre `Z:\`, créer le dossier `F83`

### Étape 3) Dans le dossier `Z:\F83`, créer un nouveau document Word

* Nommer le fichier `VotrePrenomVotreNom` (p.ex. `MathieuBergeron` dans mon cas)
* Si vous avez créé le fichier directement dans Word, enregistrer-sous dans `Z:\F83`

### Étape 4) Sur le Web, trouver...

... et prendre vos réponses en note dans votre document Word:

1. Qu'est-ce qu'une extension de fichier?
1. Comment afficher les extensions de fichier dans l'explorateur Windows
1. Qu'est-ce qu'une archive de fichiers?
1. Donner des exemples d'extensions de fichier utilisées pour les archives
1. Comment ouvrir une archive de fichier?
1. Comment créer une archive de fichier contenant tous les fichiers d'un dossier

### Étape 5) Envoyer vos réponses à l'enseignant par courriel

* Envoyer votre document Word en pièce jointe
* TRUC: vous pouvez le faire par ColNet

### Étape 6) Sur `X:\` (Publics), naviguer jusqu'à `Bergeron, Mathieu`

### Étape 7) Identifier un fichier archive

### Étape 8) Copier cette archive dans votre dossier  `Z:\F83\`

* ASTUCE: dans l'explorateur Windows, faire Fichier => Ouvrir une nouvelle fenêtre
* Copier d'un emplacement à l'autre est beaucoup plus facile avec deux fenêtres

### Étape 9) Extraire l'archive

* Vous **devez** avoir des nouveaux dossiers dans `Z:\F83\`
* Double-cliquer sur une archive donne un aperçu des fichiers qui s'y trouvent, mais ce n'est pas la même chose qu'extraire l'archive.

### Étape 10) Identifier un fichier Excel dans un des dossiers de l'archive

### Étape 11) Ouvrir ce fichier Excel

* Le fichier contient une valeur dans la cellule *A1*
* La cellule *B1* est déjà sélectionnée

### Étape 12) En Excel, saisir &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`=A1 * 0.5` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(avec le `=`)

* Cette formule va calculer la moitié de la valeur en *A1*
* Noter la réponse dans votre document Word

### Étape 13) Fermer les documents Word et Excel

### Étape 14) Créer l'archive `VotrePrenomVotreNom.zip`

* Cette archive doit contenir tous les fichiers de votre dossier `Z:\F83`

### Étape 15) Remettre votre archive sur le `Y:\` (Dépôts)

* Naviguer jusqu'au sous-dossier `Y:\Bergeron, Mathieu\F83\Atelier01\`
* Copier ou glisser votre archive

### Étape 16) Remettre votre archive sur Moodle

* Visiter `REMISE: atelier #1` pour effectuer la remise
* Ensuite, revisiter `REMISE: atelier #1` pour vérifier la remise
