# Atelier 05: Références et propagation

## Préambule

Démarrer Excel et ouvrir le fichier `atelier_05.xlsx`

La feuille `ventes_residence` est une table de 8000 ventes de residences à Montréal. 
Voici la signification de chaque champ:

* `ANNEE`: année de la vente
* `NOM_QUARTIER`: le quartier où se situe la résidence
* `PRIX`: le prix de vente
* `TAXE`: la taxe de bienvenue (montant total)
* `TAXE TR1`: première tranche de la taxe de bienvenue
* `TAXE TR2`: deuxième tranche de la taxe de bienvenue
* `TAXE TR3`: troisième tranche de la taxe de bienvenue
* `TAXE TR4`: quatrième tranche de la taxe de bienvenue
* `TRANCHE 1`: montant de la vente correspondant à la première tranche
* `TRANCHE 2`: montant de la vente correspondant à la deuxième
* `TRANCHE 3`: montant de la vente correspondant à la troisième tranche
* `TRANCHE 4`: montant de la vente correspondant à la quatrième tranche
* `EXCÉDENT 1`: montant de la vente dépassant le début de la première tranche
* `EXCÉDENT 2`: montant de la vente dépassant le début de la deuxième tranche
* `EXCÉDENT 3`: montant de la vente dépassant le début de la troisième tranche
* `EXCÉDENT 4`: montant de la vente dépassant le début de la quatrième tranche

## Travail à effectuer

* Compléter la feuille de calcul
    * IMPORTANT: vous ne devez **pas** saisir de formule, seulement insérer les `$` (référence absolue et mixte) et propager les formules existantes
* Remttre votre travail sur Moodle

## Instructions

### Examiner chaque formule

Toutes les formules nécessaires sont déjà dans la feuille.

1. En *R7*: calcul du montant maximal par tranche
    * NOTE: pour simplifier, on a ajouté un maximum à la quatrième tranche

1. En *M2*: calcul du montant de la vente qui excéde la première tranche
    * La formule se lit comme ceci: 
        * `SI` le **prix** de vente est plus grand que le **début** de la **tranche**
        * `ALORS` l'excédent est le **prix** de vente moins le **début** de la **tranche**
        * `SINON` l'excédent est `0`

1. En *I2*: calcul du montant de la première tranche
    * La formule se lit comme ceci:
        * `SI` l'**excédent** est plus grand que le **maximum** pour cette tranche
        * `ALORS` utiliser le **maximum**
        * `SINON` utiliser l'**excédent**

1. En *E2*: calcul de la taxe pour une tranche

1. En *D2*: calcul de la taxe totale

### Insérer les références mixtes et absolues au bons endroits

Pour chaque formule, vous devez décider:

* Où utiliser une référence absolue (avec un `$` pour la colonne et un `$` pour la ligne)
* Où utiliser une référence mixte (avec un `$` pour la colonne *OU* un `$` pour la ligne)
* Où utiliser une référence normale (aucun `$`)

TRUCS:

* Cliquer sur la barre de formule pour afficher les références en couleur
* Réécrire la formule une case en base, puis une case à droite, etc.
    * Quels sont les colonnes/lignes qui doivent rester les mêmes?


### Propager les formules

Vous devez calculer la taxe de bienvenue pour chaque vente, c-à-d jusqu'à la ligne 8001.

1. À vous de trouver comment $[proc ../../proc/propager_formule](propager) chaque formule
    * TRUC: pour les grandes plages: $[proc ../../proc/selection](sélectionner) la plage avec la deuxième méthode
