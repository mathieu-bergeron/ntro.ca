# Exercice 11

<!--

## Partie 1: importer un fichier `.gml` ou `.xml`

1. **Naviguer** sur le site de *données ouvertes* de la ville de Montréal:
    * <a target='_blank' href='http://donnees.ville.montreal.qc.ca/'>http://donnees.ville.montreal.qc.ca/</a>

1. **Rechercher** avec le mot clé `arrondissements`

1. **Naviguer** vers l'entrée `Arrondissements de la Ville de Montréal`

1. **Cliquer** sur le fichier `LIMADMIN.gml`

1. **Clique-droit** sur le lien `http://donnees.ville.montreal.qc.ca/[...]/limadmin.gml`
    * **Choisir** *Enregistrer la cible sous*
    * **Enregistrer sous** sur votre `Z:\`

1. **Démarrer** Excel

1. $[proc ../../proc/ouvrir_xml](Ouvrir le fichier `.xml`) que vous avez téléchargé

1. $[proc ../../proc/enregistrer_sous](Enregistrer sous) sur votre `Z:\`

1. $[proc ../../proc/colonne_supprimer](Supprimer) toutes les colonnes sauf: `ns2:NOM` et `ns2:ABBREV`

1. En *A1*, **saisir** le nom de champ `CODE` (plutôt que `ns2:ABBREV`)

1. En *B1*, **saisir** le nom de champ `NOM` (plutôt que `ns2:NOM`)

1. $[proc ../../proc/feuille_renommer](Renommer) la feuille à `arrondissements`

1. $[proc ../../proc/table_convertir_en_plage](Convertir) la table en plage normale

1. $[proc ../../proc/trier](Trier) de `A à Z` selon le champ `NOM`

1. $[proc ../../proc/colonne_deplacer](Déplacer) les colonnes *A* et *B* pour que le champ `NOM` apparaisse en premier

-->

## Partie 1: importer des fichiers .csv


1. $[proc ../../proc/feuille_ajouter](Ajouter) une feuille

1. $[proc ../../proc/feuille_renommer](Renommer) la nouvelle feuille: `ventes_terrain`

1. $[proc ../../proc/importation_largeur_fixe](Importer) le fichier texte suivant:
    * `exercice11_ventes_terrain.csv`
    * Largeur fixe

1. $[proc ../../proc/feuille_ajouter](Ajouter) une feuille

1. $[proc ../../proc/feuille_renommer](Renommer) la nouvelle feuille: `quartiers`

1. $[proc ../../proc/importation_delimite](Importer) le fichier texte suivant:
    * `exercice11_quartiers.csv`
    * Délimité
    * Délimiteur: `;` (point virgule)

1. $[proc ../../proc/feuille_ajouter](Ajouter) une feuille

1. $[proc ../../proc/feuille_renommer](Renommer) la nouvelle feuille: `ventes_residence`

1. $[proc ../../proc/importation_delimite](Importer) le fichier texte suivant:
    * `exercice11_ventes_residence.csv`
    * Délimité
    * Délimiteur: *à vous de le déterminer*


## Partie 2: corriger les erreurs d'un fichier .csv

### Feuille `ventes_terrain`

1. $[proc ../../proc/colonne_formater](Formater) les colonnnes
    * TRUC: vérifier que la date est reconnue en utilisant le format `date longue`
    * TRUC: vérifier que les nombres sont reconnus en ajoutant des décimales

1. Corriger la colonne `DATE`
    * $[proc ../../proc/colonne_inserer](Insérer) une nouvelle colonne
    * Saisir le nom du champ
    * Saisir la correction des trois premières dates
        * il replacer en ordre: `ANNÉE-MOIS-JOUR`
    * $[proc ../../proc/remplissage](Remplir automatiquement) le reste de la colonne
    * Vérifier
    * $[proc ../../proc/colonne_supprimer](Supprimer) l'ancienne colonne

1. Corriger la colonne `PRIX_TERRAIN`
    * $[proc ../../proc/colonne_inserer](Insérer) une nouvelle colonne
    * Saisir le nom du champ
    * Saisir la correction des trois premiers prix
        * format: `123,45$`
    * $[proc ../../proc/remplissage](Remplir automatiquement) le reste de la colonne
    * Vérifier
    * $[proc ../../proc/colonne_supprimer](Supprimer) l'ancienne colonne

### Feuille `ventes_residences`

1. $[proc ../../proc/supprimer_doublons](Supprimer) les doublons

1. $[proc ../../proc/colonne_formater](Formater) les colonnnes

1. Corriger la colonne `PRIX_RESIDENCE` (comme ci-haut):
    * $[proc ../../proc/colonne_inserer](Insérer) une nouvelle colonne
    * Saisir le nom du champ
    * Saisir la correction des trois premiers prix
        * format: `123,45$`
    * $[proc ../../proc/remplissage](Remplir automatiquement) le reste de la colonne
    * Vérifier
    * $[proc ../../proc/colonne_supprimer](Supprimer) l'ancienne colonne

### Feuille `quartiers`

1. $[proc ../../proc/colonne_formater](Formater) les colonnnes

1. Corriger chaque colonne qui doit être corrigée
