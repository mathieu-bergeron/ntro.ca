# Atelier 11

## Procédures utiles

* $[proc ../../proc/importation_delimite](Importer) un fichier (délimité)
* $[proc ../../proc/importation_largeur_fixe](Importer) un fichier (largeur fixe)
* $[proc ../../proc/colonne_formater](Formater) une colonne
* $[proc ../../proc/supprimer_doublons](Supprimer) les doublons
* **Corriger** les données d'une colonne:
    * $[proc ../../proc/colonne_inserer](Insérer) une nouvelle colonne
    * Saisir le nom du champ
    * Saisir la correction des trois premiers enregistrements
    * $[proc ../../proc/remplissage](Remplir automatiquement) le reste de la colonne
    * Vérifier
    * $[proc ../../proc/colonne_supprimer](Supprimer) l'ancienne colonne

## Préambule

1. **Démarrer** Excel

1. $[proc ../../proc/enregistrer_sous](Enregistrer sous) sur votre `Z:\`

## 55pts) Partie 1: importer des fichiers .csv

### `quartiers`

1. 5pts) $[proc ../../proc/feuille_renommer](Renommer) la feuille: `quartiers`

1. 10pts) **Importer** le fichier texte suivant:
    * `atelier11_quartiers.csv`
    * À vous de déterminer:
        * largeur fixe ou délimité?
        * si délimité, quel est le délimiteur?

### `ventes_residence`

1. 5pts) $[proc ../../proc/feuille_ajouter](Ajouter) une feuille

1. 5pts) $[proc ../../proc/feuille_renommer](Renommer) la nouvelle feuille: `ventes_residence`

1. 10pts) **Importer** le fichier texte suivant:
    * `atelier11_ventes_residence.csv`
    * À vous de déterminer:
        * largeur fixe ou délimité?
        * si délimité, quel est le délimiteur?

### `ventes_terrain`

1. 5pts) $[proc ../../proc/feuille_ajouter](Ajouter) une feuille

1. 5pts) $[proc ../../proc/feuille_renommer](Renommer) la nouvelle feuille: `ventes_terrain`

1. 10pts) **Importer** le fichier texte suivant:
    * `atelier11_ventes_terrain.csv`
    * À vous de déterminer:
        * largeur fixe ou délimité?
        * si délimité, quel est le délimiteur?


## 45pts) Partie 2: corriger les données importées

15pts par table) Pour chaque table de données ajoutée ci-haut:

* **Corriger** les erreurs dans les données avec l'option `Remplissage automatique`
* **Supprimer** les doublons au besoin
* **Formater** chaque colonne


## Remise

1. Remettre votre fichier sur Moodle

<!--

NOTE: correction

* Feuille `quartiers`: 
    * doublons
    * corriger la colonne APPR_MOIS (point plutôt que virgule)

* Feuille `ventes_terrain`
    * doublons
    * corriger les colonnes DATE et PRIX_TERRAIN

* Feuille `ventes_terrain`
    * doublons
    * corriger les colonnes DATE et PRIX_TERRAIN

-->

