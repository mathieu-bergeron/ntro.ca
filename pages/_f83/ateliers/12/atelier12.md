# Atelier 12

## Procédures utiles

#### Importation

* $[proc ../../proc/importation_delimite](Importer) un fichier (délimité)
* $[proc ../../proc/importation_largeur_fixe](Importer) un fichier (largeur fixe)

#### Données

* $[proc ../../proc/colonne_formater](Formater) une colonne
* $[proc ../../proc/supprimer_doublons](Supprimer) les doublons
* **Corriger** les données d'une colonne:
    * $[proc ../../proc/colonne_inserer](Insérer) une nouvelle colonne
    * Saisir le nom du champ
    * Saisir la correction des trois premiers enregistrements
    * $[proc ../../proc/remplissage](Remplir automatiquement) le reste de la colonne
    * Vérifier
    * $[proc ../../proc/colonne_supprimer](Supprimer) l'ancienne colonne

#### Macros

* $[proc ../../proc/macro_enregistrer_references_relatives](Enregistrer une macro) (références relatives)
* $[proc ../../proc/macro_enregistrer_references_absolues](Enregistrer une macro) (références absolues)
* $[proc ../../proc/macro_modifier](Modifier une macro)
* $[proc ../../proc/macro_sauvegarder](Sauvegarder une macro modifiée)
* $[proc ../../proc/macro_exécuter](Éxécuter une macro)
* $[proc ../../proc/inserer_controle_bouton](Ajouter un bouton) et y affecter une macro

## Préambule

1. **Démarrer** Excel

1. **IMPORTANT**: $[proc ../../proc/enregistrer_sous_format](Enregistrer sous) sur votre `Z:\` au **format `.xlsm`**
    * Note: c'est le format qui permet de sauvegarder vos macros

## 25pts) Partie 1: importer des fichiers `.csv`

1. 5pts) $[proc ../../proc/feuille_renommer](Renommer) la feuille courante: `ventes_terrain`

1. 5pts) **Importer** le fichier texte suivant:
    * `atelier12_ventes_terrain.csv`
    * À vous de déterminer:
        * largeur fixe ou délimité?
        * si délimité, quel est le délimiteur?

1. 5pts) $[proc ../../proc/feuille_ajouter](Ajouter) une feuille

1. 5pts) $[proc ../../proc/feuille_renommer](Renommer) la feuille courante: `quartiers`

1. 5pts) **Importer** le fichier texte suivant:
    * `aterlier12_quartiers.csv`
    * À vous de déterminer:
        * largeur fixe ou délimité?
        * si délimité, quel est le délimiteur?

## 75pts) Partie 2: corriger les erreurs

1. 60pts) **Corriger** les erreurs dans les données à l'aide des techniques suivantes:

    * Enregistrement et modification de macro
    * Remplissage automatique
    * Supprimer les doublons

1. 15pts) **Formater** les colonnes correctement

## Remise

1. Remettre votre fichier sur Moodle
