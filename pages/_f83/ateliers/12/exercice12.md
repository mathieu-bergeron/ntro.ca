# Exercice 12

## Procédures utiles

* $[proc ../../proc/importation_delimite](Importer) un fichier (délimité)
* $[proc ../../proc/importation_largeur_fixe](Importer) un fichier (largeur fixe)
* $[proc ../../proc/colonne_formater](Formater) une colonne

## Préambule

1. **Démarrer** Excel

1. $[proc ../../proc/ouvrir](Ouvrir) le fichier `exercice12.xlsx` disponible sur Moodle

1. **IMPORTANT**: $[proc ../../proc/enregistrer_sous_format](Enregistrer sous) sur votre `Z:\` au **format `.xlsm`**
    * Note: c'est le format qui permet de sauvegarder vos macros

## Partie 1: importer un fichier `.csv`

1. $[proc ../../proc/feuille_ajouter](Ajouter) une feuille

1. $[proc ../../proc/feuille_renommer](Renommer) cette feuille: `ventes_terrain`

1. **Importer** le fichier texte suivant:
    * `exercice12_ventes_terrain.csv`
    * À vous de déterminer:
        * largeur fixe ou délimité?
        * si délimité, quel est le délimiteur?

## Partie 2: corriger les erreurs avec une macro

1. **Remarquer** l'erreur à corriger:
    * À chaque trois lignes, les données sont décalées d'une cellule

1. $[proc ../../proc/ajouter_onglet_developpeur](Ajouter l'onget développeur)

1. $[proc ../../proc/macro_enregistrer_references_relatives](Enregistrer la macro suivante):
    * Utiliser des *références relatives*
    * Placer le curseur en *B2* avant d'enregistrer
    * Nom de la macro: `CorrigerLigne`
    * Actions de la macro:
        1. Sélectionner la plage *B2:F2*
        1. Avec la souris: déplacer la plage d'une cellule vers la gauche
        1. Placer le curseur en *B5* (prochaine ligne à réparer)

1. **S'assurer** d'arrêter l'enregistrement

1. $[proc ../../proc/macro_executer](Exécuter la macro) quelques fois pour vérifier que ça fonctionne
    * ne **pas** bouger le curseur à la main entre les exécutions de la macro

1. $[proc ../../proc/macro_modifier](Modifier le code de la macro)
    * **Après** la ligne `Sub CorrigerLigne()`, ajouter:

             For i = 1 to 3333

    * **Avant** la ligne `End Sub`, ajouter:

             Next i

1. $[proc ../../proc/macro_sauvegarder](Sauvegarder la macro)

1. $[proc ../../proc/macro_executer](Exécuter la macro) modifiée afin de corriger toutes les données
    * ne **pas** bouger le curseur à la main avant d'exécuter la macro
    * l'exécution va prendre du temps!

1. **Vérifier** que chaque ligne est corrigée

## Partie 3: création d'un formulaire de saisie

Sur la feuille `ventes_terrain`

1. En *I1:M1*, **copier** les en-têtes qui se trouvent en *A1:E1*

1. En *I2*, $[proc ../../proc/saisir_formule](saisir la formule) suivante qui va générer automatiquement un ID 
    * `="ter" & NBVAL(A:A)`

1. Sous *K2* $[proc ../../proc/inserer_controle_menu](insérer un menu déroulant)

1. $[proc ../../proc/controle_menu_parametres](modifier les paramètres) du menu ainsi:
    * Plage d'entrée: sur la feuille `quartiers`, sélectionner la plage *B2:B33*
    * Cellule liée: *K10*
    * Nombre de lignes: 32

1. En *K11*, $[proc ../../proc/saisir_formule](saisir la formule) suivante:
    * `=ADRESSE(K10+1;1;1;1;"quartiers")`
        * Note: cette formule va construire l'adresse (référence) pour le code de quartier choisi dans le menu déroulant

1. En *K2*, $[proc ../../proc/saisir_formule](saisir la formule) suivante:
    * `=INDIRECT(K11)`
        * Note: cette formule va chercher la valeur de l'adresse construite en *K11*

1. **Vérifier** que *K2* que vous avez le bon code de quartier 

1. Ajouter un enregistrement:
    * Choisir la date, le prix et l'aire

1. $[proc ../../proc/macro_enregistrer_references_absolues](Enregistrer la macro suivante):
    * Désactiver l'option *Utiliser les réféfences relatives*
    * Nom de la macro: `AjouterEnregistrement`
    * Actions de la macro:
        1. Sélectionner la plage *I2:M2*
        1. Copier avec CTRL + C
        1. Sélectionner *A1*, puis appuyer sur CTRL + ↧
        1. Appuyer sur ↧ pour atteindre une ligne vide
        1. $[proc ../../proc/coller_valeurs](Coller les valeurs) avec *collage spécial => valeurs*

1. **S'assurer** d'arrêter l'enregistrement


1. $[proc ../../proc/macro_executer](Exécuter la macro) quelques fois pour tester la copie des valeurs

1. $[proc ../../proc/macro_modifier](Modifier le code de la macro) pour ajouter une nouvelle ligne à chaque fois
    * Pour **l'avant-dernière** ligne
        * Remplacer:

                Range("A10002").Select

        * Par:

                Selection.Offset(1,0).Select

1. $[proc ../../proc/macro_sauvegarder](Sauvegarder la macro)

1. $[proc ../../proc/macro_executer](Exécuter la macro) quelques fois pour tester qu'une ligne est ajoutée à chaque fois

1. Près de *M2*, $[proc ../../proc/inserer_controle_bouton](ajouter un bouton) et y affecter la macro `AjouterEnregistrement`

1. **Ajouter** quelques enregistrements pour tester le bouton
