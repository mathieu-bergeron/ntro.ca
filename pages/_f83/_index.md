---
title: "F83"
bookHidden: true
---


# Séance 09

## Théorie

* {{% link "theorie/09/README" "Lisez-moi" %}}
* {{% link "theorie/09/explorations" "Explorations" %}}
* {{% link "theorie/09/demonstrations" "Démonstrations" %}}

### Exercices (formatif)

* {{% link "ateliers/09/exercice09A" "Exercice 09A" %}}
* {{% link "ateliers/09/exercice09B" "Exercice 09B" %}}

### Atelier (sommatif)

* {{% link "ateliers/09/atelier09" "Atelier 09" %}}

## Séance 11

### Théorie

* {{% link "theorie/11/explorations" "Explorations" %}}
* {{% link "theorie/11/demonstrations" "Démonstrations" %}}

### Exercice (formatif)

* {{% link "ateliers/11/exercice11" "Exercice 11" %}}

### Atelier (sommatif)

* {{% link "ateliers/11/atelier11" "Atelier 11" %}}

## Séance 12

### Théorie

* {{% link "theorie/12/explorations" "Explorations" %}}
* {{% link "theorie/12/demonstrations" "Démonstrations" %}}

### Exercice (formatif)

* {{% link "ateliers/12/exercice12" "Exercice 12" %}}

### Atelier (sommatif)

* {{% link "ateliers/12/atelier12" "Atelier 12" %}}

## Séance 13

### Théorie

* {{% link "theorie/13/explorations" "Explorations" %}}
* {{% link "theorie/13/demonstrations" "Démonstrations" %}}

### Exercice (formatif)

* {{% link "ateliers/13/exercice13" "Exercice 13" %}}

### Atelier (sommatif)

* {{% link "ateliers/13/atelier13" "Atelier 13" %}}
