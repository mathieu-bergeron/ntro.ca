---
title: "Point de vue sur l'IA"
weight: 40
aliases: ["/ia"]
---

{{% pageTitle %}}


Voir mon texte <a href="https://revueliberte.ca/article/1552/le-bien-commun-a-l-ere">Le bien commun à l'ère des algorithmes</a>. 

* (aussi disponible sur <a href="https://www.erudit.org/fr/revues/liberte/2021-n329-liberte05727/94660ac/">erudit.org</a>)

## En résumé, l'IA devrait:

1. servir d'outil pédagogique (l'humain doit préserver sa propre compréhension du monde)
2. s'intégrer à des outils libres (l'humain doit décider quand/comment appliquer l'IA)
3. s'intégrer à une économie robuste (p.ex. en rémunérant les artistes «plagiés» par l'IA)

Et pour y arriver, il faut miser sur une démocratisation du code source (de la même façon que la démocratisation de la lecture/écriture et des lois a permis l'essor des démocraties
 modernes). Voir p.ex. l'histoire des lois romaines, le conflit entre les <a href="https://fr.wikipedia.org/wiki/Loi_des_Douze_Tables">patriciens et les plébéiens</a>.

P.ex. chatGPT ne règle pas les questions importantes posées par le code source: comment décrire précisément ce qu'on veut que nos systèmes informatiques réalisent? 
Comment en discuter et se mettre d'accord? L'approximation n'est pas suffisante. Comme
un chercheur en IA <a href="http://www.argmin.net/2017/12/05/kitchen-sinks/">écrivait en 2017</a>: 
«If you’re building photo sharing services, alchemy is fine. But we’re now building systems that govern health care and our participation in civil debate. I would like to live in a world whose systems are build on rigorous, reliable, verifiable knowledge, and not on alchemy. »

## Sur la compréhension artificielle du monde

* voir <a href="https://en.wikipedia.org/wiki/From_Bacteria_to_Bach_and_Back">From Bacteria to Bach and Back. The Evolution of Minds</a> de Daniel Dennett
    * du même auteur, en traduction: *Consience Expliquée* et *Darwin est-il dangeureux?*

## Pour la liberté et l'informatique

* voir <a href="https://framablog.org/2010/05/22/code-is-law-lessig/">Le code fait loi</a> de Lawrence Lessig
    * version originale ici: <a href="https://www.harvardmagazine.com/2000/01/code-is-law-html">Code Is Law</a>

## Pour éviter un effondrement économique

* voir <a href="https://en.wikipedia.org/wiki/Who_Owns_the_Future%3F">Who Owns the Future?</a> de Jaron Lanier
    * disponible en traduction: *Internet: qui possède le futur?*

## Sur la fiabilité de l'IA

* voir <a href="http://rebooting.ai/"/>Rebooting AI: Building Artificial Intelligence We Can Trust</a>



