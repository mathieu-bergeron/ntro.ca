# Copyright (C) (2019) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
#
# This file is part of tp01_menu
#
# aquiletour is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# aquiletour is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with aquiletour.  If not, see <https://www.gnu.org/licenses/>

##### INCLUDE #####
this_dir=$(readlink -f "$0")
scripts_dir=$(dirname "$this_dir")
. "$scripts_dir/include.sh"
###################

patron="$1"
antipatron="$2"
antipatron2="$3"

find_path=""
if [ "$patron" != "" ]; then
    find_path="-path \"*$patron*\""
fi

dont_find_path=""
if [ "$antipatron" != "" ]; then
    dont_find_path="! -path \"*$antipatron*\""
fi

dont_find_path2=""
if [ "$antipatron2" != "" ]; then
    dont_find_path2="! -path \"*$antipatron2*\""
fi

save_dir

cd "$root_dir"

cd "$publishDir"


if [ -e .sftp.commands ]; then
    rm .sftp.commands
fi

find_command=$(echo "find . -type f $find_path $dont_find_path $dont_find_path2")

eval "$find_command" | while read i; 
do
    echo $i

    repertoire_distant="www/ntro.ca/$i"
    echo put \"$i\" \"$repertoire_distant\" >> .sftp.commands
done

sftp -b .sftp.commands mbergeron@ntro.ca

rm .sftp.commands

restore_dir
