# compile simply means removing all spaces and newlines

keep_endlines="3c6_remise_moodle.html 3c6_remise_gitlab.html"

for i in *.html;
do
    target=../../layouts/shortcodes/$i
    must_keep_endlines=$(echo $keep_endlines | grep $i)

    if [ "$must_keep_endlines" = "" ]; then
        echo delete endlines $i
        cat $i | tr -d '\n' | sed "s/}[[:space:]]\+/}/g" | sed 's/\xc2\xa0//g'>  $target
    else
        echo keep endlines $i
        cat $i | sed "s/}[[:space:]]\+/}/g" | sed 's/\xc2\xa0//g'>  $target
    fi

done

