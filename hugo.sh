draft=$1

if [ "$draft" != "" ]; then
    draft=" -D "
fi

render_flag="--renderToMemory=false"
hugo_version=$(hugo version)

echo $hugo_version | grep "v0.92"

if [ $? -eq 0 ]; then
    render_flag="--renderToDisk"
fi

hugo server "$render_flag" "$draft"
